<?php
$except = ['show'];
$extra = [];

$extraRoute = [
    'index' => [
        'routeMethod' => 'index',
        'routeUrl' => '',
    ],

    'update' => [
        'routeMethod' => 'update',
        'routeUrl' => '',
    ],
];

$extraTourImport = [
    'getImport' => [
        'routeMethod' => 'getImport',
        'routeUrl' => '',
    ],

    'import' => [
        'routeMethod' => 'import',
        'routeUrl' => '',
    ],

    'export' => [
        'routeMethod' => 'export',
        'routeUrl' => '/export',
    ],
];

$extraTours = array_merge($extra, ['duplicate','hotel']);

return [
    'dashboard' => ['except' => 'all', 'extra' => $extraRoute],
    'brand' => ['except' => $except, 'extra' => $extra],
    'comment' => ['except' => $except, 'extra' => $extra],
    'faq' => ['except' => $except, 'extra' => $extra],
    'order' => ['except' => $except, 'extra' => $extra],
    'redirect' => ['except' => $except, 'extra' => $extra],
    'user' => ['except' => $except, 'extra' => $extra],
    'page' => ['except' => $except, 'extra' => $extra],
    'about' => ['except' => $except, 'extra' => $extra],
    'inquire' => ['except' => $except, 'extra' => $extra],
    'home' => ['except' => $except, 'extra' => $extra],
    'howItWork' => ['except' => $except, 'extra' => $extra],
    'popup' => ['except' => $except, 'extra' => $extra],

    'review' => ['except' => $except, 'extra' => $extra],
    'reviewCategory' => ['except' => $except, 'extra' => $extra],

    'hotel' => ['except' => $except, 'extra' => $extra],
    'hotelType' => ['except' => $except, 'extra' => $extra],
    'hotelRoomType' => ['except' => $except, 'extra' => $extra],
    'hotelCity' => ['except' => $except, 'extra' => $extra],

    'member' => ['except' => $except, 'extra' => $extra],
    'memberConfig' => ['except' => $except, 'extra' => $extra],

    'blog' => ['except' => $except, 'extra' => $extra],
    'blogCategory' => ['except' => $except, 'extra' => $extra],
    'blogTag' => ['except' => $except, 'extra' => $extra],
    'blogConfig' => ['except' => $except, 'extra' => $extra],

    'generalConfig' => ['except' => $except, 'extra' => $extra],
    'socialConfig' => ['except' => $except, 'extra' => $extra],
    'menuConfig' => ['except' => $except, 'extra' => $extra],
    'footerMenuConfig' => ['except' => $except, 'extra' => $extra],
    'langConfig' => ['except' => $except, 'extra' => $extra],
    'emailConfig' => ['except' => $except, 'extra' => $extra],

    'tour' => ['except' => $except, 'extra' => $extraTours],
    'tourActivity' => ['except' => $except, 'extra' => $extra],
    'tourDestination' => ['except' => $except, 'extra' => $extra],
    'tourFilter' => ['except' => $except, 'extra' => $extra],
    'tourImport' => ['except' => 'all', 'extra' => $extraTourImport],
    'tourConfig' => ['except' => $except, 'extra' => $extra],

    'contact' => ['except' => $except, 'extra' => $extra],
    'contactAddress' => ['except' => $except, 'extra' => $extra],
    'contactSubscribe' => ['except' => $except, 'extra' => $extra],
    'contactCollection' => ['except' => $except, 'extra' => $extra],
    'contactConfig' => ['except' => $except, 'extra' => $extra],
];
