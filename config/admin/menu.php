<?php
use App\Enum\RoleEnum;

return [

    'dashboard' => [
        'name' => 'Bảng điều khiển',
        'icon' => 'fa-dashboard',
        'route' => 'dashboard.index',
    ],

    'user' => [
        'name' => 'Quản trị viên',
        'icon' => 'fa-users',
        'route' => 'user.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],

    ],

    'order' => [
        'name' => 'Đơn hàng',
        'icon' => 'fa-cart-plus',
        'route' => 'order.index',
        'role' => RoleEnum::ROLE_GROUP['saleAdmin'],
    ],

    'comment' => [
        'name' => 'Bình luận',
        'icon' => 'fa fa-comments-o',
        'route' => 'comment.index',
        'role' => RoleEnum::ROLE_GROUP['saleAdmin'],
    ],

    'review' => [
        'name' => 'Review',
        'icon' => 'fa fa-newspaper-o',
        'role' => RoleEnum::ROLE_GROUP['tourAdmin'],
        'children' => [
            ['name' => 'Danh sách', 'route' => 'review.index'],
            ['name' => 'Danh mục', 'route' => 'reviewCategory.index'],
        ]
    ],

    'home' => [
        'name' => 'Trang chủ',
        'icon' => 'fa-home',
        'route' => 'home.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'page' => [
        'name' => 'Trang',
        'icon' => 'fa-file-text',
        'route' => 'page.index',
        'role' => RoleEnum::ROLE_GROUP['editor'],
    ],

    'blog' => [
        'name' => 'Tin tức',
        'icon' => 'fa-newspaper-o',
        'role' => RoleEnum::ROLE_GROUP['editor'],
        'children' => [
            ['name' => 'Danh sách', 'route' => 'blog.index'],
            ['name' => 'Danh mục', 'route' => 'blogCategory.index'],
            ['name' => 'Thẻ', 'route' => 'blogTag.index'],
            ['name' => 'Cấu hình', 'route' => 'blogConfig.index', 'role' => RoleEnum::ROLE_GROUP['admin'],],
        ]
    ],

    'contact' => [
        'name' => 'Liên hệ',
        'icon' => 'fa-odnoklassniki',
        'role' => RoleEnum::ROLE_GROUP['saleAdmin'],
        'children' => [
            ['name' => 'Danh sách liên hệ', 'route' => 'contact.index'],
            ['name' => 'Danh sách địa chỉ', 'route' => 'contactAddress.index'],
            ['name' => 'Danh sách subscribe', 'route' => 'contactSubscribe.index'],
            ['name' => 'Danh sách mail collection', 'route' => 'contactCollection.index'],
            ['name' => 'Cấu hình', 'route' => 'contactConfig.index', 'role' => RoleEnum::ROLE_GROUP['admin'],],
        ]
    ],

    'tour' => [
        'name' => 'Tour',
        'icon' => 'fa-flag',
        'role' => RoleEnum::ROLE_GROUP['tourAdmin'],
        'children' => [
            ['name' => 'Danh sách', 'route' => 'tour.index'],
            ['name' => 'Activities', 'route' => 'tourActivity.index'],
            ['name' => 'Destination', 'route' => 'tourDestination.index'],
            ['name' => 'Cấu hình lọc tour', 'route' => 'tourFilter.index', 'role' => RoleEnum::ROLE_GROUP['admin']],
            ['name' => 'Import/Export', 'route' => 'tourImport.getImport', 'role' => RoleEnum::ROLE_GROUP['admin']],
            ['name' => 'Cấu hình', 'route' => 'tourConfig.index', 'role' => RoleEnum::ROLE_GROUP['admin']],
        ]
    ],

    'hotel' => [
        'name' => 'Khách sạn',
        'icon' => 'fa-building-o',
        'role' => RoleEnum::ROLE_GROUP['tourAdmin'],
        'children' => [
            ['name' => 'Danh sách', 'route' => 'hotel.index'],
            ['name' => 'Kiểu khách sạn', 'route' => 'hotelType.index'],
            ['name' => 'Kiểu phòng', 'route' => 'hotelRoomType.index'],
            ['name' => 'Thành phố', 'route' => 'hotelCity.index'],
        ]
    ],

    'about' => [
        'name' => 'Giới thiệu',
        'icon' => 'fa-exclamation-circle',
        'route' => 'about.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'member' => [
        'name' => 'Thành viên',
        'icon' => 'fa-user',
        'role' => RoleEnum::ROLE_GROUP['admin'],
        'children' => [
            ['name' => 'Danh sách', 'route' => 'member.index'],
            ['name' => 'Cấu hình', 'route' => 'memberConfig.index'],
        ]

    ],

    'faq' => [
        'name' => 'FAQ',
        'icon' => 'fa-question-circle-o',
        'route' => 'faq.index',
        'role' => RoleEnum::ROLE_GROUP['editor'],
    ],

    'inquire' => [
        'name' => 'Inquire page',
        'icon' => 'fa-fort-awesome',
        'route' => 'inquire.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'redirect' => [
        'name' => 'Redirect',
        'icon' => 'fa-external-link',
        'route' => 'redirect.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'brand' => [
        'name' => 'Thương hiệu',
        'icon' => 'fa-bold',
        'route' => 'brand.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'popup' => [
        'name' => 'Popup',
        'icon' => 'fa-window-restore',
        'route' => 'popup.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'howItWork' => [
        'name' => 'How it work',
        'icon' => 'fa-question',
        'route' => 'howItWork.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'backup' => [
        'name' => 'Backup',
        'icon' => 'fa-recycle',
        'route' => 'backup.index',
        'role' => RoleEnum::ROLE_GROUP['admin'],
    ],

    'config' => [
        'name' => 'Cấu hình',
        'icon' => 'fa-cogs',
        'role' => RoleEnum::ROLE_GROUP['admin'],
        'children' => [
            ['name' => 'Cấu hình chung', 'route' => 'generalConfig.index'],
            ['name' => 'Menu', 'route' => 'menuConfig.index'],
            ['name' => 'Mạng xã hội', 'route' => 'socialConfig.index'],
            ['name' => 'Footer menu', 'route' => 'footerMenuConfig.index'],
            ['name' => 'Ngôn ngữ', 'route' => 'langConfig.index'],
            ['name' => 'Mail SMTP', 'route' => 'emailConfig.index'],
        ]
    ],

];
