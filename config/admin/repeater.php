<?php
    return [
        'email' => ['comment-bcc','contact-bcc','subscribe-bcc','tour-bcc'],
        'popup' => ['collection-blacklist','collection-corner-blacklist','direction-blacklist', 'black-list'],
        'home' => ['destination','tripadvisor'],
        'faq' => ['content'],
        'inquire' => ['hotel','travel'],
        'tour' => ['filter-duration','filter-price','hotel-list','hotel-type','plan'],
    ];
