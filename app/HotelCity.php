<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelCity extends Model
{
    protected $fillable = ['name'];

    public function hotels()
    {
        return $this->hasMany(Hotel::class, 'city_id', 'id');
    }

    public function tours()
    {
        return $this->belongsToMany(Hotel::class, 'hotel_city_tour', 'hotel_city_id','tour_id');
    }
}
