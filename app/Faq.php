<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $casts = [
        'content' => 'array'
    ];

    protected $fillable = ['name', 'content', 'type', 'status', 'lang_id'];

    public function tour()
    {
        return $this->hasMany(Tour::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function members()
    {
        return $this->belongsToMany(Member::class, 'faq_member');
    }
}
