<?php

namespace App\Http\Controllers;

use App\Lang;
use Illuminate\Http\Request;

class LangController extends BaseController
{
    protected function getModel(): String
    {
        return 'Lang';
    }

    private $langActive = [
        'en',
        'fr',
        'de',
        'es',
        'it',
        'vi',
    ];

    public function changeLang(Request $request, $lang)
    {
        if (in_array($lang, $this->langActive)) {
            $request->session()->put(['lang' => $lang]);
            return back();
        }
    }
}
