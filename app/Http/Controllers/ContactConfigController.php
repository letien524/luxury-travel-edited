<?php

namespace App\Http\Controllers;


use App\Category;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class ContactConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'category';
    }

    public function index()
    {
        $data['contactConfig'] = Category::firstOrCreate(['type' => 'contact-category-main']);

        return view('backend.contactConfig.index', $data);
    }

    public function update(Request $request, Category $contactConfig)
    {

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $contactConfig->update($input);

        return back()->with( CommonEnum::MESSAGES['success']);
    }

}
