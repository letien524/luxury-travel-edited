<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use Illuminate\Http\Request;

class BlogConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'category';
    }

    public function index()
    {

        $data = [
            'config' => Category::firstOrCreate(['type'=>'blog-category-main']),
        ];
        return view('backend.blogConfig.index', $data);
    }

    public function update(Request $request, Category $config)
    {

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $config->update($input);

        return back()->with( CommonEnum::MESSAGES['success']);
    }

}
