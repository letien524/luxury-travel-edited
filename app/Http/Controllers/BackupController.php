<?php

namespace App\Http\Controllers;


use App\Enum\BackupEnum;
use App\Enum\CommonEnum;
use App\Http\Requests\BackupRequest;
use App\Jobs\BackupJob;
use App\TourDestination;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Exception;

class BackupController extends BaseController
{
    protected function getModel(): String
    {
       return '';
    }

    public function index()
    {

        $backupFiles = Storage::disk('backup')->allFiles();

        $data = [
            'backupFiles' => array_reverse($backupFiles),
        ];

        return view('backend.backup.index', $data);
    }

    public function create(){

        BackupJob::createSeeder();
        BackupJob::createBackup();

        $files = backupDisk()->allFiles();
        $newFile = end($files);

        if (backupDisk()->exists($newFile)) {
            return backupDisk()->download($newFile);
        }

        return back()->with(BackupEnum::MESSAGES['invalidFile']);
    }

    public function download(Request $request){

        $this->validate($request,['file' => 'required']);

        $file = $request->input('file');

        if(backupDisk()->exists($file)){
            return backupDisk()->download($file);
        }

        return back()->with(BackupEnum::MESSAGES['invalidFile']);


    }

    public function restore(BackupRequest $request)
    {
        if ($request->isMethod('POST')) {
            $file = $request->file('import_file');
        }
        else {
            $file = $request->input('import_file');
            $file = getBackupPath($file);
        }

        $logFiles = Zipper::make($file)->listFiles();

        $folderNeed = ['database/seeds', 'public/uploads'];

        $files = array_filter($logFiles, function ($value) use ($folderNeed) {
            $count = 0;
            foreach ($folderNeed as $target) {
                if (strpos($value, $target) !== false) {
                    $count++;
                    break;
                }
            }

            if ($count === 1) {
                return $value;
            }
        });

        if (count($files) !== count($logFiles)) {
            return back()->with(BackupEnum::MESSAGES['invalidFormat']);
        }

        Zipper::make($file)->extractTo(base_path());

        DB::beginTransaction();
        try{
            Artisan::call('migrate:fresh', ['--seed' => true]);
            DB::commit();
        }catch (Exception $exception){
            DB::rollBack();
            Log::error($exception);
            return back()->with(BackupEnum::MESSAGES['restoreError']);
        }

        Auth::logout();

        return redirect()->route('login')->with(CommonEnum::MESSAGES['success']);
    }

    public function dataTransfer(){

        $activities = \App\Category::where('type', 'tour-category')->get()->toArray();
        foreach ($activities as $item){
            $item = array_filter($item, function($value){
                return !is_null($value);
            });
            unset($item['type']);
            DB::table('tour_activities')->insert($item);
        }

        $destinations = \App\Category::where('type', 'destination-category')->get()->toArray();
        foreach ($destinations as $item){
            $item = array_filter($item, function($value){
                return !is_null($value);
            });
            unset($item['type']);
            DB::table('tour_destinations')->insert($item);
        }


        if (Schema::hasColumn('activity_tour', 'category_id')) {
            Schema::table('activity_tour', function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->renameColumn('category_id', 'activity_id');
            });
        }

        if (Schema::hasColumn('destination_tour', 'category_id')) {
            Schema::table('destination_tour', function (\Illuminate\Database\Schema\Blueprint $table) {
                $table->renameColumn('category_id', 'destination_id');
            });
        }

        return back()->with(CommonEnum::MESSAGES['success']);
    }

    public function dataTransferMember(){
        if (Schema::hasColumn('members', 'excerpt')) {
            $members = \App\Member::all();
            foreach ($members as $member){
                $member->excerpt = $member->content;
                $member->slug = Str::slug($member->name);
                $member->save();
            }
        }

        return back()->with(CommonEnum::MESSAGES['success']);
    }

    public function dataTransferDestinationReview(){
        if (Schema::hasColumn('destination_review', 'review_category_id')) {
            $data = DB::table('destination_review')->get();
            foreach ($data as $item){
                $tourDestination = TourDestination::find($item->destination_id);
                $tourDestination->review_category_id = $item->review_category_id;
                $tourDestination->save();
            }
        }

        return back()->with(CommonEnum::MESSAGES['success']);
    }

    public function migrate(){
        DB::beginTransaction();
        try{
            Artisan::call('migrate:fresh');
            DB::commit();
            return back()->with(CommonEnum::MESSAGES['success']);
        }catch (Exception $exception){
            DB::rollBack();
            Log::error($exception);
            return back()->with(BackupEnum::MESSAGES['restoreError']);
        }
    }

    public function seed(){
        DB::beginTransaction();
        try{
            Artisan::call('migrate:fresh');
            DB::commit();
            return back()->with(CommonEnum::MESSAGES['success']);
        }catch (Exception $exception){
            DB::rollBack();
            Log::error($exception);
            return back()->with(BackupEnum::MESSAGES['restoreError']);
        }

    }

    public function migrateSeed(){
        DB::beginTransaction();
        try{
            Artisan::call('migrate:fresh');
            Artisan::call('db:seed');
            DB::commit();
            return back()->with(CommonEnum::MESSAGES['success']);
        }catch (Exception $exception){
            DB::rollBack();
            Log::error($exception);
            return back()->with(BackupEnum::MESSAGES['restoreError']);
        }

    }

    public function destroy($id)
    {
        $request = new Request();
        $file = $request->input('file');

        if (!backupDisk()->exists($file)) {
            return back()->with(CommonEnum::MESSAGES['bulkDestroyFailByCheckbox']);
        }

        backupDisk()->delete($file);

        return back()->with(CommonEnum::MESSAGES['success']);

    }

    public function bulkDestroy(Request $request)
    {
        $data = $data = $request->input('chkItem');

        if (!empty($data)) {
            foreach ($data as $file) {
                backupDisk()->delete($file);
            }

            return back()->with(CommonEnum::MESSAGES['success']);
        }

        return back()->with(CommonEnum::MESSAGES['bulkDestroyFailByCheckbox']);

    }


}
