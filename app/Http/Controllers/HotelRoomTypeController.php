<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\HotelRoomType;
use App\Http\Requests\HotelRoomTypeRequest;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class HotelRoomTypeController extends BaseController
{
    protected function getModel(): String
    {
        return 'hotelRoomType';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = HotelRoomType::latest('id')->get(['id','name']);
            $datatables = DataTables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);
                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.hotelRoomType.index', $data);
    }


    public function create()
    {
        return view('backend.hotelRoomType.create-edit');
    }

    public function store(HotelRoomTypeRequest $request)
    {
        HotelRoomType::create($request->all());
        return back()->with(CommonEnum::MESSAGES['success']);
    }


    public function edit(HotelRoomType $hotelRoomType)
    {
        $data = [
            'configs' => ['action'=>'update'],
            'hotelRoomType' => $hotelRoomType,
        ];
        return view('backend.hotelRoomType.create-edit', $data);
    }


    public function update(HotelRoomTypeRequest $request, HotelRoomType $hotelRoomType)
    {
        $hotelRoomType->update($request->all());
        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
