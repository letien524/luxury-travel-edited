<?php

namespace App\Http\Controllers;

use App\Config;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class PopupConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'config';
    }

    public function index()
    {
        $popup = Config::firstOrCreate(['type' => 'popup']);

        return view('backend.popupConfig.index', compact('popup'));
    }

    public function update(Request $request, Config $popupConfig)
    {

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $popupConfig->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
