<?php

namespace App\Http\Controllers;

use App\Config;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class FooterMenuConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'config';
    }

    public function index()
    {
        $data['footer_menu'] = Config::firstOrCreate(['type' => 'footer_menu']);

        return view('backend.footerMenuConfig.index', $data);
    }

    public function update(Request $request, Config $footerMenuConfig)
    {


        $input = $request->all();

        $input['content'] = json_encode($input['content'] );

        $footerMenuConfig->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
