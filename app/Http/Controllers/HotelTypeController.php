<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\HotelType;
use App\Http\Requests\HotelTypeRequest;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class HotelTypeController extends BaseController
{
    protected function getModel(): String
    {
       return 'hotelType';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = HotelType::latest('id')->get(['id','name','order_menu']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })
                ->addColumn('order_menu', function ($row) {
                    return $row->order_menu;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);
                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'order_menu', 'name' => 'order_menu', 'title' => TableEnum::LABELS['order_menu']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.hotelType.index', $data);
    }


    public function create()
    {
        return view('backend.hotelType.create-edit');
    }

    public function store(HotelTypeRequest $request)
    {
        HotelType::create($request->all());
        return back()->with(CommonEnum::MESSAGES['success']);
    }


    public function edit(HotelType $hotelType)
    {
        $data = [
            'configs' => ['action'=>'update'],
            'hotelType' => $hotelType,
        ];
        return view('backend.hotelType.create-edit', $data);
    }


    public function update(HotelTypeRequest $request, HotelType $hotelType)
    {
        $hotelType->update($request->all());
        return back()->with(CommonEnum::MESSAGES['success']);
    }

}
