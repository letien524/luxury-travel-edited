<?php

namespace App\Http\Controllers;


use App\Category;
use App\Config;
use App\Country;
use App\HotelType;
use App\Jobs\SendMailCommentToAdminJob;
use App\Jobs\SendMailContactToAdminJob;
use App\Jobs\SendMailContactToCustomerJob;
use App\Jobs\SendMailHasTourPlanJob;
use App\Jobs\SendMailToAdminHasNewSubcribeJob;
use App\Member;
use App\Post;
use App\Tour;
use App\Contact;
use App\TourActivity;
use App\TourDestination;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;

class WelcomeController extends Controller
{
    protected $paginationNumber = 9;
    protected $paginationNumberBlog = 10;

    public function tourPlanDownload(Request $request, $slug)
    {
        $this->validate($request, [
            'customer.email' => 'required|email',
            'customer.name' => 'required',
        ]);

        $input = $request->all();
        $input['type'] = 'customer-mail-collection-tour-plan';
        $input['name'] = @$input['customer']['name'];
        $input['email'] = @$input['customer']['email'];

        $contact = Contact::create($input);

        $tour = Tour::where('slug', $slug)->first();

        $idHotelCities = $tour->hotelCities->count() ? $tour->hotelCities->pluck('id')->all() : [];
        $hotels = $tour->hotels()->whereIn('city_id', $idHotelCities)->get();
        $idHotels = $hotels->pluck('id')->all();
        $idHotelTypes = $hotels->pluck('type_id')->unique()->all();

        $hotelTypes = HotelType::with(['hotels' => function ($query) use($idHotels) {
            $query->with('city:id,name')->rightJoin('hotel_cities', 'hotels.city_id', '=', 'hotel_cities.id')
                ->whereIn('hotels.id', $idHotels)
                ->select('hotels.*')
                ->orderBy('hotel_cities.name', 'asc');
        }])->whereIn('id', $idHotelTypes)
            ->latest('order_menu')->get();

        $data = [
            'tour' => $tour,
            'hotelTypes' => $hotelTypes,
        ];

        Storage::makeDirectory('tour-plans');

        PDF::loadView('pdf.tour-plan', $data)->save(storage_path("app/tour-plans/{$tour->slug}.pdf"));

        $this->dispatch(new SendMailHasTourPlanJob($tour, $contact));

//        return view('pdf.tour-plan', $data);

        return back()->with([
            'flash_level' => 'success',
            'flash_message' => __('The itinerary has been sent to you. Please check your email inbox!'),
        ]);
    }

    public function customerMailCollectionBottom(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return back()->with([
                'popup' => 'show',
                'message_name' => 'Something is not correct',
                'message_content' => $validator->errors()->first('email')
            ]);
        }

        $input = $request->all();

        $input['type'] = 'customer-mail-collection-bottom';

        $contact = Contact::create($input);

        $this->dispatch(new SendMailToAdminHasNewSubcribeJob($contact));

        if ($request->ajax()) {
            $data = [
                'name' => __('Thank you'),
                'message' => __('Thank you for signing up. You will be the first one receiving the latest news and special offers from us.')
            ];
            return response($data);
        }

        return back()->with([
            'popup' => 'show',
            'message_name' => 'Thank you',
            'message_content' => __('Thank you for signing up. You will be the first one receiving the latest news and special offers from us.')
        ]);
    }

    public function customerMailCollection(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return back()->with([
                'popup' => 'customerMailCollection',
                'popup_status' => 1,
                'message_name' => 'Something is not correct',
                'message_content' => $validator->errors()->first('email')
            ]);
        }

        $input = $request->all();

        $input['type'] = 'customer-mail-collection';

        $contact = Contact::create($input);

        $this->dispatch(new SendMailToAdminHasNewSubcribeJob($contact));

        return back()->with([
            'popup' => 'customerMailCollection',
            'popup_status' => 0,
            'message_name' => 'Thank you',
            'message_content' => __('Thank you for signing up. You will be the first one receiving the latest news and special offers from us.')
        ]);
    }


    public function contact()
    {

        $address = Contact::where('type', 'address')->orderBy('order_menu', 'desc')->get();
        $configs = Category::where('type', 'contact-category-main')->first();
        if (empty($configs)) {
            return abort(404, 'Bạn chưa cấu hình trang liên hệ. [Menu -> Liên hệ -> Cấu hình]');
        }

        $configs->meta_title = json_decode($configs->meta_title);
        $configs->meta_description = json_decode($configs->meta_description);
        $configs->content = json_decode($configs->content);

        $metaConfig = [
            'meta_title' => strlen($configs->meta_title) ? $configs->meta_title : __('Contact'),
            'meta_description' => @$configs->meta_description,
            'meta_image' => @$configs->content->image,
            'index' => true,
        ];

        $captcha = captcha_src('flat');

        return view('frontend.pages.contact', compact('captcha', 'address', 'configs', 'metaConfig'));
    }

    public function postContact(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'email' => 'required|email',
                'content.subject' => 'required',
                'captcha' => 'required|captcha',
            ]
        );

        if(!empty($request->input('c.captcha'))){
            return redirect()->route('contact.finish')->with([
                'contact_submit' => true,
                'inquireFinish' => true
            ]);
        }

        $input = $request->all();
        $input['content'] = json_encode($input['content']);

        if (!empty($request->input('c.captcha'))) {
            $input['status'] = -1;
        }

        $contact = Contact::create($input);

        if (in_array($contact->type, ['customer-contact', 'customer-subscribe'])) {

            if($contact->type == 'customer-contact'){
                SendMailContactToCustomerJob::dispatch($contact)->onQueue('emails');
            }

            SendMailContactToAdminJob::dispatch($contact)->onQueue('emails');
        }


        return redirect()->route('contact.finish')->with([
            'contact_submit' => true,
            'inquireFinish' => true
        ]);
    }

    public function postSubscribe(Request $request){
        $this->validate($request,
            [
                'email' => 'required|email'
            ]
        );

        $contact = Contact::create($request->all());

        if (in_array($contact->type, ['customer-subscribe'])) {
            $this->dispatch(new SendMailToAdminHasNewSubcribeJob($contact));

        }

        return back()->with([
            'flash_level' => 'success',
            'flash_message' => __('Thank you'),
        ]);
    }

    public function home()
    {
        $home = Post::where('type', 'home')->first();
        if (empty($home)) {
            return abort(404, 'Bạn chưa cấu hình trang chủ. [Menu -> Trang chủ]');
        }

        $configGeneral = Config::where('type', 'general')->first();
        if (empty($configGeneral)) {
            return abort(404, 'Bạn chưa cấu hình trang. [Menu -> Cấu hình -> Cấu hình chung]');
        }


        $howItWorks = Post::where('type', 'how-it-work')->orderBy('order_menu', 'desc')->get();

        $tourHighlights = Tour::where([
            ['status', 1],
            ['highlight', 1],
        ])->orderBy('id', 'desc')->get();


        $metaConfig = [
            'meta_title' => !empty(@$home->meta_title) ? @$home->meta_title : $home->name,
            'meta_description' => @$home->meta_description,
            'meta_image' => @$home->image,
            'index' => true,
        ];


        /*$tourStyles = Tour::where('status', 1)
            ->orderBy('id', 'desc')
            ->take($this->paginationNumber)->get();*/

        $tourStyles = TourActivity::where('status', 1)->latest()->get();

        $blogs = Post::where([
            ['status', 1],
            ['type', 'blog'],
        ])->orderBy('id', 'desc')->take($this->paginationNumberBlog)->get();
        return view('frontend.pages.home', compact('home', 'metaConfig', 'configGeneral', 'howItWorks', 'tourHighlights', 'tourStyles', 'blogs'));

    }

    public function blogDetail($slug)
    {

        $blog = Post::with('comments')->where([
            ['slug', $slug],
        ])->first();


        if (empty($blog)) {
            return abort(404);
        }
        $config = Category::where('type', 'blog-category-main')->first();

        if (empty($config)) {
            return abort(404, 'Bạn chưa cấu hình trang activities. [Menu -> Tin tức -> Cấu hình]');
        }

        $config->content = json_decode($config->content);
        $config->meta_title = json_decode($config->meta_title);

        $categories = Category::where('type', 'blog-category')
            ->orderBy('id', 'desc')
            ->get();

        $trendingBlogs = Post::where([
            ['type', 'blog'],
            ['status', 1],
        ])->inRandomOrder()->take(4)->get();

        $relatedBlogs = Post::where('id', '<>', $blog->id)
            ->whereHas('tag', function ($q) use ($blog) {
                return $q->whereIn('slug', $blog->tag->pluck('slug'));
            })
            ->orWhereHas('category', function ($q) use ($blog) {
                return $q->whereIn('slug', $blog->category->pluck('slug'));
            })->inRandomOrder()->take(4)->get();

        $image = json_decode($blog->image);

        $metaConfig = [
            'meta_title' => strlen($blog->meta_title) ? $blog->meta_title : $blog->name,
            'meta_description' => $blog->meta_description,
            'meta_image' => @$image->thumbnail,
            'index' => true,
        ];


        $comments = $blog->comments()->where('status', 1)->orderBy('id', 'desc')->get();

        $captcha = captcha_src('flat');

        return view('frontend.pages.blog_details', compact('captcha', 'blog', 'metaConfig', 'categories', 'trendingBlogs', 'relatedBlogs', 'config', 'comments'));

    }

    public function blogPage($slug)
    {

        $blog = Post::with('comments')->where([
            ['slug', $slug],
        ])->first();


        if (empty($blog)) {
            return abort(404);
        }


        $config = Category::where('type', 'blog-category-main')->first();

        if (empty($config)) {
            return abort(404, 'Bạn chưa cấu hình trang activities. [Menu -> Tin tức -> Cấu hình]');
        }

        $config->content = json_decode($config->content);
        $config->meta_title = json_decode($config->meta_title);

        $categories = Category::where('type', 'blog-category')
            ->orderBy('id', 'desc')
            ->get();

        $trendingBlogs = Post::where([
            ['type', 'page'],
            ['status', 1],
        ])->inRandomOrder()->take(4)->get();


        $image = json_decode($blog->image);

        $metaConfig = [
            'meta_title' => strlen($blog->meta_title) ? $blog->meta_title : $blog->name,
            'meta_description' => $blog->meta_description,
            'meta_image' => @$image->thumbnail,
            'page' => true,
            'index' => true,
        ];


        $comments = $blog->comments()->where('status', 1)->orderBy('id', 'desc')->get();

        $captcha = captcha_src('flat');

        return view('frontend.pages.blog_details', compact('captcha', 'blog', 'metaConfig', 'categories', 'trendingBlogs', 'config', 'comments'));

    }

    public function blog()
    {
        $config = Category::where('type', 'blog-category-main')->first();
        if (empty($config)) {
            return abort(404, 'Bạn chưa cấu hình trang Tin tức. [Menu -> Tin tức -> Cấu hình]');
        }
        $config->content = json_decode($config->content);
        $config->meta_title = json_decode($config->meta_title);
        $config->meta_content = json_decode($config->meta_content);

        $blogs = Post::where([
            ['type', 'blog'],
            ['status', 1],
        ])->orderBy('id', 'desc')
            ->paginate($this->paginationNumberBlog);

        $newestBlogs = Post::where([
            ['type', 'blog'],
            ['status', 1]
        ])->orderBy('id', 'desc')->take($this->paginationNumber)->get();

        $categories = Category::where('type', 'blog-category')
            ->orderBy('id', 'desc')
            ->get();

        $trendingBlogs = Post::where([
            ['type', 'blog'],
            ['status', 1],
        ])->inRandomOrder()
            ->take(4)
            ->get();

        $metaConfig = [
            'meta_title' => @$config->meta_title,
            'meta_description' => @$config->meta_description,
            'meta_image' => @$config->image,
            'index' => true,
        ];

        return view('frontend.pages.blog', compact('blogs', 'newestBlogs', 'metaConfig', 'categories', 'trendingBlogs', 'config'));


    }

    public function blogComment(Request $request, Post $blog)
    {

        $this->validate($request,
            [
                'content' => 'required',
                'email' => 'required|email',
                'name' => 'required',
                'captcha' => 'required|captcha',
            ]
        );

        $comment = $blog->comments()->create($request->except('status'));

        $this->dispatch(new SendMailCommentToAdminJob($comment));

        return redirect()->back()->with([
            'flash_level' => 'success',
            'flash_message' => __('Your comment has been sent!'),
        ]);

    }

    public function blogTag($slug)
    {
        $tag = Category::where([
            ['type', 'blog-tag'],
            ['slug', $slug],
        ])->first();

        if (empty($tag)) {
            return abort(404);
        }
        $newestBlogs = Post::where([
            ['type', 'blog'],
            ['status', 1]
        ])->orderBy('id', 'desc')->limit(10)->get();


        $blogs = $tag
            ->blogTag()
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->paginate($this->paginationNumberBlog);

        $categories = Category::where('type', 'blog-category')
            ->orderBy('id', 'desc')
            ->get();

        $trendingBlogs = Post::where([
            ['type', 'blog'],
            ['status', 1],
        ])->inRandomOrder()
            ->take(4)
            ->get();

        $config = Category::where('type', 'blog-category-main')->first();
        $config->content = json_decode($config->content);
        $config->meta_title = json_decode($config->meta_title);

        $metaConfig = [
            'meta_title' => @$tag->meta_title,
            'meta_description' => @$tag->meta_description,
            'meta_image' => @$tag->image,
            'index' => true,
        ];

        return view('frontend.pages.blog', compact('blogs', 'newestBlogs', 'metaConfig', 'config', 'categories', 'trendingBlogs'));

    }

    public function blogCategory($slug)
    {

        $category = Category::where([
            ['type', 'blog-category'],
            ['slug', $slug],
        ])->first();


        if (empty($category)) {
            return abort(404);
        }

        $newestBlogs = Post::where([
            ['type', 'blog'],
            ['status', 1]
        ])->orderBy('id', 'desc')->limit(10)->get();

        $blogs = $category
            ->blogCategory()
            ->where('status', 1)
            ->orderBy('id', 'desc')
            ->paginate($this->paginationNumberBlog);

        $categories = Category::where('type', 'blog-category')
            ->orderBy('id', 'desc')
            ->get();

        $trendingBlogs = Post::where([
            ['type', 'blog'],
            ['status', 1],
        ])->inRandomOrder()
            ->take(4)
            ->get();

        $config = Category::where('type', 'blog-category-main')->first();
        $config->content = json_decode($config->content);
        $config->meta_title = json_decode($config->meta_title);

        $metaConfig = [
            'meta_title' => strlen($category->meta_title) ? $category->meta_title : $category->name,
            'meta_description' => @$category->meta_description,
            'meta_image' => @$category->image,
            'index' => true,
        ];

        return view('frontend.pages.blog', compact('blogs', 'newestBlogs', 'metaConfig', 'config', 'categories', 'trendingBlogs'));


    }

    public function about()
    {

        $about = Category::where('type', 'about')->first();

        if (empty($about)) {
            return abort(404, 'Bạn chưa cấu hình trang giới thiệu. [Menu -> Giới thiệu]');
        }

        $metaConfig = [
            'meta_title' => strlen($about->meta_title) ? $about->meta_title : $about->name,
            'meta_description' => @$about->meta_description,
            'meta_image' => @$about->image,
            'index' => @$about->index,
        ];

        $members = Member::where('status', 1)
            ->orderBy('order_menu', 'desc')
            ->get();
        return view('frontend.pages.about', compact('about', 'metaConfig', 'members'));
    }

    public function activity(Request $request)
    {

        $category = Category::where('type', 'activity-contact-main')->first();
        $tourSearchConfig = json_decode($category->content);
        $tourSearchConfig = @$tourSearchConfig->tourSearch;

        if (empty($category)) {
            return abort(404, 'Chưa cấu hình activity. [Menu -> Tours -> Cấu hình chung]');
        }


        $configGeneral = Config::where('type', 'general')->first();
        if (empty($configGeneral)) {
            return abort(404, 'Chưa cấu hình Trang. [Menu -> Cấu hình -> Cấu hình chung]');
        }

        $configSocial = Config::where('type', 'social')->first();
        if (empty($configSocial)) {
            return abort(404, 'Chưa cấu hình Mạng xã hội. [Menu -> Cấu hình -> Mạng xã hội]');
        }

        $tours = Tour::where('status', 1)
            ->filter($request)->latest('updated_at')
            ->paginate($this->paginationNumber);

        if ($request->ajax()) {
            if ($tours->count() === 0) {
                return response()->json([], 404);
            }
            $view = view('frontend.components.tour_ajax', compact('tours'))->render();
            return response()->json(['html' => $view]);
        }

        $metaConfig = [
            'meta_title' => strlen($category->meta_title) ? $category->meta_title : $category->name,
            'meta_description' => @$category->meta_description,
            'meta_image' => @$category->image,
            'canonical' => @$category->canonical,
            'index' => @$category->index,
        ];


        $tourDestinations = Config::firstOrCreate(['type'=>'tour-filter']);
        $tourDestinations = json_decode(@$tourDestinations->content, true);
        $tourIdDestinations = (array) @$tourDestinations['destinations'];
        $destinations = TourDestination::whereIn('id', $tourIdDestinations)->orderBy('name')->get(['name','slug']);

        $activities = TourActivity::where('status', 1)->orderBy('name')->get();

        $headerStyle = 'vk-header--style-1';

        return view('frontend.pages.activity', compact('tourSearchConfig','tours', 'metaConfig', 'category', 'destinations', 'activities', 'headerStyle', 'configSocial', 'configGeneral'));


    }

    public function activityCategory(Request $request, $slug)
    {

        $category = TourActivity::where('slug', $slug)->first();
        $categoryActivityConfig = Category::where('type', 'activity-contact-main')->first();
        $tourSearchConfig = json_decode($categoryActivityConfig->content);
        $tourSearchConfig = @$tourSearchConfig->tourSearch;

        if (empty($category)) {
            return abort(404, 'Chưa cấu hình activity. "Tours -> Cấu hình chung"');
        }

        $reviews = $category->review ? $category->review->reviews()->where('display', 1)->latest('submited_at')->paginate(5) : [];

        if ($request->ajax() && $request->{"type"} == 'reviews') {
            return view('frontend.components.review_ajax', compact('reviews'));
        }


        $configGeneral = Config::where('type', 'general')->first();
        if (empty($configGeneral)) {
            return abort(404, 'Chưa cấu hình Trang. [Menu -> Cấu hình -> Cấu hình chung]');
        }

        $configSocial = Config::where('type', 'social')->first();
        if (empty($configSocial)) {
            return abort(404, 'Chưa cấu hình Mạng xã hội. [Menu -> Cấu hình -> Mạng xã hội]');
        }

        $tours = $category->tours()->where('status', 1)
            ->filter($request)->latest('updated_at')
            ->paginate($this->paginationNumber);

        if ($request->ajax()) {
            if ($tours->count() === 0) {
                return response()->json([], 404);
            }
            $view = view('frontend.components.tour_ajax', compact('tours','tourSearchConfig'))->render();
            return response()->json(['html' => $view]);
        }

        $metaConfig = [
            'meta_title' => strlen($category->meta_title) ? $category->meta_title : $category->name,
            'meta_description' => @$category->meta_description,
            'meta_image' => @$category->image,
            'canonical' => @$category->canonical,
            'index' => @$category->index,
        ];

        $tourDestinations = Config::firstOrCreate(['type'=>'tour-filter']);
        $tourDestinations = json_decode(@$tourDestinations->content, true);
        $tourIdDestinations = (array) @$tourDestinations['destinations'];
        $destinations = TourDestination::whereIn('id', $tourIdDestinations)->orderBy('name')->get(['name','slug']);

        $activities = TourActivity::where('status', 1)->orderBy('name')->get();

        $headerStyle = 'vk-header--style-1';

        $bestDestinations = TourDestination::where('is_best', 1)->latest('updated_at')->get();

        $data = [
            'reviews' => $reviews,
            'bestDestinations' => $bestDestinations,
            'tours' => $tours,
            'metaConfig' => $metaConfig,
            'category' => $category,
            'destinations' => $destinations,
            'activities' => $activities,
            'headerStyle' => $headerStyle,
            'configGeneral' => $configGeneral,
            'configSocial' => $configSocial,
            'tourSearchConfig' => $tourSearchConfig,

        ];

        return view('frontend.pages.activity', $data);

    }

    public function destination(Request $request, $slug)
    {
        $destination = TourDestination::where('slug', $slug)->first();

        $categoryActivityConfig = Category::where('type', 'activity-contact-main')->first();
        $tourSearchConfig = json_decode($categoryActivityConfig->content);
        $tourSearchConfig = @$tourSearchConfig->tourSearch;

        if (empty($destination)) {
            return abort(404);
        }

        //is city
        if ($destination->hasParent()) {
            return abort(404);
        }

        $reviews = $destination->review ? $destination->review->reviews()->where('display', 1)->latest('submited_at')->paginate(5) : [];

        if ($request->ajax() && $request->{"type"} == 'reviews') {
            return view('frontend.components.review_ajax', compact('reviews'));
        }

        $configGeneral = Config::where('type', 'general')->first();
        if (empty($configGeneral)) {
            return abort(404, 'Chưa cấu hình Trang. [Menu -> Cấu hình -> Cấu hình chung]');
        }

        $configSocial = Config::where('type', 'social')->first();
        if (empty($configSocial)) {
            return abort(404, 'Chưa cấu hình Mạng xã hội. [Menu -> Cấu hình -> Mạng xã hội]');
        }

        $metaConfig = [
            'meta_title' => strlen($destination->meta_title) ? $destination->meta_title : $destination->name,
            'meta_description' => @$destination->meta_description,
            'meta_image' => @$destination->image,
            'canonical' => @$destination->canonical,
            'index' => @$destination->index,
        ];

        $cities = $destination->getChild()->get()->pluck('id')->toArray();

        $destinationIds = array_merge([$destination->id], $cities);


        $tours = Tour::whereHas('destinations', function ($query) use ($destinationIds) {
            return $query->whereIn('destination_id', $destinationIds);
        })->where('status', 1)->latest('updated_at')->paginate($this->paginationNumber);

        if ($request->ajax()) {
            if ($tours->count() === 0) {
                return response()->json([], 404);
            }
            $view = view('frontend.components.tour_ajax', compact('tours','tourSearchConfig'))->render();
            return response()->json(['html' => $view]);
        }

        $headerStyle = 'vk-header--style-1';

        $activities = TourActivity::where('status', 1)->orderBy('name', 'asc')->get();


        $bestDestinations = TourDestination::where([
            ['is_best', 1],
            ['parent_id', $destination->id],
        ])->latest('order_menu')->get();

        return view('frontend.pages.destination', compact('tourSearchConfig','reviews', 'bestDestinations', 'tours', 'metaConfig', 'destination', 'headerStyle', 'activities', 'configGeneral', 'configSocial'));

    }

    public function destinationCity(Request $request, $country, $city)
    {
        //city
        $destination = TourDestination::where('slug', $city)->first();

        $categoryActivityConfig = Category::where('type', 'activity-contact-main')->first();
        $tourSearchConfig = json_decode($categoryActivityConfig->content);
        $tourSearchConfig = @$tourSearchConfig->tourSearch;

        if (empty($destination)) {
            return abort(404);
        }

        //country
        $country = TourDestination::where('slug', $country)->first();

        if (empty($country)) {
            return abort(404);
        }

        //city in country
        if ($country->id != $destination->parent_id) {
            return abort(404);
        }

        $reviews = $destination->review ? $destination->review->reviews()->where('display', 1)->latest('submited_at')->paginate(5) : [];

        if ($request->ajax() && $request->{"type"} == 'reviews') {
            return view('frontend.components.review_ajax', compact('reviews'));
        }

        $configGeneral = Config::where('type', 'general')->first();
        if (empty($configGeneral)) {
            return abort(404, 'Chưa cấu hình Trang. [Menu -> Cấu hình -> Cấu hình chung]');
        }

        $configSocial = Config::where('type', 'social')->first();
        if (empty($configSocial)) {
            return abort(404, 'Chưa cấu hình Mạng xã hội. [Menu -> Cấu hình -> Mạng xã hội]');
        }

        $metaConfig = [
            'meta_title' => strlen($destination->meta_title) ? $destination->meta_title : $destination->name,
            'meta_description' => @$destination->meta_description,
            'meta_image' => @$destination->image,
            'canonical' => @$destination->canonical,
            'index' => @$destination->index,
        ];

        $tours = $destination
            ->tours()
            ->where('status', 1)
            ->latest('updated_at')
            ->paginate($this->paginationNumber);

        if ($request->ajax()) {
            if ($tours->count() === 0) {
                return response()->json([], 404);
            }
            $view = view('frontend.components.tour_ajax', compact('tours','tourSearchConfig'))->render();
            return response()->json(['html' => $view]);
        }

        $headerStyle = 'vk-header--style-1';

        $activities = TourActivity::where('status', 1)->orderBy('name', 'asc')->get();


        $bestDestinations = TourDestination::where([
            ['id', '<>', $destination->id],
            ['is_best', 1],
            ['parent_id', $destination->parent_id],
        ])->latest('order_menu')->get();

        $data = [
            'reviews' => $reviews,
            'bestDestinations' => $bestDestinations,
            'tours' => $tours,
            'metaConfig' => $metaConfig,
            'destination' => $destination,
            'headerStyle' => $headerStyle,
            'activities' => $activities,
            'configGeneral' => $configGeneral,
            'configSocial' => $configSocial,
            'tourSearchConfig' => $tourSearchConfig,
        ];

        return view('frontend.pages.destination', $data);

    }

    public function tourDetail(Request $request, $slug)
    {
        $tour = Tour::with(['activities', 'destinations'])->where([
            ['slug', $slug],
            /*['status', 1],*/
        ])->first();

        if (empty($tour)) {
            return abort(404);
        }

        $reviews = count($tour->reviews) ? $tour->reviews->first()->reviews()->where('display', 1)->latest('submited_at')->paginate(5) : [];


        if ($request->ajax()) {
            return view('frontend.components.review_ajax', compact('reviews'));
        }


        $inquire = Category::where('type', 'inquire-category-main')->first();
        if (empty($inquire)) {
            return abort(404, 'Bạn chưa cấu hình Inquired page. [Menu -> Inquired page]');
        }

        $configGeneral = Config::where('type', 'general')->first();

        if (empty($configGeneral)) {
            return abort(404, 'Bạn chưa cấu hình trang. [Menu -> Cấu hình -> Cấu hình chung]');
        }

        $inquire->content = json_decode($inquire->content);

        $howItWorks = Post::where('type', 'how-it-work')->orderBy('order_menu', 'desc')->get();

        $activityIds = $tour->activities->pluck('id')->toArray();
        $destinationIds = $tour->destinations()->pluck('id')->toArray();

        $relatedTours = Tour::where(function ($query) use ($activityIds, $destinationIds) {
            $query->whereHas('activities', function ($query) use ($activityIds) {
                return $query->whereIn('category_id', $activityIds);
            })->orWhereHas('destinations', function ($query) use ($destinationIds) {
                return $query->whereIn('id', $destinationIds);
            });
        })->where('status', 1)->inRandomOrder()->take(12)->get();

        $tourDestinations = $tour->destinations->implode('name', ', ');

        $idHotelCities = $tour->hotelCities->count() ? $tour->hotelCities->pluck('id')->all() : [];
        $hotels = $tour->hotels()->whereIn('city_id', $idHotelCities)->get();
        $idHotels = $hotels->pluck('id')->all();
        $idHotelTypes = $hotels->pluck('type_id')->unique()->all();

        $hotelTypes = HotelType::with(['hotels' => function ($query) use($idHotels) {
            $query->with('city:id,name')->rightJoin('hotel_cities', 'hotels.city_id', '=', 'hotel_cities.id')
                ->whereIn('hotels.id', $idHotels)
                ->select('hotels.*')
                ->orderBy('hotel_cities.name', 'asc');
        }])->whereIn('id', $idHotelTypes)
            ->latest('order_menu')->get();

        $metaConfig = [
            'meta_title' => strlen($tour->meta_title) ? $tour->meta_title : $tour->name,
            'meta_description' => @$tour->meta_description,
            'meta_image' => @$tour->image,
            'canonical' => @$tour->canonical,
            'index' => @$tour->index,
        ];

        $headerStyle = 'vk-header--style-1';
        $captcha = captcha_src('flat');

        $data = [
            'hotelTypes' => $hotelTypes,
            'reviews' => $reviews,
            'captcha' => $captcha,
            'tourDestinations' => $tourDestinations,
            'tour' => $tour,
            'howItWorks' => $howItWorks,
            'relatedTours' => $relatedTours,
            'metaConfig' => $metaConfig,
            'inquire' => $inquire,
            'configGeneral' => $configGeneral,
            'headerStyle' => $headerStyle,

        ];
        return view('frontend.pages.tour_details', $data);

    }

    public function tourBooking($slug)
    {
        $tour = Tour::with(['activities', 'destinations'])->where('slug', $slug)->first();

        if (empty($tour)) {
            return abort(404);
        }


        $configGeneral = Config::where('type', 'general')->first();

        if (empty($configGeneral)) {
            return abort(404, 'Bạn chưa cấu hình trang. [Menu -> Cấu hình -> Cấu hình chung]');
        }


        $howItWorks = Post::where('type', 'how-it-work')->orderBy('order_menu', 'desc')->get();

        $activityIds = $tour->activities->pluck('id')->toArray();
        $destinationIds = $tour->destinations()->pluck('id')->toArray();


        $relatedTours = Tour::whereHas('activities', function ($query) use ($activityIds) {
            return $query->whereIn('category_id', $activityIds);
        })->orWhereHas('destinations', function ($query) use ($destinationIds) {
            return $query->whereIn('id', $destinationIds);
        })->inRandomOrder()->take(12)->get();


        $relatedTours = $relatedTours->filter(function ($value) use ($tour) {
            return $value->id !== $tour->id;
        });

        $tourDestinations = $tour->destinations->implode('name', ', ');


        $metaConfig = [
            'meta_title' => strlen($tour->meta_title) ? $tour->meta_title : $tour->name,
            'meta_description' => @$tour->meta_description,
            'meta_image' => @$tour->image,
            'index' => false,

        ];

        $headerStyle = 'vk-header--style-1';
        $captcha = captcha_src('flat');

        return view('frontend.pages.tour_booking', compact('captcha', 'tourDestinations', 'tour', 'howItWorks', 'relatedTours', 'metaConfig', 'configGeneral', 'headerStyle'));

    }

    public function inquire()
    {
        $inquire = Category::where('type', 'inquire-category-main')->first();
        if (empty($inquire)) {
            return abort(404, 'Bạn chưa cấu hình Inquired page. [Menu -> Inquired page]');
        }


        $metaConfig = [
            'meta_title' => strlen($inquire->meta_title) ? $inquire->meta_title : __('Inquire'),
            'meta_description' => @$inquire->meta_description,
            'meta_image' => @$inquire->image,
        ];


        $destinations = TourDestination::where('is_inquire', 1)->orderby('order_menu', 'desc')->get();

        $countries = Country::orderBy('name', 'asc')->get();

        $captcha = captcha_src('flat');


        return view('frontend.pages.inquire', compact('metaConfig', 'inquire', 'destinations', 'countries', 'captcha'));

    }

    public function getCaptcha()
    {
        return captcha_src('flat');
    }

    public function inquireFinish()
    {

        if (session('inquireFinish')) {

            if (session('contact_submit')) {
                $data = [
                    'name' => __('Thank you'),
                    'content' => __('Thank you for contacting Luxury Travel. We will reply to you as soon as possible.'),
                ];
            } else {
                $data = [
                    'name' => __('Finish booking'),
                    'content' => __('Thank you so much for contacting Luxury Travel. We are so excited to begin <br> the process of creating the perfect itinerary for you.'),
                ];
            }


            return view('frontend.pages.inquire_finish', $data);
        }
        return redirect()->route('home');
    }

    public function tourSearch(Request $request)
    {
        $category = Category::where('type', 'activity-contact-main')->first();
        $tourSearchConfig = json_decode($category->content);
        $tourSearchConfig = @$tourSearchConfig->tourSearch;

        if (empty($category)) {
            return abort(404, 'Chưa cấu hình activity. [Menu -> Tours -> Cấu hình chung]');
        }


        $tours = Tour::where('status', 1)
            ->filter($request)->latest('updated_at')
            ->paginate($this->paginationNumber);


        if ($request->ajax()) {
            if ($tours->count() === 0) {
                return response()->json([], 404);
            }
            $view = view('frontend.components.tour_ajax', compact('tours'))->render();
            return response()->json(['html' => $view]);
        }

        $metaConfig = [
            'meta_title' => strlen($category->meta_title) ? $category->meta_title : __('Search'),
            'meta_description' => @$category->meta_description,
            'meta_image' => @$category->image,
            'index' => false,
            'search' => true,
        ];


        $tourDestinations = Config::firstOrCreate(['type'=>'tour-filter']);
        $tourDestinations = json_decode(@$tourDestinations->content, true);
        $tourIdDestinations = (array) @$tourDestinations['destinations'];
        $destinations = TourDestination::whereIn('id', $tourIdDestinations)->orderBy('name')->get(['name','slug']);

        $activities = TourActivity::where('status', 1)->orderBy('name', 'asc')->get();

        $destinationTargets = $request->get('destination');
        $destinationTargets = explode(',', $destinationTargets);
        $destinationTargets = TourDestination::whereIn('slug', $destinationTargets)->get()->pluck('name')->toArray();

        $activityTargets = $request->get('activity');
        $activityTargets = explode(',', $activityTargets);
        $activityTargets = TourActivity::whereIn('slug', $activityTargets)->get()->pluck('name')->toArray();

        $headerStyle = 'vk-header--style-1';

        $bestDestinations = TourDestination::where('is_best', 1)->latest('updated_at')->get();

        return view('frontend.pages.activity', compact('tourSearchConfig', 'bestDestinations', 'tours', 'metaConfig', 'category', 'destinations', 'activities', 'destinationTargets', 'activityTargets', 'headerStyle'));
    }

    public function getMember($slug)
    {
        $member = Member::with(['tours', 'faqs', 'reviews'])->where([
            ['slug', $slug],
        ])->first();

        $pageSettings = Category::where('type', 'member-category-main')->first('content');

        if (empty($member)) {
            return abort(404);
        }

        $metaConfig = [
            'meta_title' => strlen($member->meta_title) ? $member->meta_title : $member->name,
            'meta_description' => $member->meta_description,
            'meta_image' => $member->image
        ];

        $captcha = captcha_src('flat');

        $data = [
            'pageSettings' => $pageSettings,
            'member' => $member,
            'metaConfig' => $metaConfig,
            'captcha' => $captcha,
        ];

        return view('frontend.pages.member-details', $data);

    }
}
