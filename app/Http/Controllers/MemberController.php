<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Faq;
use App\Http\Requests\MemberRequest;
use App\Member;
use App\Review;
use App\Tour;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class MemberController extends BaseController
{
    protected function getModel(): String
    {
       return 'member';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $members = Member::latest('id')->get(['id', 'name', 'image','position','order_menu','status']);
            $datatables = Datatables::of($members)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('position', function ($row) {
                    return $row->position;
                })
                ->addColumn('order_menu', function ($row) {
                    return $row->order_menu;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusTourTable($row->status, 'status');
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'status'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'position', 'name' => 'position', 'title' => TableEnum::LABELS['position']],
            ['data' => 'order_menu', 'name' => 'order_menu', 'title' => TableEnum::LABELS['order_menu']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.member.index', $data);
    }

    public function create()
    {
        $data = [
            'reviews' => Review::orderBy('name', 'asc')->get(['id', 'name']),
            'faqs' => Faq::orderBy('name', 'asc')->get(['id', 'name']),
            'tours' => Tour::orderBy('name', 'asc')->get(['id', 'name']),
        ];

        return view('backend.member.create-edit', $data);
    }


    public function store(MemberRequest $request)
    {
        $input = $request->all();

        $member = Member::create($input);

        $member->faqs()->sync(cleanArray(@$input['faqs']));
        $member->reviews()->sync(cleanArray(@$input['reviews']));
        $member->tours()->sync(cleanArray(@$input['tours']));

        return redirect()->route('member.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Member $member)
    {
        $data = [
            'configs' => ['action' => 'update'],
            'member' => $member,
            'reviews' => Review::orderBy('name', 'asc')->get(['id', 'name']),
            'faqs' => Faq::orderBy('name', 'asc')->get(['id', 'name']),
            'tours' => Tour::orderBy('name', 'asc')->get(['id', 'name']),
        ];

        return view('backend.member.create-edit', $data);
    }


    public function update(MemberRequest $request, Member $member)
    {

        $input = $request->all();

        $input['status'] = isset($input['status']) ? $input['status'] : null;

        $member->update($input);

        $member->faqs()->sync(cleanArray(@$input['faqs']));
        $member->reviews()->sync(cleanArray(@$input['reviews']));
        $member->tours()->sync(cleanArray(@$input['tours']));

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
