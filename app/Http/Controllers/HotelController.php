<?php

namespace App\Http\Controllers;

use App\Enum\TableEnum;
use App\Hotel;
use App\Enum\CommonEnum;
use App\HotelCity;
use App\HotelRoomType;
use App\HotelType;
use App\Http\Requests\HotelRequest;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class HotelController extends BaseController
{
    protected function getModel(): String
    {
        return 'hotel';
    }

    public function index(Builder $builder)
    {

        if (request()->ajax()) {
            $data = Hotel::with('type','roomType','city')->latest('id')->get(['id','name','type_id','room_type_id','city_id']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })
                ->addColumn('type', function ($row) {
                    return @$row->type->name;
                })
                ->addColumn('roomType', function ($row) {
                    return @$row->roomType->name;
                })
                ->addColumn('city', function ($row) {
                    return @$row->city->name;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);
                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'type', 'name' => 'type', 'title' => TableEnum::LABELS['type']],
            ['data' => 'roomType', 'name' => 'roomType', 'title' => TableEnum::LABELS['roomType']],
            ['data' => 'city', 'name' => 'city', 'title' => TableEnum::LABELS['city']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.hotel.index', $data);
    }

    public function create()
    {
        $data = [
            'hotelTypes' => HotelType::orderBy('name','asc')->get(['id','name']),
            'hotelRoomTypes' => HotelRoomType::orderBy('name','asc')->get(['id','name']),
            'hotelCities' => HotelCity::orderBy('name','asc')->get(['id','name']),
        ];

        return view('backend.hotel.create-edit', $data);
    }

    public function store(HotelRequest $request)
    {
        $hotel = Hotel::create($request->all());
        return redirect()->route('hotel.edit', $hotel)->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Hotel $hotel)
    {
        $data = [
            'hotel' => $hotel,
            'configs' => ['action' => 'update'],
            'hotelTypes' => HotelType::orderBy('name','asc')->get(['id','name']),
            'hotelRoomTypes' => HotelRoomType::orderBy('name','asc')->get(['id','name']),
            'hotelCities' => HotelCity::orderBy('name','asc')->get(['id','name']),
        ];

        return view('backend.hotel.create-edit', $data);
    }

    public function update(HotelRequest $request, Hotel $hotel)
    {
        $hotel->update($request->all());

        return back()->with(CommonEnum::MESSAGES['success']);
    }

}
