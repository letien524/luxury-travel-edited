<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use Illuminate\Http\Request;

class TourConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'Category';
    }

    public function index()
    {
        $data['tourConfig'] = Category::firstOrCreate(['type'=>'activity-contact-main']);

        return view('backend.tourConfig.index', $data);
    }

    public function update(Request $request, Category $tourConfig)
    {
        $input = $request->all();

        $input['content'] = json_encode($input['content']);
        $input['index'] =  isset($input['index']) ?: null;

        $tourConfig->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);
    }

}
