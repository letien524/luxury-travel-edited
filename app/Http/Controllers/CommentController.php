<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Comment;
use App\Enum\TableEnum;
use App\Jobs\SendMailCommentToAdminJob;
use App\Post;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class CommentController extends BaseController
{
    protected function getModel(): String
    {
        return 'comment';
    }

    public function index(Builder $builder)
    {

        if (request()->ajax()) {
            $comments = Comment::with('blog')->latest('id')->get(['id', 'name', 'content', 'created_at','post_id', 'status']);
            $datatables = Datatables::of($comments)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlBlogLabelTable($row->blog, $editRoute);
                    return $name;
                })
                ->addColumn('content', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $content = getHtmlContentCommentTable($row, $editRoute);
                    return $content;
                })
                ->addColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusCommentTable($row->status);
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'status','content'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'content', 'name' => 'content', 'title' => TableEnum::LABELS['content']],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => TableEnum::LABELS['created_at']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.comment.index', $data);
    }

    public function store(Request $request, Post $blog)
    {

        $this->validate($request,
            [
                'content' => 'required',
                'email' => 'required|email',
                'name' => 'required'
            ]
        );

        $comment = $blog->comments()->create($request->except('status'));

        $this->dispatch(new SendMailCommentToAdminJob($comment));

        return back()->with([
            'flash_level' => 'success',
            'flash_message' => __('Your comment has been sent!'),
        ]);
    }

    public function edit(Comment $comment)
    {
        $data = [
            'comment' => $comment,
        ];
        return view('backend.comment.edit', $data);
    }

    public function update(Request $request, Comment $comment)
    {
        $comment->update($request->only(['status', 'content']));

        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
