<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Post;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class PageController extends BaseController
{
    private  $__model = 'page';

    protected function getModel(): String
    {
        return 'post';
    }

    public function index(Builder $builder)
    {
        $model = 'page';
        if (request()->ajax()) {
            $pages = Post::where('type', 'page')->latest('id')->get();
            $datatables = DataTables::of($pages)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $name = getHtmlBlogLabelTable($row, $editRoute);
                    return $name;
                })

                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusTourTable($row->status, 'status');
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $viewRoute = route("{$this->__model}.detail", $row->slug);
                    $editRoute = route("{$this->__model}.edit", $row);
                    $deleteRoute = route("{$this->__model}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute, $viewRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'status','content'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.page.index', $data);
    }

    public function create()
    {
        return view('backend.page.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]
        );

        $input = $request->all();

        $pageExistedName = Post::where([
            ['name', $input['name']],
            ['type', 'page'],
        ])->count();

        if ($pageExistedName) {
            return back()->withInput($request->input())->with(CommonEnum::MESSAGES['nameIsExisted']);
        }

        $pageExistedSlug = Post::where([
            ['slug', $input['slug']],
            ['type', 'page'],
        ])->count();

        if ($pageExistedSlug) {
            return back()->withInput($request->input())->with(CommonEnum::MESSAGES['slugIsExisted']);
        }


        $input['image'] = json_encode($input['image']);

        $page = Post::create($input);

        return redirect()->route('page.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Post $page)
    {
//            $tags = Category::where('type','blog-tag')->orderBy('name', 'asc')->get();
//            $categories = Category::where('type','blog-category')->orderBy('name', 'asc')->get();
        return view('backend.page.edit', compact('page'));
    }

    public function update(Request $request, Post $page)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]
        );
        $input = $request->all();

        if ($page->name != $input['name'] || $page->slug != $input['slug']) {

            $pageExistedName = Post::where([
                ['name', $input['name']],
                ['id', '<>', $page->id],
                ['type', 'page'],
            ])->count();

            if ($pageExistedName) {
                return back()->withInput($request->input())->with(CommonEnum::MESSAGES['nameIsExisted']);
            }

            $pageExistedSlug = Post::where([
                ['slug', $input['slug']],
                ['id', '<>', $page->id],
                ['type', 'page'],
            ])->count();

            if ($pageExistedSlug) {
                return back()->withInput($request->input())->with(CommonEnum::MESSAGES['slugIsExisted']);
            }

        }

        $input['status'] = isset($input['status']) ? $input['status'] : null;

        $input['image'] = json_encode($input['image']);

        $page->update($input);


        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
