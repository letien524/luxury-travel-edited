<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Review;
use App\ReviewCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class ReviewCategoryController extends BaseController
{
    protected function getModel(): String
    {
        return 'reviewCategory';
    }

    public function index(Builder $builder)
    {
        $data['data'] = ReviewCategory::latest()->get();

        if (request()->ajax()) {
            $data = ReviewCategory::latest('id')->get(['id', 'name']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlBlogLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name','action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.reviewCategory.index',$data);

    }

    public function create()
    {
         return view('backend.reviewCategory.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);

        $input = $request->all();

        $reviewCategory = ReviewCategory::create($input);

        return redirect()->route('reviewCategory.edit',$reviewCategory)->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(ReviewCategory $reviewCategory)
    {
        $data['data'] = $reviewCategory;
        return view('backend.reviewCategory.edit',$data);
    }

    public function update(Request $request, ReviewCategory $reviewCategory)
    {
        $this->validate($request,[
            'name'=>'required',
        ]);

        $input = $request->all();

        $reviewCategory->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
