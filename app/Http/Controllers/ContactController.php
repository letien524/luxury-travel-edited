<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Contact;
use App\Enum\TableEnum;
use App\Jobs\SendMailContactToAdminJob;
use App\Jobs\SendMailContactToCustomerJob;
use App\Mail\ContactSubmit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class ContactController extends BaseController
{
    protected function getModel(): String
    {
      return 'contact';
    }

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth')->except('store');
        $this->middleware('role');
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Contact::where('type', 'customer-contact')->latest('id')->get(['id','content','email', 'status','created_at']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $name = json_decode( $row->content, true);
                    $editRoute = route("{$this->getModel()}.edit", $row);

                    $name = getHtmlLabelContactTable(@$name['subject'], $editRoute);
                    return $name;
                })

                ->addColumn('email', function ($row) {
                    return $row->email;
                })
                ->addColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusContact($row->status);
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'status', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'email', 'name' => 'email', 'title' => TableEnum::LABELS['email']],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => TableEnum::LABELS['created_at']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.contact.index', $data);
    }

    public function store(Request $request)
    {

    }

    public function edit(Contact $contact)
    {
        $data['contact'] = $contact;
        $data['configs'] = ['action' => 'update'];
        return view('backend.contact.create-edit', $data);
    }


    public function update(Request $request, Contact $contact)
    {

        $contact->update($request->all());

        return back()->with( CommonEnum::MESSAGES['success']);
    }


}
