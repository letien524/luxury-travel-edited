<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Faq;
use Illuminate\Http\Request;

class InquireController extends BaseController
{
    protected function getModel(): String
    {
       return 'Category';
    }

    public function index()
    {
        $inquire = Category::firstOrCreate(['type'=>'inquire-category-main']);
        $faqs = Faq::orderBy('name','asc')->get();
        return view('backend.inquire.index',compact('inquire','faqs'));
    }


    public function update(Request $request, Category $inquire)
    {
        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $inquire->update($input);

        return back()->with( CommonEnum::MESSAGES['success']);
    }
}
