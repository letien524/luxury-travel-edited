<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Post;
use Illuminate\Http\Request;

class TravelGuideController extends BaseController
{

    protected function getModel(): String
    {
        return 'Post';
    }

    public function index()
    {
        $data = Post::where('type', 'travel-guide')->orderBy('id', 'desc')->get();
        return view('backend.guide.index', compact('data'));
    }


    public function create()
    {
        return view('backend.guide.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề'
            ]
        );

        $input = $request->all();
        $input['name'] = json_encode($input['name']);
        $input['content'] = json_encode($input['content']);

        Post::create($input);

        return redirect()->route('guide.index')->with(CommonEnum::MESSAGES['success']);


    }

    public function edit(Post $guide)
    {
        return view('backend.guide.edit', compact('guide'));
    }


    public function update(Request $request, Post $guide)
    {
        $this->validate($request,
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề'
            ]
        );

        $input = $request->all();

        $guide->name = json_encode($input['name']);
        $guide->content = json_encode($input['content']);
        $guide->order_menu = $input['order_menu'];

        $guide->save();

        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
