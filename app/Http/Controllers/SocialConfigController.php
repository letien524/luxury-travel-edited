<?php

namespace App\Http\Controllers;

use App\Config;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class SocialConfigController extends BaseController
{
    protected function getModel(): String
    {
       return 'config';
    }

    public function index()
    {
        $data['social'] = Config::firstOrCreate(['type' => 'social']);

        return view('backend.socialConfig.index', $data);
    }

    public function update(Request $request, Config $socialConfig)
    {

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $socialConfig->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
