<?php

namespace App\Http\Controllers;

use App\Config;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class EmailConfigController extends BaseController
{
   protected function getModel(): String
   {
      return 'config';
   }

    public function index()
    {
        $email = Config::firstOrCreate(['type' => 'email']);
        return view('backend.emailConfig.index', compact('email'));
    }

    public function update(Request $request, Config $emailConfig)
    {

        $this->validate($request,[
        ]);

        $input = $request->all();

        $bcc = ['tour','contact','subscribe'];
        foreach ($bcc as $mailType){
            if(!empty($input['content'][$mailType]['bcc'])){
                foreach ($input['content'][$mailType]['bcc'] as $val){
                    if(!filter_var($val,FILTER_VALIDATE_EMAIL)){
                        return back()->withInput()->with([
                            'flash_level' => 'danger',
                            'flash_message' => "Bcc email {$mailType} is invalid!"
                        ]);
                    }
                }
            }
        }

        $input['content'] = json_encode($input['content']);

        $emailConfig->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
