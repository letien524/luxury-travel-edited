<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Post;
use App\TourActivity;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class HowItWorkController extends BaseController
{
    protected function getModel(): String
    {
        return 'post';
    }

    public function index(Builder $builder)
    {

        if (request()->ajax()) {
            $data = Post::where('type','how-it-work')->latest('id')->get(['id', 'name', 'order_menu']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $name = json_decode($row->name, true);
                    $editRoute = route('howItWork.edit', $row);
                    $name = getHtmlLabelContactTable(@$name['main'], $editRoute);
                    return $name;
                })
                ->addColumn('name_sub', function ($row) {
                    $name = json_decode($row->name, true);
                    return @$name['sub'];
                })
                ->addColumn('order_menu', function ($row) {
                    return $row->order_menu;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route('howItWork.edit', $row);
                    $deleteRoute = route('howItWork.destroy', $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);

            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'name_sub', 'name' => 'name_sub', 'title' => TableEnum::LABELS['name_sub']],
            ['data' => 'order_menu', 'name' => 'order_menu', 'title' => TableEnum::LABELS['order_menu']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];
        $data['datatables'] = $builder->columns($attributes);

        return view('backend.howItWork.index', $data);
    }


    public function create()
    {
        return view('backend.howItWork.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name.main' => 'required',
                'name.sub' => 'required',
            ],
            [
                'name.main.required' => 'Nhập tiêu đề chính và phụ tiếng anh',
                'name.sub.required' => 'Nhập tiêu đề chính và phụ tiếng anh',
            ]
            );
        $input = $request->all();
        $input['name'] = json_encode($input['name']);
        $input['content'] = json_encode($input['content']);

        Post::create($input);

        return redirect()->route('howItWork.index')->with(CommonEnum::MESSAGES['success']);

    }

    public function edit(Post $howItWork)
    {
       return view('backend.howItWork.edit',compact('howItWork'));
    }


    public function update(Request $request, Post $howItWork)
    {
        $this->validate($request,
            [
                'name.main' => 'required',
                'name.sub' => 'required',
            ],
            [
                'name.main.required' => 'Nhập tiêu đề chính và phụ tiếng anh',
                'name.sub.required' => 'Nhập tiêu đề chính và phụ tiếng anh',
            ]
        );
        $input = $request->all();
        $input['name'] = json_encode($input['name']);
        $input['content'] = json_encode($input['content']);

        $howItWork->update($input);

        return back()->with( CommonEnum::MESSAGES['success']);
    }
}
