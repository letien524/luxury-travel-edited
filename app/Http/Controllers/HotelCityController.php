<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\HotelCity;
use App\Http\Requests\HotelCityRequest;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class HotelCityController extends BaseController
{
    protected function getModel(): String
    {
        return 'hotelCity';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = HotelCity::latest('id')->get(['id','name']);
            $datatables = DataTables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);
                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.hotelCity.index', $data);
    }


    public function create()
    {
        return view('backend.hotelCity.create-edit');
    }

    public function store(HotelCityRequest $request)
    {
        HotelCity::create($request->all());
        return back()->with(CommonEnum::MESSAGES['success']);
    }


    public function edit(HotelCity $hotelCity)
    {
        $data = [
            'configs' => ['action'=>'update'],
            'hotelCity' => $hotelCity,
        ];
        return view('backend.hotelCity.create-edit', $data);
    }


    public function update(HotelCityRequest $request, HotelCity $hotelCity)
    {
        $hotelCity->update($request->all());
        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
