<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Config;
use App\TourDestination;
use Illuminate\Http\Request;

class TourFilterController extends BaseController
{
    protected function getModel(): String
    {
        return 'config';
    }

    public function index()
    {
        $data = [
          'tourFilter' =>  Config::firstOrCreate(['type'=>'tour-filter']),
          'tourDestinations' =>  TourDestination::orderBy('name')->get(['id','name']),
        ];

        return view('backend.tourFilter.index', $data);
    }

    public function update(Request $request, Config $tourFilter)
    {

        $input = $request->all();

        $input['content'] = !empty( $input['content']) ? json_encode($input['content']) : null;

        $tourFilter->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);
    }

}
