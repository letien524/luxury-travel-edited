<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Enum\CommonEnum;
use App\Brand;
use App\Enum\TableEnum;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class BrandController extends BaseController
{
    protected function getModel(): String
    {
        return 'brand';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Brand::latest('id')->get(['id','name','image', 'type']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })

                ->addColumn('type', function ($row) {
                    return $row->type;
                })

                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);
                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'type', 'name' => 'type', 'title' => TableEnum::LABELS['type']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);
        return view('backend.brand.index', $data);
    }


    public function create()
    {
        return view('backend.brand.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            ['image' => 'required'],
            ['image.required' => 'Bạn chưa chọn hình ảnh!']
        );

        Brand::create($request->all());

        return redirect()->route('brand.index')->with(CommonEnum::MESSAGES['success']);

    }

    public function edit(Brand $brand)
    {

       return view('backend.brand.edit',compact('brand'));
    }


    public function update(Request $request, Brand $brand)
    {

        $this->validate($request,
            ['image' => 'required'],
            ['image.required' => 'Bạn chưa chọn hình ảnh!']
        );

        $brand->update($request->all());

        return back()->with( CommonEnum::MESSAGES['success']);

    }

}
