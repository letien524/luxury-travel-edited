<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Lang;
use Illuminate\Http\Request;

class LangConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'lang';
    }

    public function index()
    {
        $language = Lang::all();
        return view('backend.langConfig.index', compact('language'));
    }

    public function update(Request $request)
    {
        $input = $request->all();

        foreach ($input['link'] as $key => $value) {
            Lang::updateOrCreate(['locale' => $key], ['link' => $value]);
        }

        return back()->with(CommonEnum::MESSAGES['success']);
    }

}
