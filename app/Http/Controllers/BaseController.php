<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use Illuminate\Http\Request;

abstract class BaseController extends Controller
{
    protected $_model;
    protected $_modelClass;

    abstract protected function getModel(): String;

    private function initModel()
    {
        $getModel = $this->getModel();

        if (!empty($getModel)) {
            $getModel = ucfirst($getModel);
            $this->_modelClass = "\App\\{$getModel}";

            $this->_model = new $this->_modelClass;
        }
        $this->_model = null;
    }

    public function __construct()
    {
        $this->middleware(['auth', 'role']);
        $this->initModel();
    }

    public function destroy($id)
    {
        $this->_modelClass::destroy($id);
        return back()->with(CommonEnum::MESSAGES['success']);
    }

    public function bulkDestroy(Request $request)
    {
        $input = $request->input('chkItem');

        if (count((array)$input) == 0) {
            return back()->with(CommonEnum::MESSAGES['bulkDestroyFailByCheckbox']);
        }

        $this->_modelClass::destroy($input);

        return back()->with(CommonEnum::MESSAGES['success']);


    }
}
