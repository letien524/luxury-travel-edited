<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Faq;
use App\Member;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class FaqController extends BaseController
{
    protected function getModel(): String
    {
        return 'faq';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Faq::where('type', 'faq')->latest('id')->get(['id', 'name']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action',])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);
        return view('backend.faq.index', $data);
    }

    public function create()
    {
        return view('backend.faq.create');
    }

    public function store(Request $request)
    {

        $this->validate($request,
            ['name' => 'required'],
            ['name.required' => 'Bạn chưa nhập tiêu đề']
        );

        $input = $request->all();

        Faq::create($input);

        return redirect()->route('faq.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Faq $faq)
    {
        return view('backend.faq.edit', compact('faq'));
    }


    public function update(Request $request, Faq $faq)
    {
        $this->validate($request,
            ['name' => 'required'],
            ['name.required' => 'Bạn chưa nhập tiêu đề']
        );
        $input = $request->all();

        $faq->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);
    }

}
