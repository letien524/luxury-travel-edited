<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Http\Requests\ReviewRequest;
use App\Member;
use App\Review;
use App\ReviewCategory;
use App\Tour;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class ReviewController extends BaseController
{
   protected function getModel(): String
   {
       return 'review';
   }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Review::with('categories:id,name')->latest('id')->get(['id', 'name', 'image','submited_at','display']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('categories', function ($row) {
                    $categories = $row->categories->count() ? $row->categories->implode('name', ', ') : null;
                    return $categories;
                })

                ->addColumn('submited_at', function ($row) {
                    return $row->submited_at;
                })

                ->addColumn('display', function ($row) {
                    $status = getHtmlStatusTourTable($row->display, 'status');
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'display'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'categories', 'name' => 'categories', 'title' => TableEnum::LABELS['categories']],
            ['data' => 'submited_at', 'name' => 'submited_at', 'title' => TableEnum::LABELS['submited_at']],
            ['data' => 'display', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.review.index',$data);
    }

    public function create()
    {
        $data = [
            'reviewCategories' => ReviewCategory::orderBy('name','asc')->get(['id','name']),
            'tours' => Tour::orderBy('name','asc')->get(['id','name']),
        ];

        return view('backend.review.create',$data);
    }

    public function store(ReviewRequest $request)
    {

        $input = $request->all();

        $input['submited_at'] = Carbon::parse($input['submited_at'])->format('Y-m-d');

        $review = Review::create($input);

        $review->categories()->sync(cleanArray(@$input['categories']));
        $review->memberTours()->sync(cleanArray(@$input['tours']));

        return redirect()->route('review.edit',$review)->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Review $review)
    {
        $data = [
            'reviewCategories' => ReviewCategory::orderBy('name','asc')->get(['id','name']),
            'tours' => Tour::orderBy('name','asc')->get(['id','name']),
            'data' => $review,
        ];

        return view('backend.review.edit',$data);
    }

    public function update(ReviewRequest $request, Review $review)
    {
        $input = $request->all();

        $input['display'] = isset($input['display']);
        $input['submited_at'] = Carbon::parse($input['submited_at'])->format('Y-m-d');

        $review->update($input);

        $review->categories()->sync(cleanArray(@$input['categories']));
        $review->memberTours()->sync(cleanArray(@$input['tours']));

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
