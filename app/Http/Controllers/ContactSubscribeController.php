<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Contact;
use App\Enum\TableEnum;
use App\Jobs\SendMailToAdminHasNewSubcribeJob;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class ContactSubscribeController extends BaseController
{
    protected function getModel(): String
    {
        return 'contact';
    }

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth')->except('store');
        $this->middleware('role');
    }

    private $__model = 'contactSubscribe';

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Contact::where('type', 'customer-subscribe')->latest('id')->get(['id','email', 'created_at','status']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('email', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $email = getHtmlLabelContactTable($row->email, $editRoute);
                    return $email;
                })
                ->addColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusContact($row->status);
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $deleteRoute = route("{$this->__model}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'email', 'status', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'email', 'name' => 'email', 'title' => TableEnum::LABELS['email']],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => TableEnum::LABELS['created_at']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);
        return view('backend.contactSubscribe.index', $data);
    }

    public function store(Request $request)
    {

    }

    public function edit(Contact $contactSubscribe)
    {
        $data['contactSubscribe'] = $contactSubscribe;
        $data['configs'] = ['action' => 'update'];
        return view('backend.contactSubscribe.create-edit', $data);
    }


    public function update(Request $request, Contact $contactSubscribe)
    {
        $input = $request->all();
        $input['status'] = !empty($input['status']) ? $input['status'] : null;
        $contactSubscribe->update($input);
        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
