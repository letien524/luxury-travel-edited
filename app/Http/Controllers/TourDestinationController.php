<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Enum\TableEnum;
use App\Faq;
use App\Http\Requests\TourDestinationRequest;
use App\Member;
use App\ReviewCategory;
use App\TourActivity;
use App\TourDestination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class TourDestinationController extends BaseController
{

    protected function getModel(): String
    {
        return 'tourDestination';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {

            $data = TourDestination::with('parent:id,name')->latest('id')->get(['id', 'name', 'image', 'slug', 'order_menu', 'status', 'parent_id', 'is_best', 'is_inquire']);

            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('slug', function ($row) {
                    return $row->slug;
                })
                ->addColumn('destination_parent', function ($row) {
                    return $row->parent ? $row->parent->name : null;
                })
                ->addColumn('order_menu', function ($row) {
                    return $row->order_menu;
                })
                ->addColumn('status_menu', function ($row) {
                    $status = getHtmlStatusTourTable($row->status, 'status');
                    return $status;
                })
                ->addColumn('is_best', function ($row) {
                    $isBest = getHtmlStatusTourTable($row->is_best, 'status');
                    return $isBest;
                })
                ->addColumn('is_inquire', function ($row) {
                    $isInquire = getHtmlStatusTourTable($row->is_best, 'status');
                    return $isInquire;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $viewRoute = route("{$this->getModel()}.detail", $row->slug);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute, $viewRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'status_menu', 'is_best', 'is_inquire', 'action'])
                ->make(true);

            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'slug', 'name' => 'slug', 'title' => TableEnum::LABELS['slug']],
            ['data' => 'destination_parent', 'name' => 'destination_parent', 'title' => TableEnum::LABELS['destination_parent']],
            ['data' => 'order_menu', 'name' => 'order_menu', 'title' => TableEnum::LABELS['order_menu']],
            ['data' => 'status_menu', 'name' => 'status_menu', 'title' => TableEnum::LABELS['status_menu']],
            ['data' => 'is_best', 'name' => 'is_best', 'title' => TableEnum::LABELS['is_best']],
            ['data' => 'is_inquire', 'name' => 'is_inquire', 'title' => TableEnum::LABELS['is_inquire']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];
        $data['datatables'] = $builder->columns($attributes);

        return view('backend.tourDestination.index', $data);
    }


    public function create()
    {
        $data = [
            'members' => Member::orderBy('name', 'asc')->get(['id','name','image','position']),
            'faqs' => Faq::latest('id')->get(['id','name']),
            'blogCategories' => Category::where('type', 'blog-category')->orderBy('name', 'asc')->get(['id','name']),
            'tourDestinations' => TourDestination::where('parent_id', null)->orderBy('name', 'asc')->get(['id','name']),
            'reviewCategories' => ReviewCategory::orderBy('name', 'asc')->get(['id','name']),
        ];

        return view('backend.tourDestination.create-edit', $data);
    }


    public function store(TourDestinationRequest $request)
    {
        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        TourDestination::create($input);

        return redirect()->route('tourDestination.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(TourDestination $tourDestination)
    {
        $data = [
            'configs' => ['action'=>'update'],
            'members' => Member::orderBy('name', 'asc')->get(['id','name','image','position']),
            'faqs' => Faq::latest('id')->get(['id','name']),
            'tourDestination' => $tourDestination,
            'blogCategories' => Category::where('type', 'blog-category')->orderBy('name', 'asc')->get(['id','name']),
            'tourDestinations' => TourDestination::where('parent_id', null)->where('id', '<>', $tourDestination->id)->orderBy('name', 'asc')->get(['id','name']),
            'reviewCategories' => ReviewCategory::orderBy('name', 'asc')->get(['id','name']),
        ];

        return view('backend.tourDestination.create-edit', $data);
    }

    public function update(TourDestinationRequest $request, TourDestination $tourDestination)
    {
        $input = $request->all();

        $input['content'] = json_encode($input['content']);
        $input['index'] = isset($input['index']);
        $input['status'] = isset($input['status']) ;
        $input['is_best'] = isset($input['is_best']);
        $input['is_inquire'] = isset($input['is_inquire']);

        $tourDestination->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
