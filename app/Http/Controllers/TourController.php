<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Enum\TableEnum;
use App\Faq;
use App\Hotel;
use App\HotelCity;
use App\ReviewCategory;
use App\Tour;
use Illuminate\Support\Str;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;


class TourController extends BaseController
{
    protected function getModel(): String
    {
        return 'tour';
    }

    public function __construct()
    {
        parent::__construct();

        Validator::extend('less_than', function ($attribute, $value, $params, $validator) {

            $other = Input::get($params[0]);
            return intval($value) < intval($other);
        });

        Validator::replacer('less_than', function ($message, $attribute, $rule, $params) {
            if ($message !== 'validation.less_than') {
                return str_replace('_', ' ', $message);
            }
            return str_replace('_', ' ', $attribute . $rule . $params[0]);
        });
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {

            $tours = Tour::with('activities:id,name', 'destinations:id,name')
                ->latest('id')
                ->get(['id', 'name', 'image', 'slug', 'index', 'status', 'highlight']);

            $datatables = Datatables::of($tours)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('slug', function ($row) {
                    return $row->slug;
                })
                ->addColumn('activities', function ($row) {
                    $activities = $row->activities->count() ? $row->activities->implode('name', ', ') : '';
                    $activities = "<span title='$activities'>" . Str::limit($activities) . "</span>";
                    return $activities;
                })
                ->addColumn('destinations', function ($row) {
                    $destinations = $row->destinations->count() ? $row->destinations->implode('name', ', ') : '';
                    $destinations = "<span title='$destinations'>" . Str::limit($destinations, 50) . "</span>";
                    return $destinations;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusTourTable($row->status, 'status');
                    return $status;
                })
                ->addColumn('highlight', function ($row) {
                    $highlight = getHtmlStatusTourTable($row->highlight, 'highlight');
                    return $highlight;
                })
                ->addColumn('index', function ($row) {
                    $index = getHtmlStatusTourTable($row->index, 'index');
                    return $index;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $duplicateRoute = route("{$this->getModel()}.duplicate", $row);
                    $viewRoute = route("{$this->getModel()}.detail", $row->slug);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButtonTour($editRoute, $deleteRoute, $viewRoute, $duplicateRoute);
                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'activities', 'destinations', 'status', 'highlight', 'index'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'slug', 'name' => 'slug', 'title' => TableEnum::LABELS['slug']],
            ['data' => 'activities', 'name' => 'activities', 'title' => TableEnum::LABELS['activities']],
            ['data' => 'destinations', 'name' => 'destinations', 'title' => TableEnum::LABELS['destinations']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'highlight', 'name' => 'highlight', 'title' => TableEnum::LABELS['highlight']],
            ['data' => 'index', 'name' => 'index', 'title' => TableEnum::LABELS['index']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.tour.index', $data);
    }

    public function create()
    {
        $data = [
            'categories' => Category::where('type', 'tour-category')->orderBy('name', 'asc')->get(['id', 'name']),
            'destinations' => Category::where('type', 'destination-category')->orderBy('name', 'asc')->get(['id', 'name']),
            'faqs' => Faq::orderBy('id', 'desc')->get(['id', 'name']),
            'reviewCategories' => ReviewCategory::orderBy('name', 'asc')->get(['id', 'name']),
            'hotelCities' => HotelCity::orderBy('name', 'asc')->get(['id', 'name']),
        ];

        return view('backend.tour.create', $data);

    }

    public function store(Request $request)
    {
        $this->validate($request,

            [
                'name' => 'required|unique:tours,name',
                'slug' => 'required|unique:tours,slug',
                'price' => 'required',
                'price_promotion' => 'less_than:price'
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề!',
                'name.unique' => 'Tiêu đề đã tồn tại!',
                'slug.required' => 'Bạn chưa nhập đường dẫn tĩnh!',
                'slug.unique' => 'Đường dẫn tĩnh đã tồn tại!',
                'price.required' => 'Bạn chưa nhập giá!',
                'price_promotion.less_than' => 'Giá khuyến mại phải nhỏ hơn giá gốc !'
            ]
        );

        $input = $request->all();

        $input['highlight'] = isset($input['highlight']) ? $input['highlight'] : null;
        $input['content'] = json_encode($input['content']);
        $input['plan'] = isset($input['plan']) ? json_encode($input['plan']) : null;
        $input['hotel'] = isset($input['hotel']) ? json_encode($input['hotel']) : null;
        $input['map'] = json_encode($input['map']);

        $input['file'] = !empty($request->file('file')) ? image_upload($request, 'file', null, __STORAGE_FILE) : null;

        $tour = Tour::create($input);


        $tour->activities()->sync((array)@$input['activities']);
        $tour->destinations()->sync((array)@$input['destinations']);
        $tour->reviews()->sync((array)@$input['reviews']);
        $tour->hotelCities()->sync((array)@$input['hotelCities']);
        $tour->hotels()->sync((array)@$input['hotels']);

        if (count((array)$request->input('gallery'))) {

            $input['gallery'] = array_map(function ($var) {
                return ['url' => $var];
            }, $input['gallery']);

            $tour->gallery()->createMany($input['gallery']);
        }
        return redirect()->route('tour.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Tour $tour)
    {
        $data = [
            'tour' => $tour,
            'categories' => Category::where('type', 'tour-category')->orderBy('name', 'asc')->get(['id', 'name']),
            'destinations' => Category::where('type', 'destination-category')->orderBy('name', 'asc')->get(['id', 'name']),
            'faqs' => Faq::orderBy('id', 'desc')->get(['id', 'name']),
            'reviewCategories' => ReviewCategory::orderBy('name', 'asc')->get(['id', 'name']),
            'hotelCities' => HotelCity::orderBy('name', 'asc')->get(['id', 'name']),
        ];

        return view('backend.tour.edit', $data);
    }

    public function update(Request $request, Tour $tour)
    {
        $this->validate($request,
            [
                'name' => 'required|unique:tours,name,' . $tour->id,
                'slug' => 'required|unique:tours,slug,' . $tour->id,
                'price' => 'required',
                'price_promotion' => 'less_than:price'
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề!',
                'name.unique' => 'Tiêu đề đã tồn tại!',
                'slug.required' => 'Bạn chưa nhập đường dẫn tĩnh!',
                'slug.unique' => 'Đường dẫn tĩnh đã tồn tại!',
                'price.required' => 'Bạn chưa nhập giá!',
                'price_promotion.less_than' => 'Giá khuyến mại phải nhỏ hơn giá gốc !'
            ]
        );

        $input = $request->all();

        $input['status'] = isset($input['status']) ? $input['status'] : null;
        $input['category_id'] = isset($input['category_id']) ? $input['category_id'] : null;
        $input['index'] = isset($input['index']) ?: null;

        $input['highlight'] = isset($input['highlight']) ? $input['highlight'] : null;
        $input['content'] = json_encode($input['content']);
        $input['map'] = json_encode($input['map']);
        $input['file'] = !empty($input['file']) ? image_upload($request, 'file', $tour->file, __STORAGE_FILE) : $tour->file;
        $input['plan'] = isset($input['plan']) ? json_encode($input['plan']) : null;
        $input['hotel'] = isset($input['hotel']) ? json_encode($input['hotel']) : null;


        $tour->update($input);

        $tour->activities()->sync((array)@$input['activities']);
        $tour->destinations()->sync((array)@$input['destinations']);
        $tour->reviews()->sync((array)@$input['reviews']);
        $tour->hotelCities()->sync((array)@$input['hotelCities']);
        $tour->hotels()->sync((array)@$input['hotels']);

        $tour->gallery()->delete();


        if (count((array)$request->input('gallery'))) {

            $input['gallery'] = array_map(function ($var) {
                return ['url' => $var];
            }, $input['gallery']);

            $tour->gallery()->createMany($input['gallery']);
        }

        return back()->with(CommonEnum::MESSAGES['success']);

    }

    public function duplicate(Tour $tour)
    {
        $duplicate = $tour->replicate();
        $duplicate->save();
        $duplicate->name = $tour->name . '-' . $duplicate->id;
        $duplicate->slug = $tour->slug . '-' . $duplicate->id;
        $duplicate->save();

        $duplicate->activities()->sync($tour->activities->count() ? $tour->activities->pluck('id') : []);
        $duplicate->destinations()->sync($tour->destinations->count() ? $tour->destinations->pluck('id') : []);
        $duplicate->reviews()->sync($tour->reviews->count() ? $tour->reviews->pluck('id') : []);
        $duplicate->hotelCities()->sync($tour->hotelCities->count() ? $tour->hotelCities->pluck('id') : []);
        $duplicate->hotels()->sync($tour->hotels->count() ? $tour->hotels->pluck('id') : []);

        if ($tour->gallery->count()) {
            $gallery = $tour->gallery()->get(['url'])->toArray();
            $duplicate->gallery()->createMany($gallery);
        }

        return back()->with(CommonEnum::MESSAGES['success']);
    }

    public function hotel(Request $request)
    {
        if ($request->ajax()) {
            $req = $request->all();
            $idCities = (array)@$req['idCities'];
            $idHotels = (array)@$req['idHotels'];

            $data['hotels'] = Hotel::whereIn('city_id', $idCities)->get(['id', 'name', 'city_id']);
            $data['selectedHotels'] = Hotel::whereIn('id', $idHotels)->get('id');
            $data['selectedHotels'] = $data['selectedHotels']->count() ? $data['selectedHotels']->pluck('id')->toArray() : [];
            $data['hotelCities'] = HotelCity::whereIn('id', $idCities)->get(['id','name']);

            return view('backend.tour.hotel', $data);
        }
    }
}
