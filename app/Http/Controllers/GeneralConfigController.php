<?php

namespace App\Http\Controllers;

use App\Config;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class GeneralConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'config';
    }

    public function index()
    {
        $data['general'] = $this->_modelClass::firstOrCreate(['type'=> 'general']);

        return view('backend.generalConfig.index', $data);
    }

    public function update(Request $request, Config $generalConfig)
    {

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $generalConfig->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
