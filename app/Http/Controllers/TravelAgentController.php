<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use Illuminate\Http\Request;

class TravelAgentController extends BaseController
{

    protected function getModel(): String
    {
        return 'Category';
    }

    public function index()
    {
        $travel_agent = Category::firstOrCreate(['type' => 'travel-agent-category-main']);
        return view('backend.travel_agent.index', compact('travel_agent'));
    }

    public function update(Request $request, Category $travel_agent)
    {

        $travel_agent->update($request->only('content'));

        return back()->with( CommonEnum::MESSAGES['success']);
    }
}
