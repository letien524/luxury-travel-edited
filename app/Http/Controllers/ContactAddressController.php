<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Contact;
use App\Enum\TableEnum;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class ContactAddressController extends BaseController
{
    protected function getModel(): String
    {
        return 'contact';
    }

    private $__model = 'contactAddress';

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Contact::where('type', 'address')->latest('id')->get(['id','name','email', 'address','phone','hotline','order_menu']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })
                ->addColumn('email', function ($row) {
                    return $row->email;
                })
                ->addColumn('address', function ($row) {
                    return $row->address;
                })
                ->addColumn('phone', function ($row) {
                    return $row->phone;
                })
                ->addColumn('hotline', function ($row) {
                    return $row->hotline;
                })
                ->addColumn('order_menu', function ($row) {
                    return $row->order_menu;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $deleteRoute = route("{$this->__model}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'email', 'name' => 'email', 'title' => TableEnum::LABELS['email']],
            ['data' => 'address', 'name' => 'address', 'title' => TableEnum::LABELS['address']],
            ['data' => 'phone', 'name' => 'phone', 'title' => TableEnum::LABELS['phone']],
            ['data' => 'hotline', 'name' => 'hotline', 'title' => TableEnum::LABELS['hotline']],
            ['data' => 'order_menu', 'name' => 'order_menu', 'title' => TableEnum::LABELS['order_menu']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.contactAddress.index', $data);
    }

    public function create()
    {
        return view('backend.contactAddress.create-edit');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề'
            ]
        );

        $input = $request->all();

        Contact::create($input);

        return redirect()->route('contactAddress.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Contact $contactAddress)
    {
        $data['contactAddress'] = $contactAddress;
        $data['configs'] = ['action'=>'update'];
        return view('backend.contactAddress.create-edit', $data);
    }


    public function update(Request $request, Contact $contactAddress)
    {
        $this->validate($request,
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề'
            ]
        );

        $input = $request->all();

        $contactAddress->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
