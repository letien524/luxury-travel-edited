<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Http\Requests\PopupRequest;
use App\Popup;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class PopupController extends BaseController
{
    protected function getModel(): String
    {
       return 'popup';
    }


    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Popup::latest('id')->get(['id','name', 'type','is_active']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })

                ->addColumn('type', function ($row) {
                    return $row->type;
                })

                ->addColumn('is_active', function ($row) {
                    $isActive = getHtmlStatusTourTable($row->is_active,'active');
                    return $isActive;
                })

                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);
                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'is_active', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'type', 'name' => 'type', 'title' => TableEnum::LABELS['type']],
            ['data' => 'is_active', 'name' => 'is_active', 'title' => TableEnum::LABELS['is_active']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);
        return view('backend.popup.index', $data);
    }

    public function create()
    {
        return view('backend.popup.create-edit');
    }

    public function store(PopupRequest $request)
    {
        $input = $request->all();
        $popup = Popup::create($input);
        return redirect()->route('popup.edit', $popup)->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Popup $popup)
    {
        $data = [
            'configs' => ['action'=>'update'],
            'popup' => $popup,
        ];
        return view('backend.popup.create-edit', $data);
    }

    public function update(PopupRequest $request, Popup $popup)
    {
        $input = $request->all();
        $input['is_active']  = isset($input['is_active']);
        $popup->update($input);
        return back()->with(CommonEnum::MESSAGES['success']);
    }

}
