<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Enum\TableEnum;
use App\Lang;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class BlogTagController extends BaseController
{
    protected function getModel(): String
    {
        return 'category';
    }

    private $__model = 'blogTag';

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Category::where('type', 'blog-tag')->latest('id')->get(['id', 'name','slug', 'image']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })

                ->addColumn('slug', function ($row) {
                    return $row->slug;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $deleteRoute = route("{$this->__model}.destroy", $row);
                    $viewRoute = route("{$this->__model}.detail", $row->slug);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute, $viewRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'slug', 'name' => 'slug', 'title' => TableEnum::LABELS['slug']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);
        return view('backend.blogTag.index', $data);
    }

    public function create()
    {

        return view('backend.blogTag.create-edit');

    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]
        );

        $categoryNameExisted = Category::where([
            ['type','blog-tag'],
            ['name',$request->input('name')],
        ])->count();

        $categorySlugExisted = Category::where([
            ['type','blog-tag'],
            ['slug',$request->input('slug')],
        ])->count();

        if ($categoryNameExisted){
            return back() ->withInput($request->input())->with([
                'flash_level' => 'danger',
                'flash_message' => 'Dữ liệu thêm thất bại! Đã tồn tại tên thẻ',
            ]);
        }

        if ($categorySlugExisted){
            return back()->withInput($request->input())->with(CommonEnum::MESSAGES['categorySlugIsExisted']);
        }

        Category::create($request->all());

        return redirect()->route('blogTag.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Category $blogTag)
    {
        $data['blogTag'] = $blogTag;
        $data['configs'] = ['action'=>'update'];
        return view('backend.blogTag.create-edit', $data);
    }

    public function update(Request $request, Category $blogTag)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]

        );

        if ($blogTag->name != $request->input('name')) {
            $categoryNameExisted = Category::where([
                ['type','blog-tag'],
                ['name',$request->input('name')],
            ])->count();

            if ($categoryNameExisted){
                return back() ->withInput($request->input())->with([
                    'flash_level' => 'danger',
                    'flash_message' => 'Dữ liệu thêm thất bại! Đã tồn tại tên thẻ',
                ]);
            }

        }
        if ($blogTag->slug != $request->input('slug')) {
            $categorySlugExisted = Category::where([
                ['type','blog-tag'],
                ['slug',$request->input('slug')],
            ])->count();

            if ($categorySlugExisted){
                return back()->withInput($request->input())->with(CommonEnum::MESSAGES['categorySlugIsExisted']);
            }

        }

        $blogTag->update($request->all());

        return back()->with( CommonEnum::MESSAGES['success']);
    }
}
