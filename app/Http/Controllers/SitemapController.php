<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tour;

class SitemapController extends Controller
{
    public function index()
    {
        $tours = Tour::where('status', 1)->latest('updated_at')->get();
        $activities = Category::where('status', 1)->where('type', 'tour-category')->latest('updated_at')->get();
        $destinations = Category::where('index', 1)->where('type', 'destination-category')->latest('updated_at')->get();
        $pages = Post::where('status', 1)->where('type', 'page')->latest('updated_at')->get();

        $activityPage = Category::where('type', 'activity-contact-main')->first();
        $about = Post::where('type', 'about')->first();
        $contact = Category::where('type', 'contact-category-main')->first();
        $home = Post::where('type', 'home')->first();

        $data = [
            'tours' => $tours,
            'activities' => $activities,
            'destinations' => $destinations,
            'pages' => $pages,

            'about' => $about,
            'contact' => $contact,
            'home' => $home,
            'activityPage' => $activityPage,
        ];

        return response()->view('sitemap.index', $data)->header('Content-Type', 'text/xml');

    }
}
