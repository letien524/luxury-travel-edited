<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends BaseController
{
    protected function getModel(): String
    {
        return 'Post';
    }

    public function index()
    {
        $inputDefault = [
            'name' => 'Home',
            'content' => [
                'section1' => [
                    'name' => [
                        "main" => "Discover South East Asia",
                        "sub" => "Luxury Tailor-made Experience to"
                    ],
                    'content' => [
                        [
                            "name" => "Tailor made",
                            "content" => "You can expect a consistent set of amenities you need to live like you do at home–from"
                        ],
                        [
                            "name" => "Local Expert",
                            "content" => "If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't"
                        ],
                        [
                            "name" => "Premium service",
                            "content" => "You can expect a consistent set of amenities you need to live like you do at home"
                        ]
                    ]
                ],
                'section2' => [
                    "name" => "We offer luxury travel exclusively to Southeast Asia",
                    "link" => null,
                    "content" => "Luxury Travel Ltd. was founded in 2004 by Mr. Pham Ha, passionate traveler and entrepreneur. With more than 15 years of experience in the travel industry, he is a keynote speaker and luxury travel expert in Vietnam and around the globe. When Pham Ha saw the need for a tour company offering services that would specialize in high-end travelers, he didn’t hesitate for long."
                ],
                'section3' => [
                    "name" => "How it works",
                    "name_sub" => "Amazing holiday packages in 3 simple steps"
                ],
                'section5' => [
                    "name" => "Tour highlight"
                ],
                'section6' => [
                    "name" => "Tour Style",
                    "name_sub" => "Find your ideal tour"
                ],
                'section7' => [
                    "name" => "Tripadvisor",
                    "link" => null
                ],
                'section8' => [
                    "name" => "News and Special"
                ],
            ]
        ];

        $inputDefault['content'] = json_encode($inputDefault['content']);

        $data = [
            'home' => Post::firstOrCreate(['type' => 'home'], $inputDefault)
        ];

        return view('backend.home.index', $data);
    }


    public function update(Request $request, Post $home)
    {
        $this->validate($request,
            ['name' => 'required'],
            ['name.required' => 'Dữ liệu lưu thất bại! Bạn chưa nhập tiêu đề trang']
        );

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $home->update($input);

        return redirect()->route('home.index')->with(CommonEnum::MESSAGES['success']);
    }

}
