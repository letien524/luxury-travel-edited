<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Http\Requests\RedirectRequest;
use App\Member;
use App\Redirect;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class RedirectController extends BaseController
{
    protected function getModel(): String
    {
        return 'redirect';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $redirects = Redirect::latest('id')->get(['id','from','to','status']);
            $datatables = Datatables::of($redirects)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('from', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlContentRedirectTable($row->from, $editRoute);
                    return $name;
                })
                ->addColumn('to', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlContentRedirectTable($row->to, $editRoute);
                    return $name;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusTourTable($row->status, 'active');
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'to', 'from', 'action', 'status'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'from', 'name' => 'from', 'title' => TableEnum::LABELS['from']],
            ['data' => 'to', 'name' => 'to', 'title' => TableEnum::LABELS['to']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.redirect.index',$data);
    }

    public function create()
    {
        return view('backend.redirect.create');
    }


    public function store(RedirectRequest $request)
    {
        $input = $request->all();
        $input['from'] = array_reverse(explode(url(''),$input['from']))[0];

        Redirect::create($input);

        return redirect()->route('redirect.index')->with( CommonEnum::MESSAGES['success']);
    }

    public function edit(Redirect $redirect)
    {

       return view('backend.redirect.edit',compact('redirect'));
    }


    public function update(RedirectRequest $request, Redirect $redirect)
    {

        $input = $request->all();

        $input['status']  = isset($input['status']) ? $input['status'] : null;
        $input['from'] = array_reverse(explode(url(''),$input['from']))[0];

        $redirect->update($input);

        return back()->with( CommonEnum::MESSAGES['success']);
    }
}
