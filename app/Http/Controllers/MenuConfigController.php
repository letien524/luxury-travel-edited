<?php

namespace App\Http\Controllers;

use App\Config;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class MenuConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'config';
    }

    public function index()
    {
        $data['menu'] = Config::firstOrCreate(['type' => 'menu']);
        return view('backend.menuConfig.index', $data);
    }

    public function update(Request $request, Config $menuConfig)
    {
        $input = $request->all();
        $menuConfig->update($input);
        return back()->with(CommonEnum::MESSAGES['success']);

    }
}
