<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Faq;
use App\Post;
use Illuminate\Http\Request;

class AboutController extends BaseController
{
    protected function getModel(): String
    {
        return 'Category';
    }

    public function index()
    {
        $input = [
            'name'=>'About',
            'content' => [
                'section1' => [
                    "name" => "Who We Are",
                    "content" => null
                ],
                'section2' => [
                    "name" => "Our Business Is Sustainable And Responsible",
                    "content" => [
                        0 => null,
                        1 => null
                    ]
                ],
                'section4' => [
                    "name" => "Our Selling Point",
                    "content" => [
                        0 => [
                            "name" => null,
                            "content" => null
                        ],
                        1 => [
                            "name" => null,
                            "content" => null
                        ],
                        2 => [
                            "name" => null,
                            "content" => null
                        ],
                    ]
                ],
                'section5' => [
                    "name" => "Creating Authentic Experiences For Discerning Travelers."
                ],
                'section6' => [
                    "name" => "Meet Our Team",
                    "content" => null
                ],
            ]
        ];
        $input['content'] = json_encode($input['content']);

        $about = Category::firstOrCreate(['type'=>'about'],$input);
        $faqs = Faq::orderBy('id','desc')->get();

        return view('backend.about.index', compact('about','faqs'));
    }


    public function create()
    {

    }


    public function store(Request $request)
    {


    }


    public function show($id)
    {
        //
    }


    public function edit(Post $about)
    {

    }


    public function update(Request $request, Category $about)
    {
        $this->validate($request,
            ['name' => 'required'],
            ['name.required' => 'Tiêu đề chính không được để trống!']
        );
        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $input['index'] = isset($input['index']) ?: null;

        $about->update($input);

        return back()->with( CommonEnum::MESSAGES['success']);

    }


}
