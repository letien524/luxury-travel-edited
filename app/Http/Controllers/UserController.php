<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Facades\DataTables;

class UserController extends BaseController
{
    protected function getModel(): String
    {
       return 'user';
    }

    protected $messages = [
        'denied' => CommonEnum::MESSAGES['permissionDenied'],
        'success' => CommonEnum::MESSAGES['success'],
        'error_password' => CommonEnum::MESSAGES['inValidCurrentPassword'],
        'nothing' => CommonEnum::MESSAGES['emptyData'],
        'warning' => CommonEnum::MESSAGES['bulkDestroyFailByCheckbox']
    ];

    public function checkEditUserPermission($user)
    {
        if (!isDeveloper()) {
            if ($user['id'] == auth()->id()) {
                return true;
            } else {
                if (isLevel(1) && $user['id'] != 1) {
                    return true;
                }
            }
            return false;
        }
        return true;

    }

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->middleware('isSuperAdmin', ['except' => ['edit', 'update']]);
    }

    public function index(Builder $builder)
    {

        if (request()->ajax()) {
            $getColumns = ['id', 'name','level','created_at','updated_at'];
            if (isDeveloper()) {
                $users = User::get($getColumns);
            } else {
                $users = User::where('id', '<>', 1)->get($getColumns);
            }
            $datatables = Datatables::of($users)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('level', function ($row) {
                   $role = getHtmlUserRole($row->level, $row->id);
                    return $role;
                })
                ->addColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->addColumn('updated_at', function ($row) {
                    $updatedAt = $row->updated_at;
                    return $updatedAt;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'level'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'level', 'name' => 'level', 'title' => TableEnum::LABELS['level']],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => TableEnum::LABELS['created_at']],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => TableEnum::LABELS['updated_at']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.user.index', $data);
    }


    public function create()
    {
        return view('backend.user.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required|unique:users',
                'password' => 'required',
                're-password' => 'required|same:password',
            ]
        );

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        User::create($input);

        return redirect()->route('user.index')->with($this->messages['success']);

    }

    public function show($id)
    {
        //
    }


    public function edit(User $user)
    {

        if (!$this->checkEditUserPermission($user)) {
            return back()->with($this->messages['denied']);
        }
        return view('backend.user.edit', compact('user'));

    }


    public function update(Request $request, User $user)
    {

        if (!$this->checkEditUserPermission($user)) {
            return back()->with($this->messages['denied']);
        }

        if (!Hash::check($request->input('current_password'), auth()->user()->getAuthPassword())) {
            return back()->with($this->messages['error_password']);
        }


        if (isLevel(1)) {
            $input['level'] = $request->input('level');
        }

        if (strlen($request->input('password'))) {
            $input['password'] = bcrypt($request->input('password'));
        }

        if (empty($input)) {
            return back()->with($this->messages['nothing']);
        }

        $user->update($input);

        return back()->with($this->messages['success']);

    }


    public function destroy($id)
    {
        $user = $this->_model->findOrFail($id);

        if ($user['id'] != auth()->id() && !isLevel(1)) {
            return back()->with($this->messages['denied']);
        }

        $user->delete();
        return back()->with($this->messages['success']);
    }

    public function bulkDestroy(Request $request)
    {
        $input = $request->input('chkItem');

        if(count($input) == 0){
            return back()->with($this->messages['warning']);
        }

        $this->_model->destroy($input);

        return back()->with($this->messages['success']);
    }
}
