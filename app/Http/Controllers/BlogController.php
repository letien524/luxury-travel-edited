<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Enum\TableEnum;
use App\Post;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class BlogController extends BaseController
{
    private $__model = 'blog';
    protected function getModel(): String
    {
        return 'post';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Post::with('category')->where('type', 'blog')->latest('id')->get(['id', 'name','slug', 'image', 'status']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $name = getHtmlBlogLabelTable($row, $editRoute);
                    return $name;
                })

                ->addColumn('categories', function ($row) {
                    $categories = $row->category->count() ? $row->category->implode('name', ', ') : null;
                    return $categories;
                })

                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusOrder($row->status);
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $deleteRoute = route("{$this->__model}.destroy", $row);
                    $viewRoute = route("{$this->__model}.detail", $row->slug);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute, $viewRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'status'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'categories', 'name' => 'categories', 'title' => TableEnum::LABELS['categories']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);
        return view('backend.blog.index', $data);
    }

    public function create()
    {
        $tags = Category::where('type', 'blog-tag')->orderBy('name', 'asc')->get();
        $categories = Category::where('type', 'blog-category')->orderBy('name', 'asc')->get();


        return view('backend.blog.create', compact('tags', 'categories'));
    }


    public function store(Request $request)
    {

        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]
        );


        $input = $request->all();


        $blogExistedName = Post::where([
            ['name', $input['name']],
            ['type', 'blog'],
        ])->count();

        if ($blogExistedName) {
            return back()->withInput($request->input())->with(CommonEnum::MESSAGES['nameIsExisted']);
        }

        $blogExistedSlug = Post::where([
            ['slug', $input['slug']],
            ['type', 'blog'],
        ])->count();

        if ($blogExistedSlug) {
            return back()->withInput($request->input())->with(CommonEnum::MESSAGES['slugIsExisted']);
        }


        $input['tag'] = isset($input['tag']) ? $input['tag'] : [];
        $input['cat'] = isset($input['cat']) ? $input['cat'] : [];

        $input['image'] = json_encode($input['image']);


        $blog = Post::create($input);


        $blog->tag()->sync($input['tag']);
        $blog->category()->sync($input['cat']);
        return redirect()->route('blog.index')->with(CommonEnum::MESSAGES['success']);


    }

    public function edit(Post $blog)
    {
        $tags = Category::where('type', 'blog-tag')->orderBy('name', 'asc')->get();
        $categories = Category::where('type', 'blog-category')->orderBy('name', 'asc')->get();
        return view('backend.blog.edit', compact('blog', 'tags', 'categories'));
    }

    public function update(Request $request, Post $blog)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]
        );
        $input = $request->all();

        if ($blog->name != $input['name'] || $blog->slug != $input['slug']) {

            $blogExistedName = Post::where([
                ['name', $input['name']],
                ['id', '<>', $blog->id],
                ['type', 'blog'],
            ])->count();

            if ($blogExistedName) {
                return back()->withInput($request->input())->with(CommonEnum::MESSAGES['nameIsExisted']);
            }

            $blogExistedSlug = Post::where([
                ['slug', $input['slug']],
                ['id', '<>', $blog->id],
                ['type', 'blog'],
            ])->count();

            if ($blogExistedSlug) {
                return back()->withInput($request->input())->with(CommonEnum::MESSAGES['slugIsExisted']);
            }

        }

        $input['status'] = isset($input['status']) ? $input['status'] : null;
        $input['tag'] = isset($input['tag']) ? $input['tag'] : [];
        $input['cat'] = isset($input['cat']) ? $input['cat'] : [];
        $input['image'] = json_encode($input['image']);

        $blog->tag()->sync($input['tag']);
        $blog->category()->sync($input['cat']);

        $blog->update($input);


        return back()->with( CommonEnum::MESSAGES['success']);
    }

}
