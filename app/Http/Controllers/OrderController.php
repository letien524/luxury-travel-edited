<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Http\Requests\OrderRequest;
use App\Jobs\SendMailTourBookedToAdminJob;
use App\Jobs\SendMailTourBookedToCustomerJob;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class OrderController extends BaseController
{
    protected function getModel(): String
    {
        return 'order';
    }

    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth')->except('store');
        $this->middleware('role');
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $orders = Order::latest('id')->get(['id', 'name', 'email', 'phone', 'departure_date', 'created_at', 'status']);
            $datatables = Datatables::of($orders)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('email', function ($row) {
                    return $row->email;
                })
                ->addColumn('phone', function ($row) {
                    return $row->phone;
                })
                ->addColumn('departure_date', function ($row) {
                    return $row->departure_date;
                })
                ->addColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusOrder($row->status);
                    return $status;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->getModel()}.edit", $row);
                    $deleteRoute = route("{$this->getModel()}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action', 'status'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'email', 'name' => 'level', 'title' => TableEnum::LABELS['email']],
            ['data' => 'phone', 'name' => 'phone', 'title' => TableEnum::LABELS['phone']],
            ['data' => 'departure_date', 'name' => 'departure_date', 'title' => TableEnum::LABELS['departure_date']],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => TableEnum::LABELS['created_at']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.order.index', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required',
            'captcha' => 'required|captcha',
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput()
                ->with(['inquire.store' => 'errors']);
        }

        $input = $request->all();

        $today = Carbon::now();
        $departure_date = Carbon::parse($input['departure_date']);

        $input['departure_date'] = $departure_date->greaterThanOrEqualTo($today) ? $departure_date->format('Y-m-d') : $today->format('Y-m-d') ;
        $input['destination'] = isset($input['destination']) ? $input['destination'] : [];


        $order = Order::create($input);

        $order->destinations()->sync($input['destination']);

        //send mail
        $this->dispatch(new SendMailTourBookedToCustomerJob($order));
        $this->dispatch(new SendMailTourBookedToAdminJob($order));


        if (isset($input['tour_id'])) {
            return redirect()->route('tour.book.finish')->with([
                'inquireFinish' => true,
            ]);
        }

        return redirect()->route('inquire.finish')->with([
            'inquireFinish' => true,
        ]);
    }

    public function edit(Order $order)
    {
        $data = [
            'order' => $order,
        ];

        return view('backend.order.edit', $data);
    }

    public function update(Request $request, Order $order)
    {
        $order->update($request->all());
        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
