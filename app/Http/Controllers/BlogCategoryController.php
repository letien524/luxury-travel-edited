<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Category;
use App\Enum\TableEnum;
use App\Lang;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class BlogCategoryController extends BaseController
{
    private $__model = 'blogCategory';

    protected function getModel(): String
    {
        return 'category';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $data = Category::where('type', 'blog-category')->latest('id')->get(['id', 'name','slug', 'image']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })

                ->addColumn('slug', function ($row) {
                    return $row->slug;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $deleteRoute = route("{$this->__model}.destroy", $row);
                    $viewRoute = route("{$this->__model}.detail", $row->slug);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute, $viewRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'slug', 'name' => 'slug', 'title' => TableEnum::LABELS['slug']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);
        return view('backend.blogCategory.index', $data);
    }

    public function create()
    {
        return view('backend.blogCategory.create-edit');

    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]

        );

        $categoryNameExisted = Category::where([
            ['type','blog-category'],
            ['name',$request->input('name')],
        ])->count();

        $categorySlugExisted = Category::where([
            ['type','blog-category'],
            ['slug',$request->input('slug')],
        ])->count();

        if ($categoryNameExisted){
            return back() ->withInput($request->input())->with(CommonEnum::MESSAGES['categoryNameIsExisted']);
        }

        if ($categorySlugExisted){
            return back()->withInput($request->input())->with(CommonEnum::MESSAGES['categorySlugIsExisted']);
        }

        Category::create($request->all());

        return redirect()->route('blogCategory.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(Category $blogCategory)
    {
        $configs = [
            'action' => 'update',
        ];

        $data = [
            'configs' => $configs,
            'blogCategory' => $blogCategory,
        ];
        return view('backend.blogCategory.create-edit', $data);
    }

    public function update(Request $request, Category $blogCategory)
    {

        $this->validate($request,
            [
                'name' => 'required',
                'slug' => 'required',
            ],
            [
                'name.required' => 'Bạn chưa nhập tiêu đề',
                'slug.required' => 'Bạn chưa nhập đường dẫn',
            ]

        );

        if ($blogCategory->name != $request->input('name')) {
            $categoryNameExisted = Category::where([
                ['type','blog-category'],
                ['name',$request->input('name')],
            ])->count();

            if ($categoryNameExisted){
                return back() ->withInput($request->input())->with(CommonEnum::MESSAGES['categoryNameIsExisted']);
            }

        }
        if ($blogCategory->slug != $request->input('slug')) {
            $categorySlugExisted = Category::where([
                ['type','blog-category'],
                ['slug',$request->input('slug')],
            ])->count();

            if ($categorySlugExisted){
                return back()->withInput($request->input())->with(CommonEnum::MESSAGES['categorySlugIsExisted']);
            }

        }

        $blogCategory->update($request->all());

        return back()->with( CommonEnum::MESSAGES['success']);
    }
}
