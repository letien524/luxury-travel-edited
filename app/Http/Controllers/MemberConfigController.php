<?php

namespace App\Http\Controllers;

use App\Category;
use App\Enum\CommonEnum;
use Illuminate\Http\Request;

class MemberConfigController extends BaseController
{
    protected function getModel(): String
    {
        return 'category';
    }

    public function index()
    {

        $data = [
            'memberConfig' => Category::firstOrCreate(['type'=>'member-category-main']),
        ];
        return view('backend.memberConfig.index', $data);
    }

    public function update(Request $request, Category $memberConfig)
    {

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $memberConfig->update($input);

        return back()->with( CommonEnum::MESSAGES['success']);
    }
}
