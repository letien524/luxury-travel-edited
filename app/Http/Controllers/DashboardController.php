<?php

namespace App\Http\Controllers;

use DB;

class DashboardController extends BaseController
{
    protected function getModel(): String
    {
        return '';
    }

    public function index()
    {
        $tours = DB::table('tours')->count();
        $highlightTours = DB::table('tours')->where('highlight',1)->count();
        $shownTours = DB::table('tours')->where('status',1)->count();

        $orders = DB::table('orders')->count();
        $newOrders = DB::table('orders')->where('status', null)->count();
        $completedOrders = DB::table('orders')->where('status', 2)->count();

        $blogs = DB::table('posts')->where('type', 'blog')->count();
        $publicBlogs = DB::table('posts')->where([
            ['type', 'blog'],
            ['status', 1],
        ])->count();

        $contacts = DB::table('contacts')
            ->where('type','customer-contact')
            ->orWhere('type','customer-subscribe')
            ->count();

        $contactCustomers =  DB::table('contacts')
            ->where('type','customer-contact')
            ->count();

        $responseContactCustomers =  DB::table('contacts')
            ->where([
                ['type','customer-contact'],
                ['status',2],
            ])->count();

        $newContactCustomers = DB::table('contacts')->where([
            ['status', null],
            ['type','customer-contact'],
        ])->count();

        $contactSubscribe =  DB::table('contacts')
            ->where('type','customer-subscribe')
            ->count();

        $newContactSubscribes = DB::table('contacts')->where([
            ['status', null],
            ['type','customer-subscribe'],
        ])->count();


        $comments = DB::table('comments')->count();
        $newComments = DB::table('comments')
            ->where('status',null)
            ->count();
        $shownComments = DB::table('comments')
            ->where('status',1)
            ->count();

        $data = [
            'tours' => $tours,
            'highlightTours' => $highlightTours,
            'shownTours' => $shownTours,
            'orders' => $orders,
            'newOrders' => $newOrders,
            'completedOrders' => $completedOrders,
            'blogs' => $blogs,
            'publicBlogs' => $publicBlogs,
            'contacts' => $contacts,
            'newContactCustomers' => $newContactCustomers,
            'newContactSubscribes' => $newContactSubscribes,
            'responseContactCustomers' => $responseContactCustomers,
            'contactCustomers' => $contactCustomers,
            'contactSubscribe' => $contactSubscribe,
            'comments' => $comments,
            'newComments' => $newComments,
            'shownComments' => $shownComments,
        ];

        return view('backend.block.home', $data);
    }
}
