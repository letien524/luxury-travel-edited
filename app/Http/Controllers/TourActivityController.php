<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Enum\TableEnum;
use App\Faq;
use App\Http\Requests\TourActivityRequest;
use App\Member;
use App\ReviewCategory;
use App\TourActivity;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class TourActivityController extends BaseController
{

    protected function getModel(): String
    {
        return 'tourActivity';
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $activities = TourActivity::latest('id')->get(['id', 'name', 'image', 'slug', 'index', 'status']);
            $datatables = Datatables::of($activities)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route('tourActivity.edit', $row);
                    $name = getHtmlLabelTable($row, $editRoute);
                    return $name;
                })
                ->addColumn('slug', function ($row) {
                    return $row->slug;
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route('tourActivity.edit', $row);
                    $viewRoute = route('tourActivity.detail', $row->slug);
                    $deleteRoute = route('tourActivity.destroy', $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute, $viewRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action'])
                ->make(true);

            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'slug', 'name' => 'slug', 'title' => TableEnum::LABELS['slug']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];
        $data['datatables'] = $builder->columns($attributes);

        return view('backend.tourActivity.index', $data);
    }

    public function create()
    {
        $data = [
            'members' => Member::orderBy('name', 'asc')->get(['id', 'image', 'name', 'position']),
            'faqs' => Faq::get(CommonEnum::GET_DATA['id-name']),
            'reviewCategories' => ReviewCategory::orderBy('name', 'asc')->get(CommonEnum::GET_DATA['id-name']),
        ];

        return view('backend.tourActivity.create-edit', $data);
    }

    public function store(TourActivityRequest $request)
    {
        $input = $request->all();
        $input['content'] = json_encode($input['content']);
        TourActivity::create($input);
        return redirect()->route('tourActivity.index')->with(CommonEnum::MESSAGES['success']);
    }

    public function edit(TourActivity $tourActivity)
    {
        $configs['action'] = 'update';
        $data = [
            'configs' => $configs,
            'tourActivity' => $tourActivity,
            'members' => Member::latest('name')->get(['id', 'image', 'name', 'position']),
            'faqs' => Faq::get(CommonEnum::GET_DATA['id-name']),
            'reviewCategories' => ReviewCategory::latest('name')->get(CommonEnum::GET_DATA['id-name']),
        ];

        return view('backend.tourActivity.create-edit', $data);
    }

    public function update(TourActivityRequest $request, TourActivity $tourActivity)
    {

        $input = $request->all();

        $input['content'] = json_encode($input['content']);

        $input['status'] = isset($input['status']);
        $input['index'] = isset($input['index']);
        $tourActivity->update($input);

        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
