<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Exports\TourExport;
use App\Imports\TourImport;
use App\Tour;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TourImportController extends BaseController
{
    protected function getModel(): String
    {
        return '';
    }

    public function getImport()
    {
        return view('backend.tourImport.index');
    }

    public function import(Request $request)
    {
        $this->validate($request,
            [
                'fFile' => 'required'
            ]);


        Excel::import(new TourImport, $request->fFile);

        return back()->with(CommonEnum::MESSAGES['success']);
    }

    public function export()
    {
        $name = 'Luxury_Tours_' . \Carbon\Carbon::now()->format('Y-m-d-H-i-s') . '.xlsx';
        return Excel::download(new TourExport, $name);

    }

}
