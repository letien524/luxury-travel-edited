<?php

namespace App\Http\Controllers;

use App\Enum\CommonEnum;
use App\Contact;
use App\Enum\TableEnum;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class ContactCollectionController extends BaseController
{
    protected function getModel(): String
    {
        return 'contact';
    }

    private $__model = 'contactCollection';

    public function index(Builder $builder)
    {

        if (request()->ajax()) {
            $data = Contact::whereIn('type', [
                'customer-mail-collection-bottom',
                'customer-mail-collection-tour-plan',
                'customer-mail-collection'
            ])
                ->latest('id')
                ->get(['id', 'name', 'email', 'created_at', 'type','status']);
            $datatables = Datatables::of($data)
                ->addColumn('checkbox', function ($row) {
                    $checkbox = getHtmlCheckboxTable($row->id);
                    return $checkbox;
                })
                ->addColumn('id', function ($row) {
                    return $row->id;
                })
                ->addColumn('name', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $name = getHtmlLabelContactTable($row->name, $editRoute);
                    return $name;
                })
                ->addColumn('email', function ($row) {
                    return $row->email;
                })
                ->addColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->addColumn('type', function ($row) {
                    return $row->type;
                })
                ->addColumn('status', function ($row) {
                    $status = getHtmlStatusContact($row->status);
                    return $status;
                })

                ->addColumn('action', function ($row) {
                    $editRoute = route("{$this->__model}.edit", $row);
                    $deleteRoute = route("{$this->__model}.destroy", $row);

                    $btn = getHtmlActionButton($editRoute, $deleteRoute);

                    return $btn;
                })
                ->rawColumns(['checkbox', 'name', 'action','status'])
                ->make(true);
            return $datatables;
        }

        $attributes = [
            ['data' => 'checkbox', 'name' => 'checkbox', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['checkbox']],
            ['data' => 'id', 'name' => 'id', 'title' => TableEnum::LABELS['id']],
            ['data' => 'name', 'name' => 'name', 'title' => TableEnum::LABELS['name']],
            ['data' => 'email', 'name' => 'email', 'title' => TableEnum::LABELS['email']],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => TableEnum::LABELS['created_at']],
            ['data' => 'type', 'name' => 'type', 'title' => TableEnum::LABELS['type']],
            ['data' => 'status', 'name' => 'status', 'title' => TableEnum::LABELS['status']],
            ['data' => 'action', 'name' => 'action', 'searchable' => false, 'orderable' => false, 'title' => TableEnum::LABELS['actions']],
        ];

        $data['datatables'] = $builder->columns($attributes);

        return view('backend.contactCollection.index', $data);
    }

    public function edit(Contact $contactCollection)
    {
        $data['contactCollection'] = $contactCollection;
        $data['configs'] = ['action'=>'update'];
        return view('backend.contactCollection.create-edit', $data);
    }


    public function update(Request $request, Contact $contactCollection)
    {
        $input = $request->all();
        $input['status'] = !empty($input['status']) ? $input['status'] : null;
        $contactCollection->update($input);
        return back()->with(CommonEnum::MESSAGES['success']);
    }
}
