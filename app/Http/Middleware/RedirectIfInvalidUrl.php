<?php

namespace App\Http\Middleware;

use App\Redirect;
use Closure;

class RedirectIfInvalidUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        config(['missing-page-redirector.redirects' => array_merge(config('missing-page-redirector.redirects'),$this->getRedirectsFor())]);
        return $next($request);
    }

    public function getRedirectsFor()
    {
        return Redirect::where('status',1)->get()->flatMap(function($redirect) {
            return [$redirect->from => $redirect->to];
        })->toArray();
    }

}
