<?php

namespace App\Http\Middleware;

use App\Enum\CommonEnum;
use Closure;
use Illuminate\Contracts\Auth\Guard;


class IsSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if(!isLevel(1)){
            return redirect()->route('dashboard.index')->with(CommonEnum::MESSAGES['permissionDenied']);
        }
        return $next($request);
    }
}
