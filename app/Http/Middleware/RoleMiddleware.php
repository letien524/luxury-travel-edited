<?php

    namespace App\Http\Middleware;

    use Closure;

    class RoleMiddleware
    {
        private $commonRoute = [
            'dashboard.index',
            'login',
            'logout',
            'repeat.email',
            'repeat.email-contact',
            'repeat.email-subscribe',
            'repeat.email-comment',
            'repeat.about',
            'repeat.category',
            'repeat.faq.content',
            'repeat.home',
            'repeat.home-tripadvisor',
            'repeat.inquire-hotel',
            'repeat.tour-duration',
            'repeat.tour-price',
            'repeat.tour.hotelList',
            'repeat.tour.hotelType',
            'repeat.tour.plan',
            'repeat.popup',
            'repeat.popup-collection',
            'repeat.popup-collection-corner',
        ];

        private $actions = [
            'index',
            'create',
            'store',
            'edit',
            'update',
            'bulkDestroy',
            'destroy',
        ];


        /**
         * Handle an incoming request.
         *
         * @param \Illuminate\Http\Request $request
         * @param \Closure $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            $getCurrentRouteName = getCurrentRouteName();

            $users = [
                'page',
                'blog',
                'blogTag',
                'tour',
                'user',
            ];

            $tourManagers = array_merge($users, [
                'tourDestination',
                'tourActivity',
                'faq',
                'review',
                'reviewCategory',
            ]);

            $saleManagers = array_merge($tourManagers, [
                'order',
                'comment',
                'contact',
                'contactAddress',
                'contactCollection',
                'contactSubscribe',
            ]);

            $userRoutes = $this->loopRouteName($users);
            $tourManagerRoutes = $this->loopRouteName($tourManagers);
            $saleManagerRoutes = $this->loopRouteName($saleManagers);


            if ((isLevel(0) || isLevel(null)) && in_array($getCurrentRouteName, $userRoutes)) {
                return $next($request);

            } elseif (isLevel(2) && in_array($getCurrentRouteName, $tourManagerRoutes)) {
                return $next($request);

            } elseif (isLevel(3) && in_array($getCurrentRouteName, $saleManagerRoutes)) {
                return $next($request);

            } elseif (isDeveloper() || isLevel(1)) {
                return $next($request);

            }
            return abort(403);

        }

        private function loopRouteName($modules)
        {

            $actions = $this->actions;

            $array = array_map(function ($module) use ($actions) {
                return array_map(function ($action) use ($module) {
                    return "$module.$action";
                }, $actions);
            }, $modules);


            $flatten = [];
            foreach ($array as $item) {
                $flatten = array_merge($item, $flatten);
            }

            return array_merge($flatten, $this->commonRoute);
        }
    }
