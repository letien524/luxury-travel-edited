<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TourActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:tour_activities,name,' . $this->segment(3),
            'slug' => 'required|unique:tour_activities,slug,' . $this->segment(3),
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tiêu đề',
            'name.unique' => 'Tiêu đề đã tồn tại',
            'slug.required' => 'Bạn chưa nhập đường dẫn',
            'slug.unique' => 'Đường dẫn đã tồn tại',
        ];
    }
}
