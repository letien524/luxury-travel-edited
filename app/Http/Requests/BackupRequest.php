<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BackupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $method = $this->getMethod();

        switch ($method) {
            case 'POST':
                return [
                    'import_file' => 'required|mimes:zip',
                ];
            case 'PUT':
                return [
                    'import_file' => 'required',
                ];
            default:
                return [];
        }

    }
}
