<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['name', 'slug', 'image', 'content_short', 'content', 'type', 'status', 'parent_id', 'lang_id', 'order_menu', 'meta_title', 'meta_description'];

    public function lang()
    {
        return $this->belongsTo('App\Lang');
    }
    public function faq()
    {
        return $this->hasMany(Post::class, 'faq_id');
    }

    public function primaryPost()
    {
        return $this->find($this->parent_id);
    }

    public function hasChild()
    {
        return $this->where('parent_id', $this->id)->count();
    }

    public function tag()
    {
        return $this->belongsToMany(Category::class, 'blog_tag');
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'blog_category','post_id','category_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}

