<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $fillable = ['name', 'city_id', 'type_id', 'room_type_id'];

    public function city()
    {
        return $this->belongsTo(HotelCity::class, 'city_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(HotelType::class, 'type_id', 'id');
    }

    public function roomType()
    {
        return $this->belongsTo(HotelRoomType::class, 'room_type_id', 'id');
    }

    public function tours()
    {
        return $this->belongsTo(Tour::class);
    }
}
