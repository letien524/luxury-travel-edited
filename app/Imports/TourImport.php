<?php

namespace App\Imports;

use App\Tour;
use Maatwebsite\Excel\Concerns\ToModel;

class TourImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
        $key = [
            "id",
            "name",
            "slug",
            "image",
            "duration",
            "excerpt",
            "content",
            "map",
            "plan",
            "hotel",
            "file",
            "status",
            "highlight",
            "parent_id",
            "lang_id",
            "category_id",
            "faq_id",
            "destination_id",
            "price",
            "price_promotion",
            "meta_title",
            "meta_description",
            "canonical",
            "index",
            "created_at",
            "updated_at",
        ];

        $data = array_combine($key, $row);

        return Tour::firstOrCreate(['id' => $row[0]], $data);
    }
}
