<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['name', 'address', 'phone', 'hotline', 'email', 'content', 'type', 'status', 'order_menu'];
}
