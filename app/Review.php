<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['name', 'reviewer', 'image', 'country', 'content', 'rate', 'order_menu', 'submited_at', 'display'];

    protected $casts = [
        'display' => 'bool'
    ];

    public function categories()
    {
        return $this->belongsToMany(ReviewCategory::class, 'category_review', 'review_id', 'category_id');
    }

    public function members()
    {
        return $this->belongsToMany(Member::class, 'member_review');
    }

    public function memberTours()
    {
        return $this->belongsToMany(Tour::class, 'review_tour_member');
    }
}




