<?php

use App\Enum\TableEnum;
use App\Enum\CommonEnum;
use App\Enum\OrderEnum;
use App\Enum\CommentEnum;
use App\Enum\RoleEnum;
use App\Enum\ContactEnum;
use Illuminate\Support\Str;

function getHtmlStatusOrder($value)
{
    $value = (int)$value;
    if ($value & 1) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['primary'];
    } elseif ($value & 2) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['success'];
    } else {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['default'];
    }

    $content = OrderEnum::STATUS[$value];
    $html = '<span class="btn__status ' . $class . '">' . $content . '</span>';
    return $html;
}

function getHtmlStatusContact($value)
{
    $value = (int)$value;
    if ($value == 1) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['primary'];
    } elseif ($value == 2) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['success'];
    } elseif ($value == -1) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['default'];
    } else {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['danger'];
    }

    $content = ContactEnum::STATUS[$value];
    $html = '<span class="btn__status ' . $class . '">' . $content . '</span>';
    return $html;
}

function getHtmlStatusCommentTable($value)
{
    $value = (int)$value;

    if ($value & 1) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['primary'];
    } elseif ($value & 2) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['default'];
    } else {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['danger'];
    }

    $content = CommentEnum::STATUS[$value];
    $html = '<span class="btn__status ' . $class . '">' . $content . '</span>';
    return $html;
}

function getHtmlStatusTourTable($value, $field)
{
    $value = (int)$value;
    $class = getHtmlClass($value);
    $content = TableEnum::FIELDS[$field][$value];
    $html = '<span class="btn__status ' . $class . '">' . $content . '</span>';
    return $html;
}

function getHtmlUserRole($value, $id = null)
{
    $value = (int)$value;
    if ($value == 1) {
        if ($id == 1) {
            $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['danger'];
        } else {
            $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['primary'];
        }
    } elseif ($value == 2) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['success'];
    } elseif ($value == 3) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['info'];
    } else {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['default'];
    }
    if ($id == 1 && $value == 1) {
        $content = TableEnum::FIELDS['level'][9];
    } else {
        $content = TableEnum::FIELDS['level'][$value];
    }


    $html = '<span class="' . $class . '">' . $content . '</span>';

    return $html;
}


function getHtmlActionButton($editRoute, $deleteRoute, $viewRoute = null)
{
    $btn[] = Str::length($viewRoute) ? '<a href="' . $viewRoute . '" title="' . TableEnum::LABELS['view'] . '" class="edit btn btn-default btn-xs" target="_blank"><i class="fa fa-eye"></i></a>' : '';
    $btn[] = Str::length($editRoute) ? '<a href="' . $editRoute . '" title="' . TableEnum::LABELS['edit'] . '" class="edit btn btn-info btn-xs"><i class="fa fa-pencil-square-o"></i></a>' : '';
    $btn[] = Str::length($deleteRoute) ? '<a href="' . $deleteRoute . '" data-toggle="modal" data-target="#deleteRowConfirmModal" title="' . TableEnum::LABELS['delete'] . '" class="edit btn btn-danger btn-xs" onclick="deleteRowConfirmModalHandle(this)"><i class="fa fa-trash-o"></i></a>' : '';
    return implode(' ', $btn);
}

function getHtmlActionButtonTour($editRoute, $deleteRoute, $viewRoute = null, $duplicateRoute = null)
{
    $btn[] = Str::length($viewRoute) ? '<a href="' . $viewRoute . '" title="' . TableEnum::LABELS['view'] . '" class="edit btn btn-default btn-xs" target="_blank"><i class="fa fa-eye"></i></a>' : '';
    $btn[] = Str::length($duplicateRoute) ? '<a href="' . $duplicateRoute . '" title="' . TableEnum::LABELS['duplicate'] . '" class="edit btn btn-success btn-xs"><i class="fa fa-copy"></i></a>' : '';
    $btn[] = Str::length($editRoute) ? '<a href="' . $editRoute . '" title="' . TableEnum::LABELS['edit'] . '" class="edit btn btn-info btn-xs"><i class="fa fa-pencil-square-o"></i></a>' : '';
    $btn[] = Str::length($deleteRoute) ? '<a href="' . $deleteRoute . '" data-toggle="modal" data-target="#deleteRowConfirmModal" title="' . TableEnum::LABELS['delete'] . '" class="edit btn btn-danger btn-xs" onclick="deleteRowConfirmModalHandle(this)"><i class="fa fa-trash-o"></i></a>' : '';
    return implode(' ', $btn);
}

function getHtmlLabelTable($data, $route)
{
    $name = Str::limit($data->name, 50);
    $image = '<img src="' . image_url(@$data->image) . '" class="img-responsive imglist"/>';
    return "<a class='datatable__name' href='$route' title='$data->name'>$image  $name</a>";
}

function getHtmlBlogLabelTable($data, $route)
{
    $name = Str::limit($data->name, 50);
    $data['image'] = json_decode($data['image'], true);
    $image = '<img src="' . image_url(@$data['image']['thumbnail']) . '" class="img-responsive imglist"/>';
    return "<a class='datatable__name' href='$route' title='$data->name'>$image  $name</a>";
}

function getHtmlContentCommentTable($data, $route)
{
    $content = Str::limit($data->content, 50);
    return "<a class='datatable__name' href='$route' title='$data->content'>$content</a>";
}

function getHtmlLabelContactTable($name, $route)
{
    $content = Str::limit($name, 50);
    return "<a class='datatable__name' href='$route' title='$name'>$content</a>";
}

function getHtmlContentRedirectTable($data, $route)
{
    return "<a class='datatable__name' href='$route' title='$data'>$data</a>";
}


function getHtmlCheckboxTable($value)
{
    return "<input type='checkbox' name='chkItem[]'  value='$value' />";
}

function getHtmlClass($value)
{
    if ($value & 1) {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['primary'];
    } else {
        $class = 'label label-' . CommonEnum::TEXT_COLOR_CLASS['default'];
    }

    return $class;
}

function getExtraRoute($route, $model, $controller)
{
    $routeMethod = $route['routeMethod'];
    $routeName = "$model.$routeMethod";
    $routeUrl = $route['routeUrl'];
    $controllerAction = "$controller@$routeMethod";
    switch ($routeMethod) {
        case 'getImport':
        case 'export':
        case 'index':
            return Route::get($routeUrl, $controllerAction)->name($routeName);
            break;

        case 'edit':
            $routeUrl = "{{$model}}/edit";
            return Route::get($routeUrl, $controllerAction)->name($routeName);
            break;

        case 'update':
            $routeUrl = "{{$model}}";
            return Route::put($routeUrl, $controllerAction)->name($routeName);
            break;

        case 'create':
            $routeUrl = "create";
            return Route::get($routeUrl, $controllerAction)->name($routeName);
            break;

        case 'import':
        case 'store':
            return Route::post($routeUrl, $controllerAction)->name($routeName);
            break;

        case 'delete':
            $routeUrl = "{{$model}}";
            return Route::delete($routeUrl, $controllerAction)->name($routeName);
            break;

        default:
            return null;
            break;
    }
}

function getBackupPath($fileName = null)
{
    $disk = backupDisk();
    $getPathPrefix = $disk->getAdapter()->getPathPrefix();
    return $getPathPrefix . $fileName;
}


function backupDisk()
{
    return \Illuminate\Support\Facades\Storage::disk('backup');
}

function isUpdateBlade($type)
{
    return $type == 'update';
}

function cleanArray($array)
{
    $array = is_array($array) ? $array : (array)$array;
    return array_filter($array, function ($item) {
        return !empty($item);
    });
}

function getCurrentRouteName()
{
    return request()->route()->getName();
}

function isCurrentInPath($path)
{
    $currentPatch = request()->path();
    $check = explode($path, $currentPatch);

    return count($check) > 1;
}

function isPopupDisplay($name = null, $popups = [])
{
    if (@$popups[$name]['display'] == 1) {
        return true;
    }
    return false;
}

function hasBlackList($name = null, $popups = [])
{
    if (count((array)@$popups[$name]['black_list'])) {
        return true;
    }
    return false;
}

function isCurrentPageInBlackList($blackList = [])
{
    $currentPath = request()->path();
    if (in_array($currentPath, $blackList)) {
        return true;
    }
    return false;
}

function getAdminAsset($path, $prefix = 'backend')
{
    return asset("{$prefix}/{$path}");
}

function getAsset($path, $prefix = 'frontend')
{
    return asset("{$prefix}/{$path}");
}

function image_url($url = null)
{
    return strlen($url) ? url($url) : __IMAGE_THUMBNAIL_DEFAULT;
}

function isEMail($email = null)
{
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function fluginBaseUrl()
{
    return url('');
}

function getConfig($type = 'null')
{
    return \App\Config::where('type', $type)->first();
}

function isDeveloper()
{
    $user = auth()->user();
    if ($user['id'] == 1 && $user['level'] == 1)
        return true;
    return false;
}

function isCurrentModelSlug($args, $children = false)
{
    $currentModelSlug = request()->segment(2);
    if ($children) {
        $args = explode('.', $args);
        return in_array($currentModelSlug, $args);
    }

    if ($args == 'config') {
        return strpos($currentModelSlug, ucfirst($args)) > -1;
    }

    return strpos($currentModelSlug, $args) > -1;
}

function getAuthField($field)
{
    return @auth()->user()->{$field};
}

function isAdmin()
{
    if (getAuthField('level') == 1 && getAuthField('name') == 'gco_admin') {
        return true;
    }
    return false;
}

function roleValidate($role)
{
    $role = (array)$role;
    $user = auth()->user();
    $userLevel = (int)$user->level;
    $userRole = getUserRole($userLevel);
    $result = in_array($userRole, $role) || count($role) === 0;
    return $result;
}

function getUserRole($level)
{
    $level = (int)$level;
    return RoleEnum::ROLE[$level] ?? 0;
}

function isLevel($level)
{
    $user = auth()->user();
    if ($user['level'] == $level)
        return true;
    return false;
}

function adminMegaMenu($menu, $result = null)
{
    $menu = (array)$menu;
    $result .= "<ol class='dd-list'>";
    if (count($menu)) {
        foreach ($menu as $key => $item) {
            $id = @$item['id'];
            $name = @$item['name'];
            $link = @$item['link'];
            $destination = @$item['destination'];
            $children = (array)@$item['children'];

            $result .= "<li class='dd-item' data-id='$id' data-name='$name' data-link='$link' data-destination='$destination'>";
            $result .= "<div class='dd-handle'>$name</div>";
            $result .= "<div class='dd-action'>";
            $result .= "<a href='#' data-target='#modal-default' data-toggle='modal' title='$link' data-action='itemEdit' class='text-primary'><span>" . str_limit($link, 30) . "</span></a> | ";

            if ($destination === 'is_destination') {
                $result .= "<a href='#' data-target='#modal-default' data-toggle='modal' title='Destination' data-action='itemEdit' class='text-primary'><span class='badge label-warning'>Destination</span></a> | ";
            }

            if ($destination === 'is_activity') {
                $result .= "<a href='#' data-target='#modal-default' data-toggle='modal' title='Activity' data-action='itemEdit' class='text-primary'><span class='badge label-primary'>Activity</span></a> | ";
            }

            $result .= "<a href='#' data-target='#modal-default' data-toggle='modal' title='Sửa' data-action='itemEdit' class='text-primary'><i class='fa fa-pencil-square-o'></i></a> | ";
            $result .= "<a href='javascript:void(0)' title='Xóa' data-action='itemDelete' class='text-danger'><i class='fa fa-trash-o'></i></a>";
            $result .= "</div>";

            if (count($children)) {
                $result .= adminMegaMenu($children);
            }
            $result .= "</li>";

        }
    }
    $result .= "</ol>";

    return $result;
}

if (!function_exists('url_query_render')) {

    function url_query_render($key_filter = null, $value = null)
    {
        $prefix = '?';
        $url_current = url()->full();
        $url_query = explode('?', $url_current);

        if (isset($url_query[1])) {

            $url_query_list = explode('&', $url_query[1]);

            $key_filter_list = [];
            $value_filter_list = [];
            foreach ($url_query_list as $key => $item) {
                $item_array = explode('=', $item);
                $key_filter_list[$key] = $item_array[0];
                $value_filter_list[$key] = $item_array[1];
            }

            if (in_array($key_filter, $key_filter_list)) {
                $key_filter_index = array_search($key_filter, $key_filter_list);
                unset($url_query_list[$key_filter_index]);


                if (in_array($value, $value_filter_list)) {
                    $value_filter_index = array_search($value, $value_filter_list);
                    if ($value_filter_index === $key_filter_index) {

                        if (count($url_query_list) === 0) {
                            return [
                                'url' => url()->current(),
                                'active' => true
                            ];
                        }

                        return [
                            'url' => url()->current() . $prefix . implode('&', $url_query_list),
                            'active' => true
                        ];
                    }
                }

            }
            if (!empty($value)) {
                array_push($url_query_list, $key_filter . '=' . $value);
            }

            return [
                'url' => url()->current() . $prefix . implode('&', $url_query_list),
                'active' => false
            ];

        }

        return [
            'url' => url()->current() . $prefix . $key_filter . '=' . $value,
            'active' => false
        ];

    }

}

if ((!function_exists('file_delete'))) {
    function file_delete($url)
    {
        return isset($url) && strlen($url) ? Storage::delete($url) : false;
    }
}

if ((!function_exists('directory_delete'))) {
    function directory_delete($dir)
    {
        return Storage::has($dir) ? Storage::deleteDirectory($dir) : false;
    }
}


if ((!function_exists('image_upload'))) {
    function image_upload($request, $input = null, $old_image = null, $storage_url = __STORAGE_PUBLIC)
    {
        $file = $request->file($input);
        if (!empty($file)) {
            $time = (int)round(microtime(true) * 1000);
            $file_name = date('Y-m-d_', time()) . $time . '_' . $file->getClientOriginalName();

            file_delete($old_image);
            $image = Storage::putFileAs($storage_url, $file, $file_name);

            return $image;
        }
        return false;
    }
}


if ((!function_exists('image_upload_multi'))) {
    function image_upload_multi($request, $input = null, $old_image = null, $storage_url = __STORAGE_PUBLIC)
    {
        $files = $request->file($input);

        if (!empty($files)) {
            $image_list = [];
            foreach ($files as $key => $file) {
                $time = (int)round(microtime(true) * 1000);
                $file_name = date('Y-m-d_' . time()) . $time . '_' . $file->getClientOriginalName();
                file_delete($old_image);
                $image_list[$key] = Storage::putFileAs($storage_url, $file, $file_name);

            }
            return $image_list;

        }
        return false;
    }
}


if (!function_exists('video_embed_url')) {
    function video_embed_url($url = null)
    {
        if (isset($url)) {
            $url_array = [];
            if (strpos($url, 'youtube.com') !== false) {

                if (strpos($url, '&list') !== false) {
                    $url = explode('&list', $url);
                    $url = reset($url);
                }

                $url_array = explode('=', $url);

            } elseif (strpos($url, 'youtu.be/') !== false) {
                $url_array = explode('youtu.be/', $url);
            }
            if (count($url_array) > 1) {
                return __YOUTUBE_EMBED_PREFIX . end($url_array);
            }
            return $url;
        }
        return false;
    }
}




