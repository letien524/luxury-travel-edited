<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Redirect;


class DatabaseRedirector extends ServiceProvider
{

    public function getRedirectsFor()
    {
        return Redirect::where('status',1)->get()->flatMap(function($redirect) {
            return [$redirect->from => $redirect->to];
        })->toArray();
    }

    public function boot()
    {

        config(['missing-page-redirector.redirects' => array_merge(config('missing-page-redirector.redirects'),$this->getRedirectsFor())]);

    }


    public function register()
    {

    }
}
