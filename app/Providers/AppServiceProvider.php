<?php

    namespace App\Providers;

    use App\Brand;
    use App\Category;
    use App\Config;
    use App\Contact;
    use App\Popup;
    use App\Tour;
    use App\Lang;
    use App\TourActivity;
    use App\TourDestination;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Support\ServiceProvider;
    use Session;


    class AppServiceProvider extends ServiceProvider
    {
        /**
         * Bootstrap any application services.
         *
         * @return void
         */


        public function boot()
        {
            $this->init();
        }

        /**
         * Register any application services.
         *
         * @return void
         */
        public function register()
        {
            //Const
            $this->loadEnum();

            //Helpers
            $this->loadHelpers();
        }

        protected function loadHelpers()
        {
            require_once app_path(). '/Helpers/helper.php';
        }

        protected function loadEnum()
        {
            require_once app_path(). '/Enum/const.php';
        }

        protected function init(){

            Validator::extend('recaptcha', 'App\Validators\ReCaptcha@validate');

            view()->composer('*', function ($view) {
                $view->with([
                    'currentLang' => config('app.locale'),
                ]);
            });

            view()->composer('frontend.components.filter', function ($view) {

                $tourFilters = Config::where('type', 'tour-filter')->first();
                if (empty($tourFilters)) {
                    return abort(404, 'Chưa cấu hình bộ lọc tour. "Tours -> Cấu hình lọc tour"');
                }

                $view->with([
                    'tourFilters' => $tourFilters,
                ]);

            });

            view()->composer(['frontend.components.make_enquiry','frontend.components.banner'], function ($view) {

                $config_general = Config::where('type', 'general')->first();

                $view->with([
                    'config_general' => $config_general,
                ]);
            });

            view()->composer('frontend.master', function ($view) {

                $config_general = Config::where('type', 'general')->first();
                $config_social = Config::where('type', 'social')->first();
                $config_footer_menu = Config::where('type', 'footer_menu')->first();
                $locales = Contact::where('type', 'address')
                    ->orderBy('id', 'desc')
                    ->orderBy('order_menu', 'desc')
                    ->get();
                $langs = Lang::orderBy('name', 'asc')->get();
                $associationBrands = Brand::where('type', 'association')->orderBy('id', 'desc')->get();
                $awardBrands = Brand::where('type', 'awards')->orderBy('id', 'desc')->get();
                $newsBrands = Brand::where('type', 'news')->orderBy('id', 'desc')->get();

                $destinationMegaMenus = TourDestination::with(['parent','child' => function($query){
                    $query->where('status', 1)->orderBy('order_menu','desc');
                }])->where('status', 1)->where(function($query){
                    $query->where('parent_id', 0)->orWhere('parent_id', null);
                })->orderBy('order_menu', 'desc')->get();

                $activityMegaMenus = TourActivity::where('status', 1)->orderBy('order_menu', 'desc')->orderBy('name', 'asc')->get();

                $configTourMenuDestination = Category::where('type', 'activity-contact-main')->first();

                $tourHighlights = Tour::where([
                    ['status', 1],
                    ['highlight', 1],
                ])->orderBy('updated_at', 'desc')->take(6)->get();

                $travelAgent = Category::where('type','travel-agent-category-main')->first();

                $mainMenu = Config::firstOrCreate(['type'=>'menu']);

                $popups = Popup::where('is_active', 1)->orderBy('order_menu')->get();

                $view->with([
                    'config_general' => $config_general,
                    'config_social' => $config_social,
                    'config_footer_menu' => $config_footer_menu,
                    'locales' => $locales,
                    'langs' => $langs,
                    'associationBrands' => $associationBrands,
                    'awardBrands' => $awardBrands,
                    'newsBrands' => $newsBrands,
                    'activityMegaMenus' => $activityMegaMenus,
                    'destinationMegaMenus' => $destinationMegaMenus,
                    'tourHighlights' => $tourHighlights,
                    'travelAgent' => $travelAgent,
                    'mainMenu' => $mainMenu,
                    'popups' => $popups,
                    'configTourMenuDestination' => $configTourMenuDestination,
                ]);
            });
        }
    }
