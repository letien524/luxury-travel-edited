<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'slug', 'type', 'parent_id', 'meta_title', 'meta_description', 'lang_id', 'excerpt', 'content', 'status', 'is_display', 'is_best', 'is_inquire', 'order_menu', 'image', 'canonical', 'faq_id', 'member_id', 'index'];

    protected $casts = [
        'index' => 'bool'
    ];

    public function getChild()
    {
        return $this->where('parent_id', $this->id);
    }

    public function hasChild()
    {
        return $this->getChild()->count();
    }

    public function hasParent()
    {
        $parent = $this->parent_id;
        if (is_null($parent)) {
            return false;
        }
        return true;
    }

    public function getParent()
    {

        return $this->find($this->parent_id);
    }

    public function blogTag()
    {
        return $this->belongsToMany(Post::class, 'blog_tag');
    }

    public function blogCategory()
    {
        return $this->belongsToMany(Post::class, 'blog_category');
    }

    public function tourDestinations()
    {
        return $this->belongsToMany(Tour::class, 'destination_tour', 'category_id', 'tour_id');
    }

    public function tourActivityMain()
    {
        return $this->hasMany(Tour::class, 'category_id', 'id');
    }

    public function tourActivities()
    {
        return $this->belongsToMany(Tour::class, 'activity_tour', 'category_id', 'tour_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_category');
    }

    public function faq()
    {
        return $this->belongsTo(Faq::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function destinationReviews()
    {
        return $this->belongsToMany(ReviewCategory::Class, 'destination_review', 'category_id', 'review_id');
    }

    public function activitiesReviews()
    {
        return $this->belongsToMany(ReviewCategory::Class, 'category_review', 'category_id', 'review_id');
    }

}
