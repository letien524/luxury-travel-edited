<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ['name', 'slug', 'image', 'position', 'excerpt', 'quote', 'content', 'order_menu', 'status', 'meta_title', 'meta_description'];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function reviews()
    {
        return $this->belongsToMany(Review::class, 'member_review');
    }

    public function faqs()
    {
        return $this->belongsToMany(Faq::class, 'faq_member');
    }

    public function tours()
    {
        return $this->belongsToMany(Tour::class, 'member_tour');
    }
}
