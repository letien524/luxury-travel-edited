<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'website', 'content', 'type', 'status', 'post_id'];

    public function blog()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
