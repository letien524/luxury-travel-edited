<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomType extends Model
{
    protected $fillable = ['name'];

    public function hotels()
    {
        return $this->hasMany(Hotel::class, 'room_type_id', 'id');
    }
}
