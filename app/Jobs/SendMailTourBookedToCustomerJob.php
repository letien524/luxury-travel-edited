<?php

namespace App\Jobs;

use App\Mail\TourBookedCustomer;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailTourBookedToCustomerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,JobTrait;

    protected $order;

    public function __construct(Order $order)
    {
       $this->order = $order;
        $this->initMailConfig();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;

        Mail::to($order['email'])
            ->send(new TourBookedCustomer($order));
    }
}
