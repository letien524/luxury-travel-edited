<?php

namespace App\Jobs;

use App\Contact;
use App\Mail\SendMailHasTourPlan;
use App\Tour;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailHasTourPlanJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,JobTrait;

   protected $tour, $contact;

    public function __construct(Tour $tour, Contact $contact)
    {
        $this->initMailConfig();
        $this->tour = $tour;
        $this->contact = $contact;
    }


    public function handle()
    {
       $test = Mail::to($this->contact->email)->send(new SendMailHasTourPlan($this->tour, $this->contact));

        file_delete("tour-plans/{$this->tour->slug}.pdf");
    }
}
