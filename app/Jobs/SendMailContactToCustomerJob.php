<?php

namespace App\Jobs;

use App\Contact;
use App\Mail\ContactSubmitCustomer;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailContactToCustomerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, JobTrait;

   protected $contact;

    public function __construct(Contact $contact)
    {
        $this->initMailConfig();
        $this->contact = $contact;
    }


    public function handle()
    {
        $contact = $this->contact;
        $mailTo = $contact->email;

        Mail::to($mailTo)->send(new ContactSubmitCustomer($contact));
    }
}
