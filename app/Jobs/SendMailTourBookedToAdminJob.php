<?php

namespace App\Jobs;

use App\Mail\TourBooked;
use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailTourBookedToAdminJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, JobTrait;

    protected $order;

    public function __construct(Order $order)
    {
       $this->initMailConfig();
       $this->order = $order;
    }


    public function handle()
    {
        $order = $this->order;
        $mail = getConfig('email');
        $mail = json_decode($mail['content'],true);

        $bcc = (array) @$mail['tour']['bcc'];

        $mailTo = isEMail(@$mail['tour']['mail_to']) ? $mail['tour']['mail_to'] : config('mail.username');

        Mail::to($mailTo)
            ->cc($bcc)
            ->send(new TourBooked($order));
    }
}
