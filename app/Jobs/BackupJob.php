<?php

namespace App\Jobs;

use App\Enum\BackupEnum;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BackupJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        self::createSeeder();
        self::createBackup();
    }

    public static function createSeeder(){
        Log::info(BackupEnum::MESSAGES['beginCreateSeeder']);
        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();
        $tables = implode(',', $tables);

        Artisan::call('iseed', [
            'tables' => $tables,
            '--chunksize' => 50,
            '--force' => true,
            '--dumpauto' => true
        ]);
        Log::info(BackupEnum::MESSAGES['endCreateSeeder']);
    }

    public static function createBackup(){
        Log::info(BackupEnum::MESSAGES['beginCreateBackup']);
        //Artisan::call('backup:run', ['--disable-notifications' => true]);
        $lang = app()->getLocale();
        $time = \Carbon\Carbon::now()->format('Y-m-d_H-i-s');
        $fileName = "backup_{$lang}_{$time}.zip";
        $file = getBackupPath($fileName);

        Zipper::make($file)->folder('database/seeds')->add(database_path('seeds'))->close();
        //Zipper::make($file)->folder('public/uploads')->add(public_path('uploads'))->close();
        Log::info(BackupEnum::MESSAGES['endCreateBackup']);
    }


}
