<?php

namespace App\Jobs;

use App\Comment;
use App\Mail\CommentSubmit;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailCommentToAdminJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, JobTrait;

    protected $comment;
    public function __construct(Comment $comment)
    {
        $this->initMailConfig();
        $this->comment = $comment;
    }


    public function handle()
    {
        $comment = $this->comment;

        $mail = getConfig('email');
        $mail = json_decode($mail['content'],true);

        $bcc = (array) @$mail['comment']['bcc'];

        $mailTo = isEMail(@$mail['comment']['mail_to']) ? $mail['comment']['mail_to'] : config('mail.username');

        Mail::to($mailTo)
            ->cc($bcc)
            ->send(new CommentSubmit($comment));
    }
}
