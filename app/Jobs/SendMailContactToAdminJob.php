<?php

namespace App\Jobs;

use App\Contact;
use App\Mail\ContactSubmit;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailContactToAdminJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, JobTrait;

   protected $contact;

    public function __construct(Contact $contact)
    {
        $this->initMailConfig();
        $this->contact = $contact;
    }


    public function handle()
    {
        $contact = $this->contact;

        $mail = getConfig('email');
        $mail = json_decode($mail['content'],true);

        $bcc = (array) @$mail['contact']['bcc'];

        $mailTo = isEMail(@$mail['contact']['mail_to']) ? $mail['contact']['mail_to'] : config('mail.username');

        Mail::to($mailTo)->cc($bcc)->send(new ContactSubmit($contact));
    }
}
