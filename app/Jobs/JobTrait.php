<?php
namespace App\Jobs;

trait JobTrait
{
    public function initMailConfig(){
        $email = getConfig('email');
        $email = json_decode($email['content'],true);
        $smtp = @$email['smtp'];

        config()->set('mail.from.name',strlen(@$smtp['name']) ? $smtp['name'] : config('mail.from.name'));
        config()->set('mail.username',filter_var(@$smtp['username'],FILTER_VALIDATE_EMAIL) ? $smtp['username'] : config('mail.username'));
        config()->set('mail.password',strlen(@$smtp['password']) ? $smtp['password'] : config('mail.password'));

        config()->set('mail.host',strlen(@$smtp['host']) ? $smtp['host'] : config('mail.host'));
        config()->set('mail.port',strlen(@$smtp['port']) ? $smtp['port'] : config('mail.port'));
        config()->set('mail.encryption',strlen(@$smtp['encryption']) ? $smtp['encryption'] : config('mail.encryption'));

//        dd(config('mail.host'));
    }
}
