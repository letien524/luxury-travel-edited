<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelType extends Model
{
    protected $fillable = ['name', 'order_menu'];

    public function hotels()
    {
        return $this->hasMany(Hotel::class, 'type_id', 'id');
    }
}
