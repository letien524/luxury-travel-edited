<?php

namespace App;

use App\Filters\TourFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $fillable = ['name', 'slug', 'excerpt', 'image', 'duration', 'content', 'map', 'plan', 'hotel', 'file', 'status', 'highlight', 'faq_id', 'meta_title', 'meta_description', 'price', 'price_promotion', 'canonical', 'index', 'category_id'];

    public function gallery()
    {
        return $this->hasMany(Image::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function parentTour()
    {
        return $this->find($this->parent_id);
    }

    public function childTour()
    {
        return $this->where('parent_id', $this->id)->get();
    }

    public function activities()
    {
        return $this->belongsToMany(TourActivity::class, 'activity_tour', 'tour_id', 'activity_id');
    }

    public function activityMain()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }


    public function destinations()
    {
        return $this->belongsToMany(Category::class, 'destination_tour', 'tour_id', 'destination_id');
    }

    public function faq()
    {
        return $this->belongsTo(Faq::class);
    }

    public function reviews()
    {
        return $this->belongsToMany(ReviewCategory::Class, 'review_tour', 'tour_id', 'review_id');
    }

    public function memberReviews()
    {
        return $this->belongsToMany(Review::Class, 'review_tour_member');
    }

    public function members()
    {
        return $this->belongsToMany(Member::class, 'member_tour');
    }

    public function hotelCities(){
        return $this->belongsToMany(HotelCity::class, 'hotel_city_tour','tour_id','hotel_city_id');
    }

    public function hotels(){
        return $this->belongsToMany(Hotel::class);
    }

    public function scopeFilter(Builder $builder, $request)
    {
        return (new TourFilter($request))->filter($builder);
    }


}
