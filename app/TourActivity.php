<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourActivity extends Model
{
    protected $fillable = ['name', 'slug', 'image', 'excerpt', 'content', 'status', 'order_menu', 'faq_id', 'member_id', 'review_category_id', 'canonical', 'meta_title', 'meta_description', 'index'];

    protected $casts = [
        'index' => 'boolean',
    ];

    public function faq()
    {
        return $this->belongsTo(Faq::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function review()
    {
        return $this->belongsTo(ReviewCategory::Class, 'review_category_id', 'id');
    }

    public function tours()
    {
        return $this->belongsToMany(Tour::class, 'activity_tour', 'activity_id', 'tour_id');
    }

    public function getChild()
    {
        return $this->where('parent_id', $this->id);
    }

    public function hasChild()
    {
        return $this->getChild()->count();
    }

    public function hasParent()
    {
        $parent = $this->parent_id;
        if (is_null($parent)) {
            return false;
        }
        return true;
    }

    public function getParent()
    {

        return $this->find($this->parent_id);
    }

}
