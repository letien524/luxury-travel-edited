<?php

// TypeFilter.php

namespace App\Filters;

class TourActivity
{
    public function filter($builder, $value)
    {
        $activities = explode(',',$value);

        return $builder->whereHas('activities',function($query) use($activities){
            return $query->whereIn('slug',$activities);
        });

    }
}