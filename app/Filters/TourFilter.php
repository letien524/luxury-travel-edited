<?php

// ProductFilter.php

namespace App\Filters;


class TourFilter extends AbstractFilter
{
    protected $filters = [
        'tour-filter'=>TourFilterValue::class,
        'budget'=>TourBudget::class,
        'duration'=>TourDuration::class,
        'destination'=>TourDestination::class,
        'activity'=>TourActivity::class,
        'key'=>TourKey::class,

    ];
}