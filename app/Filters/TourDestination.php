<?php

// TypeFilter.php

namespace App\Filters;

class TourDestination
{
    public function filter($builder, $value)
    {

        $destinations = explode(',',$value);
        return $builder->whereHas('destinations',function($query) use($destinations){
            return $query->whereIn('slug',$destinations);
        });
    }
}