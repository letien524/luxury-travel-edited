<?php

// TypeFilter.php

namespace App\Filters;

class TourFilterValue
{
    public function filter($builder, $value)
    {
        switch ($value) {
            case 'price':
                $tours = $builder->get();
                $tours = $tours->map(function($item){
                    if($item->price_promotion > 0){
                        $item->price = $item->price_promotion;
                    }
                    return $item;
                });
                $tours = $tours->sortBy('price')->pluck('id')->toArray();
                $tours = implode(',', $tours);

                return $builder->orderByRaw("FIELD(id,$tours)");


                break;
            case 'duration':
                return $builder->orderBy('duration', 'asc');
                break;
            case 'best-seller':
                return $builder->where('highlight', 1)->orderBy('id', 'desc');
                break;
            default:
                break;
        }

    }

}