<?php

// TypeFilter.php

namespace App\Filters;

class TourBudget
{
    public function filter($builder, $value)
    {

        $budget = explode('-',$value);
        $budget[0] = intval($budget[0]);
        $budget[1] = intval($budget[1]);

        if($budget[1]==0){
            return $builder->where(function($query) use($budget){
                $query->where('price','>',$budget[0])->orWhere('price_promotion','>',$budget[0]);
            });
        }

        if($budget[0]==0){
            return $builder->where(function($query) use($budget){
                $query->where('price','<',$budget[1])->orWhere('price_promotion','<',$budget[1]);
            });
        }


        return $builder->where(function($query) use($budget){
            $query->whereBetween('price', $budget)->orWhere(function($query) use($budget){
                $query->whereBetween('price_promotion',$budget);
            });
        });

    }
}