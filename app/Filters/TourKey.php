<?php

// TypeFilter.php

    namespace App\Filters;

    class TourKey
    {
        public function filter($builder, $value)
        {
            return $builder->where(function($query) use ($value){
                $query->where('name', 'like', '%' . $value . '%')
                    ->orWhere('content', 'like', '%' . $value . '%')
                    ->orWhere('map', 'like', '%' . $value . '%')
                    ->orWhere('plan', 'like', '%' . $value . '%')
                    ->orWhere('hotel', 'like', '%' . $value . '%');
            })

             ->orWhere(function ($query) use ($value) {
                 return $query->whereHas('destinations', function ($query) use ($value) {
                     return $query->where('name', 'like', '%' . $value . '%');
                 });
             })
             ->orWhere(function ($query) use ($value) {
                 return $query->whereHas('activities', function ($query) use ($value) {
                     return $query->where('name', 'like', '%' . $value . '%');
                 });
             });
        }
    }