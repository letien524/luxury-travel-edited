<?php

// TypeFilter.php

namespace App\Filters;

class TourDuration
{
    public function filter($builder, $value)
    {
        $duration = explode('-',$value);
        $duration[0] = intval($duration[0]);
        $duration[1] = intval($duration[1]);

        if($duration[1] == 0){
            return $builder->where('duration','>',$duration[0]);
        }
        if($duration[0] == 0){
            return $builder->where('duration','<',$duration[1]);
        }

        return $builder->whereBetween('duration', $duration);

    }
}