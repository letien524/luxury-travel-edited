<?php

namespace App\Mail;

use App\Order;
use App\Tour;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TourBooked extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }


    public function build()
    {
        $order = $this->order;
        $subject = $order->email . " Vừa đặt một tour mới!";

        return $this->subject($subject)
            ->view('mail.tour-booked')
            ->with([
                'order' => $this->order
            ]);
    }
}
