<?php

namespace App\Mail;


use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TourBookedCustomer extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }


    public function build()
    {

        $order = $this->order;
        $subject = __('Your inquiry into tailor-made holiday with Luxury Travel is well received')."!";

        return $this->subject($subject)
            ->view('mail.tour-booked-customer')
            ->with([
                'order' => $order
            ]);
    }
}
