<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactSubmitCustomer extends Mailable
{
    use Queueable, SerializesModels;

    protected $contact;

    public function __construct($contact)
    {
        $this->contact = $contact;
    }


    public function build()
    {

        $contact = $this->contact;

        $subject = __('Your message for Luxury Travel is well received');

        return $this->subject($subject)
            ->view('mail.contact-submit-confirm-customer')
            ->with([
                'contact' => $contact
            ]);
    }
}
