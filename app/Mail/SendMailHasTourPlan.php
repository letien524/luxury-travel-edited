<?php

namespace App\Mail;

use App\Contact;
use App\Tour;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailHasTourPlan extends Mailable
{
    use Queueable, SerializesModels;

    protected $tour, $contact;

    public function  __construct(Tour $tour, Contact $contact)
    {
       $this->tour = $tour;
       $this->contact = $contact;
    }


    public function build()
    {
        $file = storage_path("app/tour-plans/{$this->tour->slug}.pdf");

        $data = [
            'contact' => $this->contact,
            'tour' => $this->tour,
        ];

        return $this->subject("TRIP SCHEDULE: {$this->tour->name}")
            ->view('mail.send-tour-plan-to-customer',$data)
            ->attach($file);
    }
}
