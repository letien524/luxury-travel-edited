<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentSubmit extends Mailable
{
    use Queueable, SerializesModels;

    protected $comment;

    public function __construct($comment)
    {
        $this->comment = $comment;
    }


    public function build()
    {
        $subject = $this->comment->name.' - Vừa bình luận 1 bài viết của bạn!';

        return $this->subject($subject)
            ->view('mail.comment-submit')
            ->with(['comment' => $this->comment]);
    }
}
