<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactSubmit extends Mailable
{
    use Queueable, SerializesModels;

    protected $contact;

    public function __construct($contact)
    {
        $this->contact = $contact;
    }


    public function build()
    {

        $contact = $this->contact;
        if($contact->type == 'customer-contact'){
            $subject = $contact->email . " vừa gửi yêu cầu liên hệ mới cho bạn!";
        }
        elseif($contact->type == 'customer-subscribe'){
            $subject = $contact->email . " vừa đăng ký theo dõi!";
        }else{
            $subject = $contact->email . " vừa được thêm vào mail collection!";
        }

        return $this->subject($subject)
            ->view('mail.contact-submit-confirm')
            ->with([
                'contact' => $this->contact
            ]);
    }
}
