<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewCategory extends Model
{
    protected $fillable = ['name'];

    public function reviews()
    {
        return $this->belongsToMany(Review::Class, 'category_review', 'category_id', 'review_id');
    }

    public function tours()
    {
        return $this->belongsToMany(Tour::Class, 'review_tour', 'review_id', 'tour_id');
    }

    public function destinations()
    {
        return $this->hasMany(TourDestination::Class, 'review_category_id', 'id');
    }

    public function activities()
    {
        return $this->hasMany(TourActivity::Class, 'review_category_id', 'id');
    }

}
