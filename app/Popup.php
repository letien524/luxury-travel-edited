<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{
    protected $fillable = ['name', 'image', 'link', 'content', 'button_name_display', 'delay_time', 'black_list', 'is_display', 'is_active', 'type','order_menu'];

    public function getBlackListField(){
        $blackList = $this->black_list;
        $blackList = implode(", \n", $blackList);
        return $blackList;
    }

    public function getBlackListAttribute($value)
    {
        $blackList = preg_replace("/\r|\n/", "", $value);
        $blackList = explode(',', $blackList);

        $blackList = array_map(function ($v) {
            return trim($v);
        }, $blackList);

        $blackList = array_filter($blackList, function ($v) {
            return strlen($v);
        });

        return $blackList;
    }
}
