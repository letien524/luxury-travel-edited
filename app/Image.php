<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['name', 'url', 'description', 'type', 'tour_id'];

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
