<?php
namespace App\Enum;

class CommonEnum {
    const MESSAGES = [
        'success' => [
            'flash_level' => 'success',
            'flash_message' => 'Dữ liệu cập nhật thành công!',
        ],

        'bulkDestroyFailByCheckbox' => [
            'flash_level' => 'danger',
            'flash_message' => 'Bạn chưa chọn dữ liệu hoặc dữ liệu không tồn tại!',
        ],

        'slugIsExisted' => [
            'flash_level' => 'danger',
            'flash_message' => 'Đường dẫn dữ liệu này đã tồn tại!',
        ],

        'nameIsExisted' => [
            'flash_level' => 'danger',
            'flash_message' => 'Tiêu đề dữ liệu này đã tồn tại!',
        ],

        'categoryNameIsExisted' => [
            'flash_level' => 'danger',
            'flash_message' => 'Tiêu đề danh mục dữ liệu này đã tồn tại!',
        ],

        'categorySlugIsExisted' => [
            'flash_level' => 'danger',
            'flash_message' => 'Đường dẫn danh mục dữ liệu này đã tồn tại!',
        ],

        'permissionDenied' => [
            'flash_level' => 'danger',
            'flash_message' => 'Bạn không đủ quyền thực hiện thao tác!',
        ],

        'inValidCurrentPassword' => [
            'flash_level' => 'danger',
            'flash_message' => 'Mật khẩu tài khoản hiện tại đang đăng nhập không đúng!',
        ],

        'emptyData' => [
            'flash_level' => 'danger',
            'flash_message' => 'Dữ liệu rỗng!',
        ],

        'deleteConfirm' => 'Bạn có đồng ý xóa bỏ đi nội dung đã chọn?',

        'restoreConfirm' => 'Bạn có đồng ý thay đổi lại toàn bộ hệ thống? Sau khi phục hồi xong bạn phải đăng nhập lại!',


    ];

    const MODAL = [
        'delete' => 'Xóa dữ liệu',
    ];

    const TEXT_COLOR_CLASS = [
        'default' => 'default',
        'primary' => 'primary',
        'success' => 'success',
        'info' => 'info',
        'warning' => 'warning',
        'danger' => 'danger',
    ];

    const GET_DATA = [
        'id-name' => ['id', 'name'],
    ];
}

