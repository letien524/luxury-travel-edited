<?php
namespace App\Enum;

class PopupEnum {
   const TYPES = [
        'direction' => 'Popup chuyển hướng',
        'collection' => 'Popup collect email',
        'collection_bottom' => 'Bottom popup collect email',
        'collection_corner' => 'Corner popup collect email',
   ];
}
