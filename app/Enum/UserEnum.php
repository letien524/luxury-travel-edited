<?php

namespace App\Enum;

class UserEnum
{
    const ROLE = [
        0 => 'Editor',
        1 => 'Admin',
        2 => 'Tour Admin',
        3 => 'Sale Admin',
        9 => 'Developer',
    ];
}

