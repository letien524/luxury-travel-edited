<?php
namespace App\Enum;

class BackupEnum {
    const MESSAGES = [
        'invalidFormat' => [
            'flash_level' => 'danger',
            'flash_message' => 'File không đúng định dạng!',
        ],

        'invalidFile' => [
            'flash_level' => 'danger',
            'flash_message' => 'File không tồn tại!',
        ],

        'restoreError' => [
            'flash_level' => 'danger',
            'flash_message' => 'Restore has some Errors! Your database is not effected',
        ],

        'downloadConfirm' => 'Bạn có chắc muốn tải file này?',
        'createBackupConfirm' => 'Bạn có chắc muốn tạo 1 bản backup mới?',
        'createBackupLabel' => 'Xác nhận tạo backup mới',
        'deleteBackupLabel' => 'Xác nhận xoá backup',
        'restoreBackupLabel' => 'Xác nhận khôi phục dữ liệu',

        'beginCreateSeeder' => 'BACKUP|BEGIN: CREATE SEEDER',
        'endCreateSeeder' => 'BACKUP|END: CREATE SEEDER',

        'beginCreateBackup' => 'BACKUP|BEGIN: CREATE BACKUP',
        'endCreateBackup' => 'BACKUP|END: CREATE BACKUP',
    ];
}

