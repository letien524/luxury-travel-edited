<?php
namespace App\Enum;

class RouteActionEnum {
   const ACTIONS = [
        'index' => 'Danh sách',
        'edit' => 'Sửa',
        'update' => 'Cập nhật',
        'create' => 'Thêm mới',
        'store' => 'Lưu',
        'destroy' => 'Xóa',
        'bulkDestroy' => 'Xóa nhiều',
   ];
}

