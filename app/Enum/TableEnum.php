<?php
namespace App\Enum;

class TableEnum {
    const LABELS = [
        'name' => 'Tiêu đề',
        'name_sub' => 'Tiêu đề phụ',
        'address' => 'Địa chỉ',
        'categories' => 'Danh mục',
        'type' => 'Loại',
        'roomType' => 'Loại phòng',
        'city' => 'Thành phố',
        'actions' => 'Thao tác',
        'view' => 'Xem',
        'cancel' => 'Hủy',
        'edit' => 'Sửa',
        'delete' => 'Xóa',
        'activities' => 'Activities',
        'destinations' => 'Destinations',
        'id' => 'ID',
        'status' => 'Trạng thái',
        'status_menu' => 'Menu',
        'is_inquire' => 'Inquire Form',
        'is_best' => 'Best Destination',
        'is_active' => 'Active',
        'highlight' => 'Highlight',
        'index' => 'Index',
        'slug' => 'Đường dẫn',
        'level' => 'Vai trò',
        'created_at' => 'Thời gian tạo',
        'submited_at' => 'Thời gian',
        'updated_at' => 'Thời gian cập nhật',
        'email' => 'Email',
        'phone' => 'SĐT',
        'hotline' => 'Hotline',
        'departure_date' => 'Ngày đến',
        'content' => 'Nội dung',
        'position' => 'Chức vụ',
        'order_menu' => 'STT',
        'from' => 'From',
        'to' => 'To',
        'duplicate' => 'Duplicate',
        'destination_parent' => 'Parent Des',
        'checkbox' => '<input type="checkbox" name="chkAll" id="chkAll">',
    ];

    const FIELDS = [
        'status' => [
            0 => 'Ẩn',
            1 => 'Hiển thị',
        ],
        'highlight' => [
            0 => 'No-H',
            1 => 'Highlight',
        ],
        'index' => [
            0 => 'No-I',
            1 => 'Index',
        ],

        'active' => [
            0 => 'disable',
            1 => 'enable',
        ],

        'level' => UserEnum::ROLE,

    ];
}

