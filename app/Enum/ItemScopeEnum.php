<?php
namespace App\Enum;

class ItemScopeEnum {
    const ITEM_TYPE = [
      'ItemList' => 'itemscope itemtype="http://schema.org/ItemList"',
      'Offer' => 'itemscope itemtype="http://schema.org/Offer"',
      'Product' => 'itemscope itemtype="http://schema.org/Product"',
    ];
}

