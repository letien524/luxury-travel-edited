<?php

namespace App\Enum;

class ContactEnum
{
    const STATUS = [
        -1 => 'spam',
        0 => 'Mới',
        1 => 'Đã xem',
        2 => 'Đã phản hồi',
    ];
}

