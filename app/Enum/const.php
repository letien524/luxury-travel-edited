<?php
    define('__IMAGE_THUMBNAIL_DEFAULT', asset('/placeholder.png'));
    define('__IMAGE_LAZY_LOAD', 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');
    define('__IMAGE_UPLOAD_FOLDER', 'uploads/images/');
    define('__IMAGE_PRODUCT_FOLDER', __IMAGE_UPLOAD_FOLDER . 'product/');

    define('__STORAGE_PUBLIC', '/public');
    define('__STORAGE_APP', '/storage/app');
    define('__STORAGE_IMAGE', __STORAGE_PUBLIC.'/images');
    define('__STORAGE_FILE', __STORAGE_PUBLIC.'/files');

    define('__STORAGE_CATEGORY', __STORAGE_IMAGE.'/categories');
    define('__STORAGE_BRAND', __STORAGE_IMAGE.'/brands');
    define('__STORAGE_TOUR', __STORAGE_IMAGE.'/tours');

    define('__STORAGE_ACTIVITY', __STORAGE_TOUR.'/activities');
    define('__STORAGE_DESTINATION', __STORAGE_TOUR.'/destinations');


    define('__STORAGE_PRODUCT', __STORAGE_IMAGE . '/product');
    define('__STORAGE_BLOG', __STORAGE_IMAGE . '/blog');
    define('__STORAGE_SERVICE', __STORAGE_IMAGE . '/service');
    define('__STORAGE_ADVERTISE', __STORAGE_IMAGE . '/advertise');
    define('__STORAGE_BANK', __STORAGE_IMAGE . '/bank');
    define('__STORAGE_VIDEO', __STORAGE_IMAGE . '/video');
    define('__STORAGE_PARTNER', __STORAGE_IMAGE . '/partner');
    define('__STORAGE_CUSTOMER', __STORAGE_IMAGE . '/customer');
    define('__STORAGE_HOME', __STORAGE_IMAGE . '/home');
    define('__STORAGE_ABOUT', __STORAGE_IMAGE . '/about');
    define('__STORAGE_USER', __STORAGE_IMAGE . '/user');
    define('__STORAGE_MEMBER', __STORAGE_IMAGE . '/members');
    define('__STORAGE_CONTACT', __STORAGE_IMAGE . '/contacts');

    define('__ID_PRODUCT_PREFIX', '#SHA');
    define('__ID_ORDER_PREFIX', '#SHA_ORDER_');
    define('__YOUTUBE_EMBED_PREFIX', 'https://www.youtube.com/embed/');


