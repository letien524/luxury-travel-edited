<?php

namespace App\Enum;

class RoleEnum
{
    const ROLE = [
        0 => 'editor',
        1 => 'admin',
        2 => 'tourAdmin',
        3 => 'saleAdmin',
        9 => 'develop',
    ];

    const ROLE_GROUP = [
        'admin' => [self::ROLE[1]],
        'saleAdmin' => [self::ROLE[1], self::ROLE[3]],
        'tourAdmin' => [self::ROLE[1], self::ROLE[2], self::ROLE[3]],
        'editor' => [self::ROLE[0], self::ROLE[1], self::ROLE[2], self::ROLE[3]],
    ];
}

