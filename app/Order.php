<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'country_id', 'content', 'hotel', 'duration', 'activity', 'number_child', 'number_adult', 'departure_date', 'status', 'tour_id'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    public function destinations()
    {
        return $this->belongsToMany(Category::class, 'order_category');
    }
}
