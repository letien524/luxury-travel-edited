<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourDestination extends Model
{
    protected $fillable = ['name', 'slug', 'type', 'parent_id', 'meta_title', 'meta_description', 'excerpt', 'content', 'status', 'is_display', 'is_best', 'is_inquire', 'order_menu', 'image', 'canonical', 'faq_id', 'member_id', 'review_category_id', 'index'];

    public function faq()
    {
        return $this->belongsTo(Faq::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function review()
    {
        return $this->belongsTo(ReviewCategory::Class, 'review_category_id', 'id');
    }

    public function tours()
    {
        return $this->belongsToMany(Tour::class, 'destination_tour', 'destination_id', 'tour_id');
    }

    public function parent(){
        return $this->hasOne(TourDestination::class, 'id','parent_id');
    }

    public function child(){
        return $this->hasMany(TourDestination::class, 'parent_id','id');
    }

    public function getChild()
    {
        return $this->where('parent_id', $this->id);
    }

    public function hasChild()
    {
        return $this->getChild()->count();
    }

    public function hasParent()
    {
        $parent = $this->parent_id;
        if (is_null($parent)) {
            return false;
        }
        return true;
    }

    public function getParent()
    {
        return $this->find($this->parent_id);
    }

}
