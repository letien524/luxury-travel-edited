<?php

namespace App\Exports;

use App\Tour;
use Maatwebsite\Excel\Concerns\FromCollection;

class TourExport implements FromCollection
{
    public function collection()
    {
        return Tour::all();
    }
}
