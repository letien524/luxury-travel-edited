<?php

use Illuminate\Database\Seeder;

class HotelsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hotels')->delete();
        
        \DB::table('hotels')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Hanoi Imperial hotel',
                'city_id' => 1,
                'type_id' => 1,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 08:37:51',
                'updated_at' => '2019-10-25 08:37:51',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Medalion Hanoi hotel',
                'city_id' => 1,
                'type_id' => 1,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:38:14',
                'updated_at' => '2019-10-25 08:38:14',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Festival hotel',
                'city_id' => 2,
                'type_id' => 1,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:38:42',
                'updated_at' => '2019-10-25 08:38:42',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Hue Heritage hotel',
                'city_id' => 2,
                'type_id' => 1,
                'room_type_id' => 8,
                'created_at' => '2019-10-25 08:39:32',
                'updated_at' => '2019-10-25 08:39:32',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Ancient House River Resort',
                'city_id' => 3,
                'type_id' => 1,
                'room_type_id' => 8,
                'created_at' => '2019-10-25 08:39:56',
                'updated_at' => '2019-10-25 08:39:56',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Signature Saigon hotel',
                'city_id' => 4,
                'type_id' => 1,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:40:17',
                'updated_at' => '2019-10-25 08:40:17',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Palace Saigon hotel',
                'city_id' => 4,
                'type_id' => 1,
                'room_type_id' => 9,
                'created_at' => '2019-10-25 08:41:07',
                'updated_at' => '2019-10-25 08:41:07',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Somedavi hotel',
                'city_id' => 5,
                'type_id' => 1,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 08:41:28',
                'updated_at' => '2019-10-25 08:41:28',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Sunway Hotel Hanoi',
                'city_id' => 1,
                'type_id' => 2,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 08:41:59',
                'updated_at' => '2019-10-25 08:41:59',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Silk Path hotel',
                'city_id' => 1,
                'type_id' => 2,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:42:17',
                'updated_at' => '2019-10-25 08:42:17',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Eldora Hue Hotel',
                'city_id' => 2,
                'type_id' => 2,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:42:30',
                'updated_at' => '2019-10-25 08:42:41',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Hoi An Beach Resort & Spa',
                'city_id' => 3,
                'type_id' => 2,
                'room_type_id' => 3,
                'created_at' => '2019-10-25 08:43:16',
                'updated_at' => '2019-10-25 08:43:16',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'Hoi An Boutique Resort',
                'city_id' => 3,
                'type_id' => 2,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 08:43:33',
                'updated_at' => '2019-10-25 08:43:33',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'Liberty Central Hotel',
                'city_id' => 4,
                'type_id' => 2,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:43:46',
                'updated_at' => '2019-10-25 08:43:46',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Tara Angkor',
                'city_id' => 5,
                'type_id' => 2,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 08:44:26',
                'updated_at' => '2019-10-25 08:44:26',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Intercontinental Westlake Hanoi',
                'city_id' => 1,
                'type_id' => 3,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 08:44:51',
                'updated_at' => '2019-10-25 08:44:51',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'Hotel De L’Opera Hanoi',
                'city_id' => 1,
                'type_id' => 3,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:55:04',
                'updated_at' => '2019-10-25 08:55:04',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Pilgrimage Village & Resort',
                'city_id' => 2,
                'type_id' => 3,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:57:56',
                'updated_at' => '2019-10-25 08:57:56',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Palm Garden Beach Resort & Spa',
                'city_id' => 3,
                'type_id' => 3,
                'room_type_id' => 10,
                'created_at' => '2019-10-25 08:59:40',
                'updated_at' => '2019-10-25 08:59:40',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Golden Sand Resort & Spa',
                'city_id' => 3,
                'type_id' => 3,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 08:59:53',
                'updated_at' => '2019-10-25 08:59:53',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'Caravelle Hotel Saigon',
                'city_id' => 4,
                'type_id' => 3,
                'room_type_id' => 2,
                'created_at' => '2019-10-25 09:00:03',
                'updated_at' => '2019-10-25 09:00:03',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'Sofitel Plaza Hotel Saigon',
                'city_id' => 4,
                'type_id' => 3,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 09:00:12',
                'updated_at' => '2019-10-25 09:00:12',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => 'Sofitel Royal Angkor',
                'city_id' => 5,
                'type_id' => 3,
                'room_type_id' => 1,
                'created_at' => '2019-10-25 09:00:20',
                'updated_at' => '2019-10-25 09:00:20',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'Sofitel Legend Metropole Hanoi',
                'city_id' => 1,
                'type_id' => 4,
                'room_type_id' => 4,
                'created_at' => '2019-10-25 09:00:44',
                'updated_at' => '2019-10-25 09:00:44',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'La Residence Hue',
                'city_id' => 2,
                'type_id' => 4,
                'room_type_id' => 3,
                'created_at' => '2019-10-25 09:01:09',
                'updated_at' => '2019-10-25 09:01:09',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'The Nam Hai resort',
                'city_id' => 3,
                'type_id' => 4,
                'room_type_id' => 5,
                'created_at' => '2019-10-25 09:01:23',
                'updated_at' => '2019-10-25 09:01:23',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Park Hyatt Saigon Hotel',
                'city_id' => 4,
                'type_id' => 4,
                'room_type_id' => 11,
                'created_at' => '2019-10-25 09:02:18',
                'updated_at' => '2019-10-25 09:02:18',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Amansara resort',
                'city_id' => 5,
                'type_id' => 4,
                'room_type_id' => 7,
                'created_at' => '2019-10-25 09:02:28',
                'updated_at' => '2019-10-25 09:02:28',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'Đây là tên khách sạn',
                'city_id' => 6,
                'type_id' => 5,
                'room_type_id' => 12,
                'created_at' => '2019-10-28 14:33:20',
                'updated_at' => '2019-10-28 14:33:20',
            ),
            29 => 
            array (
                'id' => 30,
            'name' => 'Đây là tên khách sạn 2 (duplicate)',
                'city_id' => 6,
                'type_id' => 5,
                'room_type_id' => 12,
                'created_at' => '2019-10-28 14:33:37',
                'updated_at' => '2019-10-28 14:33:37',
            ),
            30 => 
            array (
                'id' => 31,
                'name' => 'Ngọc ANh test tên khách sạn',
                'city_id' => 7,
                'type_id' => 3,
                'room_type_id' => 2,
                'created_at' => '2019-10-28 14:35:26',
                'updated_at' => '2019-10-28 14:35:26',
            ),
            31 => 
            array (
                'id' => 32,
                'name' => 'Ngọc ANh test tên khách sạn 4 star',
                'city_id' => 7,
                'type_id' => 2,
                'room_type_id' => 2,
                'created_at' => '2019-10-28 14:35:43',
                'updated_at' => '2019-10-28 14:35:43',
            ),
            32 => 
            array (
                'id' => 33,
                'name' => 'Ngọc ANh test tên khách sạn 3 star',
                'city_id' => 7,
                'type_id' => 2,
                'room_type_id' => 10,
                'created_at' => '2019-10-28 14:35:52',
                'updated_at' => '2019-10-28 14:35:58',
            ),
            33 => 
            array (
                'id' => 34,
                'name' => 'Vy test khách snaj',
                'city_id' => 8,
                'type_id' => 4,
                'room_type_id' => 2,
                'created_at' => '2019-10-28 14:37:34',
                'updated_at' => '2019-10-28 14:37:34',
            ),
        ));
        
        
    }
}