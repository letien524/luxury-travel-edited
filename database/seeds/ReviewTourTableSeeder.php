<?php

use Illuminate\Database\Seeder;

class ReviewTourTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('review_tour')->delete();
        
        \DB::table('review_tour')->insert(array (
            0 => 
            array (
                'review_id' => 1,
                'tour_id' => 412,
            ),
            1 => 
            array (
                'review_id' => 1,
                'tour_id' => 413,
            ),
        ));
        
        
    }
}