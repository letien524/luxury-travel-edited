<?php

use Illuminate\Database\Seeder;

class DestinationTourTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('destination_tour')->delete();

        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 42,
                'tour_id' => 13,
            ),
            1 =>
            array (
                'destination_id' => 43,
                'tour_id' => 13,
            ),
            2 =>
            array (
                'destination_id' => 1,
                'tour_id' => 14,
            ),
            3 =>
            array (
                'destination_id' => 1,
                'tour_id' => 15,
            ),
            4 =>
            array (
                'destination_id' => 1,
                'tour_id' => 16,
            ),
            5 =>
            array (
                'destination_id' => 42,
                'tour_id' => 17,
            ),
            6 =>
            array (
                'destination_id' => 48,
                'tour_id' => 17,
            ),
            7 =>
            array (
                'destination_id' => 50,
                'tour_id' => 17,
            ),
            8 =>
            array (
                'destination_id' => 1,
                'tour_id' => 18,
            ),
            9 =>
            array (
                'destination_id' => 45,
                'tour_id' => 18,
            ),
            10 =>
            array (
                'destination_id' => 58,
                'tour_id' => 18,
            ),
            11 =>
            array (
                'destination_id' => 1,
                'tour_id' => 19,
            ),
            12 =>
            array (
                'destination_id' => 1,
                'tour_id' => 20,
            ),
            13 =>
            array (
                'destination_id' => 51,
                'tour_id' => 20,
            ),
            14 =>
            array (
                'destination_id' => 1,
                'tour_id' => 21,
            ),
            15 =>
            array (
                'destination_id' => 46,
                'tour_id' => 21,
            ),
            16 =>
            array (
                'destination_id' => 1,
                'tour_id' => 22,
            ),
            17 =>
            array (
                'destination_id' => 37,
                'tour_id' => 22,
            ),
            18 =>
            array (
                'destination_id' => 63,
                'tour_id' => 22,
            ),
            19 =>
            array (
                'destination_id' => 1,
                'tour_id' => 23,
            ),
            20 =>
            array (
                'destination_id' => 1,
                'tour_id' => 24,
            ),
            21 =>
            array (
                'destination_id' => 1,
                'tour_id' => 25,
            ),
            22 =>
            array (
                'destination_id' => 23,
                'tour_id' => 25,
            ),
            23 =>
            array (
                'destination_id' => 37,
                'tour_id' => 25,
            ),
            24 =>
            array (
                'destination_id' => 63,
                'tour_id' => 25,
            ),
            25 =>
            array (
                'destination_id' => 1,
                'tour_id' => 26,
            ),
            26 =>
            array (
                'destination_id' => 42,
                'tour_id' => 27,
            ),
            27 =>
            array (
                'destination_id' => 50,
                'tour_id' => 27,
            ),
            28 =>
            array (
                'destination_id' => 1,
                'tour_id' => 28,
            ),
            29 =>
            array (
                'destination_id' => 1,
                'tour_id' => 29,
            ),
            30 =>
            array (
                'destination_id' => 63,
                'tour_id' => 29,
            ),
            31 =>
            array (
                'destination_id' => 1,
                'tour_id' => 30,
            ),
            32 =>
            array (
                'destination_id' => 45,
                'tour_id' => 30,
            ),
            33 =>
            array (
                'destination_id' => 1,
                'tour_id' => 31,
            ),
            34 =>
            array (
                'destination_id' => 1,
                'tour_id' => 32,
            ),
            35 =>
            array (
                'destination_id' => 1,
                'tour_id' => 33,
            ),
            36 =>
            array (
                'destination_id' => 1,
                'tour_id' => 34,
            ),
            37 =>
            array (
                'destination_id' => 1,
                'tour_id' => 35,
            ),
            38 =>
            array (
                'destination_id' => 45,
                'tour_id' => 35,
            ),
            39 =>
            array (
                'destination_id' => 1,
                'tour_id' => 36,
            ),
            40 =>
            array (
                'destination_id' => 49,
                'tour_id' => 36,
            ),
            41 =>
            array (
                'destination_id' => 1,
                'tour_id' => 37,
            ),
            42 =>
            array (
                'destination_id' => 1,
                'tour_id' => 38,
            ),
            43 =>
            array (
                'destination_id' => 42,
                'tour_id' => 39,
            ),
            44 =>
            array (
                'destination_id' => 43,
                'tour_id' => 39,
            ),
            45 =>
            array (
                'destination_id' => 50,
                'tour_id' => 39,
            ),
            46 =>
            array (
                'destination_id' => 1,
                'tour_id' => 40,
            ),
            47 =>
            array (
                'destination_id' => 1,
                'tour_id' => 41,
            ),
            48 =>
            array (
                'destination_id' => 1,
                'tour_id' => 42,
            ),
            49 =>
            array (
                'destination_id' => 52,
                'tour_id' => 42,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 1,
                'tour_id' => 43,
            ),
            1 =>
            array (
                'destination_id' => 1,
                'tour_id' => 44,
            ),
            2 =>
            array (
                'destination_id' => 47,
                'tour_id' => 44,
            ),
            3 =>
            array (
                'destination_id' => 51,
                'tour_id' => 44,
            ),
            4 =>
            array (
                'destination_id' => 53,
                'tour_id' => 44,
            ),
            5 =>
            array (
                'destination_id' => 1,
                'tour_id' => 45,
            ),
            6 =>
            array (
                'destination_id' => 53,
                'tour_id' => 45,
            ),
            7 =>
            array (
                'destination_id' => 42,
                'tour_id' => 46,
            ),
            8 =>
            array (
                'destination_id' => 1,
                'tour_id' => 47,
            ),
            9 =>
            array (
                'destination_id' => 1,
                'tour_id' => 48,
            ),
            10 =>
            array (
                'destination_id' => 1,
                'tour_id' => 49,
            ),
            11 =>
            array (
                'destination_id' => 1,
                'tour_id' => 50,
            ),
            12 =>
            array (
                'destination_id' => 51,
                'tour_id' => 50,
            ),
            13 =>
            array (
                'destination_id' => 1,
                'tour_id' => 51,
            ),
            14 =>
            array (
                'destination_id' => 1,
                'tour_id' => 52,
            ),
            15 =>
            array (
                'destination_id' => 42,
                'tour_id' => 53,
            ),
            16 =>
            array (
                'destination_id' => 43,
                'tour_id' => 53,
            ),
            17 =>
            array (
                'destination_id' => 1,
                'tour_id' => 54,
            ),
            18 =>
            array (
                'destination_id' => 46,
                'tour_id' => 54,
            ),
            19 =>
            array (
                'destination_id' => 24,
                'tour_id' => 55,
            ),
            20 =>
            array (
                'destination_id' => 1,
                'tour_id' => 56,
            ),
            21 =>
            array (
                'destination_id' => 1,
                'tour_id' => 57,
            ),
            22 =>
            array (
                'destination_id' => 23,
                'tour_id' => 57,
            ),
            23 =>
            array (
                'destination_id' => 37,
                'tour_id' => 57,
            ),
            24 =>
            array (
                'destination_id' => 45,
                'tour_id' => 57,
            ),
            25 =>
            array (
                'destination_id' => 42,
                'tour_id' => 58,
            ),
            26 =>
            array (
                'destination_id' => 1,
                'tour_id' => 59,
            ),
            27 =>
            array (
                'destination_id' => 1,
                'tour_id' => 60,
            ),
            28 =>
            array (
                'destination_id' => 45,
                'tour_id' => 60,
            ),
            29 =>
            array (
                'destination_id' => 49,
                'tour_id' => 60,
            ),
            30 =>
            array (
                'destination_id' => 58,
                'tour_id' => 60,
            ),
            31 =>
            array (
                'destination_id' => 1,
                'tour_id' => 61,
            ),
            32 =>
            array (
                'destination_id' => 1,
                'tour_id' => 62,
            ),
            33 =>
            array (
                'destination_id' => 1,
                'tour_id' => 63,
            ),
            34 =>
            array (
                'destination_id' => 49,
                'tour_id' => 63,
            ),
            35 =>
            array (
                'destination_id' => 1,
                'tour_id' => 64,
            ),
            36 =>
            array (
                'destination_id' => 22,
                'tour_id' => 65,
            ),
            37 =>
            array (
                'destination_id' => 42,
                'tour_id' => 66,
            ),
            38 =>
            array (
                'destination_id' => 43,
                'tour_id' => 66,
            ),
            39 =>
            array (
                'destination_id' => 50,
                'tour_id' => 66,
            ),
            40 =>
            array (
                'destination_id' => 1,
                'tour_id' => 67,
            ),
            41 =>
            array (
                'destination_id' => 42,
                'tour_id' => 68,
            ),
            42 =>
            array (
                'destination_id' => 43,
                'tour_id' => 68,
            ),
            43 =>
            array (
                'destination_id' => 45,
                'tour_id' => 68,
            ),
            44 =>
            array (
                'destination_id' => 1,
                'tour_id' => 69,
            ),
            45 =>
            array (
                'destination_id' => 51,
                'tour_id' => 69,
            ),
            46 =>
            array (
                'destination_id' => 22,
                'tour_id' => 70,
            ),
            47 =>
            array (
                'destination_id' => 23,
                'tour_id' => 70,
            ),
            48 =>
            array (
                'destination_id' => 1,
                'tour_id' => 71,
            ),
            49 =>
            array (
                'destination_id' => 23,
                'tour_id' => 71,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 37,
                'tour_id' => 71,
            ),
            1 =>
            array (
                'destination_id' => 24,
                'tour_id' => 72,
            ),
            2 =>
            array (
                'destination_id' => 24,
                'tour_id' => 73,
            ),
            3 =>
            array (
                'destination_id' => 24,
                'tour_id' => 74,
            ),
            4 =>
            array (
                'destination_id' => 24,
                'tour_id' => 75,
            ),
            5 =>
            array (
                'destination_id' => 25,
                'tour_id' => 76,
            ),
            6 =>
            array (
                'destination_id' => 1,
                'tour_id' => 77,
            ),
            7 =>
            array (
                'destination_id' => 42,
                'tour_id' => 78,
            ),
            8 =>
            array (
                'destination_id' => 50,
                'tour_id' => 78,
            ),
            9 =>
            array (
                'destination_id' => 22,
                'tour_id' => 79,
            ),
            10 =>
            array (
                'destination_id' => 23,
                'tour_id' => 79,
            ),
            11 =>
            array (
                'destination_id' => 23,
                'tour_id' => 80,
            ),
            12 =>
            array (
                'destination_id' => 1,
                'tour_id' => 81,
            ),
            13 =>
            array (
                'destination_id' => 49,
                'tour_id' => 81,
            ),
            14 =>
            array (
                'destination_id' => 1,
                'tour_id' => 82,
            ),
            15 =>
            array (
                'destination_id' => 42,
                'tour_id' => 83,
            ),
            16 =>
            array (
                'destination_id' => 43,
                'tour_id' => 83,
            ),
            17 =>
            array (
                'destination_id' => 1,
                'tour_id' => 84,
            ),
            18 =>
            array (
                'destination_id' => 1,
                'tour_id' => 85,
            ),
            19 =>
            array (
                'destination_id' => 42,
                'tour_id' => 86,
            ),
            20 =>
            array (
                'destination_id' => 48,
                'tour_id' => 86,
            ),
            21 =>
            array (
                'destination_id' => 25,
                'tour_id' => 87,
            ),
            22 =>
            array (
                'destination_id' => 1,
                'tour_id' => 88,
            ),
            23 =>
            array (
                'destination_id' => 1,
                'tour_id' => 89,
            ),
            24 =>
            array (
                'destination_id' => 44,
                'tour_id' => 89,
            ),
            25 =>
            array (
                'destination_id' => 1,
                'tour_id' => 90,
            ),
            26 =>
            array (
                'destination_id' => 1,
                'tour_id' => 91,
            ),
            27 =>
            array (
                'destination_id' => 52,
                'tour_id' => 91,
            ),
            28 =>
            array (
                'destination_id' => 1,
                'tour_id' => 92,
            ),
            29 =>
            array (
                'destination_id' => 44,
                'tour_id' => 92,
            ),
            30 =>
            array (
                'destination_id' => 56,
                'tour_id' => 92,
            ),
            31 =>
            array (
                'destination_id' => 1,
                'tour_id' => 93,
            ),
            32 =>
            array (
                'destination_id' => 22,
                'tour_id' => 93,
            ),
            33 =>
            array (
                'destination_id' => 23,
                'tour_id' => 93,
            ),
            34 =>
            array (
                'destination_id' => 37,
                'tour_id' => 93,
            ),
            35 =>
            array (
                'destination_id' => 38,
                'tour_id' => 93,
            ),
            36 =>
            array (
                'destination_id' => 41,
                'tour_id' => 93,
            ),
            37 =>
            array (
                'destination_id' => 42,
                'tour_id' => 94,
            ),
            38 =>
            array (
                'destination_id' => 43,
                'tour_id' => 94,
            ),
            39 =>
            array (
                'destination_id' => 23,
                'tour_id' => 95,
            ),
            40 =>
            array (
                'destination_id' => 1,
                'tour_id' => 96,
            ),
            41 =>
            array (
                'destination_id' => 1,
                'tour_id' => 97,
            ),
            42 =>
            array (
                'destination_id' => 1,
                'tour_id' => 98,
            ),
            43 =>
            array (
                'destination_id' => 1,
                'tour_id' => 101,
            ),
            44 =>
            array (
                'destination_id' => 1,
                'tour_id' => 102,
            ),
            45 =>
            array (
                'destination_id' => 51,
                'tour_id' => 102,
            ),
            46 =>
            array (
                'destination_id' => 42,
                'tour_id' => 103,
            ),
            47 =>
            array (
                'destination_id' => 1,
                'tour_id' => 104,
            ),
            48 =>
            array (
                'destination_id' => 1,
                'tour_id' => 105,
            ),
            49 =>
            array (
                'destination_id' => 42,
                'tour_id' => 106,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 50,
                'tour_id' => 106,
            ),
            1 =>
            array (
                'destination_id' => 25,
                'tour_id' => 107,
            ),
            2 =>
            array (
                'destination_id' => 1,
                'tour_id' => 108,
            ),
            3 =>
            array (
                'destination_id' => 63,
                'tour_id' => 108,
            ),
            4 =>
            array (
                'destination_id' => 1,
                'tour_id' => 109,
            ),
            5 =>
            array (
                'destination_id' => 63,
                'tour_id' => 109,
            ),
            6 =>
            array (
                'destination_id' => 23,
                'tour_id' => 110,
            ),
            7 =>
            array (
                'destination_id' => 42,
                'tour_id' => 111,
            ),
            8 =>
            array (
                'destination_id' => 1,
                'tour_id' => 112,
            ),
            9 =>
            array (
                'destination_id' => 1,
                'tour_id' => 113,
            ),
            10 =>
            array (
                'destination_id' => 51,
                'tour_id' => 113,
            ),
            11 =>
            array (
                'destination_id' => 1,
                'tour_id' => 114,
            ),
            12 =>
            array (
                'destination_id' => 1,
                'tour_id' => 115,
            ),
            13 =>
            array (
                'destination_id' => 1,
                'tour_id' => 116,
            ),
            14 =>
            array (
                'destination_id' => 49,
                'tour_id' => 116,
            ),
            15 =>
            array (
                'destination_id' => 1,
                'tour_id' => 117,
            ),
            16 =>
            array (
                'destination_id' => 1,
                'tour_id' => 118,
            ),
            17 =>
            array (
                'destination_id' => 42,
                'tour_id' => 120,
            ),
            18 =>
            array (
                'destination_id' => 50,
                'tour_id' => 120,
            ),
            19 =>
            array (
                'destination_id' => 1,
                'tour_id' => 122,
            ),
            20 =>
            array (
                'destination_id' => 63,
                'tour_id' => 122,
            ),
            21 =>
            array (
                'destination_id' => 42,
                'tour_id' => 123,
            ),
            22 =>
            array (
                'destination_id' => 1,
                'tour_id' => 124,
            ),
            23 =>
            array (
                'destination_id' => 1,
                'tour_id' => 125,
            ),
            24 =>
            array (
                'destination_id' => 1,
                'tour_id' => 126,
            ),
            25 =>
            array (
                'destination_id' => 1,
                'tour_id' => 127,
            ),
            26 =>
            array (
                'destination_id' => 42,
                'tour_id' => 128,
            ),
            27 =>
            array (
                'destination_id' => 50,
                'tour_id' => 128,
            ),
            28 =>
            array (
                'destination_id' => 23,
                'tour_id' => 130,
            ),
            29 =>
            array (
                'destination_id' => 42,
                'tour_id' => 131,
            ),
            30 =>
            array (
                'destination_id' => 22,
                'tour_id' => 132,
            ),
            31 =>
            array (
                'destination_id' => 1,
                'tour_id' => 133,
            ),
            32 =>
            array (
                'destination_id' => 53,
                'tour_id' => 133,
            ),
            33 =>
            array (
                'destination_id' => 1,
                'tour_id' => 134,
            ),
            34 =>
            array (
                'destination_id' => 51,
                'tour_id' => 134,
            ),
            35 =>
            array (
                'destination_id' => 1,
                'tour_id' => 135,
            ),
            36 =>
            array (
                'destination_id' => 1,
                'tour_id' => 136,
            ),
            37 =>
            array (
                'destination_id' => 51,
                'tour_id' => 136,
            ),
            38 =>
            array (
                'destination_id' => 22,
                'tour_id' => 137,
            ),
            39 =>
            array (
                'destination_id' => 42,
                'tour_id' => 138,
            ),
            40 =>
            array (
                'destination_id' => 43,
                'tour_id' => 138,
            ),
            41 =>
            array (
                'destination_id' => 50,
                'tour_id' => 138,
            ),
            42 =>
            array (
                'destination_id' => 1,
                'tour_id' => 139,
            ),
            43 =>
            array (
                'destination_id' => 1,
                'tour_id' => 140,
            ),
            44 =>
            array (
                'destination_id' => 1,
                'tour_id' => 141,
            ),
            45 =>
            array (
                'destination_id' => 1,
                'tour_id' => 142,
            ),
            46 =>
            array (
                'destination_id' => 1,
                'tour_id' => 143,
            ),
            47 =>
            array (
                'destination_id' => 1,
                'tour_id' => 144,
            ),
            48 =>
            array (
                'destination_id' => 1,
                'tour_id' => 145,
            ),
            49 =>
            array (
                'destination_id' => 1,
                'tour_id' => 146,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 1,
                'tour_id' => 147,
            ),
            1 =>
            array (
                'destination_id' => 1,
                'tour_id' => 148,
            ),
            2 =>
            array (
                'destination_id' => 1,
                'tour_id' => 149,
            ),
            3 =>
            array (
                'destination_id' => 1,
                'tour_id' => 150,
            ),
            4 =>
            array (
                'destination_id' => 49,
                'tour_id' => 150,
            ),
            5 =>
            array (
                'destination_id' => 1,
                'tour_id' => 151,
            ),
            6 =>
            array (
                'destination_id' => 1,
                'tour_id' => 152,
            ),
            7 =>
            array (
                'destination_id' => 47,
                'tour_id' => 152,
            ),
            8 =>
            array (
                'destination_id' => 51,
                'tour_id' => 152,
            ),
            9 =>
            array (
                'destination_id' => 55,
                'tour_id' => 152,
            ),
            10 =>
            array (
                'destination_id' => 38,
                'tour_id' => 153,
            ),
            11 =>
            array (
                'destination_id' => 41,
                'tour_id' => 153,
            ),
            12 =>
            array (
                'destination_id' => 1,
                'tour_id' => 154,
            ),
            13 =>
            array (
                'destination_id' => 1,
                'tour_id' => 155,
            ),
            14 =>
            array (
                'destination_id' => 42,
                'tour_id' => 156,
            ),
            15 =>
            array (
                'destination_id' => 42,
                'tour_id' => 157,
            ),
            16 =>
            array (
                'destination_id' => 50,
                'tour_id' => 157,
            ),
            17 =>
            array (
                'destination_id' => 1,
                'tour_id' => 158,
            ),
            18 =>
            array (
                'destination_id' => 1,
                'tour_id' => 159,
            ),
            19 =>
            array (
                'destination_id' => 49,
                'tour_id' => 159,
            ),
            20 =>
            array (
                'destination_id' => 1,
                'tour_id' => 160,
            ),
            21 =>
            array (
                'destination_id' => 1,
                'tour_id' => 161,
            ),
            22 =>
            array (
                'destination_id' => 50,
                'tour_id' => 161,
            ),
            23 =>
            array (
                'destination_id' => 42,
                'tour_id' => 162,
            ),
            24 =>
            array (
                'destination_id' => 1,
                'tour_id' => 163,
            ),
            25 =>
            array (
                'destination_id' => 1,
                'tour_id' => 164,
            ),
            26 =>
            array (
                'destination_id' => 1,
                'tour_id' => 165,
            ),
            27 =>
            array (
                'destination_id' => 1,
                'tour_id' => 166,
            ),
            28 =>
            array (
                'destination_id' => 1,
                'tour_id' => 167,
            ),
            29 =>
            array (
                'destination_id' => 25,
                'tour_id' => 168,
            ),
            30 =>
            array (
                'destination_id' => 1,
                'tour_id' => 169,
            ),
            31 =>
            array (
                'destination_id' => 63,
                'tour_id' => 169,
            ),
            32 =>
            array (
                'destination_id' => 22,
                'tour_id' => 170,
            ),
            33 =>
            array (
                'destination_id' => 1,
                'tour_id' => 171,
            ),
            34 =>
            array (
                'destination_id' => 23,
                'tour_id' => 172,
            ),
            35 =>
            array (
                'destination_id' => 22,
                'tour_id' => 173,
            ),
            36 =>
            array (
                'destination_id' => 63,
                'tour_id' => 173,
            ),
            37 =>
            array (
                'destination_id' => 1,
                'tour_id' => 174,
            ),
            38 =>
            array (
                'destination_id' => 51,
                'tour_id' => 174,
            ),
            39 =>
            array (
                'destination_id' => 22,
                'tour_id' => 175,
            ),
            40 =>
            array (
                'destination_id' => 1,
                'tour_id' => 176,
            ),
            41 =>
            array (
                'destination_id' => 1,
                'tour_id' => 177,
            ),
            42 =>
            array (
                'destination_id' => 44,
                'tour_id' => 177,
            ),
            43 =>
            array (
                'destination_id' => 23,
                'tour_id' => 178,
            ),
            44 =>
            array (
                'destination_id' => 1,
                'tour_id' => 179,
            ),
            45 =>
            array (
                'destination_id' => 51,
                'tour_id' => 179,
            ),
            46 =>
            array (
                'destination_id' => 1,
                'tour_id' => 180,
            ),
            47 =>
            array (
                'destination_id' => 1,
                'tour_id' => 181,
            ),
            48 =>
            array (
                'destination_id' => 42,
                'tour_id' => 182,
            ),
            49 =>
            array (
                'destination_id' => 43,
                'tour_id' => 182,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 50,
                'tour_id' => 182,
            ),
            1 =>
            array (
                'destination_id' => 1,
                'tour_id' => 183,
            ),
            2 =>
            array (
                'destination_id' => 1,
                'tour_id' => 184,
            ),
            3 =>
            array (
                'destination_id' => 23,
                'tour_id' => 185,
            ),
            4 =>
            array (
                'destination_id' => 1,
                'tour_id' => 186,
            ),
            5 =>
            array (
                'destination_id' => 23,
                'tour_id' => 186,
            ),
            6 =>
            array (
                'destination_id' => 37,
                'tour_id' => 186,
            ),
            7 =>
            array (
                'destination_id' => 49,
                'tour_id' => 186,
            ),
            8 =>
            array (
                'destination_id' => 1,
                'tour_id' => 187,
            ),
            9 =>
            array (
                'destination_id' => 58,
                'tour_id' => 187,
            ),
            10 =>
            array (
                'destination_id' => 42,
                'tour_id' => 188,
            ),
            11 =>
            array (
                'destination_id' => 43,
                'tour_id' => 188,
            ),
            12 =>
            array (
                'destination_id' => 1,
                'tour_id' => 189,
            ),
            13 =>
            array (
                'destination_id' => 42,
                'tour_id' => 190,
            ),
            14 =>
            array (
                'destination_id' => 43,
                'tour_id' => 190,
            ),
            15 =>
            array (
                'destination_id' => 1,
                'tour_id' => 191,
            ),
            16 =>
            array (
                'destination_id' => 1,
                'tour_id' => 192,
            ),
            17 =>
            array (
                'destination_id' => 1,
                'tour_id' => 193,
            ),
            18 =>
            array (
                'destination_id' => 49,
                'tour_id' => 193,
            ),
            19 =>
            array (
                'destination_id' => 1,
                'tour_id' => 194,
            ),
            20 =>
            array (
                'destination_id' => 1,
                'tour_id' => 195,
            ),
            21 =>
            array (
                'destination_id' => 55,
                'tour_id' => 195,
            ),
            22 =>
            array (
                'destination_id' => 1,
                'tour_id' => 196,
            ),
            23 =>
            array (
                'destination_id' => 54,
                'tour_id' => 196,
            ),
            24 =>
            array (
                'destination_id' => 1,
                'tour_id' => 197,
            ),
            25 =>
            array (
                'destination_id' => 45,
                'tour_id' => 197,
            ),
            26 =>
            array (
                'destination_id' => 1,
                'tour_id' => 198,
            ),
            27 =>
            array (
                'destination_id' => 1,
                'tour_id' => 199,
            ),
            28 =>
            array (
                'destination_id' => 47,
                'tour_id' => 199,
            ),
            29 =>
            array (
                'destination_id' => 56,
                'tour_id' => 199,
            ),
            30 =>
            array (
                'destination_id' => 42,
                'tour_id' => 200,
            ),
            31 =>
            array (
                'destination_id' => 44,
                'tour_id' => 201,
            ),
            32 =>
            array (
                'destination_id' => 1,
                'tour_id' => 202,
            ),
            33 =>
            array (
                'destination_id' => 42,
                'tour_id' => 203,
            ),
            34 =>
            array (
                'destination_id' => 50,
                'tour_id' => 203,
            ),
            35 =>
            array (
                'destination_id' => 1,
                'tour_id' => 204,
            ),
            36 =>
            array (
                'destination_id' => 47,
                'tour_id' => 204,
            ),
            37 =>
            array (
                'destination_id' => 23,
                'tour_id' => 205,
            ),
            38 =>
            array (
                'destination_id' => 1,
                'tour_id' => 206,
            ),
            39 =>
            array (
                'destination_id' => 1,
                'tour_id' => 207,
            ),
            40 =>
            array (
                'destination_id' => 59,
                'tour_id' => 207,
            ),
            41 =>
            array (
                'destination_id' => 1,
                'tour_id' => 208,
            ),
            42 =>
            array (
                'destination_id' => 51,
                'tour_id' => 208,
            ),
            43 =>
            array (
                'destination_id' => 23,
                'tour_id' => 209,
            ),
            44 =>
            array (
                'destination_id' => 37,
                'tour_id' => 210,
            ),
            45 =>
            array (
                'destination_id' => 38,
                'tour_id' => 210,
            ),
            46 =>
            array (
                'destination_id' => 1,
                'tour_id' => 211,
            ),
            47 =>
            array (
                'destination_id' => 47,
                'tour_id' => 211,
            ),
            48 =>
            array (
                'destination_id' => 25,
                'tour_id' => 212,
            ),
            49 =>
            array (
                'destination_id' => 22,
                'tour_id' => 213,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 1,
                'tour_id' => 214,
            ),
            1 =>
            array (
                'destination_id' => 25,
                'tour_id' => 215,
            ),
            2 =>
            array (
                'destination_id' => 42,
                'tour_id' => 216,
            ),
            3 =>
            array (
                'destination_id' => 50,
                'tour_id' => 216,
            ),
            4 =>
            array (
                'destination_id' => 24,
                'tour_id' => 217,
            ),
            5 =>
            array (
                'destination_id' => 42,
                'tour_id' => 218,
            ),
            6 =>
            array (
                'destination_id' => 1,
                'tour_id' => 219,
            ),
            7 =>
            array (
                'destination_id' => 1,
                'tour_id' => 220,
            ),
            8 =>
            array (
                'destination_id' => 1,
                'tour_id' => 221,
            ),
            9 =>
            array (
                'destination_id' => 42,
                'tour_id' => 222,
            ),
            10 =>
            array (
                'destination_id' => 22,
                'tour_id' => 223,
            ),
            11 =>
            array (
                'destination_id' => 38,
                'tour_id' => 224,
            ),
            12 =>
            array (
                'destination_id' => 41,
                'tour_id' => 224,
            ),
            13 =>
            array (
                'destination_id' => 42,
                'tour_id' => 225,
            ),
            14 =>
            array (
                'destination_id' => 48,
                'tour_id' => 225,
            ),
            15 =>
            array (
                'destination_id' => 22,
                'tour_id' => 226,
            ),
            16 =>
            array (
                'destination_id' => 1,
                'tour_id' => 227,
            ),
            17 =>
            array (
                'destination_id' => 42,
                'tour_id' => 228,
            ),
            18 =>
            array (
                'destination_id' => 50,
                'tour_id' => 228,
            ),
            19 =>
            array (
                'destination_id' => 1,
                'tour_id' => 229,
            ),
            20 =>
            array (
                'destination_id' => 1,
                'tour_id' => 230,
            ),
            21 =>
            array (
                'destination_id' => 1,
                'tour_id' => 231,
            ),
            22 =>
            array (
                'destination_id' => 1,
                'tour_id' => 232,
            ),
            23 =>
            array (
                'destination_id' => 49,
                'tour_id' => 232,
            ),
            24 =>
            array (
                'destination_id' => 1,
                'tour_id' => 233,
            ),
            25 =>
            array (
                'destination_id' => 44,
                'tour_id' => 233,
            ),
            26 =>
            array (
                'destination_id' => 1,
                'tour_id' => 234,
            ),
            27 =>
            array (
                'destination_id' => 42,
                'tour_id' => 235,
            ),
            28 =>
            array (
                'destination_id' => 50,
                'tour_id' => 235,
            ),
            29 =>
            array (
                'destination_id' => 1,
                'tour_id' => 236,
            ),
            30 =>
            array (
                'destination_id' => 49,
                'tour_id' => 236,
            ),
            31 =>
            array (
                'destination_id' => 1,
                'tour_id' => 237,
            ),
            32 =>
            array (
                'destination_id' => 1,
                'tour_id' => 238,
            ),
            33 =>
            array (
                'destination_id' => 45,
                'tour_id' => 238,
            ),
            34 =>
            array (
                'destination_id' => 25,
                'tour_id' => 239,
            ),
            35 =>
            array (
                'destination_id' => 37,
                'tour_id' => 240,
            ),
            36 =>
            array (
                'destination_id' => 38,
                'tour_id' => 240,
            ),
            37 =>
            array (
                'destination_id' => 41,
                'tour_id' => 240,
            ),
            38 =>
            array (
                'destination_id' => 42,
                'tour_id' => 241,
            ),
            39 =>
            array (
                'destination_id' => 42,
                'tour_id' => 242,
            ),
            40 =>
            array (
                'destination_id' => 43,
                'tour_id' => 242,
            ),
            41 =>
            array (
                'destination_id' => 50,
                'tour_id' => 242,
            ),
            42 =>
            array (
                'destination_id' => 1,
                'tour_id' => 243,
            ),
            43 =>
            array (
                'destination_id' => 1,
                'tour_id' => 244,
            ),
            44 =>
            array (
                'destination_id' => 1,
                'tour_id' => 245,
            ),
            45 =>
            array (
                'destination_id' => 1,
                'tour_id' => 246,
            ),
            46 =>
            array (
                'destination_id' => 1,
                'tour_id' => 247,
            ),
            47 =>
            array (
                'destination_id' => 1,
                'tour_id' => 248,
            ),
            48 =>
            array (
                'destination_id' => 23,
                'tour_id' => 249,
            ),
            49 =>
            array (
                'destination_id' => 1,
                'tour_id' => 251,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 1,
                'tour_id' => 252,
            ),
            1 =>
            array (
                'destination_id' => 23,
                'tour_id' => 253,
            ),
            2 =>
            array (
                'destination_id' => 1,
                'tour_id' => 254,
            ),
            3 =>
            array (
                'destination_id' => 49,
                'tour_id' => 254,
            ),
            4 =>
            array (
                'destination_id' => 42,
                'tour_id' => 255,
            ),
            5 =>
            array (
                'destination_id' => 50,
                'tour_id' => 255,
            ),
            6 =>
            array (
                'destination_id' => 1,
                'tour_id' => 256,
            ),
            7 =>
            array (
                'destination_id' => 1,
                'tour_id' => 257,
            ),
            8 =>
            array (
                'destination_id' => 49,
                'tour_id' => 257,
            ),
            9 =>
            array (
                'destination_id' => 1,
                'tour_id' => 258,
            ),
            10 =>
            array (
                'destination_id' => 38,
                'tour_id' => 259,
            ),
            11 =>
            array (
                'destination_id' => 41,
                'tour_id' => 259,
            ),
            12 =>
            array (
                'destination_id' => 1,
                'tour_id' => 260,
            ),
            13 =>
            array (
                'destination_id' => 1,
                'tour_id' => 261,
            ),
            14 =>
            array (
                'destination_id' => 22,
                'tour_id' => 262,
            ),
            15 =>
            array (
                'destination_id' => 1,
                'tour_id' => 263,
            ),
            16 =>
            array (
                'destination_id' => 37,
                'tour_id' => 264,
            ),
            17 =>
            array (
                'destination_id' => 1,
                'tour_id' => 265,
            ),
            18 =>
            array (
                'destination_id' => 1,
                'tour_id' => 266,
            ),
            19 =>
            array (
                'destination_id' => 1,
                'tour_id' => 267,
            ),
            20 =>
            array (
                'destination_id' => 59,
                'tour_id' => 267,
            ),
            21 =>
            array (
                'destination_id' => 1,
                'tour_id' => 268,
            ),
            22 =>
            array (
                'destination_id' => 1,
                'tour_id' => 269,
            ),
            23 =>
            array (
                'destination_id' => 42,
                'tour_id' => 270,
            ),
            24 =>
            array (
                'destination_id' => 43,
                'tour_id' => 270,
            ),
            25 =>
            array (
                'destination_id' => 42,
                'tour_id' => 271,
            ),
            26 =>
            array (
                'destination_id' => 50,
                'tour_id' => 271,
            ),
            27 =>
            array (
                'destination_id' => 1,
                'tour_id' => 272,
            ),
            28 =>
            array (
                'destination_id' => 1,
                'tour_id' => 273,
            ),
            29 =>
            array (
                'destination_id' => 1,
                'tour_id' => 274,
            ),
            30 =>
            array (
                'destination_id' => 1,
                'tour_id' => 275,
            ),
            31 =>
            array (
                'destination_id' => 51,
                'tour_id' => 275,
            ),
            32 =>
            array (
                'destination_id' => 1,
                'tour_id' => 276,
            ),
            33 =>
            array (
                'destination_id' => 1,
                'tour_id' => 277,
            ),
            34 =>
            array (
                'destination_id' => 22,
                'tour_id' => 278,
            ),
            35 =>
            array (
                'destination_id' => 1,
                'tour_id' => 279,
            ),
            36 =>
            array (
                'destination_id' => 45,
                'tour_id' => 279,
            ),
            37 =>
            array (
                'destination_id' => 55,
                'tour_id' => 279,
            ),
            38 =>
            array (
                'destination_id' => 1,
                'tour_id' => 280,
            ),
            39 =>
            array (
                'destination_id' => 42,
                'tour_id' => 281,
            ),
            40 =>
            array (
                'destination_id' => 50,
                'tour_id' => 281,
            ),
            41 =>
            array (
                'destination_id' => 1,
                'tour_id' => 282,
            ),
            42 =>
            array (
                'destination_id' => 1,
                'tour_id' => 283,
            ),
            43 =>
            array (
                'destination_id' => 1,
                'tour_id' => 284,
            ),
            44 =>
            array (
                'destination_id' => 1,
                'tour_id' => 285,
            ),
            45 =>
            array (
                'destination_id' => 1,
                'tour_id' => 286,
            ),
            46 =>
            array (
                'destination_id' => 1,
                'tour_id' => 287,
            ),
            47 =>
            array (
                'destination_id' => 1,
                'tour_id' => 288,
            ),
            48 =>
            array (
                'destination_id' => 42,
                'tour_id' => 289,
            ),
            49 =>
            array (
                'destination_id' => 50,
                'tour_id' => 289,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 42,
                'tour_id' => 290,
            ),
            1 =>
            array (
                'destination_id' => 50,
                'tour_id' => 290,
            ),
            2 =>
            array (
                'destination_id' => 1,
                'tour_id' => 291,
            ),
            3 =>
            array (
                'destination_id' => 22,
                'tour_id' => 292,
            ),
            4 =>
            array (
                'destination_id' => 1,
                'tour_id' => 293,
            ),
            5 =>
            array (
                'destination_id' => 1,
                'tour_id' => 294,
            ),
            6 =>
            array (
                'destination_id' => 1,
                'tour_id' => 295,
            ),
            7 =>
            array (
                'destination_id' => 1,
                'tour_id' => 296,
            ),
            8 =>
            array (
                'destination_id' => 42,
                'tour_id' => 297,
            ),
            9 =>
            array (
                'destination_id' => 43,
                'tour_id' => 297,
            ),
            10 =>
            array (
                'destination_id' => 42,
                'tour_id' => 298,
            ),
            11 =>
            array (
                'destination_id' => 50,
                'tour_id' => 298,
            ),
            12 =>
            array (
                'destination_id' => 42,
                'tour_id' => 299,
            ),
            13 =>
            array (
                'destination_id' => 50,
                'tour_id' => 299,
            ),
            14 =>
            array (
                'destination_id' => 1,
                'tour_id' => 300,
            ),
            15 =>
            array (
                'destination_id' => 42,
                'tour_id' => 301,
            ),
            16 =>
            array (
                'destination_id' => 43,
                'tour_id' => 301,
            ),
            17 =>
            array (
                'destination_id' => 1,
                'tour_id' => 302,
            ),
            18 =>
            array (
                'destination_id' => 42,
                'tour_id' => 303,
            ),
            19 =>
            array (
                'destination_id' => 1,
                'tour_id' => 304,
            ),
            20 =>
            array (
                'destination_id' => 1,
                'tour_id' => 305,
            ),
            21 =>
            array (
                'destination_id' => 42,
                'tour_id' => 306,
            ),
            22 =>
            array (
                'destination_id' => 50,
                'tour_id' => 306,
            ),
            23 =>
            array (
                'destination_id' => 1,
                'tour_id' => 307,
            ),
            24 =>
            array (
                'destination_id' => 51,
                'tour_id' => 307,
            ),
            25 =>
            array (
                'destination_id' => 1,
                'tour_id' => 308,
            ),
            26 =>
            array (
                'destination_id' => 42,
                'tour_id' => 309,
            ),
            27 =>
            array (
                'destination_id' => 43,
                'tour_id' => 309,
            ),
            28 =>
            array (
                'destination_id' => 1,
                'tour_id' => 310,
            ),
            29 =>
            array (
                'destination_id' => 1,
                'tour_id' => 311,
            ),
            30 =>
            array (
                'destination_id' => 1,
                'tour_id' => 312,
            ),
            31 =>
            array (
                'destination_id' => 25,
                'tour_id' => 313,
            ),
            32 =>
            array (
                'destination_id' => 37,
                'tour_id' => 314,
            ),
            33 =>
            array (
                'destination_id' => 38,
                'tour_id' => 314,
            ),
            34 =>
            array (
                'destination_id' => 42,
                'tour_id' => 315,
            ),
            35 =>
            array (
                'destination_id' => 37,
                'tour_id' => 316,
            ),
            36 =>
            array (
                'destination_id' => 38,
                'tour_id' => 316,
            ),
            37 =>
            array (
                'destination_id' => 1,
                'tour_id' => 317,
            ),
            38 =>
            array (
                'destination_id' => 1,
                'tour_id' => 319,
            ),
            39 =>
            array (
                'destination_id' => 42,
                'tour_id' => 320,
            ),
            40 =>
            array (
                'destination_id' => 1,
                'tour_id' => 321,
            ),
            41 =>
            array (
                'destination_id' => 45,
                'tour_id' => 321,
            ),
            42 =>
            array (
                'destination_id' => 49,
                'tour_id' => 321,
            ),
            43 =>
            array (
                'destination_id' => 1,
                'tour_id' => 322,
            ),
            44 =>
            array (
                'destination_id' => 42,
                'tour_id' => 323,
            ),
            45 =>
            array (
                'destination_id' => 50,
                'tour_id' => 323,
            ),
            46 =>
            array (
                'destination_id' => 1,
                'tour_id' => 324,
            ),
            47 =>
            array (
                'destination_id' => 1,
                'tour_id' => 325,
            ),
            48 =>
            array (
                'destination_id' => 1,
                'tour_id' => 326,
            ),
            49 =>
            array (
                'destination_id' => 42,
                'tour_id' => 327,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 50,
                'tour_id' => 327,
            ),
            1 =>
            array (
                'destination_id' => 42,
                'tour_id' => 328,
            ),
            2 =>
            array (
                'destination_id' => 50,
                'tour_id' => 328,
            ),
            3 =>
            array (
                'destination_id' => 1,
                'tour_id' => 329,
            ),
            4 =>
            array (
                'destination_id' => 49,
                'tour_id' => 329,
            ),
            5 =>
            array (
                'destination_id' => 42,
                'tour_id' => 330,
            ),
            6 =>
            array (
                'destination_id' => 50,
                'tour_id' => 330,
            ),
            7 =>
            array (
                'destination_id' => 1,
                'tour_id' => 331,
            ),
            8 =>
            array (
                'destination_id' => 1,
                'tour_id' => 332,
            ),
            9 =>
            array (
                'destination_id' => 1,
                'tour_id' => 333,
            ),
            10 =>
            array (
                'destination_id' => 1,
                'tour_id' => 334,
            ),
            11 =>
            array (
                'destination_id' => 1,
                'tour_id' => 335,
            ),
            12 =>
            array (
                'destination_id' => 1,
                'tour_id' => 336,
            ),
            13 =>
            array (
                'destination_id' => 1,
                'tour_id' => 337,
            ),
            14 =>
            array (
                'destination_id' => 1,
                'tour_id' => 338,
            ),
            15 =>
            array (
                'destination_id' => 1,
                'tour_id' => 339,
            ),
            16 =>
            array (
                'destination_id' => 1,
                'tour_id' => 340,
            ),
            17 =>
            array (
                'destination_id' => 51,
                'tour_id' => 340,
            ),
            18 =>
            array (
                'destination_id' => 1,
                'tour_id' => 341,
            ),
            19 =>
            array (
                'destination_id' => 1,
                'tour_id' => 342,
            ),
            20 =>
            array (
                'destination_id' => 1,
                'tour_id' => 343,
            ),
            21 =>
            array (
                'destination_id' => 25,
                'tour_id' => 344,
            ),
            22 =>
            array (
                'destination_id' => 38,
                'tour_id' => 345,
            ),
            23 =>
            array (
                'destination_id' => 41,
                'tour_id' => 345,
            ),
            24 =>
            array (
                'destination_id' => 1,
                'tour_id' => 346,
            ),
            25 =>
            array (
                'destination_id' => 1,
                'tour_id' => 347,
            ),
            26 =>
            array (
                'destination_id' => 1,
                'tour_id' => 348,
            ),
            27 =>
            array (
                'destination_id' => 23,
                'tour_id' => 349,
            ),
            28 =>
            array (
                'destination_id' => 42,
                'tour_id' => 350,
            ),
            29 =>
            array (
                'destination_id' => 42,
                'tour_id' => 351,
            ),
            30 =>
            array (
                'destination_id' => 37,
                'tour_id' => 352,
            ),
            31 =>
            array (
                'destination_id' => 1,
                'tour_id' => 353,
            ),
            32 =>
            array (
                'destination_id' => 41,
                'tour_id' => 354,
            ),
            33 =>
            array (
                'destination_id' => 37,
                'tour_id' => 355,
            ),
            34 =>
            array (
                'destination_id' => 49,
                'tour_id' => 355,
            ),
            35 =>
            array (
                'destination_id' => 42,
                'tour_id' => 356,
            ),
            36 =>
            array (
                'destination_id' => 42,
                'tour_id' => 357,
            ),
            37 =>
            array (
                'destination_id' => 37,
                'tour_id' => 358,
            ),
            38 =>
            array (
                'destination_id' => 37,
                'tour_id' => 359,
            ),
            39 =>
            array (
                'destination_id' => 38,
                'tour_id' => 360,
            ),
            40 =>
            array (
                'destination_id' => 41,
                'tour_id' => 360,
            ),
            41 =>
            array (
                'destination_id' => 1,
                'tour_id' => 361,
            ),
            42 =>
            array (
                'destination_id' => 37,
                'tour_id' => 362,
            ),
            43 =>
            array (
                'destination_id' => 37,
                'tour_id' => 363,
            ),
            44 =>
            array (
                'destination_id' => 41,
                'tour_id' => 366,
            ),
            45 =>
            array (
                'destination_id' => 68,
                'tour_id' => 366,
            ),
            46 =>
            array (
                'destination_id' => 41,
                'tour_id' => 367,
            ),
            47 =>
            array (
                'destination_id' => 37,
                'tour_id' => 368,
            ),
            48 =>
            array (
                'destination_id' => 37,
                'tour_id' => 369,
            ),
            49 =>
            array (
                'destination_id' => 41,
                'tour_id' => 370,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 60,
                'tour_id' => 371,
            ),
            1 =>
            array (
                'destination_id' => 64,
                'tour_id' => 371,
            ),
            2 =>
            array (
                'destination_id' => 37,
                'tour_id' => 372,
            ),
            3 =>
            array (
                'destination_id' => 60,
                'tour_id' => 373,
            ),
            4 =>
            array (
                'destination_id' => 68,
                'tour_id' => 373,
            ),
            5 =>
            array (
                'destination_id' => 60,
                'tour_id' => 374,
            ),
            6 =>
            array (
                'destination_id' => 60,
                'tour_id' => 375,
            ),
            7 =>
            array (
                'destination_id' => 60,
                'tour_id' => 376,
            ),
            8 =>
            array (
                'destination_id' => 37,
                'tour_id' => 377,
            ),
            9 =>
            array (
                'destination_id' => 46,
                'tour_id' => 377,
            ),
            10 =>
            array (
                'destination_id' => 37,
                'tour_id' => 378,
            ),
            11 =>
            array (
                'destination_id' => 37,
                'tour_id' => 379,
            ),
            12 =>
            array (
                'destination_id' => 46,
                'tour_id' => 379,
            ),
            13 =>
            array (
                'destination_id' => 37,
                'tour_id' => 380,
            ),
            14 =>
            array (
                'destination_id' => 37,
                'tour_id' => 381,
            ),
            15 =>
            array (
                'destination_id' => 47,
                'tour_id' => 382,
            ),
            16 =>
            array (
                'destination_id' => 60,
                'tour_id' => 382,
            ),
            17 =>
            array (
                'destination_id' => 64,
                'tour_id' => 382,
            ),
            18 =>
            array (
                'destination_id' => 60,
                'tour_id' => 383,
            ),
            19 =>
            array (
                'destination_id' => 62,
                'tour_id' => 383,
            ),
            20 =>
            array (
                'destination_id' => 41,
                'tour_id' => 384,
            ),
            21 =>
            array (
                'destination_id' => 68,
                'tour_id' => 384,
            ),
            22 =>
            array (
                'destination_id' => 41,
                'tour_id' => 385,
            ),
            23 =>
            array (
                'destination_id' => 68,
                'tour_id' => 385,
            ),
            24 =>
            array (
                'destination_id' => 41,
                'tour_id' => 386,
            ),
            25 =>
            array (
                'destination_id' => 41,
                'tour_id' => 387,
            ),
            26 =>
            array (
                'destination_id' => 66,
                'tour_id' => 387,
            ),
            27 =>
            array (
                'destination_id' => 41,
                'tour_id' => 388,
            ),
            28 =>
            array (
                'destination_id' => 41,
                'tour_id' => 389,
            ),
            29 =>
            array (
                'destination_id' => 41,
                'tour_id' => 390,
            ),
            30 =>
            array (
                'destination_id' => 41,
                'tour_id' => 391,
            ),
            31 =>
            array (
                'destination_id' => 68,
                'tour_id' => 391,
            ),
            32 =>
            array (
                'destination_id' => 41,
                'tour_id' => 392,
            ),
            33 =>
            array (
                'destination_id' => 63,
                'tour_id' => 392,
            ),
            34 =>
            array (
                'destination_id' => 66,
                'tour_id' => 392,
            ),
            35 =>
            array (
                'destination_id' => 41,
                'tour_id' => 393,
            ),
            36 =>
            array (
                'destination_id' => 66,
                'tour_id' => 393,
            ),
            37 =>
            array (
                'destination_id' => 41,
                'tour_id' => 394,
            ),
            38 =>
            array (
                'destination_id' => 68,
                'tour_id' => 394,
            ),
            39 =>
            array (
                'destination_id' => 41,
                'tour_id' => 395,
            ),
            40 =>
            array (
                'destination_id' => 68,
                'tour_id' => 395,
            ),
            41 =>
            array (
                'destination_id' => 41,
                'tour_id' => 396,
            ),
            42 =>
            array (
                'destination_id' => 66,
                'tour_id' => 396,
            ),
            43 =>
            array (
                'destination_id' => 41,
                'tour_id' => 397,
            ),
            44 =>
            array (
                'destination_id' => 41,
                'tour_id' => 398,
            ),
            45 =>
            array (
                'destination_id' => 41,
                'tour_id' => 399,
            ),
            46 =>
            array (
                'destination_id' => 68,
                'tour_id' => 399,
            ),
            47 =>
            array (
                'destination_id' => 37,
                'tour_id' => 400,
            ),
            48 =>
            array (
                'destination_id' => 63,
                'tour_id' => 400,
            ),
            49 =>
            array (
                'destination_id' => 60,
                'tour_id' => 401,
            ),
        ));
        \DB::table('destination_tour')->insert(array (
            0 =>
            array (
                'destination_id' => 64,
                'tour_id' => 401,
            ),
            1 =>
            array (
                'destination_id' => 60,
                'tour_id' => 402,
            ),
            2 =>
            array (
                'destination_id' => 64,
                'tour_id' => 402,
            ),
            3 =>
            array (
                'destination_id' => 60,
                'tour_id' => 403,
            ),
            4 =>
            array (
                'destination_id' => 61,
                'tour_id' => 403,
            ),
            5 =>
            array (
                'destination_id' => 60,
                'tour_id' => 404,
            ),
            6 =>
            array (
                'destination_id' => 60,
                'tour_id' => 405,
            ),
            7 =>
            array (
                'destination_id' => 60,
                'tour_id' => 406,
            ),
            8 =>
            array (
                'destination_id' => 60,
                'tour_id' => 407,
            ),
            9 =>
            array (
                'destination_id' => 62,
                'tour_id' => 407,
            ),
            10 =>
            array (
                'destination_id' => 63,
                'tour_id' => 407,
            ),
            11 =>
            array (
                'destination_id' => 60,
                'tour_id' => 408,
            ),
            12 =>
            array (
                'destination_id' => 62,
                'tour_id' => 408,
            ),
            13 =>
            array (
                'destination_id' => 63,
                'tour_id' => 408,
            ),
            14 =>
            array (
                'destination_id' => 60,
                'tour_id' => 409,
            ),
            15 =>
            array (
                'destination_id' => 65,
                'tour_id' => 409,
            ),
            16 =>
            array (
                'destination_id' => 60,
                'tour_id' => 410,
            ),
            17 =>
            array (
                'destination_id' => 65,
                'tour_id' => 410,
            ),
            18 =>
            array (
                'destination_id' => 61,
                'tour_id' => 411,
            ),
            19 =>
            array (
                'destination_id' => 61,
                'tour_id' => 412,
            ),
        ));


    }
}
