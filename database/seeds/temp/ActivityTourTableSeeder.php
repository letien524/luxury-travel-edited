<?php

use Illuminate\Database\Seeder;

class ActivityTourTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('activity_tour')->delete();

        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 19,
                'tour_id' => 13,
            ),
            1 =>
            array (
                'activity_id' => 13,
                'tour_id' => 14,
            ),
            2 =>
            array (
                'activity_id' => 12,
                'tour_id' => 15,
            ),
            3 =>
            array (
                'activity_id' => 13,
                'tour_id' => 16,
            ),
            4 =>
            array (
                'activity_id' => 13,
                'tour_id' => 17,
            ),
            5 =>
            array (
                'activity_id' => 19,
                'tour_id' => 18,
            ),
            6 =>
            array (
                'activity_id' => 13,
                'tour_id' => 19,
            ),
            7 =>
            array (
                'activity_id' => 19,
                'tour_id' => 20,
            ),
            8 =>
            array (
                'activity_id' => 13,
                'tour_id' => 21,
            ),
            9 =>
            array (
                'activity_id' => 13,
                'tour_id' => 22,
            ),
            10 =>
            array (
                'activity_id' => 19,
                'tour_id' => 23,
            ),
            11 =>
            array (
                'activity_id' => 12,
                'tour_id' => 24,
            ),
            12 =>
            array (
                'activity_id' => 13,
                'tour_id' => 25,
            ),
            13 =>
            array (
                'activity_id' => 40,
                'tour_id' => 26,
            ),
            14 =>
            array (
                'activity_id' => 19,
                'tour_id' => 27,
            ),
            15 =>
            array (
                'activity_id' => 9,
                'tour_id' => 28,
            ),
            16 =>
            array (
                'activity_id' => 13,
                'tour_id' => 29,
            ),
            17 =>
            array (
                'activity_id' => 19,
                'tour_id' => 30,
            ),
            18 =>
            array (
                'activity_id' => 39,
                'tour_id' => 30,
            ),
            19 =>
            array (
                'activity_id' => 19,
                'tour_id' => 31,
            ),
            20 =>
            array (
                'activity_id' => 13,
                'tour_id' => 32,
            ),
            21 =>
            array (
                'activity_id' => 19,
                'tour_id' => 33,
            ),
            22 =>
            array (
                'activity_id' => 16,
                'tour_id' => 34,
            ),
            23 =>
            array (
                'activity_id' => 16,
                'tour_id' => 35,
            ),
            24 =>
            array (
                'activity_id' => 17,
                'tour_id' => 36,
            ),
            25 =>
            array (
                'activity_id' => 21,
                'tour_id' => 37,
            ),
            26 =>
            array (
                'activity_id' => 16,
                'tour_id' => 38,
            ),
            27 =>
            array (
                'activity_id' => 16,
                'tour_id' => 39,
            ),
            28 =>
            array (
                'activity_id' => 40,
                'tour_id' => 40,
            ),
            29 =>
            array (
                'activity_id' => 18,
                'tour_id' => 41,
            ),
            30 =>
            array (
                'activity_id' => 17,
                'tour_id' => 42,
            ),
            31 =>
            array (
                'activity_id' => 17,
                'tour_id' => 43,
            ),
            32 =>
            array (
                'activity_id' => 12,
                'tour_id' => 44,
            ),
            33 =>
            array (
                'activity_id' => 16,
                'tour_id' => 44,
            ),
            34 =>
            array (
                'activity_id' => 19,
                'tour_id' => 45,
            ),
            35 =>
            array (
                'activity_id' => 17,
                'tour_id' => 46,
            ),
            36 =>
            array (
                'activity_id' => 40,
                'tour_id' => 47,
            ),
            37 =>
            array (
                'activity_id' => 11,
                'tour_id' => 48,
            ),
            38 =>
            array (
                'activity_id' => 17,
                'tour_id' => 48,
            ),
            39 =>
            array (
                'activity_id' => 19,
                'tour_id' => 49,
            ),
            40 =>
            array (
                'activity_id' => 19,
                'tour_id' => 50,
            ),
            41 =>
            array (
                'activity_id' => 16,
                'tour_id' => 51,
            ),
            42 =>
            array (
                'activity_id' => 12,
                'tour_id' => 52,
            ),
            43 =>
            array (
                'activity_id' => 16,
                'tour_id' => 52,
            ),
            44 =>
            array (
                'activity_id' => 11,
                'tour_id' => 53,
            ),
            45 =>
            array (
                'activity_id' => 18,
                'tour_id' => 54,
            ),
            46 =>
            array (
                'activity_id' => 16,
                'tour_id' => 55,
            ),
            47 =>
            array (
                'activity_id' => 19,
                'tour_id' => 56,
            ),
            48 =>
            array (
                'activity_id' => 16,
                'tour_id' => 57,
            ),
            49 =>
            array (
                'activity_id' => 13,
                'tour_id' => 58,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 19,
                'tour_id' => 59,
            ),
            1 =>
            array (
                'activity_id' => 16,
                'tour_id' => 60,
            ),
            2 =>
            array (
                'activity_id' => 19,
                'tour_id' => 61,
            ),
            3 =>
            array (
                'activity_id' => 19,
                'tour_id' => 62,
            ),
            4 =>
            array (
                'activity_id' => 19,
                'tour_id' => 63,
            ),
            5 =>
            array (
                'activity_id' => 13,
                'tour_id' => 64,
            ),
            6 =>
            array (
                'activity_id' => 17,
                'tour_id' => 65,
            ),
            7 =>
            array (
                'activity_id' => 13,
                'tour_id' => 66,
            ),
            8 =>
            array (
                'activity_id' => 17,
                'tour_id' => 67,
            ),
            9 =>
            array (
                'activity_id' => 13,
                'tour_id' => 68,
            ),
            10 =>
            array (
                'activity_id' => 19,
                'tour_id' => 69,
            ),
            11 =>
            array (
                'activity_id' => 12,
                'tour_id' => 70,
            ),
            12 =>
            array (
                'activity_id' => 12,
                'tour_id' => 71,
            ),
            13 =>
            array (
                'activity_id' => 12,
                'tour_id' => 72,
            ),
            14 =>
            array (
                'activity_id' => 12,
                'tour_id' => 73,
            ),
            15 =>
            array (
                'activity_id' => 12,
                'tour_id' => 74,
            ),
            16 =>
            array (
                'activity_id' => 12,
                'tour_id' => 75,
            ),
            17 =>
            array (
                'activity_id' => 13,
                'tour_id' => 76,
            ),
            18 =>
            array (
                'activity_id' => 16,
                'tour_id' => 76,
            ),
            19 =>
            array (
                'activity_id' => 19,
                'tour_id' => 77,
            ),
            20 =>
            array (
                'activity_id' => 19,
                'tour_id' => 78,
            ),
            21 =>
            array (
                'activity_id' => 13,
                'tour_id' => 79,
            ),
            22 =>
            array (
                'activity_id' => 16,
                'tour_id' => 79,
            ),
            23 =>
            array (
                'activity_id' => 13,
                'tour_id' => 80,
            ),
            24 =>
            array (
                'activity_id' => 19,
                'tour_id' => 81,
            ),
            25 =>
            array (
                'activity_id' => 13,
                'tour_id' => 82,
            ),
            26 =>
            array (
                'activity_id' => 19,
                'tour_id' => 83,
            ),
            27 =>
            array (
                'activity_id' => 11,
                'tour_id' => 84,
            ),
            28 =>
            array (
                'activity_id' => 16,
                'tour_id' => 85,
            ),
            29 =>
            array (
                'activity_id' => 17,
                'tour_id' => 86,
            ),
            30 =>
            array (
                'activity_id' => 13,
                'tour_id' => 87,
            ),
            31 =>
            array (
                'activity_id' => 18,
                'tour_id' => 88,
            ),
            32 =>
            array (
                'activity_id' => 19,
                'tour_id' => 89,
            ),
            33 =>
            array (
                'activity_id' => 40,
                'tour_id' => 90,
            ),
            34 =>
            array (
                'activity_id' => 15,
                'tour_id' => 91,
            ),
            35 =>
            array (
                'activity_id' => 19,
                'tour_id' => 92,
            ),
            36 =>
            array (
                'activity_id' => 12,
                'tour_id' => 93,
            ),
            37 =>
            array (
                'activity_id' => 13,
                'tour_id' => 93,
            ),
            38 =>
            array (
                'activity_id' => 17,
                'tour_id' => 94,
            ),
            39 =>
            array (
                'activity_id' => 12,
                'tour_id' => 95,
            ),
            40 =>
            array (
                'activity_id' => 9,
                'tour_id' => 96,
            ),
            41 =>
            array (
                'activity_id' => 12,
                'tour_id' => 97,
            ),
            42 =>
            array (
                'activity_id' => 12,
                'tour_id' => 98,
            ),
            43 =>
            array (
                'activity_id' => 19,
                'tour_id' => 101,
            ),
            44 =>
            array (
                'activity_id' => 19,
                'tour_id' => 102,
            ),
            45 =>
            array (
                'activity_id' => 13,
                'tour_id' => 103,
            ),
            46 =>
            array (
                'activity_id' => 17,
                'tour_id' => 103,
            ),
            47 =>
            array (
                'activity_id' => 13,
                'tour_id' => 104,
            ),
            48 =>
            array (
                'activity_id' => 19,
                'tour_id' => 105,
            ),
            49 =>
            array (
                'activity_id' => 17,
                'tour_id' => 106,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 13,
                'tour_id' => 107,
            ),
            1 =>
            array (
                'activity_id' => 13,
                'tour_id' => 108,
            ),
            2 =>
            array (
                'activity_id' => 13,
                'tour_id' => 109,
            ),
            3 =>
            array (
                'activity_id' => 13,
                'tour_id' => 110,
            ),
            4 =>
            array (
                'activity_id' => 17,
                'tour_id' => 111,
            ),
            5 =>
            array (
                'activity_id' => 17,
                'tour_id' => 112,
            ),
            6 =>
            array (
                'activity_id' => 40,
                'tour_id' => 112,
            ),
            7 =>
            array (
                'activity_id' => 19,
                'tour_id' => 113,
            ),
            8 =>
            array (
                'activity_id' => 19,
                'tour_id' => 114,
            ),
            9 =>
            array (
                'activity_id' => 13,
                'tour_id' => 115,
            ),
            10 =>
            array (
                'activity_id' => 17,
                'tour_id' => 116,
            ),
            11 =>
            array (
                'activity_id' => 19,
                'tour_id' => 117,
            ),
            12 =>
            array (
                'activity_id' => 19,
                'tour_id' => 118,
            ),
            13 =>
            array (
                'activity_id' => 39,
                'tour_id' => 120,
            ),
            14 =>
            array (
                'activity_id' => 10,
                'tour_id' => 122,
            ),
            15 =>
            array (
                'activity_id' => 11,
                'tour_id' => 123,
            ),
            16 =>
            array (
                'activity_id' => 17,
                'tour_id' => 123,
            ),
            17 =>
            array (
                'activity_id' => 13,
                'tour_id' => 124,
            ),
            18 =>
            array (
                'activity_id' => 12,
                'tour_id' => 125,
            ),
            19 =>
            array (
                'activity_id' => 13,
                'tour_id' => 126,
            ),
            20 =>
            array (
                'activity_id' => 11,
                'tour_id' => 127,
            ),
            21 =>
            array (
                'activity_id' => 19,
                'tour_id' => 128,
            ),
            22 =>
            array (
                'activity_id' => 12,
                'tour_id' => 130,
            ),
            23 =>
            array (
                'activity_id' => 11,
                'tour_id' => 131,
            ),
            24 =>
            array (
                'activity_id' => 17,
                'tour_id' => 131,
            ),
            25 =>
            array (
                'activity_id' => 13,
                'tour_id' => 132,
            ),
            26 =>
            array (
                'activity_id' => 19,
                'tour_id' => 133,
            ),
            27 =>
            array (
                'activity_id' => 14,
                'tour_id' => 134,
            ),
            28 =>
            array (
                'activity_id' => 12,
                'tour_id' => 135,
            ),
            29 =>
            array (
                'activity_id' => 18,
                'tour_id' => 136,
            ),
            30 =>
            array (
                'activity_id' => 12,
                'tour_id' => 137,
            ),
            31 =>
            array (
                'activity_id' => 16,
                'tour_id' => 138,
            ),
            32 =>
            array (
                'activity_id' => 17,
                'tour_id' => 139,
            ),
            33 =>
            array (
                'activity_id' => 13,
                'tour_id' => 140,
            ),
            34 =>
            array (
                'activity_id' => 9,
                'tour_id' => 141,
            ),
            35 =>
            array (
                'activity_id' => 19,
                'tour_id' => 142,
            ),
            36 =>
            array (
                'activity_id' => 19,
                'tour_id' => 143,
            ),
            37 =>
            array (
                'activity_id' => 19,
                'tour_id' => 144,
            ),
            38 =>
            array (
                'activity_id' => 11,
                'tour_id' => 145,
            ),
            39 =>
            array (
                'activity_id' => 19,
                'tour_id' => 146,
            ),
            40 =>
            array (
                'activity_id' => 12,
                'tour_id' => 147,
            ),
            41 =>
            array (
                'activity_id' => 19,
                'tour_id' => 148,
            ),
            42 =>
            array (
                'activity_id' => 39,
                'tour_id' => 149,
            ),
            43 =>
            array (
                'activity_id' => 19,
                'tour_id' => 150,
            ),
            44 =>
            array (
                'activity_id' => 19,
                'tour_id' => 151,
            ),
            45 =>
            array (
                'activity_id' => 18,
                'tour_id' => 152,
            ),
            46 =>
            array (
                'activity_id' => 13,
                'tour_id' => 153,
            ),
            47 =>
            array (
                'activity_id' => 19,
                'tour_id' => 154,
            ),
            48 =>
            array (
                'activity_id' => 10,
                'tour_id' => 155,
            ),
            49 =>
            array (
                'activity_id' => 19,
                'tour_id' => 156,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 18,
                'tour_id' => 157,
            ),
            1 =>
            array (
                'activity_id' => 39,
                'tour_id' => 158,
            ),
            2 =>
            array (
                'activity_id' => 39,
                'tour_id' => 159,
            ),
            3 =>
            array (
                'activity_id' => 17,
                'tour_id' => 160,
            ),
            4 =>
            array (
                'activity_id' => 19,
                'tour_id' => 161,
            ),
            5 =>
            array (
                'activity_id' => 19,
                'tour_id' => 162,
            ),
            6 =>
            array (
                'activity_id' => 17,
                'tour_id' => 163,
            ),
            7 =>
            array (
                'activity_id' => 11,
                'tour_id' => 164,
            ),
            8 =>
            array (
                'activity_id' => 13,
                'tour_id' => 165,
            ),
            9 =>
            array (
                'activity_id' => 16,
                'tour_id' => 166,
            ),
            10 =>
            array (
                'activity_id' => 9,
                'tour_id' => 167,
            ),
            11 =>
            array (
                'activity_id' => 13,
                'tour_id' => 168,
            ),
            12 =>
            array (
                'activity_id' => 14,
                'tour_id' => 169,
            ),
            13 =>
            array (
                'activity_id' => 13,
                'tour_id' => 170,
            ),
            14 =>
            array (
                'activity_id' => 18,
                'tour_id' => 171,
            ),
            15 =>
            array (
                'activity_id' => 13,
                'tour_id' => 172,
            ),
            16 =>
            array (
                'activity_id' => 13,
                'tour_id' => 173,
            ),
            17 =>
            array (
                'activity_id' => 14,
                'tour_id' => 174,
            ),
            18 =>
            array (
                'activity_id' => 13,
                'tour_id' => 175,
            ),
            19 =>
            array (
                'activity_id' => 10,
                'tour_id' => 176,
            ),
            20 =>
            array (
                'activity_id' => 16,
                'tour_id' => 177,
            ),
            21 =>
            array (
                'activity_id' => 12,
                'tour_id' => 178,
            ),
            22 =>
            array (
                'activity_id' => 18,
                'tour_id' => 179,
            ),
            23 =>
            array (
                'activity_id' => 21,
                'tour_id' => 180,
            ),
            24 =>
            array (
                'activity_id' => 16,
                'tour_id' => 181,
            ),
            25 =>
            array (
                'activity_id' => 16,
                'tour_id' => 182,
            ),
            26 =>
            array (
                'activity_id' => 12,
                'tour_id' => 183,
            ),
            27 =>
            array (
                'activity_id' => 19,
                'tour_id' => 184,
            ),
            28 =>
            array (
                'activity_id' => 17,
                'tour_id' => 185,
            ),
            29 =>
            array (
                'activity_id' => 39,
                'tour_id' => 186,
            ),
            30 =>
            array (
                'activity_id' => 15,
                'tour_id' => 187,
            ),
            31 =>
            array (
                'activity_id' => 19,
                'tour_id' => 188,
            ),
            32 =>
            array (
                'activity_id' => 39,
                'tour_id' => 189,
            ),
            33 =>
            array (
                'activity_id' => 11,
                'tour_id' => 190,
            ),
            34 =>
            array (
                'activity_id' => 19,
                'tour_id' => 191,
            ),
            35 =>
            array (
                'activity_id' => 39,
                'tour_id' => 192,
            ),
            36 =>
            array (
                'activity_id' => 17,
                'tour_id' => 193,
            ),
            37 =>
            array (
                'activity_id' => 17,
                'tour_id' => 194,
            ),
            38 =>
            array (
                'activity_id' => 18,
                'tour_id' => 195,
            ),
            39 =>
            array (
                'activity_id' => 18,
                'tour_id' => 196,
            ),
            40 =>
            array (
                'activity_id' => 21,
                'tour_id' => 197,
            ),
            41 =>
            array (
                'activity_id' => 16,
                'tour_id' => 198,
            ),
            42 =>
            array (
                'activity_id' => 16,
                'tour_id' => 199,
            ),
            43 =>
            array (
                'activity_id' => 17,
                'tour_id' => 200,
            ),
            44 =>
            array (
                'activity_id' => 19,
                'tour_id' => 201,
            ),
            45 =>
            array (
                'activity_id' => 19,
                'tour_id' => 202,
            ),
            46 =>
            array (
                'activity_id' => 17,
                'tour_id' => 203,
            ),
            47 =>
            array (
                'activity_id' => 39,
                'tour_id' => 203,
            ),
            48 =>
            array (
                'activity_id' => 18,
                'tour_id' => 204,
            ),
            49 =>
            array (
                'activity_id' => 12,
                'tour_id' => 205,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 18,
                'tour_id' => 206,
            ),
            1 =>
            array (
                'activity_id' => 18,
                'tour_id' => 207,
            ),
            2 =>
            array (
                'activity_id' => 19,
                'tour_id' => 208,
            ),
            3 =>
            array (
                'activity_id' => 12,
                'tour_id' => 209,
            ),
            4 =>
            array (
                'activity_id' => 13,
                'tour_id' => 210,
            ),
            5 =>
            array (
                'activity_id' => 18,
                'tour_id' => 211,
            ),
            6 =>
            array (
                'activity_id' => 13,
                'tour_id' => 212,
            ),
            7 =>
            array (
                'activity_id' => 10,
                'tour_id' => 213,
            ),
            8 =>
            array (
                'activity_id' => 19,
                'tour_id' => 214,
            ),
            9 =>
            array (
                'activity_id' => 13,
                'tour_id' => 215,
            ),
            10 =>
            array (
                'activity_id' => 17,
                'tour_id' => 216,
            ),
            11 =>
            array (
                'activity_id' => 39,
                'tour_id' => 216,
            ),
            12 =>
            array (
                'activity_id' => 12,
                'tour_id' => 217,
            ),
            13 =>
            array (
                'activity_id' => 19,
                'tour_id' => 218,
            ),
            14 =>
            array (
                'activity_id' => 17,
                'tour_id' => 219,
            ),
            15 =>
            array (
                'activity_id' => 40,
                'tour_id' => 220,
            ),
            16 =>
            array (
                'activity_id' => 39,
                'tour_id' => 221,
            ),
            17 =>
            array (
                'activity_id' => 16,
                'tour_id' => 222,
            ),
            18 =>
            array (
                'activity_id' => 13,
                'tour_id' => 223,
            ),
            19 =>
            array (
                'activity_id' => 12,
                'tour_id' => 224,
            ),
            20 =>
            array (
                'activity_id' => 11,
                'tour_id' => 225,
            ),
            21 =>
            array (
                'activity_id' => 13,
                'tour_id' => 226,
            ),
            22 =>
            array (
                'activity_id' => 13,
                'tour_id' => 227,
            ),
            23 =>
            array (
                'activity_id' => 39,
                'tour_id' => 228,
            ),
            24 =>
            array (
                'activity_id' => 18,
                'tour_id' => 229,
            ),
            25 =>
            array (
                'activity_id' => 39,
                'tour_id' => 230,
            ),
            26 =>
            array (
                'activity_id' => 12,
                'tour_id' => 231,
            ),
            27 =>
            array (
                'activity_id' => 17,
                'tour_id' => 232,
            ),
            28 =>
            array (
                'activity_id' => 40,
                'tour_id' => 232,
            ),
            29 =>
            array (
                'activity_id' => 19,
                'tour_id' => 233,
            ),
            30 =>
            array (
                'activity_id' => 39,
                'tour_id' => 234,
            ),
            31 =>
            array (
                'activity_id' => 39,
                'tour_id' => 235,
            ),
            32 =>
            array (
                'activity_id' => 17,
                'tour_id' => 236,
            ),
            33 =>
            array (
                'activity_id' => 39,
                'tour_id' => 237,
            ),
            34 =>
            array (
                'activity_id' => 16,
                'tour_id' => 238,
            ),
            35 =>
            array (
                'activity_id' => 12,
                'tour_id' => 239,
            ),
            36 =>
            array (
                'activity_id' => 12,
                'tour_id' => 240,
            ),
            37 =>
            array (
                'activity_id' => 15,
                'tour_id' => 241,
            ),
            38 =>
            array (
                'activity_id' => 21,
                'tour_id' => 242,
            ),
            39 =>
            array (
                'activity_id' => 15,
                'tour_id' => 243,
            ),
            40 =>
            array (
                'activity_id' => 17,
                'tour_id' => 244,
            ),
            41 =>
            array (
                'activity_id' => 12,
                'tour_id' => 245,
            ),
            42 =>
            array (
                'activity_id' => 15,
                'tour_id' => 246,
            ),
            43 =>
            array (
                'activity_id' => 11,
                'tour_id' => 247,
            ),
            44 =>
            array (
                'activity_id' => 15,
                'tour_id' => 248,
            ),
            45 =>
            array (
                'activity_id' => 13,
                'tour_id' => 249,
            ),
            46 =>
            array (
                'activity_id' => 15,
                'tour_id' => 251,
            ),
            47 =>
            array (
                'activity_id' => 14,
                'tour_id' => 252,
            ),
            48 =>
            array (
                'activity_id' => 17,
                'tour_id' => 253,
            ),
            49 =>
            array (
                'activity_id' => 17,
                'tour_id' => 254,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 39,
                'tour_id' => 255,
            ),
            1 =>
            array (
                'activity_id' => 40,
                'tour_id' => 256,
            ),
            2 =>
            array (
                'activity_id' => 17,
                'tour_id' => 257,
            ),
            3 =>
            array (
                'activity_id' => 39,
                'tour_id' => 258,
            ),
            4 =>
            array (
                'activity_id' => 13,
                'tour_id' => 259,
            ),
            5 =>
            array (
                'activity_id' => 20,
                'tour_id' => 260,
            ),
            6 =>
            array (
                'activity_id' => 12,
                'tour_id' => 261,
            ),
            7 =>
            array (
                'activity_id' => 13,
                'tour_id' => 262,
            ),
            8 =>
            array (
                'activity_id' => 12,
                'tour_id' => 263,
            ),
            9 =>
            array (
                'activity_id' => 13,
                'tour_id' => 264,
            ),
            10 =>
            array (
                'activity_id' => 10,
                'tour_id' => 265,
            ),
            11 =>
            array (
                'activity_id' => 12,
                'tour_id' => 266,
            ),
            12 =>
            array (
                'activity_id' => 13,
                'tour_id' => 267,
            ),
            13 =>
            array (
                'activity_id' => 16,
                'tour_id' => 268,
            ),
            14 =>
            array (
                'activity_id' => 15,
                'tour_id' => 269,
            ),
            15 =>
            array (
                'activity_id' => 16,
                'tour_id' => 270,
            ),
            16 =>
            array (
                'activity_id' => 39,
                'tour_id' => 271,
            ),
            17 =>
            array (
                'activity_id' => 19,
                'tour_id' => 272,
            ),
            18 =>
            array (
                'activity_id' => 16,
                'tour_id' => 273,
            ),
            19 =>
            array (
                'activity_id' => 19,
                'tour_id' => 274,
            ),
            20 =>
            array (
                'activity_id' => 19,
                'tour_id' => 275,
            ),
            21 =>
            array (
                'activity_id' => 19,
                'tour_id' => 276,
            ),
            22 =>
            array (
                'activity_id' => 19,
                'tour_id' => 277,
            ),
            23 =>
            array (
                'activity_id' => 12,
                'tour_id' => 278,
            ),
            24 =>
            array (
                'activity_id' => 18,
                'tour_id' => 279,
            ),
            25 =>
            array (
                'activity_id' => 39,
                'tour_id' => 280,
            ),
            26 =>
            array (
                'activity_id' => 39,
                'tour_id' => 281,
            ),
            27 =>
            array (
                'activity_id' => 19,
                'tour_id' => 282,
            ),
            28 =>
            array (
                'activity_id' => 15,
                'tour_id' => 283,
            ),
            29 =>
            array (
                'activity_id' => 15,
                'tour_id' => 284,
            ),
            30 =>
            array (
                'activity_id' => 11,
                'tour_id' => 285,
            ),
            31 =>
            array (
                'activity_id' => 12,
                'tour_id' => 286,
            ),
            32 =>
            array (
                'activity_id' => 39,
                'tour_id' => 287,
            ),
            33 =>
            array (
                'activity_id' => 39,
                'tour_id' => 288,
            ),
            34 =>
            array (
                'activity_id' => 39,
                'tour_id' => 289,
            ),
            35 =>
            array (
                'activity_id' => 39,
                'tour_id' => 290,
            ),
            36 =>
            array (
                'activity_id' => 16,
                'tour_id' => 291,
            ),
            37 =>
            array (
                'activity_id' => 13,
                'tour_id' => 292,
            ),
            38 =>
            array (
                'activity_id' => 19,
                'tour_id' => 293,
            ),
            39 =>
            array (
                'activity_id' => 39,
                'tour_id' => 294,
            ),
            40 =>
            array (
                'activity_id' => 39,
                'tour_id' => 295,
            ),
            41 =>
            array (
                'activity_id' => 39,
                'tour_id' => 296,
            ),
            42 =>
            array (
                'activity_id' => 19,
                'tour_id' => 297,
            ),
            43 =>
            array (
                'activity_id' => 39,
                'tour_id' => 298,
            ),
            44 =>
            array (
                'activity_id' => 39,
                'tour_id' => 299,
            ),
            45 =>
            array (
                'activity_id' => 19,
                'tour_id' => 300,
            ),
            46 =>
            array (
                'activity_id' => 9,
                'tour_id' => 301,
            ),
            47 =>
            array (
                'activity_id' => 10,
                'tour_id' => 302,
            ),
            48 =>
            array (
                'activity_id' => 17,
                'tour_id' => 303,
            ),
            49 =>
            array (
                'activity_id' => 17,
                'tour_id' => 304,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 17,
                'tour_id' => 305,
            ),
            1 =>
            array (
                'activity_id' => 39,
                'tour_id' => 306,
            ),
            2 =>
            array (
                'activity_id' => 19,
                'tour_id' => 307,
            ),
            3 =>
            array (
                'activity_id' => 39,
                'tour_id' => 308,
            ),
            4 =>
            array (
                'activity_id' => 16,
                'tour_id' => 309,
            ),
            5 =>
            array (
                'activity_id' => 39,
                'tour_id' => 310,
            ),
            6 =>
            array (
                'activity_id' => 39,
                'tour_id' => 311,
            ),
            7 =>
            array (
                'activity_id' => 12,
                'tour_id' => 312,
            ),
            8 =>
            array (
                'activity_id' => 13,
                'tour_id' => 313,
            ),
            9 =>
            array (
                'activity_id' => 20,
                'tour_id' => 314,
            ),
            10 =>
            array (
                'activity_id' => 13,
                'tour_id' => 315,
            ),
            11 =>
            array (
                'activity_id' => 13,
                'tour_id' => 316,
            ),
            12 =>
            array (
                'activity_id' => 39,
                'tour_id' => 317,
            ),
            13 =>
            array (
                'activity_id' => 12,
                'tour_id' => 319,
            ),
            14 =>
            array (
                'activity_id' => 17,
                'tour_id' => 320,
            ),
            15 =>
            array (
                'activity_id' => 16,
                'tour_id' => 321,
            ),
            16 =>
            array (
                'activity_id' => 17,
                'tour_id' => 322,
            ),
            17 =>
            array (
                'activity_id' => 19,
                'tour_id' => 322,
            ),
            18 =>
            array (
                'activity_id' => 17,
                'tour_id' => 323,
            ),
            19 =>
            array (
                'activity_id' => 39,
                'tour_id' => 323,
            ),
            20 =>
            array (
                'activity_id' => 17,
                'tour_id' => 324,
            ),
            21 =>
            array (
                'activity_id' => 39,
                'tour_id' => 324,
            ),
            22 =>
            array (
                'activity_id' => 39,
                'tour_id' => 325,
            ),
            23 =>
            array (
                'activity_id' => 39,
                'tour_id' => 326,
            ),
            24 =>
            array (
                'activity_id' => 17,
                'tour_id' => 327,
            ),
            25 =>
            array (
                'activity_id' => 39,
                'tour_id' => 327,
            ),
            26 =>
            array (
                'activity_id' => 39,
                'tour_id' => 328,
            ),
            27 =>
            array (
                'activity_id' => 40,
                'tour_id' => 329,
            ),
            28 =>
            array (
                'activity_id' => 17,
                'tour_id' => 330,
            ),
            29 =>
            array (
                'activity_id' => 39,
                'tour_id' => 330,
            ),
            30 =>
            array (
                'activity_id' => 17,
                'tour_id' => 331,
            ),
            31 =>
            array (
                'activity_id' => 39,
                'tour_id' => 331,
            ),
            32 =>
            array (
                'activity_id' => 17,
                'tour_id' => 332,
            ),
            33 =>
            array (
                'activity_id' => 39,
                'tour_id' => 332,
            ),
            34 =>
            array (
                'activity_id' => 15,
                'tour_id' => 333,
            ),
            35 =>
            array (
                'activity_id' => 17,
                'tour_id' => 334,
            ),
            36 =>
            array (
                'activity_id' => 39,
                'tour_id' => 334,
            ),
            37 =>
            array (
                'activity_id' => 39,
                'tour_id' => 335,
            ),
            38 =>
            array (
                'activity_id' => 17,
                'tour_id' => 336,
            ),
            39 =>
            array (
                'activity_id' => 19,
                'tour_id' => 336,
            ),
            40 =>
            array (
                'activity_id' => 39,
                'tour_id' => 337,
            ),
            41 =>
            array (
                'activity_id' => 16,
                'tour_id' => 338,
            ),
            42 =>
            array (
                'activity_id' => 12,
                'tour_id' => 339,
            ),
            43 =>
            array (
                'activity_id' => 12,
                'tour_id' => 340,
            ),
            44 =>
            array (
                'activity_id' => 17,
                'tour_id' => 341,
            ),
            45 =>
            array (
                'activity_id' => 9,
                'tour_id' => 342,
            ),
            46 =>
            array (
                'activity_id' => 16,
                'tour_id' => 343,
            ),
            47 =>
            array (
                'activity_id' => 13,
                'tour_id' => 344,
            ),
            48 =>
            array (
                'activity_id' => 13,
                'tour_id' => 345,
            ),
            49 =>
            array (
                'activity_id' => 39,
                'tour_id' => 346,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 39,
                'tour_id' => 347,
            ),
            1 =>
            array (
                'activity_id' => 19,
                'tour_id' => 348,
            ),
            2 =>
            array (
                'activity_id' => 13,
                'tour_id' => 349,
            ),
            3 =>
            array (
                'activity_id' => 19,
                'tour_id' => 350,
            ),
            4 =>
            array (
                'activity_id' => 39,
                'tour_id' => 351,
            ),
            5 =>
            array (
                'activity_id' => 13,
                'tour_id' => 352,
            ),
            6 =>
            array (
                'activity_id' => 16,
                'tour_id' => 353,
            ),
            7 =>
            array (
                'activity_id' => 12,
                'tour_id' => 354,
            ),
            8 =>
            array (
                'activity_id' => 10,
                'tour_id' => 355,
            ),
            9 =>
            array (
                'activity_id' => 39,
                'tour_id' => 356,
            ),
            10 =>
            array (
                'activity_id' => 39,
                'tour_id' => 357,
            ),
            11 =>
            array (
                'activity_id' => 12,
                'tour_id' => 358,
            ),
            12 =>
            array (
                'activity_id' => 10,
                'tour_id' => 359,
            ),
            13 =>
            array (
                'activity_id' => 12,
                'tour_id' => 360,
            ),
            14 =>
            array (
                'activity_id' => 12,
                'tour_id' => 361,
            ),
            15 =>
            array (
                'activity_id' => 12,
                'tour_id' => 362,
            ),
            16 =>
            array (
                'activity_id' => 10,
                'tour_id' => 363,
            ),
            17 =>
            array (
                'activity_id' => 18,
                'tour_id' => 363,
            ),
            18 =>
            array (
                'activity_id' => 12,
                'tour_id' => 366,
            ),
            19 =>
            array (
                'activity_id' => 12,
                'tour_id' => 367,
            ),
            20 =>
            array (
                'activity_id' => 12,
                'tour_id' => 368,
            ),
            21 =>
            array (
                'activity_id' => 10,
                'tour_id' => 369,
            ),
            22 =>
            array (
                'activity_id' => 13,
                'tour_id' => 370,
            ),
            23 =>
            array (
                'activity_id' => 10,
                'tour_id' => 371,
            ),
            24 =>
            array (
                'activity_id' => 10,
                'tour_id' => 372,
            ),
            25 =>
            array (
                'activity_id' => 12,
                'tour_id' => 373,
            ),
            26 =>
            array (
                'activity_id' => 12,
                'tour_id' => 374,
            ),
            27 =>
            array (
                'activity_id' => 12,
                'tour_id' => 375,
            ),
            28 =>
            array (
                'activity_id' => 10,
                'tour_id' => 376,
            ),
            29 =>
            array (
                'activity_id' => 12,
                'tour_id' => 377,
            ),
            30 =>
            array (
                'activity_id' => 10,
                'tour_id' => 378,
            ),
            31 =>
            array (
                'activity_id' => 12,
                'tour_id' => 379,
            ),
            32 =>
            array (
                'activity_id' => 10,
                'tour_id' => 380,
            ),
            33 =>
            array (
                'activity_id' => 12,
                'tour_id' => 381,
            ),
            34 =>
            array (
                'activity_id' => 12,
                'tour_id' => 382,
            ),
            35 =>
            array (
                'activity_id' => 12,
                'tour_id' => 383,
            ),
            36 =>
            array (
                'activity_id' => 12,
                'tour_id' => 384,
            ),
            37 =>
            array (
                'activity_id' => 13,
                'tour_id' => 385,
            ),
            38 =>
            array (
                'activity_id' => 12,
                'tour_id' => 386,
            ),
            39 =>
            array (
                'activity_id' => 12,
                'tour_id' => 387,
            ),
            40 =>
            array (
                'activity_id' => 12,
                'tour_id' => 388,
            ),
            41 =>
            array (
                'activity_id' => 12,
                'tour_id' => 389,
            ),
            42 =>
            array (
                'activity_id' => 10,
                'tour_id' => 390,
            ),
            43 =>
            array (
                'activity_id' => 12,
                'tour_id' => 391,
            ),
            44 =>
            array (
                'activity_id' => 12,
                'tour_id' => 392,
            ),
            45 =>
            array (
                'activity_id' => 13,
                'tour_id' => 393,
            ),
            46 =>
            array (
                'activity_id' => 18,
                'tour_id' => 394,
            ),
            47 =>
            array (
                'activity_id' => 12,
                'tour_id' => 395,
            ),
            48 =>
            array (
                'activity_id' => 12,
                'tour_id' => 396,
            ),
            49 =>
            array (
                'activity_id' => 12,
                'tour_id' => 397,
            ),
        ));
        \DB::table('activity_tour')->insert(array (
            0 =>
            array (
                'activity_id' => 10,
                'tour_id' => 398,
            ),
            1 =>
            array (
                'activity_id' => 12,
                'tour_id' => 399,
            ),
            2 =>
            array (
                'activity_id' => 10,
                'tour_id' => 400,
            ),
            3 =>
            array (
                'activity_id' => 12,
                'tour_id' => 401,
            ),
            4 =>
            array (
                'activity_id' => 13,
                'tour_id' => 402,
            ),
            5 =>
            array (
                'activity_id' => 12,
                'tour_id' => 403,
            ),
            6 =>
            array (
                'activity_id' => 12,
                'tour_id' => 404,
            ),
            7 =>
            array (
                'activity_id' => 12,
                'tour_id' => 405,
            ),
            8 =>
            array (
                'activity_id' => 13,
                'tour_id' => 406,
            ),
            9 =>
            array (
                'activity_id' => 13,
                'tour_id' => 407,
            ),
            10 =>
            array (
                'activity_id' => 13,
                'tour_id' => 408,
            ),
            11 =>
            array (
                'activity_id' => 17,
                'tour_id' => 409,
            ),
            12 =>
            array (
                'activity_id' => 17,
                'tour_id' => 410,
            ),
            13 =>
            array (
                'activity_id' => 13,
                'tour_id' => 411,
            ),
            14 =>
            array (
                'activity_id' => 18,
                'tour_id' => 412,
            ),
        ));


    }
}
