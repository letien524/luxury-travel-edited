<?php

use Illuminate\Database\Seeder;

class ReviewCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('review_categories')->delete();
        
        \DB::table('review_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Thailand Cambodia Vietnam',
                'created_at' => '2019-10-21 11:24:54',
                'updated_at' => '2019-10-21 11:24:54',
            ),
        ));
        
        
    }
}