<?php

use Illuminate\Database\Seeder;

class TourActivitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tour_activities')->delete();
        
        \DB::table('tour_activities')->insert(array (
            0 => 
            array (
                'id' => 9,
                'name' => 'Culinary Tours',
                'slug' => 'culinary-tours',
                'image' => '/uploads/images/food1-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/culninary-banner1.jpg","name":"Falling in Love with the Exquisite Tastes"},"bottom":{"image":null}},"name":"Our Best Vietnam <br> Culinary & Food Tours","content":"Along with exploring new cultures, one of the great joys of travel is the discovery of new cuisines. Those unfamiliar with Vietnamese food are in for a treat, as the national cuisine is flavorful, colorful and extremely healthy. We are experts in cooking tours and our packages range from half-day gourmet tour samplings to 16-day tours which introduce you to the finest cuisine Vietnam has to offer. In this brochure, we introduce you to a cooking class focusing on the cuisine of each of Vietnam\\u2019s three regions.\\r\\nOur culinary & food tours that combine the joys of experiencing a new country in-depth, getting to the heart of its history along with tasting new exotic flavors are among the amazing experiences you can have in our Culinary Tours in Vietnam."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:22:43',
                'updated_at' => '2019-08-04 09:23:20',
            ),
            1 => 
            array (
                'id' => 10,
                'name' => 'Family Tours',
                'slug' => 'family-tours',
                'image' => '/uploads/images/family-feature-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/beach1.jpg","name":"Create memories that will last for a lifetime."},"bottom":{"image":null}},"name":"Vietnam Family <br>Tours & Vacations<\\/br>","content":"Vietnam is perfectly suited for a family adventure and holiday. Children have a natural curiosity and quickly break through cultural and language barriers, particularly in Indochina where children are revered. Take a journey together from Hanoi to historic Hue and Hoi An, while also visiting the beautiful beaches in Nha Trang and the modern metropolis of Ho Chi Minh City. On our Vietnam Family Tours, the whole family is certain to be entertained by the people, and the rich history of the country."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:23:41',
                'updated_at' => '2019-08-04 09:27:51',
            ),
            2 => 
            array (
                'id' => 11,
                'name' => 'Biking & Trekking',
                'slug' => 'biking-trekking',
                'image' => '/uploads/images/hiking-feature-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/hiking-banner.jpg","name":"Discover the beauties and charms of Vietnam on a sporty adventure"},"bottom":{"image":"\\/uploads\\/files\\/Beautiful sunset reflected in the Lak lake, Buon Me Thuot.jpg"}},"name":"Our Best Vietnam <br>Hiking\\/Trekking Tour<\\/br>","content":"Trekking among the rice terraces in Sapa, going for a bicycle excursion in Mai Chau Valley and around the temples of Ninh Binh and Tam Coc. Vietnam holds so many natural and scenic places in its Northern Regions ideal for a relaxing and peaceful time.\\r\\nOur hiking and trekking tours include as well all the best destinations around Vietnam like The Mekong Delta area, the farm and fishing villages around Hoi An or a day tour in Hanoi.\\r\\nGet a closer look at the magnificent natural setting that Vietnam holds and make the best out of your Vietnam Holiday with a biking tour around the city or in the countryside or a trekking tour adapted to your pace."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:23:51',
                'updated_at' => '2019-08-04 09:27:21',
            ),
            3 => 
            array (
                'id' => 12,
                'name' => 'Culture Tours',
                'slug' => 'culture-tours',
                'image' => '/uploads/images/culture-feature-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/culture-banner.jpg","name":"Journeying through Southeast Asia\'s fine art & Culture"},"bottom":{"image":"\\/uploads\\/images\\/shutterstock_111.jpg"}},"name":"Our Best Culture Tours","content":"Travelers rave about Vietnam \\u2018s incredibly friendly people, unique 54 traditions, beautiful landscape, and countryside. Vietnam has established its reputation as a unique and rich Southeast Asian experience. In a Vietnam Holiday, there is ample opportunity to explore it in style and luxury. If you choose to take one of our Cultural Tours of Vietnam, you will be surprised by how many things you will discover. You can wander the charming colonial boulevards of Hanoi that will remind you of old times, then go to one of its many interesting Museums such as the Ethnology Museum, the Ho Chi Minh Complex or the Women\\u2019s Museum. And later on, embark on a peaceful cruise in the sensuous, serene scenery of Halong Bay."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => 1,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:23:59',
                'updated_at' => '2019-08-04 09:33:58',
            ),
            4 => 
            array (
                'id' => 13,
                'name' => 'Adventure Tours',
                'slug' => 'adventure-tours',
                'image' => '/uploads/images/adventure-feature-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/adventure-banner.jpg","name":"Unknown pleasures await \\u2013 come find them!"},"bottom":{"image":"\\/uploads\\/files\\/adventure-banner-duoi2.jpg"}},"name":"Our Best Vietnam Adventure Tours","content":"Vietnam is perfectly suited for a luxury adventure. You will quickly break through cultural and language barriers as your guide shows the best of this beautiful country. Our luxury adventures have been specially designed for the needs of upscale clients \\u2013 minimizing long journeys and leaving you with plenty of time to relax on beaches, explore nature trails and view the wide array of Vietnamese wildlife. You will travel by luxury train, fly on a chartered helicopter, explore the tropical jungle and trek to hill tribe villages before returning to the comfort of your luxury vacation home or villa. You\\u2019ll discover the world heritage site of Halong Bay aboard a traditional junk. You will also experience the sea kayak and explore the charming UNESCO-designated World Heritage town of Hoi An and its nearby beaches before concluding your journey in the modern metropolis of Ho Chi Minh City. On this forest & beach adventure, you are certain to be entertained by the people, history, color and culture of Vietnam."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:06',
                'updated_at' => '2019-08-04 09:17:03',
            ),
            5 => 
            array (
                'id' => 14,
                'name' => 'Honeymoon Tours',
                'slug' => 'honeymoon-tours',
                'image' => '/uploads/images/honeymoon-feature-resize-12.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/honeymoon4.jpg","name":"Celebrate your love & romance amidst the picturesque destinations"},"bottom":{"image":"\\/uploads\\/files\\/honeymoon-banner3.jpg"}},"name":"Vietnam Honeymoon Tours\\/Vacations","content":"Vietnam is one of the most romantic and fascinating destinations in Asia. We are here to help you with an array of attractive honeymoon packages at the most romantic and luxurious hotels and resorts in Asia. Create your perfect wedding party, honeymoon vacation or special anniversaries with the help of our Travel Consultants.<br>So what\\u2019s different about our Romantic Getaways and Honeymoon Holidays? We want to make this experience as personal and memorable as possible because we know that the first impression lasts forever. You will be met and presented with flowers and a welcome dinner on arrival day. You will also find chocolates and flowers in the room, and we have added a few extra touches such as limo pick-ups and transfers, hotel upgrades, champagne, couple\\u2019s spa treatment, private picnics, private sunset cruises and a lot of other nice surprises! What a lovely holiday! <\\/br>"}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:11',
                'updated_at' => '2019-08-04 09:14:12',
            ),
            6 => 
            array (
                'id' => 15,
                'name' => 'Golf Tours',
                'slug' => 'golf-tours',
                'image' => '/uploads/images/golf-feature-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/golf-banner.jpg","name":"Striking over the sand dunes and mountains"},"bottom":{"image":"\\/uploads\\/images\\/Golf-banner-duoi1.jpg"}},"name":"Our Best Vietnam <br>Golf Tours\\/Courses<\\/br>","content":"Vietnam is becoming more and more a premium destination for a golf vacation in Southeast Asia. The main reason to choose Vietnam as a perfect destination for golf lovers is firstly the perfect climate year-round as well as the variety of luxury golf resorts to stay in the country. However, golf isn\\u2019t the only thing that attracts visitors to travel to Vietnam and spend their time, Vietnam is a country with unlimited attractions, sites and natural wonders that caters anyone to come and enjoy its charming beauty.<br>The best golf trails of Vietnam include Ho Chi Minh City, Da Lat, and Da Nang golf courses. With so much adventure and beauty that Vietnam offers you will experience your favorite thrilling and luxury sport with the help of our luxury tour operators. Find out the best holiday to Vietnam and stay in luxury and comfort resorts while enjoying the mesmerizing beauty of Vietnam.<\\/br>"}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:18',
                'updated_at' => '2019-08-04 09:32:48',
            ),
            7 => 
            array (
                'id' => 16,
                'name' => 'Stopover Tours',
                'slug' => 'stopover-tours',
                'image' => '/uploads/files/strop-over-feature12.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/stop4.jpg","name":"Journey through the marvels of nature, culture, and adventure!"},"bottom":{"image":null}},"name":"Our Best Vietnam Stopover Tours","content":"We have designed these special stopovers and short trips for people keen to experience the best of each destination of Vietnam but with limited time to spare. Those tours packages will allow you to enjoy Vietnamese culture and art in combination with nature sightseeing, luxurious accommodations, transfers in private vehicles and fine dining. During your free time, challenge your skills at the country\\u2019s most spectacular golf courses. Soak up the sun while being soothed by the sound of breaking surf. Hunt for high-fashion couture in the most elegant cities of Vietnam. You\\u2019ll be provided with private, professional guides who will ensure that you get the most out of Vietnam by sharing their local knowledge and passion of art, culture or nature.\\r\\nEmbark on one of our Stopover Tours an enjoy the best of a Vietnam Holiday with the best organization so you have the itinerary that suits your needs and time."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:28',
                'updated_at' => '2019-08-04 09:00:58',
            ),
            8 => 
            array (
                'id' => 17,
                'name' => 'Excursion Tours',
                'slug' => 'excursion-tours',
                'image' => '/uploads/images/thailand-tour-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/_excursion-banner12.jpg","name":"Make your visit enriching, comfortable and fun"},"bottom":{"image":null}},"name":"Best of Our<br>Excursion Tours <\\/br>","content":"With many travelers to contend with, the natural and man-made wonders of \\bSoutheast Asia often feel like they\'ve been placed here just for you. And what wonders they are: the beautiful rice terraces in Far North Vietnam, the impressive caves in Phong Nha Ke Bang, and the colorful floating markets in Mekong Delta, the staggering imperial ruins of ancient Angkor Wat, and the incomparable palaces of Mandalay, etc. Here we have featured our excursion tours from the major cities to all of those wonders. You can easily add an extension to other nearby destinations at any point during your stay in Vietnam and other countries in Southeast Asia. For your convenience, all of the tours are private, depart daily, and can be customized to suit individual interests and schedules."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:36',
                'updated_at' => '2019-08-04 09:45:25',
            ),
            9 => 
            array (
                'id' => 18,
                'name' => 'Vietnam Beach Vacation',
                'slug' => 'vietnam-beach-vacation',
                'image' => '/uploads/images/beach8-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/family-banner.jpg","name":"Live in the sunshine. Swim in the sea. Drink in the wild air"},"bottom":{"image":"\\/uploads\\/images\\/beach-banner-duoi22.jpg"}},"name":"Vietnam Beach <br>Holidays & Vacations <\\/br>","content":"Vietnam has astonishing and varied geography, delicious cuisine, and beautiful beaches that you can enjoy all year long. From all the reasons to travel to Vietnam, the beaches are definitely a major one.\\r\\nFrom the islands near Halong Bay and the Cat Ba Archipelago all the way down to the secluded island of Phu Quoc, Vietnam offers many possibilities to enjoy the beach vacations. You can choose to discover the central coast in Hue, Hoi An, and Danang, or Mui Ne Beach which is the perfect place for adventurers who can enjoy activities such as kitesurfing, windsurfing or sand-boarding on its very famous sand dunes. Don\\u2019t forget Nha Trang - one of the 29 most beautiful bays in the world. Escape the cold and come to Vietnam to enjoy a Vietnam Beach Holiday with our paradisiac itineraries.\\r\\nIt\'s also a great idea to combine your Vietnam Beach Holiday with a trip to the most beautiful beach in Cambodia or in Thailand."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:41',
                'updated_at' => '2019-08-04 09:36:21',
            ),
            10 => 
            array (
                'id' => 19,
                'name' => 'Day Trips',
                'slug' => 'day-trips',
                'image' => '/uploads/images/day-trip-feature-ft.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/day-trip-banner.jpg","name":"Your Best Introductions to the natural and man-made wonders"},"bottom":{"image":"\\/uploads\\/files\\/shutterstock_11.jpg"}},"name":"Our Vietnam Day Trips & City Tours","content":"There is a variety of day trips you can choose from whether you wish to simply add a day excursion departing from the major cities in Vietnam including Hanoi, Ho Chi Minh City, Nha Trang, and Da Nang. You can opt for an amazing sightseeing tour by Seaplane to Halong Bay, Vintage Citro\\u00ebn Ride and dine in style in Hanoi or an incredible day excursion to the Phong Nha National Park and its impressive caves. These day trips and city tours are your best introductions to the key cities, famous tourist attractions and world heritage sites.<br> All of our Day trip tours in Vietnam include private transportation and guide to accompany you all day and tell you more about the region, our costumes, and history. Don\\u2019t miss out on discovering a place in depth thanks to the help of our agents and guides.<\\/br>"}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:47',
                'updated_at' => '2019-08-04 09:20:21',
            ),
            11 => 
            array (
                'id' => 20,
                'name' => 'Set Departure',
                'slug' => 'set-departure',
                'image' => '/uploads/images/buon-me-thuot-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/set-separture-banner.jpg","name":"Unlimited Authentic Experiences on a Budget"},"bottom":{"image":"\\/uploads\\/files\\/setdeparture-banner-duoi2.jpg"}},"name":"Our Best <br>Set Departure Tours<\\/br>","content":"The Set Departure tours were designed thinking as a different experience through the most diverse activities. Our Vietnam trips for groups are aimed at those who opt for travel packages to Vietnam at a reasonable price and are willing to share their excursions with other travelers.\\r\\nThe activities are fun and playful and you can travel with friends and with family or for those who travel alone, also a chance to make new friends.\\r\\nEnjoy the magical mix of tropical beaches, post-colonial charm, a string of world heritage sites, stunning inland scenery, world-renowned cuisine and a home-grown flair for hospitality in a group tour of Vietnam.\\r\\nOur Set Departure tours let you benefit from a group price in one of the most attractive destinations in Southeast Asia. You will visit all the must-see cities in Vietnam from North to South and stay in luxury hotels, dine in the best restaurants always in the most comfortable transports."}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:24:53',
                'updated_at' => '2019-08-04 09:21:36',
            ),
            12 => 
            array (
                'id' => 21,
                'name' => 'Ultra-Luxury Wellness & Spa Packages',
                'slug' => 'ultra-luxury-wellness-spa-packages',
                'image' => '/uploads/images/spa-feature-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/spa-banner.jpg","name":"Experience True Relaxion & Tranquility with the Tailor-made Treatments and Unique Therapies"},"bottom":{"image":"\\/uploads\\/files\\/spa-banner-duoi1.jpg"}},"name":"Luxury Wellness <br> & Spa Packages <\\/br>","content":"Luxury Travel can help you achieve your wellness goals with tours including more than just sightseeing, but also a chance to escape daily stress, free your mind, body, soul and rejuvenate yourself. Our rejuvenating activities in the Wellness and Spa tours combine a cultural experience journey through some of the most peaceful and spiritual destinations in the world. You can enjoy Buddhist meditation with a monk, visit pagodas and practice tai chi or yoga in the morning. You\\u2019ll also taste local cuisine in the best restaurants and learn the art of balance between yin and yang in your daily meal. You\\u2019ll indulge some hot stone massage on a private beach at sunset and rejuvenate with hydrotherapy, mud masks, and facials. <br>Join those extraordinary destinations for Wellness and spa, in the best resorts, handpicked by our experts.<\\/br>"}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-05-10 07:25:01',
                'updated_at' => '2019-08-04 09:23:38',
            ),
            13 => 
            array (
                'id' => 39,
                'name' => 'Luxury Cruise Tours',
                'slug' => 'luxury-cruise-tours',
                'image' => '/uploads/images/cruise-tour-banner-chuan.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/bannercruise1.jpg","name":"Cruising, it is all about you and the experience."},"bottom":{"image":"\\/uploads\\/images\\/cruise-banner-duoi1.jpg"}},"name":"Vietnam Luxury <br>Cruise Tours<\\/br>","content":"Indulge yourself with a once-in-a-lifetime experience in Vietnam by embarking on our luxury cruises and cruising around some of the most beautiful bays and islands in Southeast Asia including Bai Tu Long Bay, Halong Bay, Nha Trang Bay, and Cat Ba Island. Venture off-the-beaten-track, go kayaking and visit floating fish farms and villages to experience the daily life of the locals. There is plenty of time for nature lovers to discover many secluded beaches, lagoons, coves, and caves.<br> This is your opportunity to escape, do a little soul searching and return completely refreshed. <\\/br>"}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-06-15 11:32:46',
                'updated_at' => '2019-08-04 09:18:59',
            ),
            14 => 
            array (
                'id' => 40,
                'name' => 'Vietnam River Cruise',
                'slug' => 'vietnam-river-cruise',
                'image' => '/uploads/images/mekong-Cruise-feature-rs.jpg',
                'excerpt' => NULL,
                'content' => '{"banner":{"top":{"image":"\\/uploads\\/files\\/banner-mekong-cruise.jpg","name":"Experience the quintessence of Vietnam & Mekong Delta"},"bottom":{"image":"\\/uploads\\/files\\/mekong-cruise-banner-duoi1.jpg"}},"name":"Our Best Vietnam & Mekong River Cruises","content":"Deep in history and rich in culture, Mekong River one of the last travel frontiers with stunning natural beauty and incredible cultural diversity.Our Vietnam River Cruise Tours and Mekong River Cruise Tours highlight the evocative heritages, lifestyle, and cuisine of Indochina. Visiting colorful floating markets, delighting yourself with tasty coconut drinks, discovering greeny fruit orchards while listening to the traditional folk music, etc. Culture packed, foodie fabulous and with time to explore, this is the only way to truly go local"}',
                'status' => 1,
                'order_menu' => NULL,
                'faq_id' => NULL,
                'member_id' => NULL,
                'review_category_id' => NULL,
                'canonical' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'index' => 1,
                'created_at' => '2019-06-17 16:29:56',
                'updated_at' => '2019-08-04 09:27:36',
            ),
        ));
        
        
    }
}