<?php

use Illuminate\Database\Seeder;

class BlogCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blog_category')->delete();
        
        \DB::table('blog_category')->insert(array (
            0 => 
            array (
                'id' => 12,
                'post_id' => 29,
                'category_id' => 5,
            ),
        ));
        
        
    }
}