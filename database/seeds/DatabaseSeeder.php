<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(LangsTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(ConfigsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(MembersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ToursTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
//        $this->call(MigrationsTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(RedirectsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(BlogTagTableSeeder::class);
        $this->call(OrderCategoryTableSeeder::class);
        $this->call(TourDestinationsTableSeeder::class);
        $this->call(TourActivitiesTableSeeder::class);
        $this->call(ActivityTourTableSeeder::class);
        $this->call(BlogCategoryTableSeeder::class);
        $this->call(FaqMemberTableSeeder::class);
        $this->call(MemberReviewTableSeeder::class);
        $this->call(MemberTourTableSeeder::class);
        $this->call(ReviewCategoriesTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);
        $this->call(CategoryReviewTableSeeder::class);
        $this->call(ReviewTourTableSeeder::class);
        $this->call(ReviewTourMemberTableSeeder::class);
        $this->call(HotelCitiesTableSeeder::class);
        $this->call(HotelRoomTypesTableSeeder::class);
        $this->call(HotelTypesTableSeeder::class);
        $this->call(HotelsTableSeeder::class);
        $this->call(HotelCityTourTableSeeder::class);
        $this->call(DestinationTourTableSeeder::class);
//        $this->call(ActivityReviewTableSeeder::class);
//        $this->call(DestinationReviewTableSeeder::class);
        $this->call(HotelTourTableSeeder::class);
//        $this->call(JobsTableSeeder::class);
    }
}
