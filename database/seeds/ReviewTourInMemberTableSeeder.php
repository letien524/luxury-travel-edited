<?php

use Illuminate\Database\Seeder;

class ReviewTourInMemberTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('review_tour_in_member')->delete();
        
        \DB::table('review_tour_in_member')->insert(array (
            0 => 
            array (
                'review_id' => 6,
                'tour_id' => 123,
            ),
        ));
        
        
    }
}