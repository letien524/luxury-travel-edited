<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'superadmin',
                'email' => 'letien524@gmail.com',
                'password' => '$2y$10$Gm/YahfYAkphcr4FOckToeB16mVeYpoeK29Xf9QyMKK0Ik5kOY7G6',
                'image' => NULL,
                'level' => 1,
                'remember_token' => 'a57nk6ghPW4tXjguy6LcDGqwskdGQPHjONJ1PvW69btN8o9DHZtFwYfwawKd',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'gco_admin',
                'email' => 'letien524@gmail.com',
                'password' => '$2y$10$XVOxmS7KkRFU9yhSm/uLBupMloLFNUrcvl08CZ4fVY3Csh3htBvwm',
                'image' => NULL,
                'level' => 1,
                'remember_token' => '81rKG1GcMhLKyGlCd7fQeDGWB1trBP0cPuKxCOYyhApYHsphaiQYsL71HdTs',
                'created_at' => NULL,
                'updated_at' => '2019-08-21 10:27:49',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'editor_nam',
                'email' => '',
                'password' => '$2y$10$eHPBDlxsSWA6Kgql5OOps.h9w6mKwaVEUCHHnSXtPbIPUHMPuRJoC',
                'image' => NULL,
                'level' => 3,
                'remember_token' => 'C3elfk2fJLHLSbVvGnJ9jQYcpsL4Ks2vq3sU0wSqcgvXZxmvcK0pjBLZOKX4',
                'created_at' => '2019-08-21 10:21:18',
                'updated_at' => '2019-08-21 10:21:18',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'ngocanh',
                'email' => '',
                'password' => '$2y$10$hLqi2CL8g3HzOlCYz3BOt.Qv0PoYlYsqZAbDqXTKGbjlv28whZsCy',
                'image' => NULL,
                'level' => 1,
                'remember_token' => NULL,
                'created_at' => '2019-08-27 08:51:40',
                'updated_at' => '2019-08-27 08:51:40',
            ),
        ));
        
        
    }
}