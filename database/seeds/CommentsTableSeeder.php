<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('comments')->delete();
        
        \DB::table('comments')->insert(array (
            0 => 
            array (
                'id' => 3,
                'name' => 'Bach Nguyen Cuong',
                'email' => 'nguyencuongbach@gmail.com',
                'phone' => '393088986',
                'website' => NULL,
                'content' => 'A clear information',
                'type' => 'blog-comment',
                'status' => 2,
                'post_id' => 23,
                'created_at' => '2019-06-12 16:19:00',
                'updated_at' => '2019-06-14 14:58:21',
            ),
        ));
        
        
    }
}