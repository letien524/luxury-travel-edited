<?php

use Illuminate\Database\Seeder;

class ConfigsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('configs')->delete();
        
        \DB::table('configs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'general',
            'content' => '{"site_logo":null,"site_favicon":"\\/uploads\\/images\\/tours\\/logo-icon\\/Favicon.png","site_payment":null,"site_name":"Luxury Travel: Best 1000 Tailor-Made Luxury Private Tours In Asia","site_hotline":"(+84) 834 686 996","site_email":"sales@luxurytravelvietnam.com","site_meta_title":"Luxury Travel: Best 1000 Tailor-Made Luxury Private Tours In Asia","site_meta_description":"Luxury Travel Vietnam provides the best tailor-made & private holidays and tour packages to Vietnam, Laos, Cambodia, Myanmar, and Thailand.","site_script_header":"<meta name=\\"google-site-verification\\" content=\\"LTjz2rLsP6Ceh32z-FMGRT6-KsYcilUJOo7Y37MlXuo\\" \\/><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':\\r\\nnew Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],\\r\\nj=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\\r\\n\'https:\\/\\/www.googletagmanager.com\\/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);\\r\\n})(window,document,\'script\',\'dataLayer\',\'GTM-MV8T4SM\');<\\/script><script type=\\"application\\/ld+json\\">{\\r\\n\\"@context\\": \\"http:\\/\\/schema.org\\",\\r\\n\\"@type\\": \\"TravelAgency\\",\\r\\n\\"name\\": \\"Luxury Travel Vietnam \\",\\r\\n\\"address\\": {\\r\\n\\"@type\\": \\"PostalAddress\\",\\r\\n\\"streetAddress\\": \\"456 Lac Long Quan Street\\",\\r\\n\\"addressLocality\\": \\"Hanoi City \\",\\r\\n\\"addressRegion\\": \\"Hanoi\\",\\r\\n\\"postalCode\\": \\"100000\\"\\r\\n},\\r\\n\\"image\\": \\"https:\\/\\/luxurytravelvietnam.com\\/storage\\/app\\/public\\/images\\/banner-vn-contrast.jpg\\",\\r\\n\\"logo\\": \\"https:\\/\\/luxurytravelvietnam.com\\/public\\/frontend\\/images\\/logo-1.png\\",\\r\\n\\"email\\": \\"sales@luxurytravelvietnam.com\\",\\r\\n\\"telePhone\\": \\"+84 834 68 69 96\\",\\r\\n\\"url\\": \\"https:\\/\\/luxurytravelvietnam.com\\/\\",\\r\\n\\"openingHours\\":[\\"Mo-Sat 08:30-17:30\\", \\"Su 10:00-17:00\\"],\\r\\n\\"geo\\": {\\r\\n\\"@type\\": \\"GeoCoordinates\\",\\r\\n\\"latitude\\": \\"21.0722532\\",\\r\\n\\"longitude\\": \\"105.8103871\\"\\r\\n},\\r\\n\\"priceRange\\":\\"$\\", \\"sameAs\\": [ \\"https:\\/\\/www.facebook.com\\/luxurytravelcompany\\/\\", \\"https:\\/\\/www.linkedin.com\\/company\\/luxury-travel-vietnam\\/\\",\\t\\"https:\\/\\/twitter.com\\/LuxuryTravelLtd\\",\\t\\"https:\\/\\/www.instagram.com\\/luxurytravelvietnam\\/\\" ],\\r\\n\\"Founder\\": \\"Pham Manh Ha\\",\\r\\n\\"Award\\": [ \\"Excellent Performance Award\\",\\r\\n\\"Best Service Tour Operator Award\\",\\r\\n\\"The Leading Specialist Tour Operator Providing Great Services to Sophisticated Travelers\\",\\r\\n\\"Winner in Nomination for Vietnam\'s Leading Tour Operator of World Travel Awards 2014\\",\\r\\n\\"Winner in Nomination for Vietnam\'s Leading Tour Operator of World Travel Awards 2015\\",\\r\\n\\"Winner in Nomination for Asia\'s Leading Tour Operator of World Travel Awards 2015\\"\\r\\n],\\r\\n\\"currenciesAccepted\\": [ \\"USD\\", \\"VDN\\", \\"EUR\\"],\\r\\n\\"paymentAccepted\\": [ \\"Cash\\", \\"Credit Card\\", \\"VISA\\", \\"MASTERCARD\\", \\"AMERICAN EXPRESS\\", \\"JCB\\"],\\r\\n\\"faxNumber\\": \\"84 4 39274118\\",\\r\\n\\"foundingDate\\": \\"2004\\",\\r\\n\\"foundingLocation\\": \\"5 Nguy\\u1ec5n Tr\\u01b0\\u1eddng T\\u1ed9 - Nguy\\u1ec5n Trung Tr\\u1ef1c - Ba \\u0110\\u00ecnh, H\\u00e0 N\\u1ed9i\\",\\r\\n\\"numberOfEmployees\\":\\r\\n{\\"@type\\": \\"QuantitativeValue\\",\\r\\n\\"unitText\\": \\"70\\"},\\r\\n\\"OwnershipFundingInfo\\": \\"Mr. Pham Ha, passionate traveler and entrepreneur. With more than 15 years of experience in the travel industry, he is a keynote speaker and luxury travel expert in Vietnam and around the globe. When Pham Ha saw the need for a tour company offering services that would specialize in high-end travelers, he didn\\u2019t hesitate for long. That\\u2019s how the idea of creating a travel company with an emphasis on ultra-luxurious experiences, privately guides and bespoke holidays came to life. As a lover of Arts, Writing, History and Culture, Pham Ha created personalized tours with an immersive approach to traveling.\\",\\r\\n\\"taxID\\": \\"101622092\\",\\r\\n\\"makesOffer\\": \\"Vietnam, Cambodia, Laos, Thailand, Myanmar tailor-made tours\\",\\r\\n\\"areaServed\\": { \\"@type\\": \\"Country\\", \\"name\\": [\\"Unieted State\\", \\"Canada\\", \\"Australia\\", \\"United Kindom\\", \\"Spain\\",\\"Italy\\",\\"France\\", \\"German\\",\\"Singapore\\",\\"India\\", \\"Malaysia\\", \\"Mexico\\",\\r\\n\\"Argentina\\"\\r\\n]\\r\\n}\\r\\n} }}} <\\/script><script type=\\"application\\/ld+json\\">{\\r\\n\\"@context\\": \\"http:\\/\\/schema.org\\",\\r\\n\\"@type\\": \\"Person\\",\\r\\n\\"name\\": \\"Pham Manh Ha\\",\\r\\n\\"alternateName\\": \\"Pham Ha\\",\\r\\n\\"address\\": {\\r\\n\\"@type\\": \\"PostalAddress\\",\\r\\n\\"addressCountry\\": \\"Vietnam \\",\\r\\n\\"addressRegion\\": \\"Hanoi \\"\\r\\n},\\r\\n\\"image\\": \\"https:\\/\\/www.linkedin.com\\/in\\/ha-pham-20a10018\\/detail\\/photo\\/\\",\\r\\n\\"jobTitle\\": \\"CEO\\",\\r\\n\\"worksFor\\": \\"Luxury Travel Vietnam Ltd\\",\\r\\n\\"alumniOf\\": [\\"Hanoi National Economic University\\",\\r\\n\\"National University of Vietnam\\",\\r\\n\\"THPT \\u0110\\u1ed3ng Gia\\"\\r\\n],\\r\\n\\"nationality\\": \\"Vietnamese\\",\\r\\n\\"telephone\\": \\"\\",\\r\\n\\"sameAs\\" : [ \\"https:\\/\\/www.linkedin.com\\/in\\/ha-pham-20a10018\\/\\",\\r\\n\\"https:\\/\\/www.facebook.com\\/phamhaluxurytravel\\"\\r\\n]\\r\\n}<\\/script><script>window[\'_fs_debug\'] = false;\\r\\nwindow[\'_fs_host\'] = \'fullstory.com\';\\r\\nwindow[\'_fs_org\'] = \'PDK9M\';\\r\\nwindow[\'_fs_namespace\'] = \'FS\';\\r\\n(function(m,n,e,t,l,o,g,y){ if (e in m) {if(m.console && m.console.log) { m.console.log(\'FullStory namespace conflict. Please set window[\\"_fs_namespace\\"].\');} return;} g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[]; o=n.createElement(t);o.async=1;o.crossOrigin=\'anonymous\';o.src=\'https:\\/\\/\'+_fs_host+\'\\/s\\/fs.js\'; y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y); g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g(\'event\',{n:i,p:v},s)}; g.shutdown=function(){g(\\"rec\\",!1)};g.restart=function(){g(\\"rec\\",!0)}; g.log = function(a,b) { g(\\"log\\", [a,b]) }; g.consent=function(a){g(\\"consent\\",!arguments.length||a)}; g.identifyAccount=function(i,v){o=\'account\';v=v||{};v.acctId=i;g(o,v)}; g.clearUserCookie=function(){};\\r\\n})(window,document,window[\'_fs_namespace\'],\'script\',\'user\');<\\/script>","site_script_footer":"<noscript><iframe src=\\"https:\\/\\/www.googletagmanager.com\\/ns.html?id=GTM-MV8T4SM\\"\\r\\nheight=\\"0\\" width=\\"0\\" style=\\"display:none;visibility:hidden\\"><\\/iframe><\\/noscript>","site_enquiry_image":null,"site_enquiry_banner":null,"site_enquiry_text":"We know you are unique, so we\'re here to create the unique experinces for you"}',
                'created_at' => NULL,
                'updated_at' => '2019-10-30 11:13:56',
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'social',
                'content' => '{"social":{"facebook":"https:\\/\\/www.facebook.com\\/luxurytravelcompany\\/","twitter":"https:\\/\\/twitter.com\\/LuxuryTravelLtd","tripadvisor":"https:\\/\\/www.tripadvisor.com\\/Attraction_Review-g293924-d5984191-Reviews-Luxury_Travel-Hanoi.html","google_plus":null,"whatsapp":"84-834-68-69-96","skype":"sales8luxurytravel","youtube":"https:\\/\\/drive.google.com\\/drive\\/folders\\/19Maf-2t_miCJeBGqY08Wq4uHdPkUU4L3"}}',
                'created_at' => NULL,
                'updated_at' => '2019-10-28 14:48:26',
            ),
            2 => 
            array (
                'id' => 3,
                'type' => 'footer_menu',
                'content' => '{"luxury_travel":"<ul class=\\"vk-footer__list vk-footer__list--style-3\\">\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/about-us\\">Who We Are<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/about-us\\">Meet Our Team<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/about-us\\">Why Travel With Us<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/core-values.html\\">Core Values<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/logo-meaning.html\\">Logo Meaning<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/our-awards.html\\">Our Awards<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/wow-services.html\\">&ldquo;WOW&rdquo; Services<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/forms.gle\\/nUcFtKJtm7SPHonWA\\">Tour Evaluation<\\/a><\\/li>\\r\\n<\\/ul>","information":"<ul class=\\"vk-footer__list vk-footer__list--style-3\\">\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/contact\\">Contact Us<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/faqs.html\\">FAQs<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/payment-policies.html\\">Payment Policies<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/privacy-policies.html\\">Privacy Policies<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravels.asia\\/\\">For Travel Agents<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/drive.google.com\\/open?id=1fOCvNVFZ_Uh2JC4jFCAsWtI-vJ6P3VGQ\\">Brochure<\\/a><\\/li>\\r\\n\\t<li><a href=\\"https:\\/\\/luxurytravelvietnam.com\\/blog\\/\\">Blog<\\/a><\\/li>\\r\\n<\\/ul>"}',
                'created_at' => NULL,
                'updated_at' => '2019-09-17 17:13:04',
            ),
            3 => 
            array (
                'id' => 4,
                'type' => 'tour-filter',
                'content' => '{"filter_price":{"1557469952685":{"name":"< $500","min":null,"max":"500"},"1557472421477":{"name":"$500 - $1500","min":"500","max":"1500"},"1557472422483":{"name":"$1500 - $2000","min":"1500","max":"2000"},"1557472422922":{"name":"$2000 - $3000","min":"2000","max":"3000"},"1557472720670":{"name":"More $3000","min":"3000","max":null}},"filter_duration":{"1557469963484":{"name":"< 5 days","min":null,"max":"5"},"1557469964251":{"name":"5 - 7 days","min":"5","max":"7"},"1557469964731":{"name":"7 - 10 days","min":"7","max":"10"},"1557469965900":{"name":"10 - 15 days","min":"10","max":"15"},"1557470331343":{"name":"> 15 days","min":"15","max":null}}}',
                'created_at' => '2019-05-10 05:34:53',
                'updated_at' => '2019-05-14 04:53:46',
            ),
            4 => 
            array (
                'id' => 5,
                'type' => 'menu',
                'content' => '[{"destination":"","link":"https://luxurytravelvietnam.com","name":"HOME ","id":1566550300435},{"destination":"is_destination","link":"destinations","name":"DESTINATIONS","id":1559105277954},{"destination":"is_activity","link":"activities","name":"ACTIVITIES","id":1559105307266,"children":[{"destination":"is_activity","link":"https://luxurytravelvietnam.com/backend/config/menu","name":"Sucba Diving ","id":1571671167575}]},{"destination":"","link":"https://luxurytravelvietnam.com/blog/","name":"BLOG","id":1559105316050},{"destination":"","link":"about-us","name":"ABOUT US","id":1559105347473},{"destination":"","link":"https://luxurytravels.asia/","name":"TRAVEL AGENTS","id":1559105364281}]',
                'created_at' => '2019-05-29 04:39:27',
                'updated_at' => '2019-10-21 22:19:32',
            ),
            5 => 
            array (
                'id' => 6,
                'type' => 'email',
                'content' => '{"smtp":{"host":null,"port":null,"encryption":null,"username":"mailcentermanagers@gmail.com","password":"lciqezknpupqthmr","name":null},"tour":{"mail_to":"vyngocanh.vna@gmail.com","bcc":{"1563868553":"marketing3@luxurytravelvietnam.com"}},"contact":{"mail_to":"vyngocanh.vna@gmail.com","bcc":{"1563868577":"marketing3@luxurytravelvietnam.com","1566383367":"letien524@gmail.com"}},"subscribe":{"mail_to":"vyngocanh.vna@gmail.com"},"comment":{"mail_to":"vyngocanh.vna@gmail.com","bcc":{"1563868595":"marketing3@luxurytravelvietnam.com"}}}',
                'created_at' => '2019-07-12 02:16:08',
                'updated_at' => '2019-10-08 09:14:25',
            ),
            6 => 
            array (
                'id' => 7,
                'type' => 'popup',
                'content' => '{"direction":{"image":"\\/uploads\\/files\\/popup\\/popup-winter2019.jpg","link":"http:\\/\\/bit.ly\\/2OwLjFx","delay":"20","display":"0","black_list":{"1570506918":"destinations\\/vietnam","1570506933":"destinations\\/vietnam-cambodia-tours","1571221522":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/thailand-cambodia-vietnam-tour","1571221546":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/vietnam\\/vietnam-cambodia-laos-tour"}},"collection":{"image":"\\/uploads\\/images\\/featured%20image\\/Myanmar\\/Myanmar-beach-03.jpg","name":"Special Winter Offer 2019","content":"Travel with us Now to receive many special offers","button":"BOOK NOW","delay":"3","display":"0","black_list":{"1567735137":"activities\\/culinary-tours"}},"collection_bottom":{"content":"Design your trip now to get a special offer from us!","delay":null,"display":"0"},"collection_corner":{"image":null,"link":null,"delay":null,"display":"0"}}',
                'created_at' => '2019-08-20 10:50:15',
                'updated_at' => '2019-10-16 17:27:44',
            ),
        ));
        
        
    }
}