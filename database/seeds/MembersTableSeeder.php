<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('members')->delete();
        
        \DB::table('members')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Pham Ha',
                'slug' => 'pham-ha',
                'image' => '/uploads/images/tours/Vietnam/about-us-manh-ha.jpg',
                'email' => NULL,
                'position' => 'Founder / CEO',
                'excerpt' => 'After 12 years in the industry, Pham Ha found his own business in 2004 providing luxury tours in Vietnam. Pham Ha who is determined to create the most unforgettable travel experiences for high-end tourists.',
                'quote' => 'Đây là trích dẫn của a Phạm Hà',
                'content' => '<p>After 12 years in the industry, Pham Ha found his own business in 2004 providing luxury tours in Vietnam. Pham Ha who is determined to create the most unforgettable travel experiences for high-end tourists.</p>',
                'order_menu' => 8,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:00:41',
                'updated_at' => '2019-10-28 14:44:04',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Thang Phan',
                'slug' => 'thang-phan',
                'image' => '/uploads/images/tours/Vietnam/about-us-thang-phan.jpg',
                'email' => NULL,
                'position' => 'Director of Sales and Marketing',
                'excerpt' => 'I\'m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.',
                'quote' => NULL,
                'content' => 'I\'m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.',
                'order_menu' => 7,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:01:06',
                'updated_at' => '2019-10-25 01:21:59',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Duong Vuong',
                'slug' => 'duong-vuong',
                'image' => '/uploads/images/tours/Vietnam/about-us-duong-anh.jpg',
                'email' => NULL,
                'position' => 'English Sales Executive',
                'excerpt' => 'I’m proud to represent Luxury Travel and to help you find your perfect vacation. Before you book elsewhere, check with me please! You will receive added benefit of booking with Luxury Travel as well',
                'quote' => NULL,
                'content' => 'I’m proud to represent Luxury Travel and to help you find your perfect vacation. Before you book elsewhere, check with me please! You will receive added benefit of booking with Luxury Travel as well',
                'order_menu' => NULL,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:01:06',
                'updated_at' => '2019-10-25 01:21:59',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Quynh Nguyen',
                'slug' => 'quynh-nguyen',
                'image' => '/uploads/images/team-4.jpg',
                'email' => NULL,
                'position' => 'English Sales Executive',
                'excerpt' => 'My name is Quynh Nguyen. I like a poem that H. C. Andersen wrote: ``To move, to breath, to fly, to float, To gain all while you give, to roam the roads of lands remote. To travel is to live.”',
                'quote' => NULL,
                'content' => 'My name is Quynh Nguyen. I like a poem that H. C. Andersen wrote: ``To move, to breath, to fly, to float, To gain all while you give, to roam the roads of lands remote. To travel is to live.”',
                'order_menu' => NULL,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:01:06',
                'updated_at' => '2019-10-25 01:21:59',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Vu Long',
                'slug' => 'vu-long',
                'image' => '/uploads/images/tours/Vietnam/about-us-long-vu.jpg',
                'email' => NULL,
                'position' => 'English Sales Executive',
                'excerpt' => 'I have spent over ten years to travel and explore amazing countries in Asia from the most well-known places to the unknown places to have deep understanding and search for new unique experiences that all I want to share with our guests now who one to discover the authentic of Vietnam.',
                'quote' => NULL,
                'content' => 'I have spent over ten years to travel and explore amazing countries in Asia from the most well-known places to the unknown places to have deep understanding and search for new unique experiences that all I want to share with our guests now who one to discover the authentic of Vietnam.',
                'order_menu' => NULL,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:01:06',
                'updated_at' => '2019-10-25 01:21:59',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Hao Chu',
                'slug' => 'hao-chu',
                'image' => '/uploads/images/tours/Vietnam/about-us-hao-chu.jpg',
                'email' => NULL,
                'position' => 'English Sales Executive',
                'excerpt' => 'Hello, My name is Hao. I love working with customers. “Sales” has really influenced everything I do. It has instilled in me the important traits of operating with a sense of urgency and listening to people.',
                'quote' => NULL,
                'content' => 'Hello, My name is Hao. I love working with customers. “Sales” has really influenced everything I do. It has instilled in me the important traits of operating with a sense of urgency and listening to people.',
                'order_menu' => NULL,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:01:06',
                'updated_at' => '2019-10-25 01:21:59',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Quynh Anh',
                'slug' => 'quynh-anh',
                'image' => '/uploads/images/tours/Vietnam/about-us-quynh-anh.jpg',
                'email' => NULL,
                'position' => 'English Sales Executive',
                'excerpt' => 'And I am very proud of my job because every day I have more chances to bring our country’s image & people as well as share my travel experiences with clients all over the world, as a trustful local friend to my beloved tourists.',
                'quote' => NULL,
                'content' => 'And I am very proud of my job because every day I have more chances to bring our country’s image & people as well as share my travel experiences with clients all over the world, as a trustful local friend to my beloved tourists.',
                'order_menu' => NULL,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:01:06',
                'updated_at' => '2019-10-25 01:21:59',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Le Mai',
                'slug' => 'le-mai',
                'image' => '/uploads/images/team-8.jpg',
                'email' => NULL,
                'position' => 'English Sales Executive',
                'excerpt' => 'My name is Mai and I work as Sales executive. If you would like to travel to somewhere such as Vietnam, Cambodia, Laos, Thailand and Myanmar, please do not hesitate to contact me. I am very happy to introduce above beautiful countries to you',
                'quote' => NULL,
                'content' => 'My name is Mai and I work as Sales executive. If you would like to travel to somewhere such as Vietnam, Cambodia, Laos, Thailand and Myanmar, please do not hesitate to contact me. I am very happy to introduce above beautiful countries to you',
                'order_menu' => NULL,
                'status' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:01:06',
                'updated_at' => '2019-10-25 01:21:59',
            ),
        ));
        
        
    }
}