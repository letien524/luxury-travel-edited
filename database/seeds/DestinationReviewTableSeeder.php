<?php

use Illuminate\Database\Seeder;

class DestinationReviewTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('destination_review')->delete();
        
        \DB::table('destination_review')->insert(array (
            0 => 
            array (
                'review_id' => 1,
                'category_id' => 60,
            ),
        ));
        
        
    }
}