<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('reviews')->delete();
        
        \DB::table('reviews')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'A great trip with Luxury Travel',
                'reviewer' => 'TrinityClar @Tripadvisor',
                'image' => NULL,
                'country' => NULL,
                'content' => 'Hello Long,
I am glad that you got the gifts, enjoy it.
We are back in Canada. I just want to thank you again for working with me to plan a great trip. Everything was well done.
We were very pleased with all our guides.I hope that they enjoyed being with us too. The guide, Thu was fantastic! The tour was great!',
                'rate' => 5,
                'display' => 1,
                'order_menu' => NULL,
                'submited_at' => '2019-09-03',
                'created_at' => '2019-10-21 11:26:01',
                'updated_at' => '2019-10-21 11:26:25',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Trustworthy agent',
                'reviewer' => 'Jack Y',
                'image' => NULL,
                'country' => NULL,
                'content' => 'Just completed 16 days tour of Thailand Cambodia Vietnam with lux travel.
Booking process... Efficient, smooth. Tour.... Pleasant with nothing remiss Services... Friendly, proficient.
Food... Not quite delectable but reasonably appetising.
Accommodation.. Tops, but we paid for it, so that\'s fair. Overall, an 8+ out of ten.
Will be booking Myanmar tour with lux travels nov.
Dr yik',
                'rate' => 5,
                'display' => 1,
                'order_menu' => NULL,
                'submited_at' => '2019-08-15',
                'created_at' => '2019-10-21 11:27:57',
                'updated_at' => '2019-10-21 11:30:07',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Wonderful travel agency, very customer oriented and offering activities that are different',
                'reviewer' => 'Fabienne C @Tripadvisor',
                'image' => NULL,
                'country' => NULL,
            'content' => 'Luxury Travel organized for us a 23-day trip from North to South Vietnam in July/August 2019 for the 3 of us (2 adults + 1 12-year old). A fantastic experience, 100% recommended! We are demanding travellers and like the agency listen to us, interesting in our hobbies, what we like, etc. Thom, who organized our travel (all in Spanish if you need it!) has been very responsive, asking good questions, taking our comments into account and customizing the tour in a wonderful way. Vietnam is a very touristic country, even though, she has been able to propose alternatives tours or places where you do not cross a lot of tourists. We like nature and adventure and she found great activities as kayaking, trekking, ziplining and even discovering a huge cave as speleogists, just the 3 of us with a guide! Everything very well organized, you just have to enjoy without thinking about logistics or delays or quality. It was perfect and we had a wonderful time in Vietnam, a lovely country. Thank you so much and see you soon for another country!!!',
                'rate' => 5,
                'display' => 1,
                'order_menu' => NULL,
                'submited_at' => '2019-10-21',
                'created_at' => '2019-10-21 11:28:47',
                'updated_at' => '2019-10-21 11:28:47',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'THE BEST HOLIDAY WE HAVE EVER BEEN ON. I',
                'reviewer' => 'Tour262672 @Tripadvisor',
                'image' => NULL,
                'country' => NULL,
                'content' => 'Thank Chu Thi Hao for this experience and luxury travel for giving us such a holiday we will never forget for the rest of our lives. I hope we keep in touch and if you guys do other countries in asia that we want to visit. You guys will definitely be one to contact to arrange.',
                'rate' => 5,
                'display' => 1,
                'order_menu' => NULL,
                'submited_at' => '2019-07-29',
                'created_at' => '2019-10-21 11:29:54',
                'updated_at' => '2019-10-21 11:29:54',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Best Holiday Ever to Thailand Cambodia and Vietnam',
                'reviewer' => 'Andrew Parrott',
                'image' => NULL,
                'country' => NULL,
                'content' => 'I\'d like to start with you Chu Thi who had been always there for help and getting everything done before our trip. You were so helpful in spotting our issue with our passport and many thanks for that. Your organising of our whole trip and tailoring to our needs was so easy which is all down to you. When we were planning you always gave responses quickly even we had so many questions. You are a credit to your company and we thank you for making our trip so good.

The trip was a multi trip to many different places and countries but we never felt tired from travelling and worrying about hotels or travel to and from airports. Everything was taken care so we could fully enjoy the beautiful counries of Cambodia and Vietnam.

The hotels were of excellent quality and some were breathtaking siam siam, silverline jubilee and the pelican cruise were amazing the staff and the decor the food was very good.

The tour guides we had were also brilliant each of them were so tentative and really made us feel welcome and safe whilst enjoying the brilliant places we visited. They took care of everything we did through arranging local restaurants away from tourist areas which were amazing, to getting tickets and giving us a personal tour of each magical venue. So many thanks to Dalith, Sa, Mr Ming, Katie and Khun for making us feel so welcome and taking very good care of us. They are exceptional people who are warm, knowledgable and very funny. This holiday would not have been as good without these guys.',
                'rate' => 5,
                'display' => 1,
                'order_menu' => NULL,
                'submited_at' => '2019-09-19',
                'created_at' => '2019-10-21 11:31:13',
                'updated_at' => '2019-10-21 11:31:13',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'An amazing experience',
                'reviewer' => 'JennyEve @Tripadvisor',
                'image' => NULL,
                'country' => NULL,
                'content' => 'Beautiful hotels, roof top pools , outdoor pools . A five star experience travelling through Vietnam, Thailand and Cambodia . Escorted by an experienced tour rep in an air con mini bus or car .
Visited many different cultural areas ,boat trips on a large boat , small boat and a river . Wonderful pagodas , temples .
Lantern city .
Buffet breakfast every day and five course lunch many days .
Too much information to share . A brilliant experience from first email for a tour to meeting the lovely Lara who arranged the whole trip . No question too silly all answered immediately.
Just fantastic trip .',
                'rate' => 5,
                'display' => 1,
                'order_menu' => NULL,
                'submited_at' => '2019-07-07',
                'created_at' => '2019-10-21 11:32:18',
                'updated_at' => '2019-10-21 11:32:18',
            ),
        ));
        
        
    }
}