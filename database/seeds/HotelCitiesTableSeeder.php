<?php

use Illuminate\Database\Seeder;

class HotelCitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hotel_cities')->delete();
        
        \DB::table('hotel_cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Hanoi',
                'created_at' => '2019-10-25 08:35:45',
                'updated_at' => '2019-10-25 08:35:45',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Hue',
                'created_at' => '2019-10-25 08:35:49',
                'updated_at' => '2019-10-25 08:35:49',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Hoi An',
                'created_at' => '2019-10-25 08:35:53',
                'updated_at' => '2019-10-25 08:35:53',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Ho Chi Minh',
                'created_at' => '2019-10-25 08:35:58',
                'updated_at' => '2019-10-25 08:35:58',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Siem Reap',
                'created_at' => '2019-10-25 08:36:04',
                'updated_at' => '2019-10-25 08:36:04',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'test thành phố',
                'created_at' => '2019-10-28 14:32:51',
                'updated_at' => '2019-10-28 14:32:51',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Ngọc Anh thành phố',
                'created_at' => '2019-10-28 14:34:50',
                'updated_at' => '2019-10-28 14:34:50',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Vy Vy',
                'created_at' => '2019-10-28 14:37:14',
                'updated_at' => '2019-10-28 14:37:14',
            ),
        ));
        
        
    }
}