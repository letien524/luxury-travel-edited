<?php

use Illuminate\Database\Seeder;

class HotelRoomTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hotel_room_types')->delete();
        
        \DB::table('hotel_room_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Superior',
                'created_at' => '2019-10-25 08:34:16',
                'updated_at' => '2019-10-25 08:34:16',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Deluxe',
                'created_at' => '2019-10-25 08:34:19',
                'updated_at' => '2019-10-25 08:34:19',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Deluxe riverview',
                'created_at' => '2019-10-25 08:34:24',
                'updated_at' => '2019-10-25 08:34:24',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Grand Luxury',
                'created_at' => '2019-10-25 08:34:34',
                'updated_at' => '2019-10-25 08:34:34',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'One bedroom villa',
                'created_at' => '2019-10-25 08:34:41',
                'updated_at' => '2019-10-25 08:34:41',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Deluxe RiverviewPark room',
                'created_at' => '2019-10-25 08:34:51',
                'updated_at' => '2019-10-25 08:34:51',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Suite',
                'created_at' => '2019-10-25 08:34:54',
                'updated_at' => '2019-10-25 08:34:54',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Deluxe Garden view',
                'created_at' => '2019-10-25 08:39:11',
                'updated_at' => '2019-10-25 08:39:11',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Signature Superior',
                'created_at' => '2019-10-25 08:40:44',
                'updated_at' => '2019-10-25 08:40:44',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Deluxe Oceanview',
                'created_at' => '2019-10-25 08:59:22',
                'updated_at' => '2019-10-25 08:59:22',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Park room',
                'created_at' => '2019-10-25 09:01:51',
                'updated_at' => '2019-10-25 09:01:51',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'test kiểu phòng',
                'created_at' => '2019-10-28 14:32:43',
                'updated_at' => '2019-10-28 14:32:43',
            ),
        ));
        
        
    }
}