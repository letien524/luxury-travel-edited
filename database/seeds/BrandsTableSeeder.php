<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brands')->delete();
        
        \DB::table('brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => NULL,
                'image' => '/uploads/files/10.png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:55:33',
                'updated_at' => '2019-06-12 17:16:35',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => NULL,
            'image' => '/uploads/files/1(1).png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:55:40',
                'updated_at' => '2019-06-13 11:19:10',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => NULL,
                'image' => '/uploads/files/vecom.png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:55:45',
                'updated_at' => '2019-06-14 10:40:00',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => NULL,
                'image' => '/uploads/files/7.png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:55:52',
                'updated_at' => '2019-06-12 17:18:49',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => NULL,
                'image' => '/uploads/files/amathailand.png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:55:57',
                'updated_at' => '2019-06-14 10:39:26',
            ),
            5 => 
            array (
                'id' => 7,
                'name' => NULL,
                'image' => '/uploads/files/logopata.png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:56:09',
                'updated_at' => '2019-06-21 14:00:10',
            ),
            6 => 
            array (
                'id' => 8,
                'name' => NULL,
                'image' => '/uploads/files/5.png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:56:18',
                'updated_at' => '2019-06-12 17:19:16',
            ),
            7 => 
            array (
                'id' => 9,
                'name' => NULL,
            'image' => '/uploads/files/2(1).png',
                'type' => 'association',
                'created_at' => '2019-05-11 02:56:29',
                'updated_at' => '2019-06-13 11:19:30',
            ),
            8 => 
            array (
                'id' => 11,
                'name' => NULL,
                'image' => '/uploads/images/award-1.png',
                'type' => 'awards',
                'created_at' => '2019-05-11 02:56:50',
                'updated_at' => '2019-05-11 02:56:50',
            ),
            9 => 
            array (
                'id' => 12,
                'name' => NULL,
                'image' => '/uploads/images/award-2.png',
                'type' => 'awards',
                'created_at' => '2019-05-11 02:56:57',
                'updated_at' => '2019-05-11 02:56:57',
            ),
            10 => 
            array (
                'id' => 13,
                'name' => NULL,
                'image' => '/uploads/files/2017-tripadvisor-w.png',
                'type' => 'awards',
                'created_at' => '2019-05-11 02:57:05',
                'updated_at' => '2019-06-21 14:01:08',
            ),
            11 => 
            array (
                'id' => 14,
                'name' => NULL,
                'image' => '/uploads/images/brand-1.png',
                'type' => 'news',
                'created_at' => '2019-05-11 02:57:24',
                'updated_at' => '2019-05-11 02:57:24',
            ),
            12 => 
            array (
                'id' => 15,
                'name' => NULL,
                'image' => '/uploads/images/brand-2.png',
                'type' => 'news',
                'created_at' => '2019-05-11 02:57:32',
                'updated_at' => '2019-05-11 02:57:32',
            ),
            13 => 
            array (
                'id' => 16,
                'name' => NULL,
                'image' => '/uploads/images/brand-3.png',
                'type' => 'news',
                'created_at' => '2019-05-11 02:57:39',
                'updated_at' => '2019-05-11 02:57:39',
            ),
            14 => 
            array (
                'id' => 17,
                'name' => NULL,
                'image' => '/uploads/images/brand-4.png',
                'type' => 'news',
                'created_at' => '2019-05-11 02:57:46',
                'updated_at' => '2019-05-11 02:57:46',
            ),
            15 => 
            array (
                'id' => 18,
                'name' => NULL,
                'image' => '/uploads/images/brand-5.png',
                'type' => 'news',
                'created_at' => '2019-05-11 02:57:53',
                'updated_at' => '2019-05-11 02:57:53',
            ),
            16 => 
            array (
                'id' => 19,
                'name' => NULL,
                'image' => '/uploads/images/brand-6.png',
                'type' => 'news',
                'created_at' => '2019-05-11 02:58:00',
                'updated_at' => '2019-05-11 02:58:05',
            ),
            17 => 
            array (
                'id' => 20,
                'name' => NULL,
                'image' => '/uploads/files/2.png',
                'type' => 'association',
                'created_at' => '2019-06-12 17:21:19',
                'updated_at' => '2019-06-12 17:21:19',
            ),
            18 => 
            array (
                'id' => 21,
                'name' => NULL,
                'image' => '/uploads/images/tours/Vietnam/frommer111.png',
                'type' => 'news',
                'created_at' => '2019-06-13 11:19:45',
                'updated_at' => '2019-06-27 09:41:57',
            ),
            19 => 
            array (
                'id' => 22,
                'name' => NULL,
            'image' => '/uploads/files/4(1).png',
                'type' => 'association',
                'created_at' => '2019-06-13 11:20:03',
                'updated_at' => '2019-06-13 11:20:03',
            ),
            20 => 
            array (
                'id' => 23,
                'name' => NULL,
            'image' => '/uploads/files/5(1).png',
                'type' => 'news',
                'created_at' => '2019-06-13 11:20:27',
                'updated_at' => '2019-06-13 11:20:27',
            ),
            21 => 
            array (
                'id' => 24,
                'name' => NULL,
            'image' => '/uploads/files/6(1).png',
                'type' => 'news',
                'created_at' => '2019-06-13 11:21:00',
                'updated_at' => '2019-06-13 11:21:00',
            ),
            22 => 
            array (
                'id' => 25,
                'name' => NULL,
            'image' => '/uploads/files/7(1).png',
                'type' => 'association',
                'created_at' => '2019-06-13 11:21:13',
                'updated_at' => '2019-06-13 11:21:13',
            ),
        ));
        
        
    }
}