<?php

use Illuminate\Database\Seeder;

class HotelCityTourTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hotel_city_tour')->delete();
        
        \DB::table('hotel_city_tour')->insert(array (
            0 => 
            array (
                'hotel_city_id' => 1,
                'tour_id' => 22,
            ),
            1 => 
            array (
                'hotel_city_id' => 2,
                'tour_id' => 22,
            ),
            2 => 
            array (
                'hotel_city_id' => 3,
                'tour_id' => 22,
            ),
            3 => 
            array (
                'hotel_city_id' => 4,
                'tour_id' => 22,
            ),
            4 => 
            array (
                'hotel_city_id' => 5,
                'tour_id' => 22,
            ),
            5 => 
            array (
                'hotel_city_id' => 7,
                'tour_id' => 413,
            ),
            6 => 
            array (
                'hotel_city_id' => 8,
                'tour_id' => 413,
            ),
        ));
        
        
    }
}