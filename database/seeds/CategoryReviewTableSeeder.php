<?php

use Illuminate\Database\Seeder;

class CategoryReviewTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('category_review')->delete();
        
        \DB::table('category_review')->insert(array (
            0 => 
            array (
                'category_id' => 1,
                'review_id' => 1,
            ),
            1 => 
            array (
                'category_id' => 1,
                'review_id' => 2,
            ),
            2 => 
            array (
                'category_id' => 1,
                'review_id' => 3,
            ),
            3 => 
            array (
                'category_id' => 1,
                'review_id' => 4,
            ),
            4 => 
            array (
                'category_id' => 1,
                'review_id' => 5,
            ),
            5 => 
            array (
                'category_id' => 1,
                'review_id' => 6,
            ),
        ));
        
        
    }
}