<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('posts')->delete();
        
        \DB::table('posts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Home',
                'slug' => NULL,
                'image' => '/uploads/images/tours/logo-icon/ao-dai-vn.jpg',
                'content_short' => NULL,
            'content' => '{"section1":{"image":null,"name":{"main":"Southeast Asia","sub":"Luxury Tailor-Made Tours to"},"content":[{"name":"100% Tailor-made","content":"The holiday is designed around your interest, time, need and budget. You are the one who decide when and where to go, where to stay"},{"name":"Local Experts","content":"With more than 15 years of experiences, our knowledgeable staff will advise you the most suitable and wonderful itineraries."},{"name":"Personal Services","content":"You are more comfortable with private car and guides as well as support services 24 hours per day."}]},"section2":{"name":"We offer luxury private tours to Southeast Asia","link":"https:\\/\\/luxurytravelvietnam.com\\/about-us","content":"Luxury Travel Ltd. was founded in 2004 by Mr. Pham Ha, passionate traveler and entrepreneur. With more than 15 years of experience in the travel industry, he is a keynote speaker and luxury travel expert in Vietnam and around the globe. When Pham Ha saw the need for a tour company offering services that would specialize in high-end travelers, he didn\\u2019t hesitate for long. That\\u2019s how the idea of creating a travel company with an emphasis on ultra-luxurious experiences, privately guides and bespoke holidays came to life"},"section3":{"name":"How to book with us?","name_sub":"Amazing holiday packages in 3 simple steps"},"section4":{"a_1557545931483":{"image":"\\/uploads\\/images\\/home-2.jpg","name":"Vietnam","content":"Sensing the Timeless Charm of Vietnam","link":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/vietnam"},"a_1557545932601":{"image":"\\/uploads\\/images\\/new-laos2.jpg","name":"Laos","content":"Savoring the Simply Beautiful Tranquility","link":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/laos"},"a_1557545933324":{"image":"\\/uploads\\/images\\/home-4.jpg","name":"Thailand","content":"Blossoming Soul Amidst Nature","link":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/thailand"},"a_1557545934344":{"image":"\\/uploads\\/images\\/home-5.jpg","name":"Myanmar","content":"Awakening the Rhythms of Inner Bliss","link":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/myanmar"},"a_1557545935091":{"image":"\\/uploads\\/images\\/new-ft1.jpg","name":"Multi countries tours","content":"Do you want to discover not only one but two or three destinations in Southeast Asia?","link":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/multi-country"},"a_1557545936257":{"image":"\\/uploads\\/images\\/home-7.jpg","name":"Cambodia","content":"Cherishing the Heartfelt Hospitality of Khmer People","link":"https:\\/\\/luxurytravelvietnam.com\\/destinations\\/cambodia"}},"section5":{"name":"Best Selling Tours"},"section6":{"name":"Tour Highlights","name_sub":"Find your ideal tours"},"section7":{"name":"Tripadvisor","link":"https:\\/\\/www.tripadvisor.com.vn\\/Attraction_Review-g293924-d5984191-Reviews-Luxury_Travel-Hanoi.html","content":{"1557545895908":{"name":"Michael K","country":"United States","date":"5\\/13\\/2019","content":"<br><b>Amazing Vietnam with Luxury Travel<\\/b><\\/br>We spent several months working with Long Vu (Louis) to tailor the exact trip we wanted for Vietnam. Through multiple emails and iterations, we were able to craft the perfect trip. Everything about it was the first rate, from the planning interaction to the guides and drivers, to the accommodations, to the food, and to the wonderful country All of the meals were excellent and the hotels were wonderful."},"1557545896713":{"name":"Trip821302","country":"United States","date":"4\\/25\\/2019","content":"<br><b>Amazing from beginning to end<\\/b><\\/br>From the booking process (thank you Mrs. Duong) to each city we visited everything was perfect. Our tour guides were fantastic including Mrs. Yen, Thach and Don. Their English was terrific and each extremely knowledgeable of the area. We had private cars that were always furnished with fresh water and air conditioning. It\'s hard to pick a highlight because it was all very special. I highly recommend this company!"},"1557545897336":{"name":"Sarit l","country":"Italy","date":"4\\/1\\/2019","content":"<br><b>A trip to remember<\\/b><\\/br>We got Luxury Travel to help us with planing the itinerary for our 16-day Vietnam trip. I\\u2019m so glad we did.\\r\\nTheir attention to detail, open minded, and care was outstanding. From the meticulous cars and knowledgable guides to the perfect hotel locations and other little surprises made our trip a dream comes true.\\r\\nWe had a blast and truly enjoyed every moment of it. <br>Thank you so much! <\\/br>"}}},"section8":{"name":"News and Special Offers"}}',
                'type' => 'home',
                'status' => NULL,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'Luxury Travel - Luxury Private Tour to Southeast Asia',
                'meta_description' => 'Luxury Travel Vietnam provides the best tailor-made holiday and tour packages to Vietnam, Laos, Cambodia, Myanmar and Thailand.',
                'created_at' => '2019-05-10 05:33:03',
                'updated_at' => '2019-09-04 14:26:43',
                'index' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '{"main":"Customize and Book","sub":"Customize <br>and<\\/br> Book"}',
                'slug' => NULL,
                'image' => NULL,
                'content_short' => NULL,
                'content' => '{"main":"a perfect holiday vacation","sub":"a perfect holiday vacation"}',
                'type' => 'how-it-work',
                'status' => NULL,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => 1,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 06:43:57',
                'updated_at' => '2019-06-27 10:43:00',
                'index' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => '{"main":"Get the free quote","sub":"Get the free <br>quote<\\/br>"}',
                'slug' => NULL,
                'image' => NULL,
                'content_short' => NULL,
                'content' => '{"main":"within 24 hours from our travel experts","sub":"within 24 hours from our travel experts"}',
                'type' => 'how-it-work',
                'status' => NULL,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => 2,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 06:44:59',
                'updated_at' => '2019-06-27 10:42:10',
                'index' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '{"main":"Send us your inquiry","sub":"Send us your inquiry"}',
                'slug' => NULL,
                'image' => NULL,
                'content_short' => NULL,
                'content' => '{"main":"& tell us your preferences","sub":"& tell us your preferences"}',
                'type' => 'how-it-work',
                'status' => NULL,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => 3,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 06:45:13',
                'updated_at' => '2019-06-19 18:03:55',
                'index' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'About',
                'slug' => NULL,
                'image' => 'uploads/images/about-1.jpg',
                'content_short' => NULL,
            'content' => '{"section1":{"image":null,"name":"Who We Are","content":"Luxury Travel Ltd. was founded in 2004 by Mr. Pham Ha, passionate traveler and entrepreneur. With more than 15 years of experience in the travel industry, he is a keynote speaker and luxury travel expert in Vietnam and around the globe. When Pham Ha saw the need for a tour company offering services that would specialize in high-end travelers, he didn\\u2019t hesitate for long. That\\u2019s how the idea of creating a travel company with an emphasis on ultra-luxurious experiences, privately guides and bespoke holidays came to life. As a lover of Arts, Writing, History and Culture, Pham Ha created personalized tours with an immersive approach to traveling."},"section2":{"name":"Our Business Is Sustainable And Responsible","content":[null,null]},"section3":{"image":"/uploads\\/images\\/about-2.jpg"},"section4":{"name":"Our Selling Point","content":[{"name":"100%Tailor-made","content":"At Luxury Travel Vietnam we design customized travel services for leisure, business travelers, and MICE. We operate our own fleet of luxury vehicles and provide access to all kinds of luxury transports. Our team will be your personal experiences manager creating an authentic and 100% tailor-made holiday."},{"name":"Local Travel Experts","content":"With more than 14 years of experiences, our knowledgeable staff will advise you in finding the most suitable and wonderful itineraries according to your tastes and their extended knowledge of each region. We bring to you the best holiday without any middleman cost."},{"name":"Unique Experiences","content":"We understand that you are unique and will make sure that your journey is exceptional. With distinctive activites, off-the-beaten track destinations and immersion in local life, our travel packages are above all, experiential."}]},"section5":{"name":"Creating Authentic Experiences For Discerning Travelers.","image":"/uploads\\/images\\/about-4.jpg","content":{"1557471965295":{"name":"First Luxury TO and DMC in Vietnam","content":"<div class=\\"row\\">\\r\\n<div class=\\"col-md-4\\">\\r\\n<div class=\\"vk-img\\"><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/uploads\\/images\\/dest-1.jpg\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"col-md-8\\">\\r\\n<div>\\r\\n<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>\\r\\n<a class=\\"_link\\" href=\\"#\\">View details<\\/a><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>"},"1557472032823":{"name":"Our Products","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472052076":{"name":"Our Quality Assurance","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472065508":{"name":"Our Reputation","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472075859":{"name":"Our Vision","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472096101":{"name":"Company Information","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}},"section6":{"name":"Meet Our Team","content":"Our team, dedicated to agents and tour operators, will be your personal experience managers and deliver you and your customers the world\\u2019s most authentic luxury travel experiences with excellent service, know-how and trust."}}',
                'type' => 'about',
                'status' => NULL,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-05-10 07:04:10',
                'updated_at' => '2019-05-14 06:42:47',
                'index' => 1,
            ),
            5 => 
            array (
                'id' => 19,
                'name' => 'Core Values',
                'slug' => 'core-values',
                'image' => '{"thumbnail":"/uploads\\/images\\/tours\\/Vietnam\\/core-value-feature.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/core-value.jpg"}',
                'content_short' => NULL,
                'content' => '<p><strong>1. People</strong></p>

<p>At Luxury Travel people are at the heart of everything we do. We use all of our expertise to serve our customers so they can bring home unforgettable memories we helped them create. Our care is represented in our &ldquo;Wow&rdquo; services that come with the most serious customer focus. Our service is sincere and comes from our heart &ndash; to touch yours<strong>.</strong></p>

<p><strong>2. Passion</strong></p>

<p>Every day we work with passion to serve our travelers the best way we can. We are experts in the countries we promote and share the same passion as you for travel. We love creating unique experiences and show you the most authentic part of each country.</p>

<p>3.&nbsp;<strong>Innovation</strong></p>

<p>Creativity, education and continuous innovation are the lifeblood of our company. We welcome individual differences so that each person can contribute with their unique views and set of skills to the organization. Together we are better!</p>

<p><strong>4. Leadership</strong></p>

<p>At Luxury Travel, people are the most important asset. Our team combines their expertise with dedication and attitude to their work and we are all proud to be part of the first luxury travel company in Vietnam.</p>

<p><strong>5. Transparency</strong></p>

<p>Our way of working with our clients is always open, respectful, honest, courteous and helpful.</p>

<p><strong>6. Pride</strong></p>

<p>Luxury Travel is a company filled with friendship, pride, and professional performance. We value democracy, freedom, and happiness and all Luxers understand, share and track their responsibility to provide exceptional service.</p>

<p><strong>7. Collaboration</strong></p>

<p>We keep building win-win partnerships with other partners and through hard work we bring profits and growth for all, hand-in-hand.</p>

<p><strong>8. Commitment</strong></p>

<p>All Luxers are devoted to Luxury Travel. Our team is committed to excellence and going one step further to satisfy our travelers. Great things happen when we work together to achieve our goals!</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-11 16:23:21',
                'updated_at' => '2019-06-27 11:08:58',
                'index' => 1,
            ),
            6 => 
            array (
                'id' => 20,
                'name' => 'Logo Meaning',
                'slug' => 'logo-meaning',
                'image' => '{"thumbnail":"/uploads\\/images\\/final logo.jpg","banner":null}',
                'content_short' => NULL,
                'content' => '<p style="text-align: center;"><img alt="" src="http://2020.luxtravel.in/luxury-travel-en/storage/app/public/images/tours/Vietnam/final-logo.jpg" /></p>

<p style="text-align: center;"><span style="color:#000000;"><span style="font-size:16px;"><strong><em>&ldquo;Touching Hearts&rdquo;</em></strong></span></span></p>

<p style="text-align: justify;">&ldquo;The unveiling of a new logo, a heart icon, marks the latest phase of Luxury Travel&rsquo;s evolution and continuous growth. Our sincere service is from our heart, touching your heart.&rsquo;&rsquo;</p>

<p style="text-align: justify;">The new-look logo symbolizes the innovation, creativity, and development of Lux Travel Group Ltd. The &ldquo;Travel&rdquo; font is smooth, reflecting the flexibility in the services provided by the company.</p>

<p style="text-align: justify;">The logo heart shaped is solid, simple, elegant, luxurious, easy to understand and eye-catching. The logo has two dominant colors. The&nbsp;<strong>black</strong>&nbsp;stands for the high standard and the&nbsp;<strong>gold color</strong>&nbsp;for luxury. The gold color also represents balance and stability because it is important for us to work on win-win situations.&nbsp;<strong>White</strong>&nbsp;is for our transparency.</p>

<p style="text-align: justify;">The shape of the heart icon reflects the core values of excellent service by which we judge our performance in everything we do: Attitude, Art of Service and Efficient Processes. Our people have the knowledge, skills and positive attitude in their hearts to provide excellent service to the most sophisticated travelers.</p>

<p style="text-align: justify;">Our Mission is to provide &ldquo;Wow&rdquo; Services to the Most Sophisticated Travelers. And our new slogan &ldquo;Touching Hearts&rdquo; is a strong message, a firm commitment, and, a promise to our clients and partners.</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-11 16:26:12',
                'updated_at' => '2019-06-27 11:02:08',
                'index' => 1,
            ),
            7 => 
            array (
                'id' => 21,
                'name' => 'Our Awards',
                'slug' => 'our-awards',
                'image' => '{"thumbnail":"/uploads\\/images\\/tours\\/Vietnam\\/tripadvisor-2019.png","banner":null}',
                'content_short' => NULL,
                'content' => '<p><strong>Award Winning Tour Operator</strong></p>

<p style="text-align: justify;">Being in the travel industry for almost 15 years, Luxury Travel has been recognized for its excellence in service by many awards.</p>

<p style="text-align: justify;">Also, by acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We believe that by taking measured steps to build corporate and social responsibility into every aspect of our business, we can reduce the negative effects of travel on our environment. We continue to explore innovative and positive ways to promote Vietnam and Indochina always respecting local life and encouraging local businesses. In 2008, we won an award for sustainable development, and we are confident that, with your help, more awards are on the way.</p>

<center><p style="text-align: justify;"><img alt="" src="http://2020.luxtravel.in/luxury-travel-en/storage/app/public/images/tours/Vietnam/our-award-3.jpg" /></p></center>

<p style="text-align: justify;"><strong>Our Awards throughout the years</strong></p>

<p style="text-align: justify;">&ldquo;Excellent Performance Award 2005-2006-2007-2008-2009-2010-2011-2012-2013-2014-2015-2016-2017-2018&rdquo;- The Guide Awards, Vietnam&rsquo;s Leading Lifestyle and Travel Magazine</p>

<p style="text-align: justify;">&ldquo;Best Service Tour Operator Award 2010-11-12&rdquo;- Top Trade Services Award by the Ministry of Industry and Trades of Vietnam</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-11 16:27:08',
                'updated_at' => '2019-06-27 11:29:07',
                'index' => 1,
            ),
            8 => 
            array (
                'id' => 22,
                'name' => 'Wow Services',
                'slug' => 'wow-services',
                'image' => '{"thumbnail":"/uploads\\/images\\/tours\\/Vietnam\\/wow-service-feature.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/banner-cruise-1.jpg"}',
                'content_short' => NULL,
                'content' => '<p style="text-align: justify;">We commit to providing you the highest level of professionalism and top-notch services, as well as fast, accurate response to your needs.</p>

<p style="text-align: justify;">Our mission statement is to deliver&nbsp;<strong>&ldquo;wow&rdquo; services</strong>&nbsp;for holidays in Southeast Asia with luxury, privately guided and fully bespoke itineraries with sincere customer care from our heart &ndash; to&nbsp;<em>yours</em>.</p>

<ul>
<li style="text-align: justify;">We bring you Authentic Travel Experiences</li>
<li style="text-align: justify;">We have our own modern fleet of air-conditioned vehicles, from a standard car to a 16-seat Mercedes Benz luxury van to a full-size coach.</li>
<li style="text-align: justify;">Other modes of transport include private yachts, private jets, helicopters and luxury trains</li>
<li style="text-align: justify;">A 24/7 Concierge service on the ground. We guarantee that you will have 24/7 support on the ground and one dedicated agent from beginning to end. Our team speaks French, English, Italian, Spanish and, German, so we can fix any problem right away on your behalf.</li>
<li style="text-align: justify;">We book accommodations appropriated to the type of tour selected and your preferences and use the best at that level.</li>
<li style="text-align: justify;">We have visited our destinations, and have personally checked out the finest hotels, resorts, and restaurants designed for discriminating travelers, so we can recommend our travelers.</li>
<li style="text-align: justify;">As a high-end tour operator, we listen carefully to our travelers in order to deliver the right itineraries, experiences and we continue to raise the bar to exceed even the most sophisticated customer&lsquo;s expectations.</li>
</ul>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-11 16:28:24',
                'updated_at' => '2019-06-27 11:41:57',
                'index' => 1,
            ),
            9 => 
            array (
                'id' => 23,
                'name' => 'Privacy Policies',
                'slug' => 'privacy-policies',
                'image' => '{"thumbnail":"/uploads\\/images\\/tours\\/Vietnam\\/privacy-policy.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/privacy.jpg"}',
                'content_short' => NULL,
                'content' => '<p style="text-align: justify;">We are committed to protecting your privacy while on our website. The purpose of this privacy policy is to inform you as to what information may be collected when visiting the site, how this information is used by us, how such information may be shared with a third party, and how you can edit, update, correct or delete such information.</p>

<p style="text-align: justify;">1.<strong> Collecting personal information:</strong></p>

<ul>
<li style="text-align: justify;">The information collected through our website will help Luxury Travel Co., Ltd to<br />
&nbsp;
<ul>
<li>Support customers when purchasing our products</li>
<li>Troubleshooting customers</li>
<li>Provide you with the latest information on our Website&nbsp;</li>
<li>Review and update the content and interface of Website</li>
<li>Conduct customer surveys</li>
<li>Implement promotional activities related to the products and services of https://luxurytravelvietnam.com.</li>
</ul>
</li>
<li style="text-align: justify;">To access and use certain services at&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>, you may be required to register with our personal information (email, name, Telephone No. &hellip; ). All information declared to ensure accuracy and legality.</li>
<li style="text-align: justify;">We may also collect information about the number of visits, including the number of pages you view, the number of links you click and other information related to the connection to&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>. We also collect information that Web browsers (Browser) you use when accessing the website&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>, including IP address, browser type, language use, and the time Browser address access to.</li>
</ul>

<p style="text-align: justify;"><strong>2.&nbsp;Using personal information:</strong></p>

<ul>
<li style="text-align: justify;"><a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>&nbsp;collects and uses your personal information for the purpose consistent and fully compliant with the contents of the &rdquo; Private Policy &ldquo;.</li>
<li style="text-align: justify;">When necessary, we can use this information to contact you directly such as send welcome letters, orders, letters of thanks, and technical information about security, you can receive regular supply information of products, services, upcoming events or employment information if you subscribe to email notifications.</li>
</ul>

<p style="text-align: justify;"><strong>3. Sharing personal information:</strong></p>

<ul>
<li style="text-align: justify;">Except for the case of using personal information as described in this policy, we are committed to not disclose your personal information public.</li>
<li style="text-align: justify;">In some cases, we are able to rent a unit to conduct an independent market research project and then your information will be provided for this unit to carry out the project. This third party will be bound by a confidentiality agreement under which they are allowed to use the information provided for the purpose of completing the project.</li>
<li style="text-align: justify;">We may disclose or provide your personal information in the case which is really necessary as follows : ( a) upon request of law enforcement agencies ; ( b ) in cases where we believe it will help us to protect their legitimate rights under the law ; ( c ) emergency and necessary to protect the personal safety of other members of Luxury Travel Co., Ltd.</li>
</ul>

<p style="text-align: justify;"><strong>4. Accessing personal information:</strong></p>

<p style="text-align: justify;">Any time you can also access and edit your personal information under the appropriate link (&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>) that we provide.</p>

<p style="text-align: justify;"><strong>5. Secured personal information :</strong></p>

<ul>
<li style="text-align: justify;">When you submit your personally identifiable information to us, you agree to the terms which we have mentioned above,&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>&nbsp;committed to protecting the personal information of customers in every way possible. We will use information technology security such as international standard PCI, SSL,&hellip; to protect this information from being retrieved, use or disclose unintended.</li>
<li style="text-align: justify;">However, due to technical limitations, no data can be transmitted over the internet connection which can be 100 % secure. Therefore, we can not give a firm commitment that the information you give us will be kept confidential absolutely safe , and we can not take responsibility in the event of unauthorized accessing personal information such as you voluntarily share information with others &hellip;. If you do not agree with the terms as described above, we recommend that you should not send information to us .</li>
<li style="text-align: justify;">So you should also recommend security information relating to your access password and you should not share with anyone else.</li>
<li style="text-align: justify;">If you are using a public computer, you should log out or exit all open windows Website.</li>
</ul>

<p style="text-align: justify;"><strong>6. Using &ldquo;Cookies &rdquo;</strong></p>

<ul>
<li style="text-align: justify;">https://luxurytravelvietnam.com use &rdquo; cookies &rdquo; to help personalize and maximize efficient use of your time online visitors.</li>
<li style="text-align: justify;">A cookie is a text file that is placed on your hard disk by a Web page server. Cookies can not be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to your computer and can only be read by a web server in the domain that issued the cookie to you.</li>
<li style="text-align: justify;">One of the primary purposes of cookies is to provide the utility to save time when accessing your website or visit the website https://luxurytravelvietnam.com again without re-registering available information.</li>
<li style="text-align: justify;">You can accept or reject cookies. Most Browser automatically accepts cookies, but you can change the settings to refuse all cookies if you prefer. However, if you choose to decline cookies, that may hinder or negatively affected some services and features on the website depends on cookies.</li>
</ul>

<p style="text-align: justify;"><strong>7. Regulation on &rdquo; Spam &rdquo; :</strong></p>

<ul>
<li style="text-align: justify;"><a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>&nbsp;really concerns the problem of Spam ( junk mail), the fake identities Email we sent. Therefore,&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>&nbsp;confirm only send email to you when and only when you have to register or use services from our system.</li>
<li style="text-align: justify;">&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>&nbsp;commitment not to sell, rent or lease your email from third parties. If you inadvertently receive unsolicited email from our system due to an unexpected cause, please click on the link from this email disclaimer attached, or notice directly to the website administrator.</li>
</ul>

<p style="text-align: justify;"><strong>8. Policy changes :</strong></p>

<ul>
<li style="text-align: justify;">We can completely change the content in this site without prior notice, to suit the needs and demands of Luxury Travel Co., Ltd as well as feedback from customers, if any. When updating the content of this policy, we will revise the time &ldquo;Last Updated &rdquo; below.</li>
<li style="text-align: justify;">Content &ldquo;Private Policy&rdquo; applies only in https://luxurytravelvietnam.com, not including or related to third parties to place ads or links in&nbsp;<a href="https://luxurytravelvietnam.com/">https://luxurytravelvietnam.com</a>. We encourage you to read the Private Policy and Safety of the third-party site before providing personal information to such sites. We are not responsible for any way for the content and legality of the third-party website.</li>
<li style="text-align: justify;">So, you agree that, when you use our website after editing means you have to admit, agree to comply with and trust in this edit. Therefore, we suggest that you preview the content of this page before accessing other content on the website and you should read and study carefully the content &ldquo;Private Policy &rdquo; of each website that you are browsing.</li>
</ul>

<p style="text-align: justify;"><strong>9. Contact Information</strong>:</p>

<p style="text-align: justify;">We always welcome comments, contact, and feedback from you about the &ldquo;Private Policy &ldquo;. If you have questions related please contact us at the email address:&nbsp;<a href="mailto:sales@luxurytravelvietnam.com">sales@luxurytravelvietnam.com</a></p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-11 16:30:07',
                'updated_at' => '2019-06-27 11:12:14',
                'index' => 1,
            ),
            10 => 
            array (
                'id' => 24,
                'name' => 'Payment Policies',
                'slug' => 'payment-policies',
                'image' => '{"thumbnail":"/uploads\\/images\\/tours\\/Vietnam\\/payment-policy-ft.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/payment.jpg"}',
                'content_short' => NULL,
                'content' => '<p style="text-align: justify;"><strong>1. Deposits &amp; Payment Policy:</strong></p>

<ul>
<li style="text-align: justify;">Deposit of 20% of the total of the tour is required upon confirmation of booking tour. &nbsp;You have peace of mind until 2 months before departure dates. At that point, we would require<strong>&nbsp;full prepayment only 60 days before departure for all services</strong>&nbsp;by money transfer or credit card payment with all bank transfer charges/ fees to be paid by the client.</li>
<li style="text-align: justify;">All the tour packages on this website operate on a pre-payment basis. Payment can be made either using a major credit card as Visa, Mastercard and American Express Credit Card,&nbsp;<strong>not including</strong>&nbsp;<strong>credit card transaction fee</strong>&nbsp;( Please add in 3% fee for Visa, Mastercard or 4% for American Express to the total amount of payment each time).</li>
<li style="text-align: justify;">If you wish to pay us online on our secure server, please click&nbsp;<a href="https://luxurytravelvietnam.com/payment/"><strong>here</strong></a>&nbsp;and fill in the form.</li>
<li style="text-align: justify;">After receiving your payment, we will issue you the hotel voucher and/or final confirmation itinerary, which can be sent to you by fax or e-mail.</li>
</ul>

<p style="text-align: justify;"><strong>2. Booking Amendment</strong></p>

<p style="text-align: justify;">Flexibility is our strongest point. You can amend your booking when it is made. Just contact us at&nbsp;<a href="mailto:Sales@luxurytravelvietnam.com">sales@luxurytravelvietnam.com</a>&nbsp;with your booking number. Our reservations team is glad to amend your booking accordingly.</p>

<p style="text-align: justify;"><strong>3. Cancellation Policy</strong></p>

<p style="text-align: justify;"><strong>Non-Refundable:</strong>&nbsp;</p>

<p style="text-align: justify;">NON REFUNDABLE deposit of USD$20 per person for hotels and air ticket booking upon services confirmation and US$ 100 per person upon the packages tours services confirmation</p>

<p style="text-align: justify;"><strong>Cancellations timeline will be charged as follows:</strong></p>

<ul>
<li style="text-align: justify;">60 days prior to the arrival date: Non Refundable deposit charge and the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</li>
<li style="text-align: justify;">59 &ndash; 31 days prior to arrival date: Cancellations fee of 30% per group/booking and plus the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</li>
<li style="text-align: justify;">30 days &ndash; 15 days prior to arrival date: Cancellations fee of 50% per group/booking plus the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</li>
<li style="text-align: justify;">14 &ndash; 7 days before arrival: Cancellations fee 70% charge plus the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</li>
<li style="text-align: justify;">Less than 7 days before arrival Or No show: 100 % charge per group/booking.</li>
</ul>

<p style="text-align: justify;">*** Nonrefundable deposit and banking service fee will be forfeited in all cancellation cases.</p>

<p style="text-align: justify;"><strong>4. Refund of Unused Services</strong></p>

<p style="text-align: justify;">No refund will be given for any unused service after the trip has commenced.</p>

<p style="text-align: justify;"><strong>5. Liability</strong></p>

<p style="text-align: justify;"><strong>Luxury Travel Co., Ltd</strong>. is not responsible for any loss, injury or damage sustained by passengers. Additional expenses incurred due to delays, accidents, natural disaster, political actions, and unrest must be borne by the passengers. Passengers are required to have full travel insurance.</p>

<p style="text-align: justify;">Airline schedules and local conditions may affect accommodation and itineraries. Should this occur,&nbsp;<strong>Luxury Travel Co., Ltd</strong>&nbsp;will endeavor to substitute a suitable arrangement of similar value. Participation on any tours implies full agreement to the above conditions by all parties involved.</p>

<p style="text-align: justify;"><strong>6. Passport and Entry Visa</strong></p>

<p style="text-align: justify;">Your passport must be valid for at least six months following your entry date to your travel destination.</p>

<p style="text-align: justify;">It is your responsibility to ensure that you are in possession of a valid passport and visa and all travel documents are valid for travel.</p>

<p style="text-align: justify;"><strong>7. Travel Insurance</strong></p>

<p style="text-align: justify;">Travel insurance is not included in your tour price. It is the passenger&rsquo;s responsibility to verify whether his/her local health insurance carrier provides coverage while traveling. Please contact your insurance carrier for details.</p>

<p style="text-align: justify;"><em>Luxury Travel Co., Ltd.&nbsp;strongly advises that you purchase a comprehensive travel insurance policy to cover all aspects of your tour: the loss of deposits through cancellation lost of baggage and personal items, personal injury and death. A waiver must be signed if insurance is declined.</em></p>

<p style="text-align: justify;"><em>If you wish to speak to us, please call our Reservation Center at ++84.24.3927 4120 and live chat during our business office hours.</em></p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-11 16:31:07',
                'updated_at' => '2019-07-01 15:29:04',
                'index' => 1,
            ),
            11 => 
            array (
                'id' => 29,
                'name' => 'Best Vietnam & Cambodia itinerary 9 days',
                'slug' => '/vietnam-cambodia-itinerary-9-days.html',
                'image' => '{"thumbnail":"\\/uploads\\/images\\/featured-image\\/cambodia\\/bayon-Cambodia.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/halong.jpg"}',
                'content_short' => 'Beautiful, honest and affordable are what you should consider about a Vietnam and Cambodia vacation this summer. Read on to see what to explore, what is the best travel time and which tips and tricks to enjoy your trip amazingly.',
                'content' => NULL,
                'type' => 'blog',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-27 10:01:23',
                'updated_at' => '2019-09-04 11:26:19',
                'index' => 1,
            ),
            12 => 
            array (
                'id' => 30,
                'name' => 'Top 5 benefits of private tours to Vietnam and Cambodia',
                'slug' => '/benefits-of-private-tours-to-vietnam-cambodia.html',
                'image' => '{"thumbnail":"\\/uploads\\/images\\/featured-image\\/Vietnam\\/sapa_602858651.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/Vietnam-Sapa-Muong_Hoa_valley-1.jpg"}',
                'content_short' => 'Are you confused on private tours to Vietnam and Cambodia? Is it safe? – Yes, definitely! Safe, Affordable and Amazing experiences on these beautiful countries are waiting for you to explore.
Travel is to immerse yourself into a new culture and get a deeper understanding about other cultures and their people and cuisine. If you are wondering what to spend your summer vacation – it is worth a try to land on Vietnam and Cambodia',
                'content' => '<p><meta charset="utf-8" /></p>

<p style="text-align: justify;">&nbsp;</p>',
                'type' => 'blog',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-27 11:19:52',
                'updated_at' => '2019-09-04 11:26:02',
                'index' => 1,
            ),
            13 => 
            array (
                'id' => 31,
                'name' => '09 must-see art performances for Vietnam cultural tours',
                'slug' => '/art-performances-vietnam-cultural-tours.html',
                'image' => '{"thumbnail":"\\/uploads\\/images\\/featured-image\\/Vietnam\\/shutterstock_1186806784.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/shutterstock_705177526.jpg"}',
                'content_short' => 'After more than 4,000 years of history, this S-shaped country has developed a rich fine art system with a wide range of traditional art forms. If you are about to take Vietnam cultural tours, never miss out following performances or you will definitely regret.',
                'content' => NULL,
                'type' => 'blog',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-06-27 16:15:34',
                'updated_at' => '2019-09-04 11:25:14',
                'index' => 1,
            ),
            14 => 
            array (
                'id' => 32,
                'name' => 'Frequently Asked Questions',
                'slug' => 'faqs',
                'image' => '{"thumbnail":"/uploads\\/images\\/featured-image\\/Vietnam\\/shutterstock_1022636377.jpg","banner":"uploads\\/images\\/tours\\/Vietnam\\/ninhbinh_526010656.jpg"}',
                'content_short' => NULL,
                'content' => '<p aria-controls="ui-id-2" aria-expanded="true" aria-selected="true" id="ui-id-1" role="tab" tabindex="0"><strong>I. Visa Matters</strong></p>

<p><strong>1. Do I need a visa to visit Vietnam, Cambodia, Laos, Myanmar or Thailand?</strong></p>

<p>or more information about the necessary Visas for our destinations, go to the Visa Information pages for each country.</p>

<p><a href="http://2020.luxtravel.in/luxury-travel-en/visa-to-vietnam.html">Vietnam Visa</a></p>

<p><a href="http://2020.luxtravel.in/luxury-travel-en/visa-to-cambodia.html">Cambodia Visa</a></p>

<p><a href="http://2020.luxtravel.in/luxury-travel-en/visa-to-laos.html">Laos Visa</a></p>

<p><a href="http://2020.luxtravel.in/luxury-travel-en/visa-to-myanmar.html">Myanmar Visa</a></p>

<p><a href="http://2020.luxtravel.in/luxury-travel-en/visa-to-thailand.html">Thailand Visa</a></p>

<p>Please make sure that you have your passport, which is still valid for six months and contains at least two white pages. If you book services with Luxury Travel, we offer you the complimentary visa approved letter. &nbsp;Kindly contact your travel advisor for this free visa service once your booking has been confirmed.</p>

<p><strong>II. Making a Reservation with www.luxurytravelvietnam.com</strong></p>

<p><strong>1. How do I reserve my tours with Luxury Travel?</strong></p>

<p>Just complete and send the reservation form on our website, making sure that you have given us your e-mail address and phone number. As soon as we receive your request we will contact you to confirm the reservation. It is our policy to reply to all requests within 24 hours.</p>

<p><strong>2. Do you have tours available on specific dates?</strong></p>

<p>Almost all of our tours are daily departure. To make a booking, you must submit a reservation form. Then we process the booking and reply to you within 24 hours.</p>

<p><strong>3. I don&rsquo;t want to make a reservation online, what can I do?</strong></p>

<p>You can reserve by phone or by fax: You can call our 24/7 call center hotline at&nbsp;++ 84.1234 68 69 96 or ++ 84.24.3927 4120&nbsp;or send us a fax using our form at&nbsp;++84.24.3927 4118.</p>

<p><strong>4. Can I make a reservation directly through a travel agent?</strong></p>

<p>Yes, of course if you would like to make a reservation directly with the tour operator or travel agent, you will need to contact them directly.&nbsp;However, we are in good position to make the offer, make the booking and run the trip as we are a local tour operator.</p>

<p><strong>5. What if the preferred hotel is not available?</strong></p>

<p>You will be notified via an e-mail (within 24 hours after placing your order) and we will do our best to suggest or book an alternative similar hotel based on your preferences and our experience.</p>

<p><strong>6. What is the latest time that I can make my reservation?</strong></p>

<p>We can only guarantee bookings made at least 3-working days&nbsp;(not including Sun &amp; Public holidays)&nbsp;before the departure date.</p>

<p><strong>7. How far in advance can I book?</strong></p>

<p>You can book as far as 24 months in advance, but please ensure you remember you have made the reservation. We will charge 20% of total rate is required upon the confirmation of services require full payment 60 days before departure.</p>

<p>You can expect to receive the invoice as a reminder well in advance of your trip.</p>

<p><strong>8. How do I make a reservation for someone else?</strong></p>

<p>To complete a reservation for someone else, simply type in his or her name instead of yours for guest name. Don&rsquo;t forget to use your email address to receive your confirmation information.</p>

<p><strong>9. What happens to my tour booking if my flight is delayed?</strong></p>

<p>Please inform us and the hotel immediately as any changes or fee will depend on the hotel policy. We will also try our best to offer you similar ground services.</p>

<p><strong>10. Is it reliable to book online?</strong></p>

<p>Making an online reservation is easy and safe. Just complete the secure reservations form (SSL &ndash; 128bit), making sure that you give us your e-mail address.. As soon as we receive your request we will reply within 24 hours.</p>

<p><strong>III. Confirmation of my reservation</strong></p>

<p><strong>1. What happens when I make a reservation?</strong></p>

<p>Upon receiving your order we will process the booking. All reservations will be followed up within 24 hours with emails to confirm your booking.</p>

<p><strong>2. What is a &ldquo;confirmation voucher&rdquo;?</strong></p>

<p>Once the service you have booked is confirmed, we will send you an email confirmation voucher detailing all the reservation and price conditions. We suggest you to make a printout of this voucher to be kept and presented when requested. Your voucher will be accompanied by a booking confirmation number. Make sure to have it in hand upon arrival or hotel check in.</p>

<p><strong>3. How Long Does It Take to Get Your First Proposal Letter from Luxury Travel?</strong></p>

<p>Upon receiving your request, you will receive the first acknowledgement to reassure you that we are giving your request immediate attention.&nbsp;Rest assured that your request is always our top priority.&nbsp;The full detailed proposal will be in your inbox no later than 24 hours or within 48 hours in the case of public holidays or special events.</p>

<p><strong>IV. Changing/Canceling a reservation</strong></p>

<p><strong>1. What is your cancellation policy for hotels/tour reservation</strong>s?</p>

<p>Non-Refundable:</p>

<p>+ NON REFUNDABLE deposit of USD$20 per person for hotels and air ticket booking upon services confirmation and US$ 100 per person upon the packages tours services confirmation</p>

<p>Cancellations will be charged as follows:</p>

<ul>
<li>
<p>60 days prior to the arrival date: Non Refundable deposit charge and the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</p>
</li>
<li>
<p>59 &ndash; 31 days prior to arrival date: Cancellations fee of 30% per group/booking and plus the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</p>
</li>
<li>
<p>30 days &ndash; 15 days prior to arrival date: Cancellations fee of 50% per group/booking plus the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</p>
</li>
<li>
<p>14 &ndash; 7 days before arrival: Cancellations fee 70% charge plus the cancellation fees applied by the concerned services suppliers such as flight company, boat company, train, hotels&hellip; in accordance with their cancellations policies if any.</p>
</li>
<li>
<p>Less than 7 days before arrival Or No show: 100 % charge per group/booking.</p>
</li>
</ul>

<p>*** Nonrefundable deposit and banking service fee will be forfeited in all cancellation cases.</p>

<p>Refund of Unused Services</p>

<ul>
<li>
<p>No refund will be given for any unused service after the trip has commenced.</p>
</li>
</ul>

<p><strong>2. How about the Credit Cards Refunds?</strong></p>

<p>All credit card refunds will be processed in a timely manner. The refunds process dependent on your billing cycle with the credit card company. Once the refund instructions leave our office it can take anywhere from 15-45 working days for the proceeds to arrive in your account.</p>

<p>For more information on the refund, feel free to contact to our accounting department by email:&nbsp;<a href="mailto:sales@luxurytravelvietnam.com">sales@luxurytravelvietnam.com</a></p>

<p><strong>V. Children</strong></p>

<p><strong>1. Do children receive discounted rates?</strong></p>

<p>Yes, we are a child friendly company and we apply the following discounts:</p>

<p>Children under 2 years old are free of charge for land services, only charge fee from airlines if have</p>

<p>Children from 2-6 years old: 50% of adult price</p>

<p>Children from 7-11 years old: 75% of adult price</p>

<p>Children from 12 years old up: 100% adult price</p>

<p><strong>2. Do children under 12 years old stay in the same room with their parents?</strong></p>

<p>Generally, children under 12 years of age can stay for free in their parent&rsquo;s room using the existing bedding. The age requirement differs depending on the specific hotel&rsquo;s policy.&nbsp;The general policy concerning room and breakfast charges for children traveling together with their parents or family is as follows:</p>

<p>Children more than 12 years of age: Any child this age is considered an adult by the travel industry. Normal charges will apply both for the room and the breakfast.</p>

<p>Free baby cots are only available for children under 2 years old upon request.</p>

<p><strong>VI. Website Security and Privacy Policy</strong></p>

<p><strong>1. How safe are my credit card details?</strong></p>

<p>The importance of security for our clients is of the utmost concern to us. We exercise great care in providing secure transactions from your computer to our server. We use SSL and our site is secure for all transactions. Unfortunately, no transaction is 100% secure. We are committed to protecting you and your personal information and will continue to monitor security options in the future.</p>

<p><strong>2. What do you do with my personal information?</strong></p>

<p>We have the utmost respect for your privacy and security. We never divulge any information to anyone without your consent.</p>

<p><strong>3. How long does it take to get a confirmation?</strong></p>

<p>We have the utmost respect for your privacy and security. We never divulge any information to anyone without your consent.</p>

<p><strong>VII. Prices, Deposit &amp; Full Payment</strong></p>

<p><strong>1. Are the rates on your site per person for minimum departure from 2 people?</strong></p>

<p>Yes, most of our tour departures are private or small groups. &nbsp;Prices are based on 2 people for our scheduled tours.</p>

<p><strong>2. What credit / debit cards are accepted?</strong></p>

<p>We currently accept&nbsp;Visa and Master credit cards&nbsp;with a 3% bank fees and&nbsp;AMEX&nbsp;with a 4% bank fees.</p>

<p><strong>3. What is the deposit? When will I be fully charged?</strong></p>

<p>Once we have received your credit card details, we will charge a non-refundable USD$100 deposit if the reservation has been made at least 60 days before departure date. If the reservation is made within 60 days before departure, we will charge the full amount upon confirmation. Please be advised that we will charge your credits card only after having supplied you with a confirmed reservation.</p>

<p><strong>4. Can I pay by cash upon on arrival, traveler cheques, bank draft, bank transfer?</strong></p>

<p>All our tours packages and hotels operate on pre- paid basis. So we can only accept the payment in advance by bank transfer or credit card. If you wish to make wired bank transfer, please transfer to our bank account listed below:</p>

<p>A/C Name: LUXURY TRAVEL Co., LTD<br />
ANZ Bank, Hanoi Branch<br />
Address: 14 Le Thai To Street, Hanoi, Vietnam<br />
A/C Number: 6276881<br />
SWIFT : ANZ BVNVX<br />
Website :<a href="https://www.luxurytravelvietnam.com/EN/GE/www.anz.com/vietnam">&nbsp;www.anz.com/vietnam</a></p>

<p><strong>VIII. Payment</strong></p>

<p><strong>1. My credit card number is correct, but it is not accepted on your online reservation form / I am getting an error while making a payment? Why?</strong></p>

<p>There are a few possibilities:<br />
&ndash; Make sure you are not using a proxy internet connection. A direct connection to internet is required for security purposes<br />
&ndash; Our payment gateway opens in a new browser window. Make sure that pop up blocking software/features are turned off<br />
&ndash; Make sure the input details are correct<br />
&ndash; Make sure the card expiration date does not precede the reservation date<br />
&ndash; Make sure you have not reached your credit limit<br />
&ndash; If you get an error apart from the reasons above, please contact our online agent</p>

<p><strong>2. Why is the price different from when I last checked and/or in comparison with my last stay?</strong></p>

<p>This normally depends on market conditions and currency fluctuations. During peak tourist season, conventions, fairs etc., the prices are likely to change. However, once booked, the price for your hotel is fixed &nbsp;at the time of reservation.</p>

<p><strong>3. Can I book/ pay directly to the hotel?</strong></p>

<p>No. The rates displayed on Luxury Travel&rsquo;s website are available only through&nbsp;our Vietnam hotel and resort&nbsp;reservation service. Hotel walk-in rates are up to 75% more expensive. Our online agents are able to assist you with alternative hotels in case of non-availability and with other travel requirements which individual hotels cannot. All reservations on https://luxurytravelvietnam.com must be paid for&nbsp;in advance&nbsp;upon receiving confirmations from us. This is the only way to guarantee occupancy at the hotels and keep our prices low.</p>

<p><strong>IX. Miscellaneous</strong></p>

<p><strong>1. I Am a Foreigner/Vietnamese Man, Can I Share a Room with a Vietnamese Lady/Ladies?</strong></p>

<p>Vietnamese Law requires a Marriage Certificate if a man stays overnight with a Vietnamese woman. We recommend you to mention this if it is your case and we will try to provide with appropriate advice when make the booking with us.</p>

<p><strong>2. Who is my guide?</strong></p>

<p>We currently have 25 luxury travel guides that have all been carefully selected and trained. They are open-minded, well-educated and enthusiastic young people who can provide the traveler with first-hand knowledge of Vietnamese culture and history. They are also curious about foreign cultures and are eager to engage in a cultural exchange with visitors. Our guides speak&nbsp;English, French, Italy, Spanish and German. Many&nbsp;of them speak multiple languages.</p>

<p>We have guides in the major regions of Indochina (Hanoi, Danang/Hue,&nbsp;Ho Chi Minh City, Phnom Penh, Seam Reap, Vientiane and Luang Phrabang). Upon arrival at the airport, a local guide will meet you and be your escort for the duration of your stay in that particular city. However, you have the option of hiring a guide or a tour leader who will accompany you through your entire journey.</p>

<p><strong>3. What are the check in and check out time at hotels?</strong></p>

<p>The normal policy of hotels for check in time is after 14:00 hours and check out before 12:00.</p>

<p>Early check in or late check out is available depending on the room&rsquo;s availability. &nbsp;An extra charge may be applied.</p>

<p><strong>4. What does &ldquo;rack rate&rdquo; mean?</strong></p>

<p>Every hotel has what is known in the hotelier industry as a &ldquo;rack rate&rdquo; (also known as full rate or published rate). &nbsp;&nbsp;These are the top rates a hotel charges their guests. While few guests actually pay this rate, hoteliers use these rates as a basis for discounting. Most hotels routinely offer discounts ranging from 10-30% simply for the asking.</p>

<p>Many business hotels offer bigger discounts on weekends when few business people travel. Likewise, resort hotels often discount their rates during the week and slash their rates during the off-season. Obtaining these discounted rates is often simply a matter of requesting the&nbsp;&ldquo;corporate rate&rdquo;, &ldquo;weekend package&rdquo;, or&nbsp;&ldquo;low-season discount&rdquo;.</p>

<p>Since Luxury Travel books thousands of rooms with hotels/resorts in Vietnam and Indochina, &nbsp;we get the wholesale contract and we pass this saving on to you. You can save up to 75% off rack rates.&nbsp;&nbsp;Book Online&nbsp;With Us Today!</p>

<p><strong>5. What meal is included? What is the restaurant like?</strong></p>

<p>The breakfasts are included in the hotel. Others meals are included as stated in the itinerary.</p>

<p>We use local restaurants for all our trips and we select restaurants based on our experiences, client satisfaction, location and cuisine.</p>

<p><strong>6. Can I have vegetarian food?</strong></p>

<p>Yes, of course, you just select your preference on the special request section of&nbsp;&nbsp;the reservation form.</p>

<p><strong>7. Is it safe to travel in Vietnam? Can I travel solo?</strong></p>

<p>Traveling in Vietnam is generally safe. You can travel solo. Women and independent travelers have found it relatively hassle-free and easy to travel throughout the country. However, when traveling anywhere in the country, travelers should exercise caution and use common sense ty incidents.</p>

<p><strong>8. Do you recommend a Travel Insurance?</strong></p>

<p>We strongly recommend that all clients take our comprehensive travel insurance when making a booking.</p>

<p>This covers loss of deposit and other monies through cancellation, loss or damage to personal baggage and loss of money, medical expenses, and additional expenses to cover hotel accommodation and repatriation costs should any services need to be extended or curtailed due to illness or other insurance risk.</p>

<p><strong>9. Where can I find information about the destination and country I want to stay in?</strong></p>

<p>Everything you need can be found within our site. Just check our&nbsp;<a href="https://luxurytravelvietnam.com/vietnam-travel-guide/">Travel Information</a>&nbsp;<br />
<br />
<strong>10. Can I have references for your company?</strong></p>

<p>We have been professionals in the tourism industry for more than 10 years. We are a multiple award winning company and a member of the Ministry of Tourism, Hanoi Department of Tourism, PATA and ASTA. &nbsp;Also, our company has been nominated as the best luxury tour company in Vietnam by the readers of the Vietnam Economic Times.</p>

<p>International Trading Name in English:&nbsp;LUXURY TRAVEL CO., LTD&nbsp;<br />
Company Name in Vietnamese:&nbsp;Cong Ty TNHH Du Lich Sang Trong Viet nam:&nbsp;<br />
Company Licence &amp; Tax Id Number: 0101622092 issued by The General Taxes Department of Vietnam.<br />
International&nbsp;Luxury Tour Operator Licence: 01-074/TCDL-GPLHQT&nbsp;issued by the Vietnam National Administration of Tourism.<br />
Luxury Travel&nbsp;is our branding name, registered by the National Office of Intellectual Property of Vietnam.</p>

<p>Our satisfied clients are happy to share their unique experiences with you upon request. What are our clients saying?<a href="https://www.tripadvisor.com.vn/Attraction_Review-g293924-d5984191-Reviews-Luxury_Travel-Hanoi.html">&nbsp;Click Here!</a></p>

<p><strong>11. Who can I contact if I need immediate help?</strong></p>

<p>If traveling with your tour guide, please contact &nbsp;him first. If this does not resolve your issue , you can contact our Customer Service Manager in French, English and Vietnamese twenty-four hours a day: ++84.1234 68 69 96 &ndash; Mr. Nguyen (English, French and Vietnamese) or &nbsp;Mr Phan : + 84 989 383572 (English).</p>

<p><strong>12. Is there someone pick up me at airport?</strong></p>

<p>Our tour package is generally airport to airport, so check if your airport transfer is included in our services voucher.</p>

<p><strong>&nbsp;13.&nbsp;Can I change the travel dates after issuing the tickets and hotel vouchers?</strong></p>

<p>Packages usually have certain booking terms and restrictions and date changes may be subject to charges or not permitted.</p>

<p><strong>14. You are a luxury tour company. When I purchase a standard tour package from you, Can I Expect the Luxury?</strong></p>

<p>No, We Sell Luxury Products and Non Luxury Products. The Tour Packages Are Mentioned Clearly: Standard (2 star hotel), Superior (3 star), First Class (4 star), Deluxe (5 star) and Luxury (luxurious 5 star accommodation and service).&nbsp;However, you will enjoy our high quality service for every tour category.&nbsp;We are here to&nbsp;ensure a worry and hassle free&nbsp;holiday in our country.</p>

<p><strong>15. Which airlines fly to Vietnam ?</strong></p>

<p>There are many airline companies which fly to Vietnam every day.</p>

<p>From Europe, you can fly with Air France (&nbsp;http://www.airfrance.com&nbsp;), British Airway (&nbsp;http://www.britishairways.com&nbsp;), Lufthansa (&nbsp;http://www.lufthansa.com&nbsp;), Eva air (&nbsp;http://www.evaair.com&nbsp;&nbsp;), Vietnam Airlines (&nbsp;www.vietnamairlines.com&nbsp;)&hellip;</p>

<p>From Asia, Thai Airway (&nbsp;http://www.thaiairways.com&nbsp;) , Japan Airlines (&nbsp;http://www.jal.com/en&nbsp;) , Malaysia Airlines (&nbsp;http://www.malaysiaairlines.com&nbsp;), Singapore Airlines (&nbsp;http://www.singaporeair.com&nbsp;) &hellip; Tiger Airway (&nbsp;www.tigerairways.com&nbsp;), Air Asia (&nbsp;www.Airasia.com&nbsp;), Pacific Airlines (&nbsp;www.pacificairlines.com.vn&nbsp;),</p>

<p>If you are from the USA , Vietnam Airlines (&nbsp;www.vietnamairlines.com&nbsp;) and American Airlines (&nbsp;http://www.aa.com/index_us.jhtml&nbsp;) are the best choice</p>

<p>This website is recommended for searching all flights you want&nbsp;http://www.airline-network.co.uk/index.asp</p>

<p><strong>16. We have our international tickets to and from Vietnam. We have booked a tour package using regional and domestic flights. Could you please advise what will be happening with our internal airline tickets within Vietnam, how will we be given these?</strong></p>

<p>When you receive our service voucher confirmation, all services you have booked with us, including domestic flights, hotels and other logistic services will have been&nbsp; already confirmed. We issue your air tickets in Vietnam for your convenience and to avoid any potential loss. Our tour guide and/or our colleagues will deliver them&nbsp;to you in person or to your hotel in Vietnam. You will enjoy peace of mind as we handle your domestic flights fo ra smooth and hassle free holiday.</p>

<p><strong>17. Why Should I Book Private Tours or Set Itineraries with Scheduled Departure Dates?</strong></p>

<p>While most companies present a number of set itineraries with scheduled departure dates, thus forcing you to fit within their framework, we take a very different approach. At Luxury Travel Co., Ltd, we offer private luxury vacations in Southeast Asia.</p>

<p>We offer private vacation itineraries that are personalized and tailor-made to perfectly fit your interests, preferences, and schedule. Whether you are traveling as a couple, a family, a group of friends, an organization, or some combination thereof, we will put together a trip that fits everyone&rsquo;s desires and departs when you want!</p>

<p>The real beauty of private travel is having flexibility during your vacation. Because you have your own expert guides with private vehicles, you are free to make changes on a whim. There is no bus to catch, no rigid schedule to follow, and no large group with competing demands. This is your vacation, with your own private guides who are there each step of the way to ensure you have the perfect experience.</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'FAQs | Luxury Travel - Best 1000 Tailor-Made Premium Private Tours In Asia',
                'meta_description' => 'Frequently Asked Questions 1. Do I need a visa to visit Vietnam, Cambodia, Laos, Myanmar or Thailand? For more information about the necessary Visas for our destinations, go to the Visa Infor',
                'created_at' => '2019-06-28 09:15:46',
                'updated_at' => '2019-06-28 10:41:57',
                'index' => 1,
            ),
            15 => 
            array (
                'id' => 33,
                'name' => 'Visa to Vietnam',
                'slug' => 'visa-to-vietnam',
                'image' => '{"thumbnail":"\\/uploads\\/images\\/featured-image\\/Vietnam\\/shutterstock_604387040.jpg","banner":"\\/uploads\\/images\\/tours\\/Vietnam\\/hue-ft2.jpg"}',
                'content_short' => NULL,
                'content' => '<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">I. OVERALL KNOWLEDGE ABOUT VIETNAM VISA FOR TOURIST</span>.. 2</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">1. Do you need a Vietnam VISA to enter Vietnam?</span>. 2</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">2. Vietnam VISA exemption for some nations</span>. 2</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">3. Types of Vietnam VISA</span>.. 2</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">4. Vietnam VISA stamping fee</span>. 3</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">II. APPLYING FOR VIETNAM VISA ON ARRIVAL</span>.. 4</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">1. Definition &amp; usage of Vietnam VISA on arrival</span> 4</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">2. The validity of Vietnam VISA on arrival</span> 4</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">3. Requirements and process to get a Vietnam VISA on arrival</span> 4</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">4.&nbsp; Pros &amp; cons of Vietnam VISA on arrival</span> 4</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">III. THE INNOVATION OF APPLYING FOR VIETNAM VISA: EVISA</span>.. 5</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">1. Definition &amp; usage of Vietnam EVISA</span>.. 5</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">2. The validity of Vietnam EVISA</span>.. 7</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">3. Requirements and process to get a Vietnam EVISA for business and travel purposes.</span> 7</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">4. The cost to get Vietnam VISA stamped upon arrival</span> 8</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">5. Pros &amp; cons of Vietnam EVISA</span>.. 8</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">IV. VIETNAM VISA EXTENSION AND RENEWAL</span>.. 8</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">1. Vietnam VISA extension</span>. 8</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">2. Vietnam VISA renewal</span> 8</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif"><span lang="vi" new="" roman="" style="font-family:" times="">V. CONTACTS OF ASSISTANCES</span>. 9</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_bv0p6n2vnrci"></a><b>&nbsp;</b></span></span></span></span></span></h1>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_lmk69rfefq0g"></a><b>&nbsp;</b></span></span></span></span></span></h1>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_gk375290j8wo"></a><b>&nbsp;</b></span></span></span></span></span></h1>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_utmjrglxqu1c"></a><b>&nbsp;</b></span></span></span></span></span></h1>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_q1wf4utdvv5l"></a><b>&nbsp;</b></span></span></span></span></span></h1>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_3j9tskybofck"></a><b>&nbsp;</b></span></span></span></span></span></h1>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_cla8r5mdf3b7"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">I. OVERALL KNOWLEDGE ABOUT VIETNAM VISA FOR TOURIST</span></span></span></b></span></span></span></span></span></h1>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_f4ey9f7uta4a"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">1. Do you need a Vietnam VISA to enter Vietnam?</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Most tourists travelling to Vietnam will require an entry visa, regardless of how long they plan to stay in the country. </span></span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">When you complete VISA application, the Vietnamese agencies abroad will process your VISA application. Your VISA is issued after 2-3 days</span></span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_p8bv5xihbqmb"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">2. Vietnam VISA exemption for some nations</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-left:31px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">Vietnam offers visa exemptions ranging from 14 to 90 days to citizens of 24 countries holding valid ordinary passports.</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span style="color:#484848">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#484848">Vietnam visa free for 14 days: Brunei, Myanmar</span></span></span></span></span></span></span></span></p>

<ul>
<li><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#484848">Visa free approves for 15 days: Sweden ,Denmark, Russian, Norway, UK, Finland, Italy, Japan, South Korea, Spain</span></span></span></span></span></span></span></span></li>
<li><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#484848">Vietnam visa for 21 days and free for: Philippines</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:32px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#484848">Free visa for 30 days: Singapore, Malaysia, Indonesia, Thailand, Cambodia, Laos.</span></span></span></span></span></span></span></span></li>
</ul>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">Note:</span></span></span></span></span></span></span></span></p>

<ul>
<li><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">The exemptions listed above for Sweden, Norway, Denmark, Finland, Japan, South Korea, and Russia are valid until Dec. 31, 2019.</span></span></span></span></span></span></span></span></li>
<li><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">The exemptions listed above for the United Kingdom, France, Germany, Spain, and Italy are valid until June 30, 2021.</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:53px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">Spouses or children of Vietnamese citizens are allowed to stay in the country without a visa for six months and must show papers proving their eligibility</span></span></span></span></span></span></span></span><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_m6i75urqusk7"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">3. Types of Vietnam VISA </span></span></span></b></span></span></span></span></span></li>
</ul>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">According to the new regulations that took effect in January 2015, Vietnam visa is classified into about 20 main types by the purpose of visit, namely DL, DN, NG, DH, LV, HN, PV, VR, TT, SQ, etc.</span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">05 popular types of Vietnam visa are:</span></span></span></span></span></span></p>

<ul>
<li style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Tourist visa (DL)</span></span></span></span></span></span></span></li>
<li><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Business visa (DN &amp; LĐ)</span></span></span></span></span></span></span></li>
<li><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Student/internship visa (DH)</span></span></span></span></span></span></span></li>
<li><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Working visa (LD)</span></span></span></span></span></span></span></li>
<li style="margin-bottom:48px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Diplomatic visa (NG)</span></span></span></span></span></span></span></li>
</ul>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_8qf9gdv3b05n"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">4. Vietnam VISA stamping fee</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">The stamping fee based on how many passports you have, if your children have their passports, you will have to pay their fee too.</span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<table class="Table" style="border-collapse:collapse; border:none" width="440">
<tbody>
<tr style="height:96.0pt">
<td rowspan="2" style="border-bottom:solid #dddddd 1.0pt; width:138.75pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:96.0pt" valign="top" width="139">
<p align="center" style="margin-top:16px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">Visa Types</span></span></span></span></b></span></span></span></p>
</td>
<td colspan="2" style="border-bottom:solid #dddddd 1.0pt; width:190.5pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:96.0pt" valign="top" width="191">
<p align="center" style="margin-top:16px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">Service Fee</span></span></span></span></b></span></span></span></p>

<p align="center" style="margin-top:16px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">Cost/Person (USD)</span></span></span></span></b></span></span></span></p>

<p align="center" style="margin-top:16px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">pay online by credit card</span></span></span></span></b></span></span></span></p>
</td>
<td rowspan="2" style="border-bottom:solid #dddddd 1.0pt; width:111.0pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:96.0pt" valign="top" width="111">
<p align="center" style="margin-top:16px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">Stamp Fee </span></span></span></span></b></span></span></span></p>

<p align="center" style="margin-top:16px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">Cost/Person (USD)</span></span></span></span></b></span></span></span></p>

<p align="center" style="margin-top:16px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">pay in cash at the airport</span></span></span></span></b></span></span></span></p>
</td>
</tr>
<tr style="height:58.0pt">
<td style="border-bottom:solid #dddddd 1.0pt; width:86.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="86">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">Normal Processing</span></span></span></span></b></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:104.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="104">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">Urgent (1 working day)</span></span></span></span></b></span></span></span></p>
</td>
</tr>
<tr style="height:58.0pt">
<td style="border-bottom:solid #dddddd 1.0pt; width:138.75pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="139">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">1 month single entry (30 days)</span></span></span></span></b></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:86.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="86">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 15</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:104.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="104">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 25</span></span></span></span></span></span></span></p>
</td>
<td rowspan="2" style="border-bottom:solid #dddddd 1.0pt; width:111.0pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="111">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 25</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:58.0pt">
<td style="border-bottom:solid #dddddd 1.0pt; width:138.75pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="139">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">3 month single entry (87 days)</span></span></span></span></b></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:86.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="86">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 110</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:104.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="104">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 120</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:58.0pt">
<td style="border-bottom:solid #dddddd 1.0pt; width:138.75pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="139">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">1 month multiple entry (30 days)</span></span></span></span></b></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:86.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="86">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 25</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:104.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="104">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 35</span></span></span></span></span></span></span></p>
</td>
<td rowspan="2" style="border-bottom:solid #dddddd 1.0pt; width:111.0pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="111">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 50</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:58.0pt">
<td style="border-bottom:solid #dddddd 1.0pt; width:138.75pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="139">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">3 month multiple entry (87 days)</span></span></span></span></b></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:86.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="86">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 110</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #dddddd 1.0pt; width:104.25pt; border-top:none; border-left:none; border-right:solid #dddddd 1.0pt; padding:9.0pt 9.0pt 9.0pt 9.0pt; height:58.0pt" valign="top" width="104">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#282828">USD 120</span></span></span></span></span></span></span></p>
</td>
</tr>
</tbody>
</table>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_tm24byg8c26f"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">II. APPLYING FOR VIETNAM VISA ON ARRIVAL</span></span></span></b></span></span></span></span></span></h1>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_o3q2qx7q7x61"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">1. Definition &amp; usage of Vietnam VISA on arrival</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Vietnam visa on arrival is an alternative way to get a Vietnam Visa. After finishing the online application form and paying the process fee, you will rereive your visa approval letter within one hour to two days.It depends on the status of visa processing you have selected. Then, picking your visa approval letter at each of three Vietnam&rsquo;s International airpot in Hanoi, Ho Chi Minh City, Da Nang.</span></span></span></span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_s53vw5idcabi"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">2. The validity of Vietnam VISA on arrival</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">You can issue any type of VISA which you need with its maximum validity</span></span></span></span></span>. Vietnam visa approval letter takes <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">valid from the granted arrival date, not from the date you apply for it or the date it is issued</span></span></span></span>. <span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Vietnamese tourist visa&rsquo;s validity can be from 15 days to 3 months.</span></span></span></span></span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_uqtlh2usl8rx"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">3. Requirements and process to get a Vietnam VISA on arrival</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Step 1: Apply for an approval letter online </span></span></span></span></span></span></span></p>

<p style="margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Step 2: Pay the visa fee online </span></span></span></span></span></span></span></p>

<p style="margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Step 3: Print your Approval letter and prepare your 4x6cm photo with a white background and no glasses</span></span></span></span></span></span></span></p>

<p style="margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Step 4: Bring your approval letter, photos on your trip to Vietnam </span></span></span></span></span></span></span></p>

<p style="margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Step 5: Show your visa approval letter and photo at the visa on arrival in Vietnam airpot</span></span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_pf0h1sg6eaed"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">4.&nbsp; Pros &amp; cons of Vietnam VISA on arrival</span></span></span></b></span></span></span></span></span></h2>

<table class="Table" style="border-collapse:collapse; border:solid black 1.0pt" width="440">
<tbody>
<tr style="height:36.0pt">
<td style="border:solid black 1.0pt; width:198.0pt; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:36.0pt" valign="top" width="198">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Pros</span></span></span></span></span></span></p>
</td>
<td style="border:solid black 1.0pt; width:242.25pt; border-left:none; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:36.0pt" valign="top" width="242">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Cons</span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:78.0pt">
<td style="border:solid black 1.0pt; width:198.0pt; border-top:none; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:78.0pt" valign="top" width="198">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Visa on arrival will be obtained right at the arrival airpot and the process of having it straightforward and convienient.</span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid black 1.0pt; width:242.25pt; border-top:none; border-left:none; border-right:solid black 1.0pt; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:78.0pt" valign="top" width="242">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Vietnam on arrival is appicable for movin by airplane</span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:81.0pt">
<td style="border:solid black 1.0pt; width:198.0pt; border-top:none; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:81.0pt" valign="top" width="198">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Do not need to go to embassy to complete complicated procedures.</span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid black 1.0pt; width:242.25pt; border-top:none; border-left:none; border-right:solid black 1.0pt; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:81.0pt" valign="top" width="242">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">To get a Vietnam visa arrival, you need someone in Vietnam to guarantee your personal information as well as for purposes and staying duration</span></span></span></span></span></span></p>
</td>
</tr>
</tbody>
</table>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_uu8vw8y5g96a"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">III. THE INNOVATION OF APPLYING FOR VIETNAM VISA: EVISA</span></span></span></b></span></span></span></span></span></h1>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_elieckosed4"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">1. Definition &amp; usage of Vietnam EVISA</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Since February 1<sup>st</sup> 2017, the Government of Vietnam has inplemented E-visa. <span style="background:white"><span style="color:#333333">The e-Visa is an electronic travel authorization from the Vietnam Government for up to 30 consecutive days, issued in PDF format. It is the equivalent to a visa, but no stamp or label is placed in the passport.</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:27px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:200%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:200%"><span style="color:#333333">List of Vietnamese ports which allow foreigners to enter or exit by EVISA</span></span></span></b></span></span></span></span></p>

<table class="Table" style="border-collapse:collapse; border:solid #ededed 1.0pt" width="227">
<tbody>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; background:white; width:33.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span style="color:#333333">No.</span></span></span></b></span></span></span></p>
</td>
<td style="border:solid #ededed 1.0pt; background:white; width:193.5pt; border-left:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span style="color:#333333">Port name</span></span></span></b></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td colspan="2" style="border:solid #ededed 1.0pt; background:white; width:226.5pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="227">
<p style="margin-top:16px; margin-bottom:21px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span style="color:#333333">International Airports</span></span></span></b></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">1</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Cat Bi International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">2</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Cam Ranh International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">3</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Can Tho International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">4</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Da Nang International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">5</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Noi Bai International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">6</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Phu Bai International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">7</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Phu Quoc International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">8</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Tan Son Nhat International Airport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td colspan="2" style="border:solid #ededed 1.0pt; width:226.5pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="227">
<p style="margin-top:16px; margin-bottom:21px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Land Border Gates</span></span></span></span></b></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">9</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Bo Y Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">10</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Cha Lo Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">11</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Cau Treo Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">12</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Huu Nghi Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">13</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Ha Tien Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">14</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Lao Bao Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">15</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Lao Cai Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">16</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Moc Bai Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">17</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Mong Cai Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">18</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Nam Can Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">19</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Song Tien Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">20</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Tinh Bien Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">21</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Xa Mat Landport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td colspan="2" style="border:solid #ededed 1.0pt; width:226.5pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="227">
<p style="margin-top:16px; margin-bottom:21px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Seaports</span></span></span></span></b></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">22</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Da Nang Seaport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">23</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Hon Gai Seaport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">24</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Hai Phong Seaport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">25</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Nha Trang Seaport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">26</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Quy Nhon Seaport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">27</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Ho Chi Minh City Seaport</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:47.0pt">
<td style="border:solid #ededed 1.0pt; width:33.0pt; border-top:none; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="33">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">28</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid #ededed 1.0pt; width:193.5pt; border-top:none; border-left:none; border-right:solid #ededed 1.0pt; padding:2.0pt 6.0pt 2.0pt 6.0pt; height:47.0pt" valign="top" width="194">
<p align="center" style="margin-top:16px; margin-bottom:21px; text-align:center"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span style="color:#333333">Vung Tau Seaport</span></span></span></span></span></span></span></p>
</td>
</tr>
</tbody>
</table>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_b2tirr1hhiq"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">2. The validity of Vietnam EVISA</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">An e-Visa is single entry and valid for a maximum of 30 days.</span></span></span></span></span></span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_d93e5xaln54b"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">3. Requirements and process to get a Vietnam EVISA for business and travel purposes.</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">After filling the online form for e-visa provided Vietnam Immigration Department</span></span></span></span></span>, you are asked for electronic payment information. It costs US$ 25 (or VND 550,000) as the EVISA service fee. If your request is refused, the fee won&rsquo;t be refunded.</span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="color:#333333">Note: </span><span lang="vi" style="color:#252525">Vietnam e-Visa Requirements:</span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:10.0pt"><span style="line-height:115%"><span style="color:#252525">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">A passport with six (6) months&rsquo; validity</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:10.0pt"><span style="line-height:115%"><span style="color:#252525">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">A photo in .jpg format of your full passport data page</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:10.0pt"><span style="line-height:115%"><span style="color:#252525">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">A passport photo in .jpg format (4x6, white background, without glasses)</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:10.0pt"><span style="line-height:115%"><span style="color:#252525">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">A valid international credit or debit card</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:10.0pt"><span style="line-height:115%"><span style="color:#252525">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">An email address for support purposes</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:10.0pt"><span style="line-height:115%"><span style="color:#252525">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">A temporary address within Vietnam</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-left:24px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:10.0pt"><span style="line-height:115%"><span style="color:#252525">&middot;</span></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#252525">Your entry and exit dates and entry and exit points/airports</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_7oohmpadyjr7"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">4. The cost to get Vietnam VISA stamped upon arrival</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Payment method is required through electronic payment gateways like Visa or Mastercard. The service fee is prescribed as US$ 25 (or VND 550,000) in the official website of Vietnam Immigration Department for applying EVISA.</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_mnwbzv2mscw9"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">5. Pros &amp; cons of Vietnam EVISA</span></span></span></b></span></span></span></span></span></h2>

<table class="Table" style="border-collapse:collapse; border:solid black 1.0pt" width="440">
<tbody>
<tr style="height:36.0pt">
<td style="border:solid black 1.0pt; width:213.0pt; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:36.0pt" valign="top" width="213">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Pros</span></span></span></span></span></span></p>
</td>
<td style="border:solid black 1.0pt; width:227.25pt; border-left:none; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:36.0pt" valign="top" width="227">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Cons</span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:51.0pt">
<td style="border:solid black 1.0pt; width:213.0pt; border-top:none; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:51.0pt" valign="top" width="213">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Vietnam E-visa is the convenient and simple process of applying</span></span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid black 1.0pt; width:227.25pt; border-top:none; border-left:none; border-right:solid black 1.0pt; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:51.0pt" valign="top" width="227">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">you can&rsquo;t issue a VISA with longer validity than 90 days and it is single-entry only</span></span></span></span></span></span></span></span></p>
</td>
</tr>
<tr style="height:81.0pt">
<td style="border:solid black 1.0pt; width:213.0pt; border-top:none; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:81.0pt" valign="top" width="213">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">No excess expenditure on travel cost and required documents, applicants for Vietnam E-visa only have to pay US$25 for one time visa</span></span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid black 1.0pt; width:227.25pt; border-top:none; border-left:none; border-right:solid black 1.0pt; padding:5.0pt 5.0pt 5.0pt 5.0pt; height:81.0pt" valign="top" width="227">
<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="background:white"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">As of February 2019 the pilot Evisa for Vietnam program only grants citizens from 80 countries the right to obtain Vietnamese visa in advance</span></span></span></span></span></span></span></span></p>
</td>
</tr>
</tbody>
</table>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_3jbnppbtwttg"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">IV. VIETNAM VISA EXTENSION AND RENEWAL</span></span></span></b></span></span></span></span></span></h1>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_l61zu9cg1kc9"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">1. Vietnam VISA extension</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">It is possible to extend your Vietnam visa for temporary staying permission in Vietnam with purpose for travelling, studying, business and others. <span style="background:white">Extending Vietnam visa<span style="color:#333333"> must be applied some days before your visa expired date. Your current visa has to be correspondent to your staying extending requirement. For example: if you apply for a 3-month tourist visa, you can extend it for another 3 months</span></span></span></span></span></span></span></span></p>

<h2 style="margin-bottom:5px; margin-top:24px"><span style="font-size:16pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_mjnpqekzb6ip"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">2. Vietnam VISA renewal</span></span></span></b></span></span></span></span></span></h2>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif">&nbsp;</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">Visa renewal is good for those who wish to get a new visa without leaving Vietnam. The staying length is valid maximun in 3 months. More over, renewal visa fee is higher than extension visa because you have to pay stamping fee and visa sticker fee.</span></span></span></span></span></span></p>

<h1 style="margin-top:32px; margin-bottom:8px"><span style="font-size:20pt"><span style="line-height:150%"><span style="page-break-after:auto"><span style="font-family:Arial,sans-serif"><span style="font-weight:normal"><a name="_rbw1hxj7jm2c"></a><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:150%"><span new="" roman="" style="font-family:" times="">V. CONTACTS OF ASSISTANCES </span></span></span></b></span></span></span></span></span></h1>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">&nbsp;Vietnam VISA extend authorities</span></span></span></span></span></span></p>

<p style="margin-top:16px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#111111">Vietnam Consular Department in Hanoi</span></span></span></span></b></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Address:</span></span></span></span></b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333"> No. 40 Tran Phu, Dien Ban, Ba Dinh, Hanoi</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Hours of operation</span></span></span></span></b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">: Embassy: Mon &ndash; Fri, 8:30 AM to 4:00 PM</span></span></span></span></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Tel</b>: (+84) 243 799 3388</span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Fax: (</b>+84) 243 823 6928 | (+84) 243 799 3105</span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Website:</b><a href="https://lanhsuvietnam.gov.vn/"><span style="color:#006600"> Vietnam Consular Department in Hanoi</span></a></span></span></span></span></p>

<p style="margin-top:16px"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Email</b>: cls.mfa@mofa.gov.vn</span></span></span></span></p>

<p style="margin-top:16px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="color:#111111">Department of External Relations in Ho Chi Minh City</span></b></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Address:</span></span></span></span></b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333"> No. 6 Alexandre De Rhodes, Ben Nghe Ward, 1 District, Ho Chi Minh City</span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Hours of operation</span></span></span></span></b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">: Mon &ndash; Fri, 7:45 AM to 11:15 AM &amp; 1:10 PM to 4:00 PM</span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Tel</b>: (+84) 283 822 4224 | (+84) 283 822 3055</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Fax: </b>(+84) 283 825 1436 | (+84) 283 829 7785</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Website:</b><a href="http://www.mofahcm.gov.vn/"><span style="color:#006600"> Department of External Relations in Ho Chi Minh City</span></a></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Email</b>: snv@hcm.vnn.vn</span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times="">4. Emergency telephone numbers within Vietnam</span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Police</span></span></span></span></b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">: 113</span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">Fire brigade</span></span></span></span></b><span lang="vi" style="font-size:12.0pt"><span style="line-height:115%"><span new="" roman="" style="font-family:" times=""><span style="color:#333333">: 114</span></span></span></span></span></span></span></p>

<p style="margin-top:16px; margin-bottom:16px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Arial,sans-serif"><b>Ambulance</b>: 115</span></span></span></p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'Vietnam Visa | Luxury Travel | How to apply for Vietnam e-Visa',
                'meta_description' => 'Book any Vietnam holiday package with Luxury Travel Vietnam and get your FREE tourist visa from us. Visa on Arrival to eVisa, Visa Exemption, Visa Validity, get all information you need to kn',
                'created_at' => '2019-06-28 09:28:50',
                'updated_at' => '2019-10-21 11:15:15',
                'index' => 1,
            ),
            16 => 
            array (
                'id' => 34,
                'name' => 'Visa to Cambodia',
                'slug' => 'visa-to-cambodia',
                'image' => '{"thumbnail":"\\/uploads\\/images\\/featured-image\\/cambodia\\/bayon-Cambodia.jpg","banner":"\\/uploads\\/images\\/tours\\/Cambodia\\/Cambodia-Siem_Reap-Angkor_at_sunrise.jpg"}',
                'content_short' => NULL,
                'content' => '<p><strong>Cambodia Visa</strong></p>

<p>All visitors, except citizens of Indonesia, Malaysia, Singapore, Philippines, Laos, Thailand and Vietnam, need a visa to enter Cambodia. The official price for a tourist visa is US$20 and US$25 for an ordinary visa. Staff may try to charge more at some land border crossings, so hold out for the official price, particularly at major crossings, but don&rsquo;t be upset if you have to pay 1 or 2 dollars extra.</p>

<p>Visas can be obtained at Cambodian embassies or consulates. Visas are also available &ldquo;on arrival&rdquo; at international airports, all six international border crossings with Thailand, some international border crossings with Vietnam, and at the main border crossing with Laos.</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'Cambodia Visa | Luxury Travel | How to apply for Cambodia e-Visa',
                'meta_description' => 'Get Visa on Arrival or Apply for your Online Visa and get it on your email within 3 days. Learn all you need to know about Cambodia Tourist Visa, Visa Validity, Visa Exemption, Visa Fees at l',
                'created_at' => '2019-06-28 09:29:50',
                'updated_at' => '2019-10-21 11:19:19',
                'index' => 1,
            ),
            17 => 
            array (
                'id' => 35,
                'name' => 'Visa to Laos',
                'slug' => 'visa-to-laos',
                'image' => '{"thumbnail":"/uploads\\/images\\/Laos\\/laos.jpg","banner":"uploads\\/images\\/tours\\/Laos\\/Villag-and-bungalows-along-Nam-song-River-in-Vang-Vieng.jpg"}',
                'content_short' => NULL,
                'content' => '<p>Unless&nbsp;you&nbsp;hold a passport from Japan or one of the ASEAN member states,&nbsp;you&rsquo;llneed a visa to enter Laos. The good news is that&nbsp;you&nbsp;probably won&rsquo;t&nbsp;need&nbsp;to arrange it in advance; thirty-day&nbsp;visas&nbsp;are now available on arrival at most international borders.</p>

<p><strong>Visas</strong>&nbsp;on arrival take just a few minutes to process, cost around $35, and are available to passengers flying into Luang Prabang Airport, Pakse Airport and Wattay Airport in Vientiane. It is also possible to extend your visa for another 30 days at a cost of about USD $2 per day. Otherwise, you will be charged USD $10 per day by border authorities when you leave Laos.</p>

<p>Travelers will all need a passport valid for at least 6 months after they depart.</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'Laos Visa | Luxury Travel | How to apply for Laos Visa',
                'meta_description' => 'Get Tourist Visa on Arrival at Laos International Airports and Border Crossings easily. Learn all you need to know about Laos Visa- how to apply, visa fees and validity at luxurytravelvietnam',
                'created_at' => '2019-06-28 09:33:48',
                'updated_at' => '2019-06-28 09:36:42',
                'index' => 1,
            ),
            18 => 
            array (
                'id' => 36,
                'name' => 'Visa to Myanmar',
                'slug' => 'visa-to-myanmar',
                'image' => '{"thumbnail":"/uploads\\/images\\/featured-image\\/Myanmar\\/myanmar.jpg","banner":"uploads\\/images\\/tours\\/Myanmar\\/myanmar-yagon-39.jpg"}',
                'content_short' => NULL,
                'content' => '<p><strong>Myanmar E-Visa</strong></p>

<p>In addition to getting visas at embassies and consulates, Myanmar has an online&nbsp;e-visa&nbsp;system. It is designed to make the process of application simpler for visitors &ndash; particularly those from countries that do not have a Myanmar embassy.</p>

<p>With this new service, you can apply for a regular Tourist Visa using expedited application processing, 365 days a year, with results e-mailed within 24 hours. The cost of this service is $56, and is available to all applicants currently eligible for a regular Tourist Visa.</p>

<p>For e-visa applications, go to the&nbsp;<a href="http://evisa.moip.gov.mm/">official Ministry of Immigration e-visa website</a>. Citizens of 100 countries are eligible for Myanmar e-visas; to see the full list, go&nbsp;<a href="http://evisa.moip.gov.mm/NoticetoTourists.aspx">here</a>. For a list of Myanmar embassies and consulates around the world, go&nbsp;<a href="https://www.go-myanmar.com/embassies-and-consulates-myanmar-foreign">here</a>.</p>

<p>E-visas are currently only valid for entry at Myanmar&rsquo;s three main&nbsp;<a href="https://www.go-myanmar.com/getting-to-myanmar"><strong>international airports</strong></a>:&nbsp;<a href="https://www.go-myanmar.com/getting-to-yangon"><strong>Yangon</strong></a>,&nbsp;<a href="https://www.go-myanmar.com/getting-to-mandalay"><strong>Mandalay</strong></a>&nbsp;and&nbsp;<a href="https://www.go-myanmar.com/getting-to-nay-pyi-taw"><strong>Nay Pyi Taw</strong></a>&nbsp;and<strong>&nbsp;</strong><a href="https://www.go-myanmar.com/arriving-and-departing-over-land"><strong>overland travel to Myanmar</strong></a><strong>&nbsp;</strong>via the Thai-Myanmar border points of Mae Sai/Tachileik, Mae Sot/Myawaddy and Ranong/Kawthaung only (e-visa is not valid for China, India or other Thai entry points).</p>

<p>Due to the sensitive political situation in some border regions,&nbsp;<strong>entry and exit via land borders is restricted</strong>; there are four Thailand border points that do not need permission for crossings, but China and India require special permits. See&nbsp;<a href="https://www.go-myanmar.com/arriving-and-departing-over-land"><strong>arriving and departing over land</strong></a>&nbsp;for more details.</p>

<p><strong>Myanmar Visa</strong></p>

<p><strong>Tourist visas are valid for 28 days</strong>, which can be extended by a further 14 days at a cost of&nbsp;<strong>US$3 per day&nbsp;</strong>(plus a one-off $3 administration fee). It is important to be ready with the&nbsp;<strong>correct change</strong>&nbsp;at your departure point. Bear in mind that if you overstay, you might encounter some difficulties booking travel and/or hotels in your overstay period, as not all service staff are aware of the overstay allowance.</p>

<p><strong>Do not apply for your visa too early</strong>: the period for visa validity (i.e. your Myanmar entry date) may range from one to three months.</p>

<p><strong>Do not apply for your visa too late</strong>: if you are applying for a paper visa, embassies and consulates can take anything from one day to more than two weeks to process your application. If you are from a country that does not have a Myanmar embassy or consulate and you are unable to apply for e-visa, it will take longer still.</p>

<p>You must have a passport that is valid for at least&nbsp;six months&nbsp;after your departure from Myanmar.</p>

<p><strong>A single entry tourist visa will typically cost around US$50</strong>. Depending on which country you are applying from, the application procedures may vary, but it is usually a simple process. Go<strong>&nbsp;</strong><a href="https://www.go-myanmar.com/embassies-and-consulates-myanmar-foreign"><strong>here</strong></a>&nbsp;to find your local embassy or consulate.</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'Myanmar Visa | Luxury Travel | How to apply for Myanmar e-Visa',
                'meta_description' => 'Get Myanmar Visa at the Embassy or Apply for eVisa. Learn all you need to know about Myanmar Visa, Tourist Visa, Validity of Visa, Visa Exemption, Visa Fees at luxurytravelvietnam.com. Travel',
                'created_at' => '2019-06-28 09:37:41',
                'updated_at' => '2019-06-28 09:38:17',
                'index' => 1,
            ),
            19 => 
            array (
                'id' => 37,
                'name' => 'Visa to thailand',
                'slug' => 'visa-to-thailand',
                'image' => '{"thumbnail":"\\/uploads\\/images\\/featured-image\\/thailand\\/chiang-Mai.jpg","banner":"\\/uploads\\/images\\/tours\\/Thailand\\/air-balloon-thailand.jpg"}',
                'content_short' => NULL,
                'content' => '<ol style="list-style-type:upper-roman">
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><a name="_Toc21636001"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Knowledge</span></span></span></span></b></a><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"> overview</span></span></span></span></b></span></span></span></li>
</ol>

<p style="margin-left:72px; text-align:justify">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px; margin-left:48px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><a name="_Toc21636002"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">I.1. The definition</span></span></span></span></b></a><a href="#_ftn1" name="_ftnref1" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span class="MsoFootnoteReference" style="vertical-align:super"><b><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">[1]</span></span></span></span></b></span></span></span></span></span></b></span></a><b> </b></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The passport is a type of document which is used to verify the identification and nationality of a person when coming to another country.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The visa is a legal proof that is placed in the passport aiming to affirm that the official visa owner is allowed to enter, leave or stay in a country in a specific period of time.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px; margin-left:48px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><a name="_Toc21636003"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">I.2. What is the different between visa and passport?</span></span></span></span></b></a></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Passport is the document that is issued by a country&#39;s competent authority to its citizens. Whereas, the visa is a document granted by another country where the visa applicant wants to come.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Crucial notes: You need to make a passport first, apply for a visa afterward. If you do not have a passport, you cannot apply for a visa successfully.</span></span></span></span></span></span></span></p>

<ol start="2" style="list-style-type:upper-roman">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><a name="_Toc21636004"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Visa in Thailand</span></span></span></span></b></a></span></span></span></li>
</ol>

<h2 style="margin-top:13px"><span style="font-size:13pt"><span style="line-height:115%"><span style="font-family:Cambria,serif"><span style="color:#4f81bd"><a name="_Toc21636005"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">II.1. The visa policy of Thailand</span></span></a></span></span></span></span></h2>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><a name="_Toc21636006"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal">II.1.1. The typical milestones in Thailand visa policy</span></span></span></a><a href="#_ftn2" name="_ftnref2" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">[2]</span></span></span></span></span></span></span></span></span></a></span></span></h3>

<p style="text-align:justify; text-indent:18.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">In May 2014, the foreigners want to re-enter Thailand after visa-free or visa on arrival<a href="#_ftn3" name="_ftnref3" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">[3]</span></span></span></span></span></span></a> period has expired, they must apply for a visa in advance, or stay outside Thailand for at least one night. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:18.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">In August 2014, the Immigration Police got command from the Thai Prime Minister in enforcing flexibly visa-related laws due to the application of strict regulations affected schools and the tourism industry</span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:18.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">On October 31, 2018, Thailand announced that, since the end of November 2018, overstay visas will no longer be allowed to appear in the country.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:18.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">On 20 November 2018, Thailand&#39;s Cabinet officially approved numerous new visa-free and visa on arrival countries. In which, new visa-free countries (30 days) include Latvia, Lithuania, Andorra, San Marino, Ukraine, Maldives, Mauritius. Visa on arrival countries (15 days) are Mexico, Vanuatu, Nauru, Georgia. </span></span></span></span></span></span></span></p>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><a name="_Toc21636007"><span style="font-size:13.0pt"><span style="color:black">II.1.2. Do you need to apply for a visa in Thailand?</span></span></a><a href="#_ftn4" name="_ftnref4" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:13.0pt"><span style="color:black"><span class="MsoFootnoteReference" style="vertical-align:super"><b><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">[4]</span></span></span></span></b></span></span></span></span></a></span></span></h3>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">In general, a foreigner who wants to enter Thailand is required to apply a Visa from Royal Thailand Embassy/Royal Thailand Consulate. Nevertheless, Thailand also put out each certain visa policy for each country, whereby passport holder could be not required a visa if they meet visa exemption requirements. Regardless of your purpose of coming to Thailand, you need to be aware of the following cases in applying for a visa. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Let&#39;s read the information carefully and find out where your case is in order to apply successfully for a visa<a href="#_ftn5" name="_ftnref5" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">[5]</span></span></span></span></span></span></a>!</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">II.1.2.1 Visa exemption</span></span></span></span></span></span></span></p>

<ol style="list-style-type:lower-alpha">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Visa exemption for normal passport</span></span></span></span></span></span></span></li>
</ol>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">90 days: Citizens from 5 countries including Argentina, South Korea, Peru, Brazil and Chile where the government signed the bilateral visa waiver agreements with Thailand will be allowed to travel to Thailand for 90 days without a visa. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">30 days (1): Citizens who hold a normal passport and come from 6 countries consist of Hong Kong, Laos, Macao, Mongolia, Vietnam and Russia where the government contracted the bilateral agreement on visa waiver will be allowed to stay at Thailand during 30 days. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">30 days (2): People who hold a normal passport from 51 following countries will be issued visa-free travel to Thailand for up to 30 days. </span></span></span></span></span></span></span></p>

<p align="center" style="text-align:center; margin-bottom:13px"><img height="343" src="/uploads/files/image-20191021111950-1.png" width="685" /></p>

<p align="center" class="MsoCaption" style="text-align:center; margin-bottom:13px"><span style="font-size:9pt"><span style="font-family:Calibri,sans-serif"><span style="color:#4f81bd"><span style="font-weight:bold"><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span style="font-weight:normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Table 1: 51 countries will be issued visa-free travel to Thailand for up to 30 days &ndash; Source: Wikipedia</span></span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:18.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">14 days: According to bilateral agreements, citizens who hold normal passports from two countries Myanmar and Cambodia can stay in Thailand without a visa for a maximum of 14 days.</span></span></span></span></span></span></span></p>

<ol start="2" style="list-style-type:lower-alpha">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Visa exemption for diplomatic or service category passports</span></span></span></span></span></span></span></li>
</ol>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">People who hold diplomatic or service passports granted by the 46 following government will be allowed to visit Thailand for trips last for up to 90 days. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Except for the three following special cases: </span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The duration is 30 days for 17 countries, including Brunei, Cambodia, Canada, China, Ecuador, Hong Kong, Indonesia, Kazakhstan, Laos, Macau, Mongolia, Myanmar, Oman, Singapore, United Kingdom, United States, and Vietnam.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The duration is 90 days only apply for the diplomatic passport with four countries embrace Bangladesh, Estonia, France, and Spain. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Pakistan: The duration is 30 days and only apply for the diplomatic passport</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">II.1.2.2. Visa on arrival </span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Citizens who own a normal passport from 18 following countries can apply for a visa on arrival which allows them to stay up to 15 days at the main entry point whether by air or by land.</span></span></span></span></span></span></span></p>

<p align="center" style="text-align:center; margin-bottom:13px"><img height="171" src="/uploads/files/image-20191021111950-2.png" width="441" /></p>

<p align="center" class="MsoCaption" style="text-align:center; margin-bottom:13px"><span style="font-size:9pt"><span style="font-family:Calibri,sans-serif"><span style="color:#4f81bd"><span style="font-weight:bold"><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span style="font-weight:normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Table 2: 18 countries will be allowed to stay in Thailand for up to 15 days - Source: Wikipedia</span></span></span></span></span></span></span></span></p>

<p align="center" style="text-align:center; margin-bottom:13px">&nbsp;</p>

<h2 style="margin-top:13px"><span style="font-size:13pt"><span style="line-height:115%"><span style="font-family:Cambria,serif"><span style="color:#4f81bd"><a name="_Toc21636008"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">II.2. Visa types in Thailand</span></span></a> </span></span></span></span></h2>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Today, there are key visas in Thailand. Specifically:</span></span></span></span></span></span></span></p>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&nbsp;<a name="_Toc21636009"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal">II.2.1. Courtesy visa</span></span></span></a></span></span></h3>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">This kind of visa is issued by the Royal Thai Embassy under the official request to diplomats/UN officials/leaders who visit Thailand officially for a national duty or special mission.&nbsp; </span></span></span></span></span></span></span></p>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><a name="_Toc21636010"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal">II.2.2. Tourist visa</span></span></span></a></span></span></h3>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Tourist visa in Thailand is a verify evidence which used for tourism purpose, it is usually stamped on the passport when you arrive at the airport or applied for in your country, at the Thai Embassy or Consulate.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Notes: The citizens of some countries can extend the visa duration owing to their government signed bilateral visa agreements with the Thai government. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">&nbsp;(The detail information will be analyzed in section III)</span></span></span></span></span></span></span></p>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><a name="_Toc21636011"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal">II.2.3. Permanent resident visa</span></span></span></a></span></span></h3>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">In this case, in order to qualify for this kind of visa, you must have stayed in Thailand for 3 continuously years with a one-year visa extension. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">If you married with a Thai partner, you need to prove that your monthly income is the minimum of 30 000 Thai Baht. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">If you are single, your monthly income is at least 80 000 Thai Baht.</span></span></span></span></span></span></span></p>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><a name="_Toc21636012"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal">II.2.4. Non-Immigration visa</span></span></span></a></span></span></h3>

<ol style="list-style-type:lower-alpha">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Business visa (or known as type B)</span></span></span></span></span></span></span></li>
</ol>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">This visa divides into three small types:</span></span></span></span></span></span></span></p>

<ul>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">When you work for a company/corporation in Thailand</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">When you become a teacher/trainer at a private school in Thailand</span></span></span></span></span></span></span></li>
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">When you come to Thailand to attend business meetings/conferences/summits or looking for business chances.</span></span></span></span></span></span></span></li>
</ul>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">If you want to apply for a visa for one of the three above cases, you need to contact the Thai Embassy at your local and the business that you want to be recruited at.</span></span></span></span></span></span></span></p>

<ol start="2" style="list-style-type:lower-alpha">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Official visa </span></span></span></span></span></span></span></li>
</ol>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">This visa includes two main types: </span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Type D : Diplomatic visa</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Type M: Applying for staffs who come to Thailand to work as a film producer, journalist or reporter with permission from Ministry of Foreign Affair (usually in a short term)</span></span></span></span></span></span></span></p>

<ol start="3" style="list-style-type:lower-alpha">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Education/work visa</span></span></span></span></span></span></span></li>
</ol>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">This type of visa applied for these following subjects:</span></span></span></span></span></span></span></p>

<table class="MsoTableGrid" style="width:562.5pt; margin-left:-53px; border-collapse:collapse; border:solid windowtext 1.0pt" width="563">
<tbody>
<tr>
<td style="border:solid windowtext 1.0pt; width:31.5pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="32">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">No</span></span></span></span></span></span></span></p>
</td>
<td style="border:solid windowtext 1.0pt; width:184.5pt; border-left:none; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="185">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Subjects</span></span></span></span></span></span></span></p>
</td>
<td style="border:solid windowtext 1.0pt; width:243.0pt; border-left:none; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="243">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Required documents that need to submit at the embassy in your country</span></span></span></span></span></span></span></p>
</td>
<td style="border:solid windowtext 1.0pt; width:103.5pt; border-left:none; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="104">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Notes</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr>
<td style="border:solid windowtext 1.0pt; width:31.5pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="32">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">1</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid windowtext 1.0pt; width:184.5pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="185">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">People who seek an internship job or attend a training course in a specific sector of an organization (held not by an official school) or apply for a work permit.</span></span></span></span></span></span></span></p>

<p style="text-align:justify">&nbsp;</p>
</td>
<td style="border-bottom:solid windowtext 1.0pt; width:243.0pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="243">
<ul>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Passport valid at least 6 months.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Filled out an application form.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">2 portrait pictures not older than 6 months.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Confirmation letter from your school/organization.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Copy of business license of the employer.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">FormWP3, which has to be provided by the employer.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Confirmation of the working contract.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Copy of company profile.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Copy of location description of the company.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">List of shareholders of the company in Thailand.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Por Ngor Dor 50 und Por Ngor Dor 30 of the last year, the employer has to provide.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Por Por 20, the employer has to provide.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Plane ticket.</span></span></span></span></span></span></span></li>
</ul>
</td>
<td rowspan="3" style="border-bottom:solid windowtext 1.0pt; width:103.5pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="104">
<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify">&nbsp;</p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The Embassy has the right to require any other necessary documents and reserves the right to refuse application forms that do not qualify.</span></span></span></span></span></span></span></p>
</td>
</tr>
<tr>
<td style="border:solid windowtext 1.0pt; width:31.5pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="32">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">2</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid windowtext 1.0pt; width:184.5pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="185">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">People who take part in an educational course or an intern program which is organized by the Thailand government or international organizations that headquarter in Thailand. </span></span></span></span></span></span></span></p>

<p style="text-align:justify">&nbsp;</p>
</td>
<td style="border-bottom:solid windowtext 1.0pt; width:243.0pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="243">
<ul>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Passport valid at least 6 months.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Filled out an application form.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">2 portrait pictures not older than 6 months.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Confirmed flight ticket.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Confirmation letter from the school.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Confirmation letter from the organization</span></span></span></span></span></span></span></li>
</ul>
</td>
</tr>
<tr>
<td style="border:solid windowtext 1.0pt; width:31.5pt; border-top:none; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="32">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">3</span></span></span></span></span></span></span></p>
</td>
<td style="border-bottom:solid windowtext 1.0pt; width:184.5pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="185">
<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">.</span></span></span></span></span></span></span></p>

<p style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">People who participate in volunteer programs or NGOs organization</span></span></span></span></span></span></span></p>

<p style="text-align:justify">&nbsp;</p>
</td>
<td style="border-bottom:solid windowtext 1.0pt; width:243.0pt; border-top:none; border-left:none; border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt" valign="top" width="243">
<ul>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Passport valid at least 6 months.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Filled out an application form.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">2 portrait pictures not older than 6 months.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Confirmed flight ticket.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Confirmation of the NGO or charity mentioning the duration of the stay and the nature of the activity.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Copy of the passport of the organization&rsquo;s director.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">For a Thai organization, a copy of the organization registration form.</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">For a foreign organization, a copy of the approval letter to operate in Thailand.</span></span></span></span></span></span></span></li>
</ul>
</td>
</tr>
</tbody>
</table>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<ol start="4" style="list-style-type:lower-alpha">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Retirement visa</span></span></span></span></span></span></span></li>
</ol>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Retirement visa is known as O-A visa type which is applied for foreigners over 50 years old who want to coming to Thailand to live or enjoy life</span></span></span></span></span></span></span></p>

<ol start="5" style="list-style-type:lower-alpha">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Marriage visa</span></span></span></span></span></span></span></li>
</ol>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">With this type of visa, you must meet the three following conditions:</span></span></span></span></span></span></span></p>

<ul>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Firstly, you need to demonstrate that you are married to a Thai person. </span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Secondly, at least 3 months before applying for a visa, you must verify that you own 400,000 Thai Baht on your bank account. </span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Thirdly, every after 90 days, you must identify that you live along with your husband/wife at the place that you applied for the visa.</span></span></span></span></span></span></span></li>
</ul>

<p style="text-align:justify; margin-left:48px">&nbsp;</p>

<ol start="3" style="list-style-type:upper-roman">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><a name="_Toc21636013"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Tourist visa in Thailand</span></span></span></span></b></a><a href="#_ftn6" name="_ftnref6" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span class="MsoFootnoteReference" style="vertical-align:super"><b><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">[6]</span></span></span></span></b></span></span></span></span></span></b></span></a></span></span></span></li>
</ol>

<h2 style="margin-top:13px"><span style="font-size:13pt"><span style="line-height:115%"><span style="font-family:Cambria,serif"><span style="color:#4f81bd"><a name="_Toc21636014"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">III.1. What are visa exemption and visa application?</span></span></a></span></span></span></span></h2>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><a name="_Toc21636015"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal">III.1.1. Visa exemption</span></span></span></a></span></span></h3>

<p align="center" style="text-align:center; margin-bottom:13px"><img height="519" src="/uploads/files/image-20191021111950-3.png" width="617" /></p>

<p align="center" class="MsoCaption" style="margin-left:240px; text-align:center; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:9pt"><span style="font-family:Calibri,sans-serif"><span style="color:#4f81bd"><span style="font-weight:bold"><span style="font-size:8.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span style="font-weight:normal">&nbsp;&nbsp; Table 3, Source: exploringrealthailand.com</span></span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The citizen from the countries in this above list can enter Thailand for tourism purposes and stay in Thailand for up to 30 days.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">When foreigners come to Thailand from another neighbor country (overland crossings), they just can stay in Thailand for a maximum of 15 days, except countries include UK, USA, Canada, Japan, Italia, France, Germany, and Malaysia, the people can stay in Thailand up to 30 days.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Foreigners enter Thailand without a visa need to prove that they have enough funding for the time of stay in cash or checks, specifically: 20.000 Baht per person or 40.000 Baht per family. </span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Within 6 months from the first time entry to Thailand, foreigners exempted from visa are entitled to leave or return to Thailand for up to 90 days (note that the passport need be valid six months from the day of the last departure)</span></span></span></span></span></span></span></p>

<h3><span style="font-size:13.5pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><a name="_Toc21636016"><span style="font-size:13.0pt"><span style="color:black"><span style="font-weight:normal">III.1.2. Visa application</span></span></span></a> </span></span></h3>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The tourists can apply for a visa application under the requirement of two main type visa which consists of Single-entry visa and Multiple-entry visa.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">To apply for a single-entry visa or multiple-entry visa, the tourist needs to prepare these following documents:</span></span></span></span></span></span></span></p>

<ul>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Passport valid for at least six months from the date of leaving Thailand</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Complaint application form. (This form is the Swiss version but your local Thai embassy will have the same).</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Two passport photos (not older than six months)</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The flight ticket which confirms leaving your country</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Hotel booking confirmation, the invitation letter, copy of the passport and residence permit of the person inviting you to Thailand.</span></span></span></span></span></span></span></li>
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Copies of the last three months&#39; bank statement with 1000 Dollars at the end of each month (with multiple entry visa, this money is more than) </span></span></span></span></span></span></span></li>
</ul>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Validity:</span></span></span></span></span></span></span></p>

<ul>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Single entry:&nbsp;&nbsp;&nbsp;&nbsp; Valid is 3 months; Period of stay is 60 days</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Multiple entry: Valid is 6 months; Period of stay is 60 days</span></span></span></span></span></span></span></li>
<li style="text-align:justify"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The fee depends on the regulation of Embassy in each country</span></span></span></span></span></span></span></li>
</ul>

<p style="text-align:justify; margin-bottom:13px; margin-left:48px">&nbsp;</p>

<h2 style="margin-top:13px"><span style="font-size:13pt"><span style="line-height:150%"><span style="font-family:Cambria,serif"><span style="color:#4f81bd"><a name="_Toc21636017"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">III.2. The process to apply for a tourist visa in Thailand</span></span></a></span></span></span></span></h2>

<p style="margin-bottom:13px">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Step 1</span></span></span></span></u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">: Determine what type of tourist visa that you need to apply for</span></span></span></span></span></span></span></p>

<p style="text-align:justify; text-indent:36.0pt; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Single entry visa or multiple entry visa</span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Step 2</span></span></span></span></u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">: Preparing the required documents before applying for a tourist visa</span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">In order to apply for a tourist visa, there is a compulsory document list and certain fees that the tourist needs to prepare to obtain the visa. The below list provides information about documents and fees that required: </span></span></span></span></span></span></span></span></p>

<ul>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Travel document or passport valid up to 6 months</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Duly filled Thailand visa application form</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Recent 4&times;6 cm. 1 passport size photograph</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Return e-ticket or air ticket (confirmed)</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Proof of financial sources (40000 baht/family, 20000 baht per person)Step 3: Apply for a visa</span></span></span></span></span></span></span></span></li>
</ul>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Step 3</span></span></span></span></u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">: Applying for a visa</span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Before applying, you need to prepare carefully documents and legal formalities. You should have an original copy of the passport which has valid for at least 6 months from the date arriving. </span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">There are two ways to apply for a tourist visa in Thailand. </span></span></span></span></span></span></span></span></p>

<ul>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Online: You can apply for a visa online VFS Thailand site (Visa Application Centre)</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Offline: You can submit the visa application and pay visa fees at the nearest Thailand Visa Application Centre. </span></span></span></span></span></span></span></span></li>
</ul>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Step 4</span></span></span></span></u><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">: Waiting for a visa</span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Under the current policy of Thailand, the visa applicant can receive the pre-approved visa with some significant documents after a period of 2 to 4 working days. </span></span></span></span></span></span></span></span></p>

<h2 style="margin-top:13px"><span style="font-size:13pt"><span style="line-height:115%"><span style="font-family:Cambria,serif"><span style="color:#4f81bd"><a name="_Toc21636018"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">III.3. How much does a tourist visa cost?</span></span></a></span></span></span></span></h2>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The fee for a tourist visa is 2000 Baht Thai on entry. </span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The paying form: Paying fee in cash and it should be paid in Thai currency. </span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Notes: Need to usually update the new information about tourist visa at the Thai Embassy/Consulate when having the need so that avoiding the sudden changes.</span></span></span></span></span></span></span></span></p>

<h2 style="margin-top:13px"><span style="font-size:13pt"><span style="line-height:115%"><span style="font-family:Cambria,serif"><span style="color:#4f81bd"><a name="_Toc21636019"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">III.4. Tips for interview round</span></span></a></span></span></span></span></h2>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The interview questions include two main parts:</span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Firstly, the guider will require you to cross-check the information which relates to yourself and your journey, so please preparing carefully all aspects and remember firmly your tour plan </span></span></span></span></span></span></span></span></p>

<p style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Secondly, you will be asked the questions relating directly to your trip, therefore, you need to speak honestly and focus on some key following points:</span></span></span></span></span></span></span></span></p>

<ul>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The time of stay in Thailand and the purpose of your Thai trip</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Considering all aspects of your travel plans</span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Your bank account </span></span></span></span></span></span></span></span></li>
<li style="margin-bottom:10px; text-align:justify"><span style="font-size:11pt"><span style="background:white"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Your occupation and organization.</span></span></span></span></span></span></span></span></li>
</ul>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<ol start="4" style="list-style-type:upper-roman">
<li style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><a name="_Toc21636020"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">Contact assistance for people who apply for a visa in Thailand</span></span></span></span></b></a></span></span></span></li>
</ol>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">The applicant can go to the local bureau of immigration in Thailand to ask the questions relating to exemption, extension and re-entry permission into Thailand.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">For questions and information within Thailand, visiting the website &ldquo;www.immigration.go.th&rdquo; or call +66 (0) 2 209 1100 to know more detail information.</span></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px">&nbsp;</p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:150%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:13.0pt"><span style="line-height:150%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black">REFERENCES:</span></span></span></span></b></span></span></span></p>

<h1 style="margin-bottom:8px; text-align:justify"><span style="font-size:14pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Cambria,serif"><span style="color:#365f91"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span style="font-weight:normal">Pretywildworld.com, (2019). <i>Thailand Visa on Arrival: How to apply for Thailand Visa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </i></span></span></span></span></span></span></span></span></span></span></h1>

<h1 style="margin-bottom:8px; margin-left:48px; text-align:justify"><span style="font-size:14pt"><span style="background:white"><span style="line-height:115%"><span style="font-family:Cambria,serif"><span style="color:#365f91"><i><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span style="font-weight:normal">2019</span></span></span></span></span></i><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:black"><span style="font-weight:normal">. Retrieved from </span></span></span></span></span><a href="https://www.prettywildworld.com/thailand-visa-on-arrival-%20%20requirements-application-exemption/" style="color:blue; text-decoration:underline"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="font-weight:normal">https://www.prettywildworld.com/thailand-visa-on-arrival-&nbsp; requirements-application-exemption/</span></span></span></span></a></span></span></span></span></span></h1>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">ExploringrealThailand.com, (2019). <i>Do you need a new visa?</i></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Retrieved from </span></span></span><a href="https://exploringrealthailand.com/do-you-need-a-visa-for-thailand" style="color:blue; text-decoration:underline"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">https://exploringrealthailand.com/do-you-need-a-visa-for-thailand</span></span></span></a></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">Jamie Gilpin, (2014). <i>What is the difference between a visa and a passport?</i></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Retrieved from </span></span></span><a href="https://resources.envoyglobal.com/blog/what-is-the-difference-between-a-passport-and-a-visa" style="color:blue; text-decoration:underline"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">https://resources.envoyglobal.com/blog/what-is-the-difference-between-a-passport-and-a-visa</span></span></span></a></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">Thaivisaexpert.com, (2019). <i>Thailand general visa information</i>.</span></span></span></span></span></span></p>

<p style="margin-left:36px; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">Retrieved from </span></span></span><a href="http://www.thaivisaexpert.com/?gclid=Cj0KCQjwivbsBRDsARIsADyISJ9sELTytACikXRkUZpLXEy9bNDg7qsul0di8yVzqZHi-6ZkFJgcIrYaAndCEALw_wcB" style="color:blue; text-decoration:underline"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">http://www.thaivisaexpert.com/?gclid=Cj0KCQjwivbsBRDsARIsADyISJ9sELTytACikXRkUZpLXEy9bNDg7qsul0di8yVzqZHi-6ZkFJgcIrYaAndCEALw_wcB</span></span></span></a></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">Wikipedia, (2019). <i>Visa policy of Thailand.</i></span></span></span></span></span></span></p>

<p style="text-align:justify; margin-bottom:13px"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Retrieved from </span></span></span><a href="https://en.wikipedia.org/wiki/Visa_policy_of_Thailand" style="color:blue; text-decoration:underline"><span style="font-size:13.0pt"><span style="line-height:115%"><span style="font-family:&quot;Times New Roman&quot;,serif">https://en.wikipedia.org/wiki/Visa_policy_of_Thailand</span></span></span></a></span></span></span></p>

<div>&nbsp;
<hr align="left" size="1" width="33%" />
<div id="ftn1">
<p class="MsoFootnoteText"><span style="font-size:10pt"><span style="font-family:Calibri,sans-serif"><a href="#_ftnref1" name="_ftn1" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">[1]</span></span></span></span></span></a> Refer to the article: What is the Difference Between a Passport and a Visa?</span></span></p>
</div>

<div id="ftn2">
<p class="MsoFootnoteText"><span style="font-size:10pt"><span style="font-family:Calibri,sans-serif"><a href="#_ftnref2" name="_ftn2" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">[2]</span></span></span></span></span></a> Refer to the article: Visa policy of Thailand</span></span></p>
</div>

<div id="ftn3">
<p class="MsoFootnoteText"><span style="font-size:10pt"><span style="font-family:Calibri,sans-serif"><a href="#_ftnref3" name="_ftn3" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">[3]</span></span></span></span></span></a> Visa on arrival is a kind of visa that is issued at a visa counter at the airport, people will apply and pay for a visa which will be placed into the passport on the spot before flying to another country</span></span></p>
</div>

<div id="ftn4">
<p class="MsoFootnoteText"><span style="font-size:10pt"><span style="font-family:Calibri,sans-serif"><a href="#_ftnref4" name="_ftn4" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">[4]</span></span></span></span></span></a> Refer to the article &ldquo;Do you need a visa for Thailand?&rdquo;</span></span></p>
</div>

<div id="ftn5">
<p class="MsoFootnoteText"><span style="font-size:10pt"><span style="font-family:Calibri,sans-serif"><a href="#_ftnref5" name="_ftn5" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">[5]</span></span></span></span></span></a> Refer to the website &ldquo;Thai visa expert&rdquo;</span></span></p>
</div>

<div id="ftn6">
<p class="MsoFootnoteText"><span style="font-size:10pt"><span style="font-family:Calibri,sans-serif"><a href="#_ftnref6" name="_ftn6" style="color:blue; text-decoration:underline" title=""><span class="MsoFootnoteReference" style="vertical-align:super"><span class="MsoFootnoteReference" style="vertical-align:super"><span style="font-size:10.0pt"><span style="line-height:115%"><span style="font-family:&quot;Calibri&quot;,sans-serif">[6]</span></span></span></span></span></a> Refer to the article &ldquo;Thailand visa on arrival: How to apply for Thailand Visa 2019&rdquo;</span></span></p>
</div>
</div>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'Thailand Visa | Luxury Travel | How to apply for Thailand Visa',
                'meta_description' => 'Visa on Arrival or get in advance from Thai Embassy. How will you apply for Thailand Visa to enter the country as a tourist? Know more about Thailand Visa- Tourist Visa Application, Validity',
                'created_at' => '2019-06-28 09:39:07',
                'updated_at' => '2019-10-21 11:19:59',
                'index' => 1,
            ),
            20 => 
            array (
                'id' => 38,
                'name' => 'ARE YOU LOOKING A JOB FOR YOUR CAREER?',
                'slug' => 'career',
                'image' => '{"thumbnail":"/uploads\\/images\\/sunset-bangkok.jpg","banner":"uploads\\/images\\/banner-laos9.jpg"}',
                'content_short' => NULL,
                'content' => '<p>Luxury Travel Company is Vietnam&rsquo;s First Luxury Tour Operator and Full Travel Service Agency To Service Vietnam, Laos, Cambodia, Myanmar and Thailand.</p>

<p align="justify">We have won numerous travel awards over many years. For further development, we are looking for the most qualified candidates for vacancies in our Hanoi head office.</p>

<p align="justify"><strong>Who We Are Looking For?</strong></p>

<p align="justify">Luxury Travel is looking for excellent people to join our dynamic, creative, energetic and friendly company.</p>

<p align="justify"><strong>The people we are looking for should:</strong></p>

<ul>
<li>Have a passion for travel, especially luxury travel</li>
<li>Possess excellent customer service and communication skills</li>
<li>Have excellent in English and Vietnamese (French and German are also helpful)</li>
<li>Have a luxury mind set</li>
<li>Be motivated, responsible and reliable</li>
<li>Be creative and innovative</li>
<li>Have a strong commitment to company values</li>
<li>Be able to work well in a multicultural team</li>
<li>Have working experience nationally or internationally, preferably in tourism.</li>
<li>Be honest</li>
<li>Have a sense of humor and a smile!</li>
</ul>

<p align="justify"><strong>What Do We Offer?</strong></p>

<ul>
<li>Attractive and competitive salary (negotiable) + bonus</li>
<li>Benefits &amp; allowances</li>
<li>Discounted travel benefits for all staff</li>
<li>Annual leave</li>
<li>Accrued 12 days of annual leave</li>
<li>Sick leave and maternity leave benefits</li>
<li>Health insurance and social insurance according to Vietnamese labor codes to all local employees (the staff member pays 6% and the Company 17% based on fixed month salary).</li>
<li>Travel allowance for those who travel for purposes of training or business</li>
</ul>

<hr />
<h3><strong>Position: Sales Manager</strong></h3>

<p>Luxury Travel is looking for a young, vibrant Business Developer to lead sales and business development activities in USA, Australia and New Zealand.</p>

<p><strong>Position Overview:</strong></p>

<p>Based in Hanoi, Vietnam you would promote the services of Luxury Travel (<a href="http://www.luxurytravels.asia/">www.luxurytravels.asia</a>) to travel partners (tour operators, travel agents, incentive houses, etc.).</p>

<p>Your main role will be to drive sales and business development, you should be a highly driven and proactive sales person able to identify and secure new accounts as well as to strengthen business partnerships with existing partners.</p>

<p>This position presents an opportunity to impact and contribute to overall group sales strategy and to directly participate in and drive the next phase of the company&rsquo;s growth with a team of motivated and passionate travel professionals.</p>

<p>You will report directly to the DOSM/COO based in Hanoi Head Office and will set and run your own budget with a great deal of freedom to run your own show. We are looking for a highly motivated sales person able to understand the big picture and lead our ambitious plans.</p>

<p><strong>Responsibilities:</strong></p>

<p>&middot; Developing strategies along with senior management to expand group sales into new markets and increase business in existing markets including strategic plans to increase business through Tour Operators and Travel Agents networks.</p>

<p>&middot; Create annual sales and marketing plan and budget for assigned market.</p>

<p>&middot; Identifying and contacting new potential agents and tour operators.</p>

<p>&middot; Making sales calls to introduce LUX&rsquo;s services to prospective clients.</p>

<p>&middot; Initiate marketing activities targeted at the assigned market and coordinate with marketing department to ensure that the market requirements are well represented and by marketing and communication materials.</p>

<p>&middot; Participate in major travel trade shows around the world to represent LUX, as assigned.</p>

<p>&middot; Respond to and follow up new agent enquiries and forward to operations offices with advice.</p>

<p>&middot; Liaise and coordinate with reservations and operations teams to ensure that agents are dealt with promptly, professionally and receive the best possible services.</p>

<p>&middot; Arrange FAM trips for overseas agents and escort groups.</p>

<p>&middot; Monitor new agents&rsquo; performances and bookings.</p>

<p>&middot; Provide annual training for Travel Consultants handling your Markets in each of the LUX Destinations.</p>

<p><strong>Qualifications</strong></p>

<p>&bull; Graduate/ Post Graduate</p>

<p>&bull; Minimum 5 yrs of experience in Travel with minimum 2yrs of leading Sales for a large market.</p>

<p>&bull; Fluency in English (verbal &amp; written) must.</p>

<p>&bull; Experience in using some type of Travel software and Database would be beneficial.</p>

<p>Skills</p>

<p>&bull; Understanding of the market trends &amp; comprehensive travel industry knowledge</p>

<p>&bull; Understanding customer needs and high customer service standards.</p>

<p>&bull; Strong organization and prioritization skills</p>

<p>&bull; Excellent interpersonal and good public speaking skills.</p>

<p>&bull; Leader with a positive and dynamic personality, able to adapt to different cultures.</p>

<p>If you think this role is for you, please contact&nbsp;<a href="mailto:ceo@luxurytravelvietnam.com">ceo@luxurytravelvietnam.com</a></p>

<hr />
<h3>Position: COO</h3>

<p>We are looking for a competent COO in Hanoi, Vietnam, who has knowledge, skills and attitude.</p>

<p>To lead and manage the Lux office including the long term development and the management of day to day business of the Company.</p>

<p>The COO will provide the leadership to enable the company to achieve its sales goals, by promoting business profitability, monitoring performance, developing new business and effectively utilizing resources.</p>

<p>The COO reports to CEO and shall submit strategy and implement the approved business plans.</p>

<p><strong>RESPONSIBILITIES:-</strong></p>

<ul>
<li>Manage development and implementation of Business strategy (Company 3 year, 1 year and Department goals approved by CEO and regular performance reporting)</li>
<li>Monitoring and reporting of company performance (monthly performance analysis report and financial reports against targets)</li>
<li>Company profitability (Operating expenses per month within budget, supplier costs within budget and quality assessed by the LUX management)</li>
<li>Manage product development process to ensure unique and defendable</li>
<li>Quality of trips (target rating from passenger feedback and other monitoring system)</li>
<li>Establish and maintain an effective local marketing and sales strategy (Direct sales, B2B contracts)</li>
<li>Maintain regular contact with government including timely reports (tax department and other agencies as required)</li>
<li>Understand company&rsquo;s vision, mission, purpose and values and be able to articulate them to the staff and partners.</li>
<li>Develop businesses; meet the annual sales target set in the budget.</li>
<li>Ensure effective HR management; employ only competent people having knowledge + skills+ attitude. Monthly train the team for perfection.</li>
<li>Ensure an effective development of IT system, processes, standards in LUX.</li>
<li>Maintain good processes and standards of five star travel experiences.</li>
<li>Develop external relationships and partnerships and represent the company (State agencies, third party, media, industry)</li>
<li>Proven track record of management across different cultures</li>
<li>Perform any other related duties incidental to the work described herein.</li>
</ul>

<p><strong>QUALIFICATIONS AND EXPERIENCE:-</strong></p>

<ul>
<li>Minimum of middle level management experience; Company management experience preferred</li>
<li>University or Master degree, preferably economics, business administration or tourism management</li>
<li>High standards of customer service</li>
<li>Understand of Hoteliers, DMCs and Tour Operators, Cruising operations.</li>
<li>Work experience in tourism management or similar position is a plus</li>
<li>Proven experience in cruising, MICE, Group Operations and other areas of the company is a plus.</li>
<li>Experience of working with computers, particularly Microsoft applications</li>
<li>For Vietnamese candidates must be fluent in spoken and written English, Vietnamese and 2nd foreign language is an added value.</li>
<li>Work independently, under pressure, be a good sales goals setter and time management elite.</li>
<li>Willingness to travel</li>
</ul>

<p><strong>OTHER SKILLS AND ATTRIBUTES:-</strong></p>

<ul>
<li>Ability to work strategically on the business and also operationally in the business</li>
<li>Responsible and reliable</li>
<li>Knowledge and experience in financial management</li>
<li>Understanding of luxury tour operations</li>
<li>Excellent interpersonal and communication skills</li>
<li>Demonstrate leadership and ability to foster teamwork</li>
<li>Experience travel in the region and passion for luxury travel</li>
<li>Problem solving and decision making capacities</li>
<li>Experience in multinational business group advantageous</li>
<li>Ability to deal with uncertainties and a fast-changing business environment</li>
<li>An understanding of and believe in Responsible Travel philosophy</li>
<li>Vietnamese nationality, expatriates candidates are preferable.</li>
</ul>

<p><strong>THE PACKAGE</strong></p>

<p>The successful candidate will be offered an excellent salary along with bonus and company benefits.</p>

<p><strong>INTERESTED?</strong></p>

<p>If you are interested in this position please send CV to&nbsp;<a href="mailto:ceo@luxurytravelvietnam.com">ceo@luxurytravelvietnam.com</a>,</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => NULL,
                'created_at' => '2019-07-01 15:32:04',
                'updated_at' => '2019-07-01 15:32:04',
                'index' => 1,
            ),
            21 => 
            array (
                'id' => 40,
                'name' => 'Policy and procedures handbook of Luxury Travel',
                'slug' => 'policy-and-procedures-handbook-of-luxury-travel',
                'image' => '{"thumbnail":"\\/uploads\\/images\\/tours\\/Laos\\/hanoi-ft.jpg","banner":"\\/uploads\\/images\\/tours\\/Laos\\/payment.jpg"}',
                'content_short' => NULL,
                'content' => '<div class="WordSection1">
<p>&nbsp;&nbsp; &nbsp;&nbsp;<br />
As a travel company who engage with various stakeholders in tourism, namely consumers, tour guides, travel agencies, hotels, transport companies, restaurants, and attractions, Luxury Travel understands our key role and influence in the sustainability development of tourism. Therefore, we are committed to promoting sustainability. We aim to follow, implement and promote good sustainability practices to maximize positive impacts and minimize negative impacts on tourism of our operations and to influence our clients and partners to do the same.&nbsp;<br />
Our sustainability policy is divided into 10 themes. Each theme consists of a set of principles and practical actions accordingly. &nbsp;&nbsp; &nbsp;<br />
1. Sustainability Management &amp; Legal compliance<br />
We commit to sustainability management, practiced by these following actions:<br />
&bull;&nbsp;&nbsp; &nbsp;To have an appointed employee who is responsible for sustainability coordinator tasks;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;To have a sustainability mission statement that is communicated to customers, partners and suppliers;<br />
&bull;&nbsp;&nbsp; &nbsp;To have an accessible and written sustainability policy that aims for a reduction of the negative social, cultural, economic and environmental impacts of the company&rsquo;s activities; and includes employee related health and safety aspects;<br />
&bull;&nbsp;&nbsp; &nbsp;To collaborate and actively involved in external forums and working groups which are supportive to sustainability in tourism;<br />
&bull;&nbsp;&nbsp; &nbsp;To conduct baseline assessment of the company&rsquo;s performance on sustainable practices;<br />
&bull;&nbsp;&nbsp; &nbsp;To have sustainability guidelines and assessment system in place to identify the sustainability performance of key suppliers/partners;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;To have sustainability action plan with clear targets, actions, measures, responsibilities and time planning;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;To develop documented procedures to monitor and evaluate the implementation of the sustainability policy, objectives and targets;<br />
&bull;&nbsp;&nbsp; &nbsp;To ensure company&rsquo;s transparency in sustainability by public reporting and communicating;<br />
&bull;&nbsp;&nbsp; &nbsp;To ensure that all staff are fully aware of our Sustainability Policy and are committed to implementing and improving it.<br />
We commit to complying with all national legislation, regulations and codes of practice.<br />
2. Internal management: social policy &amp; human rights<br />
We commit to sustainable internal management by having clear written and well-communicated social policy that includes the following principles:<br />
&bull;&nbsp;&nbsp; &nbsp;To grant employees the freedom of employment and contract termination with notice (ideally minimum one month) and without penalty&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;To include labor conditions according to national labor law and a job description in the employment contract;<br />
&bull;&nbsp;&nbsp; &nbsp;Wage rate is to be mentioned in the contract and equals or above the national legal wage;<br />
&bull;&nbsp;&nbsp; &nbsp;To determinate and compensate of overtime working hours based on agreement;<br />
&bull;&nbsp;&nbsp; &nbsp;To provide medical and liability insurance according to the national law;<br />
&bull;&nbsp;&nbsp; &nbsp;To grant employees fixed paid yearly holiday and sick leave and unpaid annual leave allowance;<br />
&bull;&nbsp;&nbsp; &nbsp;To have health and safety policy for employees which complies to national legal standards;<br />
&bull;&nbsp;&nbsp; &nbsp;To have first aid sets and trained staff are available at all relevant locations;<br />
&bull;&nbsp;&nbsp; &nbsp;To obey national concerning Minimum Age for Admission to Employment;<br />
&bull;&nbsp;&nbsp; &nbsp;To have documented effective procedures in place for employees to voice out their complaints and expectations;<br />
&bull;&nbsp;&nbsp; &nbsp;To have a clear disciplinary procedure that is effectively communicated with employees;<br />
&bull;&nbsp;&nbsp; &nbsp;To have a measurement system for employee satisfaction on a regular basis;<br />
&bull;&nbsp;&nbsp; &nbsp;To provide periodic guidance and training for employees on roles, rights and responsibilities regarding health and safety issues. This includes fire and relevant natural disasters;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;To create opportunities for students in participating in traineeship/internship/apprenticeship; &nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;To encourage employment opportunities for persons with special needs; &nbsp;<br />
We commit to practice human rights by ensure the enforcement of following practices:<br />
&bull;&nbsp;&nbsp; &nbsp;To declare not to hinder trade union membership, collective labor negotiations and representation of members by trade unions;<br />
&bull;&nbsp;&nbsp; &nbsp;To participate and comply with a (sector wide) collective labor condition negotiation structure (if locally existing)<br />
&bull;&nbsp;&nbsp; &nbsp;To prohibit discriminations, regard to recruitment, conditions of employment, access to training and senior positions, or promotion in terms of gender, race, age, disability, ethnicity, religion/beliefs or sexual orientation;<br />
&bull;&nbsp;&nbsp; &nbsp;To ensure all employees have an equal chance and access to resources and opportunities for personal development through regular training, education;&nbsp;<br />
3. Internal Management: Environment and community relations<br />
We commit to practice environmental protection and enhance community relations by ensuring the enforcement of following practices:<br />
&bull;&nbsp;&nbsp; &nbsp;Actively reduce the use of disposable and consumer goods;<br />
&bull;&nbsp;&nbsp; &nbsp;Favor the purchase of sustainable goods and services, office and catering supply, giveaways and merchandise;<br />
&bull;&nbsp;&nbsp; &nbsp;Purchase products in bulk, to reduce the amount of packaging materials;<br />
&bull;&nbsp;&nbsp; &nbsp;Set copy and printing machines by default to double-sided printing or other forms of paper saving modes;<br />
&bull;&nbsp;&nbsp; &nbsp;Use cleaning materials which are non-hazardous, non-eutrophic and biodegradable and are certified with an eco-label, if locally available;<br />
&bull;&nbsp;&nbsp; &nbsp;Print brochures on environmentally friendly paper, with a printing company that works with a certified environmental management system, if locally available at reasonable costs;<br />
&bull;&nbsp;&nbsp; &nbsp;Implement measurements to reduce brochure wastage or an &#39;internet only&#39; policy;<br />
&bull;&nbsp;&nbsp; &nbsp;Have an active commitment to measure, monitor and reduce energy consumption;<br />
&bull;&nbsp;&nbsp; &nbsp;Calculate and compensate CO2 emissions and compare different periods;<br />
&bull;&nbsp;&nbsp; &nbsp;Purchase green energy and energy efficient lighting for all areas, when available;<br />
&bull;&nbsp;&nbsp; &nbsp;Switch off Lights and equipment when not in use, use automatic switch on/off system with timers or movement sensors and set equipment by default in the energy saving mode, where this is feasible;<br />
&bull;&nbsp;&nbsp; &nbsp;Prefer low energy equipment when buying new items, including considerations of cost and quality;<br />
&bull;&nbsp;&nbsp; &nbsp;Have an active policy to reduce water consumption, implemented and monitored on a monthly or yearly basis for benchmark purposes;<br />
&bull;&nbsp;&nbsp; &nbsp;Use sustainable water sourcing, which does not adversely affect environmental flows;<br />
&bull;&nbsp;&nbsp; &nbsp;Install water saving equipment in toilets, re-use waste water and/or collected rainwater;<br />
&bull;&nbsp;&nbsp; &nbsp;Comply with the national legislation concerning waste disposal;<br />
&bull;&nbsp;&nbsp; &nbsp;Develop and implement a solid waste reduction and recycling policy, with quantitative goals;<br />
&bull;&nbsp;&nbsp; &nbsp;Take measures to reduce the amount of packaging materials and not provide non-recyclable or non-biodegradable package materials;<br />
&bull;&nbsp;&nbsp; &nbsp;Take action to reduce the amount of (non-refillable) plastic bottles of drinking water for office use;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Separate all materials which can be recycled and organize collection and proper disposal;<br />
&bull;&nbsp;&nbsp; &nbsp;Implement waste reducing methods when using ink and toner cartridges for printing and copying, whenever feasible; &nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Recycle or properly dispose of batteries; &nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Comply with national legislation of wastewater treatment, which should be reused or released safely;<br />
&bull;&nbsp;&nbsp; &nbsp;Minimize and substitute the use of harmful substances and manage properly the storage, handling and disposal of chemicals;<br />
&bull;&nbsp;&nbsp; &nbsp;Use lead-free and water based paints, both inside and outside, when locally available;<br />
&bull;&nbsp;&nbsp; &nbsp;Implement practices to minimize pollution from its buildings (as far as being able to be controlled by the company);<br />
&bull;&nbsp;&nbsp; &nbsp;Measure and reduce staff related travel and use more sustainable modes of transport. Calculate its emissions, with the aim to reduce and compensate, through a reliable locally available program;<br />
&bull;&nbsp;&nbsp; &nbsp;Financially encourage employees to use public transport or sustainable means of transport;<br />
&bull;&nbsp;&nbsp; &nbsp;Reduce transport related impacts &nbsp; by tele-work, tele/video meetings, work-at-home policies or other means;<br />
&bull;&nbsp;&nbsp; &nbsp;Maintain and properly check motorized company vehicles, to reduce emissions and energy use and make sure they comply with the legal emission standards,<br />
&bull;&nbsp;&nbsp; &nbsp;Provide periodic guidance, training and/or information to all staff members, about their roles and responsibilities with respect to internal environmental practices;<br />
&bull;&nbsp;&nbsp; &nbsp;Comply with land use, zoning and protected or heritage area laws and regulations; when planning, designing, constructing, renovating, operating or demolishing &nbsp; company buildings and infrastructure;<br />
&bull;&nbsp;&nbsp; &nbsp;Base planning, design and construction of new buildings or renovations, on locally appropriate and feasible sustainable practices and materials;<br />
&bull;&nbsp;&nbsp; &nbsp;Contribute to the protection and preservation of local historical, archaeological, culturally, and spiritually important properties and sites, and &nbsp; not impede access to them by local residents;<br />
4. Partner agency<br />
Based on an inventory of our key partner agencies, we have developed and implemented a policy to improve sustainability of our partner agencies. Our aim is to make sustainable development concrete to each and every partner within our business.&nbsp;<br />
We commit to this by;<br />
&bull;&nbsp;&nbsp; &nbsp;Keeping a list of the sustainability practices of partner accommodations and agents;<br />
&bull;&nbsp;&nbsp; &nbsp;Only working with organisations who are truly implementing sustainability in their tourism policy;<br />
&bull;&nbsp;&nbsp; &nbsp;Minimalizing the ecologic footprint of the office by travelling mainly via public transport, working as paperless as possible, separating waste, and to make use of certified recycled paper;<br />
&bull;&nbsp;&nbsp; &nbsp;Paying attention to the local benefits of communities when selecting local accommodations and their social policy for employees;<br />
&bull;&nbsp;&nbsp; &nbsp;Raising awareness among key partners on sustainable consumption by organising (online) campaigns and trainings;<br />
&bull;&nbsp;&nbsp; &nbsp;Informing key partners on the Travelife and national tourism standards;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Having a cooperation contract including an annex of the national code of conduct for local partners to encourage their practices towards sustainability;<br />
&bull;&nbsp;&nbsp; &nbsp;Evaluating the sustainability practices regularly of our key partners to ensure their practices are truly sustainable;<br />
&bull;&nbsp;&nbsp; &nbsp;Informing key partners about the travel companies&rsquo; sustainability policy and that they are expected to comply with it and/or communicate it to final customers where relevant;<br />
&bull;&nbsp;&nbsp; &nbsp;Including key sustainability clauses in contracts with inbound/receptive partners;<br />
&bull;&nbsp;&nbsp; &nbsp;Motivating incoming/inbound partners to participate in sustainability trainings for travel companies;<br />
&bull;&nbsp;&nbsp; &nbsp;Having a written contract with partner agencies;<br />
&bull;&nbsp;&nbsp; &nbsp;Including clauses in the partner contracts that enable contract partners to end the contractual agreement prematurely if the partner company does not take adequate measures to prevent sexual exploitation of children within the direct supply chain;<br />
&bull;&nbsp;&nbsp; &nbsp;Ensuring that partner companies comply with all relevant national laws protecting the rights of employees;<br />
5. Transport<br />
We try to ensure that vehicles used on tours do not cause more than average pollution. We believe that transport is an important aspect of sustainable tourism, and we do our best to decrease the average pollution level.<br />
We commit to this by;<br />
&bull;&nbsp;&nbsp; &nbsp;Selecting the most sustainable options considering price and comfort when selecting transport options to the destination;<br />
&bull;&nbsp;&nbsp; &nbsp;Including sustainable (public) transport to the point of departure for the international/long distance journey;<br />
&bull;&nbsp;&nbsp; &nbsp;Considering and giving preference to more sustainable alternatives when selecting transport options for transfers and excursions in the destination, taking into account price, comfort, and practical considerations;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Integrating and/or promoting one or more sustainable holiday products/packages based on a recognised methodology, including sustainable transport, sustainable accommodations, and sustainable activities.&nbsp;<br />
6. Accommodations<br />
We try to achieve a tourism supply chain that is fully sustainable. The partner accommodations play an important role in achieving this, and are stimulated and motivated to adapt sustainable practices.&nbsp;<br />
We commit to this by;<br />
&bull;&nbsp;&nbsp; &nbsp;Selecting accommodations that comply with sustainability and quality standards with a special focus on the following items;<br />
-&nbsp;&nbsp; &nbsp;Do they have a signed sustainability contract?<br />
-&nbsp;&nbsp; &nbsp;Do they have a water saving program?<br />
-&nbsp;&nbsp; &nbsp;Do they have an energy saving program?<br />
-&nbsp;&nbsp; &nbsp;Do they have a waste management program?<br />
-&nbsp;&nbsp; &nbsp;Do they have an energy reduction system?<br />
-&nbsp;&nbsp; &nbsp;Do they have a sustainable supply chain?<br />
-&nbsp;&nbsp; &nbsp;Do they have a child protection policy?<br />
-&nbsp;&nbsp; &nbsp;Do they conduct CSR activities?&nbsp;<br />
-&nbsp;&nbsp; &nbsp;Do they train employees in Health &amp; Safety?<br />
&bull;&nbsp;&nbsp; &nbsp;Motivating and encouraging partner accommodations to become sustainably certified;<br />
&bull;&nbsp;&nbsp; &nbsp;Preferring and selecting accommodations that are locally owned and managed;<br />
&bull;&nbsp;&nbsp; &nbsp;Selecting accommodations that employ local communities;<br />
&bull;&nbsp;&nbsp; &nbsp;Having accommodations provide evidence clarifying their sustainability goals and strategies;<br />
&bull;&nbsp;&nbsp; &nbsp;Having accommodations sign a sustainability addendum;<br />
&bull;&nbsp;&nbsp; &nbsp;Encouraging accommodations to follow best practices/trainings on responsible tourism;<br />
&bull;&nbsp;&nbsp; &nbsp;Encouraging accommodations to fill in the sustainability questionnaire to gain insight in their practises;<br />
&bull;&nbsp;&nbsp; &nbsp;Clearly and actively communicating our sustainability objectives and requirements regarding accommodations to contracted and other relevant accommodations;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Giving clear preference to accommodations that work with internationally acknowledged (e.g. GSTC recognised) and/or Travelife certification;<br />
&bull;&nbsp;&nbsp; &nbsp;Including standard sustainability clauses in all contracts with accommodation providers that focus on child labour, anti-corruption and bribery, waste management and protection of biodiversity;<br />
&bull;&nbsp;&nbsp; &nbsp;Offering incentives to accommodations that are actively engaging in sustainability;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Ensuring that through our accommodation supply chain, the rights of children are respected and safeguarded by;<br />
-&nbsp;&nbsp; &nbsp;Having a clause in contracts throughout the value chain stating a common repudiation and zero tolerance policy of sexual exploitation of children;<br />
-&nbsp;&nbsp; &nbsp;Having a clause dedicated to this aspect in their contract that enables the travel company to end the contractual agreement prematurely if the accommodation supplier does not take adequate measures to prevent sexual exploitation of children;<br />
-&nbsp;&nbsp; &nbsp;Training employees in children&rsquo;s rights, the prevention of sexual exploitation and how to report suspected cases;<br />
-&nbsp;&nbsp; &nbsp;Supporting, collaborating with, and engaging stakeholders in the prevention of sexual exploitation of children;<br />
&bull;&nbsp;&nbsp; &nbsp;Working with accommodations and restaurants that incorporate elements of local art, architecture, or cultural heritage; while respecting the intellectual property rights of local communities;<br />
&bull;&nbsp;&nbsp; &nbsp;Terminating cooperation with accommodation in case of clear evidence that contracted accommodations jeopardize the provision of integrity of basic services such as food, water, energy, healthcare, or soil to the neighbouring companies.;<br />
7. Excursions and activities<br />
We value animal and community welfare extremely high and aims at tours that only leave a minor footprint. We are safeguarding the authenticity of the communities and the natural environment, and are strongly against harming wildlife and polluting the environment.&nbsp;<br />
We commit to this by;<br />
&bull;&nbsp;&nbsp; &nbsp;Having an inventory of environmentally or culturally sensitive excursions which are offered in each destination;<br />
&bull;&nbsp;&nbsp; &nbsp;Advising guests on behaviour standards during excursions and activities with a focus on respecting the local culture, nature, and environment;<br />
&bull;&nbsp;&nbsp; &nbsp;Communicating our sustainability objectives and requirements to contracted and other relevant excursion providers by distributing this information via code of conducts, representative agents, social media, email, discussions, and/or meetings, to minimise negative visitor impact and maximise enjoyment;<br />
&bull;&nbsp;&nbsp; &nbsp;Not offering any excursions that harm humans, animals, plants, natural resources such as water and energy, or which are socially and culturally unacceptable;<br />
&bull;&nbsp;&nbsp; &nbsp;Not offering any excursions in which wildlife is held captive, except for properly regulated activities in compliance with local, national, and international law;<br />
&bull;&nbsp;&nbsp; &nbsp;Not being involved with companies that harvest, consume, display, sell, or trade wildlife species unless it is part of a regulated activity that ensures that their utilisation is sustainable and in compliance with local, national, and international law;<br />
&bull;&nbsp;&nbsp; &nbsp;Having skilled and/or certified guides to guide our guests in sensitive cultural sites, heritage sites, or ecologically sensitive destinations;<br />
&bull;&nbsp;&nbsp; &nbsp;Promoting and advise our guests on excursions and activities which directly involve and support local communities by purchasing services or goods, traditional crafts and local (food) production methods, or visiting social projects;&nbsp;<br />
&bull;&nbsp;&nbsp; &nbsp;Promoting and advising our guests on excursions and activities which support local environment and biodiversity such as visiting protected areas or environmental protection projects;<br />
8. Tour leaders, local representatives and guides<br />
We aim at involving as many locals as possible by employing them in the tourism business. We stand for a fair and safe working environment that supports and respects local communities.&nbsp;<br />
We commit to this by;<br />
&bull;&nbsp;&nbsp; &nbsp;Ensuring that all employees have a written employment contract, including labour conditions and a job description, and fully understand the terms and conditions;<br />
&bull;&nbsp;&nbsp; &nbsp;Preferring to work with local tour leaders, local representatives, local tour guides, porters, drivers, cooks, and other local staff in case of equal ability, and provide training as required;<br />
&bull;&nbsp;&nbsp; &nbsp;Ensuring that our local partners comply with all applicable international, national, and local laws and regulations, industry minimum standards, and any other relevant statutory requirements whichever requirements are more stringent;<br />
&bull;&nbsp;&nbsp; &nbsp;Paying tour leaders, local representatives, guides, porters and other local staff contracted by us at least a living wage that is equal to or above the legal minimum or relevant industry standard;<br />
&bull;&nbsp;&nbsp; &nbsp;Ensuring that our tour guides, hosts, and other employees under contract are qualified and trained regularly;<br />
&bull;&nbsp;&nbsp; &nbsp;Ensuring that our local employees are informed on relevant aspects of our sustainability policy and comply with it, by newsletters, references or supplements to contracts, emails, or training and information sessions;<br />
&bull;&nbsp;&nbsp; &nbsp;Offering a special sustainable travel module in the trainings program for local tour guides and hosts, in which the main responsible tourism aspects are brought to attention followed by the role expected from the employees. This module will also include knowledge regarding the destination and its relevant sustainability aspects;<br />
&bull;&nbsp;&nbsp; &nbsp;Having our tour leaders, local representatives and guides inform clients on relevant sustainability matters in the destination (e.g. protection of flora, fauna, and cultural heritage, resource use), social norms and values (e.g. tips, dressing code and photography) and human rights (e.g. sexual exploitation);<br />
&bull;&nbsp;&nbsp; &nbsp;Training our employed tour leaders and local representatives on the avoidance of sexual exploitation of children. This will include training on how to check the requirements concerning exclusion of child abuse;<br />
9. Destination<br />
We aim to maximize positive impacts and minimize negative impacts at destination to ensure the sustainable development of the places that we operate in.&nbsp;<br />
We commit to this by:<br />
&bull;&nbsp;&nbsp; &nbsp;Consider sustainability aspects in the selection process of new destinations and possibly offer alternative, non-mainstream destinations;<br />
&bull;&nbsp;&nbsp; &nbsp;Not selecting destinations in which tourism leads to structural negative local effects, (unless the company&#39;s involvement results in clear counter balancing effects);<br />
&bull;&nbsp;&nbsp; &nbsp;Consider selection of new destinations, which are reachable through more sustainable means of transport;<br />
&bull;&nbsp;&nbsp; &nbsp;Comply with legally based spatial planning, protected areas and heritage regulations. &nbsp;Also with destination management strategies of local, regional and national authorities;<br />
&bull;&nbsp;&nbsp; &nbsp;Support initiatives that improve the relationships between accommodations and local producers;<br />
&bull;&nbsp;&nbsp; &nbsp;Influence and support local government (when possible, together with other travel companies and stakeholders) concerning sustainability, destination planning and management, use of natural resources and socio-cultural issues;<br />
&bull;&nbsp;&nbsp; &nbsp;Support biodiversity conservation, including protected areas and areas of high biodiversity, through &nbsp; financial contribution, political support, and integration in product offers;<br />
&bull;&nbsp;&nbsp; &nbsp;Not promote souvenirs which contain threatened flora and fauna species as indicated in the CITES treaty and the IUCN &lsquo;Red List&rsquo;; or historic and archaeological artefacts (except as permitted by law);<br />
10. Customer communication and protection<br />
Customers welfare and information are very important to us. At Indochina Junk, we ensure clear and constant communication and high protection to our clients.&nbsp;<br />
Prior to booking, we commit to this by:<br />
&bull;&nbsp;&nbsp; &nbsp;Make available a company guideline for client consultation, which is followed by client advisors;<br />
&bull;&nbsp;&nbsp; &nbsp;Ensure that customer privacy is not compromised;<br />
&bull;&nbsp;&nbsp; &nbsp;Comply with relevant standards and voluntary codes of conduct in marketing and advertising messages, and not promise more than is delivered;<br />
&bull;&nbsp;&nbsp; &nbsp;Make product and price information clear, complete and accurate, with regard to the company and its products and services, including sustainability claims;<br />
&bull;&nbsp;&nbsp; &nbsp;Provide destination information, including sustainability aspects, which is factually correct, balanced and complete;<br />
&bull;&nbsp;&nbsp; &nbsp;Inform clients about the environmental impact of different transport options to reach the destination (in case these are not included in the package), and to offer sustainable alternatives, where available;<br />
&bull;&nbsp;&nbsp; &nbsp;Promote (Certified) sustainable accommodations, excursions, packages and/or transport options, with logos or other messages; ensuring they are recognizable to consumer and presented as the &ldquo;better&rdquo; option;<br />
&bull;&nbsp;&nbsp; &nbsp;Inform the customer about sustainable alternatives concerning accommodations, excursions, package holidays and transport options, if available;<br />
&bull;&nbsp;&nbsp; &nbsp;Clearly inform (potential) direct customers, about sustainability commitments and actions;<br />
After booking and during holidays, we commit to this by:<br />
&bull;&nbsp;&nbsp; &nbsp;Provide Information to consumers about the natural surroundings, local culture and cultural heritage in the holiday destination;<br />
&bull;&nbsp;&nbsp; &nbsp;Inform consumers about key sustainability aspects and issues in the destination and receive recommendations on how to make a positive contribution;<br />
&bull;&nbsp;&nbsp; &nbsp;Inform customers about risks and precautions related to health and safety matters in the destination;<br />
&bull;&nbsp;&nbsp; &nbsp;Keep a contact person &nbsp; and a telephone number permanently available for emergency situations;<br />
&bull;&nbsp;&nbsp; &nbsp;Train personnel and keep guidelines available, on how to deal with emergency situations;<br />
&bull;&nbsp;&nbsp; &nbsp;Provide clients with documented guidelines and/or codes of conduct for sensitive excursions and activities, to minimize negative visitor impact and maximize enjoyment. When possible, guidelines are developed in collaboration with relevant NGO&#39;s and the affected community;<br />
&bull;&nbsp;&nbsp; &nbsp;Provide customers with information about commercial, sexual or any other form of exploitation and harassment, particularly of children and adolescents;<br />
&bull;&nbsp;&nbsp; &nbsp;Inform clients about applicable legislation concerning the purchasing, sales, import and export of historic or religious artefacts and articles containing materials of threatened flora and/or fauna in the destination;<br />
&bull;&nbsp;&nbsp; &nbsp;Motivate clients to use local restaurants and shops (where appropriate);<br />
&bull;&nbsp;&nbsp; &nbsp;Inform clients on sustainable transport options in destinations, when feasible;<br />
&bull;&nbsp;&nbsp; &nbsp;Encourage clients to donate to local charity and sustainable initiatives;<br />
After holidays, we commit to this by:<br />
&bull;&nbsp;&nbsp; &nbsp;Measure systematically client satisfaction and take into account the results, for service and product improvements;<br />
&bull;&nbsp;&nbsp; &nbsp;Include sustainability as an integral part of the research into client satisfaction;<br />
&bull;&nbsp;&nbsp; &nbsp;Have clear procedures in case of complaints from clients;</p>

<p><br />
&nbsp;</p>
</div>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => 'Policy and procedures handbook of Luxury Travel',
                'meta_description' => 'As a travel company who engage with various stakeholders in tourism, namely consumers, tour guides, travel agencies, hotels, transport companies, restaurants, and attractions, Luxury Travel u',
                'created_at' => '2019-07-22 13:27:55',
                'updated_at' => '2019-07-22 13:29:20',
                'index' => 1,
            ),
            22 => 
            array (
                'id' => 41,
                'name' => 'test',
                'slug' => 'test',
                'image' => '{"thumbnail":null,"banner":null}',
                'content_short' => NULL,
                'content' => '<p>12</p>',
                'type' => 'page',
                'status' => 1,
                'lang_id' => NULL,
                'parent_id' => NULL,
                'order_menu' => NULL,
                'meta_title' => NULL,
                'meta_description' => 'Hfhfjfjfj',
                'created_at' => '2019-09-06 19:08:31',
                'updated_at' => '2019-10-15 10:38:28',
                'index' => 1,
            ),
        ));
        
        
    }
}