<?php

use Illuminate\Database\Seeder;

class HotelTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hotel_types')->delete();
        
        \DB::table('hotel_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super Category / 3-star hotel',
                'order_menu' => NULL,
                'created_at' => '2019-10-25 08:33:11',
                'updated_at' => '2019-10-25 08:33:11',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'First Class Category / 4-star hotel',
                'order_menu' => NULL,
                'created_at' => '2019-10-25 08:33:27',
                'updated_at' => '2019-10-25 08:33:27',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Dulex Category / 5-star hotel',
                'order_menu' => NULL,
                'created_at' => '2019-10-25 08:33:46',
                'updated_at' => '2019-10-25 08:33:46',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Luxury Category / Luxury 5-star hotel',
                'order_menu' => NULL,
                'created_at' => '2019-10-25 08:34:03',
                'updated_at' => '2019-10-25 08:34:03',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Test kiểu khách sạn',
                'order_menu' => NULL,
                'created_at' => '2019-10-28 14:32:30',
                'updated_at' => '2019-10-28 14:32:30',
            ),
        ));
        
        
    }
}