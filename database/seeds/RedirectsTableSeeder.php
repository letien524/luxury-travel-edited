<?php

use Illuminate\Database\Seeder;

class RedirectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('redirects')->delete();
        
        \DB::table('redirects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'from' => '/old-blog',
                'to' => 'https://news.zing.vn/curacao-doi-thu-cua-tuyen-viet-nam-o-chung-ket-manh-co-nao-post954035.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-06-06 10:28:41',
                'updated_at' => '2019-06-06 10:28:41',
            ),
            1 => 
            array (
                'id' => 2,
                'from' => '/blog/aaa',
                'to' => 'http://demo03.gcosoftware.vn/luxury-travel/blog/payment-plicies',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-06-06 10:35:59',
                'updated_at' => '2019-06-06 10:35:59',
            ),
            2 => 
            array (
                'id' => 4,
                'from' => '/tour-item/tour-link-cu',
                'to' => 'http://2020.luxtravel.in/tour-item/tour-link-moi',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-06-12 14:21:02',
                'updated_at' => '2019-06-12 14:21:02',
            ),
            3 => 
            array (
                'id' => 9,
                'from' => 'bach-dep-trai-vo-doi/',
                'to' => 'http://2020.luxtravel.in/luxury-travel-en/tour-item/mai-chau-valley-adventure-7-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-06-21 09:54:18',
                'updated_at' => '2019-06-21 09:54:18',
            ),
            4 => 
            array (
                'id' => 10,
                'from' => '/tour-item/northern-tribes-markets-5-days',
                'to' => 'http://2020.luxtravel.in/luxury-travel-fr/tour-item/le-meilleur-de-lheritage-culturel-du-vietnam-11-jours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-06-29 14:49:20',
                'updated_at' => '2019-06-29 14:49:20',
            ),
            5 => 
            array (
                'id' => 11,
                'from' => '/vietnam-tour-2018/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:46:08',
                'updated_at' => '2019-07-01 14:46:08',
            ),
            6 => 
            array (
                'id' => 12,
                'from' => '/vietnam-tours-2019/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:46:29',
                'updated_at' => '2019-07-01 14:46:29',
            ),
            7 => 
            array (
                'id' => 13,
                'from' => '/vietnam-tours/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:51:23',
                'updated_at' => '2019-07-01 14:51:23',
            ),
            8 => 
            array (
                'id' => 14,
                'from' => '/vietnam-travel-guide/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:51:56',
                'updated_at' => '2019-07-01 14:51:56',
            ),
            9 => 
            array (
                'id' => 15,
                'from' => '/laos-tours/',
                'to' => 'https://luxurytravelvietnam.com/destinations/laos',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:55:41',
                'updated_at' => '2019-07-01 14:55:51',
            ),
            10 => 
            array (
                'id' => 16,
                'from' => '/laos-travel-guide/',
                'to' => 'https://luxurytravelvietnam.com/destinations/laos',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:56:18',
                'updated_at' => '2019-07-01 14:56:18',
            ),
            11 => 
            array (
                'id' => 17,
                'from' => '/cambodia-tours/',
                'to' => 'https://luxurytravelvietnam.com/destinations/cambodia',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:57:13',
                'updated_at' => '2019-07-01 14:57:13',
            ),
            12 => 
            array (
                'id' => 18,
                'from' => '/cambodia-travel-guide/',
                'to' => 'https://luxurytravelvietnam.com/destinations/cambodia',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:57:32',
                'updated_at' => '2019-07-01 14:57:32',
            ),
            13 => 
            array (
                'id' => 19,
                'from' => '/thailand-tours/',
                'to' => 'https://luxurytravelvietnam.com/destinations/thailand',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:58:10',
                'updated_at' => '2019-07-01 14:58:10',
            ),
            14 => 
            array (
                'id' => 20,
                'from' => '/thailand-travel-guide/',
                'to' => 'https://luxurytravelvietnam.com/destinations/thailand',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:59:09',
                'updated_at' => '2019-07-01 14:59:09',
            ),
            15 => 
            array (
                'id' => 21,
                'from' => '/myanmar-tours/',
                'to' => 'https://luxurytravelvietnam.com/destinations/myanmar',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 14:59:54',
                'updated_at' => '2019-07-01 14:59:54',
            ),
            16 => 
            array (
                'id' => 22,
                'from' => '/myanmar-travel-guide/',
                'to' => 'https://luxurytravelvietnam.com/destinations/myanmar',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:00:19',
                'updated_at' => '2019-07-01 15:00:19',
            ),
            17 => 
            array (
                'id' => 23,
                'from' => '/wow-services/',
                'to' => 'https://luxurytravelvietnam.com/wow-services.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:16:47',
                'updated_at' => '2019-07-01 15:16:47',
            ),
            18 => 
            array (
                'id' => 24,
                'from' => '/privacy-policies/',
                'to' => 'https://luxurytravelvietnam.com/privacy-policies.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:17:30',
                'updated_at' => '2019-07-01 15:17:30',
            ),
            19 => 
            array (
                'id' => 25,
                'from' => '/our-awards/',
                'to' => 'https://luxurytravelvietnam.com/our-awards.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:17:59',
                'updated_at' => '2019-07-01 15:17:59',
            ),
            20 => 
            array (
                'id' => 26,
                'from' => '/logo-meaning/',
                'to' => 'https://luxurytravelvietnam.com/logo-meaning.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:18:37',
                'updated_at' => '2019-07-01 15:18:37',
            ),
            21 => 
            array (
                'id' => 27,
                'from' => '/faqs/',
                'to' => 'https://luxurytravelvietnam.com/faqs.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:19:15',
                'updated_at' => '2019-07-01 15:19:15',
            ),
            22 => 
            array (
                'id' => 28,
                'from' => '/core-values/',
                'to' => 'https://luxurytravelvietnam.com/core-values.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:20:37',
                'updated_at' => '2019-07-01 15:20:37',
            ),
            23 => 
            array (
                'id' => 29,
                'from' => '/contact-us/',
                'to' => 'https://luxurytravelvietnam.com/contact',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:23:03',
                'updated_at' => '2019-07-01 15:23:03',
            ),
            24 => 
            array (
                'id' => 30,
                'from' => '/payment-policies/',
                'to' => 'https://luxurytravelvietnam.com/payment-policies.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:24:10',
                'updated_at' => '2019-07-01 15:24:10',
            ),
            25 => 
            array (
                'id' => 31,
                'from' => '/about-us/',
                'to' => 'https://luxurytravelvietnam.com/about-us',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:25:39',
                'updated_at' => '2019-07-01 15:25:39',
            ),
            26 => 
            array (
                'id' => 32,
                'from' => '/adventure-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/adventure-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:26:45',
                'updated_at' => '2019-07-01 15:26:45',
            ),
            27 => 
            array (
                'id' => 33,
                'from' => '/beach-vacation/',
                'to' => 'https://luxurytravelvietnam.com/activities/vietnam-beach-vacation',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:27:32',
                'updated_at' => '2019-07-01 15:27:32',
            ),
            28 => 
            array (
                'id' => 34,
                'from' => '/biking-trekking/',
                'to' => 'https://luxurytravelvietnam.com/activities/biking-trekking',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:28:16',
                'updated_at' => '2019-07-01 15:28:16',
            ),
            29 => 
            array (
                'id' => 35,
                'from' => '/booking-guide/',
                'to' => 'https://luxurytravelvietnam.com/payment-policies.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:29:21',
                'updated_at' => '2019-07-01 15:29:21',
            ),
            30 => 
            array (
                'id' => 36,
                'from' => '/career/',
                'to' => 'https://luxurytravelvietnam.com/career.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-01 15:36:04',
                'updated_at' => '2019-07-01 15:36:04',
            ),
            31 => 
            array (
                'id' => 37,
                'from' => '/ambienonline/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => NULL,
                'created_at' => '2019-07-02 09:16:18',
                'updated_at' => '2019-08-22 12:04:14',
            ),
            32 => 
            array (
                'id' => 38,
                'from' => '/component/bookpro/tour/341-best-of-vietnam-and-cambodia-14-days.html',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:19:05',
                'updated_at' => '2019-07-02 09:19:05',
            ),
            33 => 
            array (
                'id' => 39,
                'from' => '/ssl/book_tour.php',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:19:26',
                'updated_at' => '2019-07-02 09:19:26',
            ),
            34 => 
            array (
                'id' => 40,
                'from' => '/vietnam-luxury-vacation-packages',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:19:46',
                'updated_at' => '2019-07-02 09:19:46',
            ),
            35 => 
            array (
                'id' => 41,
                'from' => '/Vietnamhotels/hoianresort/Villa_Hoian_Beach_Resort_Vietnam.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:20:18',
                'updated_at' => '2019-07-02 09:20:18',
            ),
            36 => 
            array (
                'id' => 42,
                'from' => '/vietnam-tour-holiday-packages',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:20:39',
                'updated_at' => '2019-07-02 09:20:39',
            ),
            37 => 
            array (
                'id' => 43,
                'from' => '/Vietnamhotels/hiltonhanoi/deluxe-room-hilton-hanoi-opera-hotel.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:21:17',
                'updated_at' => '2019-07-02 09:21:17',
            ),
            38 => 
            array (
                'id' => 44,
                'from' => '/Vietnamhotels/hiltonhanoi/suites-room-hilton-hanoi-opera-hotel.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:21:33',
                'updated_at' => '2019-07-02 09:21:33',
            ),
            39 => 
            array (
                'id' => 45,
                'from' => '/Vietnamhotels/The_NamHai_Resort_China_Beach_Hoian/index.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:22:11',
                'updated_at' => '2019-07-02 09:22:11',
            ),
            40 => 
            array (
                'id' => 46,
                'from' => '/destination/travelguide/230-vietnam.html',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:22:37',
                'updated_at' => '2019-07-02 09:22:37',
            ),
            41 => 
            array (
                'id' => 47,
                'from' => '/Vietnamhotels/Evason_Hideway_Sixsense_Condao/index.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:23:09',
                'updated_at' => '2019-07-02 09:23:09',
            ),
            42 => 
            array (
                'id' => 48,
                'from' => '/vietnam-family-vacation-tours',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:24:36',
                'updated_at' => '2019-07-02 09:24:36',
            ),
            43 => 
            array (
                'id' => 49,
                'from' => '/hiltonhanoi/executive-room-hilton-hanoi-opera-hotel.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:24:57',
                'updated_at' => '2019-07-02 09:24:57',
            ),
            44 => 
            array (
                'id' => 50,
                'from' => '/vietnam-cruises',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:25:36',
                'updated_at' => '2019-07-02 09:25:36',
            ),
            45 => 
            array (
                'id' => 51,
                'from' => '/vietnam-honeymoon-tours',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:25:56',
                'updated_at' => '2019-07-02 09:25:56',
            ),
            46 => 
            array (
                'id' => 52,
                'from' => '/FR/index.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:26:19',
                'updated_at' => '2019-07-02 09:26:19',
            ),
            47 => 
            array (
                'id' => 53,
                'from' => '/beach-vietnam-vacation-packages',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:26:33',
                'updated_at' => '2019-07-02 09:26:33',
            ),
            48 => 
            array (
                'id' => 54,
                'from' => '/de/index.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:27:27',
                'updated_at' => '2019-08-13 17:03:33',
            ),
            49 => 
            array (
                'id' => 55,
                'from' => '/thailand-luxury-travel-holidays',
                'to' => 'https://luxurytravelvietnam.com/destinations/thailand',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:28:39',
                'updated_at' => '2019-07-02 09:28:39',
            ),
        ));
        \DB::table('redirects')->insert(array (
            0 => 
            array (
                'id' => 56,
                'from' => '/myanmar-tours-holiday-packages',
                'to' => 'https://luxurytravelvietnam.com/destinations/myanmar',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 09:29:20',
                'updated_at' => '2019-07-02 09:29:20',
            ),
            1 => 
            array (
                'id' => 57,
                'from' => '/culinary-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/culinary-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:02:03',
                'updated_at' => '2019-07-02 11:02:03',
            ),
            2 => 
            array (
                'id' => 58,
                'from' => '/set-departure-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/set-departure',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:04:53',
                'updated_at' => '2019-07-02 11:04:53',
            ),
            3 => 
            array (
                'id' => 59,
                'from' => '/day-trips-from-danang/',
                'to' => 'https://luxurytravelvietnam.com/activities/day-trips',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:06:11',
                'updated_at' => '2019-07-02 11:06:11',
            ),
            4 => 
            array (
                'id' => 60,
                'from' => '/day-trips-from-hanoi/',
                'to' => 'https://luxurytravelvietnam.com/activities/day-trips',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:06:33',
                'updated_at' => '2019-07-02 11:06:33',
            ),
            5 => 
            array (
                'id' => 61,
                'from' => '/day-trips-from-ho-chi-minh/',
                'to' => 'https://luxurytravelvietnam.com/activities/day-trips',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:06:53',
                'updated_at' => '2019-07-02 11:06:53',
            ),
            6 => 
            array (
                'id' => 62,
                'from' => '/day-trips-from-nha-trang/',
                'to' => 'https://luxurytravelvietnam.com/activities/day-trips',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:07:18',
                'updated_at' => '2019-07-02 11:07:18',
            ),
            7 => 
            array (
                'id' => 63,
                'from' => '/day-trips/',
                'to' => 'https://luxurytravelvietnam.com/activities/day-trips',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:07:46',
                'updated_at' => '2019-07-02 11:07:46',
            ),
            8 => 
            array (
                'id' => 64,
                'from' => '/excursion/',
                'to' => 'https://luxurytravelvietnam.com/activities/excursion-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:09:35',
                'updated_at' => '2019-07-02 11:09:35',
            ),
            9 => 
            array (
                'id' => 65,
                'from' => '/excursion-from-ho-chi-minh/',
                'to' => 'https://luxurytravelvietnam.com/activities/excursion-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:10:01',
                'updated_at' => '2019-07-02 11:10:01',
            ),
            10 => 
            array (
                'id' => 66,
                'from' => '/excursion-from-hanoi/',
                'to' => 'https://luxurytravelvietnam.com/activities/excursion-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:10:25',
                'updated_at' => '2019-07-02 11:10:25',
            ),
            11 => 
            array (
                'id' => 67,
                'from' => '/excursion-from-danang/',
                'to' => 'https://luxurytravelvietnam.com/activities/excursion-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:10:55',
                'updated_at' => '2019-07-02 11:10:55',
            ),
            12 => 
            array (
                'id' => 68,
                'from' => '/northern-stopover/',
                'to' => 'https://luxurytravelvietnam.com/activities/stopover-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:12:19',
                'updated_at' => '2019-07-02 11:12:19',
            ),
            13 => 
            array (
                'id' => 69,
                'from' => '/southern-stopover/',
                'to' => 'https://luxurytravelvietnam.com/activities/stopover-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:12:45',
                'updated_at' => '2019-07-02 11:12:45',
            ),
            14 => 
            array (
                'id' => 70,
                'from' => '/stopover-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/stopover-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:13:06',
                'updated_at' => '2019-07-02 11:13:06',
            ),
            15 => 
            array (
                'id' => 71,
                'from' => '/centre-stopover/',
                'to' => 'https://luxurytravelvietnam.com/activities/stopover-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:13:28',
                'updated_at' => '2019-07-02 11:13:28',
            ),
            16 => 
            array (
                'id' => 72,
                'from' => '/golf-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/golf-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:14:16',
                'updated_at' => '2019-07-02 11:14:16',
            ),
            17 => 
            array (
                'id' => 73,
                'from' => '/honeymoon-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/honeymoon-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:15:14',
                'updated_at' => '2019-07-02 11:15:14',
            ),
            18 => 
            array (
                'id' => 74,
                'from' => '/wellness-spa/',
                'to' => 'https://luxurytravelvietnam.com/activities/ultra-luxury-wellness-spa-packages',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:17:04',
                'updated_at' => '2019-07-02 11:17:04',
            ),
            19 => 
            array (
                'id' => 75,
                'from' => '/family-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/family-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:19:00',
                'updated_at' => '2019-07-02 11:19:00',
            ),
            20 => 
            array (
                'id' => 76,
                'from' => '/culture-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/culture-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:19:39',
                'updated_at' => '2019-07-02 11:19:39',
            ),
            21 => 
            array (
                'id' => 77,
                'from' => '/halong-cruises/',
                'to' => 'https://luxurytravelvietnam.com/activities/luxury-cruise-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:20:42',
                'updated_at' => '2019-07-02 11:20:42',
            ),
            22 => 
            array (
                'id' => 78,
                'from' => '/mekong-cruises/',
                'to' => 'https://luxurytravelvietnam.com/activities/vietnam-river-cruise',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:21:22',
                'updated_at' => '2019-07-02 11:21:22',
            ),
            23 => 
            array (
                'id' => 79,
                'from' => '/cruises-tours/',
                'to' => 'https://luxurytravelvietnam.com/activities/luxury-cruise-tours',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:22:41',
                'updated_at' => '2019-07-02 11:22:41',
            ),
            24 => 
            array (
                'id' => 80,
                'from' => '/summer-tour-2018/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:25:10',
                'updated_at' => '2019-07-02 11:25:10',
            ),
            25 => 
            array (
                'id' => 81,
                'from' => '/tour-list/',
                'to' => 'https://luxurytravelvietnam.com/activities',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:27:30',
                'updated_at' => '2019-07-02 11:27:30',
            ),
            26 => 
            array (
                'id' => 82,
                'from' => '/vietnam-cambodia-tour-package/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam-cambodia',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:28:33',
                'updated_at' => '2019-07-02 11:28:33',
            ),
            27 => 
            array (
                'id' => 83,
                'from' => '/visa-to-cambodia/',
                'to' => 'https://luxurytravelvietnam.com/visa-to-cambodia.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:32:23',
                'updated_at' => '2019-07-02 11:32:23',
            ),
            28 => 
            array (
                'id' => 84,
                'from' => '/visa-to-laos/',
                'to' => 'https://luxurytravelvietnam.com/visa-to-laos.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:33:22',
                'updated_at' => '2019-07-02 11:33:22',
            ),
            29 => 
            array (
                'id' => 85,
                'from' => '/visa-to-myanmar/',
                'to' => 'https://luxurytravelvietnam.com/visa-to-myanmar.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:33:52',
                'updated_at' => '2019-07-02 11:33:52',
            ),
            30 => 
            array (
                'id' => 86,
                'from' => '/visa-to-thailand/',
                'to' => 'https://luxurytravelvietnam.com/visa-to-thailand.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:34:08',
                'updated_at' => '2019-07-02 11:34:08',
            ),
            31 => 
            array (
                'id' => 87,
                'from' => '/visa-to-vietnam/',
                'to' => 'https://luxurytravelvietnam.com/visa-to-vietnam.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:34:26',
                'updated_at' => '2019-07-02 11:34:26',
            ),
            32 => 
            array (
                'id' => 88,
                'from' => '/why-travel-with-us/',
                'to' => 'https://luxurytravelvietnam.com/about-us',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:38:36',
                'updated_at' => '2019-07-02 11:38:36',
            ),
            33 => 
            array (
                'id' => 89,
                'from' => '/who-we-are/',
                'to' => 'https://luxurytravelvietnam.com/about-us',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:38:57',
                'updated_at' => '2019-07-02 11:38:57',
            ),
            34 => 
            array (
                'id' => 90,
                'from' => '/meet-our-team/',
                'to' => 'https://luxurytravelvietnam.com/about-us',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-02 11:40:15',
                'updated_at' => '2019-07-02 11:40:15',
            ),
            35 => 
            array (
                'id' => 91,
                'from' => '/EN/thailand/general-information/visa-thailand.htm',
                'to' => 'https://luxurytravelvietnam.com/visa-to-thailand.html',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:18:50',
                'updated_at' => '2019-07-07 20:18:50',
            ),
            36 => 
            array (
                'id' => 92,
                'from' => '/EN/Vietnam_tours/VietnamTrain/VictoriaTrainExprss.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:19:11',
                'updated_at' => '2019-07-07 20:19:11',
            ),
            37 => 
            array (
                'id' => 93,
                'from' => '/EN/GE/Career_Luxury_Travel.htm',
                'to' => 'https://luxurytravelvietnam.com',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:19:41',
                'updated_at' => '2019-07-07 20:19:41',
            ),
            38 => 
            array (
                'id' => 94,
                'from' => '/plan-your-trip-now/',
                'to' => 'https://luxurytravelvietnam.com/inquire',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:21:03',
                'updated_at' => '2019-07-07 20:21:03',
            ),
            39 => 
            array (
                'id' => 95,
                'from' => '/multi-countries-tours/',
                'to' => 'https://luxurytravelvietnam.com/destinations/multi-country',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:21:33',
                'updated_at' => '2019-07-07 20:21:33',
            ),
            40 => 
            array (
                'id' => 96,
                'from' => '/contact.php',
                'to' => 'https://luxurytravelvietnam.com/contact',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:22:13',
                'updated_at' => '2019-07-07 20:22:13',
            ),
            41 => 
            array (
                'id' => 97,
                'from' => '/EN/Vietnam_tours/VietnamTrain/vietnamtrain.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:22:32',
                'updated_at' => '2019-07-07 20:22:32',
            ),
            42 => 
            array (
                'id' => 98,
                'from' => '/category/travel/exotic-vacations/',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:23:11',
                'updated_at' => '2019-07-07 20:23:11',
            ),
            43 => 
            array (
                'id' => 99,
                'from' => '/DE/Vietnam_reisen/Kreuzfahrten/Bai_Tho_Victory_Cruise_3Tage.htm',
                'to' => 'https://de.luxurytravelvietnam.com/tour-item/bai-tho-victory-star-2-kabinen-dschunke-2-tage',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:23:57',
                'updated_at' => '2019-07-07 20:23:57',
            ),
            44 => 
            array (
                'id' => 100,
                'from' => '/EN/GE/our_team.htm',
                'to' => 'https://luxurytravelvietnam.com/about-us',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:24:15',
                'updated_at' => '2019-07-07 20:24:15',
            ),
            45 => 
            array (
                'id' => 101,
                'from' => '/EN/GE/whyus.htm',
                'to' => 'https://luxurytravelvietnam.com/about-us',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:24:32',
                'updated_at' => '2019-07-07 20:24:32',
            ),
            46 => 
            array (
                'id' => 102,
                'from' => '/EN/thailand/hotels/the-chedi-resort-phuket-island/index.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/thailand',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:24:51',
                'updated_at' => '2019-07-07 20:24:51',
            ),
            47 => 
            array (
                'id' => 103,
                'from' => 'island-life-experience-for-4-days-in-cat-ba-islan',
                'to' => 'https://luxurytravelvietnam.com/tour-item/island-life-experience-for-4-days-in-cat-ba-island',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:25:54',
                'updated_at' => '2019-07-07 20:25:54',
            ),
            48 => 
            array (
                'id' => 104,
                'from' => '/EN/Vietnam_tours/Adventures/Kayak_Catba_Island_and_Halong_4days.htm',
                'to' => 'https://luxurytravelvietnam.com/tour-item/island-life-experience-for-4-days-in-cat-ba-island',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:26:11',
                'updated_at' => '2019-07-07 20:26:11',
            ),
            49 => 
            array (
                'id' => 105,
                'from' => '/EN/Vietnam_tours/Attractions/phuquoc.htm',
                'to' => 'https://luxurytravelvietnam.com/tour-item/phu-quoc-island-escape-4-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:26:51',
                'updated_at' => '2019-07-07 20:26:51',
            ),
        ));
        \DB::table('redirects')->insert(array (
            0 => 
            array (
                'id' => 106,
                'from' => '/EN/Vietnam_tours/Set_Departure/Vietnam_Cambodia_Experiences_14days.htm',
                'to' => 'https://luxurytravelvietnam.com/tour-item/best-of-vietnam-and-cambodia-14-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:27:32',
                'updated_at' => '2019-07-07 20:27:32',
            ),
            1 => 
            array (
                'id' => 107,
                'from' => '/EN/Vietnam_tours/Special_Interest_Tours/12_Day_Vietnam_Tour_for_Seniors.htm',
                'to' => 'https://luxurytravelvietnam.com/tour-item/authentic-vietnam-12-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:27:48',
                'updated_at' => '2019-07-07 20:28:26',
            ),
            2 => 
            array (
                'id' => 108,
                'from' => '/EN/Vietnam_tours/Special_Interest_Tours/Follow_the_Battle_Fields_of_the_Vietnam_War_22days.htm',
                'to' => 'https://luxurytravelvietnam.com/tour-item/unforgetable-vietnam-complete-tour-21-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:28:56',
                'updated_at' => '2019-07-07 20:28:56',
            ),
            3 => 
            array (
                'id' => 109,
                'from' => '/EN/Vietnam_tours/Victoriasapapackages/VictoriaSapaPackages.htm',
                'to' => 'https://luxurytravelvietnam.com/tour-item/saigon-spa-package-4-days-3-nights',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:29:49',
                'updated_at' => '2019-07-07 20:29:49',
            ),
            4 => 
            array (
                'id' => 110,
                'from' => '/EN/Vietnam_tours/Vietnamcruises/Bai_Tho_Deluxe_Junk_3days.htm',
                'to' => 'https://luxurytravelvietnam.com/tour-item/bai-tho-2-cabin-deluxe-junk-2-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:30:34',
                'updated_at' => '2019-07-07 20:30:34',
            ),
            5 => 
            array (
                'id' => 111,
                'from' => '/images/destinations/cambodia/mondulkiri/',
                'to' => 'https://luxurytravelvietnam.com/destinations/cambodia',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:30:55',
                'updated_at' => '2019-07-07 20:30:55',
            ),
            6 => 
            array (
                'id' => 112,
                'from' => '/images/Indochinamap.gif',
                'to' => 'https://luxurytravelvietnam.com/destinations/multi-country',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:31:19',
                'updated_at' => '2019-07-07 20:31:19',
            ),
            7 => 
            array (
                'id' => 113,
                'from' => '/index2.htm',
                'to' => 'https://luxurytravelvietnam.com',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:31:29',
                'updated_at' => '2019-07-07 20:31:43',
            ),
            8 => 
            array (
                'id' => 114,
                'from' => '/LaosHotels/anou_hotel_vientiane/index.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/laos',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:32:19',
                'updated_at' => '2019-07-07 20:32:19',
            ),
            9 => 
            array (
                'id' => 115,
                'from' => '/LaosHotels/royal_dokmaideng_hotel_vientiane/',
                'to' => 'https://luxurytravelvietnam.com/destinations/laos',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:32:34',
                'updated_at' => '2019-07-07 20:32:34',
            ),
            10 => 
            array (
                'id' => 116,
                'from' => '/tour-item/exciting-mekong-delta-gateway',
                'to' => 'https://luxurytravelvietnam.com/tour-item/mekong-delta-discovery-in-style-3-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:34:30',
                'updated_at' => '2019-07-16 09:14:51',
            ),
            11 => 
            array (
                'id' => 117,
                'from' => '/tour-item/best-of-indochina-21-days/',
                'to' => 'https://luxurytravelvietnam.com/tour-item/indochina-revealed-20-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-07 20:35:33',
                'updated_at' => '2019-07-16 09:30:54',
            ),
            12 => 
            array (
                'id' => 120,
                'from' => '/best-of-indochina-tour-16-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/best-of-vietnam-cambodia-laos-tour-16-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-19 11:16:18',
                'updated_at' => '2019-07-19 11:24:44',
            ),
            13 => 
            array (
                'id' => 121,
                'from' => '/indochina-revealed-20-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/indochina-revealed-20-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-19 11:18:02',
                'updated_at' => '2019-07-19 11:18:18',
            ),
            14 => 
            array (
                'id' => 122,
                'from' => '/treasures-of-indochina-diwali-tour-12-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/treasures-of-vietnam-cambodia-laos-tour-12-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-19 11:22:36',
                'updated_at' => '2019-07-19 11:22:36',
            ),
            15 => 
            array (
                'id' => 123,
                'from' => '/danang-–-my-khe-beach-free-easy-4-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/danang-my-khe-beach-free-easy-4-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-07-24 15:32:23',
                'updated_at' => '2019-07-24 15:32:23',
            ),
            16 => 
            array (
                'id' => 124,
                'from' => '/EN/Vietnam_tours/Set_Departure/Vietnam_Experience_in_12days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:15:50',
                'updated_at' => '2019-08-13 16:15:50',
            ),
            17 => 
            array (
                'id' => 125,
                'from' => '/destinations/vietnam',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:16:38',
                'updated_at' => '2019-08-13 16:16:38',
            ),
            18 => 
            array (
                'id' => 126,
                'from' => '/indochina-tour-holidays-packages',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:26:22',
                'updated_at' => '2019-08-13 16:26:22',
            ),
            19 => 
            array (
                'id' => 127,
                'from' => '/special-offers/luxury_villas.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:32:09',
                'updated_at' => '2019-08-13 16:32:09',
            ),
            20 => 
            array (
                'id' => 128,
                'from' => '/special-offers/index2.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:32:19',
                'updated_at' => '2019-08-13 16:32:19',
            ),
            21 => 
            array (
                'id' => 129,
                'from' => '/Vietnamhotels/hiltonhanoi/executive-room-hilton-hanoi-opera-hotel.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:32:50',
                'updated_at' => '2019-08-13 16:32:50',
            ),
            22 => 
            array (
                'id' => 130,
                'from' => '/blog/wp-content/uploads/2009/03/whaleislandnhatrang1.jpg',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:33:19',
                'updated_at' => '2019-08-13 16:33:19',
            ),
            23 => 
            array (
                'id' => 131,
                'from' => '/EN/Vietnam_tours/Vietnam_Stopover_Tours/Best_Of_Northern_Vietnam_7days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:33:29',
                'updated_at' => '2019-08-13 16:33:29',
            ),
            24 => 
            array (
                'id' => 132,
                'from' => '/EN/Vietnam_tours/Culturetours/The-best-of-the-best-Vietnam-Holiday-18days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:33:59',
                'updated_at' => '2019-08-13 16:48:22',
            ),
            25 => 
            array (
                'id' => 133,
                'from' => '/EN/Vietnam_tours/Culturetours/Exotic_Vietnam_in_3_weeks.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:34:15',
                'updated_at' => '2019-08-13 16:34:15',
            ),
            26 => 
            array (
                'id' => 134,
                'from' => '/EN/Vietnam_tours/Culturetours/Vietnam_Luxury_Adventure_Travel_12day.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:34:23',
                'updated_at' => '2019-08-13 16:34:23',
            ),
            27 => 
            array (
                'id' => 135,
                'from' => '/EN/Vietnam_Cambodia/Classic_Vietnam_and_Cambodia_11days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:34:36',
                'updated_at' => '2019-08-13 16:34:36',
            ),
            28 => 
            array (
                'id' => 136,
                'from' => '/EN/Vietnam_tours/Adventures/Discover-the-charm-of-Ninh-Binh-by-Bike-2days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:34:45',
                'updated_at' => '2019-08-13 16:34:45',
            ),
            29 => 
            array (
                'id' => 137,
                'from' => '/EN/Vietnam_tours/Daytrips/Water_Way_to_Cu_Chi_Tunnels.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:34:53',
                'updated_at' => '2019-08-13 16:34:53',
            ),
            30 => 
            array (
                'id' => 138,
                'from' => '/EN/Vietnam_tours/vietnam_golf_tours/Central_Golf_Coast_Tour_of_Vietnam_6days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:35:48',
                'updated_at' => '2019-08-13 16:35:48',
            ),
            31 => 
            array (
                'id' => 139,
                'from' => '/Vietnamhotels/Terracotta_Resort_MuiNe_PhanThiet/index.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:35:58',
                'updated_at' => '2019-08-13 16:35:58',
            ),
            32 => 
            array (
                'id' => 140,
                'from' => '/EN/6_TOURS/indochiana_tours.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:36:07',
                'updated_at' => '2019-08-13 16:36:07',
            ),
            33 => 
            array (
                'id' => 141,
                'from' => '/EN/Vietnam_tours/Culturetours/Discover_The_Hidden_Treasures_of_Vietnam_by_Road_29days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:36:19',
                'updated_at' => '2019-08-13 16:36:19',
            ),
            34 => 
            array (
                'id' => 142,
                'from' => '/Vietnamhotels/HongNgocHanoi/HNHangBac.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:36:29',
                'updated_at' => '2019-08-13 16:36:29',
            ),
            35 => 
            array (
                'id' => 143,
                'from' => '/destination/tours/1/38-cambodia.html',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:36:38',
                'updated_at' => '2019-08-13 16:36:38',
            ),
            36 => 
            array (
                'id' => 144,
                'from' => '/EN/Vietnam_tours/Adventures/Adventure_Holiday_in_Vietnam_18days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:38:34',
                'updated_at' => '2019-08-13 16:38:34',
            ),
            37 => 
            array (
                'id' => 145,
                'from' => '/EN/Vietnam_tours/Adventures/Vietnam_Chic_Adventure_Tour_in_14_days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:38:46',
                'updated_at' => '2019-08-13 16:38:46',
            ),
            38 => 
            array (
                'id' => 146,
                'from' => '/EN/myanmar/packages/Discover_Yangon_in_Depth_5days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:38:54',
                'updated_at' => '2019-08-13 16:38:54',
            ),
            39 => 
            array (
                'id' => 147,
                'from' => '/blog/?tag=vietnam-airlines',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:39:03',
                'updated_at' => '2019-08-13 16:39:03',
            ),
            40 => 
            array (
                'id' => 148,
                'from' => '/blog/laos-tours-picking-your-off-the-beaten-track-destination.html',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:39:11',
                'updated_at' => '2019-08-13 16:39:11',
            ),
            41 => 
            array (
                'id' => 149,
                'from' => '/blog/wp-content/uploads/2008/09/luxury.jpg',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:39:22',
                'updated_at' => '2019-08-13 16:39:22',
            ),
            42 => 
            array (
                'id' => 150,
                'from' => '/EN/Vietnam_Cambodia/Best_of_Vietnam_and_Cambodia_14days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:40:20',
                'updated_at' => '2019-08-13 16:40:20',
            ),
            43 => 
            array (
                'id' => 151,
                'from' => '/EN/Vietnam_tours/Adventures/The_Mixing_of_Adventure_Heritage_Sites_Visit_in_Central_Vietnam_10days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:40:36',
                'updated_at' => '2019-08-13 16:40:36',
            ),
            44 => 
            array (
                'id' => 152,
                'from' => '/EN/Vietnam_tours/Vietnam_Stopover_Tours/Exotic_Southern_Vietnam_Tour_in_12_days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:40:44',
                'updated_at' => '2019-08-13 16:40:44',
            ),
            45 => 
            array (
                'id' => 153,
                'from' => '/blog/wp-content/uploads/2015/11/thailand-luxury-holiday-972x709.jpg',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:40:57',
                'updated_at' => '2019-08-13 16:40:57',
            ),
            46 => 
            array (
                'id' => 154,
                'from' => '/destination/tours/1/117-laos.html',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:41:06',
                'updated_at' => '2019-08-13 16:41:06',
            ),
            47 => 
            array (
                'id' => 155,
                'from' => '/EN/6_TOURS/vietnam_cambodia_tours.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:41:18',
                'updated_at' => '2019-08-13 16:41:18',
            ),
            48 => 
            array (
                'id' => 156,
                'from' => '/EN/Vietnam_tours/Daytrips/Vung-Tau-Day-Trip.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:41:29',
                'updated_at' => '2019-08-13 16:41:29',
            ),
            49 => 
            array (
                'id' => 157,
                'from' => '/EN/Vietnam_tours/Set_Departure/Vietnam_Cambodia_Experiences_14day',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:41:39',
                'updated_at' => '2019-08-13 16:41:39',
            ),
        ));
        \DB::table('redirects')->insert(array (
            0 => 
            array (
                'id' => 158,
                'from' => '/EN/thailand/hotels/banyan-tree-hotel-and-resort-bangkok/images/one-bedroom-banyan-suite.jpg',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:42:15',
                'updated_at' => '2019-08-13 16:42:15',
            ),
            1 => 
            array (
                'id' => 159,
                'from' => '/EN/GE/special_offers.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:42:25',
                'updated_at' => '2019-08-13 16:42:25',
            ),
            2 => 
            array (
                'id' => 160,
                'from' => '/EN/Vietnam_tours/Daytrips/half_day_co_loa_citadel.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:42:35',
                'updated_at' => '2019-08-13 16:42:35',
            ),
            3 => 
            array (
                'id' => 161,
                'from' => '/blog/laos-travel-and-tour-packages-explore-fascinations-destination.html',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:42:44',
                'updated_at' => '2019-08-13 16:42:44',
            ),
            4 => 
            array (
                'id' => 162,
                'from' => '/blog/sihanoukville-to-add-on-your-tour-packages-cambodia.html',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:42:55',
                'updated_at' => '2019-08-13 16:42:55',
            ),
            5 => 
            array (
                'id' => 163,
                'from' => '/blog/wp-content/uploads/2008/07/sheratonasiapacific.jpg',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:43:03',
                'updated_at' => '2019-08-13 16:43:03',
            ),
            6 => 
            array (
                'id' => 165,
                'from' => '/EN/GE/agent_zone_login.php',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:43:34',
                'updated_at' => '2019-08-13 16:48:15',
            ),
            7 => 
            array (
                'id' => 166,
                'from' => '/EN/Laos_tours/Discover_Laos_in_Style_10_days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:43:41',
                'updated_at' => '2019-08-13 16:44:00',
            ),
            8 => 
            array (
                'id' => 167,
                'from' => '/EN/Vietnam_tours/Adventures/Cycling-Trip-To-Mai-Chau-2days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:43:52',
                'updated_at' => '2019-08-13 16:43:52',
            ),
            9 => 
            array (
                'id' => 168,
                'from' => '/Vietnamhotels/tuy-hoa.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:44:17',
                'updated_at' => '2019-08-13 16:44:17',
            ),
            10 => 
            array (
                'id' => 169,
                'from' => '/EN/6_TOURS/laos_tours.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:44:28',
                'updated_at' => '2019-08-13 16:44:28',
            ),
            11 => 
            array (
                'id' => 170,
                'from' => '/EN/GE/booking_guide.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:44:35',
                'updated_at' => '2019-08-13 16:44:35',
            ),
            12 => 
            array (
                'id' => 171,
                'from' => '/EN/Vietnam_tours/Adventures/The_Charm_Natural_Beauty_of_North_Eastern_Vietnam_9_days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:44:44',
                'updated_at' => '2019-08-13 16:44:44',
            ),
            13 => 
            array (
                'id' => 172,
                'from' => '/EN/Vietnam_tours/Excursions/Saigon/Nam_Cat_Tien_2days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:44:54',
                'updated_at' => '2019-08-13 16:44:54',
            ),
            14 => 
            array (
                'id' => 173,
                'from' => '/EN/Vietnam_tours/VietnamCulinaryTours/The_Culture_and_Cuisine_of_Vietnam_9nights.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:45:08',
                'updated_at' => '2019-08-13 16:45:08',
            ),
            15 => 
            array (
                'id' => 174,
                'from' => '/EN/Vietnam_tours/Vietnam_Stopover_Tours/Vietnam_Escape_5days_4nights.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:45:20',
                'updated_at' => '2019-08-13 16:45:20',
            ),
            16 => 
            array (
                'id' => 175,
                'from' => '/EN/myanmar/packages/Discover-The-Charm-Of-Myanmar-12days.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:45:29',
                'updated_at' => '2019-08-13 16:45:29',
            ),
            17 => 
            array (
                'id' => 176,
                'from' => '/EN/myanmar/packages/luxuries-and-mysteries-of-myanmar.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:45:45',
                'updated_at' => '2019-08-13 16:45:45',
            ),
            18 => 
            array (
                'id' => 177,
                'from' => '/Vietnamhotels/meliahanoi/Deluxe_Room.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:46:23',
                'updated_at' => '2019-08-13 16:46:23',
            ),
            19 => 
            array (
                'id' => 178,
                'from' => '/Vietnamhotels/meliahanoi/Executive_Suite.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:46:34',
                'updated_at' => '2019-08-13 16:46:34',
            ),
            20 => 
            array (
                'id' => 179,
                'from' => '/Vietnamhotels/meliahanoi/Presidential_Suite.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:46:40',
                'updated_at' => '2019-08-13 16:46:40',
            ),
            21 => 
            array (
                'id' => 180,
                'from' => '/Vietnamhotels/victoriasapa/Victoria_Family_Studio.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:46:47',
                'updated_at' => '2019-08-13 16:46:47',
            ),
            22 => 
            array (
                'id' => 181,
                'from' => '/Vietnamhotels/victoriasapa/Victoria_Suite.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:46:58',
                'updated_at' => '2019-08-13 16:46:58',
            ),
            23 => 
            array (
                'id' => 182,
                'from' => '/Vietnamhotels/victoriasapa/Victoria_Superior_Room.htm',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 16:47:06',
                'updated_at' => '2019-08-13 16:47:06',
            ),
            24 => 
            array (
                'id' => 183,
                'from' => '/blog/www.ganeshindianrestaurant.com',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-08-13 17:04:49',
                'updated_at' => '2019-08-13 17:04:49',
            ),
            25 => 
            array (
                'id' => 184,
                'from' => '/tour-evaluation',
                'to' => 'https://docs.google.com/forms/d/e/1FAIpQLSe-mAVIba7sK40ayJ22KMnhp7QiUh1KLiIEe7ONhyzBVz0BPw/viewform',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-09-19 09:51:25',
                'updated_at' => '2019-09-19 09:51:25',
            ),
            26 => 
            array (
                'id' => 185,
                'from' => '/tour-item/indochina-discovery-from-hanoi-7-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/exotic-vietnam-laos-cambodia-tour-7-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-09-19 13:39:52',
                'updated_at' => '2019-09-19 13:52:49',
            ),
            27 => 
            array (
                'id' => 186,
                'from' => '/tour-item/world-heritage-sites-discovery-and-superb-food-and-cultural-immersion-in-indochina-16-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/laos-vietnam-cambodia-tour-16-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-09-19 13:43:53',
                'updated_at' => '2019-09-19 13:50:07',
            ),
            28 => 
            array (
                'id' => 187,
                'from' => '/tour-item/essential-indochina-holiday-16-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/essential-cambodia-laos-vietnam-holidays-16-days',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-09-19 13:48:05',
                'updated_at' => '2019-09-19 13:52:54',
            ),
            29 => 
            array (
                'id' => 188,
                'from' => '/tour-item/best-of-vietnam-cambodia-tour-20-days',
                'to' => 'https://luxurytravelvietnam.com/tour-item/best-of-vietnam-cambodia-tour-3-week-itinerary',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-09-26 09:47:30',
                'updated_at' => '2019-09-26 09:47:30',
            ),
            30 => 
            array (
                'id' => 189,
                'from' => '/index.php/destinations/vietnam/vietnam-cambodia-laos-tour',
                'to' => 'https://luxurytravelvietnam.com/destinations/vietnam/vietnam-cambodia-laos-tour',
                'type' => NULL,
                'status' => 1,
                'created_at' => '2019-09-27 12:18:21',
                'updated_at' => '2019-09-27 12:18:21',
            ),
        ));
        
        
    }
}