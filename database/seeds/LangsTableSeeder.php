<?php

use Illuminate\Database\Seeder;

class LangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('langs')->delete();
        
        \DB::table('langs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'English',
                'locale' => 'en',
                'link' => 'https://luxurytravelvietnam.com/',
                'created_at' => NULL,
                'updated_at' => '2019-07-07 20:06:56',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'French',
                'locale' => 'fr',
                'link' => 'https://fr.luxurytravelvietnam.com/',
                'created_at' => NULL,
                'updated_at' => '2019-07-07 20:06:56',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'German',
                'locale' => 'de',
                'link' => 'https://de.luxurytravelvietnam.com/',
                'created_at' => NULL,
                'updated_at' => '2019-07-07 20:06:56',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Spanish',
                'locale' => 'es',
                'link' => 'https://es.luxurytravelvietnam.com/',
                'created_at' => NULL,
                'updated_at' => '2019-07-07 20:06:56',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Italian',
                'locale' => 'it',
                'link' => 'https://it.luxurytravelvietnam.com/',
                'created_at' => NULL,
                'updated_at' => '2019-07-07 20:06:56',
            ),
        ));
        
        
    }
}