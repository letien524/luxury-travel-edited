<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191)->nullable();
            $table->string('slug', 191)->nullable();
            $table->string('image', 191)->nullable();
            $table->text('excerpt')->nullable();
            $table->longText('content')->nullable();
            $table->integer('status')->nullable();
            $table->integer('order_menu')->nullable();

            $table->integer('faq_id', false, true)->nullable();
            $table->integer('member_id', false, true)->nullable();
            $table->integer('review_category_id', false, true)->nullable();

            $table->string('canonical', 191)->nullable();
            $table->string('meta_title', 191)->nullable();
            $table->text('meta_description')->nullable();
            $table->integer('index')->nullable()->default(1);

            $table->foreign('faq_id')->references('id')->on('faqs')->onDelete('set null');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('set null');
            $table->foreign('review_category_id')->references('id')->on('review_categories')->onDelete('set null');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_activities');
    }
}
