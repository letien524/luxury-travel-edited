<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelCityTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_city_tour', function (Blueprint $table) {
            $table->integer('hotel_city_id', false, true);
            $table->integer('tour_id', false, true);
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
            $table->foreign('hotel_city_id')->references('id')->on('hotel_cities')->onDelete('cascade');
            $table->primary(['hotel_city_id','tour_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_city_tour');
    }
}
