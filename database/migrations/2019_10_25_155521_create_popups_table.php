<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->string('image',255)->nullable();
            $table->string('link',255)->nullable();
            $table->text('content')->nullable();
            $table->string('button_name_display', 255)->nullable();
            $table->integer('delay_time')->nullable();
            $table->text('black_list')->nullable();
            $table->integer('is_display')->default(0);
            $table->integer('is_active')->default(0);
            $table->string('type')->nullable();
            $table->integer('order_menu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popups');
    }
}
