<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestinationTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination_tour', function (Blueprint $table) {
            $table->integer('destination_id', false, true);
            $table->integer('tour_id', false, true);
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
            $table->foreign('destination_id')->references('id')->on('tour_destinations')->onDelete('cascade');
            $table->primary(['destination_id','tour_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destination_tour');
    }
}
