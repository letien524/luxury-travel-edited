<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->integer('city_id', false, true)->nullable();
            $table->integer('type_id', false, true)->nullable();
            $table->integer('room_type_id', false, true)->nullable();
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('hotel_cities')->onDelete('set null');
            $table->foreign('type_id')->references('id')->on('hotel_types')->onDelete('set null');
            $table->foreign('room_type_id')->references('id')->on('hotel_room_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
