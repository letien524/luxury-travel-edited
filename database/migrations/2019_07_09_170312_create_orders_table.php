<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('email', 191)->nullable();
			$table->string('phone', 191)->nullable();
			$table->integer('country_id',false, true)->nullable();
			$table->string('activity', 191)->nullable();
			$table->string('hotel', 191)->nullable();
			$table->string('duration', 191)->nullable();
			$table->integer('number_child')->nullable();
			$table->integer('number_adult')->nullable();
			$table->date('departure_date')->nullable()->default('1970-01-01');
			$table->text('content')->nullable();
			$table->integer('status')->nullable();
			$table->integer('tour_id', false, true)->nullable();
			$table->timestamps();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
