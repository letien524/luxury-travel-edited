<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name')->nullable();
			$table->text('address')->nullable();
			$table->string('phone', 191)->nullable();
			$table->string('hotline', 191)->nullable();
			$table->string('email', 191)->nullable();
			$table->longText('content')->nullable();
			$table->string('type', 191)->nullable();
			$table->integer('status')->nullable();
			$table->integer('order_menu')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
