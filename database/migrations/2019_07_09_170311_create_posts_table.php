<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('slug', 191)->nullable();
			$table->string('image', 191)->nullable();
			$table->text('content_short')->nullable();
			$table->longText('content')->nullable();
			$table->string('type', 191)->nullable();
			$table->integer('status')->nullable();
			$table->integer('lang_id')->unsigned()->nullable()->index('posts_lang_id_foreign');
			$table->integer('parent_id')->nullable();
			$table->integer('order_menu')->nullable();
			$table->string('meta_title', 191)->nullable();
			$table->string('meta_description', 191)->nullable();
			$table->timestamps();
			$table->integer('index')->nullable()->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
