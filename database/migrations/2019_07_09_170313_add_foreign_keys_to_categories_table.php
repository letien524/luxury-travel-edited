<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories', function(Blueprint $table)
		{
			$table->foreign('faq_id')->references('id')->on('faqs')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('lang_id')->references('id')->on('langs')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('member_id')->references('id')->on('members')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categories', function(Blueprint $table)
		{
			$table->dropForeign('categories_faq_id_foreign');
			$table->dropForeign('categories_lang_id_foreign');
			$table->dropForeign('categories_member_id_foreign');
		});
	}

}
