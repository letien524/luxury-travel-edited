<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateToursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tours', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('slug', 191)->nullable();
			$table->string('image', 191)->nullable();
			$table->integer('duration')->nullable();
			$table->text('excerpt')->nullable();
			$table->longText('content')->nullable();
			$table->longText('map')->nullable();
			$table->longText('plan')->nullable();
			$table->longText('hotel')->nullable();
			$table->string('file', 191)->nullable();
			$table->integer('status')->nullable();
			$table->integer('highlight')->nullable();
			$table->integer('parent_id')->nullable();
			$table->integer('lang_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('faq_id', false, true)->nullable();
			$table->integer('destination_id')->nullable();
			$table->integer('price')->nullable();
			$table->integer('price_promotion')->nullable();
			$table->string('meta_title', 191)->nullable();
			$table->text('meta_description')->nullable();
			$table->string('canonical', 191)->nullable();
			$table->integer('index')->nullable()->default(1);
            $table->timestamps();
            $table->foreign('faq_id')->references('id')->on('faqs')->onDelete('set null');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tours');
	}

}
