<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_destinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191)->nullable();
            $table->string('slug', 191)->nullable();
            $table->string('image', 191)->nullable();
            $table->text('excerpt')->nullable();
            $table->longText('content')->nullable();
            $table->string('parent_id', 191)->nullable();
            $table->string('meta_title', 191)->nullable();
            $table->string('meta_description', 191)->nullable();

            $table->integer('faq_id', false, true)->nullable();
            $table->integer('member_id', false, true)->nullable();
            $table->integer('review_category_id', false, true)->nullable();

            $table->integer('status')->nullable();
            $table->integer('is_best')->nullable();
            $table->integer('is_inquire')->nullable();
            $table->integer('order_menu')->nullable();
            $table->string('canonical', 191)->nullable();
            $table->integer('index')->nullable()->default(1);

            $table->timestamps();

            $table->foreign('faq_id')->references('id')->on('faqs')->onDelete('set null');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('set null');
            $table->foreign('review_category_id')->references('id')->on('review_categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_destinations');
    }
}
