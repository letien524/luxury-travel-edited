<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_tag', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id')->unsigned()->index('blog_tag_post_id_foreign');
			$table->integer('category_id')->unsigned()->index('blog_tag_category_id_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_tag');
	}

}
