<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191)->nullable();
			$table->string('slug', 191)->nullable();
			$table->string('type', 191)->nullable();
			$table->text('excerpt')->nullable();
			$table->longText('content')->nullable();
			$table->string('parent_id', 191)->nullable();
			$table->string('meta_title', 191)->nullable();
			$table->string('meta_description', 191)->nullable();
			$table->integer('lang_id')->unsigned()->nullable()->index('categories_lang_id_foreign');
			$table->string('image', 191)->nullable();
			$table->integer('status')->nullable();
			$table->integer('is_display')->nullable();
			$table->integer('is_best')->nullable();
			$table->integer('is_inquire')->nullable();
			$table->integer('order_menu')->nullable();

			$table->string('canonical', 191)->nullable();
			$table->integer('index')->nullable()->default(1);
			$table->integer('faq_id')->unsigned()->nullable()->index('categories_faq_id_foreign');
			$table->integer('member_id')->unsigned()->nullable()->index('categories_member_id_foreign');

            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
