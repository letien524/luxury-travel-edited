<!DOCTYPE html>
<html>
<head>
    <title>{{ $tour->name }}</title>
    <style>
        img {
            max-width: 100%;
        }

        .pdf__left {
            width: 25%;
        }

        .pdf__table td {
            vertical-align: baseline;
        }
    </style>
</head>
<body>
<div class="pdf__header">
    <h1>{{ $tour->name }}</h1>
</div>
<div class="pdf__content">

</div>
<?php $plans = json_decode($tour->plan, true); ?>
<table class="pdf__table">
    @foreach($plans as $key => $plan)
        <tr>
            <td class="pdf__left"><h3 class="pdf__name">{!! @$plan['name'] !!}</h3></td>
            <td>
                <div>{!! @$plan['content'] !!}</div>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>
