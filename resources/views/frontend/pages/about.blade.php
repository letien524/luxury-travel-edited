@extends('frontend.master')


@section('content')
    <?php
    $content = json_decode($about->content);
    $section1 = @$content->section1;
    $section2 = @$content->section2;
    $section3 = @$content->section3;
    $section4 = @$content->section4;
    $section5 = @$content->section5;
    $section6 = @$content->section6;
    $section7 = @$content->section7;

    ?>

    <section class="vk-content">

        @if(!empty($section1))
            <div class="vk-banner--style-6" {!! strlen(@$section1->image) ? 'style="background-image: url('.image_url(@$section1->image).')"' : null !!}>
                <div class="container">
                    <div class="vk-banner__content">
                        <div class="_wrapper">
                            <div>
                                <h1 class="_title">
                                    {!! @$section1->name !!}
                                    @if(Auth::check())
                                        <span class="admin_editable">
                                    <a href="{!! route('about.index',$about) !!}" title="Sửa">
                                    <i class="fa fa-pencil"></i>
                                    </a>
                                </span>
                                    @endif
                                </h1>
                                <div class="_text">{!! @$section1->content !!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(!empty($section2))
            <div class="container pt-30 pb-10 pt-lg-60 pb-lg-60">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <h2 class="vk-about__title">{!! @$section2->name !!}</h2>
                        @if(count(@$section2->content))
                            <div class="row">
                                @foreach(@$section2->content as $val)
                                    <div class="col-lg-6">
                                        <div class="vk-about__text">{!! $val !!}</div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif

        @if(!empty($section3))

            <div class="container pb-40">
                <div class="vk-img"><img
                            src="{!! __IMAGE_LAZY_LOAD !!}"
                            data-src="{!! strlen(@$section3->image) ? image_url(@$section3->image) : getAsset('/images/about-2.jpg') !!}"
                            alt=""></div>
            </div>
        @endif

        @if(!empty($section4))
            <div class="vk-about__sell-point">
                <div class="container">
                    <h2 class="vk-about__title--style-1">{!! @$section4->name !!}</h2>

                    @if(!empty(@$section4->content))
                        <div class="vk-about__list--style-1 row">
                            @foreach($section4->content as $val)
                                <div class="_item col-lg-4">
                                    <div class="vk-about-item">
                                        <h2 class="vk-about-item__title">{!! @$val->name !!}</h2>
                                        <div class="vk-about-item__text">{!! @$val->content !!}</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        @endif

        @if(!empty($section5))
            <div class="vk-about__mid">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">

                            <div class="_left">
                                <h2 class="vk-about__title--style-2">{!! $section5->name !!}</h2>
                                <div class="vk-img"><img
                                            src="{!! __IMAGE_LAZY_LOAD !!}"
                                            data-src="{!! strlen(@$section5->image) ? image_url($section5->image) : getAsset('/images/about-4.jpg') !!}"
                                            alt="{!! $section5->name !!}"></div>
                            </div>

                        </div>
                        <div class="col-lg-9">
                            <div class="_right">
                                <?php $content = @$about->faq->content ?>
                                @if(!empty($content))
                                    @include('frontend.components.faq')
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                @if(!empty($section7) && strlen(@$section7->name))
                    <div class="vk-home__about vk-home__about--style-2">
                        <div class="container">
                            <a href="{{strlen(@$section7->link) ? @$section7->link : 'javascript:void(0);'}}" class="vk-btn" data-animation="fadeInUpFix"data-animation-duration="2"><span>{{ $section7->name }}</span></a>
                        </div>
                    </div> <!--./about-->
                @endif

            </div>
        @endif




        @if(!empty($section6))
                <div class="vk-about__team" style="position: relative;">
                    <div class="container">
                        <div class="vk-about__title-box">
                            <h2 class="_title">{!! @$section6->name !!}</h2>
                            <div class="_title-sub">{!! @$section6->content !!}</div>
                        </div>
                        @if($members->count())
                        <div class="vk-about__list row">
                            @foreach($members as $member)
                                @include('frontend.components.member',['member'=>$member])
                            @endforeach
                        </div>
                        @endif
                    </div>
                </div>
        @endif

        @if(!empty($section7) && strlen(@$section7->name))
            <div class="vk-home__about vk-home__about--style-1">
                <div class="container">
                    <a href="{{strlen(@$section7->link) ? @$section7->link : 'javascript:void(0);'}}" class="vk-btn" data-animation="fadeInUpFix"data-animation-duration="2"><span>{{ $section7->name }}</span></a>
                </div>
            </div> <!--./about-->
        @endif

        @include('frontend.components.make_enquiry')

    </section><!--./content-->

@endsection