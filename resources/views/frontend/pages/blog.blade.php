@extends('frontend.master')


@section('content')


    <section class="vk-content">

        <div class="vk-blog__top">

            <h1 class="vk-blog__title">{!! __('Blogs') !!}</h1>

            @if($newestBlogs->count())
                <div class="vk-blog__slider slick-slider vk-slider--style-10" data-slider="blog-main">
                    @foreach($newestBlogs as $blog)
                        <?php $blogClass = 'vk-blog-item--style-5'; ?>
                        @include('frontend.components.blog',['blogWrapper'=>'col-12'])
                    @endforeach
                </div>
            @endif

        </div>

        <div class="container pt-60 pb-60 pt-lg-90 pb-lg-90">
            <div class="row">
                <div class="col-lg-9">
                    <div class="vk-blog__left">
                        @if($blogs->count())
                            @if($blogs->count() <10)

                                <div class="vk-blog__list row">
                                    @foreach($blogs as $blog)
                                        <?php $blogClass = 'vk-blog-item--style-2' ?>
                                        @include('frontend.components.blog')
                                    @endforeach
                                </div>
                            @else
                                <?php
                                $data[0] = $blogs->slice(0, 3);
                                $data[1] = $blogs->slice(3, 4);
                                $data[2] = $blogs->slice(7);

                                ?>

                                <div class="vk-blog__list row">
                                    @foreach($data[0] as $blog)
                                        <?php $blogClass = 'vk-blog-item--style-2' ?>
                                        @include('frontend.components.blog')
                                    @endforeach
                                </div>

                                <div class="vk-blog__mid d-lg-block">
                                    <div class="vk-blog__list row">
                                        @foreach($data[1] as $blog)
                                            <?php $blogClass = 'vk-blog-item--style-3' ?>
                                            @include('frontend.components.blog')
                                        @endforeach
                                    </div>
                                </div>


                                <div class="vk-blog__list row">
                                    @foreach($data[2] as $blog)
                                        <?php $blogClass = 'vk-blog-item--style-2' ?>
                                        @include('frontend.components.blog')
                                    @endforeach
                                </div>
                            @endif

                            <nav class="vk-pagination">{!! $blogs->appends(request()->except('page'))->links('frontend.components.pagination') !!}</nav>
                            <!--./vk-breadcrumb-->

                        @endif

                    </div>
                </div>
                <div class="col-lg-3 ">
                    @include('frontend.components.sidebar')
                </div>
            </div>
        </div>


    </section><!--./content-->




@endsection