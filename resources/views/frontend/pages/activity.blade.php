@extends('frontend.master')
@section('content')

    <?php
    $activityName = $category->name;
    $activityContent = json_decode($category->content);


    //    if ($category->type == 'activity-contact-main') {
    //        $activityName = $category->name;
    //        $activityName = @$activityName->name;
    //    }

    ?>

    <section class="vk-content">
        <div class="vk-banner--style-4" {!! strlen(@$activityContent->banner->top->image) ? 'style="background-image: url('.image_url($activityContent->banner->top->image).')"' : null !!}>
            <div class="container">
                <div class="vk-banner__content">
                    <div class="_wrapper">
                        <div>
                            @if(@$metaConfig['search'] === true && strlen(request('key')))
                                <h1 class="_title">Search result for: “{{ request()->key }}”</h1>
                            @else
                                <h1 class="_title">{!! $activityName !!}</h1>
                                <h2 class="_title-sub">{!! @$activityContent->banner->top->name !!}</h2>
                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>

        @include('frontend.components.filter')
        @if(!isset($metaConfig['search']))
            @if(!empty(@$activityContent->about) || !empty(@$activityContent->name || !empty(@$activityContent->content)))
                <div class="vk-home__about">
                    <div class="container">
                        <div class="vk-home__about-content">
                            <h2 class="vk-home__title">{!! strlen(@$activityContent->about->name) ? @$activityContent->about->name : $activityContent->name !!}</h2>

                            <div class="_text">{!! strlen(@$activityContent->about->content) ? @$activityContent->about->content : $activityContent->content !!}</div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        <div class="container">
            <div class="vk-shop__sort">
                <?php

                $destination = count(@(array)$destinationTargets) ? implode(', ', @$destinationTargets) : __('Destinations');
                $activity = count(@(array)$activityTargets) ? implode(', ', @$activityTargets) : $activityName;
                ?>
                <div class="_left">
                    @if(isset($metaConfig['search']))
                        <span>{!! __('Found :total tours', ['total'=> $tours->total()])  !!}</span>
                        {!! __('in :destination with :activity style',['destination'=> $destination,'activity'=>$activity]) !!}
                    @endif
                </div>

                <div class="_right">
                    <div class="_label">{!! __('Sort by') !!}:</div>
                    <?php $urlFilterValue = explode('tour-filter=', Request::getQueryString()); ?>

                    <div class="vk-shop__list-sort">

                        <a href="{!!  url_query_render('tour-filter')['url'] !!}"
                           class="{!! empty(@$urlFilterValue[1]) ? 'active' : null !!}">{!! __('all') !!}</a>
                        <a href="{!! url_query_render('tour-filter','price')['url'] !!}"
                           class="{!! in_array('price',$urlFilterValue) ? 'active' : null !!}">{!! __('price') !!}</a>
                        <a href="{!! url_query_render('tour-filter','duration')['url'] !!}"
                           class="{!! in_array('duration',$urlFilterValue) ? 'active' : null !!}">{!! __('duration') !!}</a>
                        <a href="{!! url_query_render('tour-filter','best-seller')['url']  !!}"
                           class="{!! in_array('best-seller',$urlFilterValue) ? 'active' : null !!}">{!! __('Best Sellers') !!}</a>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="vk-shop__list row" id="tourList" {!! \App\Enum\ItemScopeEnum::ITEM_TYPE['ItemList'] !!}>
                <?php if(@$category->type == 'tour-category'){
                    $tourActivityActive = true;
                } ?>
                @foreach($tours as $tour)
                    @include('frontend.components.tour')
                @endforeach

                @include('frontend.components.tour-search-item',[
                    'tours' => $tours,
                    'tourSearchConfig' => @$tourSearchConfig,
                ])
            </div>
            <div class="vk-shop__more">
                @if($tours->currentPage() < $tours->lastPage())
                    <a href="javascript:void(0)" rel="nofollow"
                       onclick="loadMoreData(this)"><span>{!! __('View More') !!}</span></a>
                @endif
            </div>

        </div>
        <?php
        $bannerClass = 'vk-banner--style-1';
        $bannerBackground = @$activityContent->banner->bottom->image;
        ?>

        @if(!empty($category->faq))
            @include('frontend.components.tour-guide')
        @endif

        @if(!empty(@$reviews))
            <div class="container pt-60 pb-60">
                <div class="vk-home__title-box" data-animation="fadeInUp" data-animation-duration="2">
                    <h2 class="_title">{!! __('Reviews') !!}</h2>
                </div>

                <div class="vk-blog-detail__comment-list">
                    <div class="vk-comment__list" id="reviewList">
                        @include('frontend.components.review',['reviews'=>$reviews])
                    </div>
                </div>
            </div>
        @endif

        @if(!empty($bestDestinations) && $bestDestinations->count() && 1 === 2)
            <div class="vk-home__shop">
                <div class="">
                    <div class="vk-home__how-content">
                        <div class="vk-home__title-box" data-animation="fadeInUp" data-animation-duration="2">
                            <h2 class="_title">{!! __('Best destinations') !!}</h2>
                        </div>
                        <?php
                        $tourWrapper = 'col-12';
                        $tourClass = 'vk-shop-item--style-5';
                        $tourAnimation = false;
                        ?>
                        <div>
                            <div class="vk-shop__list vk-slider--style-2"
                                 data-animation="fadeIn"
                                 data-slider="tour-detail">
                                @foreach($bestDestinations as $destination)
                                    <div>
                                        <div>
                                            @include('frontend.components.destination')
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>

                </div>
            </div> <!--./tour-->
        @endif

        @include('frontend.components.banner')


    </section><!--./content-->
@endsection

@section('end')
    <script type="text/javascript">
        var pageNumber = parseInt('{{$tours->currentPage() + 1}}');
        var lastPage = parseInt('{!! $tours->lastPage() !!}');
        var url ='{!! url()->full() !!}';

        var prefix = url.indexOf('?') !== -1 ? '&' : '?';

        function loadMoreData(el) {
            $.ajax({
                type: 'GET',
                {{--url: '{!! url_query_render('page',null)['url'] !!}',--}}
                url: url + prefix + 'page=' +pageNumber,
                success: function (data) {
                    $('#tourList').append(data.html);

                    pageNumber++;
                    if (pageNumber === lastPage + 1) {
                        $(el).hide();
                    }
                }, error: function (data) {

                },
            })
        }
    </script>
@endsection
