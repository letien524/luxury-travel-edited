@extends('frontend.master')


@section('content')
    <?php
    $content = json_decode($home->content);
    $section1 = @$content->section1;
    $section2 = @$content->section2;
    $section3 = @$content->section3;
    $section4 = @$content->section4;
    $section5 = @$content->section5;
    $section6 = @$content->section6;
    $section7 = @$content->section7;
    $section8 = @$content->section8;


    $configGeneralContent = json_decode($configGeneral->content);

    ?>


    <section class="vk-content">

        @if(!empty($section1))
            <div class="vk-home__banner" {!! strlen(@$section1->image) ? 'style="background-image: url('.image_url($section1->image).')"' : null !!}>
                <div class="vk-banner--style-7">
                    <div class="vk-banner__content">
                        <div class="_wrapper">

                            <h2 class="_title-sub">{!! @$section1->name->sub !!}
                                @if(Auth::check())
                                    <span class="admin_editable" style="">
                                        <a href="{!! route('dashboard.index') !!}" title="Sửa" target="_blank">
                                        <i class="fa fa-pencil"></i>
                                        </a>
                                    </span>
                                @endif
                            </h2>
                            <h1 class="_title-1">{!! @$section1->name->main !!}</h1>

                            <div class="_form">
                                <?php $filterType = 'homeSearch' ?>
                                @include('frontend.components.filter')
                            </div>

                        </div>
                    </div>
                </div>

                <div class="d-lg-none">
                    <div class="vk-banner__hotline">
                        <div class="_left">
                            <img class="_icon lazy" src="{{ __IMAGE_LAZY_LOAD }}" data-src="{!! getAsset('') !!}/images/icon-phone.png" alt="{!! __('Call Us') !!}" width="" height="" />
                            <span class="_label">{!! __('Call Us') !!} </span>
                            <span class="_text">{!! @$configGeneralContent->site_hotline !!}</span>
                            <a href="tel:{!! @$configGeneralContent->site_hotline !!}" class="_link"></a>
                        </div>
                        <div class="_right">
                            <a href="{!! route('inquire') !!}">{!! __('inquire now') !!}</a>
                        </div>
                    </div>
                </div>

            @if(count(@$section1->content))
                    <div class="vk-home__banner-bot">
                        <div class="container">
                            <div class="vk-banner__bot-list row">
                                @foreach(@$section1->content as $val)
                                    <div class="_item col-lg-4">
                                        <div class="vk-bb-item {{ $loop->iteration  == count($section1->content)  ? 'last' : null }}" data-animation="fadeInUpFix" data-animation-delay="0"
                                             data-animation-duration="2">
                                            <h2 class="vk-bb-item__title">{!! @$val->name !!}</h2>
                                            <div class="vk-bb-item__text">{!! @$val->content !!}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div> <!--./banner-->
        @endif

        @if(!empty($section2))
            <div class="vk-home__about">
                <div class="container">
                    <div class="vk-home__about-content">
                        <h2 class="vk-home__title" data-animation="fadeInUpFix"
                            data-animation-duration="2">{!! @$section2->name !!}</h2>
                        <div class="_text" data-animation="fadeInUpFix"
                             data-animation-duration="2">{!! @$section2->content !!}</div>
                        <a href="{!! @$section2->link !!}" class="vk-btn" data-animation="fadeInUpFix"
                           data-animation-duration="2"><span>{!! __('View Details') !!}</span></a>
                    </div>
                </div>

            </div> <!--./about-->
        @endif

        @if(!empty($section3))
            <div class="vk-home__how">
                <div class="container">
                    <div class="vk-home__how-content">
                        <div class="vk-home__title-box" data-animation="fadeInUpFix" data-animation-duration="2">
                            <h2 class="_title">{!! @$section3->name !!}</h2>
                            <div class="_title-sub">{!! @$section3->name_sub !!}</div>
                        </div>
                        <div>
                            @if($howItWorks->count())
                                <div class="row vk-step__list justify-content-center  vk-slider--style-8"
                                     data-slider="">
                                    <?php $count = 1 ?>
                                    @foreach($howItWorks as $val)
                                        <?php
                                        $name = json_decode($val->name);
                                        $content = json_decode($val->content);
                                        ?>
                                        <div class="_item col-lg-3 col-md-3">
                                            <div class="vk-step-item vk-step-item--style-1" data-animation="fadeInUpFix"
                                                 data-animation-delay="0" data-animation-duration="2">
                                                <div class="vk-step-item__num">0{!! $count++ !!}</div>
                                                <div class="vk-step-item__brief">
                                                    <h3 class="vk-step-item__title">{!! @$name->sub !!}</h3>
                                                    <div class="vk-step-item__text">{!! @$content->sub !!}</div>
                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div> <!--./how-->
        @endif

        @if(!empty($section4))
            <?php $section4 = (array)$section4; ?>

            <div class="vk-home__tour-cat">
                <div class="container-fluid p-lg-0">
                    <div class="row vk-home__tour-cat-list">
                        <?php $count = 0; ?>
                        @foreach($section4 as $item)
                            <?php
                            $class = 'col-6 col-md-4 col-lg-3';
                            if ($count == 0 || $count == 4) {
                                $class = 'col-6 col-md-8 col-lg-6';
                            } elseif ($count == 2 || $count == 3) {
                                $class = 'col-6 col-md-6 col-lg-3 _item';
                            }
                            $count++
                            ?>
                            <div class="{!! $class !!} _item">
                                <div class="vk-shop-item vk-shop-item--style-4" data-animation="fadeInUpFix"
                                     data-animation-delay="0" data-animation-duration="2">
                                    <a href="{!! @$item->link  !!}" title="{!! @$item->name  !!}" class="vk-shop-item__img">
                                        <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url(@$item->image) !!}" alt="{!! @$item->name  !!}" width="" height="" class="_img lazy">
                                    </a>

                                    <div class="vk-shop-item__brief">
                                        <div>
                                            <h3 class="vk-shop-item__title"><a href="{!! @$item->link !!}" title="{!! @$item->name !!}">{!! @$item->name !!}</a>
                                            </h3>
                                            <div class="collapse">
                                                <div class="vk-shop-item__text">{!! @$item->content !!}</div>
                                                <a href="{!! @$item->link !!}"
                                                   class="vk-shop-item__btn">{!! __('View Details') !!}</a>

                                            </div>
                                        </div>

                                    </div>

                                    <a href="{!! @$item->link !!}" class="vk-shop-item__link"
                                       title="{!! @$item->name !!}"></a>

                                </div> <!--./vk-shop-item-->
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        @endif


        @if(!empty($section5))
            <div class="vk-home__shop">
                <div class="container-fluid">
                    <div class="vk-home__how-content">
                        <div class="vk-home__title-box" data-animation="fadeInUp" data-animation-duration="2">
                            <h2 class="_title">{!! @$section5->name !!}</h2>
                        </div>
                        @if($tourHighlights->count())
                            <?php
                            $tourWrapper = 'col-12';
                            $tourClass = 'vk-shop-item--style-5';
                            $tourAnimation = false;
                            ?>

                            <div class="vk-shop__list row vk-slider--style-2"
                                 data-animation="fadeIn"
                                 data-slider="tour-detail"
                                {!! \App\Enum\ItemScopeEnum::ITEM_TYPE['ItemList'] !!}>
                                @foreach($tourHighlights as $tour)
                                    @include('frontend.components.tour')
                                @endforeach
                            </div>

                        @endif
                    </div>
                </div>
                @if(strlen(@$section5->banner->image))
                    <div class="container">
                        <div class="vk-home__shop-banner">
                            <a href="{{ @$section5->banner->link }}" title="{{ @$section5->banner->name }}" target="_blank" class="vk-img">
                                <img src="{{ image_url(@$section5->banner->image) }}" alt="{{ @$section5->banner->name }}">
                            </a>
                        </div>
                    </div>
                @endif

            </div> <!--./tour-->
        @endif


        @if(!empty($section6))
            <div class="vk-home__tour-style" data-background-content="{!! @$section6->name_sub !!}">
                <div class="container-fluid">
                    <h2 class="vk-home__title--style-1" data-animation="fadeInUpFix"
                        data-animation-duration="2">{!! @$section6->name !!}</h2>
                    @if($tourStyles->count())
                        <?php
                        $tourWrapper = 'col-12';
                        $tourClass = 'vk-shop-item--style-3';
                        ?>
                        <div data-animation="fadeInUpFix" data-animation-duration="2">
                            <div class="vk-tour__list--style-1 row vk-slider--style-11" data-slider="home-tour-1">
                                @foreach($tourStyles as $activity)
                                <?php $image = '<img src="'.__IMAGE_LAZY_LOAD.'" data-src="'.image_url($activity->image).'" class="_img" width="" height="" alt="'.$activity->name.'">';?>
                                    <div>
                                        <div>
                                            <div class="{!! isset($tourWrapper) ? $tourWrapper : 'col-12' !!} _item">
                                                <div class="vk-shop-item {!! $tourClass !!}">
                                                    <a href="{!! route('tourActivity.detail',['slug'=>$activity->slug]) !!}"
                                                       title="{!! $activity->name !!}"
                                                       class="vk-shop-item__img">
                                                        {!! $image !!}
                                                    </a>

                                                    <div class="vk-shop-item__brief">
                                                        <h3 class="vk-shop-item__title">
                                                            <a href="{!! route('tourActivity.detail',['slug'=>$activity->slug]) !!}"
                                                               title="{!! $activity->name !!}">{!! $activity->name !!}</a></h3>

                                                    </div>
                                                </div> <!--./vk-shop-item-->
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>


            </div>
        @endif

        @if(!empty($section7))
            <div class="vk-sale__review">
                <div class="container">
                    <div class="vk-review__wrapper" data-animation="fadeInUpFix" data-animation-duration="2">
                        <h2 class="vk-review__title"><span>{!! @$section7->name !!}</span></h2>
                        <div>
                            <?php $content = (array)@$section7->content;?>
                            @if(count($content))
                                <div class="vk-review__list slick-slider  vk-slider--style-3" data-slider="review">
                                    @foreach($content as $item)
                                        <div class="_item">
                                            <div class="vk-review-item">
                                                <div class="_top">
                                                    <div class="vk-review-item__title">{!! @$item->name !!}</div>
                                                    <div class="vk-review-item__meta">{!! @$item->country !!} &nbsp; •
                                                        &nbsp;
                                                        {!! @$item->date !!}
                                                    </div>
                                                </div>
                                                <div class="_bot">{!! @$item->content !!}</div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            @endif
                        </div>

                        <div class="vk-review__button">
                            <a href="{!! @$section7->link !!}"
                               target="_blank" class="vk-review__btn">{!! __('Write a review') !!}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(!empty($section8))
            <div class="vk-home__shop">
                <div class="container">
                    <div class="vk-home__how-content">
                        <div class="vk-home__title-box" data-animation="fadeInUpFix" data-animation-duration="2">
                            <h2 class="_title">{!! @$section8->name !!}</h2>
                        </div>
                        <div>
                            @if($blogs->count())
                                <div class="vk-blog__list row " data-slider="blog-home">
                                    @foreach($blogs as $blog)
                                        @include('frontend.components.blog')
                                    @endforeach
                                </div>
                            @endif
                        </div>

                    </div>

                </div>
            </div> <!--./blog-->
        @endif

    </section><!--./content-->

@endsection
