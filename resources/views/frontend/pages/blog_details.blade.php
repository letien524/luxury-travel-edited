@extends('frontend.master')


@section('content')

    @if($errors->any())
        <script>
            var erros = "{!! implode(', ',$errors->all()) !!}";
            alert(erros)
        </script>
        </div>
    @endif

    <section class="vk-content">
        <?php $image = json_decode($blog->image) ?>
        <div class="vk-blog-detail__top" {!! strlen(@$image->banner) ? 'style="background-image:url('.image_url($image->banner).')"' : null  !!}>

            <div class="container">
                <div class="vk-blog-detail__top-content">
                    <div class="_wrapper">
                        <div class="vk-blog-detail__meta">

                            <span class="_item">{!! @$blog->category->last()->name!!} </span>

                            <?php
                            $time = date('H-m-s-m-d-Y', strtotime($blog->created_at));
                            $time = explode('-', $time);

                            setlocale(LC_TIME, $currentLang);
                            $time = strftime("%B %d, %Y", mktime($time[0], $time[1], $time[2], $time[3], $time[4], $time[5]));

                            ?>
                            <span class="_item">{!! $time !!}</span>
                        </div>
                        <h1 class="vk-blog-detail__title">{!! $blog->name !!}
                            @if(Auth::check())
                                <span class="admin_editable">
                                    @if(isset($metaConfig['page']))
                                        <a href="{!! route('page.edit',$blog) !!}" title="Sửa">
                                              <i class="fa fa-pencil"></i>
                                        </a>
                                    @else
                                        <a href="{!! route('blog.edit',$blog) !!}" title="Sửa">
                                          <i class="fa fa-pencil"></i>
                                        </a>
                                    @endif


                                </span>
                            @endif
                        </h1>
                        <ul class="vk-blog-detail__breadcrumb">
                            <li><a href="{!! route('home') !!}">{!! __('Home') !!}</a></li>
                            @if(isset($metaConfig['page']))
                                <li><a href="{!! route('about') !!}">{!! __('About us') !!}</a></li>
                            @else
                                <li><a href="{!! route('blog') !!}">{!! __('Blogs') !!}</a></li>
                            @endif

                        </ul>
                    </div>
                </div>
            </div>

        </div>

        <div class="container pt-30 pb-60 pt-lg-60 pb-lg-80">
            <div class="row">
                <div class="col-lg-9">
                    <div class="vk-blog__left">

                        <div class="vk-blog__share">
                            <a href="https://www.facebook.com/sharer/sharer.php?u={!! urlencode(URL::full()) !!}"
                               target="_blank" class="facebook"><i class="_icon fa fa-facebook-f"></i></a>
                            <a href="https://twitter.com/home?status={!! urlencode(URL::full()) !!}" target="_blank"
                               class="twitter"><i class="_icon fa fa-twitter"></i></a>
                            {{--<a href="#" class="google"><i class="_icon fa fa-google-plus"></i></a>--}}
                            {{--<a href="#" class="more"><i class="_icon ">+</i></a>--}}


                        </div>
                        <div class="vk-blog-detail__content">{!! $blog->content !!}</div>

                        @if($comments->count())
                            <div class="vk-blog-detail__comment-list">
                                <div class="vk-comment__list">
                                    @foreach($comments as $comment)
                                        <div class="_item">
                                            <div class="vk-comment-item">
                                                <div class="vk-comment-item__img">
                                                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url('') !!}" alt="">
                                                </div>
                                                <div class="vk-comment-item__text">
                                                    <b>{!! $comment->name !!}</b> {!! $comment->content !!}</div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif


                        <?php $tags = $blog->tag; ?>
                        @if($tags->count())
                            <div class="vk-blog-detail__tag">
                                @foreach($tags as $tag)
                                    <a href="{!! route('blogTag.detail',$tag->slug) !!}">{!! $tag->name !!}</a>
                                @endforeach
                            </div>
                        @endif


                        <div class="vk-blog-detail__comment">
                            <h3 class="vk-blog-detail__title--style-1">{!! __('Leave A Reply') !!}</h3>
                            <div class="vk-blog-detail__title-sub">{!! __('Your email address will not be published. Required fields are marked *') !!}</div>
                            <div class="vk-form--comment">
                                <form action="{!! route('blog.comment',$blog) !!}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="type" value="blog-comment">

                                    <div class="form-group">
                                        <div class="_title">{!! __('Comment') !!}</div>
                                        <textarea class="form-control" placeholder="{!! __('Write here') !!}"
                                                  name="content" required>{!! old('content') !!}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="_title">{!! __('Name') !!} *</div>
                                                <input type="text" class="form-control" placeholder="" name="name"
                                                       value="{!! old('name') !!}" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="_title">{!! __('Email') !!} *</div>
                                                <input type="email" class="form-control" placeholder="" name="email"
                                                       value="{!! old('email') !!}" required>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="_title">{!! __('Phone number') !!}</div>
                                                <input type="tel" class="form-control" placeholder="" name="phone"
                                                       value="{!! old('phone') !!}">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="_title">{!! __('Website') !!}</div>
                                                <input type="text" class="form-control" placeholder="" name="website"
                                                       value="{!! old('website') !!}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="vk-form--sale">
                                        @include('frontend.components.captcha')
                                    </div>

                                    <div class="vk-button">
                                        <button class="vk-btn" type="submit">{!! __('post comment') !!}</button>
                                    </div>
                                </form>
                            </div>


                        </div>

                        @if(!empty(@$relatedBlogs) && $relatedBlogs->count())
                            <div class="vk-blog-detail__relate">
                                <h3 class="vk-blog-detail__title--style-2">{!! __('More posts on this topic') !!}</h3>

                                <div class="vk-blog__mid">

                                    <div class="vk-blog__list row">
                                        <?php $blogClass = 'vk-blog-item--style-3'; ?>
                                        @foreach($relatedBlogs as $blog)
                                            @include('frontend.components.blog')
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        @endif

                    </div>
                </div>
                <div class="col-lg-3 ">
                    @include('frontend.components.sidebar')
                </div>
            </div>
        </div>


    </section><!--./content-->


@endsection