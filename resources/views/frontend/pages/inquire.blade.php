@extends('frontend.master')
@section('link')
    <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/select2/css/select2.min.css">
    <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/select2/css/select2-bootstrap4.min.css">
    <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css">
@endsection
@section('end')
    <script defer src="{!! getAsset('') !!}/plugin/select2/js/select2.full.min.js"></script>

@endsection
@section('content')

    <section class="vk-content">
        <div class="vk-inquire__wrapper">
            <form action="{!! route('inquire.store') !!}" method="post" onsubmit="this.querySelector('.vk-btn._submit').setAttribute('disabled','disabled')">
                {{csrf_field()}}
                <div class="container">
                    <div class="vk-inquire__content">
                        <div class="vk-inquire__mid">
                            <?php $content = json_decode($inquire->content);?>

                            <div class="vk-inquire__title-box">
                                <h1 class="_title">{!! $inquire->name !!}</h1>
                                <div class="_text">{!! @$content->content !!}</div>
                            </div>

                            <div class="vk-sale__bbuster-content--style-1">
                                <div class="_wrapper">
                                    <div class="_top">

                                        <div class="progress">
                                            <div class="progress-bar" id="progress" role="progressbar"
                                                 style="width: 25%"
                                                 aria-valuenow="25"
                                                 aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>

                                        <div class="vk-bbuster">
                                            <h2 class="vk-bbuster__title">{!! __('Where will you be travelling to?') !!}</h2>

                                            <div class="vk-bbuster__list--style-1 row">
                                                <?php $destinationOld = old('destination') ? old('destination') : [] ?>
                                                @foreach($destinations as $destination)
                                                    <div class="_item">
                                                        <label class="vk-bbuster-item">
                                                            <input type="checkbox" name="destination[]"
                                                                   value="{!! $destination->id !!}" {!! in_array($destination->id, $destinationOld ) ? 'checked' : null !!}>
                                                            <span class="vk-bbuster-item__content">
                                                     <span class="vk-bbuster-item__img">
                                                         <img
                                                                 src="{!! __IMAGE_LAZY_LOAD !!}"
                                                                 data-src="{!! image_url($destination->image) !!}"
                                                                 alt="{!! $destination->name !!}">
                                                     </span>
                                                    <span class="vk-bbuster-item__title">{!! $destination->name !!}</span>
                                                </span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div> <!--./top-->

                                    <div class="_bot" id="inquireForm">

                                        <div class="vk-form--sale">.

                                            @if($errors->any())
                                                <script>
                                                    window.addEventListener('DOMContentLoaded', function() {
                                                        (function($) {
                                                            $('html, body').stop().animate({
                                                                scrollTop: $("#inquireForm").offset().top - 80

                                                            }, 1000);

                                                        })(jQuery);
                                                    });
                                                </script>
                                                <div class="alert alert-warning vk-alert-warning" role="alert">
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }}<br>
                                                    @endforeach
                                                </div>
                                            @endif

                                            <div class="row">

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="_title">{!! __('Departure date') !!}(YYYY-MM-DD/)
                                                        </div>
                                                        <input type="text" class="form-control vk-datepicker"
                                                               autocomplete="off"
                                                               name="departure_date"
                                                               value="{!! old('departure_date') !!}">
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-6">
                                                    <div class="form-group">
                                                        <div class="_title">{!! __('Number of Adults') !!}</div>
                                                        <div class="vk-calculator" data-calculator>
                                                            <input type="number" name="number_adult"
                                                                   value="{!! old('number_adult',0) !!}" min="0"
                                                                   class="form-control">
                                                            <a href="#" class="_btn _plus" data-index="plus"><i
                                                                        class="_icon ti-angle-up"></i></a>
                                                            <a href="#" class="_btn _minus" data-index="minus"><i
                                                                        class="_icon ti-angle-down"></i></a>
                                                        </div> <!--./calculator-->
                                                    </div>
                                                </div>

                                                <div class="col-lg-3 col-6">
                                                    <div class="form-group">
                                                        <div class="_title">{!! __('Number of Children') !!}</div>
                                                        <div class="vk-calculator" data-calculator>
                                                            <input type="number" name="number_child"
                                                                   value="{!! old('number_child   ',0) !!}" min="0"
                                                                   class="form-control">
                                                            <a href="#" class="_btn _plus" data-index="plus"><i
                                                                        class="_icon ti-angle-up"></i></a>
                                                            <a href="#" class="_btn _minus" data-index="minus"><i
                                                                        class="_icon ti-angle-down"></i></a>
                                                        </div> <!--./calculator-->
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="_title text-capitalize">{!! __('duration') !!}</div>
                                                        <input type="text" class="form-control" name="duration"
                                                               value="{!! old('duration') !!}"
                                                               placeholder="{{__('The number of days')}}">
                                                    </div>
                                                </div>

                                                <?php $hotels = (array)@$content->hotel_category; ?>
                                                @if(count($hotels))
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="_title">{!! __('Hotel Category') !!}</div>
                                                            <div class="vk-dropdown vk-dropdown--style-2"
                                                                 data-layout="dropdown">
                                                                <div class="_current">
                                                                    <div><span class="_text"></span></div>
                                                                    <span class="_icon fa fa-angle-down"></span>
                                                                </div>

                                                                <div class="_list collapse">
                                                                    <input type="hidden" name="hotel" value="">
                                                                    @foreach($hotels as $hotel)
                                                                        <a href="javascript:void(0);"
                                                                           data-value="{!! @$hotel !!}">
                                                                            <span class="_content">{!! @$hotel !!}</span>
                                                                        </a>
                                                                    @endforeach
                                                                </div>
                                                            </div> <!--./dropdown-->
                                                        </div>
                                                    </div>
                                                @endif

                                                <?php //$travels = (array)@$content->travel_style; ?>
                                                @if(count([]))
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="_title">{!! __('Travel Style') !!}</div>
                                                            <div class="vk-dropdown vk-dropdown--style-2"
                                                                 data-layout="dropdown">
                                                                <div class="_current">
                                                                    <div><span class="_text"></span></div>
                                                                    <span class="_icon fa fa-angle-down"></span>
                                                                </div>

                                                                <div class="_list collapse">
                                                                    <input type="hidden" name="activity">
                                                                    @foreach($travels as $travel)
                                                                        <a href="javascript:void(0);"
                                                                           data-value="{!! @$travel !!}"> <span
                                                                                    class="_content">{!! @$travel !!}</span></a>
                                                                    @endforeach
                                                                </div>
                                                            </div> <!--./dropdown-->
                                                        </div>
                                                    </div>
                                                @endif
                                            </div> <!--./row-->

                                            <div class="vk-button collapse {{$errors->any() ?: "show" }}" id="buttonStep1">
                                                <button onclick="buttonSlideUp(this,event,'#step2')" class="vk-btn">{{ __('Continue') }}</button>
                                            </div>

                                            <!--STEP 2-->
                                            <div class="collapse {{$errors->any() ? "show" : "" }}" id="step2">
                                                <div class="_bot pt-50">
                                                    <h2 class="vk-bbuster__title--style-1 mb-30">{!! __('Customer Information') !!}</h2>
                                                    <div class="vk-form--sale">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <div class="_title">{!! __('Your Name') !!} *</div>
                                                                    <input type="text" class="form-control" name="name"
                                                                           value="{!! old('name') !!}" required
                                                                           placeholder="Garrett Valdez">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <div class="_title">{!! __('Email') !!} *</div>
                                                                    <input type="emai" class="form-control" name="email"
                                                                           value="{!! old('email') !!}" required
                                                                           placeholder="yost.junius@hotmail.com">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <div class="_title">{!! __('Phone number') !!}</div>

                                                                    <div class="input-group">
                                                                        <input type="tel" class="form-control"
                                                                               name="phone" value="{!! old('phone') !!}"
                                                                               placeholder="+84 123 456 789">

                                                                    </div>


                                                                </div>
                                                            </div>


                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <div class="_title">{!! __('Nationality') !!}</div>
                                                                    <select class="js-example-basic-single form-control"
                                                                            name="country_id" style="width: 100%;">
                                                                        <option value="">{!! __('Select') !!}</option>
                                                                        @foreach($countries as $country)
                                                                            <option value="{!! $country->id !!}" {!! $country->id == old('country_id') ? 'selected': null !!}>{!! __($country->name) !!}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12 ">
                                                                <div class="form-group">
                                                                    <div class="_title">{!! __('Special Request') !!}</div>
                                                                    <textarea class="form-control" name="content"
                                                                              placeholder="{!! __('Write here') !!}">{!! old('content') !!}</textarea>
                                                                </div>
                                                            </div>


                                                        </div> <!--./row-->

                                                        <div class="vk-divider--style-1 mt-20 pb-20"></div>


                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <div class="_title">{{__('Please enter the code above')}}</div>
                                                                    <input type="text" class="form-control" name="captcha" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6">
                                                                <div class="form-group">
                                                                    <div class="_title"><span>{{__('Recaptcha')}}</span><a href="javascript:void(0);" onclick="captchaChange(this,'{{route("get.captcha")}}')"><i class="_icon fa fa-repeat"></i> {{__('Change Image')}}</a></div>
                                                                    <img src="{{ $captcha }}" alt="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <script>
                                                            function captchaChange(el,url){
                                                                $.ajax({
                                                                    type: "get",
                                                                    url: url,
                                                                    success: function($data){
                                                                        $(el).closest('.form-group').find('img').attr('src',$data);
                                                                    }
                                                                })
                                                            }
                                                        </script>

                                                        {{--<div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="d-flex justify-content-center">
                                                                    {!! NoCaptcha::renderJs($currentLang,false,'onloadCallback') !!}
                                                                    {!! NoCaptcha::display() !!}
                                                                </div>
                                                            </div>
                                                        </div>--}}

                                                        <div class="vk-button">
                                                            <!--<button onclick="buttonBack(this,event,'#step2','#buttonStep1')" class="vk-btn">Back</button>-->
                                                            <button class="vk-btn _submit"
                                                                    type="submit">{!! __('Finish') !!}</button>
                                                        </div>

                                                    </div>

                                                </div> <!--./bot-->
                                            </div>


                                            <!--STEP 3-->


                                        </div>

                                    </div> <!--./bot-->
                                </div>
                            </div>

                        </div> <!--./mid-->
                        <?php $faqs = $inquire->faq; ?>
                        @if(!empty($faqs))
                            <?php $content = $faqs->content;?>
                            <div class="vk-inquire__bot">
                                <h2 class="vk-inquire__title">{!! __('FAQs') !!}</h2>

                                @include('frontend.components.faq')
                            </div>
                        @endif

                    </div>
                </div>
            </form>
        </div>

    </section><!--./content-->

@endsection
