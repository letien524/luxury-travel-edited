@extends('frontend.master')
@section('content')
    <?php
    $destinationOriginName = $destination->name;
    $destinationHasParent = intval($destination->parent_id);
    $destinationContent = json_decode($destination->content);
    $destinationName = @$destinationContent->name;
    $destinationBanner = @$destinationContent->banner;
    $destinationCount = @$destinationContent->count;
    $destinationTravelGuide = @$destinationContent->travel_guide;
    $destinationImage = @$destinationContent->image;
    $destinationCategory = (array)@$destinationContent->blog_category;

    ?>
    <section class="vk-content">
        <div class="vk-banner--style-4" {!!  !empty(@$destinationBanner->top->image) ? 'style="background-image: url('.image_url(@$destinationBanner->top->image).');"' : null !!}>
            <div class="container">
                <div class="vk-banner__content">
                    <div class="_wrapper">
                        <div>
                            <h1 class="_title">{!! $destinationOriginName !!}
                                @if(Auth::check())
                                    <span class="admin_editable" style="">
                                        <a href="{!! route('tourDestination.edit',$destination) !!}" title="Sửa"
                                           target="_blank">
                                        <i class="fa fa-pencil"></i>
                                        </a>
                                    </span>
                                @endif
                            </h1>
{{--                            <h2 class="_title-sub">{!! $destination->is_best == 1 ? $destination->excerpt :@$destinationBanner->top->name !!}</h2>--}}
                            <h2 class="_title-sub">{!! @$destinationBanner->top->name !!}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="vk-destiny__top">
            <div class="container">
                <div class="vk-destiny__top-content">
                    <div class="row">
                        <div class="col-lg-6 order-1 order-lg-0">
                            <div class="_left pb-40 pb-lg-0">

                                <h2 class="vk-destiny__title d-lg-block d-none">{!! @$destinationContent->name !!}</h2>

                                <div class="_content">

                                    <div class="_text">{!! @$destinationContent->content !!}</div>

                                    @if(@$destinationCount->status == 1)
                                        <div class="vk-destiny__count">
                                            <div class="_item">
                                                <div class="_num">{!! @$destinationCount->population !!}</div>
                                                <div class="_title">{!! strlen(@$destinationCount->population_alt) ? $destinationCount->population_alt :  __('Population') !!}</div>
                                            </div>

                                            <div class="_item">
                                                <div class="_num">{!! @$destinationCount->temple !!}</div>
                                                <div class="_title">{!!  strlen(@$destinationCount->temple_alt) ? $destinationCount->temple_alt :__('Temples') !!}</div>
                                            </div>

                                            <div class="_item">
                                                <div class="_num">{!! @$destinationCount->city !!}</div>
                                                <div class="_title">{!! strlen(@$destinationCount->city_alt) ? $destinationCount->city_alt : __('Cities') !!}</div>
                                            </div>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 order-0 order-lg-1">
                            <div class="_right">
                                <h2 class="vk-destiny__title d-lg-none mb-50">{!! @$destinationContent->name !!}</h2>
                                <div class="vk-img vk-img--mw100">
                                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url(@$destinationContent->image) !!}" alt="{!! @$destinationContent->name !!}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <h2 class="vk-destiny__title--style-1">{!! __('Our Luxury :name Tours',['name'=>$destinationOriginName]) !!}</h2>
        </div>
        <?php $filterType = 'destination' ?>

        @include('frontend.components.filter')

        <div class="container pt-60">
            <div class="vk-shop__list row" id="tourList" {!! \App\Enum\ItemScopeEnum::ITEM_TYPE['ItemList'] !!}>
                @foreach($tours as $tour)
                    @include('frontend.components.tour')
                @endforeach

                @include('frontend.components.tour-search-item',[
                    'tours' => $tours,
                    'tourSearchConfig' => @$tourSearchConfig,
                ])
            </div>

            <div class="vk-shop__more">
                @if($tours->count() >= $tours->perPage() && $tours->currentPage() < $tours->lastPage())
                    <a href="javascript:void(0)" rel="nofollow" onclick="loadMoreData(this)"><span>{!! __('View More') !!}</span></a>
                @endif
            </div>
        </div>

        @if(!empty($destination->faq))
            <?php $category = $destination ?>
            @include('frontend.components.tour-guide')
        @endif

        @if(!empty(@$reviews))
            <div class="container pt-60 pb-60">
                <div class="vk-home__title-box" data-animation="fadeInUp" data-animation-duration="2">
                    <h2 class="_title">{!! __('Reviews') !!}</h2>
                </div>

                <div class="vk-blog-detail__comment-list">
                    <div class="vk-comment__list" id="reviewList">
                        @include('frontend.components.review',['reviews'=>$reviews])
                    </div>
                </div>
            </div>
        @endif

        @if($bestDestinations->count())
            <div class="vk-home__shop">
                <div class="container-fluid">
                    <div class="vk-home__how-content">
                        <div class="vk-home__title-box" data-animation="fadeInUp" data-animation-duration="2">
                            <div class="container">
                                {{--./ _title--}}
                                <h2 class="_title">{!! $destinationHasParent == 0 ? __('Best destinations in :name',['name'=>$destinationOriginName]) : __('Best destinations') !!}</h2>
                            </div>
                        </div>
                        <?php
                        $tourWrapper = 'col-12';
                        $tourClass = 'vk-shop-item--style-5';
                        $tourAnimation = false;
                        ?>

                        <div class="vk-shop__list row vk-slider--style-2"
                             data-animation="fadeIn"
                             data-slider="tour-detail">
                            @foreach($bestDestinations as $destination)
                                <div>
                                    <div>
                                        @include('frontend.components.destination')
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>

                </div>
            </div> <!--./tour-->
        @endif

        <?php $blogs = \App\Post::where('status', 1)->whereHas('category',function($query) use ($destinationCategory){
            $query->whereIn('category_id', $destinationCategory);
        })->latest()->take(10)->get(); ?>

        @if($blogs->count())
            <?php
            $blogClass = 'vk-blog-item--style-1';
            $blogWrapper = 'col-12';
            ?>
            <div class="vk-destiny__blog" style="background-color: #fff;">
                <div class="container">
                    <div class="vk-home__title-box">
                        <h3 class="_title">{!! __('Travel News in :name',['name'=>$destinationName]) !!}</h3>
                    </div>
                   {{-- <h3 class="vk-destiny__title--style-2">{!! __('Travel News in :name',['name'=>$destinationName]) !!}</h3>--}}
                    <div class="vk-blog__list row slick-slider vk-slider--style-9" data-slider="tour-des">
                        @foreach($blogs as $blog)
                            @include('frontend.components.blog')
                        @endforeach
                    </div>
                </div>
            </div>
        @endif


        <?php
        $bannerStyle = 'style-8';
        $bannerClass = 'vk-banner--style-8';
        $bannerBackground = @$destinationBanner->bottom->image;
        ?>
        @include('frontend.components.banner')

    </section><!--./content-->
@section('end')
    <script type="text/javascript">
        var pageNumber = 2;
        var lastPage = {!! $tours->lastPage() !!};
        function loadMoreData(el){
            $.ajax({
                type : 'GET',
                url: '{!! url_query_render('page',null)['url'] !!}' + pageNumber,
                success : function(data){
                    $('#tourList').append(data.html);

                    pageNumber ++;
                    if(pageNumber === lastPage+1){
                        $(el).hide();
                    }
                },error: function(data){

                },
            })
        }
    </script>
@endsection

@endsection
