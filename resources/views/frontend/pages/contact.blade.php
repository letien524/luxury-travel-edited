@extends('frontend.master')


@section('content')

    <section class="vk-content">
        <div class="vk-contact__map">
            <div class="vk-img">
                <img
                        src="{!! __IMAGE_LAZY_LOAD !!}"
                        data-src="{!! strlen(@$configs->content->image) ? image_url(@$configs->content->image) : getAsset('/images/map-3.jpg') !!}"
                        alt="">
            </div>
        </div>

        <div class="vk-contact__content">
            <div class="d-flex flex-column">
                <div class="vk-contact__top order-1 order-lg-0">
                    <div class="container">
                        @if($address->count())
                            <div class="row">
                                <div class="col-lg-3 _item">
                                    <h2 class="vk-contact__title d-none d-lg-block">{!! __('Office Information') !!}</h2>
                                </div>

                                @foreach($address as $item)

                                    <div class="col-lg-3 _item">
                                        <div class="vk-contact-item">
                                            <h3 class="vk-contact-item__title">{!! $item->name !!}</h3>
                                            <ul class="vk-contact-item__list">
                                                <li>
                                                    <div class="vk-contact-item__label">{!! __('Address') !!}:</div>
                                                    <div class="vk-contact-item__text">{!! $item->address !!}</div>
                                                </li>
                                                <li>
                                                    <div class="vk-contact-item__label">{!! __('Phone') !!}:</div>
                                                    <div class="vk-contact-item__text">
                                                        <a href="tel:{!! $item->phone !!}">{!! $item->phone !!}</a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="vk-contact-item__label">{!! __('Hotline') !!}:</div>
                                                    <div class="vk-contact-item__text">
                                                        <a href="tel:{!! $item->hotline !!}">{!! $item->hotline !!}</a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="vk-contact-item__label">{!! __('Email') !!}:</div>
                                                    <div class="vk-contact-item__text">
                                                        <a href="mailto:{!! $item->email !!}">{!! $item->email !!}</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!--./item-->
                                @endforeach


                            </div>
                        @endif

                    </div>
                </div> <!--./top-->



                <div class="vk-contact__bot order-0 order-lg-1" id="contact">
                    <div class="container">
                        <div class="vk-contact__bot-content">
                            <div class="_left">
                                <h1 class="vk-contact__title--style-3 d-none d-lg-block">{!! __('Contact Us') !!}</h1>
                            </div>
                            <div class="_right">
                                <div class="vk-contact__form">
                                    @if($errors->any())
                                    <script>
                                        window.addEventListener('DOMContentLoaded', function() {
                                            (function($) {
                                                $('html, body').stop().animate({
                                                    scrollTop: $("#contact").offset().top - 80

                                                }, 1000);

                                            })(jQuery);
                                        });
                                    </script>
                                    <div class="alert alert-warning vk-alert-warning" role="alert">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}<br>
                                        @endforeach
                                    </div>
                                    @endif
                                    <h3 class="vk-contact__title--style-2">{!! __('Please write your request, we will reply to you soon.') !!}</h3>

                                    @include('frontend.components.contact-form', ['action'=> route('contact.store')])

                                </div>

                            </div>
                        </div>
                    </div>
                </div> <!--./bot-->
            </div>

        </div> <!--./content-->

        @include('frontend.components.make_enquiry')


    </section><!--./content-->


@endsection