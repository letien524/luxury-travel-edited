@extends('frontend.master')
@section('link')
    <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/fancybox/jquery.fancybox.min.css">
    <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css">
@endsection

@section('end')
    <script defer src="{!! getAsset('') !!}/plugin//fancybox/jquery.fancybox.min.js"></script>
    <script>
        function tourFormShow(el, ev) {
            ev.preventDefault();
            el = $(el);
            var target = $(el.data('target'));


            if (!target.hasClass('show')) {
                target.collapse('show');

                target.on('shown.bs.collapse', function () {


                    el.find('span').text('Send');
                });

            } else {
                var inputName = target.find('input[name=name]').val();
                if (inputName.length === 0) {
                    return alert('{!! __('Your name is required!') !!}');
                }

                var inputPhone = target.find('input[name=phone]').val();
                if (inputPhone.length === 0) {
                    return alert('{!! __('Your phone is required!') !!}');
                }

                var inputEmail = target.find('input[name=email]').val();
                if (inputEmail.length === 0) {
                    return alert('{!! __('Your email is required!') !!}');
                }

                $('#exampleModal').modal('show');

                $('#cusGreeting').text('{!! __('Hello') !!} ' + inputName);
                $('#cusPhone').text(inputPhone);
                $('#cusEmail').text(inputEmail);

            }
        }

        function tourFormShowEx(el, ev) {
            ev.preventDefault();
            el = $(el);
            var target = $(el.data('target'));
            var form = $('#tourForm');


            var inputName = form.find('input[name=name]').val();

            if (inputName.length === 0) {
                return alert('{!! __('Your name is required!') !!}');
            }

            var inputPhone = form.find('input[name=phone]').val();
            if (inputPhone.length === 0) {
                return alert('{!! __('Your phone is required!') !!}');
            }

            var inputEmail = form.find('input[name=email]').val();
            if (inputEmail.length === 0) {
                return alert('{!! __('Your email is required!') !!}');
            }

            target.modal('show');

            $('#cusGreeting').text('{!! __('Hello') !!} ' + inputName);
            $('#cusPhone').text(inputPhone);
            $('#cusEmail').text(inputEmail);


        }

    </script>
@endsection

@section('content')
    <?php
    $content = json_decode($tour->content);
    $video = (array)@$content->video;
    $plans = (array)json_decode($tour->plan);
    $hotels = (array)json_decode($tour->hotel);
    $faq = $tour->faq;
    $map = json_decode($tour->map);
    $gallery = $tour->gallery;
    $tourName = $tour->name;


    ?>



    <section class="vk-content">
        <form action="{!! route('inquire.store') !!}" method="post" onsubmit="this.querySelector('.vk-btn._submit').setAttribute('disabled','disabled')">
            {{csrf_field()}}
            <input type="hidden" name="name" value="{{old('name',request('name'))}}">
            <input type="hidden" name="phone" value="{{old('phone',request('phone'))}}">
            <input type="hidden" name="email" value="{{old('email',request('email'))}}">
            <div class="vk-popup__content">
                <div class="_left">
                    <div class="vk-img">
                        <img src="{!! __IMAGE_LAZY_LOAD !!}"
                             data-src="{!! strlen(@$configGeneral->site_enquiry_banner) ? image_url(@$configGeneral->site_enquiry_banner) : getAsset('/images/poppup-1.jpg') !!}"
                             alt="{!! $tour->name !!}">
                    </div>
                    <div class="_text">{!! __(':tour in a Tour of :destination',['destination' => $tourDestinations, 'tour' => $tourName]) !!}</div>
                </div> <!--./left-->

                <div class="_right">

                    <h3 class="vk-popup__title"
                        id="cusGreeting">{!! __('Hello :name',['name'=>request('name')]) !!} </h3>
                    <div class="vk-popup__row">
                        <div class="_label">{!! __('You have requested the tour') !!}</div>

                        <div class="_text">{!! __(':tour in a Tour of :destination',['destination' => $tourDestinations, 'tour' => $tourName]) !!}</div>
                    </div>

                    <div class="vk-popup__row">
                        <div class="_label">{!! __('Contact information is') !!}</div>
                        <div class="_text">
                            <span id="cusPhone">{{request('phone')}}</span> &nbsp; | &nbsp;
                            <span id="cusEmail">{{request('email')}}</span>
                        </div>
                    </div>

                    <div class="vk-divider--style-1 mt-20 mb-30"></div>


                    <div class="vk-form--sale _fix">

                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="_title">{!! __('Departure date') !!} (YYYY/MM/DD)</div>
                                    <input type="text" class="form-control vk-datepicker" autocomplete="none"
                                           name="departure_date" value="{!! old('departure_date') !!}">
                                </div>
                            </div>

                            <div class="col-lg-3 col-6">
                                <div class="form-group">
                                    <div class="_title">{!! __('Number of Adults') !!}</div>
                                    <div class="vk-calculator" data-calculator>
                                        <input type="number" name="number_adult"
                                               value="{!! old('number_adult',0) !!}" min="0"
                                               class="form-control">
                                        <a href="#" class="_btn _plus" data-index="plus"><i
                                                    class="_icon ti-angle-up"></i></a>
                                        <a href="#" class="_btn _minus" data-index="minus"><i
                                                    class="_icon ti-angle-down"></i></a>
                                    </div> <!--./calculator-->
                                </div>
                            </div>

                            <div class="col-lg-3 col-6">
                                <div class="form-group">
                                    <div class="_title">{!! __('Number of Children') !!}</div>
                                    <div class="vk-calculator" data-calculator>
                                        <input type="number" name="number_child"
                                               value="{!! old('number_child',0) !!}" min="0"
                                               class="form-control">
                                        <a href="#" class="_btn _plus" data-index="plus"><i
                                                    class="_icon ti-angle-up"></i></a>
                                        <a href="#" class="_btn _minus" data-index="minus"><i
                                                    class="_icon ti-angle-down"></i></a>
                                    </div> <!--./calculator-->
                                </div>
                            </div>

                            <?php $hotels = (array)@$inquire->content->hotel_category; ?>
                            @if(count($hotels))
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="_title">{!! __('Hotel Category') !!}</div>
                                        <div class="vk-dropdown vk-dropdown--style-2"
                                             data-layout="dropdown">
                                            <div class="_current">
                                                <div><span class="_text"></span></div>
                                                <span class="_icon fa fa-angle-down"></span>
                                            </div>

                                            <div class="_list collapse">
                                                <input type="hidden" name="hotel" value="{!! old('hotel') !!}">
                                                @foreach($hotels as $hotel)
                                                    <a href="javascript:void(0);" class="{!! old('hotel') == @$hotel ? 'active' : null !!}"
                                                       data-value="{!! @$hotel !!}">
                                                        <span class="_content">{!! @$hotel !!}</span>
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div> <!--./dropdown-->
                                    </div>
                                </div>
                            @endif

                            <?php $travels = (array)@$inquire->content->travel_style; ?>
                            @if(count($travels))
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="_title">{!! __('Travel Style') !!}</div>
                                        <div class="vk-dropdown vk-dropdown--style-2"
                                             data-layout="dropdown">
                                            <div class="_current">
                                                <div><span class="_text"></span></div>
                                                <span class="_icon fa fa-angle-down"></span>
                                            </div>

                                            <div class="_list collapse">
                                                <input type="hidden" name="activity" value="{!! old('activity') !!}">
                                                @foreach($travels as $travel)
                                                    <a href="javascript:void(0);" class="{!! old('activity') == @$travel ? 'active' : null !!}"
                                                       data-value="{!! @$travel !!}"> <span
                                                                class="_content">{!! @$travel !!}</span></a>
                                                @endforeach
                                            </div>
                                        </div> <!--./dropdown-->
                                    </div>
                                </div>
                            @endif

                        </div> <!--./row-->
                        <div class="vk-divider--style-1 mt-20 mb-20"></div>

                        @include('frontend.components.captcha')

                        <div class="vk-button collapse show" id="buttonStep1">
                            <button type="submit" class="vk-btn _submit">{!! __('Send now') !!}</button>
                        </div>


                    </div>


                </div> <!--./right-->
            </div>
        </form>
    </section><!--./content-->
@endsection
