@extends('frontend.master')
@section('content')

    <section class="vk-content">
        <div class="vk-inquire__wrapper">

            <div class="container">
                <div class="vk-inquire__content">

                    <div class="vk-inquire__mid">

                        <div class="vk-sale__bbuster-content--style-1">
                            <div class="_wrapper">

                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>

                                <div class="_bot">
                                    <div class="vk-inquire__title-box mb-30">
                                        <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! getAsset('') !!}/images/icon-11.png" alt="{!! __('Finish booking') !!}"> <br><br>
                                        <h1 class="_title">{!! $name !!}</h1>
                                        <div class="_text">{!! $content !!}</div>
                                    </div>

                                    <div class="vk-form--sale">

                                        <div class="vk-button">
                                            <button onclick="window.location.href='{!! route('home') !!}'" class="vk-btn">{!! __('Back to home') !!}</button>
                                        </div>

                                    </div>

                                </div> <!--./bot-->
                            </div>
                        </div>

                    </div> <!--./mid-->

                </div>
            </div>
        </div>

    </section><!--./content-->


@endsection