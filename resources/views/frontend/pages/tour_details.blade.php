@extends('frontend.master')
@section('link')
    <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/fancybox/jquery.fancybox.min.css">
    <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css">
@endsection

@section('end')
    <script defer src="{!! getAsset('') !!}/plugin//fancybox/jquery.fancybox.min.js"></script>

    @if(session('inquire.store'))
        <script defer src="{!! getAsset('plugin/extra/tourBookingError.js') !!}"></script>
    @endif

    <script type="text/javascript">

        function tourFormShow(el, ev) {
            console.log('tourFormShow');
            ev.preventDefault();
            el = $(el);
            var target = $(el.data('target'));


            if (!target.hasClass('show')) {
                target.collapse('show');

                target.on('shown.bs.collapse', function () {


                    el.find('span').text('Send');
                });

            } else {

                var inputName = target.find('input[name=name]').val();
                if (inputName.length === 0) {
                    return alert('{!! __('Your name is required!') !!}');
                }

                var inputPhone = target.find('input[name=phone]').val();
                if (inputPhone.length === 0) {
                    return alert('{!! __('Your phone is required!') !!}');
                }

                var inputEmail = target.find('input[name=email]').val();
                if (inputEmail.length === 0) {
                    return alert('{!! __('Your email is required!') !!}');
                }

                if(window.outerWidth < 992){
                    window.location.href = "{{ route('tour.booking',['slug'=>$tour->slug]) }}?name="+inputName+"&phone="+inputPhone+"&email="+inputEmail;
                }else{
                    $('#exampleModal').modal('show');
                    $('#cusGreeting').text('{!! __('Hello') !!} ' + inputName);
                    $('#cusPhone').text(inputPhone);
                    $('#cusEmail').text(inputEmail);
                }



            }
        }

        function tourFormShowEx(el, ev) {
            console.log('tourFormShowEx');
            ev.preventDefault();
            el = $(el);
            var target = $(el.data('target'));
            var form = $('#tourForm');


            var inputName = form.find('input[name=name]').val();

            if (inputName.length === 0) {
                return alert('{!! __('Your name is required!') !!}');
            }

            var inputPhone = form.find('input[name=phone]').val();
            if (inputPhone.length === 0) {
                return alert('{!! __('Your phone is required!') !!}');
            }

            var inputEmail = form.find('input[name=email]').val();
            if (inputEmail.length === 0) {
                return alert('{!! __('Your email is required!') !!}');
            }

            if(window.outerWidth < 992){
                window.location.href = "{{ route('tour.booking',['slug'=>$tour->slug]) }}?name="+inputName+"&phone="+inputPhone+"&email="+inputEmail;
            }else{
                target.modal('show');

                $('#cusGreeting').text('{!! __('Hello') !!} ' + inputName);
                $('#cusPhone').text(inputPhone);
                $('#cusEmail').text(inputEmail);
            }

        }

    </script>
@endsection

@push('schema')
    @include('schema.tour-detail',['tour'=>$tour])
@endpush

@section('content')
    <?php
    $mainTour = $tour;
    $content = json_decode($tour->content);
    $video = (array)@$content->video;
    $plans = (array)json_decode($tour->plan);
    $hotels = (array)json_decode($tour->hotel);
    $faq = $tour->faq;
    $map = json_decode($tour->map);
    $gallery = $tour->gallery;
    $tourName = $tour->name;


    ?>

    <section class="vk-content">
        <form action="{!! route('inquire.store') !!}" method="post" id="formBooking" onsubmit="this.querySelector('.vk-btn._submit').setAttribute('disabled','disabled')">
            {!! csrf_field() !!}
            <input type="hidden" name="tour_id" value="{!! $tour->id !!}">
            {{--<div class="pt-75 pt-lg-150"></div>--}}
            <div class="vk-banner__slider--style-1">
                <div class="_top">
                    <div class="slider-for slick-slider vk-slider--style-1">

                        @if(strlen(@$video['link']))
                            <div class="_item">
                                <a data-fancybox="images1" href="{{$video['link']}}" class="vk-tour__video vk-img" title="{!! $tour->name !!}">
                                    <img class="card-img-top img-fluid" src="{{image_url(@$video['thumbnail'])}}" alt="{!! $tour->name !!}">
                                    <span class="_icon"> <i class="fa fa-youtube-play"></i></span>
                                </a>
                            </div>
                        @endif
                        <?php $count = strlen(@$video['link']) ? 1 : 0 ?>
                        @foreach($gallery as $image)
                            <div class="_item">
                                <a href="{!! image_url($image->url) !!}" class="vk-img" data-fancybox="images1" title="{!! $tour->name !!}">
                                    <div class="vk-img"><img src="{!! image_url($image->url) !!}" data-src="{!! image_url($image->url) !!}" alt="{!! $tour->name !!}"></div>
                                </a>
                            </div>
                            <?php $count++;
                            if ($count == 5) break;
                            ?>
                        @endforeach
                    </div>
                </div>

                <div class="_bot">
                    <div class="_left">
                        <div class="slider-nav slick-slider">

                            @if(strlen(@$video['link']))
                                <div class="_item">
                                    <div class="vk-img"><img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($video['thumbnail']) !!}" alt="{!! $tour->name !!}">
                                    </div>
                                </div>
                            @endif
                            <?php $count = strlen(@$video['link']) ? 1 : 0 ?>


                            @foreach($gallery as $image)

                                <div class="_item">
                                    <div class="vk-img"><img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($image->url) !!}" alt="{!! $tour->name !!}">
                                    </div>
                                </div>
                                <?php $count++;
                                if ($count == 5) break;
                                ?>
                            @endforeach
                        </div>
                    </div>

                    @if($gallery->count() > 5)
                        <div class="_more">
                            <div class="_item">

                                <a href="{!! image_url($gallery[0]->url) !!}" class="vk-img" data-fancybox="images">
                                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($gallery[0]->url) !!}" alt="{!! $tour->name !!}">
                                    <span class="_label">+{!! $gallery->count() - 5 !!}</span>
                                </a>

                                <div class="d-none">
                                    @foreach($gallery as $image)
                                        <a href="{!! image_url($image->url) !!}" data-fancybox="images">
                                            <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($image->url) !!}" alt="{!! $tour->name !!}"/>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif

                </div>

            </div> <!--./banner-slider-->

            <div class="vk-shop__nav">
                <div class="container">
                    <div class="vk-shop__nav-content">
                        <div class="_left">
                            <div class="vk-menu__nav" id="top-menu">
                                <a href="#overview" data-scroll-to="#overview" data-scroll-offset="70" class="active">{!! __('Overview') !!}</a>
                                @if(!empty($plans))
                                    <a href="#tourPlan" data-scroll-to="#tourPlan" data-scroll-offset="70">{!! __('Tour plan') !!}</a>
                                @endif
                                @if($tour->hotelCities->count() || !empty($hotels))
                                    <a href="#hotelList" data-scroll-to="#hotelList" data-scroll-offset="70">{!! __('Hotel list') !!}</a>
                                @endif
                                @if(!empty($faq))
                                    <a href="#faq" data-scroll-to="#faq" data-scroll-offset="70">FAQs</a>
                                @endif
                                {{--<a href="#makeEnquiry" data-scroll-to="#makeEnquiry" data-scroll-offset="70">{!! __('Make enquiry') !!}</a>--}}
                                @if($relatedTours->count())
                                    <a href="#similarTour" data-scroll-to="#similarTour" data-scroll-offset="70">{!! __('Similar tours') !!}</a>
                                @endif
                            </div>

                        </div>
                        <div class="_right">
                            {{--<a href="{!! route('inquire') !!}" class="vk-btn">{!! __('Book This Tour') !!}</a>--}}
                            <a href="javascript:void(0);"  onclick="goToTourForm(this,event)" class="vk-btn" data-scroll-to="#sidebar"
                               data-scroll-offset="90">{!! __('Book This Tour') !!}</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vk-tour-detail__price d-lg-none">
                <div class="vk-form--tour">
                    <div class="_bot">
                        <div class="_left">
                            <div class="_label-1">{!! __('from') !!}</div>
                            <div class="_total">
                                $ {!! (int)$tour->price_promotion > 0 ? $tour->price_promotion : $tour->price !!}</div>
                        </div>
                        <div class="_right">
                            <a href="#" class="_btn" onclick="goToTourForm(this)" data-scroll-to="#sidebar"
                               data-scroll-offset="90">{!! __('Book This Tour') !!}</a>

                        </div>
                    </div>
                </div>
            </div> <!--./price-->

            <div class="vk-shop-detail__main">

                <div class="container">

                    <div class="vk-shop-detail__wrapper">

                        <div class="_left">

                            <div id="overview" data-scroll-anchor="true">
                                <?php $mainActivities = @$tour->activityMain;?>
                                <ul class="vk-blog-detail__breadcrumb">
                                    <li><a href="{!! route('home') !!}">{!! __('Home') !!}</a></li>
                                    <li><a href="{!! route('activity') !!}">{!! __('Activities') !!}</a></li>
                                    @if(!empty($mainActivities))
                                        <li><a href="{!! route('tourActivity.detail',['slug' => $mainActivities->slug]) !!}">{{ $mainActivities->name }}</a></li>
                                    @endif
                                    <li><a href="{!! route('tour.detail',['slug' => $tour->slug]) !!}">{{ $tourName }}</a></li>

                                </ul>
                                <div class="vk-shop-detail__meta">
                                    <div class="_cat">{!! @$tour->activities->pluck('name')->implode(' & ') !!}</div>
                                    <div class="_date">{!! $tour->duration !!} {!! $tour->duration > 1 ? __('days') : __('day'); !!}</div>
                                </div>
                                <h1 class="vk-shop-detail__title">{!! $tourName !!}
                                    @if(Auth::check())
                                        <span class="admin_editable">
                                    <a href="{!! route('tour.edit',$tour) !!}" title="Sửa" target="_blank">
                                    <i class="fa fa-pencil"></i>
                                    </a>
                                </span>
                                    @endif
                                </h1>
                                <div class="vk-shop-detail__content">
                                    {!! @$content->main !!}
                                    <div class="row">
                                        @if(strlen(@$content->include))
                                            <div class="col-lg-6">
                                                <h2 class="text-body">{!! __('Tour Included') !!}</h2>
                                                {!! $content->include !!}
                                            </div>
                                        @endif

                                        @if(strlen(@$content->exclude))
                                            <div class="col-lg-6">
                                                <h2 class="text-body">{!! __('Tour Excluded') !!}</h2>
                                                {!! $content->exclude !!}
                                            </div>
                                        @endif
                                    </div>

                                </div> <!--./content-->
                            </div> <!--./overview-->

                            @if(!empty($plans))
                                <div id="tourPlan" data-scroll-anchor="true">
                                    <div class="vk-shop-detail__title--style-1">
                                        <h2 class="_title">{!! __('Tour Plan') !!}</h2>

                                            <a href="#tourPlanDownload" data-target="#tourPlanDownload" data-toggle="modal" class="_btn" download><i
                                                        class="_icon fa fa-download"></i> {!! __('Download plan') !!}
                                            </a>

                                        {{--@if(strlen($tour->file))image_url($tour->file)@endif--}}
                                    </div>

                                    <!--BEGIN: ACCORDION-->
                                    <div class="accordion accordion--style-5" id="accordionExample">
                                        <?php $count = 0 ?>
                                        @foreach($plans as $key => $plan)
                                            <div class="card">
                                                <h3 class="card-header">
                                                    <a class="card-link {!! $count == 0 ? null : 'collapsed' !!}"
                                                       data-toggle="collapse"
                                                       href="#collapse-{!! $key !!}">{!! @$plan->name !!}</a>
                                                </h3>

                                                <div id="collapse-{!! $key !!}"
                                                     class="collapse {!! $count == 0 ? 'show' : null !!}"
                                                     data-parent="#accordionExample">
                                                    <div class="card-body">{!! @$plan->content !!}</div>
                                                </div>
                                            </div>
                                            <?php $count++ ?>
                                        @endforeach
                                    </div>
                                    <!--END: ACCORDION-->
                                </div>
                                <div class="pb-lg-60 pb-40"></div>
                            @endif

                            @include('frontend.components.tour-detail-hotel', ['hotelTypes' => $hotelTypes, 'hotels' => $hotels] )

                            @if(!empty($faq))
                                <?php $content = $faq->content;?>
                                <div id="faq" data-scroll-anchor="true">

                                    <div class="vk-shop-detail__title--style-1">
                                        <h2 class="_title">{!! __('FAQs') !!}</h2>
                                    </div>

                                    @include('frontend.components.faq')
                                </div> <!--./faq-->
                                <div class="pb-30 pb-lg-0"></div>
                            @endif

                            @if(!empty(@$reviews))

                                <div class="vk-shop-detail__title--style-1">
                                    <h2 class="_title">{!! __('Reviews') !!}</h2>
                                </div>

                                <div class="vk-blog-detail__comment-list">
                                    <div class="vk-comment__list" id="reviewList">
                                        @include('frontend.components.review',['reviews'=>$reviews])
                                    </div>
                                </div>
                            @endif

                        </div> <!--./left-->


                        <div class="_right">
                            <div class="vk-sidebar--style-1">

                                @if(strlen(@($map->image)) || strlen(@($map->link)) )
                                    <div class="vk-sidebar__box">
                                        <div class="vk-map">
                                            <div class="_content">
                                                @if(strlen(@($map->link)) )
                                                    <a target="_blank"
                                                       href="{!! @$map->link !!}"
                                                       class="vk-img">
                                                        <img
                                                                src="{!! __IMAGE_LAZY_LOAD !!}"
                                                                data-src="{!! image_url(@$map->image ) !!}"
                                                                alt="{!! $tour->name !!}">
                                                    </a>

                                                    <a target="_blank"
                                                       href="{!! @$map->link  !!}"
                                                       class="_btn"><span>{!! __('View full map') !!}</span></a>
                                                @else
                                                    <a href="{!! image_url(@$map->image)!!}" class="vk-img" data-fancybox="map">
                                                        <img src="{!! __IMAGE_LAZY_LOAD !!}"
                                                            data-src="{!! image_url(@$map->image) !!}"
                                                            alt="{!! $tour->name !!}">
                                                    </a>
                                                    <a href="{!! image_url(@$map->image)  !!}" data-fancybox="map1"
                                                       class="_btn"><span>{!! __('View full map') !!}</span></a>

                                                @endif
                                            </div>
                                        </div>
                                    </div> <!--./box-->
                                @endif

                                <div class="vk-sidebar__sticky">
                                    <div class="vk-sidebar__box--style-1" id="sidebar">

                                        <div class="_wrapper">
                                            <div class="vk-form--tour">
                                                <div class="_top">
                                                    <h2 class="_heading">{!! __('Book This Tour') !!}</h2>
                                                </div>

                                                <div class="collapse" id="tourForm">
                                                    <div class="_mid">
                                                        <div class="form-group">
                                                            <div class="_label">{!! __('Your name') !!}</div>
                                                            <input type="text" class="form-control" name="name"
                                                                   value="{!! old('name') !!}" required
                                                                   placeholder="Anthony">
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="_label">{!! __('Phone Number') !!}</div>
                                                            <input type="text" class="form-control" name="phone"
                                                                   value="{!! old('phone') !!}" required
                                                                   placeholder="996-311-5335">
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="_label">{!! __('Email Address') !!}</div>
                                                            <input type="email" class="form-control" name="email"
                                                                   value="{!! old('email') !!}" required
                                                                   placeholder="moen_lenora@kuvalis.name">
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="_text">{!! __('By Continuing with this, You have read and') !!}
                                                                <a href="javascript:void(0)">{!! __('agreed to the terms') !!}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="_bot">
                                                    <div class="_left">
                                                        <div class="_label-1">{!! __('from') !!}</div>
                                                        <div class="_total">
                                                            $ {!! (int)$tour->price_promotion > 0 ? $tour->price_promotion : $tour->price !!}</div>
                                                    </div>
                                                    <div class="_right">

                                                        <button type="button" class="_btn d-lg-none"
                                                                id="tourFormBtnMobile" data-target="#tourForm"
                                                                onclick="tourFormShow(this,event)">
                                                            <span>{!! __('Book This Tour') !!}</span>
                                                        </button>

                                                        <button type="button" class="_btn d-none d-lg-flex"
                                                                data-target="#exampleModal"
                                                                onclick="tourFormShowEx(this,event)">
                                                            <span>{!! __('Book This Tour') !!}</span></button>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div> <!--./box-->


                                    @if($howItWorks->count())
                                        <div class="vk-sidebar__box">

                                            <h2 class="vk-sidebar__title">{!! __('How It Works') !!}</h2>

                                            <div class="vk-sidebar__wrapper">

                                                <div class="vk-sidebar__list--style-1">
                                                    <?php $count = 1 ?>
                                                    @foreach($howItWorks as $step)
                                                        <?php $name = json_decode($step->name);
                                                        $content = json_decode($step->content);
                                                        ?>
                                                        <div class="_item">
                                                            <div class="vk-sidebar-item">
                                                                <div class="vk-sidebar-item__num">
                                                                    0{!! $count++ !!}</div>
                                                                <div class="vk-sidebar-item__brief">
                                                                    <h2 class="vk-sidebar-item__title">{!! @$name->main !!}</h2>
                                                                    <div class="vk-sidebar-item__text">{!! @$content->main !!}</div>
                                                                </div>
                                                            </div>
                                                        </div> <!--./item-->
                                                    @endforeach
                                                </div>

                                            </div>

                                        </div> <!--./box-->
                                    @endif
                                </div>


                            </div> <!--./sidebar-->

                        </div> <!--./right-->

                    </div>

                </div>
                <div class="pb-lg-100 pb-30"></div>

                @include('frontend.components.make_enquiry')

            </div>

            @if($relatedTours->count())
                <div class="vk-shop-detail__slider" id="similarTour" data-scroll-anchor="true">
                    <h2 class="vk-shop-detail__title--style-2">{!! __('Similar tours you may like') !!}</h2>
                    <div class="vk-slider--style-2 vk-shop__list slick-slider" data-slider="tour-detail" {!! \App\Enum\ItemScopeEnum::ITEM_TYPE['ItemList'] !!}>
                        <?php $tourClass = 'vk-shop-item--style-5';$tourWrapper = 'col-12';$tourAnimation = false ?>
                        @foreach($relatedTours as $tour)
                            @include('frontend.components.tour')
                        @endforeach
                    </div>
                </div>
        @endif

        <?php  $configGeneral = json_decode($configGeneral->content);  ?>
        <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog vk-modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">

                            <button type="button" class="close vk-modal__close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            <div class="vk-popup__content">
                                <div class="_left">
                                    <div class="vk-img">
                                        <img src="{!! __IMAGE_LAZY_LOAD !!}"
                                             data-src="{!! strlen(@$configGeneral->site_enquiry_banner) ? image_url(@$configGeneral->site_enquiry_banner) : getAsset('/images/poppup-1.jpg') !!}"
                                             alt="{!! $tour->name !!}">
                                    </div>

                                    <div class="_text">{!! __(':tour in a Tour of :destination',['destination' => $tourDestinations, 'tour' => $tourName]) !!}</div>
                                </div> <!--./left-->

                                <div class="_right">

                                    <h3 class="vk-popup__title"
                                        id="cusGreeting">{!! __('Hello :name',['name'=>'Anthony']) !!} </h3>
                                    <div class="vk-popup__row">
                                        <div class="_label">{!! __('You have requested the tour') !!}</div>

                                        <div class="_text">{!! __(':tour in a Tour of :destination',['destination' => $tourDestinations, 'tour' => $tourName]) !!}</div>
                                    </div>

                                    <div class="vk-popup__row">
                                        <div class="_label">{!! __('Contact information is') !!}</div>
                                        <div class="_text">
                                            <span id="cusPhone">517-510-9924</span> &nbsp; | &nbsp;
                                            <span id="cusEmail">cody.parker@hotmail.com</span>
                                        </div>
                                    </div>

                                    <div class="vk-divider--style-1 mt-20 mb-30"></div>

                                    <div class="vk-form--sale _fix">
                                        @if($errors->has('captcha') || $errors->has('name') || $errors->has('email'))
                                            <div class="alert alert-warning vk-alert-warning" role="alert">
                                                @foreach ($errors->all() as $error)
                                                    {{ $error }}<br>
                                                @endforeach
                                            </div>
                                        @endif

                                        <div class="row">

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="_title">{!! __('Departure date') !!} (YYYY/MM/DD)</div>
                                                    <input type="text" class="form-control vk-datepicker" autocomplete="none"
                                                           name="departure_date" value="{!! old('departure_date') !!}">
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-6">
                                                <div class="form-group">
                                                    <div class="_title">{!! __('Number of Adults') !!}</div>
                                                    <div class="vk-calculator" data-calculator>
                                                        <input type="number" name="number_adult"
                                                               value="{!! old('number_adult',0) !!}" min="0"
                                                               class="form-control">
                                                        <a href="#" class="_btn _plus" data-index="plus"><i
                                                                    class="_icon ti-angle-up"></i></a>
                                                        <a href="#" class="_btn _minus" data-index="minus"><i
                                                                    class="_icon ti-angle-down"></i></a>
                                                    </div> <!--./calculator-->
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-6">
                                                <div class="form-group">
                                                    <div class="_title">{!! __('Number of Children') !!}</div>
                                                    <div class="vk-calculator" data-calculator>
                                                        <input type="number" name="number_child"
                                                               value="{!! old('number_child',0) !!}" min="0"
                                                               class="form-control">
                                                        <a href="#" class="_btn _plus" data-index="plus"><i
                                                                    class="_icon ti-angle-up"></i></a>
                                                        <a href="#" class="_btn _minus" data-index="minus"><i
                                                                    class="_icon ti-angle-down"></i></a>
                                                    </div> <!--./calculator-->
                                                </div>
                                            </div>

                                            <?php $hotels = (array)@$inquire->content->hotel_category; ?>
                                            @if(count($hotels))
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="_title">{!! __('Hotel Category') !!}</div>
                                                        <div class="vk-dropdown vk-dropdown--style-2"
                                                             data-layout="dropdown">
                                                            <div class="_current">
                                                                <div><span class="_text"></span></div>
                                                                <span class="_icon fa fa-angle-down"></span>
                                                            </div>

                                                            <div class="_list collapse">
                                                                <input type="hidden" name="hotel" value="{!! old('hotel') !!}">
                                                                @foreach($hotels as $hotel)
                                                                    <a href="javascript:void(0);" class="{!! old('hotel') == @$hotel ? 'active' : null !!}"
                                                                       data-value="{!! @$hotel !!}">
                                                                        <span class="_content">{!! @$hotel !!}</span>
                                                                    </a>
                                                                @endforeach
                                                            </div>
                                                        </div> <!--./dropdown-->
                                                    </div>
                                                </div>
                                            @endif

                                            <?php $travels = (array)@$inquire->content->travel_style; ?>
                                            @if(count($travels))
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="_title">{!! __('Travel Style') !!}</div>
                                                        <div class="vk-dropdown vk-dropdown--style-2"
                                                             data-layout="dropdown">
                                                            <div class="_current">
                                                                <div><span class="_text"></span></div>
                                                                <span class="_icon fa fa-angle-down"></span>
                                                            </div>

                                                            <div class="_list collapse">
                                                                <input type="hidden" name="activity" value="{!! old('activity') !!}">
                                                                @foreach($travels as $travel)
                                                                    <a href="javascript:void(0);" class="{!! old('activity') == @$travel ? 'active' : null !!}"
                                                                       data-value="{!! @$travel !!}"> <span
                                                                                class="_content">{!! @$travel !!}</span></a>
                                                                @endforeach
                                                            </div>
                                                        </div> <!--./dropdown-->
                                                    </div>
                                                </div>
                                            @endif

                                        </div> <!--./row-->
                                        <div class="vk-divider--style-1 mt-20 mb-20"></div>

                                        @include('frontend.components.captcha')

                                        <div class="vk-button collapse show" id="buttonStep1">
                                            <button type="submit" class="vk-btn _submit">{!! __('Send now') !!}</button>
                                        </div>


                                    </div>


                                </div> <!--./right-->
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </form>
    </section><!--./content-->

    <!-- Modal -->
    <div class="modal fade" id="tourPlanDownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__("The travel itinerary will be sent to your email")}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if($errors->has('customer.name') || $errors->has('customer.email'))
                        <script type="text/javascript">
                            window.addEventListener('DOMContentLoaded', function() {
                                (function($) {
                                    setTimeout(function(){
                                        $('html, body').stop().animate({
                                            scrollTop: $('#tourPlan').offset().top - 70
                                        }, 1000);
                                    },1000);

                                    $('#tourPlanDownload').modal('show');

                                })(jQuery);
                            });
                        </script>

                        <div class="alert alert-warning vk-alert-warning" role="alert">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif

                    <form action="{{route('tour.plan.download',['slug'=>$mainTour->slug])}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input style="border-radius: 0" type="text" name="customer[name]" value="{{ old('customer.name') }}" class="form-control" placeholder="{{__('Your name')}}" required>
                        </div>

                        <div class="form-group">
                            <input style="border-radius: 0" type="email" name="customer[email]" value="{{ old('customer.email') }}" class="form-control" placeholder="{{__('Your Email')}}" required>
                        </div>
                        <div class="text-right">
                            <button type="button" class="vk-btn vk-btn--outline-default" data-dismiss="modal">{{__('Close')}}</button>
                            <button type="submit" class="vk-btn vk-btn--default">{{__('Send')}}</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
