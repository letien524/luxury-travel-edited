@extends('frontend.master')


@section('content')
    <div class="vk-member-detail">

        <div class="vk-member-detail__top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="vk-member-detail__left">
                            <div class="vk-member-detail__avatar">
                                <img src="{{ __IMAGE_LAZY_LOAD }}" data-src="{{ image_url($member->image) }}"
                                     alt="{{ $member->name }}" class="_img">
                            </div>
                            <div class="vk-member-detail__quote">
                                {{ $member->quote }}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="vk-member-detail__right">
                            <div class="vk-member-detail__right-top">
                                <h1 class="vk-member-detail__name">{{ $member->name }}</h1>
                                <div class="vk-member-detail__position">{{ $member->position }}</div>
                                <a href="#contact" data-scroll-to="#contact" data-scroll-offset="100"
                                   class="vk-member-detail__btn">{{ __('Send me email') }}</a>
                            </div>
                            <div class="vk-member-detail__content">{!! $member->content !!}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div> {{--./top--}}

        @if($member->reviews->count())
            <?php $memberReviews = $member->reviews->load('memberTours'); ?>
            <div class="vk-member-detail__feedback">
                <div class="container">
                    <div class="vk-member-detail__feedback-content">
                        <div class="vk-home__title-box vk-home__title-box--style-1" >
                            <h2 class="_title">{{ __('What did our customers say?') }}</h2>
                            <div class="_title-sub">{{ __('We are proud to report that we have a 99% satisfaction rate with much of our business coming from referrals and business network recommendations.') }}</div>
                        </div>
                        <div class="vk-member-detail__feedback-list row vk-slider--style-2"  data-slider="member-feedback">
                            @foreach($memberReviews as $item)
                                @include('frontend.components.member-reviews',[
                                'review' => $item,
                                'col' => 'col-12',
                                ])
                            @endforeach
                        </div>

                        <div class="vk-member-detail__feedback-button">
                            <?php
                            $pageSettings = json_decode(@$pageSettings->content, true);
                            $btnLable = \Illuminate\Support\Str::length(@$pageSettings['button_cta_content']) ? $pageSettings['button_cta_content'] : __('View more');
                            ?>
                            <a href="#contact" data-scroll-to="#contact" data-scroll-offset="100" class="vk-member-detail__feedback-btn"><span>{{ $btnLable }}</span></a>
                        </div>
                    </div>
                </div>
            </div> {{--./customer-say--}}
        @endif




        <div class="vk-member-detail__contact" id="contact">
            <div class="container">
                <div class="vk-home__title-box">
                    <h2 class="_title">{!! __('Contact :name <br> for a wonderful holiday',['name' => $member->name]) !!}</h2>
                </div>

                <div class="vk-member-detail__contact-form">
                    @if($errors->any())
                        <script>
                            window.addEventListener('DOMContentLoaded', function() {
                                (function($) {
                                    setTimeout(function(){
                                        $('html, body').stop().animate({
                                            scrollTop: $("#contact").offset().top - 100

                                        }, 1000);
                                    },1000)

                                })(jQuery);
                            });
                        </script>
                        <div class="alert alert-warning vk-alert-warning" role="alert">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    @include('frontend.components.contact-form', [
                        'action'=> route('contact.store'),
                        'class'=> 'vk-form--contact-member',
                        'submitLabelBtn'=> __('Send message'),
                    ])

                </div>

            </div>
        </div>

        @if($member->tours->count()))
            <?php $meberTours = $member->tours  ?>
            <div class="vk-home__shop">
                <div class="container-fluid">
                    <div class="vk-home__how-content">
                        <div class="vk-home__title-box">
                            <h2 class="_title">{!! __('Highly recommended tours <br> & holiday packages from :name', ['name'=>$member->name]) !!}</h2>
                        </div>
                        <?php
                        $tourWrapper = 'col-12';
                        $tourClass = 'vk-shop-item--style-5';
                        $tourAnimation = false;
                        ?>

                        <div class="vk-shop__list row vk-slider--style-2"  data-slider="tour-detail">
                            @foreach($meberTours as $tour)
                                @include('frontend.components.tour', ['tour' => $tour])
                            @endforeach
                        </div>
                    </div>
                </div>
            </div> <!--./tour-->
        @endif

        @if($member->faqs->count())
            <?php $memberFaq = $member->faqs->first(); ?>
            <div class="vk-member-detail__faq">
                <div class="container">
                    <div class="vk-home__how-content">
                        <div class="vk-home__title-box">
                            <h2 class="_title">{!! __('Talk with :name', ['name'=>$member->name]) !!}</h2>
                        </div>
                    </div>
                    <div class="vk-member-detail__faq-content">
                        @include('frontend.components.faq', ['content' => $memberFaq->content])
                    </div>
                </div>
            </div> <!--./tour-->
        @endif

    </div>


@endsection
