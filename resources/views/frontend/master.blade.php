<?php
    $config_general = json_decode($config_general->content);

    $site_name = @$config_general->site_name;
    $site_meta_title = !empty($metaConfig['meta_title']) ? $metaConfig['meta_title'] : @$config_general->site_meta_title;
    $site_meta_description = !empty($metaConfig['meta_description']) ? $metaConfig['meta_description'] : @$config_general->site_meta_description;
    $site_canonical = !empty($metaConfig['canonical']) ? $metaConfig['canonical'] : url()->current();

    $site_logo = @$config_general->site_logo;
    $site_favicon = @$config_general->site_favicon;
    $site_image = !empty($metaConfig['meta_image']) ? $metaConfig['meta_image'] : @$config_general->site_image;

    $site_search_engine_index = @$config_general->site_search_engine_index;
    $site_hotline = @$config_general->site_hotline;
    $site_email = @$config_general->site_email;
    $site_script_header = @$config_general->site_script_header;
    $site_script_footer = @$config_general->site_script_footer;

    $site_payment = @$config_general->site_payment;



?>
<!doctype html>
<html lang="{!! config('app.locale') !!}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @if($site_search_engine_index == 1)
    <meta name="robots" content="{!! @$metaConfig['index'] == true ? 'index' : 'noindex'  !!},follow">
    @else
    <meta name="robots" content="noindex,nofollow">
    @endif

    <title>{!! $site_meta_title !!}</title>
    <meta name="description" content="{!! $site_meta_description !!}"/>

    {{--Open Graph protocol--}}
    <meta property="og:url" content="{!! URL::current() !!}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{!! $site_meta_title !!}"/>
    <meta property="og:description" content="{!! $site_meta_description !!}" />
    <meta property="og:image:secure_url " content="{!! image_url($site_image) !!}" />
    <meta property="og:image" content="{!! image_url($site_image) !!}"/>

    <link rel="canonical" href="{!! $site_canonical !!}"/>
    <link rel="shortcut icon" href="{!! image_url($site_favicon) !!}" type="image/x-icon"/>

    <style>
        /* vietnamese */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 300;
            src: local('Nunito Sans Light'), local('NunitoSans-Light'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc8WAc5iU1EQVg.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            font-display: swap;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 300;
            src: local('Nunito Sans Light'), local('NunitoSans-Light'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc8WAc5jU1EQVg.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            font-display: swap;
        }
        /* latin */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 300;
            src: local('Nunito Sans Light'), local('NunitoSans-Light'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc8WAc5tU1E.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            font-display: swap;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Sans Regular'), local('NunitoSans-Regular'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe0qMImSLYBIv1o4X1M8cceyI9tScg.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            font-display: swap;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Sans Regular'), local('NunitoSans-Regular'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe0qMImSLYBIv1o4X1M8ccezI9tScg.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            font-display: swap;
        }
        /* latin */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Nunito Sans Regular'), local('NunitoSans-Regular'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe0qMImSLYBIv1o4X1M8cce9I9s.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        font-display: swap;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito Sans SemiBold'), local('NunitoSans-SemiBold'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc9iB85iU1EQVg.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            font-display: swap;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito Sans SemiBold'), local('NunitoSans-SemiBold'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc9iB85jU1EQVg.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            font-display: swap;
        }
        /* latin */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 600;
            src: local('Nunito Sans SemiBold'), local('NunitoSans-SemiBold'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc9iB85tU1E.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            font-display: swap;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Sans Bold'), local('NunitoSans-Bold'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc8GBs5iU1EQVg.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            font-display: swap;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Sans Bold'), local('NunitoSans-Bold'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc8GBs5jU1EQVg.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            font-display: swap;
        }
        /* latin */
        @font-face {
            font-family: 'Nunito Sans';
            font-style: normal;
            font-weight: 700;
            src: local('Nunito Sans Bold'), local('NunitoSans-Bold'), url(https://fonts.gstatic.com/s/nunitosans/v4/pe03MImSLYBIv1o4X1M8cc8GBs5tU1E.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            font-display: swap;
        }


        /* cyrillic */
        @font-face {
            font-display: swap;
            /*font-family: 'Playfair Display';*/
            font-family: 'Freight-Big';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: local('Playfair Display Regular'), local('PlayfairDisplay-Regular'), url(https://fonts.gstatic.com/s/playfairdisplay/v14/nuFiD-vYSZviVYUb_rj3ij__anPXDTjYgFE_.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* vietnamese */
        @font-face {
            font-display: swap;
            /*font-family: 'Playfair Display';*/
            font-family: 'Freight-Big';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: local('Playfair Display Regular'), local('PlayfairDisplay-Regular'), url(https://fonts.gstatic.com/s/playfairdisplay/v14/nuFiD-vYSZviVYUb_rj3ij__anPXDTPYgFE_.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-display: swap;
            /*font-family: 'Playfair Display';*/
            font-family: 'Freight-Big';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: local('Playfair Display Regular'), local('PlayfairDisplay-Regular'), url(https://fonts.gstatic.com/s/playfairdisplay/v14/nuFiD-vYSZviVYUb_rj3ij__anPXDTLYgFE_.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-display: swap;
            /*font-family: 'Playfair Display';*/
            font-family: 'Freight-Big';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: local('Playfair Display Regular'), local('PlayfairDisplay-Regular'), url(https://fonts.gstatic.com/s/playfairdisplay/v14/nuFiD-vYSZviVYUb_rj3ij__anPXDTzYgA.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }


    </style>
    @yield('link')

    <noscript id="deferred-styles">
        <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/fonts/font-awesome/css/font-awesome.min.css">
        <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/fonts/themify-icons/themify-icons.css">
        <link media="all" rel="stylesheet" href="{!! getAsset('') !!}/plugin/slick/slick.min.css">
        <link media="screen and (min-width: 1200px)" rel="stylesheet" href="{!! getAsset('') !!}/plugin/animate/animate.min.css">

    </noscript>
{{--    screen and (min-width: 1200px)--}}
    <link media="all" type="text/css" rel="stylesheet" href="{!! getAsset('') !!}/plugin/animsition/animsition.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="{!! getAsset('') !!}/plugin/bootstrap/css/bootstrap.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="{!! getAsset('') !!}/css/style.css"/>
    <link media="all" type="text/css" rel="stylesheet" href="{!! getAsset('') !!}/css/customize.css"/>
</head>

<script>
    function getCaptchaUrl(){
        return "{{ url('') }}"
    }
</script>

<body class="">
{!! $site_script_header !!}

@include('frontend.components.errors')

<!--animsition-->
<div class="animsition main-wrapper">
    {{--<div class="">--}}

    @include('frontend.components.header')

    @yield('content')


    @include('frontend.components.footer')

    <?php $menu = json_decode($mainMenu->content,true) ?>

    <div class="vk-mmenu" id="menu">

        <div class="_top">

            <ul class="vk-header__list">
                <li><a href="mailto:{!! $site_email !!}"><i class="_icon fa fa-envelope-o"></i>
                        {!! $site_email !!}</a></li>
                <li><a href="tel:{!! $site_hotline !!}"><i class="_icon fa fa-phone"></i> {!! __('Call Us') !!}
                        : {!! $site_hotline !!}</a></li>
            </ul>

            @if(count($menu))
                <ul class="vk-menu__mobile">
                    @foreach($menu as $item)
                        @if($item['destination'] === 'is_destination')
                            @if($destinationMegaMenus->count())
                                <li>
                                    <a href="#collapseExample" data-toggle="collapse" class="collapsed">
                                        {!! __($item['name']) !!}
                                        <i class="_icon fa fa-angle-down"></i>
                                    </a>
                                    <div class="collapse" id="collapseExample">
                                        <div>
                                            <ul class="vk-menu__mobile-child">
                                                @foreach($destinationMegaMenus as $menu)
                                                    <li>
                                                        @if($menu->hasChild())
                                                            <a href="#collapseExampleChild-{{$menu->id}}" data-toggle="collapse" class="collapsed">
                                                                {!! $menu->name !!}
                                                                <i class="_icon fa fa-angle-down"></i>
                                                            </a>

                                                            <div class="collapse" id="collapseExampleChild-{{$menu->id}}">
                                                                <div>
                                                                    <ul class="vk-menu__mobile-child vk-menu__mobile-child--style-1">
                                                                        <a href="{!! route('tourDestination.detail',['slug'=>$menu->slug]) !!}">{!! $menu->name !!}</a>
                                                                        @foreach($menu->child as $child)
                                                                            <a href="{!! route('destination.city',['country'=>$menu->slug, 'city' => $child->slug]) !!}">{!! $child->name !!}</a>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            </div>

                                                        @else
                                                            <a href="{!! route('tourDestination.detail',['slug'=>$menu->slug]) !!}">{!! $menu->name !!}</a>
                                                        @endif



                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @elseif($item['destination'] === 'is_activity')
                            @if($activityMegaMenus->count())
                                <li>
                                    <a href="#collapseExample1" data-toggle="collapse" class="collapsed">{!! __($item['name']) !!} <i
                                                class="_icon fa fa-angle-down"></i></a>
                                    <div class="collapse" id="collapseExample1">
                                        <div>
                                            <ul class="vk-menu__mobile-child">
                                                @foreach($activityMegaMenus as $item)
                                                    <li><a href="{!! route('tourActivity.detail',$item->slug) !!}">{!! $item->name !!}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>

                                    </div>
                                </li>
                            @endif
                        @else
                            <li><a href="{!! url($item['link']) !!}">{!! __($item['name']) !!}</a></li>
                        @endif
                    @endforeach

                    <li><a href="#headerSearch" class="vk-header__btn--search collapsed" data-toggle="collapse"><i  class="_icon fa"></i></a></li>
                </ul>
            @endif
        </div>

        @if($langs->count())
            <div class="vk-dropdown--style-1" data-layout="dropdown--style-4">
                <div class="_current"><span class="_text">{!! __('Language') !!}</span> <span
                            class="_icon fa fa-angle-down"></span></div>
                <div class="_list collapse">
                    @foreach($langs as $lang)
                        <a href="{!! $lang->link !!}"
                           class="{!! $lang->locale == $currentLang ? 'active' : null !!}">{!! $lang->name !!}</a>
                    @endforeach
                </div>
            </div>
            <div class="_bot">
                <div class="vk-mmenu__lang">
                    @foreach($langs as $lang)
                        <a href="{!! $lang->link !!}"
                           class="{!! $lang->locale == $currentLang ? 'active' : null !!}">{!! $lang->name !!}</a>
                    @endforeach
                </div>

            </div>
        @endif

    </div>

    @include('frontend.components.popup', ['popups' => $popups])

</div>
<!--./main-wrapper-->

@stack('schema')

<script async>
    function init() {
        var imgDefer = document.getElementsByTagName('img');
        for (var i=0; i<imgDefer.length; i++) {
            var dataSrc = imgDefer[i].getAttribute('data-src');
            if(dataSrc) {
                imgDefer[i].setAttribute('src',dataSrc);
                imgDefer[i].setAttribute('srcset',dataSrc);
            } } }
    window.onload = init;
</script>

<script async type="text/javascript">
    function asset(){return "{!! url('/frontend') !!}"}
    function backToTop(){return "{!! __('BACK TO TOP') !!}"}
</script>

<script type="text/javascript">
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement);
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
</script>

<!-- BEGIN: SCRIPT -->
<script defer="defer" src="{!! getAsset('') !!}/plugin//jquery/jquery-3.2.1.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//bootstrap/js/bootstrap.bundle.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//slick/slick.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//scrollup/jquery.scrollUp.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//truncate/truncate.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//stickOnScroll/jquery.stickOnScroll.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//animsition/animsition.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//waypoint/jquery.waypoints.min.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin//bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js"></script>
@yield('end')
<script defer="defer" src="{!! getAsset('') !!}/plugin/main.js"></script>
<script defer="defer" src="{!! getAsset('') !!}/plugin/custom.js"></script>
<!-- END: SCRIPT -->
{!! $site_script_footer!!}



</body>
</html>


