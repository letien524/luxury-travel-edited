<div class="_item {{ @$col }}">
    <div class="vk-mdi">
        <div class="vk-mdi__img">
            <img src="{{ __IMAGE_LAZY_LOAD }}" data-src="{{ image_url($review->image) }}" alt="{{ $review->reviewer }}" class="_img">
        </div>
        <div class="vk-mdi__brief">
            <h3 class="vk-mdi__name">{{ $review->reviewer }}</h3>
            <div class="vk-mdi__meta">
                <span>{{ $review->country }}</span>
                <span>{{ \Carbon\Carbon::parse($review->date)->format('d/m/Y') }}</span>
            </div>
            <div class="vk-mdi__content">{!! $review->content !!}</div>

            @if($review->memberTours->count())
                <?php $tourReview = $review->memberTours->first() ?>
                <div class="vk-mdi__tour">
                    <h4 class="vk-mdi__title">{{ __('Travel with us in') }}</h4>
                    <a href="{{ route('tour.detail', $tourReview->slug) }}" title="{{ $tourReview->name }}">{{ $tourReview->name }}</a>
                </div>
            @endif
        </div>
    </div>
</div>
