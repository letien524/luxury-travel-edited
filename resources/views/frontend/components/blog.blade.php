<?php $image = json_decode($blog->image) ?>
<?php $thumbnail = '<img src="'.image_url(@$image->thumbnail).'" data-src="'.image_url(@$image->thumbnail).'" class="_img" width="" height="" alt="'.$blog->name.'">' ?>
<?php $link = isset($metaConfig['page']) ? route('page.detail',['slug'=>$blog->slug]) : route('blog.detail',['slug'=>$blog->slug]); ?>
@if(isset($blogClass))
    @if($blogClass == 'vk-blog-item--style-3')
        <div class="{!! isset($blogWrapper) ? $blogWrapper : 'col-6 col-md-3 col-lg-3'  !!} _item">
            <div class="vk-blog-item {!! $blogClass !!}">
                <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                   title="{!! $blog->name !!}"
                   class="vk-blog-item__img">
                    {!! $thumbnail !!}
                </a>
                <div class="vk-blog-item__brief">
                    <h3 class="vk-blog-item__title">
                        <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                           title="{!! $blog->name !!}">{!! $blog->name !!}</a>
                    </h3>
                </div>
            </div> <!--./vk-blog-item-->
        </div>

    @elseif($blogClass == 'vk-blog-item--style-1')
        <div class="{!! isset($blogWrapper) ? $blogWrapper : 'col-sm-6 col-md-4'  !!} _item">
            <div class="vk-blog-item {!! $blogClass !!}">
                <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}" title="{!! $blog->name !!}"
                   class="vk-blog-item__img">
                    {!! $thumbnail !!}
                </a>

                <div class="vk-blog-item__brief">
                    <h3 class="vk-blog-item__title">
                        <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}" title="{!! $blog->name !!}"
                           data-truncate-lines="">{!! $blog->name !!}</a>
                    </h3>
                    <div class="vk-blog-item__text" data-truncate-lines="2">{!! $blog->content_short !!}</div>
                    <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                       class="vk-blog-item__btn"><span>{!! __('View Details') !!}</span></a>
                </div>
            </div> <!--./vk-blog-item-->
        </div>

    @elseif($blogClass == 'vk-blog-item--style-2')
        <div class="{!! isset($blogWrapper) ? $blogWrapper : 'col-sm-6 col-lg-12'  !!} _item">
            <div class="vk-blog-item {!! $blogClass !!}">
                <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                   title="{!! $blog->name !!}"
                   class="vk-blog-item__img">
                    {!! $thumbnail !!}
                </a>

                <div class="vk-blog-item__brief">
                    <h3 class="vk-blog-item__title">
                        <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                           title="{!! $blog->name !!}">{!! $blog->name !!}</a>
                    </h3>
                    <div class="vk-blog-item__text"
                         data-truncate-lines="3">{!! $blog->content_short !!}</div>
                    <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                       class="vk-blog-item__btn"><span>{!! __('View Details') !!}</span></a>
                </div>
            </div> <!--./vk-blog-item-->
        </div>

    @elseif($blogClass == 'vk-blog-item--style-3')
        <div class="{!! isset($blogWrapper) ? $blogWrapper : 'col-6 col-md-3 col-lg-3'  !!} _item">
            <div class="vk-blog-item {!! $blogClass !!}">
                <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                   title="{!! $blog->name !!}"
                   class="vk-blog-item__img">
                    {!! $thumbnail !!}
                </a>

                <div class="vk-blog-item__brief">
                    <h3 class="vk-blog-item__title">
                        <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                           title="{!! $blog->name !!}">{!! $blog->name !!}</a>
                    </h3>
                </div>
            </div> <!--./vk-blog-item-->
        </div>

    @elseif($blogClass == 'vk-blog-item--style-4')
        <div class="{!! isset($blogWrapper) ? $blogWrapper : 'col-12'  !!} _item">
            <div class="vk-blog-item {!! $blogClass !!}">
                <a href="{!! $link !!}"
                   title="{!! $blog->name !!}"
                   class="vk-blog-item__img">
                    {!! $thumbnail !!}
                </a>

                <div class="vk-blog-item__brief">
                    <h3 class="vk-blog-item__title">
                        <a href="{!! $link !!}"
                           title="{!! $blog->name !!}">{!! $blog->name !!}</a>
                    </h3>
                </div>
            </div> <!--./vk-blog-item-->
        </div>

    @elseif($blogClass == 'vk-blog-item--style-5')
        <div class="{!! isset($blogWrapper) ? $blogWrapper : 'col-sm-6 col-md-4'  !!} _item">
            <div class="vk-blog-item {!! $blogClass !!}">
                <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                   title="How To Play Texas Holdem For Beginners"
                   class="vk-blog-item__img">
                    {!! $thumbnail !!}
                </a>

                <div class="vk-blog-item__brief">
                    <h3 class="vk-blog-item__title">
                        <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                           title="{!! $blog->name !!}"
                           data-truncate-lines="">{!! $blog->name !!}
                        </a>
                    </h3>
                    <div class="vk-blog-item__text"
                         data-truncate-lines="2">{!! $blog->content_short !!}</div>
                    <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                       class="vk-blog-item__btn"><span>{!! __('View Details') !!}</span></a>
                </div>
            </div> <!--./vk-blog-item-->
        </div>

    @endif
@else
    <div class="col-12 _item">
        <div class="vk-blog-item vk-blog-item--style-6"
            {{-- data-animation="fadeInUpFix"
             data-animation-delay="0" data-animation-duration="2"--}}>
            <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
               title="{!! $blog->name !!}"
               class="vk-blog-item__img">
                {!! $thumbnail !!}
            </a>

            <div class="vk-blog-item__brief">
                <h3 class="vk-blog-item__title">
                    <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}" title="{!! $blog->name !!}">{!! $blog->name !!}</a>
                </h3>
                <div class="vk-blog-item__text" data-truncate-lines="3">{!! $blog->content_short !!}</div>
                <a href="{!! route('blog.detail',['slug'=>$blog->slug]) !!}"
                   class="vk-blog-item__btn"><span>{!! __('View Details') !!}</span></a>
            </div>
        </div> <!--./vk-blog-item-->
    </div>
@endif
