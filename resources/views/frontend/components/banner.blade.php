<?php $content = json_decode($config_general->content); ?>
@if(isset($bannerClass))
    <div class="{!! $bannerClass !!}"
         {!! strlen(@$bannerBackground) ? 'style="background-image: url('.image_url($bannerBackground).')"' : null !!})>
        <div class="container">
            <div class="vk-banner__content">
                <div class="_wrapper">
                    <div class="_title">{!! @$content->site_enquiry_text !!}</div>
                    {{--<div class="_title-sub"></div>--}}
                    <a href="{!! route('inquire') !!}" class="_btn">{!! __('Make An Enquiry') !!}</a>
                </div>
            </div>
        </div>
    </div>
@endif