<?php
    $thumbnail = '<img src="'.image_url(@$destination->image).'" data-src="'.image_url(@$destination->image).'" class="_img" width="" height="" alt="'.$destination->name.'">' ;
    $route = route('tourDestination.detail',['slug'=>$destination->slug]);

    if($destination->hasParent()){
        $country = $destination->getParent();

        $destinationRoute = route('destination.city',['country'=>$country->slug, 'city'=>$destination->slug]);
    }else{
        $destinationRoute = route('tourDestination.detail',['slug'=>$destination->slug]);
    }
?>

<div class="col-12 _item">
    <div class="vk-blog-item vk-blog-item--style-6">
        <a href="{!! $destinationRoute !!}"
           title="{!! $destination->name !!}"
           class="vk-blog-item__img">
            {!! $thumbnail !!}
        </a>

        <div class="vk-blog-item__brief">
            <h3 class="vk-blog-item__title">
                <a href="{!! $destinationRoute !!}" title="{!! $destination->name !!}">{!! $destination->name !!}</a>
            </h3>
            <div class="vk-blog-item__text" data-truncate-lines="3">{!! @$destination->excerpt !!}</div>
            <a href="{!! $destinationRoute !!}"
               class="vk-blog-item__btn"><span>{!! __('View Details') !!}</span></a>
        </div>
    </div> <!--./vk-blog-item-->
</div>
