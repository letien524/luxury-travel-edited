
@foreach($popups as $popup)

    <?php
    $attributes = [
        'delay'=>$popup->delay_time,
        'ex_days' => 1, // 1 day
        'popup' => $popup,
    ] ;
    ?>

    @if($popup->is_display)

        @if(!isCurrentPageInBlackList($popup->black_list))
            @include("popup.{$popup->type}", $attributes)
        @endif

    @else

        @if(isCurrentPageInBlackList($popup->black_list))
            @include("popup.{$popup->type}", $attributes)
        @endif

    @endif

@endforeach
