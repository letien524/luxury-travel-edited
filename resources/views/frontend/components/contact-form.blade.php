<div class="{{ !empty($class) ? $class : 'vk-form--contact' }}">
    <form action="{{ $action }}" method="post" onsubmit="this.querySelector('.vk-btn._submit').setAttribute('disabled','disabled')">
        {{csrf_field()}}
        <input type="hidden" name="type" value="{{!empty($type) ? $type : 'customer-contact' }}">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <div class="_label">{!! __('Your Name') !!}
                        ({!! __('required') !!})
                    </div>
                    <input type="text" class="form-control" name="name" required
                           value="{!! old('name') !!}">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <div class="_label">{!! __('Your Email') !!}
                        ({!! __('required') !!})
                    </div>
                    <input type="text" class="form-control" name="email" required
                           value="{!! old('email') !!}">
                </div>
            </div>
        </div> <!--./row-->

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="_label">{!! __('Subject') !!}
                        ({!! __('required') !!})
                    </div>
                    <input type="text" class="form-control" name="content[subject]"
                           required
                           value="{!! old('content.subject') !!}">
                </div>
            </div>


        </div> <!--./row-->

        <div class="form-group">
            <div class="_label">{!! __('Your Message') !!}</div>
            <textarea class="form-control" placeholder="{!! __('Write here') !!}"
                      name="content[message]">{!! old('content.message') !!}</textarea>

        </div>
        <div class="vk-form--sale">
            @include('frontend.components.captcha')
        </div>

        <div class="vk-button">
            <button class="vk-btn _submit" type="submit">{!! !empty($submitLabelBtn) ? $submitLabelBtn : __('submit') !!}</button>
        </div>
    </form>
</div>
