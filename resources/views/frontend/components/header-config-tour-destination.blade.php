<?php
$configTourMenuDestination = json_decode($configTourMenuDestination->content, true);
$configTourMenuDestination = @$configTourMenuDestination['tourMenuDestination'];
?>
@if(@$configTourMenuDestination['display'] == 1)
    <?php $configTourMenuDestinationName = strlen(@$configTourMenuDestination['name']) ? $configTourMenuDestination['name'] : __('Unable to find…'); ?>
    <div class="_item _hidden">
        <div class="_box _find">
            <div class="vk-img">
                <h3 class="_title">{{ $configTourMenuDestinationName }}</h3>
            </div>

            <a href="{{ @$configTourMenuDestination['link'] }}"
               class="_link"
               title="{{ $configTourMenuDestinationName }}"></a>
        </div>
    </div>
@endif
