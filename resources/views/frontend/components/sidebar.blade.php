<?php $content = $config->content; ?>

<div class="vk-sidebar--style-2">
    @if(!isset($metaConfig['page']))
        @if($categories->count())
            <div class="vk-sidebar__box d-none d-lg-block">
                <h2 class="vk-sidebar__title">{!! __('Categories') !!}</h2>
                <ul class="vk-sidebar__list">
                    @foreach($categories as $category)
                        <li><a href="{!! route('blogCategory.detail',$category->slug) !!}"> {!! $category->name !!}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endif


    @if($trendingBlogs->count())
        <div class="vk-sidebar__box d-none d-lg-block">
            <h2 class="vk-sidebar__title">{!! isset($metaConfig['page']) ? __('About us') : __('Trending Posts') !!}</h2>
            <div class="vk-blog__list--style-1 row">
                @foreach($trendingBlogs as $blog)
                    <?php $blogClass = 'vk-blog-item--style-4'; ?>
                    @include('frontend.components.blog')
                @endforeach
            </div>

        </div>
    @endif


    <div class="vk-sidebar__box--style-1">
        <h2 class="vk-sidebar__title">{!! __('Subscribe') !!}</h2>
        <p>{!! __('Leave your email to receive Free Travel Guides, Travel Tips and Lastest Deals from us') !!}</p>
        <div class="vk-form--newsregister">
        <form action="{!! route('subscribe.store') !!}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="type" value="customer-subscribe">

            <input type="email" class="form-control" name="email" placeholder="{!! __('Your email') !!}" required>
            <button class="vk-btn" type="submit"><i class="_icon fa fa-paper-plane"></i></button>
        </form>

        </div>

    </div>
        @if(!empty(@$content->banner))
            <div class="vk-sidebar__box d-none d-lg-block">
                <a href="{!! @$content->link !!}" class="vk-img vk-img--mw100">
                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url(@$content->banner) !!}" alt="{!! @$config->meta_title !!}">
                </a>
            </div>
        @endif

</div>
