<?php $content = json_decode($config_general->content);?>
<div class="vk-banner--style-3" id="makeEnquiry" data-scroll-anchor="true" {!! strlen(@$content->site_enquiry_image) ? 'style="background-image: url('.image_url(@$content->site_enquiry_image).');"' : null !!}>
    <div class="container">
        <div class="vk-banner__content">
            <div class="_title">{!! @$content->site_enquiry_text !!}
            </div>

            <div class="vk-banner__list row">
                <div class="_item col-lg-4">
                    <div class="vk-banner-item">
                        <div class="vk-banner-item__img"><img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! getAsset('') !!}/images/icon-1.png" alt=""></div>
                        <div class="vk-banner-item__brief">
                            <h2 class="vk-banner-item__title">{!! __('Contact us') !!}</h2>
                            <div class="vk-banner-item__text">{!! __('Call Us') !!}: <a href="tel:{!! @$content->site_hotline !!}">{!! @$content->site_hotline !!}</a></div>
                        </div>
                    </div>
                </div> <!--./item-->

                <div class="_item col-lg-4">
                    <div class="vk-banner-item">
                        <div class="vk-banner-item__img"><img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! getAsset('') !!}/images/icon-2.png" alt=""></div>
                        <div class="vk-banner-item__brief">
                            <h2 class="vk-banner-item__title">{!! __('Contact us') !!}</h2>
                            {{--<div class="vk-banner-item__text"><a href="mailto:{!! @$content->site_email !!}">{!! __('Send Enquiry') !!}</a></div>--}}
                            <div class="vk-banner-item__text"><a href="{!! route('inquire') !!}">{!! __('Send Enquiry') !!}</a></div>
                        </div>
                    </div>
                </div> <!--./item-->

                <div class="_item col-lg-4">
                    <div class="vk-banner-item">
                        <div class="vk-banner-item__img"><img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! getAsset('') !!}/images/icon-3.png" alt=""></div>
                        <div class="vk-banner-item__brief">
                            <h2 class="vk-banner-item__title">{!! __('Mail Us') !!}</h2>
                            <div class="vk-banner-item__text"><a href="mailto:{!! @$content->site_email !!}">{!! @$content->site_email !!}</a>
                            </div>
                        </div>
                    </div>
                </div> <!--./item-->
            </div>
        </div>
    </div>
</div>