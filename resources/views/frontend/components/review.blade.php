@foreach($reviews as $review)
    <div class="_item" data-animation="fadeIn"
         data-animation-delay="0.{!! $loop->iteration !!}"
         data-animation-duration="1">

        <div class="vk-comment-item vk-comment-item--style-1">
            <div class="vk-comment-item__top">
                <div class="_left">
                    <h3 class="vk-comment-item__title">{{$review->name}}</h3>
                    <div class="vk-comment-item__star">
                        @for ($i = 0; $i < 5; $i++)
                            <i class="fa fa-star {{$review->rate > $i ? "text-warning" : null }}"></i>
                        @endfor
                    </div>
                </div>
                <div class="_right"><div class="vk-comment-item__date">{{\Carbon\Carbon::parse($review->submited_at)->format('m/d/Y')}}</div></div>

            </div>

            <div class="vk-comment-item__text">
                <b>{!! $review->reviewer !!}</b> {!! $review->content !!}</div>
        </div>
    </div>
@endforeach
{!! $reviews->links() !!}
