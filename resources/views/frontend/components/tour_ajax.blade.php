<?php $count = 0;?>
@foreach($tours as $tour)
    <?php
    $mainActivities = @$tour->activities->pluck('name')->first();
    $tourLocation = $tour->hotelCities->count() ? $tour->hotelCities->implode('name', ' - ') : null;
    ?>
    <div class="{!! isset($tourWrapper) ? $tourWrapper : 'col-sm-6 col-lg-4' !!} _item">
        @include('schema.tour-item', ['tour' => $tour])
        <div class="vk-shop-item {!! isset($tourClass) ? $tourClass : 'vk-shop-item--style-1' !!}"
             data-animation="fadeIn"
             data-animation-delay="0.{!! $count++ !!}"
             data-animation-duration="1">
            <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}" title="{!! $tour->name !!}"
               class="vk-shop-item__img">
                @if(strlen($mainActivities))
                    <span class="vk-shop-item__cat">{!! $mainActivities !!}</span>
                @endif

                <img src="{!! image_url($tour->image) !!}" alt="{!! $tour->name !!}" class="_img">

            </a>


            <div class="vk-shop-item__brief">
                <div
                    class="vk-shop-item__date">{!! $tour->duration !!} {!! $tour->duration > 1 ? __('DAYS') : __('DAY') !!}</div>
                <div class="vk-shop-item__box">
                    <h3 class="vk-shop-item__title">
                        <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                           title="{!! $tour->name !!}">{!! $tour->name !!}</a></h3>
                    @include('frontend.components.tour-location', ['location' => $tourLocation])
                    @include('frontend.components.tour-excerpt', ['excerpt' => $tour->excerpt])
                    <div class="vk-shop-item__button">
                        <div class="vk-shop-item__price">
                            @if(strlen($tour->price_promotion) && (int)$tour->price_promotion > 0)
                                <span class="_current">${!! $tour->price_promotion !!}</span>
                                <span class="_old">${!! $tour->price !!}</span>
                            @else
                                <span class="_current">${!! $tour->price !!}</span>
                            @endif

                        </div>
                        <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                           class="vk-shop-item__btn"><span>{!! __('View Details') !!}</span></a>
                    </div>
                </div>


            </div>
        </div> <!--./vk-shop-item-->
    </div>
@endforeach

@include('frontend.components.tour-search-item',[
                       'tours' => $tours,
                       'tourSearchConfig' => @$tourSearchConfig,
                   ])

<script>
    var anchor = $('[data-animation]');

    anchor.waypoint(function (direction) {
        var el = $(this.element);
        var animationName = el.data('animation');
        var animationDuration = el.data('animation-duration');
        var animationDelay = el.data('animation-delay');

        el.css('opacity', 1);
        if (animationDuration) {

            el.css({
                "-webkit-animation-duration": animationDuration + "s",
                "animation-duration": animationDuration + "s"
            })
        }

        if (animationDelay) {

            el.css({
                "-webkit-animation-delay": animationDelay + "s",
                "animation-delay": animationDelay + "s"
            })
        }

        el.addClass('animated ' + animationName);
    }, {
        offset: '100%',
        triggerOnce: true
    });

</script>




