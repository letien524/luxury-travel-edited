@if($destinationMegaMenus->count())
    <div class="vk-menu__mega active" data-parent="#destinationMega" id="tourDestination">
        <div class="_mega_content">
            <div class="container">
                <div class="_list">
                    @foreach($destinationMegaMenus as $menu)
                        <?php
                        if ($menu->parent_id) {
                            $country = $menu->parent;
                            $destinationRoute = route('destination.city', ['country' => $country->slug, 'city' => $menu->slug]);

                        } else {
                            $destinationRoute = route('tourDestination.detail', ['slug' => $menu->slug]);
                        }
                        $hasChild = $menu->child->count() ? 'true' : 'false';
                        ?>
                        <div class="_item {{ $destinationMegaMenus->count() > 10 && $loop->index > 8 ? '_hidden' : null }}">
                            <div class="_box">
                                <div class="vk-img">
                                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($menu->image) !!}"
                                         alt="" width="" height="">
                                </div>
                                <h3 class="_title">{!! $menu->name !!}</h3>
                                <a href="{!! $destinationRoute !!}"
                                   class="_link"
                                   onclick="tourDestinationAction(event, this, {{ $hasChild }}, '#tourDestinationChild-{{ $menu->id }}')"
                                   title="{!! $menu->name !!}"></a>
                            </div>
                        </div>
                    @endforeach

                    @include('frontend.components.header-config-tour-destination', ['configTourMenuDestination' => $configTourMenuDestination] )

                    @include('frontend.components.header-destination-more', ['destinationMegaMenus' => $destinationMegaMenus] )

                </div>
            </div>
        </div>
    </div>

    @foreach($destinationMegaMenus as $menu)
        <?php
        $childMenu = $menu->child;
        ?>

        @if($childMenu->count())
            <?php $childMenuId = $menu->id; ?>
            <div class="vk-mega-child" id="tourDestinationChild-{{ $childMenuId }}">

                <div class="vk-mega-child__wrapper">
                    <div class="container">
                        <div class="vk-mega-child__content">
                            <div class="vk-mega-child__left">
                                <div class="vk-mega-child__parent">
                                    <div class="_box">
                                        <div class="vk-img">
                                            <img src="{!! __IMAGE_LAZY_LOAD !!}"
                                                 data-src="{!! image_url($menu->image) !!}"
                                                 alt="" width="" height="">
                                        </div>
                                        <h3 class="_title">{!! $menu->name !!}</h3>
                                        <a href="{!! route('tourDestination.detail', ['slug' => $menu->slug]) !!}"
                                           class="_link"
                                           title="{!! $menu->name !!}"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="vk-mega-child__right">
                                <?php
                                $childMenu = $childMenu->split(3);
                                ?>

                                <div class="row">
                                    @foreach($childMenu as $menus)
                                        <div class="col-lg-4">
                                            <ul class="vk-mega__list {{$loop->first ? '_first' : ''}}">
                                                @if($loop->first)
                                                <li class="_first">
                                                    <a href="{{ route('tourDestination.detail', ['slug' => $menu->slug]) }}"
                                                       title="{{ $menu->name }}">{{ $menu->name }}</a>
                                                </li>
                                                @endif
                                                @foreach($menus as $item)
                                                    <?php
                                                    $country = $item->parent;
                                                    $destinationRoute = route('destination.city', ['country' => $country->slug, 'city' => $item->slug]);
                                                    ?>
                                                    <li>
                                                        <a href="{{ $destinationRoute }}"
                                                           title="{{ $item->name }}">{{ $item->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endforeach
                                </div>

                                    <a href="#" onclick="tourDestinationBackList(event, this, '#tourDestination')" class="vk-mega-child__back">
                                        <img src="{{ getAsset('images/icon-left-arrow.png') }}" alt="">
                                        {{ __('All Destinations') }}
                                    </a>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        @endif

    @endforeach

@endif

<div class="vk-menu__mega" data-parent="#activityMega">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="vk-mega__left">
                    <h3 class="vk-mega__title">{!! __('All activities') !!}</h3>
                    @if($activityMegaMenus->count())
                        <?php
                        $activityMegaMenus = $activityMegaMenus->split(2)
                        ?>
                        <div class="vk-mega__content">
                            <div class="row">
                                @foreach($activityMegaMenus as $menus)
                                    <div class="col-lg-6">
                                        <ul class="vk-mega__list">
                                            @foreach($menus as $item)
                                                <li>
                                                    <a href="{!! route('tourActivity.detail',$item->slug) !!}">{!! $item->name !!}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-lg-8">

                <div class="vk-mega__right">
                    <h3 class="vk-mega__title">{!! __('Popular Tours') !!}</h3>
                    @if($tourHighlights->count())
                        <div class="vk-mega__list--style-1 row" {!! \App\Enum\ItemScopeEnum::ITEM_TYPE['ItemList'] !!}>
                            <?php $tourClass = 'vk-mega-item' ?>
                            @foreach($tourHighlights as $tour)
                                @include('frontend.components.tour')
                            @endforeach

                        </div>
                    @endif

                </div>

            </div>
        </div>
    </div>
</div>

<header class="vk-header {!! @$headerStyle !!}"
        data-layout="{!! isset($headerStyle)&& $headerStyle == 'vk-header--style-1'  ? null :"sticky" !!}">
    <div class="vk-header__top">

        <div class="vk-header__top-content">
            <div class="_left">
                <a href="#menu" onclick="menuToggle(this,event)" class="vk-header__btn--menu"><i
                        class="ti-menu"></i></a>
                <ul class="vk-header__list">
                    <li><a href="mailto:{!! $site_email !!}"><i class="_icon fa fa-envelope-o"></i> {!! $site_email !!}
                        </a></li>
                    <li><a href="tel:{!! $site_hotline !!}"><i class="_icon fa fa-phone"></i> {!! __('Call Us') !!}
                            : {!! $site_hotline !!}</a></li>
                </ul>
            </div>
            <div class="_mid">
                <a href="{!! route('home') !!}" class="vk-header__logo">
                    <img src="{!! __IMAGE_LAZY_LOAD !!}"
                         data-src="{!! !empty($site_logo) ? image_url($site_logo) : getAsset('/images/logo-1.png') !!}"
                         alt="{!! $site_name !!}" width="" height="">
                </a>
            </div>
            <div class="_right">

                <a href="#headerSearch" class="vk-header__btn--menu" data-toggle="collapse"><i
                        class="_icon fa fa-search"></i></a>

                <div class="d-none d-lg-flex">
                    @if($langs->count())
                        <div class="vk-dropdown--style-1" data-layout="dropdown--style-4">
                            <div class="_current"><span class="_text">{!! __('Language') !!}</span> <span
                                    class="_icon fa fa-angle-down"></span></div>
                            <div class="_list collapse">
                                @foreach($langs as $lang)
                                    <a href="{{ $lang->link }}"
                                       class="{!! $lang->locale == $currentLang ? 'active' : null !!}">{!! __($lang->name) !!}</a>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    <a href="{!! route('inquire') !!}"
                       class="vk-header__btn vk-header__btn--top">{!! __('Inquire Now') !!}</a>
                </div>

            </div>
        </div>

    </div> <!--./top-->
    <?php $menu = json_decode($mainMenu->content, true) ?>

    <div class="vk-header__bot">
        <div class="container">
            <div class="vk-header__bot-content">
                <nav class="vk-header__menu">

                    @if(count($menu))
                        <ul class="vk-menu__main">
                            @foreach($menu as $item)
                                @if($item['destination'] === 'is_destination')
                                    <li class="_destination" id="destinationMega"><a
                                            href="javascript:void(0)">{!! __($item['name']) !!}</a></li>
                                @elseif($item['destination'] === 'is_activity')
                                    <li class="_destination" id="activityMega"><a
                                            href="{!! url($item['link']) !!}">{!! __($item['name']) !!}</a></li>
                                @else
                                    <li><a href="{!! url($item['link']) !!}">{!! __($item['name']) !!}</a></li>
                                @endif
                            @endforeach

                            <li><a href="#headerSearch" class="vk-header__btn--search collapsed" data-toggle="collapse"><i
                                        class="_icon fa"></i></a></li>
                        </ul>
                    @endif

                </nav>
            </div>
        </div> <!--./container-->
    </div> <!--./bot-->

    <div class="collapse" id="headerSearch">
        <div class="vk-header__before">
            <div class="container">
                <div class="vk-header__search">
                    <div class="vk-form vk-form--search">
                        <form action="{!! route('tour.search') !!}" method="get" autocomplete="off">
                            <input type="text" name="key" class="form-control"
                                   placeholder="|{!! __('Search on site...') !!}">
                            <!--<button class="vk-btn"><i class="ti-search"></i></button>-->
                        </form>
                    </div>
                </div> <!--./search-->
            </div>
        </div>
    </div> <!--./headerSearch-->


</header> <!--./vk-header-->




