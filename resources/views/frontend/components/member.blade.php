<?php
$image = image_url($member->image);
$name = $member->name;
$link = route('member.detail', $member->slug);


?>

<div class="col-md-6 _item">
    <div class="vk-team-item ">
        <a href="{{ $link }}" class="vk-team-item__img" title="{{ $name }}">
            <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{{ $image }}" alt="{{ $name }}" class="_img">
        </a>

        <div class="vk-team-item__brief">
            <h3 class="vk-team-item__title"><a href="{{ $link }}" title="{{ $name }}">{{ $name }}</a></h3>
            <div class="vk-team-item__position">{{ $member->position }}</div>
            <div class="vk-team-item__text">{!! $member->excerpt !!} <a href="{{ $link }}" class="vk-team-item__view-more">{{ __('View more') }}</a></div>
            <div class="vk-team-item__button">
                <a href="{{ "{$link}#contact" }}" class="vk-team-item__btn">{{ __('Send me email') }}</a>
            </div>
        </div>
    </div> <!--./vk-team-item-->
</div>
