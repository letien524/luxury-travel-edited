@if(@$type === 'login')
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <div class="_title">{{__('Please enter the code above')}}</div>
                <input type="text" class="form-control" name="captcha" placeholder="" autocomplete="off" required>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <div class="_title"><span>{{__('Recaptcha')}}</span><a class="pull-right" href="javascript:void(0);" onclick="captchaChange(this,'{{route("get.captcha")}}')"><i class="_icon fa fa-repeat"></i> {{__('Change Image')}}</a></div>
                <img src="{{ $captcha }}" alt="">
            </div>
        </div>
    </div>
@else
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <div class="_title">{{__('Please enter the code above')}}</div>
                <input type="text" class="form-control" name="captcha" placeholder="" autocomplete="off">
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <div class="_title"><span>{{__('Recaptcha')}}</span><a href="javascript:void(0);" onclick="captchaChange(this,'{{route("get.captcha")}}')"><i class="_icon fa fa-repeat"></i> {{__('Change Image')}}</a></div>
                <img src="{{ $captcha }}" alt="">
            </div>
        </div>
    </div>
@endif

<div class="row" style="overflow: hidden; height: 0">
    <div class="col-lg-12">
        <div class="form-group">
            <div class="_title">{{__('Nood bot')}}</div>
            <input type="text" class="form-control" name="c[captcha]" placeholder="" autocomplete="off">
        </div>
    </div>
</div>