@if($destinationMegaMenus->count() > 10 )
    <div class="_item">
        <div class="_box _more">
            <h3 class="_title"><span>{{ __('View more') }} +</span></h3>
            <a href="javascript:void(0);" onclick="itemMenuShow(this)" class="_link"
               title="{{ __('View more') }} +"></a>
        </div>
    </div>
@endif
