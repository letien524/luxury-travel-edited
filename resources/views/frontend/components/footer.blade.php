<footer class="vk-footer pt-60">

    @if($associationBrands->count())
        <div class="vk-association pb-60">
            <div class="container">
                <h2 class="vk-heading--style-1"><span>{!! __('Our Associations') !!}</span></h2>
                <div class="slick-slider vk-association__slider" data-slider="association">
                    @foreach($associationBrands as $brand)
                        <div class="_item">
                            <div class="vk-img vk-img--mw100">
                                <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($brand->image) !!}" alt="{!! $brand->name !!}" width="" height="">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div> <!--./association-->
    @endif

    @if($awardBrands->count())
        <div class="vk-award pb-60">
            <div class="container">
                <h2 class="vk-heading--style-1"><span>{!! __('Our Awards') !!}</span></h2>
                <div class="vk-award__list">
                    @foreach($awardBrands as $brand)
                        <div class="_item">
                            <div class="vk-img vk-img--mw100">
                                <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($brand->image) !!}" alt="{!! $brand->name !!}" width="" height="">
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    @endif

    @if($newsBrands->count())
        <div class="vk-brand pb-60">
            <div class="container">
                <h2 class="vk-heading--style-1"><span>{!! __('In The News') !!}</span></h2>
                <div class="vk-brand__list slick-slider" data-slider="brand">
                    @foreach($newsBrands as $brand)
                        <div>
                            <div class="_item">
                                <div class="vk-img vk-img--mw100">
                                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($brand->image) !!}" alt="{!! $brand->name !!}" width="" height="">
                                </div>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div> <!--./brand-->
    @endif


    <div class="vk-footer__top">
        <div class="container">
            <div class="vk-footer__top-content row">
                <div class="col-lg-3">
                    <div class="vk-footer__item first">

                        <h2 class="vk-footer__title">{!! __('Our Offices') !!}</h2>

                        @if($locales->count())
                            <ul class="vk-footer__list--style-1">
                                @foreach($locales as $locale)
                                    <li>
                                        <div class="vk-footer__addr">
                                            <div class="_title">{!! $locale->name !!}:</div>
                                            <div class="_text">{!! $locale->address !!}</div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endif


                    </div> <!--./item-->
                </div> <!--./col-->

                <?php
                $config_footer_menu = json_decode($config_footer_menu->content);
                $footerMenus = @$config_footer_menu;
                ?>

                <div class="col-lg-3 col-6">
                    <div class="vk-footer__item">
                        @if(!empty(@$footerMenus->luxury_travel))
                            <h2 class="vk-footer__title">{!! __('Luxury Travel') !!}</h2>
                        <div class="vk-footer__list vk-footer__list--style-3">
                            {!! @$footerMenus->luxury_travel !!}
                        </div>

                        @endif

                    </div> <!--./item-->
                </div> <!--./col-->

                <div class="col-lg-3  col-6">
                    <div class="vk-footer__item">
                        <!--vk-footer__list vk-footer__list--style-3-->
                        @if(!empty(@$footerMenus->information))
                            <h2 class="vk-footer__title">{!! __('Information') !!}</h2>
                            {!! @$footerMenus->information !!}
                        @endif

                    </div> <!--./item-->
                </div> <!--./col-->

                <div class="col-lg-3">
                    <div class="vk-footer__item">
                        <h2 class="vk-footer__title">{!! __('Accept payment via') !!}</h2>
                        <br>
                        <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! !empty($site_payment) ? image_url($site_payment) : getAsset('/images/visa.png') !!}" alt="" width="" height="">
                        <br><br>
                        <br>
                        <?php
                        $socials = json_decode($config_social->content);
                        $socials = @$socials->social;
                        ?>

                        @if(!empty($socials))
                            <h2 class="vk-footer__title">{!! __('Social') !!}</h2>
                            <ul class="vk-footer__list--style-1">
                                @if(!empty(@$socials->facebook))
                                @endif
                                @if(!empty(@$socials->facebook))
                                    <li><a href="{!! $socials->facebook !!}"><i class="fa fa-facebook-f"></i></a></li>
                                @endif
                                @if(!empty(@$socials->twitter))
                                    <li><a href="{!! $socials->twitter !!}"><i class="fa fa-twitter"></i></a></li>
                                @endif
                                @if(!empty(@$socials->tripadvisor))
                                    <li><a href="{!! $socials->tripadvisor !!}"><i class="fa fa-tripadvisor"></i></a>
                                    </li>
                                @endif
                                @if(!empty(@$socials->google_plus))
                                    <li><a href="{!! $socials->google_plus !!}"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                @endif
                                @if(!empty(@$socials->whatsapp))
                                    <li><a href="{!! $socials->whatsapp!!}"><i class="fa fa-whatsapp"></i></a></li>
                                @endif
                                @if(!empty(@$socials->skype))
                                    <li><a href="{!! $socials->skype !!}"><i class="fa fa-skype"></i></a></li>
                                @endif
                                @if(!empty(@$socials->youtube))
                                    <li><a href="{!! $socials->youtube !!}"><i class="fa fa-youtube"></i></a></li>
                                @endif
                            </ul>
                        @endif

                    </div> <!--./item-->
                </div> <!--./col-->

            </div> <!--./top-content-->
        </div> <!--./container-->
    </div> <!--./top-->


    <div class="vk-footer__bot">
        <div class="container">
            <div class="vk-footer__bot-content">
                <div class="_left">

                    {!! __('© Luxury Travel Co., Ltd.') !!} <br class="d-lg-none">
                    &nbsp; <br class="d-lg-none">
                    &nbsp;
                    {!! __("Vietnam's First Luxury Tour Operator and DMC, International Tour Operator Licence 01-074/TCDL-GPLHQT") !!}
                    <br class="d-lg-none">
                    <br class="d-lg-none">
                </div>
                <div class="_right">
                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! !empty($site_secure) ? $site_secure : getAsset('/images/footer-1.png') !!}" alt="" width="" height="">
                </div>
            </div>
        </div>
    </div> <!--./vk-footer__bot-->
</footer><!--./vk-footer-->