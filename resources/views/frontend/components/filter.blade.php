<?php
$budgetUrl = Request::get('budget');
$durationUrl = Request::get('duration');
$destinationUrl = Request::get('destination');
$activityUrl = Request::get('activity');

$tourFilterContent = json_decode($tourFilters->content);
$durationFilters = (array)@$tourFilterContent->filter_duration;
$priceFilters = (array)@$tourFilterContent->filter_price;


?>
@if(isset($filterType))
    @if($filterType == 'destination')
        <form action="{!! route('tour.search') !!}" method="get">
            <input type="hidden" name="destination" value="{!! $destination->slug !!}">
            <div class="vk-shop__top vk-shop__top--style-1 hide" data-layout="fix-top">
                <div class="container">
                    <div class="vk-shop__top-content">
                        <div class="vk-search vk-search--style-2">
                            <div class="_left">
                                @if(count($activities))

                                    <?php
                                    $activityUrlArray = explode(',', $activityUrl);
                                    $activityNames = \App\TourActivity::whereIn('slug', $activityUrlArray)->get()->pluck('name')->toArray();
                                    ?>
                                    <div class="_item">
                                        <div class="_title">{!! __('Travel Style') !!}</div>


                                        <div class="vk-dropdown vk-dropdown--style-2" data-layout="dropdown">
                                            <div class="_current">
                                                <div><span class="_text">{!! __('All Styles') !!}</span></div>
                                                <span class="_icon fa fa-angle-down"></span>
                                            </div>

                                            <div class="_list collapse">
                                                <div class="_list_wrapper">
                                                    <input type="hidden" name="activity" value="{!! $activityUrl !!}">

                                                    <a href="javascript:void(0)" rel="nofollow" data-value=""
                                                       class="{!! strlen($activityUrl) == 0 ? 'active' : null !!}">
                                                        <span class="_content">{!! __('All Travel Styles') !!}</span>
                                                    </a>

                                                    @foreach($activities as $key => $val)
                                                        <a href="javascript:void(0)" rel="nofollow"
                                                           class="{!! in_array($val->slug,$activityUrlArray) ? 'active' : null !!}"
                                                           data-value="{!! $val->slug !!}">
                                                            <span class="_content">{!! $val->name !!}</span>
                                                        </a>
                                                    @endforeach

                                                </div>
                                            </div>
                                        </div> <!--./dropdown-->

                                    </div> <!--./item-->

                                @endif

                                @if(count($durationFilters))

                                    <?php $durationFilters = collect($durationFilters) ?>

                                    <div class="_item">
                                        <div class="_title">{!! __('Duration of the trip') !!}</div>
                                        <div class="vk-dropdown vk-dropdown--style-2" data-layout="dropdown">
                                            <div class="_current">
                                                <div><span class="_text">{!! __('All Durations') !!}</span></div>
                                                <span class="_icon fa fa-angle-down"></span>
                                            </div>

                                            <div class="_list collapse">
                                                <div class="_list_wrapper">
                                                    <input type="hidden" name="duration" value="{!! $durationUrl !!}">
                                                    <a href="javascript:void(0)" rel="nofollow" data-value=""
                                                       class="{!! strlen($durationUrl) == 0 ? 'active' : null !!}">
                                                        <span class="_content">{!! __('All Durations') !!}</span>
                                                    </a>
                                                    @foreach($durationFilters as $filter)
                                                        <?php $value = @$filter->min . '-' . @$filter->max; ?>
                                                        <a href="javascript:void(0)" rel="nofollow"
                                                           class="{!!  $value == $durationUrl ? "active" : null !!}"
                                                           data-value="{!!$value !!}">
                                                            <span class="_content">{!! @$filter->name !!}</span>
                                                        </a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div> <!--./dropdown-->
                                    </div> <!--./item-->
                                @endif
                            </div>

                            <div class="_right">
                                <button class="vk-btn"><span>{!! __('Find tour') !!}</span></button>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="vk-button">
                    <a href="#" class="_btn" onclick="filterShow(this,event)"><i class="_icon fa"></i> <span
                                class="_text">{!! __('Show filter') !!}</span></a>
                </div>
            </div>
        </form>
    @else
        <form action="{!! route('tour.search') !!}" method="get">
            <div class="vk-search vk-search--style-3 d-none d-lg-flex">
                <div class="_left">
                    <div class="_item">
                        <div class="vk-dropdown vk-dropdown--style-3"
                             data-layout="dropdown-style-2">
                            <div class="_current">
                                <div class="_title">{!! __('What are you looking for?') !!}</div>
                                <div class="_field">
                                    <input type="text" class="form-control" name="key"
                                           placeholder="{!! __('Destination, City or Tour…') !!}">
                                    <button class="_btn"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div> <!--./dropdown-->

                    </div> <!--./item-->

                    @if(count($durationFilters))
                        <?php $durationFilters = collect($durationFilters); ?>


                        <div class="_item">
                            <div class="vk-dropdown vk-dropdown--style-2" data-layout="dropdown">
                                <div class="_current">
                                    <div class="_title">{!! __('Duration of the trip') !!}</div>
                                    <div>
                                        <span class="_text"></span>
                                    </div>
                                    <span class="_icon fa fa-angle-down"></span>
                                </div>


                                <div class="_list collapse">
                                    <div class="_list_wrapper">
                                        <input type="hidden" name="duration" value="{!! $durationUrl !!}">
                                        <a href="javascript:void(0)" rel="nofollow" data-value=""
                                           class="{!! strlen($durationUrl) == 0 ? 'active' : null !!}">
                                            <span class="_content">{!! __('All Durations') !!}</span>
                                        </a>
                                        @foreach($durationFilters as $filter)
                                            <?php $value = @$filter->min . '-' . @$filter->max; ?>
                                            <a href="javascript:void(0)" rel="nofollow"
                                               class="{!!  $value == $durationUrl ? "active" : null !!}"
                                               data-value="{!!$value !!}">
                                                <span class="_content">{!! @$filter->name !!}</span>
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            </div> <!--./dropdown-->
                        </div> <!--./item-->
                    @endif
                </div>

                <div class="_right">
                    <button class="vk-btn"><span>{!! __('Search tour') !!}</span></button>
                </div>

            </div>
        </form>
    @endif

@else
    <form action="{!! route('tour.search') !!}" method="get">
        <input type="hidden" name="key" value="{{ request('key') }}">
        <div class="vk-shop__top hide" data-layout="fix-top">
            <div class="container">
                <div class="vk-shop__top-content">
                    <div class="vk-search vk-search--style-1">
                        <div class="_left">
                            @if(count($priceFilters))
                                <?php $priceFilters = collect($priceFilters); ?>

                                <div class="_item">
                                    <div class="vk-dropdown vk-dropdown--style-2" data-layout="dropdown">
                                        <div class="_current">
                                            <div class="_title">{!! __('Budget') !!}</div>
                                            <div><span class="_text">{!!  __('All Budgets') !!}</span></div>
                                            <span class="_icon fa fa-angle-down"></span>
                                        </div>

                                        <div class="_list collapse">
                                            <div class="_list_wrapper">
                                                <input type="hidden" name="budget" value="{!! $budgetUrl !!}">
                                                <a href="javascript:void(0)" rel="nofollow" data-value=""
                                                   class="{!! strlen($budgetUrl) == 0 ? 'active' : null !!}">
                                                    <span class="_content">{!! __('All Budgets') !!}</span>
                                                </a>
                                                <?php $count = 0 ?>
                                                @foreach($priceFilters as $filter)
                                                    <?php $value = @$filter->min . '-' . @$filter->max; ?>
                                                    <a href="javascript:void(0)" rel="nofollow"
                                                       class="{!!  $value == $budgetUrl ? "active" : null !!}"
                                                       data-value="{!! $value !!}">
                                                        <span class="_content">{!! @$filter->name !!}</span>
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div> <!--./dropdown-->
                                </div> <!--./item-->
                            @endif

                            @if(count($durationFilters))
                                <?php $durationFilters = collect($durationFilters) ?>
                                <div class="_item">
                                    <div class="vk-dropdown vk-dropdown--style-2" data-layout="dropdown">
                                        <div class="_current">
                                            <div class="_title">{!! __('Duration') !!}</div>
                                            <div><span class="_text">{!! __('All Durations') !!}</span></div>
                                            <span class="_icon fa fa-angle-down"></span>
                                        </div>

                                        <div class="_list collapse">
                                            <div class="_list_wrapper">
                                                <input type="hidden" name="duration" value="{!! $durationUrl !!}">
                                                <a href="javascript:void(0)" rel="nofollow" data-value=""
                                                   class="{!! strlen($durationUrl) == 0 ? 'active' : null !!}">
                                                    <span class="_content">{!! __('All Durations') !!}</span>
                                                </a>
                                                @foreach($durationFilters as $filter)
                                                    <?php $value = @$filter->min . '-' . @$filter->max; ?>
                                                    <a href="javascript:void(0)" rel="nofollow"
                                                       class="{!!  $value == $durationUrl ? "active" : null !!}"
                                                       data-value="{!!$value !!}">
                                                        <span class="_content">{!! @$filter->name !!}</span>
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div> <!--./dropdown-->
                                </div> <!--./item-->
                            @endif

                            @if(count($destinations))
                                <?php
                                $destinationUrlArray = explode(',', $destinationUrl);
                                $destinationNames = \App\TourDestination::whereIn('slug', $destinationUrlArray)->get()->pluck('name')->toArray();
                                ?>
                                <div class="_item">
                                    <div class="vk-dropdown vk-dropdown--style-3" data-layout="dropdown-style-3"
                                         data-placeholder="{!! __('All Destinations') !!}">
                                        <div class="_current">
                                            <div class="_title">{!! __('Destination') !!}</div>
                                            <div>
                                                <span class="_text">{!! strlen($destinationUrl) ? implode(", ",$destinationNames) : __('All Destinations') !!}</span>
                                            </div>
                                            <span class="_icon fa fa-angle-down"></span>
                                        </div>
                                        <div class="_list collapse">
                                            <div class="_list_wrapper">
                                                <input type="hidden" name="destination" value="{!! $destinationUrl !!}">

                                                @foreach($destinations as $key => $val)
                                                    <a href="javascript:void(0)" rel="nofollow" onclick="itemDropdownSelect(this)"
                                                       class="{!! in_array($val->slug,$destinationUrlArray) ? 'active' : null !!}"
                                                       data-value="{!! $val->slug !!}"> {!! $val->name !!} </a>
                                                @endforeach
                                            </div>

                                        </div>
                                    </div> <!--./dropdown-->

                                </div> <!--./item-->
                            @endif

                            @if(count($activities))
                                <?php
                                $activityUrlArray = explode(',', $activityUrl);
                                $activityNames = \App\TourActivity::whereIn('slug', $activityUrlArray)->get()->pluck('name')->toArray();
                                ?>
                                <div class="_item">
                                    <div class="vk-dropdown vk-dropdown--style-3" data-layout="dropdown-style-2"
                                         data-placeholder="{!! __('All Styles') !!}">
                                        <div class="_current">
                                            <div class="_title">{!! __('Style') !!}</div>
                                            <div>
                                                <span class="_text">{!! strlen($activityUrl) ? implode(", ",$activityNames) : __('All Styles') !!}</span>
                                            </div>
                                            <span class="_icon fa fa-angle-down"></span>
                                        </div>

                                        <div class="_list collapse">
                                            <div class="_list_wrapper">
                                                <input type="hidden" name="activity" value="{!! $activityUrl !!}">

                                                @foreach($activities as $key => $val)
                                                    <a href="javascript:void(0)" rel="nofollow" onclick="itemDropdownSelect(this)"
                                                       class="{!! in_array($val->slug,$activityUrlArray) ? 'active' : null !!}"
                                                       data-value="{!! $val->slug !!}"> {!! $val->name !!} </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div> <!--./dropdown-->


                                </div> <!--./item-->
                            @endif
                        </div>

                        <div class="_right">
                            <button class="vk-btn"><span>{!! __('Search tour') !!}</span></button>
                        </div>

                    </div>

                </div>
            </div>
            <div class="vk-button">
                <a href="#" class="_btn" onclick="filterShow(this,event)"><i class="_icon fa"></i> <span
                            class="_text">{!! __('Show filter') !!}</span></a>
            </div>
        </div>
    </form>
@endif
