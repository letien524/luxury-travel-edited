<!--BEGIN: ACCORDION-->
<div class="accordion accordion--style-4" id="accordionExample" itemscope itemtype="https://schema.org/FAQPage">
    <?php $count = 0; $content = (array) $content?>
    @foreach($content as $key => $faq)
        <div class="card" itemprop="mainEntity" itemscope itemtype="https://schema.org/Question">
            <h3 class="card-header">
                <a class="card-link {!! $count == 0 ? null : 'collapsed' !!}"
                   data-toggle="collapse"
                   href="#collapseOne-{!! $key !!}">
                    <span itemprop="name">{!! @$faq['name'] !!}</span>
                </a>
            </h3>

            <div id="collapseOne-{!! $key !!}"
                 class="collapse {!! $count == 0 ? 'show' : null !!}"
                 aria-labelledby="headingOne"
                 itemprop="acceptedAnswer" itemscope itemtype="https://schema.org/Answer">
                <div class="card-body" itemprop="text">{!! @$faq['content'] !!}</div>
            </div>
        </div>
        <?php $count++ ?>
    @endforeach
</div>
{{--@include('schema.faq',['faqs' => $content])--}}
<!--END: ACCORDION-->
