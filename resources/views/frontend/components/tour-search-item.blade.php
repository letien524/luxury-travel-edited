@if( $tours->currentPage() == $tours->lastPage() && @$tourSearchConfig->display == 1)
    <?php
    $name = strlen(@$tourSearchConfig->name) ? $tourSearchConfig->name : __("Unable to find what you're looking for?");
    $btnLabel = strlen(@$tourSearchConfig->btnLabel) ? $tourSearchConfig->btnLabel : __('CONTACT US NOW');
    $link = @$tourSearchConfig->link;
    ?>

    <div class="col-sm-6 col-lg-4 _item"
         data-animation="fadeIn"
         data-animation-delay="0.15"
         data-animation-duration="1">
        <div class="vk-shop-search">
            <div class="vk-shop-search__wrapper">
                <div class="vk-shop-search__content">
                    <h3 class="vk-shop-search__name"><a href="{{ $link }}" title="{{ $name }}">{{ $name }}</a></h3>
                    <a href="{{ $link }}" class="vk-shop-search__btn"><span>{{ $btnLabel }}</span></a>
                </div>
            </div>
        </div> <!--./vk-shop-item-->
    </div>
@endif
