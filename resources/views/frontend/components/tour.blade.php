<?php $image = '<img src="' . __IMAGE_LAZY_LOAD . '" data-src="' . image_url($tour->image) . '" class="_img" width="" height="" alt="' . $tour->name . '">';

if (@$tourActivityActive === true) {
    $mainActivities = $category->name;
} else {
    $mainActivities = @$tour->activities->pluck('name')->first();
}

$tourLocation = $tour->hotelCities->count() ? $tour->hotelCities->implode('name', ' - ') : null;

$priceNotSale = '<span class="_current">$' . $tour->price . '</span>'

?>
@if(isset($tourClass))

    @if($tourClass == 'vk-shop-item--style-3')

        <div class="{!! isset($tourWrapper) ? $tourWrapper : 'col-12' !!} _item">
            @include('schema.tour-item', ['tour' => $tour])
            <div class="vk-shop-item {!! $tourClass !!}">
                <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                   title="{!! $tour->name !!}"
                   class="vk-shop-item__img">
                    {!! $image !!}
                </a>

                <div class="vk-shop-item__brief">
                    <h3 class="vk-shop-item__title">
                        <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                           title="{!! $tour->name !!}">{!! $tour->name !!}</a></h3>
                    @include('frontend.components.tour-location', ['location' => $tourLocation])
                    @include('frontend.components.tour-excerpt', ['excerpt' => $tour->excerpt])

                </div>
            </div> <!--./vk-shop-item-->
        </div>
    @elseif($tourClass == 'vk-mega-item')
        <div class="col-lg-6">
            @include('schema.tour-item', ['tour' => $tour])
            <div class="{!! $tourClass !!}">
                <div class="vk-mega-item__img">
                    {!! $image !!}
                </div>
                <div class="vk-mega-item__brief">
                    <h3 class="vk-mega-item__title">{!! $mainActivities !!}</h3>
                    <div class="vk-mega-item__text">{!! $tour->name !!}</div>
                </div>
                <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}" class="vk-mega-item__link"
                   title="{!! $tour->name !!}"></a>
            </div>
        </div> <!--./item-->
    @elseif($tourClass == 'vk-shop-item--style-5')
        <div class="{!! isset($tourWrapper) ? $tourWrapper : 'col-sm-6 col-lg-4' !!} _item">
            @include('schema.tour-item', ['tour' => $tour])
            <div class="vk-shop-item {!! $tourClass !!}"
                 @if(@$tourAnimation !== false)
                 data-animation=""
                 data-animation-delay="0"
                 data-animation-duration="1.5"
                    @endif
            >
                <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}" title="{!! $tour->name !!}"
                   class="vk-shop-item__img">
                    @if(strlen($mainActivities))
                        <span class="vk-shop-item__cat">{!! $mainActivities !!}</span>
                    @endif

                    {!! $image !!}
                </a>


                <div class="vk-shop-item__brief">
                    <div class="vk-shop-item__date">{!! $tour->duration !!} {!! $tour->duration > 1 ? __('DAYS') : __('DAY') !!}</div>
                    <div class="vk-shop-item__box">
                        <h3 class="vk-shop-item__title">
                            <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                               title="{!! $tour->name !!}">{!! $tour->name !!}</a></h3>
                        @include('frontend.components.tour-location', ['location' => $tourLocation])
                        @include('frontend.components.tour-excerpt', ['excerpt' => $tour->excerpt])
                        <div class="vk-shop-item__button">
                            <div class="vk-shop-item__price">
                                @if(strlen($tour->price_promotion) && (int)$tour->price_promotion > 0)
                                    <span class="_current">${!! $tour->price_promotion !!}</span>
                                    <span class="_old">${!! $tour->price !!}</span>
                                @else
                                    {!! $priceNotSale !!}
                                @endif

                            </div>
                            <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                               class="vk-shop-item__btn"><span>{!! __('View Details') !!}</span></a>
                        </div>
                    </div>


                </div>
            </div> <!--./vk-shop-item-->
        </div>
    @endif
@else
    <div class="{!! isset($tourWrapper) ? $tourWrapper : 'col-sm-6 col-lg-4' !!} _item">
        @include('schema.tour-item', ['tour' => $tour])
        <div class="vk-shop-item {!! isset($tourClass) ? $tourClass : 'vk-shop-item--style-1' !!}" data-animation=""
             data-animation-delay="{{ isset($delay) ? $delay/10 : 0  }}"
             data-animation-duration="1.5">
            <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}" title="{!! $tour->name !!}"
               class="vk-shop-item__img">
                @if(strlen($mainActivities))
                    <span class="vk-shop-item__cat">{!! $mainActivities !!}</span>
                @endif

                {!! $image !!}
            </a>

            <div class="vk-shop-item__brief">
                <div class="vk-shop-item__date">{!! $tour->duration !!} {!! $tour->duration > 1 ? __('DAYS') : __('DAY') !!}</div>
                <div class="vk-shop-item__box">
                    <h3 class="vk-shop-item__title">
                        <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                           title="{!! $tour->name !!}">{!! $tour->name !!}</a></h3>
                    @include('frontend.components.tour-location', ['location' => $tourLocation])
                    @include('frontend.components.tour-excerpt', ['excerpt' => $tour->excerpt])
                    <div class="vk-shop-item__button">
                        <div class="vk-shop-item__price">
                            @if(strlen($tour->price_promotion) && (int)$tour->price_promotion > 0)
                                <span class="_current">${!! $tour->price_promotion !!}</span>
                                <span class="_old">${!! $tour->price !!}</span>
                            @else
                                {!! $priceNotSale !!}
                            @endif

                        </div>
                        <a href="{!! route('tour.detail',['slug'=>$tour->slug]) !!}"
                           class="vk-shop-item__btn"><span>{!! __('View Details') !!}</span></a>
                    </div>
                </div>


            </div>
        </div> <!--./vk-shop-item-->
    </div>
@endif
