
@if(isset($exception) && strlen($exception->getMessage()))
    <script>alert('{!! $exception->getMessage() !!}')</script>
@endif

@if(session('flash_message'))
    <script>alert('{!! session('flash_message') !!}')</script>
@endif

{{--
@if($errors->any())
    <script>
        var erros = "{!! implode(', ',$errors->all()) !!}";
        alert(erros)
    </script>
    </div>
@endif
--}}


{{--
@if($errors->any())
    @if(strlen(@$target))
        <script>
            window.addEventListener('DOMContentLoaded', function() {
                (function($) {
                    $('html, body').stop().animate({
                        scrollTop: $("{{$target}}").offset().top - 80

                    }, 1000);

                })(jQuery);
            });
        </script>
    @endif
    <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $error)
            {{ $error }}<br>
        @endforeach
    </div>
@endif--}}
