@if(strlen($location))
    <div class="vk-shop-item__location" title="{{$location}}">
        <img src="/frontend/images/icon-map.svg" alt="" class="_icon">
        {{ $location }}
    </div>
@endif
