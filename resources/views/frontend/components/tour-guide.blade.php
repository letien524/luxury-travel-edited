<div class="vk-destiny__guide">
    <div class="container">
        <div class="vk-destiny__guide-content">
            <?php
            $content = @$category->faq->content;
            ?>
            <div class="_left">
                <h2 class="vk-destiny__title--style-2">{!! __('Travel Guide') !!}</h2>
                @include('frontend.components.faq')
            </div> {{--./left--}}


            <?php $tourGuide = $category->member; ?>
            <div class="_right">
                @if(!empty($tourGuide))
                    <div class="vk-destiny__box">
                        <h2 class="_title">{!! __('Meet our Experts') !!}</h2>
                        <div class="vk-guide-item">
                            <div class="vk-guide-item__img">
                                <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! image_url($tourGuide->image) !!}" alt="{!! $tourGuide->name !!}">
                            </div>
                            <div class="vk-guide-item__brief">
                                <h3 class="vk-guide-item__title">{!! $tourGuide->name !!}</h3>
                                <div class="vk-guide-item__text">{!! $tourGuide->excerpt !!}</div>
                            </div>
                        </div>
                    </div>
                @endif
                <?php
                $configGeneral = json_decode($configGeneral->content);
                $configSocial = json_decode($configSocial->content);

                ?>
                <div class="vk-destiny__box">
                    <div class="vk-guide-hotline">
                        <a href="tel:{!! @$configGeneral->site_hotline !!}"
                           class="vk-guide-hotline__num">{!! @$configGeneral->site_hotline !!}</a>
                        <div class="vk-guide-hotline__text">{!! __('We offer luxury tour packages to Southeast Asia') !!}</div>
                        <div class="vk-guide-hotline__list">
                            @if(!empty(@$configSocial->social->skype))
                                <a href="{!! @$configSocial->social->skype !!}" target="_blank"><i
                                            class="_icon fa fa-skype"></i></a>
                            @endif
                            @if(!empty(@$configSocial->social->google_plus))
                                <a href="{!! @$configSocial->social->google_plus !!}" target="_blank"><i
                                            class="_icon fa fa-google-plus"></i></a>
                            @endif
                            @if(!empty(@$configSocial->social->whatsapp))
                                <a href="{!! @$configSocial->social->whatsapp !!}" target="_blank"><i
                                            class="_icon fa fa-whatsapp"></i></a>
                            @endif
                        </div>
                        <a href="mailto:{!! @$configGeneral->site_email !!}"
                           class="vk-guide-hotline__email">{!! @$configGeneral->site_email !!}    </a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>