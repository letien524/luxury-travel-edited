@if($hotelTypes->count())
    <div id="hotelList" data-scroll-anchor="true">
        <div class="vk-shop-detail__title--style-1">
            <h2 class="_title">{!! __('Hotel list') !!}</h2>
        </div>
        <div class="vk-hotel__table--heading d-none d-lg-block">
            <div class="_row">
                <div class="_col">{!! __('City') !!}</div>
                <div class="_col">{!! __('Hotel list') !!}</div>
                <div class="_col">{!! __('Room type') !!}</div>
            </div>
        </div>
        <!--BEGIN: ACCORDION-->
        <div class="accordion accordion--style-6" id="accordionExample2">
            @foreach($hotelTypes as $key => $hotelType)
                <div class="card">
                    <h3 class="card-header">
                        <a class="card-link" data-toggle="collapse"
                           href="#collapseOne-{!! $key !!}">{!! $hotelType->name !!}</a>
                    </h3>

                    <div id="collapseOne-{!! $key !!}" class="collapse show"
                         data-parent="#accordionExample">
                        <div class="card-body">
                            @if($hotelType->hotels->count())
                                <div class="vk-hotel__table">
                                    @foreach($hotelType->hotels as $hotel)
                                        <div class="_row">
                                            <div class="_col _col-title">{!! __('City') !!}</div>
                                            <div class="_col">{!! @$hotel->city->name !!}</div>
                                            <div class="_col _col-title">{!! __('Hotel list') !!}</div>
                                            <div class="_col">{!! @$hotel->name !!}</div>
                                            <div class="_col _col-title">{!! __('Room type') !!}</div>
                                            <div class="_col">{!! @$hotel->roomType->name !!}</div>
                                        </div>
                                    @endforeach
                                </div> <!--./table-->
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!--END: ACCORDION-->
    </div> <!--./hotelList-->
    <div class="pb-lg-60 pb-40"></div>

@elseif(!empty($hotels))
    <div id="hotelList" data-scroll-anchor="true">
        <div class="vk-shop-detail__title--style-1">
            <h2 class="_title">{!! __('Hotel list') !!}</h2>
        </div>
        <div class="vk-hotel__table--heading d-none d-lg-block">
            <div class="_row">
                <div class="_col">{!! __('City') !!}</div>
                <div class="_col">{!! __('Hotel list') !!}</div>
                <div class="_col">{!! __('Room type') !!}</div>
            </div>
        </div>
        <!--BEGIN: ACCORDION-->
        <div class="accordion accordion--style-6" id="accordionExample2">
            @foreach($hotels as $key => $hotel)
                <?php $hotelContents = (array)@$hotel->content ?>
                <div class="card">
                    <h3 class="card-header">
                        <a class="card-link" data-toggle="collapse"
                           href="#collapseOne-{!! $key !!}">{!! @$hotel->type !!}</a>
                    </h3>

                    <div id="collapseOne-{!! $key !!}" class="collapse show"
                         data-parent="#accordionExample">
                        <div class="card-body">
                            @if(!empty($hotelContents))
                                <div class="vk-hotel__table">
                                    @foreach($hotelContents as $hotelContent)
                                        <div class="_row">
                                            <div class="_col _col-title">{!! __('City') !!}</div>
                                            <div class="_col">{!! @$hotelContent->city !!}</div>
                                            <div class="_col _col-title">{!! __('Hotel list') !!}</div>
                                            <div class="_col">{!! @$hotelContent->name !!}</div>
                                            <div class="_col _col-title">{!! __('Room type') !!}</div>
                                            <div class="_col">{!! @$hotelContent->type !!}</div>
                                        </div>
                                    @endforeach
                                </div> <!--./table-->
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!--END: ACCORDION-->
    </div> <!--./hotelList-->
    <div class="pb-lg-60 pb-40"></div>
@endif
