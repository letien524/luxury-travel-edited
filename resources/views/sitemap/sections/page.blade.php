@foreach ($pages as $page)
    <?php
    $loc = route('page.detail',['slug'=>$page['slug']]);
    $lastmod =  $page['updated_at']->tz('UTC')->toAtomString();
    ?>
    @include('sitemap.components.url',['loc'=>$loc, 'lastmod'=>$lastmod])
@endforeach
