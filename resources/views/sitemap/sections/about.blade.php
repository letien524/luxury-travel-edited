<?php
    $loc = route('about');
    $lastmod = $about['updated_at']->tz('UTC')->toAtomString();
?>
@include('sitemap.components.url',['loc'=>$loc, 'lastmod'=>$lastmod])

