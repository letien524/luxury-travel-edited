@foreach ($destinations as $destination)
    <?php
    if($destination->hasParent()){
        $country = $destination->getParent();

        $loc = route('destination.city',['country'=>$country->slug, 'city'=>$destination->slug]);
    }else{
        $loc = route('tourDestination.detail',['slug'=>$destination->slug]);
    }

    $lastmod =  $destination['updated_at']->tz('UTC')->toAtomString();
    ?>
    @include('sitemap.components.url',['loc'=>$loc, 'lastmod'=>$lastmod])
@endforeach
