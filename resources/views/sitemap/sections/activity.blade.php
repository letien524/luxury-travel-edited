<?php
$loc = route('activity');
$lastmod =  $activityPage['updated_at']->tz('UTC')->toAtomString();
?>
@include('sitemap.components.url',['loc'=>$loc, 'lastmod'=>$lastmod])
