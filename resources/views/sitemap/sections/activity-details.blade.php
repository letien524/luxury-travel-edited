@foreach ($activities as $activity)
    <?php
    $loc = route('tourActivity.detail',['slug'=>$activity['slug']]);
    $lastmod =  $activity['updated_at']->tz('UTC')->toAtomString();
    ?>
    @include('sitemap.components.url',['loc'=>$loc, 'lastmod'=>$lastmod])
@endforeach
