@foreach ($tours as $tour)
    <?php
    $loc = route('tour.detail',['slug'=>$tour['slug']]);
    $lastmod =  $tour['updated_at']->tz('UTC')->toAtomString();
    ?>
    @include('sitemap.components.url',['loc'=>$loc, 'lastmod'=>$lastmod])
@endforeach
