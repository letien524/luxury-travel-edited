<url>
    <loc>{{@$loc}}</loc>
    <lastmod>{{@$lastmod}}</lastmod>
    <changefreq>weekly</changefreq>
    <priority>0.8</priority>
</url>