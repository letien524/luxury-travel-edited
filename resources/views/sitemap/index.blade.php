{!! '<?xml version="1.0" encoding="UTF-8"?>' !!}
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   @include('sitemap.sections.home')
   @include('sitemap.sections.about')
   @include('sitemap.sections.contact')
   @include('sitemap.sections.activity')
   @include('sitemap.sections.activity-details')
   @include('sitemap.sections.destinations')
   @include('sitemap.sections.tour')
   @include('sitemap.sections.page')

</urlset>