<div class="modal fade" id="{{@$popup}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog vk-modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="vk-inquire__content" style="width: 100%;">

                <div class="vk-inquire__mid">

                    <div class="vk-sale__bbuster-content--style-1">
                        <div class="_wrapper">

                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            <div class="_bot">
                                <div class="vk-inquire__title-box mb-30" id="notifyContent">
                                    <img src="{!! __IMAGE_LAZY_LOAD !!}" data-src="{!! getAsset('') !!}/images/icon-11.png" alt=""> <br><br>
                                    <h2 class="_title">{!! @$name !!}</h2>
                                    <div class="_text">{!! @$content !!}</div>
                                </div>

                                <div class="vk-form--sale">

                                    <div class="vk-button">
                                        <button data-dismiss="modal" class="vk-btn">{!! __('Close') !!}</button>
                                    </div>

                                </div>

                            </div> <!--./bot-->
                        </div>
                    </div>
                </div> <!--./mid-->
            </div>
        </div>
    </div>
</div>

@if(strlen(@$popup))
    <script>
        window.addEventListener('DOMContentLoaded', function() {
            (function($) {
                $('#{{$popup}}').modal('show');

            })(jQuery);
        });
    </script>
@endif
