@extends('frontend.master')

@section('content')

<section class="vk-content">
	<div class="vk-404">
		<div class="container">
			<div class="vk-404__wrapper">
				<div class="vk-404__content">
					<h1 class="vk-404__title">404</h1>
					<div class="vk-404__title-sub">{!! __('Oops, We cannot find the page you are looking for. Try refreshing the page or click the button below to go back the Home Page.') !!}</div>
					<a href="{!! route('home') !!}" class="vk-404__btn">{!! __('Back to Home') !!}</a>
				</div>
			</div>
		</div>
	</div>
</section><!--./content-->


@endsection