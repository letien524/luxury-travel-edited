@extends('frontend.master')

@section('content')


	<section class="vk-content">
		<div class="vk-404">
			<div class="container">
				<div class="vk-404__wrapper">
					<div class="vk-404__content">
						<h1 class="vk-404__title">403</h1>
						<div class="vk-404__title-sub">{!! __('Unauthorized action!') !!}</div>
						<a href="{!! route('home') !!}" class="vk-404__btn">{!! __('Back to Home') !!}</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--./content-->
    
@endsection