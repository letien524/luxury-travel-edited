@extends('backend.index')
@section('controller','Tin tức')
@section('controller_route',route('blog.index'))
@section('action','Danh sách')
@section('live',route('blog'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
             'routeBulkDestroy' => route('blog.bulkDestroy'),
             'routeCreate' => route('blog.create'),
             'datatables' => $datatables,
             'object' => new \App\Http\Controllers\BlogController,
         ])
@endsection