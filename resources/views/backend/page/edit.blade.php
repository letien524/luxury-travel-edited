@extends('backend.index')
@section('controller','Trang')
@section('controller_route',route('page.index'))
@section('action','Cập nhật')
@section('live',route('page.detail',$page->slug))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('page.update',$page) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Nội dung bài viết</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <?php $image = (array)json_decode($page->image); ?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Ảnh đại diện</label>
                                        <div class="image">
                                            <div class="image__thumbnail">
                                                <img src="{{ image_url(old('image.thumbnail',@$image['thumbnail']) ? old('image.thumbnail',@$image['thumbnail']) : null) }}"  data-init="{{ image_url('') }}">
                                                <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                                <input type="hidden" name="image[thumbnail]" value="{{ old('image.thumbnail',@$image['thumbnail'])  }}">
                                                <div class="image__button" onclick="fileSelect(this)">
                                                    <i class="fa fa-upload"></i>
                                                    Upload
                                                </div>
                                            </div>
                                        </div> {{--./image--}}

                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Banner</label>
                                        <div class="image">
                                            <div class="image__thumbnail">
                                                <img src="{{ image_url(old('image.banner',@$image['banner']) ? old('image.banner',@$image['banner']) : null) }}"  data-init="{{ image_url('') }}">
                                                <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                                <input type="hidden" name="image[banner]" value="{{ old('image.banner',@$image['banner'])  }}">
                                                <div class="image__button" onclick="fileSelect(this)">
                                                    <i class="fa fa-upload"></i>
                                                    Upload
                                                </div>
                                            </div>
                                        </div> {{--./image--}}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" data-target="#slug" name="name"
                                       value="{!! old('name',$page->name) !!}">
                            </div>
                            <div class="form-group">
                                <label>Đường dẫn tĩnh</label>
                                <input type="text" class="form-control" id="slug" name="slug"
                                       value="{!! old('slug',$page->slug) !!}">
                            </div>

                        </div>
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title',$page->meta_title) !!}">
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="5">{!! old('meta_description',$page->meta_description) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="status" value="1" {!! $page->status == 1 ? 'checked' : null !!}>
                                        Hiển thị
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <hr>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" class="form-control content">{!! old('content',$page->content) !!}</textarea>
                            </div>

                        </div>
                    </div> {{--./row--}}


                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->


        <button type="submit" class="btn btn-primary">OK</button>
    </form>

@endsection
