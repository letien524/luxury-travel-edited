@extends('backend.index')
@section('controller','Trang')
@section('controller_route',route('page.index'))
@section('action','Thêm')
@section('live',route('blog'))
@section('content')

    @include('backend.block.error')


    <form action="{!! route('page.store') !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="type" value="page">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Nội dung bài viết</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Ảnh đại diện</label>
                                        <div class="image">
                                            <div class="image__thumbnail">
                                                <img src="{{ image_url(old('image.thumbnail') ? old('image.thumbnail') : null) }}"  data-init="{{ image_url('') }}">
                                                <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                                <input type="hidden" name="image[thumbnail]" value="{{ old('image.thumbnail')  }}">
                                                <div class="image__button" onclick="fileSelect(this)">
                                                    <i class="fa fa-upload"></i>
                                                    Upload
                                                </div>
                                            </div>
                                        </div> {{--./image--}}

                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Banner</label>
                                        <div class="image">
                                            <div class="image__thumbnail">
                                                <img src="{{ image_url(old('image.banner') ? old('image.banner') : null) }}"  data-init="{{ image_url('') }}">
                                                <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                                <input type="hidden" name="image[banner]" value="{{ old('image.banner')  }}">
                                                <div class="image__button" onclick="fileSelect(this)">
                                                    <i class="fa fa-upload"></i>
                                                    Upload
                                                </div>
                                            </div>
                                        </div> {{--./image--}}

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" name="name" data-target="#slug" value="{!! old('name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn tĩnh</label>
                                <input type="text" class="form-control" name="slug" id="slug" value="{!! old('slug') !!}">
                            </div>

                        </div>
                        <div class="col-lg-4">


                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title') !!}">
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="5">{!! old('meta_description') !!}</textarea>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="status" value="1" checked>
                                        Hiển thị
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <hr>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" class="form-control content">{!! old('content') !!}</textarea>
                            </div>

                        </div>
                    </div> {{--./row--}}


                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>
        </div>




    </form>

@endsection