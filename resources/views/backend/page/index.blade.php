@extends('backend.index')
@section('controller','Trang')
@section('controller_route',route('page.index'))
@section('action','Danh sách')
@section('live',route('blog'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
           'routeBulkDestroy' => route('page.bulkDestroy'),
           'routeCreate' => route('page.create'),
           'datatables' => $datatables,
           'object' => new \App\Http\Controllers\PageController,
       ])

@endsection
