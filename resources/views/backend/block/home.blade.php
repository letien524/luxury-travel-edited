@extends('backend.index')
@section('live',url('/'))
@section('content')

    <div class="row">
        <div class="col-20">
            <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Tours</span>
                    <span class="info-box-number">{!! number_format($tours) !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! $tours ? $shownTours / $tours * 100 : 100!!}%"></div>
                    </div>
                    <span class="progress-description">
                    {!! $highlightTours !!} highlight,  {!! $tours - $shownTours !!} ẩn
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('tour.index') !!}" class="_link" title="{!! $highlightTours !!} highlight,  {!! $tours - $shownTours !!} ẩn"></a>
            </div>
        </div>

        <div class="col-20">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-cart-plus   "></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Đơn hàng</span>
                    <span class="info-box-number">{!! number_format($orders) !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! $orders ? $completedOrders / $orders * 100 : 100 !!}%"></div>
                    </div>
                    <span class="progress-description">
                    {!! $newOrders !!} đơn mới, {!! $completedOrders !!} hoàn thành, {!! $orders - $completedOrders !!} chưa xử lý
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('order.index') !!}" class="_link" title="{!! $newOrders !!} đơn mới, {!! $completedOrders !!} hoàn thành, {!! $orders - $completedOrders !!} chưa xử lý"></a>
            </div>
        </div>

        <div class="col-20">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-address-card-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Liên hệ</span>
                    <span class="info-box-number">{!! number_format($contacts) !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! $contactCustomers ? $responseContactCustomers / $contactCustomers * 100 : 100!!}%"></div>
                    </div>
                    <span class="progress-description">
                    {!! $newContactCustomers !!} liên hệ mới,{!! $responseContactCustomers !!} đã phản hồi || {!! $newContactSubscribes !!} theo dõi mới, {!! $contactSubscribe - $newContactSubscribes !!} theo dõi đã xem
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('contact.index') !!}" class="_link" title="{!! $newContactCustomers !!} liên hệ mới,{!! $responseContactCustomers !!} đã phản hồi || {!! $newContactSubscribes !!} theo dõi mới, {!! $contactSubscribe - $newContactSubscribes !!} theo dõi đã xem"></a>
            </div>

        </div>

        <div class="col-20">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Bình luận</span>
                    <span class="info-box-number">{!! $comments !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! $comments  ? $shownComments / $comments * 100 : 100 !!}%"></div>
                    </div>
                    <span class="progress-description">
                    {!! $newComments !!} mới, {!! $shownComments !!} hiển thị, {!! $comments - $shownComments !!} ẩn
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('comment.index') !!}" class="_link" title=" {!! $newComments !!} mới, {!! $shownComments !!} hiển thị, {!! $comments - $shownComments !!} ẩn"></a>
            </div>
        </div>

        <div class="col-20">
            <div class="info-box bg-purple">
                <span class="info-box-icon"><i class="fa fa-newspaper-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Bài viết</span>
                    <span class="info-box-number">{!! number_format($blogs) !!}</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: {!! $blogs ? $publicBlogs / $blogs * 100 : 100 !!}%"></div>
                    </div>
                    <span class="progress-description">
                    {!! $publicBlogs !!} hiển thị, {!! $blogs - $publicBlogs !!} ẩn
                  </span>
                </div>
                <!-- /.info-box-content -->
                <a href="{!! route('blog.index') !!}" class="_link" title="{!! $publicBlogs !!} hiển thị, {!! $blogs - $publicBlogs !!} ẩn"></a>
            </div>
        </div>
    </div>

    <div class="panel panel-primary hidden">
        {{-- <div class="panel-heading"></div> --}}
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Tiêu đề</th>
                            <th>Liên kết mẫu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Trang chủ</td>
                            <td>
                                <a href="{!! route('home') !!}" target="_blank">
                                    /
                                </a>
                            </td>
                        </tr>


                          <tr>
                              <td>Activities</td>
                              <td>
                                  <a href="{!! route('activity') !!}" target="_blank">
                                      /activities
                                  </a>
                              </td>
                          </tr>
                        <tr>
                            <td>Tin tức</td>
                            <td>
                                <a href="{!! route('blog') !!}" target="_blank">
                                    /blog
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td>Giới thiệu</td>
                            <td>
                                <a href="{!! route('about') !!}" target="_blank">
                                    /about
                                </a>
                            </td>
                        </tr>

                          <tr>
                              <td>Liên hệ</td>
                              <td>
                                  <a href="{!! url('contact') !!}" target="_blank">
                                      /contact
                                  </a>
                              </td>
                          </tr>

                          <tr>
                              <td>Travel agent</td>
                              <td>
                                  <a href="{!! !empty($travelAgent) ? $travelAgent->content : '#' !!}" target="_blank">
                                      /travel-agent
                                  </a>
                              </td>
                          </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    

@endsection