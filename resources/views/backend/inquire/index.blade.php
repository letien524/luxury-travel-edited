@extends('backend.index')
@section('controller','Inquired page')
@section('controller_route',route('inquire.index'))
@section('action','Cập nhật')
@section('live',route('inquire'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('inquire.update',$inquire) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="true">Hotel Category</a></li>
                <li class=""><a href="#activity2" data-toggle="tab" aria-expanded="true">Travel Style</a></li>
            </ul>


            <?php
            $content = json_decode($inquire->content);
            $hotel_categories = (array)@$content->hotel_category;
            $travel_style = (array)@$content->travel_style;
            $faqCurrent = @$content->faq;
            $content = @$content->content;
            ?>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',$inquire->image) ? old('image',$inquire->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$inquire->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}


                            </div>
                        </div>

                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name',$inquire->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>

                                <textarea name="content[content]" class="form-control" rows="5">{!! old('content.content',@$content) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <label>FAQs</label>
                                <p><a href="{!! route('faq.create') !!}" target="_blank"><i class="fa fa-plus"></i> <i>Tạo mẫu mới</i></a></p>
                                <select name="faq_id" class="form-control multislt" style="width: 100%;">
                                    <option value="">Chọn</option>
                                    @foreach($faqs as $faq)
                                        <option value="{!! $faq->id !!}" {!! old('faq_id',$inquire->faq_id) == $faq->id ? 'selected' : null !!}>{!! $faq->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title" value="{!! old('meta_title',$inquire->meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control" rows="5">{!! old('meta_description',$inquire->meta_description) !!}</textarea>
                            </div>
                        </div>
                    </div> {{--./row--}}


                </div>

                <div class="tab-pane " id="activity1">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <th style="width: 30px;"></th>
                                    <th>Nội dung</th>
                                    </thead>
                                    <tbody>

                                    @if(!empty(old('content.section5.content',$hotel_categories)))
                                        <?php $count = 1; ?>
                                        @foreach(old('content.section5.content',$hotel_categories) as $key => $val)
                                            <tr>
                                                <td><span class="index">{!! $count++ !!}</span></td>

                                                <td>
                                                    <input type="text" class="form-control" value="{!! $val !!}" name="content[hotel_category][<?php echo $key ?>]">
                                                    <a href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger repeat_btn" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeater(event,this)">Thêm</button>
                                </div>
                            </div>

                        </div>

                        <script>
                            function repeater(event, el) {
                                event.preventDefault();
                                var target = $(el).closest('.repeater').find('table tbody');
                                $.get("{{ route('repeat.inquire.hotel') }}", function (data) {
                                    target.append(data)
                                })

                            }
                        </script>
                    </div>
                </div>

                <div class="tab-pane" id="activity2">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <th style="width: 30px;"></th>
                                    <th>Nội dung</th>
                                    </thead>
                                    <tbody>
                                    @if(old('content.section5.content',$travel_style))
                                        <?php $count = 1; ?>
                                        @foreach(old('content.section5.content',$travel_style) as $key => $val)
                                            <tr>
                                                <td><span class="index-trvel">{!! $count++ !!}</span></td>
                                                <td style="position:relative">
                                                    <input type="text" class="form-control" value="{!! $val !!}" name="content[travel_style][<?php echo $key ?>]">
                                                    <a href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger repeat_btn" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeaterTravel(event,this)">Thêm</button>
                                </div>
                            </div>

                        </div>

                        <script>
                            function repeaterTravel(event, el) {
                                event.preventDefault();
                                var target = $(el).closest('.repeater').find('table tbody');
                                $.get("{{ route('repeat.inquire.travel') }}", function (data) {
                                    target.append(data)
                                })

                            }
                        </script>
                    </div>
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
