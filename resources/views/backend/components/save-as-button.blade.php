<button type="button" onclick="tourSaveAs(this,'{{ @$storeRoute }}')" class="btn btn-success">Duplicate</button>
<script>
    function tourSaveAs(el,  storeRoute){
        const form = el.closest('form');
        const _method = form.querySelector('input[name=_method]');
        form.setAttribute('action', storeRoute);
        _method.remove();

        return form.submit();
    }
</script>
