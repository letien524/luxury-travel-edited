<div class="btnAdd">
    @if(method_exists($object, 'create'))
        <a href="{!! $routeCreate !!}">
            <fa class="btn btn-primary"><i class="fa fa-plus"></i> Thêm</fa>
        </a>
    @endif
    <button type="submit" class="btn btn-danger" onclick="return confirm('{{ \App\Enum\CommonEnum::MESSAGES['deleteConfirm'] }}')"><i class="fa fa-trash-o"></i> {{ \App\Enum\TableEnum::LABELS['delete'] }}</button>

</div>
