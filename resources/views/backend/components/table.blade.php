<div class="table-responsive {{ $class }}">
    {!! $datatables->table()  !!}
    {!! $datatables->scripts() !!}
</div>
