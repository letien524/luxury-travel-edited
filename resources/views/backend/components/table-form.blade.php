<form action="{{ $routeBulkDestroy }}" method="POST">
    {{ csrf_field() }}

    @include('backend.components.table-button-before', [
        'routeCreate' => $routeCreate,
        'object' => $object,
    ])

    @include('backend.components.table', ['datatables' => $datatables, 'class' => @$class])
</form>

<div class="modal fade" id="deleteRowConfirmModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="POST" onsubmit="$('#deleteRowConfirmModal').modal('hide')">
                {{ csrf_field() }}
                {{ method_field('delete') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ \App\Enum\CommonEnum::MODAL['delete'] }}</h4>
                </div>
                <div class="modal-body">
                    {{ \App\Enum\CommonEnum::MESSAGES['deleteConfirm'] }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ \App\Enum\TableEnum::LABELS['cancel'] }}</button>
                    <button type="submit" class="btn btn-danger">{{ \App\Enum\TableEnum::LABELS['delete'] }}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    function deleteRowConfirmModalHandle(el){
        el = $(el);
        const href = el.attr('href');
        const target = el.data('target');
        const modal = $(target);
        const form = modal.find('form');
        form.prop('action',href);
    }
</script>
