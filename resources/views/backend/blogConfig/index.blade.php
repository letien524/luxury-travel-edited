@extends('backend.index')
@section('controller','Tin tức')
@section('controller_route',route('blogConfig.index'))
@section('action','Cấu hình')
@section('live',route('blog'))
@section('content')

    @include('backend.block.error')
    <form action="{!! route('blogConfig.update',$config) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a>
                </li>
            </ul>
            <?php $content = json_decode($config->content); ?>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Ảnh đại diện</label><br>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',$config->image) ? old('image',$config->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$config->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>

                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Ảnh sidebar</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.banner',@$content->banner) ? old('content.banner',@$content->banner) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[banner]" value="{{ old('content.banner',@$content->banner)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="content[link]" value="{!! old('content.link',@$content->link) !!}">
                            </div>


                        </div> {{--./col --}}

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title" value="{!! old('meta_title',$config->meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control" rows="5">{!! old('meta_description',$config->meta_description) !!}</textarea>
                            </div>
                        </div>

                    </div> {{--./row--}}


                </div>
            </div>

            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
