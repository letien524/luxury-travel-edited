@extends('backend.index')
@section('controller','Thành viên')
@section('controller_route',route('member.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
              'routeBulkDestroy' => route('member.bulkDestroy'),
              'routeCreate' => route('member.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\MemberController,
          ])
@endsection
