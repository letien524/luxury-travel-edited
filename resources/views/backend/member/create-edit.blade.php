<?php
if(isUpdateBlade(@$configs['action'])){
    $liveRoute = route('member.detail',@$member->slug);
    $actionLabelRoute = \App\Enum\RouteActionEnum::ACTIONS['update'];
    $actionFormRoute = route('member.update', @$member);
}else{
    $liveRoute = route('about');
    $actionLabelRoute = \App\Enum\RouteActionEnum::ACTIONS['create'];
    $actionFormRoute = route('member.store');
}
?>

@extends('backend.index')
@section('controller','Thành viên')
@section('controller_route',route('member.index'))
@section('action', $actionLabelRoute)
@section('live', $liveRoute)
@section('content')

    @include('backend.block.error')

    <form action="{!! $actionFormRoute!!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="false">Thông tin đính kèm</a></li>
            </ul>

            <style>
                .page-header {
                    font-size: 16px;
                }
            </style>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',@$member->image) ? old('image',@$member->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',@$member->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}


                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" data-target="#slug" name="name" value="{!! old('name',@$member->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="slug" id="slug" value="{!! old('slug',@$member->slug) !!}">
                            </div>

                            <div class="form-group">
                                <label>Số thứ tự</label>
                                <input type="number" class="form-control" name="order_menu" min="0" value="{!! @$member->order_menu !!}">
                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title" value="{!! old('meta_title', @$member->meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control" rows="5">{!! old('meta_description', @$member->meta_description) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="status" value="1" {!! @$member->status == 1 ? 'checked' : null !!}>
                                        Hiển thị
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Chức vụ</label>
                                <input type="text" class="form-control" name="position" value="{!! old('position',@$member->position) !!}">
                            </div>

                            <div class="form-group">
                                <label>Trích dẫn</label>
                                <input type="text" class="form-control" name="quote" value="{!! old('quote',@$member->quote) !!}">
                            </div>

                            <div class="form-group">
                                <label>Mô tả ngắn</label>
                                <textarea name="excerpt" class="form-control" rows="5">{!! old('excerpt',@$member->excerpt) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" class="form-control content" rows="5">{!! old('content',@$member->content) !!}</textarea>
                            </div>
                        </div>

                    </div> {{--./row--}}
                </div>

                <div class="tab-pane" id="activity1">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Đánh giá từ khách hàng</label>
                                <select name="reviews[]" multiple class="form-control multislt" style="width: 100%">
                                    @if($reviews->count())
                                        <?php
                                        if(isUpdateBlade(@$configs['action'])){
                                            $SelectedReviews = @$member->reviews->count() ? @$member->reviews->pluck('id')->toArray() : [];
                                        }
                                        else{
                                            $SelectedReviews = [];
                                        }


                                        ?>
                                        @foreach($reviews as $item)
                                            <option value="{{ $item->id }}" {{ in_array($item->id, old('reviews',$SelectedReviews)) ? 'selected' : null }}>{{ $item->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label>FAQ</label>
                                <select name="faqs[]" class="form-control multislt" style="width: 100%">
                                    <option value="">Chọn</option>
                                    @if($faqs->count())
                                        <?php
                                        if(isUpdateBlade(@$configs['action'])){
                                            $faqSelected = @$member->faqs->count() ? @$member->faqs->pluck('id')->toArray() : [];
                                        } else{
                                            $faqSelected = [];
                                        }
                                        ?>
                                        @foreach($faqs as $item)
                                            <option value="{{ $item->id }}" {{ in_array($item->id, old('faqs',$faqSelected)) ? 'selected' : null }}>{{ $item->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>


                            <div class="form-group">
                                <label>Recommended tours</label>
                                <select name="tours[]" multiple class="form-control multislt" style="width: 100%">
                                    @if($tours->count())
                                        <?php
                                        if(isUpdateBlade(@$configs['action'])){
                                            $tourSelected = @$member->tours->count() ? @$member->tours->pluck('id')->toArray() : [];
                                        }else{
                                            $tourSelected = [];
                                        }
                                         ?>
                                        @foreach($tours as $item)
                                            <option value="{{ $item->id }}" {{ in_array($item->id, old('tours', $tourSelected))  ? 'selected' : null }}>{{ $item->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">OK</button>
    </form>

@endsection
