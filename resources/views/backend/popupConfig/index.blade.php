@extends('backend.index')
@section('controller','Popup')
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')
    @include('backend.block.error')

    <form action="{!! route('popupConfig.update',$popup) !!}" method='POST' enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <p><i>**Blacklist là danh sách ngoại trừ phụ thuộc vào tùy chọn hiển thị.</i></p>
        <p><i>**Nếu đang để hiển thị, các đường dẫn nằm trong blacklist sẽ không được hiển thị và ngược lại.</i></p>
        <p>
            <i>
                **Đường dẫn được tính từ vị trí dấu "<code>/</code>" sau domain chính.
                Ví dụ:
                <code> doamin.com/<strong>vi_tri_duong_dan</strong></code>
            </i>
        </p>
        <p><i>**Nếu đường dẫn là trang chủ, đường dẫn là "<code>/</code>"</i></p>

        <hr>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Popup chuyển hướng</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="true">Popup collect email </a></li>
                <li class=""><a href="#activity2" data-toggle="tab" aria-expanded="true">Bottom Popup collect email</a></li>
                <li class=""><a href="#activity3" data-toggle="tab" aria-expanded="true">Corner Popup collect Email</a></li>
            </ul>
            <?php
            $content = json_decode($popup['content'],true);
            ?>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.direction.image',@$content['direction']['image'])) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[direction][image]" value="{{ old('content.direction.image',@$content['direction']['image'])  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="content[direction][link]" value="{{ old('content.direction.link',@$content['direction']['link']) }}">
                            </div>

                            <div class="form-group">
                                <label>Thời gian trì hoãn (giây)</label>
                                <input type="number" min="0" class="form-control" name="content[direction][delay]" value="{{ old('content.direction.delay',@$content['direction']['delay']) }}">
                            </div>

                        </div>
                    </div> {{--./row--}}

                    <hr>
                    <div class="row">
                        <div class="col-lg-4">
                            <label>Tùy chọn hiển thị</label> <br>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[direction][display]" value="1" {!! old('content.direction.display',@$content['direction']['display']) == 1 ? 'checked' : null!!}>
                                    Hiển thị
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[direction][display]" value="0" {!! old('content.direction.display',@$content['direction']['display']) != 1 ? 'checked' : null!!}>
                                    Ẩn
                                </label>
                            </div>
                        </div>

                        <div class="col-lg-12"><hr></div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Black list</label> <br>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <th style="width: 30px;">No.</th>
                                        <th>Đường dẫn</th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $blackList = @$content['direction']['black_list']; ?>


                                        @if(!empty(old('content.direction.black_list',$blackList)))
                                            @foreach(old('content.direction.black_list',$blackList) as $key => $val)
                                                <tr class="">
                                                    <td><span class="index">{!! $loop->iteration !!}</span></td>
                                                    <td style="position:relative">
                                                        <input type="text" class="form-control" name="content[direction][black_list][{!! $key !!}]" value="{!! @$val !!}">

                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                        <a style="
                                                            position: absolute;
                                                            top: calc(50% - 20px);
                                                            right: -8px;
                                                            border:1px solid #ccc;
                                                            border-radius:15px;
                                                            width: 15px;
                                                            height: 15px;
                                                            font-size: 10px;
                                                            text-align: center;
                                                            background-color: #fff;"
                                                           href="javascript:void(0);" onclick="rowAdd(event,this)" class="text-primary" title="Thêm">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeater(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAdd(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index').closest('table').find('.index');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{ route('repeat.popup.directionBlacklist') }}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }
                                    function repeater(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index').closest('table').find('.index');
                                        var index = indexs.length;
                                        $.get("{{ route('repeat.popup.directionBlacklist') }}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="tab-pane" id="activity1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.collection.image',@$content['collection']['image'])) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[collection][image]" value="{{ old('content.collection.image',@$content['collection']['image'])  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[collection][name]" value="{{ old('content.collection.name',@$content['collection']['name']) }}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[collection][content]" class="form-control" rows="5">{{ old('content.collection.content',@$content['collection']['content']) }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Tiêu đề nút</label>
                                <input type="text" class="form-control" name="content[collection][button]" value="{{ old('content.collection.button',@$content['collection']['button']) }}">
                            </div>

                            <div class="form-group">
                                <label>Thời gian trì hoãn (giây)</label>
                                <input type="number" min="0" class="form-control" name="content[collection][delay]" value="{{ old('content.collection.delay',@$content['collection']['delay']) }}">
                            </div>

                        </div>
                    </div> {{--./row--}}

                    <hr>
                    <div class="row">
                        <div class="col-lg-4">
                            <label>Tùy chọn hiển thị</label> <br>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[collection][display]" value="1" {!! old('content.collection.display',@$content['collection']['display']) == 1 ? 'checked' : null!!}>
                                    Hiển thị
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[collection][display]" value="0" {!! old('content.collection.display',@$content['collection']['display']) != 1 ? 'checked' : null!!}>
                                    Ẩn
                                </label>
                            </div>
                        </div>

                        <div class="col-lg-12"><hr></div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Black list</label> <br>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <th style="width: 30px;">No.</th>
                                        <th>Đường dẫn</th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $blackList = @$content['collection']['black_list']; ?>


                                        @if(!empty(old('content.collection.black_list',$blackList)))
                                            @foreach(old('content.collection.black_list',$blackList) as $key => $val)
                                                <tr class="">
                                                    <td><span class="index-collection">{!! $loop->iteration !!}</span></td>
                                                    <td style="position:relative">
                                                        <input type="text" class="form-control" name="content[collection][black_list][{!! $key !!}]" value="{!! @$val !!}">

                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                        <a style="
                                                            position: absolute;
                                                            top: calc(50% - 20px);
                                                            right: -8px;
                                                            border:1px solid #ccc;
                                                            border-radius:15px;
                                                            width: 15px;
                                                            height: 15px;
                                                            font-size: 10px;
                                                            text-align: center;
                                                            background-color: #fff;"
                                                           href="javascript:void(0);" onclick="rowAddCollection(event,this)" class="text-primary" title="Thêm">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeaterCollection(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAddCollection(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index-collection').closest('table').find('.index-collection');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{ route('repeat.popup.collectionBlacklist') }}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }
                                    function repeaterCollection(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index-collection').closest('table').find('.index-collection');
                                        var index = indexs.length;
                                        $.get("{{ route('repeat.popup.collectionBlacklist') }}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="tab-pane" id="activity2">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[collection_bottom][content]" class="form-control" rows="5">{{ old('content.collection_bottom.content',@$content['collection_bottom']['content']) }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Thời gian trì hoãn (giây)</label>
                                <input type="number" min="0" class="form-control" name="content[collection_bottom][delay]" value="{{ old('content.collection_bottom.delay',@$content['collection_bottom']['delay']) }}">
                            </div>

                            <hr>
                            <label>Tùy chọn hiển thị</label> <br>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[collection_bottom][display]" value="1" {!! old('content.collection_bottom.display',@$content['collection_bottom']['display']) == 1 ? 'checked' : null!!}>
                                    Hiển thị
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[collection_bottom][display]" value="0" {!! old('content.collection_bottom.display',@$content['collection_bottom']['display']) != 1 ? 'checked' : null!!}>
                                    Ẩn
                                </label>
                            </div>


                        </div>
                    </div> {{--./row--}}




                </div>

                <div class="tab-pane" id="activity3">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.collection_corner.image',@$content['collection_corner']['image'])) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[collection_corner][image]" value="{{ old('content.collection_corner.image',@$content['collection_corner']['image'])  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="content[collection_corner][link]" value="{{ old('content.collection_corner.link',@$content['collection_corner']['link']) }}">
                            </div>

                            <div class="form-group">
                                <label>Thời gian trì hoãn (giây)</label>
                                <input type="number" min="0" class="form-control" name="content[collection_corner][delay]" value="{{ old('content.collection_corner.delay',@$content['collection_corner']['delay']) }}">
                            </div>

                        </div>
                    </div> {{--./row--}}

                    <hr>
                    <div class="row">
                        <div class="col-lg-4">
                            <label>Tùy chọn hiển thị</label> <br>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[collection_corner][display]" value="1" {!! old('content.collection_corner.display',@$content['collection_corner']['display']) == 1 ? 'checked' : null!!}>
                                    Hiển thị
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input type="radio" name="content[collection_corner][display]" value="0" {!! old('content.collection_corner.display',@$content['collection_corner']['display']) != 1 ? 'checked' : null!!}>
                                    Ẩn
                                </label>
                            </div>
                        </div>

                        <div class="col-lg-12"><hr></div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Black list</label> <br>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <th style="width: 30px;">No.</th>
                                        <th>Đường dẫn</th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $blackList = @$content['collection_corner']['black_list']; ?>


                                        @if(!empty(old('content.collection_corner.black_list',$blackList)))
                                            @foreach(old('content.collection_corner.black_list',$blackList) as $key => $val)
                                                <tr class="">
                                                    <td><span class="index-collection-corner">{!! $loop->iteration !!}</span></td>
                                                    <td style="position:relative">
                                                        <input type="text" class="form-control" name="content[collection_corner][black_list][{!! $key !!}]" value="{!! @$val !!}">

                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                        <a style="
                                                            position: absolute;
                                                            top: calc(50% - 20px);
                                                            right: -8px;
                                                            border:1px solid #ccc;
                                                            border-radius:15px;
                                                            width: 15px;
                                                            height: 15px;
                                                            font-size: 10px;
                                                            text-align: center;
                                                            background-color: #fff;"
                                                           href="javascript:void(0);" onclick="rowAddCollectionCorner(event,this)" class="text-primary" title="Thêm">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeaterCollectionCorner(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAddCollectionCorner(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index-collection-corner').closest('table').find('.index-collection-corner');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{ route('repeat.popup.collectionCornerBlacklist') }}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }
                                    function repeaterCollectionCorner(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index-collection-corner').closest('table').find('.index-collection-corner');
                                        var index = indexs.length;
                                        $.get("{{ route('repeat.popup.collectionCornerBlacklist') }}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>

                    </div>

                </div>

                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>

        <button type="submit" class="btn btn-primary">Lưu</button>
    </form>

@endsection