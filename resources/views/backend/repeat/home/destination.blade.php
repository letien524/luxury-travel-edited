<?php $temp = (int) round(microtime(true) * 1000);
    $temp = 'a_'.$temp;
?>

<tr>
    <td><span class="index"></span></td>
    <td>
        <div class="image">
            <div class="image__thumbnail">
                <img src="{{ __IMAGE_THUMBNAIL_DEFAULT }}"  data-init="{{ __IMAGE_THUMBNAIL_DEFAULT }}">
                <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                <input type="hidden" name="content[section4][{{ $temp }}][image]" value="">
                <div class="image__button" onclick="fileSelect(this)">
                    <i class="fa fa-upload"></i>
                    Upload
                </div>
            </div>
        </div>

    </td>
    <td>
        <input type="text" class="form-control" name="content[section4][{{ $temp }}][name]" value="">
    </td>

    <td>
        <textarea name="content[section4][{{ $temp }}][content]" class="form-control" rows="5"></textarea>
    </td>

    <td style="position:relative">
        <input type="text" class="form-control" name="content[section4][{{ $temp }}][link]" value="">
        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
            <i class="fa fa-minus"></i>
        </a>
    </td>

</tr>

<script>
    var indexs = $('.index').closest('table').find('.index');
    var index = indexs.length;
    if(index == 1){
        indexs.last().html(index);
    }else{
        indexs.last().html(parseInt($(indexs[index - 2]).text()) + 1);
    }


</script>