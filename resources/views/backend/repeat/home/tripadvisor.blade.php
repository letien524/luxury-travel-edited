<?php $temp = (int) round(microtime(true) * 1000); ?>

<tr>
    <td><span class="index-tripadvisor"></span></td>
    <td>
        <input type="text" class="form-control" name="content[section7][content][{{ $temp }}][name]" value="">
    </td>
    <td>
        <input type="text" class="form-control" name="content[section7][content][{{ $temp }}][country]" value="">
    </td>

    <td>
        <input type="text" class="form-control" name="content[section7][content][{{ $temp }}][date]" value="">
    </td>

    <td style="position:relative">
        <textarea name="content[section7][content][{{ $temp }}][content]" class="form-control" rows="4"></textarea>
        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
            <i class="fa fa-minus"></i>
        </a>
    </td>

</tr>

<script>
    var indexs = $('.index-tripadvisor').closest('table').find('.index-tripadvisor');
    var index = indexs.length;
    if(index == 1){
        indexs.last().html(index);
    }else{

        indexs.last().html(parseInt($(indexs[index - 2]).text()) + 1);
    }

    $(".inpImg").fileinput({
        allowedFileTypes: ["image"],
        maxFileSize: 2000,
        showUpload: false
    });

</script>