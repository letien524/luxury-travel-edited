<?php $key = $key ?? Carbon\Carbon::now()->subHours(24)->getTimestamp();?>

<tr>
    <td><span class="index"></span></td>
    <td style="position:relative">
        <input type="text" class="form-control" name="black_list[{{ $key }}]" value="{{ @$value }}">

        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
            <i class="fa fa-minus"></i>
        </a>
        <a style="
                position: absolute;
                top: calc(50% - 20px);
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="rowAdd(event,this)" class="text-primary" title="Thêm">
            <i class="fa fa-plus"></i>
        </a>
    </td>
</tr>
<script>
    const indexList = $('.index').closest('table').find('.index');
    const index = indexList.length;
    indexList.last().html(index);
</script>
