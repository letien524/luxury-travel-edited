<?php $temp = (int) round(microtime(true) * 1000); ?>
<tr>
    <td><span class="index-plan"></span></td>
    <td style="position:relative">
        <div class="row form-group">
            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Tiêu đề</label></div>
            <div class="col-lg-10"><input type="text" class="form-control" name="plan[<?php echo $temp ?>][name]" value=""></div>
        </div>
        <div class="row form-group">
            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Nội dung</label></div>
            <div class="col-lg-10"><textarea name="plan[<?php echo $temp ?>][content]" class="form-control content" rows="6"></textarea></div>
        </div>
        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
            <i class="fa fa-minus"></i>
        </a>
    </td>

</tr>

<script>
    var indexs = $('.index-plan').closest('table').find('.index-plan');
    var index = indexs.length;
    indexs.last().html(index);
    console.log(indexs.last());


    var editor = CKEDITOR.replace('plan[<?php echo $temp ?>][content]');
    CKFinder.setupCKEditor( editor );

</script>