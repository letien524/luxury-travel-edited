<?php $temp = @$_GET['id']; ?>

<tr>
    <td><span class="index"></span></td>
    <td style="position:relative">
        <div class="row form-group">
            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Tiêu đề</label></div>
            <div class="col-lg-10"><input type="text" class="form-control" name="hotel[<?php echo $temp ?>][type]" value=""></div>
        </div>
        <div class="row form-group">
            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Nội dung</label></div>
            <div class="col-lg-10">
                <div class="repeater">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="width: 30%;">Thành phố</th>
                            <th style="width: 40%;">Khách sạn</th>
                            <th style="width: 30%;">Kiểu phòng</th>
                        </tr>
                        </thead>
                        <tbody class="sortable"></tbody>
                    </table>
                    <div class="text-right">
                        <button class="btn btn-primary" onclick="repeaterHotelList(event,this,<?php echo $temp ?>)">Thêm khách sạn</button>
                    </div>
                </div>
            </div>
        </div>
        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
            <i class="fa fa-minus"></i>
        </a>
    </td>
</tr>

<script>
    $( function() {
        $( ".sortable" ).sortable({
            placeholder: "ui-state-highlight"
        });
        $( ".sortable" ).disableSelection();
    } );
</script>

<script>
    var indexs = $('.index').closest('table').find('.index');
    var index = indexs.length;
    indexs.last().html(index);
</script>
