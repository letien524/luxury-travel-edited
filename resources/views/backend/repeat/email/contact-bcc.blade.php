<tr>
    <td><span class="index-contact">{{ request('index') }}</span></td>
    <td style="position:relative">

        <input type="text" class="form-control" name="content[contact][bcc][{{Carbon\Carbon::now()->getTimestamp()+request('index')}}]" placeholder="sample@domain.com">

        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
            <i class="fa fa-minus"></i>
        </a>
        <a style="
                position: absolute;
                top: calc(50% - 20px);
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="rowAddContact(event,this)" class="text-primary" title="Thêm">
            <i class="fa fa-plus"></i>
        </a>
    </td>
</tr>
