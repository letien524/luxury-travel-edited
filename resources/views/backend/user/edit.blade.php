@extends('backend.index')
@section('controller','Quản trị viên')
@section('controller_route',route('user.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('user.update',$user) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>


            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tài khoản</label>
                                <input type="text" class="form-control" name="name" id="name"value="{!! old('name',$user->name) !!}" disabled>
                            </div>

                            <div class="form-group">
                                <label>Mật khẩu hiện tại(Mật khẩu tài khoản đang đăng nhập)</label>
                                <input type="password" class="form-control" name="current_password" id="name"value="{!! old('current_password') !!}" autocomplete="current_password">
                            </div>

                            <div class="form-group">
                                <label>Mật khẩu mới</label>
                                <input type="password" class="form-control" name="password" id="name"value="{!! old('password') !!}" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu mới</label>
                                <input type="password" class="form-control" name="re-password" id="name"value="{!! old('re-password') !!}">
                            </div>

                            <div class="form-group">
                                <label>Vai trò</label>
                                <select name="level" class="form-control mtlslt" {{ !isLevel(1) ? 'disabled' : null }}>
                                    <option value="">Biên tập viên</option>
                                    <option value="2" {{ old('level', $user['level']) == 2 ? 'selected' : null }}>Quản lý tour</option>
                                    <option value="3" {{ old('level', $user['level']) == 3 ? 'selected' : null }}>Quản lý bán hàng</option>
                                    <option value="1" {{ old('level', $user['level']) == 1 ? 'selected' : null }}>Admin</option>
                                </select>
                            </div>

                        </div>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection