@extends('backend.index')
@section('controller','Quản trị viên')
@section('controller_route',route('user.index'))
@section('action','Thêm')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('user.store') !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="level" value="1">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tài khoản</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Mật khẩu</label>
                                <input type="password" class="form-control" name="password" value="{!! old('password') !!}" autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu</label>
                                <input type="password" class="form-control" name="re-password" value="{!! old('re-password') !!}">
                            </div>

                            <div class="form-group">
                                <label>Vai trò</label>
                                <select name="level" class="form-control mtlslt">
                                    @foreach(\App\Enum\UserEnum::ROLE as $key => $role)
                                        @if($key !== 9)
                                            <option value="{{ $key }}">{{ $role }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                        </div>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>

    </form>

@endsection