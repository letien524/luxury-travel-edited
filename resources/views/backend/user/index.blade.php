@extends('backend.index')
@section('controller','Quản trị viên')
@section('controller_route',route('user.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
           'routeBulkDestroy' => route('user.bulkDestroy'),
           'routeCreate' => route('user.create'),
           'datatables' => $datatables,
           'object' => new \App\Http\Controllers\UserController,
       ])
@endsection
