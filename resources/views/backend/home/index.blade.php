@extends('backend.index')
@section('controller','Trang chủ')
@section('controller_route',route('dashboard.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('home.update',$home) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div>
            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Thông tin chung</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta image</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',$home->image) ? old('image',$home->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$home->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>


                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name"value="{!! old('name',$home->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title',$home->meta_title) !!}">
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="5">{!! old('meta_description',$home->meta_description) !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{--Thông tin chung--}}
            <?php
            $content = json_decode($home->content);
            $section1 = @$content->section1;
            $section2 = @$content->section2;
            $section3 = @$content->section3;
            $section4 = @$content->section4;
            $section5 = @$content->section5;
            $section6 = @$content->section6;
            $section7 = @$content->section7;
            $section8 = @$content->section8;

            ?>
            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Banner</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.section1.image',@$section1->image) ? old('content.section1.image',@$section1->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[section1][image]" value="{{ old('content.section1.image',@$section1->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề chính</label>
                                <input type="text" class="form-control" name="content[section1][name][main]" value="{!! old('content.section1.name.main',@$section1->name->main) !!}">
                            </div>

                            <div class="form-group">
                                <label>Tiêu đề phụ</label>
                                <input type="text" class="form-control" name="content[section1][name][sub]" value="{!! old('content.section1.name.sub',@$section1->name->sub) !!}">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4>Nội dung dưới slider</h4>
                    <div class="row">

                        @if(!empty(@$section1->content))
                            @foreach($section1->content as $key => $val)
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Tiêu đề</label>
                                        <input type="text" class="form-control" name="content[section1][content][{!! $key !!}][name]" value="{!! old('content.section1.content.'.$key.'.name',@$val->name) !!}">
                                    </div>

                                    <div class="form-group">
                                        <label>Nội dung</label>
                                        <textarea name="content[section1][content][{!! $key !!}][content]" class="form-control" rows="3">{!! old('content.section1.content.'.$key.'.content',@$val->content) !!}</textarea>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div><!-- /.box-body -->
            </div> {{--banner--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">We offer luxury travel exclusively to Southeast Asia</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section2][name]" value="{!! old('content.section2.name', @$section2->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="content[section2][link]" value="{!! old('content.section2.link',@$section2->link) !!}">
                            </div>

                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[section2][content]" class="form-control" rows="5">{!! old('content.section2.content', @$section2->content) !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{--We offer luxury travel exclusively to Southeast Asia--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">How it works</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section3][name]" value="{!! old('content.section3.name', @$section3->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Tiêu đề phụ</label>
                                <input type="text" class="form-control" name="content[section3][name_sub]" value="{!! old('content.section3.name_sub',@$section3->name_sub) !!}">
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{--How it works--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Destination</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="repeater" id="repeater">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <th></th>
                            <th>Hình ảnh</th>
                            <th>Tiêu đề</th>
                            <th>Nội dung</th>
                            <th>Đường dẫn</th>
                            </thead>
                            <tbody>
                          <?php $section4 = (array)$section4;  ?>

                            @if(!empty(old('content.section4', $section4)))
                                @foreach(old('content.section4', $section4) as $key => $val)
                                    <?php $val = (array) $val; ?>
                                    <tr>
                                        <td><span class="index"></span></td>
                                        <td>

                                            <div class="image">
                                                <div class="image__thumbnail">
                                                    <img src="{{ image_url(@$val['image']) }}"  data-init="{{ image_url('') }}">
                                                    <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                                    <input type="hidden" name="content[section4][{{ $key }}][image]" value="{{ @$val['image'] }}">
                                                    <div class="image__button" onclick="fileSelect(this)">
                                                        <i class="fa fa-upload"></i>
                                                        Upload
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="content[section4][{!! $key !!}][name]" value="{!! @$val['name'] !!}">
                                        </td>

                                        <td>
                                            <textarea name="content[section4][{!! $key !!}][content]" class="form-control" rows="2">{!! @$val['content'] !!}</textarea>
                                        </td>

                                        <td style="position:relative">
                                            <input type="text" class="form-control" name="content[section4][{!! $key !!}][link]" value="{!! @$val['link'] !!}">
                                            <a style="
                                                    position: absolute;
                                                    top: 50%;
                                                    right: -8px;
                                                    border:1px solid #ccc;
                                                    border-radius:15px;
                                                    width: 15px;
                                                    height: 15px;
                                                    font-size: 10px;
                                                    text-align: center;
                                                    background-color: #fff;"
                                               href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                <i class="fa fa-minus"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>

                        <div class="text-right">
                            <button class="btn btn-primary" onclick="repeater(event,this)">Thêm</button>
                        </div>

                    </div>

                    <script>
                        function repeater(event, el) {
                            event.preventDefault();
                            var target = $(el).closest('.repeater').find('table tbody');
                            $.get("{{route('repeat.home.destination')}}", function (data) {
                                target.append(data)
                            })
                        }
                    </script>
                </div><!-- /.box-body -->
            </div> {{--Destination--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Tour highlight</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" name="content[section5][name]" class="form-control" value="{!! old('content.section5.name', @$section5->name) !!}">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4>Banner</h4>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="image">
                                <div class="image__thumbnail">
                                    <img src="{{ image_url(old('content.section5.banner.image', @$section5->banner->image)) }}"  data-init="{{ image_url() }}">
                                    <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                    <input type="hidden" name="content[section5][banner][image]" value="{{ old('content.section5.banner.image', @$section5->banner->image)  }}">
                                    <div class="image__button" onclick="fileSelect(this)">
                                        <i class="fa fa-upload"></i>
                                        Upload
                                    </div>
                                </div>
                            </div> {{--./image--}}
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề banner</label>
                                <input type="text" name="content[section5][banner][name]" class="form-control" value="{!! old('content.section5.banner.name', @$section5->banner->name) !!}">
                            </div>
                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" name="content[section5][banner][link]" class="form-control" value="{!! old('content.section5.banner.link', @$section5->banner->link) !!}">
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{--Tour highlight--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Tour Style</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" name="content[section6][name]" class="form-control" value="{!! old('content.section6.name', @$section6->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Tiêu đề dưới</label>
                                <input type="text" name="content[section6][name_sub]" class="form-control" value="{!! old('content.section6.name_sub', @$section6->name_sub) !!}">
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{--Tour Style--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Tripadvisor</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section7][name]" value="{!! old('content.section7.name', @$section7->name) !!}">
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Đường dẫn viết review</label>
                                <input type="text" class="form-control" name="content[section7][link]" value="{!! old('content.section7.link', @$section7->link) !!}">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4>Nội dung</h4>
                    <div class="repeater" id="repeater">

                        <table class="table table-bordered table-hover">
                            <thead>
                            <th></th>
                            <th>Họ tên</th>
                            <th>Quốc gia</th>
                            <th>Thời gian</th>
                            <th>Nội dung</th>
                            </thead>
                            <tbody>

                            <?php $content = (array)@$section7->content; ?>
                            @if(!empty(old('content.section7.content', $content)))
                                @foreach(old('content.section7.content', $content) as $key => $val)
                                    <?php $val = (array)$val; ?>
                                    <tr>
                                        <td><span class="index-tripadvisor"></span></td>
                                        <td>
                                            <input type="text" class="form-control" name="content[section7][content][{!! $key !!}][name]" value="{!! @$val['name'] !!}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="content[section7][content][{!! $key !!}][country]" value="{!! @$val['country'] !!}">
                                        </td>

                                        <td>
                                            <input type="text" class="form-control" name="content[section7][content][{!! $key !!}][date]" value="{!! @$val['date'] !!}">
                                        </td>

                                        <td style="position:relative">
                                            <textarea name="content[section7][content][{!! $key !!}][content]" class="form-control" rows="4">{!! @$val['content'] !!}</textarea>
                                            <a style="
                        position: absolute;
                        top: 50%;
                        right: -8px;
                        border:1px solid #ccc;
                        border-radius:15px;
                        width: 15px;
                        height: 15px;
                        font-size: 10px;
                        text-align: center;
                        background-color: #fff;"
                                               href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                <i class="fa fa-minus"></i>
                                            </a>
                                        </td>

                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>

                        <div class="text-right">
                            <button class="btn btn-primary" onclick="repeaterTripadvisor(event,this)">Thêm</button>
                        </div>

                    </div>

                    <script>
                        function repeaterTripadvisor(event, el) {
                            event.preventDefault();
                            var target = $(el).closest('.repeater').find('table tbody');
                            $.get("{{route('repeat.home.tripadvisor')}}", function (data) {
                                target.append(data)
                            })
                        }
                    </script>
                </div><!-- /.box-body -->
            </div> {{--Tripadvisor--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title"> News and Special</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" name="content[section8][name]" class="form-control" value="{!! old('content.section8.name',@$section8->name) !!}">
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{-- News and Special--}}

        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
