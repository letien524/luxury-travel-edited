<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lux-Travel Admin</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex,nofollow">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ getAdminAsset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ getAdminAsset('dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ getAdminAsset('dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ getAdminAsset('plugins/datatables/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ getAdminAsset('plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="{{ getAdminAsset('plugins/nestable/nestable.css') }}">

    <link rel="stylesheet" href="{{ getAdminAsset('cus/mystyle.css') }}">

    <script type="text/javascript">
        function homeUrl() {
            return "{!! asset('/') !!}";
        }
    </script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script src="{{ getAdminAsset('plugins/select2/select2.full.min.js') }}"></script>

    <script src="{{ getAdminAsset('bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ getAdminAsset('plugins/fastclick/fastclick.js') }}"></script>
    <script src="{{ getAdminAsset('dist/js/adminlte.js') }}"></script>
    <script src="{{ getAdminAsset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ getAdminAsset('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ getAdminAsset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>

    <script src="{{ getAdminAsset('plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ getAdminAsset('plugins/ckfinder/ckfinder.js') }}"></script>

    <script src="{{ getAdminAsset('cus/myscript.js') }}"></script>

    @yield('script')


</head>

<body class="hold-transition skin-blue sidebar-mini">
<div id="elfinder1" ></div>
<div class="wrapper">

    <header class="main-header">

        <a href="@yield('live')" target="_blank" class="logo" title="@yield('live')">
            <span class="logo-mini"><b>W</b>eb</span>
            <span class="logo-lg"><b>{!! isset($site_info->site_title) ? $site_info->site_title : 'Xem website'!!}</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    @if(Auth::user())
                        <?php

                        $user = Auth::user();
                        ?>
                        <li class="user user-menu">
                            <a href="{!! route('user.edit',$user->id) !!}" title="Chỉnh sửa tài khoản">
                                <img src="{{ image_url($user->image) }}" class="user-image" alt="User Image">
                                {!! $user->name !!}
                            </a>
                        </li>
                @endif
                <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="{!! route('logout') !!}"
                           onclick="return confirm('Bạn có chắc chắn muốn đăng xuất ?');">
                            <i class="fa fa-power-off"></i>
                            <span class="hidden-xs">Đăng xuất</span>
                        </a>
                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->

    @include('backend.components.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @if(URL::current() != url('public/backend'))
            <section class="content-header">
                <h1>
                    <a href="@yield('controller_route')">@yield('controller')</a>
                    <small>@yield('action')</small>
                </h1>

                <ol class="breadcrumb">
                    <li><a href="{!! url('public/backend') !!}"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="@yield('controller_route')">@yield('controller')</a></li>
                    <li class="active">@yield('action')</li>
                </ol>
            </section>
        @endif


        <section class="content">
            <div class="box">
                @if(session('flash_message'))
                    <div class="alert alert-{!! session('flash_level') !!} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Thông báo</h4>
                        {!! session('flash_message') !!}
                    </div>
                @endif

                <div class="box-body">
                    @yield('content')
                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <strong>Copyright &copy; 2-2018 _ <a href="mailto:letien524@gmail.com">letien524@gmail.com</a> </strong>
        All rights reserved.
    </footer>


</div>
<!-- ./wrapper -->


</body>
</html>
