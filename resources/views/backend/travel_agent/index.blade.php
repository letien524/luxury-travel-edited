@extends('backend.index')
@section('controller','Travel Agent')
@section('controller_route',route('travel_agent.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('travel_agent.update',$travel_agent) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Đường dẫn</label>
                            <input type="text" class="form-control" name="content" value="{{ old('content',$travel_agent->content) }}">
                        </div> {{--./col-20--}}

                    </div> {{--./col-12--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection