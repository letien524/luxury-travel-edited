@extends('backend.index')
@section('controller','Thành phố')
@section('controller_route',route('hotelCity.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

@include('backend.block.error')

@include('backend.components.table-form', [
              'routeBulkDestroy' => route('hotelCity.bulkDestroy'),
              'routeCreate' => route('hotelCity.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\HotelCityController,
          ])
@endsection
