@extends('backend.index')
@section('controller','How it works')
@section('controller_route',route('howItWork.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('howItWork.update',$howItWork) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div>
                        <i>**Tiêu đề chính, nội dung chính hiển thị trên sidebar</i><br>
                        <i class="text-primary">**Tiêu đề phụ, nội dung phụ hiển thị trong trang chủ</i>
                    </div>
                    <hr>
                    <?php
                    $name = json_decode($howItWork->name);
                    $content = json_decode($howItWork->content);

                    ?>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề chính </label>
                                <input type="text" class="form-control" name="name[main]" value="{!! old('name.main',$name->main) !!}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung chính</label>
                                <textarea name="content[main]" class="form-control" rows="5">{!! old('content.main',$content->main) !!}</textarea>
                            </div>

                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="text-primary">Tiêu đề phụ </label>
                                <input type="text" class="form-control" name="name[sub]" value="{!! old('name.sub',$name->sub) !!}">
                            </div>

                            <div class="form-group">
                                <label class="text-primary">Nội dung phụ</label>
                                <textarea name="content[sub]" class="form-control" rows="5">{!! old('content.sub',$content->sub) !!}</textarea>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Số thứ tự </label>
                                <input type="number" min="0" class="form-control" name="order_menu" value="{!! old('order_menu',$howItWork->order_menu) !!}">
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
