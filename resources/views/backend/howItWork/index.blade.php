@extends('backend.index')
@section('controller','How it works')
@section('controller_route',route('howItWork.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
              'routeBulkDestroy' => route('howItWork.bulkDestroy'),
              'routeCreate' => route('howItWork.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\HowItWorkController,
          ])

@endsection
