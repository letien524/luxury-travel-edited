@extends('backend.index')
@section('controller','Đơn hàng')
@section('controller_route',route('order.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('order.update',$order) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>


            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="">
                                <table class="table table-bordered">
                                    <tr>
                                        <td style="width: 100px;">Họ tên</td>
                                        <td> <b>{!! $order->name !!}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td> <b><a href="mailto:{!! $order->email !!}">{!! $order->email !!}</a></b></td>
                                    </tr>
                                    <tr>
                                        <td>Số điện thoại</td>
                                        <td> <b><a href="tel:{!! $order->phone !!}" title="{!! $order->phone !!}">{!! $order->phone !!}</a></b></td>
                                    </tr>
                                    <tr>
                                        <td>Quốc gia</td>
                                        <td><b>{!! @$order->country->name !!}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Kiểu phòng</td>
                                        <td><b>{!! $order->hotel !!}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Kiểu tour</td>
                                        <td><b>{!! $order->activity !!}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Điểm đến</td>
                                        <?php

                                        $tour = $order->tour;
                                        if(empty($tour)){
                                            $destinations = $order->destinations->pluck('name')->toArray();
                                            $destinations = implode(', ',$destinations);
                                        }else{
                                            $destinations = $tour->destinations->pluck('name')->implode(',');
                                        }

                                        ?>
                                        <td><b>{!! $destinations !!}</b></td>

                                    </tr>
                                    <tr>
                                        <td>Tour</td>
                                        <td>@if(!empty($tour))<a href="{!! route('tour.detail',['slug'=>@$tour->slug]) !!}" title="{!! @$tour->name !!}">{!! @$tour->name  !!}</a>@endif</td>
                                    </tr>
                                    <tr>
                                        <td>Trẻ em</td>
                                        <td><b>{!! $order->number_child !!}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Người lớn</td>
                                        <td><b>{!! $order->number_adult !!}</b></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Ngày đến</label>
                                        <input type="text" class="form-control" readonly value="{!! $order->departure_date !!}">
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Thời gian đặt</label>
                                        <input type="text" class="form-control" readonly value="{!! date_format(date_create($order->created_at),"Y.m.d - H:m:s")  !!}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea class="form-control" rows="10" readonly>{!! $order->content !!}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select name="status" class="form-control">
                                    <option value="">Mới</option>
                                    <option value="1" {!! $order->status == 1 ? 'selected' : null !!}>Đã xem</option>
                                    <option value="2" {!! $order->status == 2 ? 'selected' : null !!}>Đã phản hồi</option>
                                </select>
                            </div>

                        </div>
                    </div>


                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection