@extends('backend.index')
@section('controller','Thương hiệu')
@section('controller_route',route('brand.index'))
@section('action','Thêm')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('brand.store') !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="file-loading">
                                    <input class="inpImg" name="image" type="file" value="{!! old('image') !!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" id="name"
                                       value="{!! old('name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Danh mục</label>
                                <select name="type" class="form-control">
                                    <option value="">Chọn danh mục</option>
                                    <option value="association">Association</option>
                                    <option value="awards">Awards</option>
                                    <option value="news">News</option>
                                </select>
                            </div>

                        </div>

                    </div> {{--./row--}}
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>

    </form>

@endsection