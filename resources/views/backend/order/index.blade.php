@extends('backend.index')
@section('controller','Đơn hàng')
@section('controller_route',route('order.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
           'routeBulkDestroy' => route('order.bulkDestroy'),
           'routeCreate' => route('order.create'),
           'datatables' => $datatables,
           'object' => new \App\Http\Controllers\OrderController,
       ])

@endsection
