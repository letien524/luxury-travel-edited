<?php include resource_path('views/backend/category/config.blade.php'); ?>
@extends('backend.index')
@section('controller',$controller)
@section('controller_route',route($route))
@section('action','Danh sách')
@section('live',route($routeMain))
@section('content')

    @include('backend.block.error')


    <?php //dd($site_info) ?>

    <form action="{!! route($routebulkDestroy) !!}" method="POST">
        {{ csrf_field() }}
        {{ method_field('delete') }}

        <div class="btnAdd">
            <a href="{!! route($routeCreate) !!}">
                <fa class="btn btn-primary"><i class="fa fa-plus"></i> Thêm</fa>
            </a>
            <button type="submit" class="btn btn-danger" onclick="return confirm('Bạn có chắc chắn xóa không ?')"><i
                        class="fa fa-trash-o"></i> Xóa
            </button>
        </div>

        <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th style="width: 20px;"><input type="checkbox" name="chkAll" id="chkAll"></th>
                    <th style="width: 20px;">No.</th>
                    <th>Tiêu đề</th>
                    <th>Đường dẫn</th>
                    @if(Request::segment(2) == 'tour')
                        <th>Số thứ tự</th>
                        <th title="Hiển thị trên menu chính">Hiển thị menu <i class="fa fa-question-circle text-primary" style="font-size: 13px;"></i></th>
                        @if(Request::segment(3) == 'destination')
                            <th>Best destination</th>
                            <th>Inquire Form</th>
                         @endif
                    @endif
                    <th style="width: 150px;">Thao tác</th>
                </tr>
                </thead>
                <tbody>

                @foreach($data as $key => $item)
                    <?php

                    if($item->hasParent() && request()->segment(3) == 'destination'){
                        $slug = "{$item->getParent()->slug}/{$item->slug}";
                        $route = route('destination.city',['country'=>$item->getParent()->slug, 'city'=>$item->slug]);

                    }else{
                        $slug = $item->slug;
                        $route = route($routeShow,$item->slug);
                    }

                    ?>

                    <tr>
                        <td><input type="checkbox" name="chkItem[]" value="{!! $item->id !!}"></td>
                        <td>{!! $key + 1 !!}</td>
                        <td>
                            <a class="text-default" href="{!! route($routeEdit,$item) !!}" title="{!! $item->name !!}">
                                <img src="{!! image_url($item->image) !!}" class="img-responsive imglist">
                                {!! $item->name !!}
                            </a>
                        </td>
                        <td>{!! $slug  !!}</td>


                        @if(Request::segment(2) == 'tour')
                            <td>{!! $item->order_menu !!}</td>
                            <td>
                                @if($item->status == 1)
                                    <span class="badge label-primary">Hiển thị</span>
                                @else
                                    <span class="badge label-default">Ẩn</span>
                                @endif

                                @if(!$item->index)
                                    <span class="badge label-danger">no-index</span>
                                @endif
                            </td>
                            @if(Request::segment(3) == 'destination')
                                <td>
                                    @if($item->is_best == 1)
                                        <span class="badge label-primary">Hiển thị</span>
                                    @else
                                        <span class="badge label-default">Ẩn</span>
                                    @endif
                                </td>

                                <td>
                                    @if($item->is_inquire == 1)
                                        <span class="badge label-primary">Hiển thị</span>
                                    @else
                                        <span class="badge label-default">Ẩn</span>
                                    @endif
                                </td>

                            @endif
                        @endif

                        <td>
                            <div>
                                <a href="{!! $route !!}" target="_blank" title="Xem"><i class="fa fa-eye"></i> Xem</a> &nbsp;
                                <a href="{!! route($routeEdit,$item) !!}" title="Sửa"> <i
                                            class="fa fa-pencil fa-fw"></i> Sửa</a> &nbsp; &nbsp; &nbsp;
                                <a class="text-danger" href="{!! route($routeDelete,$item) !!}"
                                   onclick="return confirm('Bạn có chắc chắn xóa không ?')" title="Xóa"> <i
                                            class="fa fa-trash-o fa-fw"></i> Xóa</a>
                            </div>

                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </form>

@endsection