<?php $temp = (int) round(microtime(true) * 1000); ?>
<tr>
    <td><span class="index"></span></td>
    <td style="position:relative">
        <div class="row form-group">
            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Tiêu đề</label></div>
            <div class="col-lg-10"><input type="text" class="form-control" name="content[travel_guide][content][<?php echo $temp ?>][name]" value=""></div>
        </div>
        <div class="row form-group">
            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Nội dung</label></div>
            <div class="col-lg-10"><textarea name="content[travel_guide][content][<?php echo $temp ?>][content]" class="form-control content" rows="6"></textarea></div>
        </div>
        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
            <i class="fa fa-minus"></i>
        </a>
    </td>

</tr>

<script>
    var indexs = $('.index').closest('table').find('.index');
    var index = indexs.length;
    if(index == 1){
        indexs.last().html(index);
    }else{

        indexs.last().html(parseInt($(indexs[index - 2]).text()) + 1);
    }

    var editor = CKEDITOR.replace('content[travel_guide][content][<?php echo $temp ?>][content]');
    CKFinder.setupCKEditor( editor );
</script>