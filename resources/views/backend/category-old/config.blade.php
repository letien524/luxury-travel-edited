<?php
$controller = 'Danh mục tin tức';
$route = 'blogCategory.index';
$routeCreate = 'blogCategory.create';
$routeStore = 'blogCategory.store';
$routeEdit = 'blogCategory.edit';
$routeUpdate ='blogCategory.update';
$routeDelete = 'blogCategory.destroy';
$routebulkDestroy = 'blogCategory.bulkDestroy';
$routeShow = 'blogCategory';
$routeMain = 'blog';

if(Request::segment(2) == 'blog'){
    if (Request::segment(3) == 'tag') {
        $controller = 'Danh sách thẻ';
        $route = 'blogTag.index';
        $routeCreate = 'blog.tag.create';
        $routeStore = 'blog.tag.store';
        $routeEdit = 'blog.tag.edit';
        $routeUpdate ='blog.tag.update';
        $routeDelete = 'blog.tag.destroy';
        $routebulkDestroy = 'blog.tag.bulkDestroy';
        $routeShow = 'blog.tag';
        $routeMain = 'blog';
    }
}

if(Request::segment(2) == 'tour'){
    if (Request::segment(3) == 'category') {
        $controller = 'Activities';
        $route = 'tourActivity.index';
        $routeCreate = 'tour.category.create';
        $routeStore = 'tour.category.store';
        $routeEdit = 'tour.category.edit';
        $routeUpdate ='tour.category.update';
        $routeDelete = 'tour.category.destroy';
        $routebulkDestroy = 'tour.category.bulkDestroy';
        $routeShow = 'tourActivity.detail';
        $routeMain = 'activity';
    }
}

    if(Request::segment(3) == 'destination'){
        $controller = 'Destinations';
        $route = 'tourDestination.index';
        $routeCreate = 'tour.destination.create';
        $routeStore = 'tour.destination.store';
        $routeEdit = 'tourDestination.edit';
        $routeUpdate ='tour.destination.update';
        $routeDelete = 'tour.destination.destroy';
        $routebulkDestroy = 'tour.destination.bulkDestroy';
        $routeShow = 'destination';
        $routeMain = 'home';
    }


