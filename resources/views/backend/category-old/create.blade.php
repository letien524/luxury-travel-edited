<?php include resource_path('views/backend/category/config.blade.php'); ?>

@extends('backend.index')
@section('controller',$controller)
@section('controller_route',route($route))
@section('action','Thêm')
@section('live',route($routeMain))
@section('content')

    @include('backend.block.error')

    <form action="{!! route($routeStore) !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}

        @if(Request::segment(2) == 'blog')
            @if(Request::segment(3) == 'category')
                <input type="hidden" name="type" value="blog-category">
            @elseif(Request::segment(3) == 'tag')
                <input type="hidden" name="type" value="blog-tag">
            @endif

        @elseif(Request::segment(2) == 'tour')
            @if(Request::segment(3) == 'category')
                <input type="hidden" name="type" value="tour-category">
            @elseif(Request::segment(3) == 'destination')
                <input type="hidden" name="type" value="destination-category">
            @endif
        @endif


        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
                @if(request()->segment(2) == 'tour')
                    <li class=""><a href="#activity7" data-toggle="tab" aria-expanded="true">Review</a></li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Ảnh đại diện</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image') ? old('image') : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image')  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" data-target="#slug" name="name" id="name"
                                       value="{!! old('name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="slug" id="slug"
                                       value="{!! old('slug') !!}">
                            </div>
                            @if(Request::segment(3) == 'destination')
                                <div class="form-group">
                                    <label>Danh mục cha</label>
                                    <select name="parent_id" class="form-control multislt">
                                        <option value="">Chọn</option>
                                        @foreach($destinations as $destination)
                                            <option value="{{$destination->id}}" {{ old('parent_id') == $destination->id ? 'selected' : null }}>{{$destination->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Mô tả ngắn</label>
                                    <textarea name="excerpt" class="form-control"  rows="5">{!! old('excerpt') !!}</textarea>
                                </div>
                            @endif

                        </div>


                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Canonical URL</label>
                                <input type="text" class="form-control" name="canonical" value="{!! old('canonical') !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title') !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="4">{!! old('meta_description') !!}</textarea>
                            </div>
                            @if(Request::segment(2) == 'tour')
                                <div class="form-group">
                                    <label>Số thứ tự</label>
                                    <input type="number" min="0" class="form-control" name="order_menu"
                                           value="{!! old('order_menu') !!}">
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="status" value="1" checked>
                                            Hiển thị {{Request::segment(3) == 'destination' ? 'trên menu' : null}}
                                        </label>
                                    </div>
                                </div>

                                @if(Request::segment(3) == 'destination')
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_best" value="1" checked>
                                                Hiển thị trong danh sách best destination
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="is_inquire" value="1">
                                                Hiển thị trong inquire form
                                            </label>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="index" value="1" checked>
                                            Index
                                        </label>
                                    </div>
                                </div>
                            @endif

                        </div>


                    </div> {{--./row--}}

                    @if(Request::segment(2) == 'tour')
                        <hr>
                        <div class="row">
                            <div class="col-lg-6">
                                <h4>Banner trên</h4>
                                <div class="form-group">
                                    <label>Hình ảnh</label>

                                    <div class="image">
                                        <div class="image__thumbnail">
                                            <img src="{{ image_url(old('content.banner.top.image') ? old('content.banner.top.image') : null) }}"  data-init="{{ image_url('') }}">
                                            <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                            <input type="hidden" name="content[banner][top][image]" value="{{ old('content.banner.top.image')  }}">
                                            <div class="image__button" onclick="fileSelect(this)">
                                                <i class="fa fa-upload"></i>
                                                Upload
                                            </div>
                                        </div>
                                    </div> {{--./image--}}

                                </div>

                                <div class="form-group">
                                    <label>Tiêu đề phụ</label>
                                    <input type="text" class="form-control" name="content[banner][top][name]" value="{!! old('content.banner.top.name') !!}">
                                </div>

                            </div>

                            <div class="col-lg-6">
                                <h4>Banner dưới</h4>
                                <div class="form-group">
                                    <label>Hình ảnh</label>
                                    <div class="image">
                                        <div class="image__thumbnail">
                                            <img src="{{ image_url(old('content.banner.bottom.image') ? old('content.banner.bottom.image') : null) }}"  data-init="{{ image_url('') }}">
                                            <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                            <input type="hidden" name="content[banner][bottom][image]" value="{{ old('content.banner.bottom.image')  }}">
                                            <div class="image__button" onclick="fileSelect(this)">
                                                <i class="fa fa-upload"></i>
                                                Upload
                                            </div>
                                        </div>
                                    </div> {{--./image--}}

                                </div>
                            </div>
                        </div> {{--./row--}}
                        @if(Request::segment(2) == 'tour')
                            <hr>
                            <h3> Giới thiệu</h3>
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Tiêu đề</label>
                                        <input type="text" class="form-control" name="content[name]"
                                               value="{!! old('content.name') !!}">
                                    </div>

                                    <div class="form-group">
                                        <label>Mô tả</label>
                                        <textarea name="content[content]" class="form-control"
                                                  rows="8">{!! old('content.content') !!}</textarea>

                                    </div>
                                    @if(Request::segment(3) == 'destination')
                                        <hr>
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="content[count][status]" value="1" checked>
                                                    Hiển thị nội dung thống kê
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label>{{ __('Population') }}</label>
                                                    <input type="text" class="form-control"
                                                           name="content[count][population_alt]"
                                                           placeholder="Tên thay thế"
                                                           value="{!! old('content.count.population_alt') !!}"> <br>
                                                    <input type="text" class="form-control"
                                                           name="content[count][population]"
                                                           placeholder="Nội dung"
                                                           value="{!! old('content.count.population') !!}">
                                                </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label>{{ __('Temples') }}</label>
                                                    <input type="text" class="form-control"
                                                           name="content[count][temple_alt]"
                                                           placeholder="Tên thay thế"
                                                           value="{!! old('content.count.temple_alt') !!}"> <br>
                                                    <input type="text" class="form-control" name="content[count][temple]"
                                                           placeholder="Nội dung"
                                                           value="{!! old('content.count.temple') !!}">
                                                </div>
                                            </div>

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                    <label>Cities</label>
                                                    <input type="text" class="form-control"
                                                           name="content[count][city_alt]"
                                                           placeholder="Tên thay thế"
                                                           value="{!! old('content.count.city_alt') !!}"> <br>

                                                    <input type="text" class="form-control" name="content[count][city]"
                                                           placeholder="Nội dung"
                                                           value="{!! old('content.count.city') !!}">
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                @if(Request::segment(3) == 'destination')
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Hình ảnh</label>

                                            <div class="image">
                                                <div class="image__thumbnail">
                                                    <img src="{{ image_url(old('content.image') ? old('content.image') : null) }}"  data-init="{{ image_url('') }}">
                                                    <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                                    <input type="hidden" name="content[image]" value="{{ old('content.image')  }}">
                                                    <div class="image__button" onclick="fileSelect(this)">
                                                        <i class="fa fa-upload"></i>
                                                        Upload
                                                    </div>
                                                </div>
                                            </div> {{--./image--}}

                                        </div>
                                    </div>
                                @endif

                            </div> {{--./row--}}


                        @endif
                            <hr>
                            <h3> Tour guide</h3>
                            <div class="row">
                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <h4>Chọn mẫu faq có sẵn</h4>
                                        <p><a href="{!! route('faq.create') !!}" target="_blank"><i class="fa fa-plus"></i> <i>Tạo mẫu mới</i></a></p>

                                        <select name="faq_id" class="form-control multislt" style="width: 100%;">
                                            <option value="">Chọn</option>
                                            @foreach($faqs as $faq)
                                                <option value="{!! $faq->id !!}" {!! old('faq_id') == $faq->id ? 'selected' : null !!}>{!! $faq->name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label>Hướng dẫn viên</label>
                                        <select name="member_id" class="form-control" id="e4">
                                            <option value="">Chọn</option>
                                            @foreach($members as $member)
                                                <option value="{!! $member->id !!}"
                                                        data-image="{!! image_url($member->image) !!}">{!! $member->name !!}
                                                    - {!! $member->position !!}</option>
                                            @endforeach
                                        </select>

                                        <script>

                                            $("#e4").select2({
                                                templateResult: format,
                                                templateSelection: format,
                                                escapeMarkup: function (m) {
                                                    return m;
                                                }
                                            });

                                            function format(state) {
                                                var originalOption = state.element;

                                                var image = typeof $(originalOption).data('image') != undefined ? $(originalOption).data("image") : false;

                                                image = image ? '<img src="' + image + '" style="height:30px;margin-right:5px;" />' : '';

                                                return image + state.text;
                                            }
                                        </script>
                                    </div>

                                </div>

                            </div> {{--./row--}}
                        @if(Request::segment(3) == 'destination')
                            <hr>
                            <h3> Danh mục tin tức</h3>
                            <div class="row">
                                <div class="col-lg-6">
                                    <select name="content[blog_category][]" class="form-control multislt" multiple>
                                        @foreach($blogCategories as $blogCategory)
                                            <option value="{!! $blogCategory->id !!}" {!! in_array($blogCategory->id, !empty(old('content.blog_category')) ? old('content.blog_category') :[] ) ? 'selected' : null !!}>{!! $blogCategory->name !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
                @if(request()->segment(2) == 'tour')
                <div class="tab-pane" id="activity7">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Chọn mẫu Review</label> <br>
                                <select name="reviews" class="form-control multislt" style="width: 100%;">
                                    <option value="">Chọn</option>
                                    @foreach($reviewCategories as $category)
                                        <option value="{{$category->id}}" {{old('reviews') == $category->id ? 'selected' : null }}>{{$category->name}}({{$category->reviews->count()}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> {{--./row--}}
                </div>
                @endif
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>
        </div>


    </form>

@endsection