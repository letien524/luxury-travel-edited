@extends('backend.index')
@section('controller','Tours')
@section('controller_route',route('tour.index'))
@section('action','Thêm')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( ".sortable" ).sortable({
                placeholder: "ui-state-highlight"
            });
            $( ".sortable" ).disableSelection();
        } );
    </script>

    <form action="{!! route('tour.store') !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
                <li class=""><a href="#activity2" data-toggle="tab" aria-expanded="true">Bản đồ</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="true">Lịch trình</a></li>
                <li class=""><a href="#activity3" data-toggle="tab" aria-expanded="true">Danh sách khách sạn</a></li>
                <li class=""><a href="#activity5" data-toggle="tab" aria-expanded="true">FAQs</a></li>
                <li class=""><a href="#activity4" data-toggle="tab" aria-expanded="true">Thư viện ảnh</a></li>
                <li class=""><a href="#activity6" data-toggle="tab" aria-expanded="true">Video</a></li>
                <li class=""><a href="#activity7" data-toggle="tab" aria-expanded="true">Review</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image') ? old('image') : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image')  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" data-target="#slug" name="name" value="{!! old('name') !!}" required>
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn tĩnh</label>
                                <input type="text" class="form-control" name="slug" id="slug" value="{!! old('slug') !!}" required>
                            </div>

                            <div class="form-group">
                                <label>Activity</label>
                                <?php
                                $old = (array) old('activities');
                                $categorySelected = \App\Category::whereIn('id',$old)->get();
                                ?>
                                <select name="activities[]" class="form-control multislt" multiple="multiple" data-target="#mainActivity">
                                    @foreach($categories as $category)
                                        <option value="{!! $category->id !!}" {!! in_array($category->id, $old) ? "selected" : null !!}>{!! $category->name !!}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label>Main Activity</label>
                                <select class="form-control"  name="category_id" id="mainActivity">
                                    @foreach($categorySelected as $category)
                                        <option value="{!! $category->id !!}" {!!  $category->id == old('category_id')  ? "selected" : null !!}>{!! $category->name !!}</option>
                                    @endforeach
                                </select>
                            </div>



                            <div class="form-group">
                                <label>Destination</label>
                                <select name="destinations[]" class="form-control multislt" multiple="multiple">
                                    @foreach($destinations as $destination)
                                        <option value="{!! $destination->id !!}" {!! old('destination_id') ==  $destination->id ? "selected" : null !!}>{!! $destination->name !!}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Thời gian (ngày)</label>
                                <input type="number" min="0" class="form-control" name="duration" value="{!! old('duration') !!}">
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Giá ($)</label>
                                        <input type="number" min="0" class="form-control" name="price" value="{!! old('price') !!}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Giá khuyên mại ($)</label>
                                        <input type="number" min="0" class="form-control" name="price_promotion" value="{!! old('price_promotion') !!}">
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Canonical URL</label>
                                <input type="text" class="form-control" name="canonical" value="{!! old('canonical') !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title" value="{!! old('meta_title') !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control" rows="5">{!! old('meta_description') !!}</textarea>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="status" value="1" checked>
                                                Hiển thị
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="highlight" value="1">
                                                Highlight
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="index" value="1" checked >
                                        Robot index
                                    </label>
                                </div>
                            </div>
                        </div>


                    </div> {{--./row--}}
                    <hr>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Mô tả ngắn</label>
                                <textarea name="excerpt" class="form-control content">{!! old('excerpt') !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[main]" class="form-control content">{!! old('content.main') !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tour Included</label>
                                <textarea name="content[include]" class="form-control content">{!! old('content.include') !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tour Excluded</label>
                                <textarea name="content[exclude]" class="form-control content">{!! old('content.exclude') !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="activity2">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh bản đồ</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('map.image') ? old('map.image') : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="map[image]" value="{{ old('map.image')  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <textarea name="map[link]" class="form-control" rows="5">{!! old('map.link') !!}</textarea>
                            </div>
                        </div>
                    </div> {{--./row--}}

                </div>

                <div class="tab-pane" id="activity1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>File</label>
                                <input class="" name="file" type="file" value="{!! old('file') !!}">
                            </div>

                        </div>
                    </div> {{--./row--}}
                    <hr>

                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Danh sách nội dung</h4>
                            <div class="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead></thead>
                                    <tbody>
                                    <?php

                                    $plans = (array) old('plan');
                                    ?>
                                    @if(!empty($plans))
                                        <?php $count = 1; ?>
                                        @foreach($plans as $key => $plan)
                                            <tr>
                                                <td><span class="index-plan">{{ $count++ }}</span></td>
                                                <td style="position:relative">
                                                    <div class="row form-group">
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Tiêu đề</label></div>
                                                        <div class="col-lg-10"><input type="text" class="form-control" name="plan[{!! $key !!}][name]" value="{!! @$plan['name'] !!}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Nội dung</label></div>
                                                        <div class="col-lg-10"><textarea name="plan[{!! $key !!}][content]" class="form-control content" rows="6">{!! @$plan['content'] !!}</textarea></div>
                                                    </div>
                                                    <a style="
                                                        position: absolute;
                                                        top: 50%;
                                                        right: -8px;
                                                        border:1px solid #ccc;
                                                        border-radius:15px;
                                                        width: 15px;
                                                        height: 15px;
                                                        font-size: 10px;
                                                        text-align: center;
                                                        background-color: #fff;"
                                                       href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeater(event,this)">Thêm</button>
                                </div>

                            </div>

                            <script>
                                function repeater(event, el) {
                                    event.preventDefault();
                                    var target = $(el).closest('.repeater').find('table tbody');
                                    $.get("{{ route('repeat.tour.plan') }}", function (data) {
                                        target.append(data)
                                    })
                                }
                            </script>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="activity3">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>Chọn thành phố</h4>
                            <div class="form-group">
                                <?php $oldHotelCities = old('hotelCities',[]); ?>
                                <select class="form-control multislt" id="hotelSelected" style="width: 100%" name="hotelCities[]" multiple>
                                    @foreach($hotelCities as $hotelCity)
                                        <option value="{{ $hotelCity->id }}" {{ in_array($hotelCity->id, $oldHotelCities) ? 'selected' : null }}>{{ $hotelCity->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="button" id="hotelUpdateBtn" onclick="hotelLoad(event)"><i class="fa fa-refresh"></i> Cập nhật</button>
                            </div>
                        </div>
                    </div> {{--./row--}}

                    @section('script')
                        <script>
                            function hotelLoad(e){
                                const hotelCities = $("#hotelSelected").val();
                                let hotels = [];
                                $("#hotelList").find('input:checked').each(function(i){
                                    hotels.push(this.value);
                                });

                                $.ajax({
                                    type: "GET",
                                    url: "{{route('tour.hotel')}}",
                                    data: {
                                        idCities: hotelCities,
                                        idHotels: hotels,
                                    },
                                    success: function (res) {
                                        $("#hotelList").html(res);
                                    }
                                })
                            }
                        </script>
                    @endsection

                    <div id="hotelList">

                        <?php $hotels =  \App\Hotel::whereIn('city_id', $oldHotelCities)->get()?>
                        <?php $hotelCities =  \App\Hotel::whereIn('id', $oldHotelCities)->get(['id','name'])?>
                        <?php
                        $selectedHotels =  old('hotels')
                        ?>

                        @include('backend.tour.hotel',[
                            'hotels' => $hotels,
                            'hotelCities' => $hotelCities,
                            'selectedHotels' => $selectedHotels,
                        ])
                    </div>

                </div>

                <div class="tab-pane" id="activity5">
                    <div class="row">
                        <div class="col-lg-4">
                            <h4>Chọn mẫu faq có sẵn</h4>
                            <p><a href="{!! route('faq.create') !!}" target="_blank"><i class="fa fa-plus"></i> <i>Tạo mẫu mới</i></a></p>

                            <select name="faq_id" class="form-control multislt" style="width: 100%;">
                                <option value="">Chọn</option>
                                @foreach($faqs as $faq)
                                    <option value="{!! $faq->id !!}" {!! old('faq_id') == $faq->id ? 'selected' : null !!}>{!! $faq->name !!}</option>
                                @endforeach
                            </select>


                        </div>
                    </div> {{--./row--}}

                </div>

                <div class="tab-pane" id="activity4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">

                                <div class="image">
                                    <button type="button" class="btn btn-primary" onclick="fileMultiSelect(this)"><i class="fa fa-upload"></i> Chọn hình ảnh</button>
                                    <br><br>
                                    <div class="image__gallery"></div> {{--./gallery--}}
                                </div> {{--./image--}}


                            </div>
                        </div>
                    </div> {{--./row--}}

                </div>

                <div class="tab-pane" id="activity6">
                    <div class="row">
                        <div class="col-lg-12">


                            <div class="form-group">
                                <label>Ảnh đại diện video</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url('') }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[video][thumbnail]" value="">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <label>Link video</label>
                                <input type="text" class="form-control" name="content[video][link]" value="">

                            </div>
                        </div>
                    </div> {{--./row--}}

                </div>

                <div class="tab-pane" id="activity7">
                    <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group">
                               <label>Chọn mẫu Review</label> <br>
                               <select name="reviews" class="form-control multislt" style="width: 100%;">
                                   <option value="">Chọn</option>
                                   @foreach($reviewCategories as $category)
                                       <option value="{{$category->id}}" {{old('reviews') == $category->id ? 'selected' : null }}>{{$category->name}}({{$category->reviews->count()}})</option>
                                   @endforeach
                               </select>
                           </div>
                        </div>
                    </div> {{--./row--}}

                </div>


            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>

    </form>

@endsection


