@extends('backend.index')
@section('controller','Cấu hình lọc tour')
@section('controller_route',route('tourFilter.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <p><i>**Giá trị mong muốn sẽ <b>LỚN</b> hơn min, <b>NHỎ</b> hơn max</i></p>
    <p>
        <i>**Ví dụ: <br>
            <code>
                - Trên 500 ==> <b>min = 500</b> <br>
                - Khoảng 500 đến 1000 ==> <b>min = 500, max = 1000</b><br>
                - Dưới 1000 ==> <b> max = 1000</b><br>
            </code>
        </i>
    </p>
    <hr>


    <form action="{!! route('tour.filter.update',$filter) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Lọc theo Giá</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="true" title="Thời lượng hành trình của tour">Lọc theo thời gian</a></li>
            </ul>


            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <th></th>
                                    <th>Tiêu đề</th>
                                    <th>Min</th>
                                    <th>Max</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $content = json_decode($filter->content);
                                    $filter_price = old('content.filter_price', (array)@$content->filter_price);
                                    ?>
                                    @if(!empty($filter_price))
                                        <?php $count=1; ?>
                                        @foreach($filter_price as $key => $val)
                                            <tr>
                                                <td><span class="index">{!! $count++ !!}</span></td>
                                                <td><input type="text" class="form-control" name="content[filter_price][{!! $key !!}][name]" value="{!! @$val->name !!}"></td>
                                                <td><input type="number" min="0" class="form-control" name="content[filter_price][{!! $key !!}][min]" value="{!! @$val->min !!}"></td>
                                                <td style="position:relative">
                                                    <input type="number" min="0" class="form-control" name="content[filter_price][{!! $key !!}][max]" value="{!! @$val->max !!}">
                                                    <a style="
                                                        position: absolute;
                                                        top: 50%;
                                                        right: -8px;
                                                        border:1px solid #ccc;
                                                        border-radius:15px;
                                                        width: 15px;
                                                        height: 15px;
                                                        font-size: 10px;
                                                        text-align: center;
                                                        background-color: #fff;"
                                                       href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeater(event,this)">Thêm</button>
                                </div>
                            </div>

                        </div>

                        <script>
                            function repeater(event, el) {
                                event.preventDefault();
                                var target = $(el).closest('.repeater').find('table tbody');
                                $.get("{{ route('repeat.tour-price') }}", function (data) {
                                    target.append(data)
                                })

                            }
                        </script>

                    </div> {{--./row--}}
                </div>
                <div class="tab-pane" id="activity1">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <th></th>
                                    <th>Tiêu đề</th>
                                    <th>Min</th>
                                    <th>Max</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $filter_duration = old('content.filter_duration', (array)@$content->filter_duration);
                                    ?>
                                    @if(!empty($filter_duration))
                                        <?php $count=1; ?>
                                        @foreach($filter_duration as $key => $val)
                                            <tr>
                                                <td><span class="index-duration">{!! $count++ !!}</span></td>
                                                <td><input type="text" class="form-control" name="content[filter_duration][{!! $key !!}][name]" value="{!! $val->name !!}"></td>
                                                <td><input type="number" min="0" class="form-control" name="content[filter_duration][{!! $key !!}][min]" value="{!! $val->min !!}"></td>
                                                <td style="position:relative">
                                                    <input type="number" min="0" class="form-control" name="content[filter_duration][{!! $key !!}][max]" value="{!! $val->max !!}">
                                                    <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                       href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeaterDuration(event,this)">Thêm</button>
                                </div>
                            </div>

                        </div>

                        <script>
                            function repeaterDuration(event, el) {
                                event.preventDefault();
                                var target = $(el).closest('.repeater').find('table tbody');
                                $.get("{{ route('repeat.tour-duration') }}", function (data) {
                                    target.append(data)
                                })

                            }
                        </script>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
