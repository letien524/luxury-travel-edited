@extends('backend.index')
@section('controller','Tours')
@section('controller_route',route('tour.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
          'routeBulkDestroy' => route('tour.bulkDestroy'),
          'routeCreate' => route('tour.create'),
          'datatables' => $datatables,
          'object' => new \App\Http\Controllers\TourController,
          'class' => 'tour-table'
      ])
@endsection
