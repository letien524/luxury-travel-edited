@extends('backend.index')
@section('controller','Tours')
@section('controller_route',route('tour.index'))
@section('action','Import/Export')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')



    <form action="{!! route('tourImport.import') !!}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6">
                <h4>Import</h4>
                <div class="form-group">
                    <label>Chọn file Excel (*.csv, *.xls, *.xlsx)</label>
                    <input type="file" name="fFile" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Import</button>
                    <a href="{{ route('tourImport.export') }}" class="btn btn-success"><i class="fa fa-download"></i> Export</a>
                </div>

            </div>


        </div>

    </form>

@endsection
