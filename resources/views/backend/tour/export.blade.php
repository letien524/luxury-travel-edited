<?php $tourModel = new \App\Tour; ?>

<table>
    <tr>
        <th>id</th>
        @foreach($tourModel->getFillable() as $field)
            <th>{{ $field }}</th>
        @endforeach
    </tr>

    @foreach($data as $item)
        <tr>
            <td>{{ $item->id }}</td>
            @foreach($tourModel->getFillable() as $field)
                <td>{{ $item->$field }}</td>
            @endforeach
        </tr>
    @endforeach
</table>