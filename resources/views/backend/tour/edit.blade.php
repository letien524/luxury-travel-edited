@extends('backend.index')
@section('controller','Tours')
@section('controller_route',route('tour.index'))
@section('action','Cập nhật')
@section('live', route('tour.detail',$tour->slug))
@section('content')

    @include('backend.block.error')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( ".sortable" ).sortable({
                placeholder: "ui-state-highlight"
            });
            $( ".sortable" ).disableSelection();
        });
    </script>

    <form action="{!! route('tour.update',$tour) !!}" method='POST' enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
                <li class=""><a href="#activity2" data-toggle="tab" aria-expanded="true">Bản đồ</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="true">Lịch trình</a></li>
                <li class=""><a href="#activity3" data-toggle="tab" aria-expanded="true">Khách sạn</a></li>
                <li class=""><a href="#activity5" data-toggle="tab" aria-expanded="true">FAQs</a></li>
                <li class=""><a href="#activity4" data-toggle="tab" aria-expanded="true">Thư viện ảnh</a></li>
                <li class=""><a href="#activity6" data-toggle="tab" aria-expanded="true">Video</a></li>
                <li class=""><a href="#activity7" data-toggle="tab" aria-expanded="true">Review</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',$tour->image) ? old('image',$tour->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$tour->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" data-target="#slug" name="name" value="{!! old('name',$tour->name) !!}" required>
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn tĩnh</label>
                                <input type="text" class="form-control" id="slug" name="slug" value="{!! old('slug',$tour->slug) !!}" required>
                            </div>

                            <div class="form-group">
                                <label>Activity</label>
                                <?php
                                $old = $tour->activities->pluck('id')->toArray();
                                $old = (array) old('activities',$old);

                                $categorySelected = \App\Category::whereIn('id',$old)->get();

                                ?>
                                <select name="activities[]" class="form-control multislt" multiple="multiple" data-target="#mainActivity">
                                    @foreach($categories as $category)
                                        <option value="{!! $category->id !!}" {!!  in_array($category->id, (array)$old)  ? "selected" : null !!}>{!! $category->name !!}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Main Activity</label>
                                <select class="form-control"  name="category_id" id="mainActivity">
                                    @foreach($categorySelected as $category)
                                        <option value="{!! $category->id !!}" {!!  $category->id == old('category_id',$tour->category_id)  ? "selected" : null !!}>{!! $category->name !!}</option>
                                    @endforeach
                                </select>
                            </div>



                            <div class="form-group">
                                <label>Destination</label>

                                <select name="destinations[]" class="form-control multislt" multiple="multiple">
                                    <?php
                                    $old = $tour->destinations->pluck('id')->toArray();
                                    $old = old('destinations',$old)
                                    ?>
                                    @foreach($destinations as $destination)
                                        <option value="{!! $destination->id !!}" {!!  in_array($destination->id, (array)$old)  ? "selected" : null !!}>{!! $destination->name !!}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Thời gian (ngày)</label>
                                <input type="number" min="0" class="form-control" name="duration" value="{!! old('duration',$tour->duration) !!}">
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Giá ($)</label>
                                        <input type="number" min="0" class="form-control" name="price" value="{!! old('price',$tour->price) !!}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Giá khuyên mại ($)</label>
                                        <input type="number" min="0" class="form-control" name="price_promotion" value="{!! old('price_promotion',$tour->price_promotion) !!}">
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Canonical URL</label>
                                <input type="text" class="form-control" name="canonical" value="{!! old('canonical',$tour->canonical) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title" value="{!! old('meta_title',$tour->meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control" rows="5">{!! old('meta_description',$tour->meta_description) !!}</textarea>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="status" value="1" {!! $tour->status == 1 ? 'checked' : null !!}>
                                                Hiển thị
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="highlight" value="1" {!! $tour->highlight == 1 ? 'checked' : null !!}>
                                                Highlight
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="index" value="1" {{ $tour->index == 1  ? 'checked' : null }}>
                                        Robot index
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div> {{--./row--}}
                    <hr>

                    <div class="row">
                        <?php $content = json_decode($tour->content); ?>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Mô tả ngắn</label>
                                    <textarea name="excerpt" class="form-control content">{!! old('excerpt', $tour->excerpt) !!}</textarea>
                                </div>
                            </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[main]" class="form-control content">{!! old('content.main',$content->main) !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tour Included</label>
                                <textarea name="content[include]" class="form-control content">{!! old('content.include',$content->include) !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tour Excluded</label>
                                <textarea name="content[exclude]" class="form-control content">{!! old('content.exclude',$content->exclude) !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="activity2">
                    <div class="row">
                        <?php $map = json_decode($tour->map); ?>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh bản đồ</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('map.image',@$map->image) ? old('map.image',@$map->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="map[image]" value="{{ old('map.image',@$map->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <textarea name="map[link]" class="form-control" rows="5">{!! old('map.link',@$map->link) !!}</textarea>
                            </div>
                        </div>
                    </div> {{--./row--}}
                </div>

                <div class="tab-pane" id="activity1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>File</label><br>
                                @if(!empty($tour->file) && strlen($tour->file))
                                    <p><a href="{!! image_url($tour->file) !!}" download title="{!! image_url($tour->file) !!}"><i class="fa fa-download"></i> Download</a></p>
                                @endif

                                <input class="" name="file" type="file" value="{!! old('file') !!}">
                            </div>

                        </div>
                    </div> {{--./row--}}
                    <hr>

                    <div class="row">

                        <div class="col-lg-12">
                            <h4>Danh sách nội dung</h4>
                            <div class="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead></thead>
                                    <tbody>
                                    <?php
                                    $plans = (array) json_decode($tour->plan);

                                    $plans = old('plan', $plans);
                                    ?>
                                   @if(!empty($plans))
                                       <?php $count = 1; ?>
                                        @foreach($plans as $key => $plan)
                                            <?php $plan = (array) $plan; ?>
                                            <tr>
                                                <td><span class="index-plan">{{ $count++ }}</span></td>
                                                <td style="position:relative">
                                                    <div class="row form-group">
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Tiêu đề</label></div>
                                                        <div class="col-lg-10"><input type="text" class="form-control" name="plan[{!! $key !!}][name]" value="{!! @$plan['name'] !!}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Nội dung</label></div>
                                                        <div class="col-lg-10"><textarea name="plan[{!! $key !!}][content]" class="form-control content" rows="6">{!! @$plan['content'] !!}</textarea></div>
                                                    </div>
                                                    <a style="
                                                        position: absolute;
                                                        top: 50%;
                                                        right: -8px;
                                                        border:1px solid #ccc;
                                                        border-radius:15px;
                                                        width: 15px;
                                                        height: 15px;
                                                        font-size: 10px;
                                                        text-align: center;
                                                        background-color: #fff;"
                                                       href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                   @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeater(event,this)">Thêm</button>
                                </div>

                            </div>

                            <script>
                                function repeater(event, el) {
                                    event.preventDefault();
                                    var target = $(el).closest('.repeater').find('table tbody');
                                    $.get("{{ route('repeat.tour.plan') }}", function (data) {
                                        target.append(data)
                                    })
                                }
                            </script>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="activity3">
                    <div class="row">

                        <div class="col-lg-6">
                            <h4>Chọn thành phố</h4>
                            <div class="form-group">
                                <select class="form-control multislt" style="width: 100%" id="hotelSelected" name="hotelCities[]" multiple>
                                    <?php $oldHotelCities = old('hotelCities', $tour->hotelCities->count() ? $tour->hotelCities->pluck('id')->toArray() : []); ?>
                                    @foreach($hotelCities as $hotelCity)
                                        <option value="{{ $hotelCity->id }}" {{ in_array($hotelCity->id, $oldHotelCities) ? 'selected' : null }}>{{ $hotelCity->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-success" type="button" id="hotelUpdateBtn" onclick="hotelLoad(event)"><i class="fa fa-refresh"></i> Cập nhật</button>
                            </div>
                        </div>

                    </div> {{--./row--}}

                    @section('script')
                        <script>
                            function hotelLoad(e){
                                const hotelCities = $("#hotelSelected").val();
                                let hotels = [];
                                $("#hotelList").find('input:checked').each(function(i){
                                    hotels.push(this.value);
                                });

                                $.ajax({
                                    type: "GET",
                                    url: "{{route('tour.hotel')}}",
                                    data: {
                                        idCities: hotelCities,
                                        idHotels: hotels,
                                    },
                                    success: function (res) {
                                        $("#hotelList").html(res);
                                    }
                                })
                            }
                        </script>
                    @endsection

                    <div id="hotelList">

                        <?php $hotels =  \App\Hotel::whereIn('city_id', $oldHotelCities)->get()?>
                        <?php $hotelCities =  $tour->hotelCities()->whereIn('id', $oldHotelCities)->get(['id','name'])?>
                        <?php
                        $hotelTours = $tour->hotels()->get(['id']);
                        $hotelTours = $hotelTours->count() ? $hotelTours->pluck('id')->toArray() : [];
                        $selectedHotels =  old('hotels', $hotelTours)
                        ?>

                        @include('backend.tour.hotel',[
                            'hotels' => $hotels,
                            'hotelCities' => $hotelCities,
                            'selectedHotels' => $selectedHotels,
                        ])
                    </div>

                    {{--//old data--}}
                    <div class="row hidden">

                        <div class="col-lg-12">
                            <h4>Danh sách nội dung</h4>
                            <div class="repeater">
                                <table class="table table-bordered table-hover">
                                    <thead></thead>
                                    <tbody class="sortable">
                                    <?php
                                    $hotels = (array) json_decode($tour->hotel);
                                    $hotels = old('hotel',$hotels);
                                    ?>
                                    @if(!empty($hotels))
                                        <?php $count =1 ?>
                                        @foreach($hotels as $key => $hotel)
                                            <?php $hotel = (array)$hotel; ?>
                                            <tr>
                                                <td><span class="index">{{ $loop->iteration }}</span></td>
                                                <td style="position:relative">
                                                    <div class="row form-group">
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Tiêu đề</label></div>
                                                        <div class="col-lg-10"><input type="text" class="form-control" name="hotel[{!! $key !!}][type]" value="{!! @$hotel['type'] !!}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <?php $contents = (array) @$hotel['content'] ?>
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Nội dung</label></div>
                                                        <div class="col-lg-10">
                                                            <div class="repeater">
                                                                <table class="table table-bordered table-hover">
                                                                    <thead>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th style="width: 30%;">Thành phố</th>
                                                                        <th style="width: 40%;">Khách sạn</th>
                                                                        <th style="width: 30%;">Kiểu phòng</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="sortable">
                                                                    @if(!empty($contents))
                                                                        @foreach($contents as $index => $content)
                                                                            <?php $content = (array)$content; ?>
                                                                            <tr>
                                                                                <td><span class="index-child-{{ $key }}">{{ $loop->iteration }}</span></td>
                                                                                <td style="width: 30%;"><input type="text" class="form-control" name="hotel[{!! $key !!}][content][{!! $index !!}][city]" value="{!! $content['city'] !!}"></td>
                                                                                <td style="width: 40%;"><input type="text" class="form-control" name="hotel[{!! $key !!}][content][{!! $index !!}][name]" value="{!! $content['name'] !!}"></td>

                                                                                <td style="position:relative; width: 30%;">
                                                                                    <input type="text" class="form-control" name="hotel[{!! $key !!}][content][{!! $index !!}][type]" value="{!! $content['type'] !!}">
                                                                                    <a style="
                        position: absolute;
                        top: 50%;
                        right: -8px;
                        border:1px solid #ccc;
                        border-radius:15px;
                        width: 15px;
                        height: 15px;
                        font-size: 10px;
                        text-align: center;
                        background-color: #fff;"
                                                                                       href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                                                        <i class="fa fa-minus"></i>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                    </tbody>
                                                                </table>
                                                                <div class="text-right">
                                                                    <button class="btn btn-primary" onclick="repeaterHotelList(event,this,{!! $key !!})">Thêm khách sạn</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a style="
                    position: absolute;
                    top: 50%;
                    right: -8px;
                    border:1px solid #ccc;
                    border-radius:15px;
                    width: 15px;
                    height: 15px;
                    font-size: 10px;
                    text-align: center;
                    background-color: #fff;"
                                                       href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeaterHotelType(event,this)">Thêm</button>
                                </div>

                            </div>

                            <script>
                                function repeaterHotelType(event, el) {
                                    event.preventDefault();
                                    var target = $(el).closest('.repeater').find('table tbody').first();
                                    $.get("{{ route('repeat.tour.hotelType') }}",{'id': Date.now() }, function (data) {
                                        target.append(data)
                                    })
                                }

                                function repeaterHotelList(event, el, id) {
                                    event.preventDefault();
                                    id = id === null ? Date.now() : id;
                                    var target = $(el).closest('.repeater').find('table tbody').first();
                                    $.get("{{ route('repeat.tour.hotelList') }}", {'id': id }, function (data) {
                                        target.append(data)
                                    })
                                }
                            </script>
                        </div>
                    </div> {{--./row--}}
                    {{--end old data--}}
                </div>

                <div class="tab-pane" id="activity5">
                    <div class="row">
                        <div class="col-lg-4">
                            <h4>Chọn mẫu faq có sẵn</h4>
                            <p><a href="{!! route('faq.create') !!}" target="_blank"><i class="fa fa-plus"></i> <i>Tạo mẫu mới</i></a></p>

                            <select name="faq_id" class="form-control multislt" style="width: 100%;">
                                <option value="">Chọn</option>
                                @foreach($faqs as $faq)
                                    <option value="{!! $faq->id !!}" {!! old('faq_id',@$tour->faq_id) == $faq->id ? 'selected' : null !!}>{!! $faq->name !!}</option>
                                @endforeach
                            </select>


                        </div>
                    </div> {{--./row--}}

                </div>

                <div class="tab-pane" id="activity4">
                    <?php $gallery = old('gallery',$tour->gallery->toArray());?>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">

                                <div class="image">
                                    <button type="button" class="btn btn-primary" onclick="fileMultiSelect(this)"><i class="fa fa-upload"></i> Chọn hình ảnh</button>
                                    <br><br>
                                    <div class="image__gallery">
                                        @if(count($gallery))
                                            @foreach($gallery as $image)
                                                <div class="image__thumbnail image__thumbnail--style-1">
                                                    <img src="{{ image_url(@$image['url']) }}" >
                                                    <a href="javascript:void(0)" class="image__delete" onclick="urlFileMultiDelete(this)"><i class="fa fa-times"></i></a>
                                                    <input type="hidden" name="gallery[]" value="{{ @$image['url'] }}">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div> {{--./gallery--}}
                                </div> {{--./image--}}
                            </div>
                        </div>
                    </div> {{--./row--}}

                </div>

                <div class="tab-pane" id="activity6">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php $content = json_decode($tour->content,true); ?>

                                <div class="form-group">
                                    <label>Ảnh đại diện video</label>

                                    <div class="image">
                                        <?php $value = old('content.video.thumbnail',@$content['video']['thumbnail']); ?>
                                        <div class="image__thumbnail">
                                            <img src="{{ image_url($value) }}"  data-init="{{ image_url('') }}">
                                            <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                            <input type="hidden" name="content[video][thumbnail]" value="{{$value}}">
                                            <div class="image__button" onclick="fileSelect(this)">
                                                <i class="fa fa-upload"></i>
                                                Upload
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            <div class="form-group">
                                <label>Link video</label>
                                <input type="text" class="form-control" name="content[video][link]" value="{{ old('content.video.link',@$content['video']['link']) }}">

                            </div>
                        </div>
                    </div> {{--./row--}}

                </div>

                <div class="tab-pane" id="activity7">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Chọn mẫu Review</label> <br>
                                <select name="reviews" class="form-control multislt" style="width: 100%;">
                                    <option value="">Chọn</option>
                                    @foreach($reviewCategories as $category)
                                        <option value="{{$category->id}}" {{old('reviews',@$tour->reviews->first()->id) == $category->id ? 'selected' : null }}>{{$category->name}}({{$category->reviews->count()}})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary" id="tourSaveBtn">Lưu </button>

    </form>


@endsection
