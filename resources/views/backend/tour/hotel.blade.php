@foreach($hotelCities as $hotelCity)
    <h4 class="text-primary" style="font-size: 16px; border-bottom: 1px solid #ccc;">{{$hotelCity->name}}</h4>
    <div class="row">
        @foreach($hotels->where('city_id', $hotelCity->id) as $hotel)
            <div class="col-sm-6 col-lg-3"><label><input type="checkbox" name="hotels[]" value="{{$hotel->id}}" {{ in_array($hotel->id, $selectedHotels) ? 'checked' : '' }}> {{$hotel->name}}</label></div>
        @endforeach
    </div>
@endforeach


