@extends('backend.index')
@section('controller','Ngôn ngữ')
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')
    @include('backend.block.error')

    <form action="{!! route('langConfig.update', 'null') !!}" method='POST' enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}
        <p><i>**Nhập đường dẫn tĩnh các trang ngôn ngữ phía dưới</i></p>
        <hr>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">

                    <div class="row">
                        <div class="col-lg-4">

                            @foreach($language as $lang)
                                <div class="form-group">
                                    <label>{{ $lang->name }}</label>
                                    <input type="text" class="form-control" name="link[{{ $lang->locale }}]" value="{!! old('link.'.$lang->locale.'',$lang->link) !!}">
                                </div>
                            @endforeach


                            {{--<div class="form-group">--}}
                                {{--<label>Tiếng Pháp</label>--}}
                                {{--<input type="text" class="form-control" name="content[fr]" value="{!! old('content.en',@$content->fr) !!}">--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label>Tiếng Đức</label>--}}
                                {{--<input type="text" class="form-control" name="content[de]" value="{!! old('content.en',@$content->de) !!}">--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label>Tiếng Tây Ban Nha</label>--}}
                                {{--<input type="text" class="form-control" name="content[es]" value="{!! old('content.en',@$content->es) !!}">--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--<label>Tiếng Ý</label>--}}
                                {{--<input type="text" class="form-control" name="content[it]" value="{!! old('content.en',@$content->it) !!}">--}}
                            {{--</div>--}}


                        </div>
                    </div> {{--./row--}}

                </div>

                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>

        <button type="submit" class="btn btn-primary">Lưu</button>
    </form>

@endsection