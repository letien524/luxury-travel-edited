<?php
if (@$configs['action'] == 'update') {
    $showRoute = route('tourActivity.detail', $tourActivity->slug);
    $formActionRoute = route('tourActivity.update', $tourActivity);
    $nameRoute = \App\Enum\RouteActionEnum::ACTIONS['update'];
} else {
    $showRoute = route('activity');
    $formActionRoute = route('tourActivity.store');
    $nameRoute = \App\Enum\RouteActionEnum::ACTIONS['create'];
}
?>
@extends('backend.index')
@section('controller','Activities')
@section('controller_route',route('tourActivity.index'))
@section('action', $nameRoute)
@section('live', $showRoute)

@section('content')
    @include('backend.block.error')
    <form action="{{ $formActionRoute }}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if(@$configs['action'] == 'update')
            {!! method_field('put') !!}
        @endif

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
                <li class=""><a href="#activity7" data-toggle="tab" aria-expanded="true">Review</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Ảnh đại diện</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image', @$tourActivity->image)) }}"
                                             data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete"
                                           onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image"
                                               value="{{ old('image',@$tourActivity->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" data-target="#slug" name="name" id="name"
                                       value="{!! old('name', @$tourActivity->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="slug" id="slug"
                                       value="{!! old('slug', @$tourActivity->slug) !!}">
                            </div>
                        </div>


                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Canonical URL</label>
                                <input type="text" class="form-control" name="canonical"
                                       value="{!! old('canonical', @$tourActivity->canonical) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title', @$tourActivity->meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="4">{!! old('meta_description', @$tourActivity->meta_description) !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Số thứ tự</label>
                                <input type="number" min="0" class="form-control" name="order_menu"
                                       value="{!! old('order_menu', @$tourActivity->order_menu) !!}">
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="status"
                                               value="1" {!! @$tourActivity->status == 1 ? 'checked' : null !!}>
                                        Hiển thị
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="index"
                                               value="1" {{ @$tourActivity->index == 1  ? 'checked' : null }}>
                                        Robot index
                                    </label>
                                </div>
                            </div>
                        </div>


                    </div> {{--./row--}}

                    <?php $content = json_decode(@$tourActivity->content, true); ?>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>Banner trên</h4>
                            <div class="form-group">
                                <label>Hình ảnh</label><br>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.banner.top.image',@$content['banner']['top']['image'])) }}"
                                             data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete"
                                           onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[banner][top][image]"
                                               value="{{ old('content.banner.top.image',@$content['banner']['top']['image'])  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>

                            <div class="form-group">
                                <label>Tiêu đề phụ</label>
                                <input type="text" class="form-control" name="content[banner][top][name]"
                                       value="{!! old('content.banner.top.name',@$content['banner']['top']['name']) !!}">
                            </div>

                        </div>

                        <div class="col-lg-6">
                            <h4>Banner dưới</h4><br>

                            <div class="image">
                                <div class="image__thumbnail">
                                    <img src="{{ image_url(old('content.banner.bottom.image',@$content['banner']['bottom']['image'])) }}"
                                         data-init="{{ image_url('') }}">
                                    <a href="javascript:void(0)" class="image__delete"
                                       onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                    <input type="hidden" name="content[banner][bottom][image]"
                                           value="{{ old('content.banner.bottom.image',@$content['banner']['bottom']['image'])  }}">
                                    <div class="image__button" onclick="fileSelect(this)">
                                        <i class="fa fa-upload"></i>
                                        Upload
                                    </div>
                                </div>
                            </div> {{--./image--}}
                        </div>
                    </div> {{--./row--}}

                    <hr>
                    <h3> Giới thiệu</h3>
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[name]"
                                       value="{!! old('content.name',@$content['name']) !!}">
                            </div>

                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea name="content[content]" class="form-control"
                                          rows="8">{!! old('content.content',@$content['content']) !!}</textarea>
                            </div>
                        </div>

                    </div> {{--./row--}}
                    <hr>
                    <h3> Tour guide</h3>
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="form-group">
                                <h4>Chọn mẫu faq có sẵn</h4>
                                <p><a href="{{ route('faq.create') }}" target="_blank"><i class="fa fa-plus"></i>
                                        <i>Tạo mẫu mới</i></a></p>

                                <select name="faq_id" class="form-control multislt" style="width: 100%;">
                                    <option value="">Chọn</option>
                                    @foreach($faqs as $faq)
                                        <option value="{{ $faq->id }}" {{ old('faq_id',@$tourActivity->faq_id) == $faq->id ? 'selected' : null }}>
                                            {{ $faq->name }}
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group">
                                <label>Hướng dẫn viên</label>
                                <select name="member_id" class="form-control" id="e4">
                                    <option value="">Chọn</option>
                                    @foreach($members as $member)
                                        <option value="{!! $member->id !!}"
                                                data-image="{!! image_url($member->image) !!}" {!! old('member_id', @$tourActivity->member_id) == $member->id  ? 'selected' : null !!} >
                                            {!! $member->name !!}
                                            - {!! $member->position !!}</option>
                                    @endforeach
                                </select>
                            </div>

                            <script>

                              $("#e4").select2({
                                templateResult: format,
                                templateSelection: format,
                                escapeMarkup: function (m) {
                                  return m;
                                }
                              });

                              function format(state) {
                                var originalOption = state.element;

                                var image = typeof $(originalOption).data('image') !== undefined ? $(originalOption).data("image") : false;

                                image = image ? '<img src="' + image + '" style="height:30px;margin-right:5px;" />' : '';

                                return image + state.text;
                              }
                            </script>
                        </div>
                    </div>

                </div>

                <div class="tab-pane" id="activity7">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Chọn mẫu Review</label> <br>

                                <select name="review_category_id" class="form-control multislt" style="width: 100%;">
                                    <option value="">Chọn</option>
                                    @foreach($reviewCategories as $review)
                                        <option value="{{$review->id}}" {{ old('review_category_id', @$tourActivity->review_category_id) == $review->id ? 'selected' : null }}>
                                            {{ $review->name }}({{ $review->count() }})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> {{--./row--}}
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->


        <button type="submit" class="btn btn-primary">OK</button>
    </form>

@endsection
