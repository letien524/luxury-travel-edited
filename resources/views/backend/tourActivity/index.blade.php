@extends('backend.index')
@section('controller','Activities')
@section('controller_route',route('tourActivity.index'))
@section('action','Danh sách')
@section('live',route('activity'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
        'routeBulkDestroy' => route('tourActivity.bulkDestroy'),
        'routeCreate' => route('tourActivity.create'),
        'datatables' => $datatables,
        'object' => new \App\Http\Controllers\TourActivityController,
    ])
@endsection
