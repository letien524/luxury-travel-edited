@extends('backend.index')
@section('controller','Cấu hình chung')
@section('controller_route',route('tourConfig.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('tourConfig.update', $tourConfig) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Activities</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="false">Tour search</a></li>
                <li class=""><a href="#activity2" data-toggle="tab" aria-expanded="false">Tour Menu Destinations</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Ảnh đại diện</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',$tourConfig->image) ? old('image',$tourConfig->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$tourConfig->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <?php $content = json_decode($tourConfig->content);?>
                            <div class="form-group">
                                <label>Banner trên</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.banner.top.image',@$content->banner->top->image) ? old('content.banner.top.image',@$content->banner->top->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[banner][top][image]" value="{{ old('content.banner.top.image',@$content->banner->top->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Banner dưới</label>

                                    <div class="image">
                                        <div class="image__thumbnail">
                                            <img src="{{ image_url(old('content.banner.bottom.image',@$content->banner->bottom->image) ? old('content.banner.bottom.image',@$content->banner->bottom->image) : null) }}"  data-init="{{ image_url('') }}">
                                            <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                            <input type="hidden" name="content[banner][bottom][image]" value="{{ old('content.banner.bottom.image',@$content->banner->bottom->image)  }}">
                                            <div class="image__button" onclick="fileSelect(this)">
                                                <i class="fa fa-upload"></i>
                                                Upload
                                            </div>
                                        </div>
                                    </div> {{--./image--}}

                                </div>
                            </div>
                        </div>


                    </div> {{--./row--}}
                    <hr>

                    <div class="row">

                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name',$tourConfig->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Tiêu đề phụ</label>
                                <input type="text" class="form-control" name="content[banner][top][name]" value="{!! old('content.banner.top.name',@$content->banner->top->name) !!}">
                            </div>

                        </div> {{--./col-20--}}

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Giới thiệu(tiêu đề)</label>
                                <input type="text" class="form-control" name="content[about][name]" value="{!! old('content.about.name',@$content->about->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Mô tả ngắn</label>
                                <textarea name="content[about][content]" class="form-control" rows="5">{!! old('content.about.content',@$content->about->content) !!}</textarea>
                            </div>

                        </div> {{--./col-20--}}

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title" value="{!! old('meta_title',$tourConfig->meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control" rows="5">{!! old('meta_description',$tourConfig->meta_description) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="index" value="1" {{ $tourConfig->index == 1  ? 'checked' : null }}>
                                        Robot index
                                    </label>
                                </div>
                            </div>
                        </div> {{--./col-20--}}


                    </div> {{--./col-12--}}
                    <hr>

                </div> {{--./tab--}}

                <div class="tab-pane" id="activity1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[tourSearch][name]" value="{{ old('content.tourSearch.name', @$content->tourSearch->name) }}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="content[tourSearch][link]" value="{{ old('content.tourSearch.link', @$content->tourSearch->link) }}">
                            </div>

                            <div class="form-group">
                                <label>Button CTA label</label>
                                <input type="text" class="form-control" name="content[tourSearch][btnLabel]" value="{{ old('content.tourSearch.btnLabel', @$content->tourSearch->btnLabel) }}">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="content[tourSearch][display]" value="1" {{ @$content->tourSearch->display == 1  ? 'checked' : null }}>
                                    Hiển thị
                                </label>
                            </div>

                        </div>
                    </div>
                </div> {{--./tab--}}

                <div class="tab-pane" id="activity2">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[tourMenuDestination][name]" value="{{ old('content.tourMenuDestination.name', @$content->tourMenuDestination->name) }}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="content[tourMenuDestination][link]" value="{{ old('content.tourMenuDestination.link', @$content->tourMenuDestination->link) }}">
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="content[tourMenuDestination][display]" value="1" {{ @$content->tourMenuDestination->display == 1  ? 'checked' : null }}>
                                    Hiển thị
                                </label>
                            </div>

                        </div>
                    </div>
                </div> {{--./tab--}}

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-meta_description -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
