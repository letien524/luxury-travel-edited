@extends('backend.index')
@section('controller','Mail Collection')
@section('controller_route',route('contactCollection.index'))
@section('action','Danh sách')
@section('live',route('contact'))
@section('content')

    @include('backend.block.error')


    @include('backend.components.table-form', [
               'routeBulkDestroy' => route('contactCollection.bulkDestroy'),
               'routeCreate' => route('contactCollection.create'),
               'datatables' => $datatables,
               'object' => new \App\Http\Controllers\ContactCollectionController,
           ])

@endsection
