@extends('backend.index')
@section('controller','Thẻ tin tức')
@section('controller_route',route('blogTag.index'))
@section('action','Danh sách')
@section('live',route('blog'))
@section('content')

    @include('backend.block.error')


    @include('backend.components.table-form', [
              'routeBulkDestroy' => route('blogTag.bulkDestroy'),
              'routeCreate' => route('blogTag.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\blogTagController,
          ])
@endsection