<?php
if(isUpdateBlade(@$configs['action'])){
    $actionLableRoute = \App\Enum\RouteActionEnum::ACTIONS['update'];
    $actionFormRoute = route('popup.update', $popup);
}else{
    $actionLableRoute = \App\Enum\RouteActionEnum::ACTIONS['create'];
    $actionFormRoute = route('popup.store');
}

?>

@extends('backend.index')
@section('controller','Popup')
@section('controller_route',route('popup.index'))
@section('action', $actionLableRoute)
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! $actionFormRoute !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image', @$popup->image)) }}" data-init="{{ __IMAGE_THUMBNAIL_DEFAULT }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image', @$popup->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>
                        </div> <!-- ./col -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name', @$popup->name) }}">
                            </div>
                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="link" value="{{ old('link', @$popup->link) }}">
                            </div>
                            <div class="form-group">
                                <label>Thời gian trì hoãn(giây)</label>
                                <input type="number" class="form-control" name="delay_time" value="{{ old('delay_time', @$popup->delay_time) }}">
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề Button CTA</label>
                                <input type="text" class="form-control" name="button_name_display" value="{{ old('button_name_display', @$popup->button_name_display) }}">
                            </div>
                        </div> <!-- ./col -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Số thứ tự</label>
                                <input type="number" min="0" class="form-control" name="order_menu" value="{{ old('order_menu', @$popup->order_menu) }}">
                            </div>

                            <div class="form-group">
                                <label>Kiểu Popup</label>
                                <select name="type" class="form-control">
                                    @foreach(\App\Enum\PopupEnum::TYPES as $key => $value)
                                        <option value="{{ $key }}" {{ old('type', @$popup->type) == $key ? 'selected' : null }}>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="is_active" value="1" {!! old('is_active',@$popup->is_active) == 1 ? 'checked' : null!!}>
                                        Active
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Tùy chọn hiển thị</label> <br>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="is_display" value="1" {!! old('is_display',@$popup->is_display) == 1 ? 'checked' : null!!}>
                                        Hiển thị
                                    </label>
                                </div>

                                <div class="radio">
                                    <label>
                                        <input type="radio" name="is_display" value="0" {!! old('is_display',@$popup->is_display) != 1 ? 'checked' : null!!}>
                                        Ẩn
                                    </label>
                                </div>
                            </div>
                        </div> <!-- ./col -->

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nội dung</label> <br>
                                <textarea name="content" class="form-control content" rows="5">{!! old('content', @$popup->content) !!}</textarea>
                            </div>
                        </div> <!-- ./col -->

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Black list</label> <br>
                                <code>/duong_dan_1,</code> <br>
                                <code>/duong_dan_2</code> <br> <br>
                                <textarea name="black_list" class="form-control" rows="11">{{ isset($popup) ? $popup->getBlackListField() : null }}</textarea>

                            </div>
                        </div> <!-- ./col -->

                    </div>
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        @if(isUpdateBlade(@$configs['action']))
            @include('backend.components.save-as-button', ['storeRoute' => route('popup.store')])
        @endif
        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
