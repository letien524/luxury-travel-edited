@extends('backend.index')
@section('controller','Popup')
@section('controller_route',route('popup.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
                 'routeBulkDestroy' => route('popup.bulkDestroy'),
                 'routeCreate' => route('popup.create'),
                 'datatables' => $datatables,
                 'object' => new \App\Http\Controllers\PopupController,
             ])
@endsection
