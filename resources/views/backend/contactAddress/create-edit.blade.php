<?php
if(isUpdateBlade(@$configs['action'])){
    $actionLabel = \App\Enum\RouteActionEnum::ACTIONS['update'];
    $actionFormRoute = route('contactAddress.update',@$contactAddress);
}else{
    $actionLabel = \App\Enum\RouteActionEnum::ACTIONS['create'];
    $actionFormRoute = route('contactAddress.store');
}

?>

@extends('backend.index')
@section('controller','Địa chỉ')
@section('controller_route',route('contactAddress.index'))
@section('action', $actionLabel)
@section('live',route('contact'))
@section('content')

    @include('backend.block.error')

    <form action="{!! $actionFormRoute !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="type" value="address">
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name',@$contactAddress->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <textarea name="address" class="form-control" rows="5">{!! old('address',@$contactAddress->address) !!}</textarea>
                            </div>
                        </div> {{--./col-20--}}

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input type="tel" class="form-control" name="phone" value="{!! old('phone',@$contactAddress->phone) !!}">
                            </div>

                            <div class="form-group">
                                <label>Hotline</label>
                                <input type="tel" class="form-control" name="hotline" value="{!! old('hotline',@$contactAddress->hotline) !!}">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" value="{!! old('email',@$contactAddress->email) !!}">
                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Số thứ tự</label>
                                <input type="number" min="0"  class="form-control" name="order_menu" value="{!! old('order_menu',@$contactAddress->order_menu) !!}">
                            </div>
                        </div>


                    </div> {{--./row--}}


                </div>
            </div>

            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">OK</button>
    </form>

@endsection
