<?php
$controller = 'Địa chỉ';
$route = 'contactAddress.index';
$routeCreate = 'contactAddress.create';
$routeStore = 'contactAddress.store';
$routeEdit = 'contactAddress.edit';
$routeUpdate ='contactAddress.update';
$routeDestroy = 'contactAddress.destroy';
$routeBulkDestroy = 'contactAddress.bulkDestroy';
?>
@extends('backend.index')
@section('controller',$controller)
@section('controller_route',route($route))
@section('action','Danh sách')
@section('live',route('contact'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
               'routeBulkDestroy' => route($routeBulkDestroy),
               'routeCreate' => route($routeCreate),
               'datatables' => $datatables,
               'object' => new \App\Http\Controllers\ContactAddressController,
           ])

@endsection
