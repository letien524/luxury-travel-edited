@extends('backend.index')
@section('controller','Footer menu')
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')
    @include('backend.block.error')

    <form action="{!! route('footerMenuConfig.update',$footer_menu) !!}" method='POST' enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <?php $content = json_decode($footer_menu->content); ?>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Luxury Travel Menu</label>
                                <textarea name="content[luxury_travel]" class="content form-control" >{!! old('content.luxury_travel',@$content->luxury_travel) !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Information Menu</label>
                                <textarea name="content[information]" class="content form-control">{!! old('content.information',@$content->information) !!}</textarea>
                            </div>
                        </div>


                    </div> {{--./row--}}

                </div>

                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>

        <button type="submit" class="btn btn-primary">Lưu</button>
    </form>

@endsection