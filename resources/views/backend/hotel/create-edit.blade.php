<?php

if(isUpdateBlade(@$configs['action'])){
    $pageActionLabel = \App\Enum\RouteActionEnum::ACTIONS['update'];
    $pageActionForm = route('hotel.update',$hotel);
}else{
    $pageActionLabel = \App\Enum\RouteActionEnum::ACTIONS['create'];
    $pageActionForm = route('hotel.store');
}

?>
@extends('backend.index')
@section('controller','Khách sạn')
@section('controller_route',route('hotel.index'))
@section('action',  $pageActionLabel)
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{{ $pageActionForm }}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" id="name" value="{!! old('name', @$hotel->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Kiểu khách sạn</label>
                                <select name="type_id" class="form-control multislt">
                                    <option value="">Chọn</option>
                                    @if($hotelTypes->count())
                                        @foreach($hotelTypes as $item)
                                            <option value="{{ $item->id }}" {{ $item->id == old('type_id', @$hotel->type_id) ? 'selected' : null }}>{{ $item->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Kiểu phòng</label>
                                <select name="room_type_id" class="form-control multislt">
                                    <option value="">Chọn</option>
                                    @if($hotelRoomTypes->count())
                                        @foreach($hotelRoomTypes as $item)
                                            <option value="{{ $item->id }}" {{ $item->id == old('room_type_id', @$hotel->room_type_id) ? 'selected' : null }}>{{ $item->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Thành phố</label>
                                <select name="city_id" class="form-control multislt">
                                    <option value="">Chọn</option>
                                    @if($hotelCities->count())
                                        @foreach($hotelCities as $item)
                                            <option value="{{ $item->id }}" {{ $item->id == old('city_id', @$hotel->city_id) ? 'selected' : null }}>{{ $item->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </div>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        @if(isUpdateBlade(@$configs['action']))
            @include('backend.components.save-as-button', ['storeRoute' => route('hotel.store')])
        @endif
        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
