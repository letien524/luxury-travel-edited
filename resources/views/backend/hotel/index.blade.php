@extends('backend.index')
@section('controller','Khách sạn')
@section('controller_route',route('hotel.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

@include('backend.block.error')

@include('backend.components.table-form', [
              'routeBulkDestroy' => route('hotel.bulkDestroy'),
              'routeCreate' => route('hotel.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\HotelController,
          ])
@endsection
