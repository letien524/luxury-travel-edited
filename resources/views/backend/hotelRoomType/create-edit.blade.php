<?php

if(isUpdateBlade(@$configs['action'])){
    $pageActionLabel = \App\Enum\RouteActionEnum::ACTIONS['update'];
    $pageActionForm = route('hotelRoomType.update',$hotelRoomType);
}else{
    $pageActionLabel = \App\Enum\RouteActionEnum::ACTIONS['create'];
    $pageActionForm = route('hotelRoomType.store');
}

?>
@extends('backend.index')
@section('controller','Kiểu phòng')
@section('controller_route',route('hotelRoomType.index'))
@section('action',  $pageActionLabel)
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{{ $pageActionForm }}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name', @$hotelRoomType->name) !!}">
                            </div>
                        </div>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection