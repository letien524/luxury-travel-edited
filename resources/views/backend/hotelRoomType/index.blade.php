@extends('backend.index')
@section('controller','Kiêu phòng')
@section('controller_route',route('hotelRoomType.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

@include('backend.block.error')

@include('backend.components.table-form', [
              'routeBulkDestroy' => route('hotelRoomType.bulkDestroy'),
              'routeCreate' => route('hotelRoomType.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\HotelRoomTypeController,
          ])
@endsection
