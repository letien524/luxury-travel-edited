@extends('backend.index')
@section('controller','Thương hiệu')
@section('controller_route',route('brand.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
                 'routeBulkDestroy' => route('brand.bulkDestroy'),
                 'routeCreate' => route('brand.create'),
                 'datatables' => $datatables,
                 'object' => new \App\Http\Controllers\BrandController,
             ])
@endsection
