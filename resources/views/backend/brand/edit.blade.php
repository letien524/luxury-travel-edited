@extends('backend.index')
@section('controller','Thương hiệu')
@section('controller_route',route('brand.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('brand.update',$brand) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>


            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',$brand->image) ? old('image',$brand->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$brand->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" id="name"value="{!! old('name',$brand->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Danh mục</label>
                                <select name="type" class="form-control">
                                    <option value="">Chọn danh mục</option>
                                    <option value="association" {!! $brand->type === 'association' ? 'selected' : null !!}>Association</option>
                                    <option value="awards" {!! $brand->type === 'awards' ? 'selected' : null !!}>Awards</option>
                                    <option value="news" {!! $brand->type === 'news' ? 'selected' : null !!}>News</option>
                                </select>
                            </div>
                        </div>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection