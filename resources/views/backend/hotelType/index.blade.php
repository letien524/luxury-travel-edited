@extends('backend.index')
@section('controller','Kiêu khách sạn')
@section('controller_route',route('hotelType.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

@include('backend.block.error')

@include('backend.components.table-form', [
              'routeBulkDestroy' => route('hotelType.bulkDestroy'),
              'routeCreate' => route('hotelType.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\HotelTypeController,
          ])
@endsection
