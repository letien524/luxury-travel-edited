@extends('backend.index')
@section('controller','Destinations')
@section('controller_route',route('tourDestination.index'))
@section('action','Danh sách')
@section('live',route('home'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
        'routeBulkDestroy' => route('tourDestination.bulkDestroy'),
        'routeCreate' => route('tourDestination.create'),
        'datatables' => $datatables,
        'object' => new \App\Http\Controllers\TourActivityController,
    ])
@endsection
