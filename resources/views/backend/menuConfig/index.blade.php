@extends('backend.index')
@section('controller','Menu')
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')
    @include('backend.block.error')
    <form action="{{ route('menuConfig.update',$menu) }}" id="menuForm" method="post">
        {{ csrf_field() }}
        {{ method_field('put') }}
        <textarea hidden id="nestable-output" name="content"></textarea>
    </form>
    <?php $menu = json_decode($menu->content, true);?>

    <div class="row">
        <div class="col-lg-6">

            <div class="form-group">

                <div class="dd" id="nestable">

                   {!! adminMegaMenu($menu) !!}

                    <div class="form-group text-right">
                        <button type="button" data-target="#modal-default" data-action="itemCreate" id="itemCreate"
                                data-toggle="modal" class="btn-sm btn btn-success">
                            <i class="fa fa-plus"></i> Thêm mới
                        </button>
                    </div>
                    <div class="modal fade" id="modal-default">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Thông tin menu</h4>
                                </div>
                                <div class="modal-body">
                                    <form action="" id="formItemMenu">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Tiêu đề</label>
                                                    <input type="text" name="name" class="form-control" value="">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Đường dẫn</label>
                                                    <input type="text" name="slug" class="form-control" value="">
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="is_destination"> Destination
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="is_activity"> Activity
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>

                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                                    <button type="button" id="itemStore" data-action="itemStore"
                                            class="btn btn-primary">Lưu
                                    </button>
                                </div>

                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    {{--modal--}}
                </div>
            </div>
            <div class="form-group">
                <hr>
                <button type="button" onclick="$('#menuForm').submit()" class="btn-sm btn btn-primary">
                    <i class="fa fa-save"></i> Lưu
                </button>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ getAdminAsset('plugins/nestable/jquery.nestable.js') }}"></script>
    <script src="{{ getAdminAsset('plugins/nestable/script.js') }}"></script>
@endsection
