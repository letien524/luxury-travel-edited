@extends('backend.index')
@section('controller','Bình luận')
@section('controller_route',route('comment.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
              'routeBulkDestroy' => route('comment.bulkDestroy'),
              'routeCreate' => route('comment.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\CommentController(),
          ])
@endsection
