@extends('backend.index')
@section('controller','Bình luận')
@section('controller_route',route('comment.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('comment.update',$comment) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>


            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="">
                                <table class="table table-bordered">

                                    <tr>
                                        <td style="width: 100px;">Thời gian</td>
                                        <td>{!! date_format(date_create($comment->created_at),"Y.m.d - H:m:s")  !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Bài viết</td>
                                        <td>
                                            <?php
                                            $image = json_decode(@$comment->blog->image, true);
                                            ?>
                                            <a class="text-default" target="_blank" href="{!! route('blog.detail',@$comment->blog->slug) !!}" title="{!! @$comment->blog->name !!}">
                                                <img src="{!! image_url(@$image['thumbnail']) !!}" class="img-responsive imglist" />
                                                <span class="name">{!! @$comment->blog->name !!}</span>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Họ tên</td>
                                        <td>{!! $comment->name !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Email</td>
                                        <td><a href="mailto:{!! $comment->email !!}" title="{!! $comment->email !!}">{!! $comment->email !!}</a></td>
                                    </tr>
                                    <tr>
                                        <td>Số điện thoại</td>
                                        <td><a href="tel:{!! $comment->phone !!}" title="{!! $comment->phone !!}">{!! $comment->phone !!}</a></td>
                                    </tr>
                                    <tr>
                                        <td>Website</td>
                                        <td><a href="{!! $comment->website !!}" target="_blank" title="{!! $comment->website !!}">{!! $comment->website !!}</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea class="form-control" rows="6" name="content">{!! old('content',$comment->content) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select name="status" class="form-control">
                                    <option value="">Mới</option>
                                    <option value="1" {!! old('status',$comment->status) == 1 ? 'selected' : null !!}>Hiển thị</option>
                                    <option value="2" {!! old('status',$comment->status) == 2 ? 'selected' : null !!}>Ẩn</option>

                                </select>
                            </div>
                        </div>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
