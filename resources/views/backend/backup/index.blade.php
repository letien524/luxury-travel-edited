@extends('backend.index')
@section('controller','Backup & Restore')
@section('controller_route',route('backup.index'))
@section('action','')
@section('live',route('home'))

@section('content')

    @include('backend.block.error')

    <div class="row">

        <div class="col-lg-12">
            @if(isDeveloper())
                <form action="{{route('backup.migrate')}}" method="post">
                    {!! csrf_field() !!}
                    <button type="submit">Migrate</button>
                </form>
                <form action="{{route('backup.seed')}}" method="post">
                    {!! csrf_field() !!}
                    <button type="submit">Seed</button>
                </form>
                <form action="{{route('backup.migrateSeed')}}" method="post">
                    {!! csrf_field() !!}
                    <button type="submit">Migrate + Seed</button>
                </form>
                <form action="{{route('backup.dataTransfer')}}" method="post">
                    {!! csrf_field() !!}
                    <button type="submit">Transfer data categories to tour_activities and tour_destinations</button>
                </form>
                <form action="{{route('backup.dataTransferMember')}}" method="post">
                    {!! csrf_field() !!}
                    <button type="submit">backup.dataTransferMember</button>
                </form>
                <form action="{{route('backup.dataTransferDestinationReview')}}" method="post">
                    {!! csrf_field() !!}
                    <button type="submit">backup.dataTransferDestinationReview</button>
                </form>



            @endif
            <form action="" method='POST' enctype="multipart/form-data" onsubmit="return confirm('{{ \App\Enum\CommonEnum::MESSAGES['restoreConfirm'] }}')"
                 >
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" name="import_file" id="exampleInputFile" accept='.zip'>
                </div>

{{--                <a href="{{ route('backup.seeder') }}" class="btn btn-danger">Create seeder</a>--}}
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default"> Create backup </button>
                <button type="submit" class="btn btn-warning">Restore</button>
            <!-- /.tab-content -->
             </form>
                @if(count($backupFiles))
                    <hr>
                    <h4>Backup List</h4>
                    <form action="{{ route('backup.bulkDestroy') }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('{{ \App\Enum\CommonEnum::MESSAGES['deleteConfirm'] }}')"><i class="fa fa-trash-o"></i> Xóa </button>
                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="example1" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 20px;"><input type="checkbox" name="chkAll" id="chkAll"></th>
                                    <th style="width: 20px;">STT</th>
                                    <th>Name</th>
                                    <th>Size(b)</th>
                                    <th>Last modified</th>
                                    <th style="width: 150px;">Actions</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($backupFiles as $file)
                                    <tr>
                                        <td><input type="checkbox" name="chkItem[]" value="{{ $file }}"></td>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <a href="#downloadBackupModal"
                                               onclick="backupHandleGetFile('{{ $file }}','#downloadBackupModal')"
                                               data-toggle="modal"
                                               data-target="#downloadBackupModal"
                                               title="Download: {{ $file }}">
                                                <i class="fa fa-download"></i> {{ $file }}
                                            </a>

                                        </td>
                                        <td><span title="{{ backupDisk()->size($file) }}b">{{ backupDisk()->size($file) }}</span></td>
                                        <td>{{ \Carbon\Carbon::createFromTimestamp(backupDisk()->lastModified($file)) }}</td>

                                        <td>
                                            <div>
                                                <a href="#restoreBackupModal"  onclick="backupHandleGetFile('{{ $file }}','#restoreBackupModal')" data-toggle="modal" data-target="#restoreBackupModal" title="Restore"><i class="fa fa-refresh"></i> Restore</a> |
                                                <a href="#destroyBackupModal" onclick="backupHandleGetFile('{{ $file }}','#destroyBackupModal')" data-toggle="modal" data-target="#destroyBackupModal" class="text-danger" title="Delete"><i class="fa fa-trash-o"></i> Delete</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </form>

                @endif
        </div>

    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('backup.create') }}" method="post" onsubmit="$('#modal-default').modal('hide')">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{ \App\Enum\BackupEnum::MESSAGES['createBackupLabel'] }}</h4>
                    </div>
                    <div class="modal-body">
                        {{ \App\Enum\BackupEnum::MESSAGES['createBackupConfirm'] }}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="destroyBackupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('backup.destroy') }}" method="post" onsubmit="$('#destroyBackupModal').modal('hide')">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                    <input type="text" name="file" class="backup-input-form-control" hidden value="">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"> {{ \App\Enum\BackupEnum::MESSAGES['deleteBackupLabel'] }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            {{ \App\Enum\CommonEnum::MESSAGES['deleteConfirm'] }} <code class="file-name-space"></code>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-danger btn-danger">Delete</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="restoreBackupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('backup.restore') }}" method="post" onsubmit="$('#restoreBackupModal').modal('hide')">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <input type="text" name="import_file" class="backup-input-form-control" hidden value="">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{ \App\Enum\BackupEnum::MESSAGES['restoreBackupLabel'] }}</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            {{ \App\Enum\CommonEnum::MESSAGES['restoreConfirm'] }} <code class="file-name-space"></code>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-danger btn-danger">Restore</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="downloadBackupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('backup.download') }}" method="post" onsubmit="$('#downloadBackupModal').modal('hide')">
                    {{ csrf_field() }}
                    <input type="text" name="file" class="backup-input-form-control" hidden value="">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            {{ \App\Enum\BackupEnum::MESSAGES['downloadConfirm'] }} <code class="file-name-space"></code>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Download</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script>
        function backupHandleGetFile(file, modal){
            const input = $(modal).find('.backup-input-form-control');
            const fileNameSpace = $(modal).find('.file-name-space');
          input.val(file);
          fileNameSpace.html(file)
        }
    </script>
@endsection
