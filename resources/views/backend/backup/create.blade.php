    @extends('backend.index')
@section('controller','Giới thiệu')
@section('controller_route',route('about.index'))
@section('action','Thêm')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('about.store') !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="type" value="about">

        <div class="div">

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Thông tin chung</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Ảnh đại diện</label>
                                <div class="file-loading">
                                    <input class="inpImg" name="image" type="file" value="{!! old('image') !!}">
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Tiêu đề trang</label>
                                <input type="text" class="form-control" name="name"
                                       value="{!! old('name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Ngôn ngữ</label>
                                <select name="lang_id" class="form-control multislt" data-placeholder="Chọn">
                                    @foreach($langs as $lang)
                                        <option value="{!! $lang->id !!}">{!! $lang->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title') !!}">
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="5">{!! old('meta_description') !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Who We Are</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="file-loading">
                                    <input class="inpImg" name="content[section1][image]" type="file" >
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section1][name]"
                                       value="{!! old('content.section1.image','Who We Are') !!}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[section1][content]" class="form-control"
                                          rows="5">{!! old('content.section1.content') !!}</textarea>
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Our Business Is Sustainable And Responsible</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section2][name]"
                                       value="{!! old('content.section2.name','Our Business Is Sustainable And Responsible') !!}">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label>Nội dung</label>
                        </div>
                        <div class="col-lg-6">

                            <div class="form-group">

                                <textarea name="content[section2][content][]" class="form-control"
                                          rows="5">{!! old('content.section2.content.0') !!}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <textarea name="content[section2][content][]" class="form-control"
                                          rows="5">{!! old('content.section2.content.1') !!}</textarea>
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section2--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Banner</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="file-loading">
                                    <input class="inpImg" name="content[section3][image]" type="file">
                                </div>
                            </div>
                        </div>

                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section3--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Our Selling Point</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Tiêu đề chính </label>
                                <input type="text" class="form-control" name="content[section4][name]"
                                       value="{!! old('content.section4.name','Our Selling Point') !!}">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <hr>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section4][content][0][name]"
                                       value="{!! old('content.section4.content.0.name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[section4][content][0][content]" class="form-control"
                                          rows="5">{!! old('content.section4.content.0.content') !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section4][content][1][name]"
                                       value="{!! old('content.section4.content.1.name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[section4][content][1][content]" class="form-control"
                                          rows="5">{!! old('content.section4.content.1.content') !!}</textarea>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section4][content][2][name]"
                                       value="{!! old('content.section4.content.2.name') !!}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[section4][content][2][content]" class="form-control"
                                          rows="5">{!! old('content.section4.content.2.content') !!}</textarea>
                            </div>
                        </div>

                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section4--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Creating Authentic Experiences For Discerning Travelers</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section5][name]"
                                       value="{!! old('content.section5.name','Creating Authentic Experiences For Discerning Travelers.') !!}">
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="file-loading">
                                    <input class="inpImg" name="content[section5][image]" type="file">
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-8">
                            <h4>Danh sách nội dung</h4>
                            <div class="repeater" id="repeater">

                                <table class="table table-bordered table-hover">
                                    <thead></thead>
                                    <tbody>
                                    @if(old('content.section5.content'))
                                        <?php $count=1; ?>
                                        @foreach(old('content.section5.content') as $key => $val)
                                            <tr>
                                                <td><span class="index">{!!$count++ !!}</span></td>
                                                <td style="position:relative">
                                                    <div class="row form-group">
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Tiêu đề</label></div>
                                                        <div class="col-lg-10"><input type="text" class="form-control" name="content[section5][content][{!! $key !!}][name]" value="{!! $val->name !!}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Nội dung</label></div>
                                                        <div class="col-lg-10"><textarea name="content[section5][content][{!! $key !!}][content]" class="form-control content" rows="6">{!! $val->content !!}</textarea></div>
                                                    </div>
                                                    <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                       href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                        <i class="fa fa-minus"></i>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <div class="text-right">
                                    <button class="btn btn-primary" onclick="repeater(event,this)">Thêm</button>
                                </div>

                            </div>

                            <script>
                                function repeater(event, el) {
                                    event.preventDefault();
                                    var target = $(el).closest('.repeater').find('table tbody');
                                    $.get("{!! url('').'/resources/views/backend/about/repeater.php' !!}", function (data) {
                                        target.append(data)
                                    })
                                }
                            </script>
                        </div>

                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section5--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Meet Our Team</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề chính </label>
                                <input type="text" class="form-control" name="content[section6][name]"
                                       value="{!! old('content.section6.name','Meet Our Team') !!}">
                            </div>

                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Mô tả </label>
                                <textarea name="content[section6][content]" class="form-control"
                                          rows="5">{!! old('content.section6.content') !!}</textarea>
                            </div>
                        </div>


                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section6--}}

        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>
        </div>


    </form>

@endsection