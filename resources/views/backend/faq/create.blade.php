@extends('backend.index')
@section('controller','FAQs')
@section('controller_route',route('faq.index'))
@section('action','Thêm')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('faq.store') !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="type" value="faq">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>


            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name') !!}">
                            </div>


                        </div>
                        <div class="col-lg-12">
                            <hr>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead></thead>
                                        <tbody>
                                        @if(!empty(old('content')))
                                            <?php $count = 1; ?>
                                            @foreach(old('content') as $key => $val)
                                                <tr>
                                                    <td><span class="index">{!! $count++ !!}</span></td>
                                                    <td style="position:relative">
                                                        <div class="row form-group">
                                                            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Câu hỏi</label></div>
                                                            <div class="col-lg-10"><input type="text" class="form-control" name="content[{!! $key !!}][name]" value="{!! $val['name'] !!}"></div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-2"><label for="" style='padding-top: 8px;'>Trả lời</label></div>
                                                            <div class="col-lg-10"><textarea name="content[{!! $key !!}][content]" class="form-control content" rows="6">{!! $val['content'] !!}</textarea></div>
                                                        </div>
                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeater(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAdd(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index').closest('table').find('.index');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{route('repeat.faq.content')}}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }

                                    function repeater(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index').closest('table').find('.index');
                                        var index = indexs.length;
                                        $.get("{{route('repeat.faq.content')}}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>

    </form>

@endsection
