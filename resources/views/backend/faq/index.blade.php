@extends('backend.index')
@section('controller','FAQs')
@section('controller_route',route('faq.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
                'routeBulkDestroy' => route('faq.bulkDestroy'),
                'routeCreate' => route('faq.create'),
                'datatables' => $datatables,
                'object' => new \App\Http\Controllers\FaqController,
            ])

@endsection
