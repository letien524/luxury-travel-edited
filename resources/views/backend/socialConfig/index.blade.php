@extends('backend.index')
@section('controller','Mạng xã hội')
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')
    @include('backend.block.error')

    <form action="{!! route('socialConfig.update',$social) !!}" method='POST' enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <?php $content = json_decode(@$social->content); ?>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Facebook</label>
                                <input type="text" class="form-control" name="content[social][facebook]" value="{!! old('content.social.facebook',@$content->social->facebook) !!}">
                            </div>
                            <div class="form-group">
                                <label>Twitter</label>
                                <input type="text" class="form-control" name="content[social][twitter]" value="{!! old('content.social.twitter',@$content->social->twitter) !!}">
                            </div>
                            <div class="form-group">
                                <label>Tripadvisor</label>
                                <input type="text" class="form-control" name="content[social][tripadvisor]" value="{!! old('content.social.tripadvisor',@$content->social->tripadvisor) !!}">
                            </div>
                            <div class="form-group">
                                <label>Google plus</label>
                                <input type="text" class="form-control" name="content[social][google_plus]" value="{!! old('content.social.google_plus',@$content->social->google_plus) !!}">
                            </div>
                            <div class="form-group">
                                <label>Whatsapp</label>
                                <input type="text" class="form-control" name="content[social][whatsapp]" value="{!! old('content.social.whatsapp',@$content->social->whatsapp) !!}">
                            </div>
                            <div class="form-group">
                                <label>Skype</label>
                                <input type="text" class="form-control" name="content[social][skype]" value="{!! old('content.social.skype',@$content->social->skype) !!}">
                            </div>
                            <div class="form-group">
                                <label>Youtube</label>
                                <input type="text" class="form-control" name="content[social][youtube]" value="{!! old('content.social.youtube',@$content->social->youtube) !!}">
                            </div>

                        </div>
                    </div> {{--./row--}}

                </div>

                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>

        <button type="submit" class="btn btn-primary">Lưu</button>
    </form>

@endsection