@extends('backend.index')
@section('controller','Giới thiệu')
@section('controller_route',route('about.index'))
@section('action','Cập nhật')
@section('live',route('about'))

@section('content')

    @include('backend.block.error')

    <form action="{!! route('about.update',$about) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="div">

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Thông tin chung</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Ảnh đại diện</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',$about->image) ? old('image',$about->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$about->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Tiêu đề trang</label>
                                <input type="text" class="form-control" name="name"
                                       value="{!! old('name',$about->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title',$about->meta_title) !!}">
                            </div>
                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="5">{!! old('meta_description',$about->meta_description) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="index" value="1" {{ $about->index == 1  ? 'checked' : null }}>
                                        Robot index
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <?php $content = json_decode($about->content); ?>

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Who We Are</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.section1.image',@$content->section1->image) ? old('content.section1.image',@$content->section1->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[section1][image]" value="{{ old('content.section1.image',@$content->section1->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>

                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section1][name]"
                                       value="{!! old('content.section1.name',@$content->section1->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content[section1][content]" class="form-control"
                                          rows="5">{!! old('content.section1.content',@$content->section1->content) !!}</textarea>
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Our Business Is Sustainable And Responsible</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section2][name]"
                                       value="{!! old('content.section2.name',@$content->section2->name) !!}">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label>Nội dung</label>
                        </div>
                        <?php $section2Content = (array)@$content->section2->content; ?>
                        <div class="col-lg-6">

                            <div class="form-group">

                                <textarea name="content[section2][content][]" class="form-control"
                                          rows="5">{!! old('content.section2.content.0',@$section2Content[0]) !!}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <textarea name="content[section2][content][]" class="form-control"
                                          rows="5">{!! old('content.section2.content.1',@$section2Content[1]) !!}</textarea>
                            </div>

                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section2--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Banner</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.section3.image',@$content->section3->image) ? old('content.section3.image',@$content->section3->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[section3][image]" value="{{ old('content.section3.image',@$content->section3->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>

                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section3--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Our Selling Point</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Tiêu đề chính </label>
                                <input type="text" class="form-control" name="content[section4][name]"
                                       value="{!! old('content.section4.name',@$content->section4->name) !!}">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <hr>
                        </div>
                        @foreach(@$content->section4->content as $key => $val)
                            <?php $oldName = 'content.section4.content.' . $key . '.name'; ?>
                            <?php $oldContent = 'content.section4.content.' . $key . '.content'; ?>
                            <div class="col-lg-4">
                                <div class="form-group">

                                    <label>Tiêu đề</label>
                                    <input type="text" class="form-control" name="content[section4][content][{!! $key !!}][name]"
                                           value="{!! old($oldName,$val->name) !!}">
                                </div>

                                <div class="form-group">
                                    <label>Nội dung</label>
                                    <textarea name="content[section4][content][{!! $key !!}][content]" class="form-control"
                                              rows="5">{!! old($oldContent, $val->content) !!}</textarea>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div><!-- /.box-body -->
            </div> {{--./section4--}}

            <div class="box box-solid box-default">
                <div class="box-header">
                    <h3 class="box-title">Creating Authentic Experiences For Discerning Travelers</h3>
                    <div class="box-tools">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="content[section5][name]"
                                       value="{!! old('content.section5.name',@$content->section5->name) !!}">
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.section5.image',@$content->section5->image) ? old('content.section5.image',@$content->section5->image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[section5][image]" value="{{ old('content.section5.image',@$content->section5->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>


                        <div class="col-lg-8">

                            <div class="form-group">
                                <h4>Chọn mẫu faq có sẵn</h4>
                                <p><a href="{!! route('faq.create') !!}" target="_blank"><i class="fa fa-plus"></i> <i>Tạo mẫu mới</i></a></p>

                                <select name="faq_id" class="form-control multislt" style="width: 100%;">
                                    <option value="">Chọn</option>
                                    @foreach($faqs as $faq)
                                        <option value="{!! $faq->id !!}" {!! old('faq_id',@$about->faq_id) == $faq->id ? 'selected' : null !!}>{!! $faq->name !!}</option>
                                    @endforeach
                                </select>


                            </div>

                        </div>

                    </div>

                </div>
            </div><!-- /.box-body -->
        </div> {{--./section5--}}

        <div class="box box-solid box-default">
            <div class="box-header">
                <h3 class="box-title">Meet Our Team</h3>
                <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Tiêu đề chính </label>
                            <input type="text" class="form-control" name="content[section6][name]"
                                   value="{!! old('content.section6.name',@$content->section6->name) !!}">
                        </div>

                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label>Mô tả </label>
                            <textarea name="content[section6][content]" class="form-control"
                                      rows="5">{!! old('content.section6.content',@$content->section6->content) !!}</textarea>
                        </div>
                    </div>


                </div>
            </div><!-- /.box-body -->
        </div>
        {{--./section6--}}

        <div class="box box-solid box-default">
            <div class="box-header">
                <h3 class="box-title">CTA Button</h3>
                <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Tiêu đề</label>
                            <input type="text" class="form-control" name="content[section7][name]"
                                   value="{!! old('content.section7.name',@$content->section7->name) !!}">
                        </div>

                        <div class="form-group">
                            <label>Đường dẫn</label>
                            <input type="text" class="form-control" name="content[section7][link]"
                                   value="{!! old('content.section7.link',@$content->section7->link) !!}">
                        </div>

                    </div>

                </div>
            </div><!-- /.box-body -->
        </div>
        {{--./section7--}}

        <button type="submit" class="btn btn-primary">OK</button>

        </div>
        <!-- /.tab-content -->


    </form>

@endsection