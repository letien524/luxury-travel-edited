@extends('backend.index')
@section('controller','Danh mục tin tức')
@section('controller_route',route('blogCategory.index'))
@section('action','Danh sách')
@section('live',route('blog'))
@section('content')

    @include('backend.block.error')


    @include('backend.components.table-form', [
              'routeBulkDestroy' => route('blogCategory.bulkDestroy'),
              'routeCreate' => route('blogCategory.create'),
              'datatables' => $datatables,
              'object' => new \App\Http\Controllers\blogCategoryController,
          ])
@endsection