<?php
if (isUpdateBlade(@$configs['action'])) {
    $actionLable = \App\Enum\RouteActionEnum::ACTIONS['update'];
    $detailRoute = route('blogCategory.detail', @$blogCategory->slug);
    $actionFormRoute = route('blogCategory.update', @$blogCategory);
} else {

    $actionLable = \App\Enum\RouteActionEnum::ACTIONS['create'];
    $detailRoute = route('blog');
    $actionFormRoute = route('blogCategory.store');
}
?>

@extends('backend.index')
@section('controller', 'Danh mục tin tức')
@section('controller_route',route('blogCategory.index'))
@section('action', $actionLable)
@section('live', $detailRoute))

@section('content')

    @include('backend.block.error')
    <form action="{!! $actionFormRoute !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif
        <input type="hidden" name="type" value="blog-category">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Ảnh đại diện</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image',@$blogCategory->image) ? old('image',@$blogCategory->image) : null) }}"
                                             data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete"
                                           onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image"
                                               value="{{ old('image',@$blogCategory->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control name" data-target="#slug" name="name" id="name"
                                       value="{!! old('name',@$blogCategory->name) !!}">
                            </div>

                            <div class="form-group">
                                <label>Đường dẫn</label>
                                <input type="text" class="form-control" name="slug" id="slug"
                                       value="{!! old('slug',@$blogCategory->slug) !!}">
                            </div>

                        </div>


                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Canonical URL</label>
                                <input type="text" class="form-control" name="canonical"
                                       value="{!! old('canonical',@$blogCategory->canonical) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="meta_title"
                                       value="{!! old('meta_title',@$blogCategory->meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="meta_description" class="form-control"
                                          rows="4">{!! old('meta_description',@$blogCategory->meta_description) !!}</textarea>
                            </div>
                        </div>
                    </div> {{--./row--}}

                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->


        <button type="submit" class="btn btn-primary">OK</button>
    </form>

@endsection