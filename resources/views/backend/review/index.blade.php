@extends('backend.index')
@section('controller','Review')
@section('controller_route',route('review.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
             'routeBulkDestroy' => route('review.bulkDestroy'),
             'routeCreate' => route('review.create'),
             'datatables' => $datatables,
             'object' => new \App\Http\Controllers\ReviewController,
         ])
@endsection