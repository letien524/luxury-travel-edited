@extends('backend.index')
@section('controller','Review')
@section('controller_route',route('review.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('review.update',$data) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="true">Thông tin liên quan </a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('image', $data->image)) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="image" value="{{ old('image',$data->image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name',$data['name']) !!}">
                            </div>


                            <div class="form-group">
                                <label>Đánh giá sao</label>
                                <input type="number" class="form-control" min="0" max="5" name="rate" value="{!! old('rate',$data['rate']) !!}">
                            </div>

                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Danh mục</label>
                                <?php $oldCategories = $data->categories->count() ? $data->categories->pluck('id')->toArray() : [] ;?>


                                <select name="categories[]" class="form-control multislt" multiple>
                                    @foreach($reviewCategories as $category)
                                        <option value="{{$category->id}}" {{ in_array($category->id ,$oldCategories) ? 'selected' : null }}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="display" value="1" {{old('display',$data['display']) == 1 ? 'checked' : null}}> Hiển thị
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-12">


                            <div class="form-group">
                                <label>Họ tên</label>
                                <input type="text" class="form-control" name="reviewer" value="{!! old('reviewer',$data['reviewer']) !!}">
                            </div>

                            <div class="form-group">
                                <label>Quốc gia</label>
                                <input type="text" class="form-control" name="country" value="{!! old('country',$data['country']) !!}">
                            </div>

                            <div class="form-group">
                                <label>Thời gian</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="submited_at" class="form-control pull-right" id="datepicker" value="{{old('submited_at',\Carbon\Carbon::parse($data['submited_at'])->format('m/d/Y'))}}">
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" class="form-control content" rows="5">{{old('content',$data['content'])}}</textarea>
                            </div>
                        </div>

                    </div> {{--./row--}}
                </div>

                <div class="tab-pane" id="activity1">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Chọn Tour liên quan</label>
                                <select name="tours[]" class="form-control multislt" style="width: 100%">
                                    <option value="">Chọn</option>
                                    @if($tours->count())
                                        <?php $oldTours = $data->memberTours->count() ? $data->memberTours->pluck('id')->toArray() : []; ?>
                                        @foreach($tours as $item)
                                            <option value="{{ $item->id }}" {{ in_array($item->id, old('tours', $oldTours))  ? 'selected' : null }}>{{ $item->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div> {{--./col--}}

                    </div>
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection