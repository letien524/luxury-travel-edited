@extends('backend.index')
@section('controller','Liên hệ')
@section('controller_route',route('contact.index'))
@section('action','Danh sách')
@section('live',route('contact'))
@section('content')

    @include('backend.block.error')


    @include('backend.components.table-form', [
               'routeBulkDestroy' => route('contact.bulkDestroy'),
               'routeCreate' => route('contact.create'),
               'datatables' => $datatables,
               'object' => new \App\Http\Controllers\ContactController,
           ])

@endsection
