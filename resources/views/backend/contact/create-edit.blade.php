@extends('backend.index')
@section('controller','Liên hệ')
@section('controller_route',route('contact.index'))
@section('action','Cập nhật')
@section('live',route('contact'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('contact.update', @$contact) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <?php $content = json_decode($contact->content); ?>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="">
                                <table class="table table-bordered">
                                    <tr>
                                        <td style="width: 100px;">Thời gian:</td>
                                        <td>{!! date_format(date_create($contact->created_at),"d.m.Y - H:m:s") !!}</td>
                                    </tr>
                                    <tr>
                                        <td>Họ tên:</td>
                                        <td>{{$contact->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td>{{$contact->email}}</td>
                                    </tr>

                                    <tr>
                                        <td>Email:</td>
                                        <td>{{$contact->type}}</td>
                                    </tr>

                                    <tr>
                                        <td>Chủ đề:</td>
                                        <td>{{@$content->subject}}</td>
                                    </tr>
                                    <tr>
                                        <td>Message:</td>
                                        <td>{{@$content->message}}</td>
                                    </tr>
                                    <tr>
                                        <td>Comment:</td>
                                        <td>{{@$content->content}}</td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select name="status" class="form-control">
                                    <option value="">Chưa xem</option>
                                    <option value="1" {!! $contact->status == 1 ? 'selected' : null !!}>Đã xem</option>
                                    <option value="2" {!! $contact->status == 2 ? 'selected' : null !!}>Đã phản hồi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.tab-pane -->
        </div>

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
