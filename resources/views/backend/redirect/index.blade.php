@extends('backend.index')
@section('controller','Redirect')
@section('controller_route',route('redirect.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
                'routeBulkDestroy' => route('redirect.bulkDestroy'),
                'routeCreate' => route('redirect.create'),
                'datatables' => $datatables,
                'object' => new \App\Http\Controllers\RedirectController,
            ])
@endsection
