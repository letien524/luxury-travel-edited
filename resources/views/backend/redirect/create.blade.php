@extends('backend.index')
@section('controller','Redirect')
@section('controller_route',route('redirect.index'))
@section('action','Thêm')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <p><i>**Rules:</i></p>
    <p><i><code>/old-blog</code> => <code>/new-blog</code></i>:</p>
    <p>domain.com/old-blog => domain.com/new-blog</p>
    <hr>

    <p><i><code>/old-blog/{parameter}</code> => <code>/new-blog/{parameter}</code></i></p>
    <p>domain.com/old-blog/alias => domain.com/new-blog/alias</p>
    <p>domain.com/old-blog/sample.html => domain.com/new-blog/sample.html</p>
    <hr>


    <form action="{!! route('redirect.store') !!}" method="POST" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Old Url</label>
                                <input type="text" class="form-control" name="from" value="{!! old('from') !!}">
                            </div>

                            <div class="form-group">
                                <label>New Url</label>
                                <input type="text" class="form-control" name="to" value="{!! old('to') !!}">
                            </div>


                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="status" value="1" checked>
                                        Active
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div> {{--./row--}}
                </div>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">Lưu</button>

    </form>

@endsection