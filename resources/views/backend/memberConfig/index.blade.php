@extends('backend.index')
@section('controller','Cấu hình thành viên')
@section('controller_route',route('memberConfig.index'))
@section('action','Cấu hình')
@section('live',route('blog'))
@section('content')

    @include('backend.block.error')
    <form action="{!! route('memberConfig.update',$memberConfig) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a>
                </li>
            </ul>
            <?php $content = json_decode($memberConfig->content, true); ?>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">

                            <div class="form-group">
                                <label>Button CTA label</label>
                                <input type="text" class="form-control" name="content[button_cta_content]" value="{!! old('content.button_cta_content',@$content['button_cta_content']) !!}">
                            </div>
                        </div> {{--./col --}}

                    </div> {{--./row--}}
                </div>
            </div>

            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
