@extends('backend.index')
@section('controller','Email')
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')
    @include('backend.block.error')

    <form action="{!! route('emailConfig.update',$email) !!}" method='POST' enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">SMTP</a></li>
                <li class=""><a href="#activity2" data-toggle="tab" aria-expanded="true">Tour</a></li>
                <li class=""><a href="#activity1" data-toggle="tab" aria-expanded="true">Contact form</a></li>
                <li class=""><a href="#activity3" data-toggle="tab" aria-expanded="true">Subscribe form</a></li>
                <li class=""><a href="#activity4" data-toggle="tab" aria-expanded="true">Comment</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <?php $content = json_decode($email['content'],true);?>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Mail host</label>
                                <input type="text" placeholder="smtp.gmail.com" class="form-control" name="content[smtp][host]" value="{{ old('content.smtp.host',@$content['smtp']['host']) }}">
                            </div>

                            <div class="form-group">
                                <label>Mail port</label>
                                <input type="text" placeholder="587" class="form-control" name="content[smtp][port]" value="{{ old('content.smtp.port',@$content['smtp']['port']) }}">
                            </div>

                            <div class="form-group">
                                <label>Mail encryption</label>
                                <input type="text" placeholder="tls" class="form-control" name="content[smtp][encryption]" value="{{ old('content.smtp.encryption',@$content['smtp']['encryption']) }}">
                            </div>

                            <div class="form-group">
                                <label>Username</label>
                                <input type="text   " placeholder="email@gmail.com" class="form-control" name="content[smtp][username]" value="{{ old('content.smtp.username',@$content['smtp']['username']) }}">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" placeholder="********" class="form-control" name="content[smtp][password]" value="{{ old('content.smtp.password',@$content['smtp']['password']) }}">
                            </div>

                            <div class="form-group">
                                <label>Mail name</label>
                                <input type="text" placeholder="Luxury Travel" class="form-control" name="content[smtp][name]" value="{{ old('content.smtp.name',@$content['smtp']['name']) }}">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane " id="activity2">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email Tour to:</label>
                                <input type="text" placeholder="email@gmail.com" class="form-control" name="content[tour][mail_to]" value="{{ old('content.tour.mail_to',@$content['tour']['mail_to']) }}">
                            </div>

                            <div class="form-group">
                                <label>Bcc:</label>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <th style="width: 30px;">No.</th>
                                        <th>Email</th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $tourBcc = @$content['tour']['bcc']; ?>


                                        @if(!empty(old('content.tour.bcc',$tourBcc)))
                                            @foreach(old('content.tour.bcc',$tourBcc) as $key => $val)
                                                <tr class="">
                                                    <td><span class="index">{!! $loop->iteration !!}</span></td>
                                                    <td style="position:relative">
                                                        <input type="text" class="form-control" name="content[tour][bcc][{!! $key !!}]" value="{!! @$val !!}">

                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                        <a style="
                                                            position: absolute;
                                                            top: calc(50% - 20px);
                                                            right: -8px;
                                                            border:1px solid #ccc;
                                                            border-radius:15px;
                                                            width: 15px;
                                                            height: 15px;
                                                            font-size: 10px;
                                                            text-align: center;
                                                            background-color: #fff;"
                                                           href="javascript:void(0);" onclick="rowAdd(event,this)" class="text-primary" title="Thêm">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeater(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAdd(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index').closest('table').find('.index');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{ route('repeat.email.tourBcc') }}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }
                                    function repeater(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index').closest('table').find('.index');
                                        var index = indexs.length;
                                        $.get("{{ route('repeat.email.tourBcc') }}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane " id="activity1">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email Contact to:</label>
                                <input type="text" placeholder="email@gmail.com" class="form-control" name="content[contact][mail_to]" value="{{ old('content.contact.mail_to',@$content['contact']['mail_to']) }}">
                            </div>

                            <div class="form-group">
                                <label>Bcc:</label>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <th style="width: 30px;">No.</th>
                                        <th>Email</th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $contactBcc = @$content['contact']['bcc']; ?>


                                        @if(!empty(old('content.contact.bcc',$contactBcc)))
                                            @foreach(old('content.contact.bcc',$contactBcc) as $key => $val)
                                                <tr class="">
                                                    <td><span class="index-contact">{!! $loop->iteration !!}</span></td>
                                                    <td style="position:relative">
                                                        <input type="text" class="form-control" name="content[contact][bcc][{!! $key !!}]" value="{!! @$val !!}">

                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                        <a style="
                                                            position: absolute;
                                                            top: calc(50% - 20px);
                                                            right: -8px;
                                                            border:1px solid #ccc;
                                                            border-radius:15px;
                                                            width: 15px;
                                                            height: 15px;
                                                            font-size: 10px;
                                                            text-align: center;
                                                            background-color: #fff;"
                                                           href="javascript:void(0);" onclick="rowAddContact(event,this)" class="text-primary" title="Thêm">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeaterContact(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAddContact(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index-contact').closest('table').find('.index-contact');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{ route('repeat.email.contactBcc') }}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }
                                    function repeaterContact(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index-contact').closest('table').find('.index-contact');
                                        var index = indexs.length;
                                        $.get("{{ route('repeat.email.contactBcc') }}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane " id="activity3">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email Subscribe to:</label>
                                <input type="text" placeholder="email@gmail.com" class="form-control" name="content[subscribe][mail_to]" value="{{ old('content.subscribe.mail_to',@$content['subscribe']['mail_to']) }}">
                            </div>

                            <div class="form-group">
                                <label>Bcc:</label>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <th style="width: 30px;">No.</th>
                                        <th>Email</th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $subscribeBcc = @$content['subscribe']['bcc']; ?>


                                        @if(!empty(old('content.subscribe.bcc',$subscribeBcc)))
                                            @foreach(old('content.subscribe.bcc',$subscribeBcc) as $key => $val)
                                                <tr class="">
                                                    <td><span class="index-subscribe">{!! $loop->iteration !!}</span></td>
                                                    <td style="position:relative">
                                                        <input type="text" class="form-control" name="content[subscribe][bcc][{!! $key !!}]" value="{!! @$val !!}">

                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                        <a style="
                                                            position: absolute;
                                                            top: calc(50% - 20px);
                                                            right: -8px;
                                                            border:1px solid #ccc;
                                                            border-radius:15px;
                                                            width: 15px;
                                                            height: 15px;
                                                            font-size: 10px;
                                                            text-align: center;
                                                            background-color: #fff;"
                                                           href="javascript:void(0);" onclick="rowAddSubscribe(event,this)" class="text-primary" title="Thêm">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeaterSubscribe(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAddSubscribe(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index-subscribe').closest('table').find('.index-subscribe');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{ route('repeat.email.subscribeBcc') }}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }
                                    function repeaterSubscribe(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index-subscribe').closest('table').find('.index-subscribe');
                                        var index = indexs.length;
                                        $.get("{{ route('repeat.email.subscribeBcc') }}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane " id="activity4">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email Comment to:</label>
                                <input type="text" placeholder="email@gmail.com" class="form-control" name="content[comment][mail_to]" value="{{ old('content.comment.mail_to',@$content['comment']['mail_to']) }}">
                            </div>

                            <div class="form-group">
                                <label>Bcc:</label>
                                <div class="repeater" id="repeater">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <th style="width: 30px;">No.</th>
                                        <th>Email</th>
                                        </thead>
                                        <tbody id="sortable">
                                        <?php $subscribeBcc = @$content['comment']['bcc']; ?>


                                        @if(!empty(old('content.comment.bcc',$subscribeBcc)))
                                            @foreach(old('content.comment.bcc',$subscribeBcc) as $key => $val)
                                                <tr class="">
                                                    <td><span class="index-comment">{!! $loop->iteration !!}</span></td>
                                                    <td style="position:relative">
                                                        <input type="text" class="form-control" name="content[comment][bcc][{!! $key !!}]" value="{!! @$val !!}">

                                                        <a style="
                position: absolute;
                top: 50%;
                right: -8px;
                border:1px solid #ccc;
                border-radius:15px;
                width: 15px;
                height: 15px;
                font-size: 10px;
                text-align: center;
                background-color: #fff;"
                                                           href="javascript:void(0);" onclick="$(this).closest('tr').remove()" class="text-danger" title="Xóa">
                                                            <i class="fa fa-minus"></i>
                                                        </a>
                                                        <a style="
                                                            position: absolute;
                                                            top: calc(50% - 20px);
                                                            right: -8px;
                                                            border:1px solid #ccc;
                                                            border-radius:15px;
                                                            width: 15px;
                                                            height: 15px;
                                                            font-size: 10px;
                                                            text-align: center;
                                                            background-color: #fff;"
                                                           href="javascript:void(0);" onclick="rowAddComment(event,this)" class="text-primary" title="Thêm">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>

                                    <div class="text-right">
                                        <button class="btn btn-primary" onclick="repeaterComment(event,this)">Thêm
                                        </button>
                                    </div>

                                </div>

                                <script>
                                    function rowAddComment(event, el){
                                        event.preventDefault();
                                        var indexs = $('.index-comment').closest('table').find('.index-comment');
                                        var index = indexs.length;


                                        var target = $(el).closest('tr');
                                        $.get("{{ route('repeat.email.commentBcc') }}",{index:index+1}, function (data) {
                                            target.after(data);
                                        });
                                    }
                                    function repeaterComment(event, el) {
                                        event.preventDefault();
                                        var target = $(el).closest('.repeater').find('table tbody');
                                        var indexs = $('.index-comment').closest('table').find('.index-comment');
                                        var index = indexs.length;
                                        $.get("{{ route('repeat.email.commentBcc') }}",{index:index+1}, function (data) {
                                            target.append(data)
                                        })
                                    }
                                </script>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
        </div>

        <button type="submit" class="btn btn-primary">Lưu</button>
    </form>

@endsection