@extends('backend.index')
@section('controller','Danh mục review')
@section('controller_route',route('reviewCategory.index'))
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('reviewCategory.update',$data) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        {!! method_field('put') !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input type="text" class="form-control" name="name" value="{!! old('name',$data->name) !!}">
                            </div>

                        </div>

                    </div> {{--./row--}}
                </div>

            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->

        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
