@extends('backend.index')
@section('controller','Danh mục review')
@section('controller_route',route('reviewCategory.index'))
@section('action','Danh sách')
@section('live',url('/'))
@section('content')

    @include('backend.block.error')

    @include('backend.components.table-form', [
                 'routeBulkDestroy' => route('reviewCategory.bulkDestroy'),
                 'routeCreate' => route('reviewCategory.create'),
                 'datatables' => $datatables,
                 'object' => new \App\Http\Controllers\ReviewCategoryController,
             ])

@endsection
