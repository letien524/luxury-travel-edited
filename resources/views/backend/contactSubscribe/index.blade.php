@extends('backend.index')
@section('controller','Subscribe')
@section('controller_route',route('contactSubscribe.index'))
@section('action','Danh sách')
@section('live',route('contact'))
@section('content')

    @include('backend.block.error')


    @include('backend.components.table-form', [
               'routeBulkDestroy' => route('contactSubscribe.bulkDestroy'),
               'routeCreate' => route('contactSubscribe.create'),
               'datatables' => $datatables,
               'object' => new \App\Http\Controllers\ContactSubscribeController,
           ])

@endsection
