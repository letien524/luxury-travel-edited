@extends('backend.index')
@section('controller','Subscribe')
@section('controller_route',route('contactSubscribe.index'))
@section('action','Cập nhật')
@section('live',route('contact'))
@section('content')

    @include('backend.block.error')

    <form action="{!! route('contactSubscribe.update', @$contactSubscribe) !!}" method='POST' enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="type" value="customer-subscribe">
        @if(isUpdateBlade(@$configs['action']))
            {!! method_field('put') !!}
        @endif

        <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="activity">
                        <?php $content = json_decode(@$contactSubscribe->content); ?>
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="width: 100px;">Thời gian:</td>
                                            <td>{!! date_format(date_create(@$contactSubscribe->created_at),"d.m.Y - H:m:s") !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Họ tên:</td>
                                            <td>{{@$contactSubscribe->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Email:</td>
                                            <td>{{@$contactSubscribe->email}}</td>
                                        </tr>

                                        <tr>
                                            <td>Email:</td>
                                            <td>{{@$contactSubscribe->type}}</td>
                                        </tr>

                                        <tr>
                                            <td>Chủ đề:</td>
                                            <td>{{@$content->subject}}</td>
                                        </tr>
                                        <tr>
                                            <td>Message:</td>
                                            <td>{{@$content->message}}</td>
                                        </tr>
                                        <tr>
                                            <td>Comment:</td>
                                            <td>{{@$content->content}}</td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Trạng thái</label>
                                    <select name="status" class="form-control">
                                        <option value="">Chưa xem</option>
                                        <option value="1" {!! @$contactSubscribe->status == 1 ? 'selected' : null !!}>Đã xem</option>
                                        <option value="2" {!! @$contactSubscribe->status == 2 ? 'selected' : null !!}>Đã phản hồi</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.tab-pane -->
            </div>
        <button type="submit" class="btn btn-primary">OK</button>

    </form>

@endsection
