@extends('backend.index')
@section('controller','Cấu hình chung')
@section('action','Cập nhật')
@section('live',url('/'))
@section('content')
    @include('backend.block.error')

    <form action="{!! route('generalConfig.update',$general) !!}" method='POST' enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('put') }}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Thông tin chung</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">
                    <?php $content = json_decode($general->content); ?>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Logo</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.site_logo',@$content->site_logo) ? old('content.site_logo',@$content->site_logo) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[site_logo]" value="{{ old('content.site_logo',@$content->site_logo)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Favicon</label><br>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.site_favicon',@$content->site_favicon) ? old('content.site_favicon',@$content->site_favicon) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[site_favicon]" value="{{ old('content.site_favicon',@$content->site_favicon)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Hình thức thanh toán</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.site_payment',@$content->site_payment) ? old('content.site_payment',@$content->site_payment) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[site_payment]" value="{{ old('content.site_payment',@$content->site_payment)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>
                    </div> {{--./row--}}
                    <hr>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Tiêu đề Website</label>
                                <input type="text" class="form-control" name="content[site_name]" value="{!! old('content.site_name',@$content->site_name) !!}">
                            </div>
                            <div class="form-group">
                                <label>Hotline</label>
                                <input type="text" class="form-control" name="content[site_hotline]" value="{!! old('content.site_hotline',@$content->site_hotline) !!}">
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="content[site_email]" value="{!! old('content.site_email',@$content->site_email) !!}">
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="content[site_search_engine_index]" value="1" {!! old('content.site_search_engine_index',@$content->site_search_engine_index) == 1 ? 'checked' : null!!}>
                                        Search engine index
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Meta title</label>
                                <input type="text" class="form-control" name="content[site_meta_title]" value="{!! old('content.site_meta_title',@$content->site_meta_title) !!}">
                            </div>

                            <div class="form-group">
                                <label>Meta description</label>
                                <textarea name="content[site_meta_description]" class="form-control" rows="5">{!! old('content.site_meta_description',@$content->site_meta_description) !!}</textarea>
                            </div>

                        </div>

                        <div class="col-lg-12">
                            <hr>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Script đầu trang</label>
                                        <textarea name="content[site_script_header]" class="form-control" rows="5">{!! old('content.site_script_header',@$content->site_script_header) !!}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Script cuối trang trang</label>
                                        <textarea name="content[site_script_footer]" class="form-control" rows="5">{!! old('content.site_script_header',@$content->site_script_footer) !!}</textarea>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-12">
                            <hr>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Inquiry Background</label>
                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.site_enquiry_image',@$content->site_enquiry_image) ? old('content.site_enquiry_image',@$content->site_enquiry_image) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[site_enquiry_image]" value="{{ old('content.site_enquiry_image',@$content->site_enquiry_image)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Inquiry Banner Modal</label>

                                <div class="image">
                                    <div class="image__thumbnail">
                                        <img src="{{ image_url(old('content.site_enquiry_banner',@$content->site_enquiry_banner) ? old('content.site_enquiry_banner',@$content->site_enquiry_banner) : null) }}"  data-init="{{ image_url('') }}">
                                        <a href="javascript:void(0)" class="image__delete" onclick="urlFileDelete(this)"><i class="fa fa-times"></i></a>
                                        <input type="hidden" name="content[site_enquiry_banner]" value="{{ old('content.site_enquiry_banner',@$content->site_enquiry_banner)  }}">
                                        <div class="image__button" onclick="fileSelect(this)">
                                            <i class="fa fa-upload"></i>
                                            Upload
                                        </div>
                                    </div>
                                </div> {{--./image--}}
                            </div>

                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Inquiry Text</label>
                                <textarea name="content[site_enquiry_text]" class="form-control" rows="5">{!! old('content.site_enquiry_text',@$content->site_enquiry_text) !!}</textarea>
                            </div>
                        </div>


                    </div>

                </div>

                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>

        <button type="submit" class="btn btn-primary">Lưu</button>
    </form>

@endsection