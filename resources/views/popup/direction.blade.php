<div class="modal fade" id="directionPopup-{{ $popup->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog vk-modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="vk-popup--collection-direction" id="collectionDirection">
                <a href="{{ $popup->link ?? 'javascript:void(0);' }}" target="_blank" class="vk-img">
                    <img src="{{ $popup->image }}" alt="">
                </a>

                <a class="__close" title="{{__('Close')}}" href="javascript:void(0)" onclick="$('#directionPopup-{{ $popup->id }}').modal('hide')"><i class="fa fa-times-circle"></i></a>
            </div>
        </div>

    </div>
</div>
<script>
    window.addEventListener('load', function() {
        (function($) {
            function directionPopupShow(time=1){
                return setTimeout(function(){ $('#directionPopup-{{ $popup->id }}').modal('show');},time*1000);
            }

            if(!checkCookie('directionPopupShow-{{ $popup->id }}')){
                directionPopupShow({{$delay}});
                setCookie('directionPopupShow-{{ $popup->id }}',true, {{ $ex_days > 0  ? $ex_days : 1 }});
            }


            $('#directionPopup-{{ $popup->id }}').on('hide.bs.modal', function (e) {
                // $('.modal-backdrop').remove();
            });

            /*document.addEventListener("mouseleave", function(event){
                if(event.clientY <= 0 || event.clientX <= 0 || (event.clientX >= window.innerWidth || event.clientY >= window.innerHeight)){
                    directionPopupShow(0);
                }
            })*/

        })(jQuery);
    });
</script>
