<div class="vk-popup--collection-bottom" id="collectionBottom">
    <div class="container">
        <div class="__content">
            <div class="__text">{!! $popup->content !!}</div>
            <div class="__form vk-form--newsregister __style-1">
                <form action="{!! route('mail-collection-bottom') !!}" method="post">
                    {{csrf_field()}}
                    <input type="email" class="form-control" name="email" placeholder="{!! __('Your email') !!}" autocomplete="off" required>
                    <button class="vk-btn" type="submit"><i class="_icon fa fa-paper-plane"></i></button>
                </form>
            </div>
            <a class="__close" title="{{__('Close')}}" href="javascript:void(0)" onclick="$(this).closest('#collectionBottom').removeClass('active')"><i class="fa fa-times-circle-o"></i></a>
        </div>
    </div>
</div>

@include('notify.mail_subscribe',['name'=> session('message_name'), 'content'=> session('message_content'),'popup' => session('popup')])

<script>

    function mailCollectBottom(e){
        e.preventDefault();
        var form  = $(e.target),
            action = form.attr('action'),
            dataString = form.serialize();

          $.ajax({
              type: "POST",
              url: action,
              data: dataString,
              success: function(data){
                  var popup = $('#subscribeNotify');
                  popup.find('._title').text(data.name);
                  popup.find('._text').text(data.message);
                  popup.modal('show');
              },
              error: function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR);
                      var popup = $('#subscribeNotify');
                      popup.find('._title').text(jqXHR.responseJSON.message);
                      popup.find('._text').text(jqXHR.responseJSON.errors.email);
                      popup.modal('show');
              },

          });

    }

    window.addEventListener('load', function() {

        (function($) {
            function collectionBottomPopup(time=0){
                return setTimeout(function(){ $('#collectionBottom').addClass('active');},time*1000);
            }

            collectionBottomPopup({{$delay}});


        })(jQuery);
    });
</script>
