<!-- Modal -->
<div class="modal fade" id="collection-{{ $popup->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog vk-modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="vk-popup--collection" id="collection">
                @if($popup->image)
                <div class="_left">
                    <div class="vk-img vk-img--cover h-100">
                        <img src="{{ image_url($popup->image) }}" alt="{{ $popup->name }}">
                    </div>
                </div>
                @endif
                <div class="_right">
                    <h2 class="_title">{{ $popup->name }}</h2>
                    <div class="_text">{!! $popup->content !!}</div>

                    <form action="{!! route('mail-collection') !!}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="{!! __('Enter Your Email') !!}" autocomplete="off" required>
                        </div>

                        <button type="submit" class="vk-btn vk-btn--default vk-btn--block" style="white-space: normal" type="submit">{{ $popup->button_name_display ?? __('GET OFFER NOW')}}</button>
                    </form>
                </div>



                <a class="__close" title="{{__('Close')}}" href="javascript:void(0)" onclick="$('#collection-{{ $popup->id }}').modal('hide')"><i class="fa fa-times-circle"></i></a>
            </div>

        </div>
    </div>
</div>

@include('notify.mail_subscribe',['name'=> session('message_name'), 'content'=> session('message_content'),'popup' => session('customerMailCollection')])


<script>
    window.addEventListener('load', function() {
        (function($) {
            function collectionShow(time=1){
                return setTimeout(function(){ $('#collection-{{ $popup->id }}').modal('show');},time*1000);
            }

            if(!checkCookie('collectionShow-{{ $popup->id }}')){
                collectionShow({{@$delay}});
                setCookie('collectionShow-{{ $popup->id }}',true, {{ $ex_days > 0  ? $ex_days : 1 }});
            }

            @if(session('popup_status') == 1)
                collectionShow({{@$delay}});
            @endif


            $('#collection-{{ $popup->id }}').on('hide.bs.modal', function (e) {
                // $('.modal-backdrop').remove();
            });

            /*document.addEventListener("mouseleave", function(event){
                if(event.clientY <= 0 || event.clientX <= 0 || (event.clientX >= window.innerWidth || event.clientY >= window.innerHeight)){
                    collectionShow(0);
                }
            })*/

        })(jQuery);
    });
</script>
