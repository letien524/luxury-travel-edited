<div class="vk-popup--collection-corner" id="collectionCorner-{{ $popup->id }}">
    <a href="{{ $popup->link ?? 'javascript:void(0);' }}" target="_blank" class="vk-img">
        <img src="{{ image_url($popup->image) }}" alt="">
    </a>

    <a class="__close" title="{{__('Close')}}" href="javascript:void(0)" onclick="$(this).closest('#collectionCorner-{{ $popup->id }}').removeClass('active')"><i class="fa fa-times-circle"></i></a>
</div>

<script>
    window.addEventListener('load', function() {
        (function($) {
            function collectionCornerPopup(time=0){
                return setTimeout(function(){ $('#collectionCorner-{{ $popup->id }}').addClass('active');},time*1000);
            }

            collectionCornerPopup({{$delay}});



           /* document.addEventListener("mouseleave", function(event){
                if(event.clientY <= 0 || event.clientX <= 0 || (event.clientX >= window.innerWidth || event.clientY >= window.innerHeight)){
                    collectionCornerPopup(0);
                }
            })*/


        })(jQuery);
    });
</script>
