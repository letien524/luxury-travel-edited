@extends('mail.mail')
@section('content')
    <div style="text-align: center;">
        <div style="text-align: justify;">
        <span style="color:#000000">
            <span style="font-size:13px">
                <strong>{{ __('Dear :name',['name' => $contact->name]) }},</strong><br><br>
                {{ __('Thank you for your interest in our company and our travel services to Southeast Asia') }}.<br><br>
                {{ __('This email is to confirm that we have received your information as below') }}:
            </span><br>
            <ol style="margin-bottom: 0;padding-left: 17px;">
                @if(!empty($contact->name))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Customer name') }}: {!! $contact->name !!}</span></span><br></li>
                @endif

                @if(!empty($contact->email))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Email') }}: {!! $contact->email !!}</span></span><br></li>
                @endif

                <?php $content = json_decode($contact->content, true); ?>

                @if(strlen($content['subject']))
                <li> <span style="font-size:13px"><span
                            style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Subject') }}: {!! $content['subject'] !!}</span></span><br></li>
                @endif

                @if(strlen($content['message']))
                <li> <span style="font-size:13px"><span
                            style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Message') }}: {!! $content['message'] !!}</span></span><br></li>
                @endif

                @if(!empty($contact->created_at))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('You contacted us at') }}: {!! date_format(date_create($contact->created_at),"Y.m.d - H:m:s") !!}</span></span><br></li>
                @endif

            </ol>

        </span>
        </div>

        <div style="text-align: justify;"><br>
            <span style="color:#000000"><span
                    style="font-size:13px"><span
                        style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">
                                    {{ __('Our team will contact and send you the first draft of itinerary within 24 hours') }}.
                                    {!! __('Please don’t reply to this email. If you have any questions or need immediate assistance, please don’t hesitate to send us an email to :email or contact us via telephone :phone1 or :phone2',['email'=>'<a href="mailto:sales@luxurytravelvietnam.com">sales@luxurytravelvietnam.com</a>','phone1'=>'<a href="tel:+84 4 3927 4120">+84 4 3927 4120</a>', 'phone2'=>'<a href="tel:+84 834 68 69 96">+84 834 68 69 96</a>']) !!}.</span></span></span><br>
            &nbsp;
        </div>

        <div style="text-align: left;"><span
                style="color:#000000"><span
                    style="font-size:13px"><span
                        style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{__('Thanks and best regards')}},</span></span><br>
<span style="font-size:13px"><span
        style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif"><strong>{{ __('Luxury Travel Team') }}</strong></span></span></span>
        </div>
        @if(@$admin == 1)
            @if($contact->type == 'customer-contact')
                <p><a href="{{ route('contact.edit',$contact) }}"><i>Detail</i></a></p>
            @elseif($contact->type == 'customer-subscribe')
                <p><a href="{{ route('contact.subscribe.edit',$contact) }}"><i>Detail</i></a></p>
            @else
                <p><a href="{{ route('contact.collection.edit',$contact) }}"><i>Detail</i></a></p>
            @endif
        @endif
    </div>
@endsection
