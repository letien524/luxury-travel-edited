<h3>Nội dung</h3>

<table class="table table-bordered">

    <tr>
        <td style="width: 100px;">Thời gian</td>
        <td>{!! date_format(date_create($comment->created_at),"Y.m.d - H:m:s")  !!}</td>
    </tr>
    <tr>
        <td>Bài viết</td>
        <td>
            <?php
            $image = json_decode(@$comment->blog->image,true);
            ?>
            <a class="text-default" target="_blank" href="{!! route('blog.detail',['slug'=>@$comment->blog->slug,@$comment->blog]) !!}" title="{!! @$comment->blog->name !!}">
                <span class="name">{!! @$comment->blog->name !!}</span>
            </a>
        </td>
    </tr>
    <tr>
        <td>Họ tên</td>
        <td>{!! $comment->name !!}</td>
    </tr>

    <tr>
        <td>Email</td>
        <td><a href="mailto:{!! $comment->email !!}" title="{!! $comment->email !!}">{!! $comment->email !!}</a></td>
    </tr>
    <tr>
        <td>Số điện thoại</td>
        <td><a href="tel:{!! $comment->phone !!}" title="{!! $comment->phone !!}">{!! $comment->phone !!}</a></td>
    </tr>
    <tr>
        <td>Website</td>
        <td><a href="{!! $comment->website !!}" target="_blank" title="{!! $comment->website !!}">{!! $comment->website !!}</a></td>
    </tr>
    <tr>
        <td>Nội dung</td>
        <td>{{$comment->content}}</td>
    </tr>
</table>
<p><a href="{{route('comment.edit',$comment)}}"><i>Chi tiết</i></a></p>
