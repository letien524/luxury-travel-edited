@extends('mail.mail')
@section('content')
    <div style="text-align: center;">
    <div style="text-align: justify;">
        <span style="color:#000000">
            <span style="font-size:13px">
                <strong>{{ __('Dear :name',['name' => $order->name]) }},</strong><br><br>
                {{ __('Thank you for your interest in our company and our travel services to Southeast Asia') }}.<br><br>
                {{ __('This email is to confirm that we have received your inquiry as below') }}:
            </span><br>
            <ol style="margin-bottom: 0;padding-left: 17px;">
                @if(!empty($order->name))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Your name') }}: {!! $order->name !!}</span></span><br></li>
                @endif

                @if(!empty($order->email))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Email') }}: {!! $order->email !!}</span></span><br></li>
                @endif

                @if(!empty($order->phone))
                    <li> <span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Telephone') }}: {!! $order->phone !!}</span></span><br></li>
                @endif

                @if(!empty(@$order->country->name))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Your Nationality') }}: {!! @$order->country->name !!}</span></span><br></li>
                @endif

                <?php
                $tour = $order->tour;
                if (empty($tour)) {
                    $destinations = $order->destinations->pluck('name')->toArray();
                    $destinations = implode(', ', $destinations);
                } else {
                    $destinations = $tour->destinations->pluck('name')->implode(',');
                }
                ?>

                @if(!empty($destinations))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Destinations you want to travel to') }}: {!! $destinations !!}</span></span><br></li>
                @endif

                @if(!empty(@$tour->name))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Tour request') }}: @if(!empty($tour))
                                    <a href="{!! route('tour.detail',['slug'=>@$tour->slug]) !!}"
                                       title="{!! @$tour->name !!}">{!! @$tour->name  !!}</a>@endif</span></span><br></li>
                @endif

                @if(!empty($order->departure_date))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('You would like to start your holiday on') }}: {!! $order->departure_date !!}</span></span><br></li>
                @endif

                @if(!empty($order->hotel))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Hotel Category') }}: {!! $order->hotel !!}</span></span><br></li>
                @endif

                @if(!empty(@$order->duration))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif;text-transform: capitalize">{{ __('duration') }}: {!! $order->duration !!}</span></span><br></li>
                @endif

                @if(!empty($order->activity))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Tour Style') }}: {!! $order->activity !!}</span></span><br></li>
                @endif

                @if(!empty($order->number_adult))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('The number of Adults') }}: {!! $order->number_adult !!}</span></span><br></li>
                @endif

                @if(!empty($order->number_child))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('The number of Children') }}: {!! $order->number_child !!}</span></span><br></li>
                @endif

                @if(!empty($order->created_at))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('You booked this tour on') }}: {!! date_format(date_create($order->created_at),"Y.m.d - H:m:s") !!}</span></span><br></li>
                @endif

                @if(!empty($order->content))
                    <li><span style="font-size:13px"><span
                                style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{ __('Your special request is') }}: {!! $order->content !!}</span></span></li>
                @endif
            </ol>

        </span>
    </div>

    <div style="text-align: justify;"><br>
        <span style="color:#000000"><span
                style="font-size:13px"><span
                    style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">
                                    {{ __('Our team will contact and send you the first draft of itinerary within 24 hours') }}.
                                    {!! __('Please don’t reply to this email. If you have any questions or need immediate assistance, please don’t hesitate to send us an email to :email or contact us via telephone :phone1 or :phone2',['email'=>'<a href="mailto:sales@luxurytravelvietnam.com">sales@luxurytravelvietnam.com</a>','phone1'=>'<a href="tel:+84 4 3927 4120">+84 4 3927 4120</a>', 'phone2'=>'<a href="tel:+84 834 68 69 96">+84 834 68 69 96</a>']) !!}.</span></span></span><br>
        &nbsp;
    </div>

    <div style="text-align: left;"><span
            style="color:#000000"><span
                style="font-size:13px"><span
                    style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">{{__('Thanks and best regards')}},</span></span><br>
<span style="font-size:13px"><span
        style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif"><strong>{{ __('Luxury Travel Team') }}</strong></span></span></span>
    </div>
        @if(@$admin == 1)
            <p><a href="{{ route('order.edit',$order) }}"><i>Detail</i></a></p>
        @endif
</div>
@endsection
