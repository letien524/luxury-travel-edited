<p>{{__('Dear :name', ['name' => $contact->name])}},</p>
<p>{!! __("Thank you for your interest in our company and our travel services in Southeast Asia.  This is the travel itinerary of \":tourName\". If you\\'d like to change anything, please tell us and we will customize it for you. <a href=\":contact\">Contact us now</a> for a free consultation.", ['tourName' => $tour->name, 'contact' => route('contact')]) !!}</p>
<p>{{__('Please don’t reply to this email. If you have any questions or need immediate assistance, please don’t hesitate to send us an email to sales@luxurytravelvietnam.com or contact us via telephone +84 4 3927 4120 or +84 834 68 69 96.')}}</p>
<p>{{__('Thank you and Best Regards')}},</p>
