<script type="application/ld+json">
    {
        "@context":"https://schema.org",
        "@type":"ItemList",
        "itemListElement":[
            @foreach($tours as $tour)
            {
                "@type":"ListItem",
                "position": "{{ $loop->iteration }}",
                "name": "{{ $tour->name }}",
                "image": "{{ image_url($tour->image) }}",
                "url":"{{ route('tour.detail', $tour->slug) }}"
            }{{ $loop->last ? null : ',' }}
            @endforeach
        ]
    }
</script>