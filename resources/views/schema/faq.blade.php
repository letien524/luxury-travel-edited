<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "FAQPage",
        "mainEntity":
        [
            @foreach($faqs as $faq)
                {
                    "@type": "Question",
                    "name": "{{ @$faq['name'] }}",
                    "acceptedAnswer": {
                        "@type": "Answer",
                        "text": "{!! @$faq['content'] !!}"
                    }
                }{{ $loop->last ? null : ',' }}
            @endforeach
        ]
    }
</script>