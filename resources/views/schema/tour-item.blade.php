<div hidden itemprop="itemListElement" {!! \App\Enum\ItemScopeEnum::ITEM_TYPE['Product'] !!}>
    <a href="{{ route('tour.detail', $tour->slug) }}" itemprop="url"> <span itemprop="name">{{ $tour->name }}</span></a>
    <img itemprop="image" src="{{ image_url($tour->image) }}" alt="{{ $tour->name }}">
    <span itemprop="description">{!! $tour->meta_description !!}</span>
    <span itemprop="offers" {!! \App\Enum\ItemScopeEnum::ITEM_TYPE['Offer'] !!}>
        <span itemprop="price">{{ (int)$tour->price_promotion > 0 ? $tour->price_promotion : $tour->price }}</span>
    </span>
</div>
