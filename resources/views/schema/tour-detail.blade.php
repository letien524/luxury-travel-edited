<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Product",
        "name": "{{ $tour->name }}",
        "image": "{{ image_url($tour->image) }}",
        "description": "{{ $tour->meta_description }}",
        "url": "{{ route('tour.detail', $tour->slug) }}",
        "sku": "{{ $tour->id }}",
        "offers": {
            "@type": "Offer",
            "price": "{{ (int)$tour->price_promotion > 0 ? $tour->price_promotion : $tour->price }}"
        }
    }
</script>
