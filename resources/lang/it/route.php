<?php

return [
    'about-us' => 'chi-siamo',
    'activities' => 'attività',
    'destinations' => 'destinazioni',
    'blog' => 'blog',
    'blog-category' => 'category',
    'blog-tag' => 'tag',
    'blog-comment' => ' commento',
    'tour-item' => 'tour-item',
    'tour-booking' => 'booking',
    'tour-search' => 'ricerca',
    'contact' => 'contattaci',
    'contact-subscribe' => 'sottoscrivi',
    'inquire' => 'Richiedi',
    'thank-you-book-this-tour' => 'thank-you-prenota-questo-tour',
    'thank-you-contact-us' => 'thank-you-contattaci',
    'inquire-finish' => 'thank-you-crea-il-suo-viaggio'
];