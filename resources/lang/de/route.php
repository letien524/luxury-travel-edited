<?php

return [
    'about-us' => 'ÜBER-UNS',
    'activities' => 'aktivitäten',
    'destinations' => 'ziele',
    'blog' => 'blog',
    'blog-category' => 'kategorie',
    'blog-tag' => 'tag',
    'blog-comment' => 'kommentar',
    'tour-item' => 'tour-item',
    'tour-booking' => 'booking',
    'tour-search' => 'suchen',
    'contact' => 'kontaktieren',
    'contact-subscribe' => 'abonnieren',
    'inquire' => 'anfragen',
    'thank-you-book-this-tour' => 'thank-you-buchen-sie-diese-tour',
    'thank-you-contact-us' => 'thank-you-kontaktiere-uns',
    'inquire-finish' => 'thank-you-ihre-reise-plannen'
];