<?php

return [
    'about-us' => 'chi-siamo',
    'activities' => 'attività',
    'activities-detail' => 'attivita/{slug}',
    'destinations-detail' => 'destinazioni/{slug}',
    'blog' => 'blog',
    'blog-category' => 'category/{category}',
    'blog-tag' => 'tag/{tag}',
    'blog-comment' => ' commento/{blog}',
    'tour-item' => 'tour-item',
    'tour-search' => 'ricerca',
    'contact' => 'contattaci',
    'contact-subscribe' => 'sottoscrivi',
    'inquire' => 'Richiedi ',
    'thank-you-book-this-tour' => '/thank-you-prenota-questo-tour/',
    'thank-you-contact-us' => 'thank-you-contattaci',
	'inquire/route.inquire-finish' => 'thank-you-crea-il-suo-viaggio',
];
