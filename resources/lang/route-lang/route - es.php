<?php

return [
    'about-us' => 'quiénes-somos',
    'activities' => 'actividades',
    'activities-detail' => 'actividades/{slug}',
    'destinations-detail' => 'destinos/{slug}',
    'blog' => 'blog',
    'blog-category' => 'Categoría/{category}',
    'blog-tag' => 'tag/{tag}',
    'blog-comment' => 'comentario/{blog}',
    'tour-item' => 'tour-item',
    'tour-search' => 'Buscar',
    'contact' => 'contacto ',
    'contact-subscribe' => 'Suscribir',
    'inquire' => 'pregunta',
    'thank-you-book-this-tour' => 'thank-you-book-this-tour',
    'thank-you-contact-us' => 'thank-you-reservar-un-viaje',
	'inquire/route.inquire-finish' => 'thank-you-planificar-su-viaje',
];
