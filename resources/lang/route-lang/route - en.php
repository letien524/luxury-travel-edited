<?php

return [
    'about-us' => 'about-us',
    'activities' => 'activities',
    'activities-detail' => 'activities/{slug}',
    'destinations-detail' => 'destinations/{slug}',
    'blog' => 'blog',
    'blog-category' => 'category/{category}',
    'blog-tag' => 'tag/{tag}',
    'blog-comment' => 'comment/{blog}',
    'tour-item' => 'tour-item',
    'tour-search' => 'search',
    'contact' => 'contact',
    'contact-subscribe' => 'subscribe',
    'inquire' => 'inquire',
    'thank-you-book-this-tour' => 'thank-you-book-this-tour',
    'thank-you-contact-us' => 'thank-you-contact-us',
	'inquire/route.inquire-finish' => 'thank-you-plan-your-trip/',
];
