<?php
    return [
    'blog-tag' => 'tag/{tag}',
    'blog-comment' => 'Commentaire/{blog}',
    'tour-item' => 'tour-item',

    'about-us' => 'a-propos-de-nous',
    'activities' => 'activités',
    'activities-detail' => 'activites/{slug}',
    'destinations-detail' => 'destinations/{slug}',
    'blog' => 'blog',
    'blog-category' => 'Catégorie/{category}',
    'tour-search' => 'Rechrcher',
    'contact' => 'Contactez',
    'contact-subscribe' => 'Souscrire',
    'inquire' => 'renseigner',
    'thank-you-book-this-tour' => 'thank-you-reserver-ce-tour',
    'thank-you-contact-us' => 'thank-you-contactez-nous',
	'inquire/route.inquire-finish' => 'thank-you-planifiez-mon-voyage',
];
