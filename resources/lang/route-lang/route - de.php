<?php

return [
    'about-us' => 'ÜBER-UNS',
    'activities' => 'aktivitäten',
    'activities-detail' => 'aktivitäten/{slug}',
    'destinations-detail' => 'ziele/{slug}',
    'blog' => 'blog',
    'blog-category' => 'kategorie/{category}',
    'blog-tag' => 'tag/{tag}',
    'blog-comment' => 'kommentar/{blog}',
    'tour-item' => 'tour-item',
    'tour-search' => 'suchen',
    'contact' => 'kontaktieren',
    'contact-subscribe' => 'abonnieren',
    'inquire' => 'anfragen',
    'thank-you-book-this-tour' => 'thank-you-buchen-sie-diese-tour',
    'thank-you-contact-us' => 'thank-you-kontaktiere-uns',
	'inquire/route.inquire-finish' => 'thank-you-ihre-reise-plannen',
];
