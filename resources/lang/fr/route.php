<?php

return [
    'about-us' => 'a-propos-de-nous',
    'activities' => 'activités',
    'destinations' => 'destinations',
    'blog' => 'blog',
    'blog-category' => 'Catégorie',
    'blog-tag' => 'tag',
    'blog-comment' => 'Commentaire',
    'tour-item' => 'tour-item',
    'tour-booking' => 'booking',
    'tour-search' => 'Rechrcher',
    'contact' => 'Contactez',
    'contact-subscribe' => 'Souscrire',
    'inquire' => 'renseigner',
    'thank-you-book-this-tour' => 'thank-you-reserver-ce-tour',
    'thank-you-contact-us' => 'thank-you-contactez-nous',
    'inquire-finish' => 'thank-you-planifiez-mon-voyage'
];