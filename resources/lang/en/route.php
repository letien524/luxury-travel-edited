<?php

return [
    'about-us' => 'about-us',
    'activities' => 'activities',
    'destinations' => 'destinations',
    'blog' => 'blog',
    'blog-category' => 'category',
    'blog-tag' => 'tag',
    'blog-comment' => 'comment',
    'tour-item' => 'tour-item',
    'tour-booking' => 'booking',
    'tour-search' => 'search',
    'contact' => 'contact',
    'contact-subscribe' => 'subscribe',
    'inquire' => 'inquire',
    'thank-you-book-this-tour' => 'thank-you-book-this-tour',
    'thank-you-contact-us' => 'thank-you-contact-us',
    'inquire-finish' => 'thank-you-plan-your-trip',
    'members' => 'members',
];