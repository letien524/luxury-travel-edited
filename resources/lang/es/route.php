<?php

return [
    'about-us' => 'quiénes-somos',
    'activities' => 'actividades',
    'destinations' => 'destinos',
    'blog' => 'blog',
    'blog-category' => 'Categoría',
    'blog-tag' => 'tag',
    'blog-comment' => 'comentario',
    'tour-item' => 'tour-item',
    'tour-booking' => 'booking',
    'tour-search' => 'Buscar',
    'contact' => 'contactanos',
    'contact-subscribe' => 'Suscribir',
    'inquire' => 'pregunta',
    'thank-you-book-this-tour' => 'thank-you-book-this-tour',
    'thank-you-contact-us' => 'thank-you-reservar-un-viaje',
    'inquire-finish' => 'thank-you-planificar-su-viaje'
];