<?php

Route::get('sitemap.xml', 'SitemapController@index');

Route::prefix('backend')->group(function () {

    $routes = config('admin.route');
    foreach ($routes as $name => $route) {
        $controller = ucfirst($name) . 'Controller';
        $exceptRoute = $route['except'];
        $extraRoutes = $route['extra'];

        if ($exceptRoute !== 'all') {
            Route::resource($name, $controller, ['except' => $exceptRoute]);

            Route::prefix($name)->group(function () use ($exceptRoute, $extraRoutes, $controller, $name) {

                if (in_array('getData', $extraRoutes)) {
                    Route::get("getData", "$controller@getData")->name("$name.getData");
                }

                if (in_array('duplicate', $extraRoutes)) {
                    Route::get("{{$name}}/duplicate", "$controller@duplicate")->name("$name.duplicate");
                }

                if (in_array('hotel', $extraRoutes)) {
                    Route::get("hotel", "$controller@hotel")->name("$name.hotel");
                }

                if (!in_array('bulkDestroy', $exceptRoute)) {
                    Route::post("bulkDestroy", "$controller@bulkDestroy")->name("$name.bulkDestroy");
                }

            });

        } else {
            Route::prefix($name)->group(function () use ($extraRoutes, $name, $controller) {
                foreach ($extraRoutes as $extraRoute) {
                    getExtraRoute($extraRoute, $name, $controller);
                }
            });

        }
    }

    //repeater
    $repeaters = config('admin.repeater');

    if (count($repeaters)) {
        Route::prefix('repeat')->middleware('auth')->group(function () use ($repeaters) {
            foreach ($repeaters as $module => $repeater) {
                foreach ($repeater as $value) {
                    Route::get("{$module}-{$value}", function () use ($module, $value) {
                        return view("backend.repeat.{$module}.{$value}");
                    })->name("repeat.{$module}." . \Illuminate\Support\Str::camel($value));
                }
            }
        });
    }

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::prefix('backup')->group(function () {
        Route::post('migrate', 'BackupController@migrate')->name('backup.migrate');
        Route::post('seed', 'BackupController@seed')->name('backup.seed');
        Route::post('migrateSeed', 'BackupController@migrateSeed')->name('backup.migrateSeed');
        Route::post('dataTransfer', 'BackupController@dataTransfer')->name('backup.dataTransfer');
        Route::post('dataTransferMember', 'BackupController@dataTransferMember')->name('backup.dataTransferMember');
        Route::post('dataTransferDestinationReview', 'BackupController@dataTransferDestinationReview')->name('backup.dataTransferDestinationReview');

        Route::get('', 'BackupController@index')->name('backup.index');
        Route::post('', 'BackupController@restore')->name('backup.restore');
        Route::put('', 'BackupController@restore');
        Route::delete('', 'BackupController@destroy')->name('backup.destroy');
        Route::delete('bulkDestroy', 'BackupController@bulkDestroy')->name('backup.bulkDestroy');
        Route::post('create', 'BackupController@create')->name('backup.create');
        Route::post('download}', 'BackupController@download')->name('backup.download');
    });

});

//Frontend routes

Route::get('lang/{lang}', 'LangController@changeLang')->name('lang');
Route::get('/', 'WelcomeController@home')->name('home');
Route::get(__('route.about-us'), 'WelcomeController@about')->name('about');
Route::get(__('route.activities'), 'WelcomeController@activity')->name('activity');
Route::get(__('route.activities') . '/{slug}', 'WelcomeController@activityCategory')->name('tourActivity.detail');

Route::get(__('route.destinations') . '/{slug}', 'WelcomeController@destination')->name('tourDestination.detail');
Route::get(__('route.destinations') . '/{country}/{city}', 'WelcomeController@destinationCity')->name('destination.city');

Route::get('/{slug}.html', 'WelcomeController@blogPage')->name('page.detail');
Route::prefix(__('route.blog'))->group(function () {
    Route::get('', 'WelcomeController@blog')->name('blog');
    Route::get('{slug}', 'WelcomeController@blogDetail')->name('blog.detail');
    Route::get(__('blog-category') . '/{category}', 'WelcomeController@blogCategory')->name('blogCategory.detail');
    Route::get(__('route.blog-tag') . '/{tag}', 'WelcomeController@blogTag')->name('blogTag.detail');
    Route::post(__('route.blog-comment') . '/{blog}', 'WelcomeController@blogComment')->name('blog.comment');
});

Route::prefix(__('route.tour-item'))->group(function () {
    Route::get(__('route.tour-search'), 'WelcomeController@tourSearch')->name('tour.search');
    Route::get('{slug}', 'WelcomeController@tourDetail')->name('tour.detail');
    Route::get('{slug}/' . __('route.tour-booking'), 'WelcomeController@tourBooking')->name('tour.booking');
    Route::post('{slug}/tour-plan-download', 'WelcomeController@tourPlanDownload')->name('tour.plan.download');

});

Route::prefix(__('route.contact'))->group(function () {
    Route::get('', 'WelcomeController@contact')->name('contact');
    Route::post('', 'WelcomeController@postContact')->name('contact.store');
    Route::post(__('route.contact-subscribe'), 'WelcomeController@postSubscribe')->name('subscribe.store');
});

Route::prefix(__('route.members'))->group(function () {
    Route::get('{slug}', 'WelcomeController@getMember')->name('member.detail');
    Route::post('{member}', 'WelcomeController@postContact')->name('member.contact');
});

Route::prefix(__('route.inquire'))->group(function () {
    Route::get('', 'WelcomeController@inquire')->name('inquire');
    Route::post('', 'OrderController@store')->name('inquire.store');
    Route::get(__('route.inquire-finish'), 'WelcomeController@inquireFinish')->name('inquire.finish');
});
Route::get(__('route.thank-you-book-this-tour'), 'WelcomeController@inquireFinish')->name('tour.book.finish');
Route::get(__('route.thank-you-contact-us'), 'WelcomeController@inquireFinish')->name('contact.finish');

Route::get('get-captcha', 'WelcomeController@getCaptcha')->name('get.captcha');
Route::post('customer-mail-collection-bottom', 'WelcomeController@customerMailCollectionBottom')->name('mail-collection-bottom');
Route::post('customer-mail-collection', 'WelcomeController@customerMailCollection')->name('mail-collection');


