-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2019 at 05:36 AM
-- Server version: 10.0.28-MariaDB
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `awi3_luxury_travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

DROP TABLE IF EXISTS `blog_category`;
CREATE TABLE IF NOT EXISTS `blog_category` (
  `id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `post_id`, `category_id`) VALUES
(1, 6, 5),
(2, 8, 5),
(3, 7, 5),
(4, 9, 5),
(5, 10, 5),
(6, 11, 5),
(7, 12, 5),
(8, 13, 5),
(9, 14, 5),
(10, 15, 5),
(11, 17, 5);

-- --------------------------------------------------------

--
-- Table structure for table `blog_tag`
--

DROP TABLE IF EXISTS `blog_tag`;
CREATE TABLE IF NOT EXISTS `blog_tag` (
  `id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_tag`
--

INSERT INTO `blog_tag` (`id`, `post_id`, `category_id`) VALUES
(1, 6, 29),
(2, 6, 26),
(3, 6, 27),
(4, 6, 28),
(5, 8, 29),
(6, 8, 26),
(7, 8, 27),
(8, 8, 28),
(9, 7, 29),
(10, 7, 26),
(11, 7, 27),
(12, 7, 28),
(13, 9, 29),
(14, 9, 26),
(15, 9, 27),
(16, 9, 28),
(17, 10, 29),
(18, 10, 26),
(19, 10, 27),
(20, 10, 28),
(21, 11, 29),
(22, 11, 26),
(23, 11, 27),
(24, 11, 28),
(25, 12, 29),
(26, 12, 26),
(27, 12, 27),
(28, 12, 28),
(29, 13, 29),
(30, 13, 26),
(31, 13, 27),
(32, 13, 28),
(33, 14, 29),
(34, 14, 26),
(35, 14, 27),
(36, 14, 28),
(37, 15, 29),
(38, 15, 26),
(39, 15, 27),
(40, 15, 28),
(41, 17, 29),
(42, 17, 26),
(43, 17, 27),
(44, 17, 28);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `image`, `type`, `created_at`, `updated_at`) VALUES
(1, NULL, 'public/images/partner-1.png', 'association', '2019-05-11 02:55:33', '2019-05-11 02:55:33'),
(2, NULL, 'public/images/partner-10.png', 'association', '2019-05-11 02:55:40', '2019-05-11 02:55:40'),
(3, NULL, 'public/images/partner-2.png', 'association', '2019-05-11 02:55:45', '2019-05-11 02:55:45'),
(4, NULL, 'public/images/partner-3.png', 'association', '2019-05-11 02:55:52', '2019-05-11 02:55:52'),
(5, NULL, 'public/images/partner-4.png', 'association', '2019-05-11 02:55:57', '2019-05-11 02:55:57'),
(6, NULL, 'public/images/partner-5.png', 'association', '2019-05-11 02:56:03', '2019-05-11 02:56:03'),
(7, NULL, 'public/images/partner-6.png', 'association', '2019-05-11 02:56:09', '2019-05-11 02:56:09'),
(8, NULL, 'public/images/partner-7.png', 'association', '2019-05-11 02:56:18', '2019-05-11 02:56:18'),
(9, NULL, 'public/images/partner-8.png', 'association', '2019-05-11 02:56:29', '2019-05-11 02:56:29'),
(10, NULL, 'public/images/partner-9.png', 'association', '2019-05-11 02:56:34', '2019-05-11 02:56:34'),
(11, NULL, 'public/images/award-1.png', 'awards', '2019-05-11 02:56:50', '2019-05-11 02:56:50'),
(12, NULL, 'public/images/award-2.png', 'awards', '2019-05-11 02:56:57', '2019-05-11 02:56:57'),
(13, NULL, 'public/images/award-3.png', 'awards', '2019-05-11 02:57:05', '2019-05-11 02:57:05'),
(14, NULL, 'public/images/brand-1.png', 'news', '2019-05-11 02:57:24', '2019-05-11 02:57:24'),
(15, NULL, 'public/images/brand-2.png', 'news', '2019-05-11 02:57:32', '2019-05-11 02:57:32'),
(16, NULL, 'public/images/brand-3.png', 'news', '2019-05-11 02:57:39', '2019-05-11 02:57:39'),
(17, NULL, 'public/images/brand-4.png', 'news', '2019-05-11 02:57:46', '2019-05-11 02:57:46'),
(18, NULL, 'public/images/brand-5.png', 'news', '2019-05-11 02:57:53', '2019-05-11 02:57:53'),
(19, NULL, 'public/images/brand-6.png', 'news', '2019-05-11 02:58:00', '2019-05-11 02:58:05');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `parent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_id` int(10) unsigned DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `order_menu` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `type`, `content`, `parent_id`, `meta_title`, `meta_description`, `lang_id`, `image`, `status`, `order_menu`, `created_at`, `updated_at`) VALUES
(1, 'Vietnam', 'vietnam', 'destination-category', '{"banner":{"top":{"image":null,"name":"The Kingdom of Wonder"},"bottom":{"image":null}},"name":"Explore Our Cambodia","content":"Cambodia''s geography has played a substantial role in shaping its history. Its central location along the Indochinese Peninsula and relatively flat terrain facilitated the rise of the Khmer Empire which flourished from its base in Angkor for over 500 years. Conversely, Cambodia''s proximity to the chaos of the Vietnam War sowed seeds of catastrophe that the country is now recovering from. Both of these periods are inextricable to the identity of both Cambodia, and its people","count":{"status":"1","population":"15m","temple":"1,2k","city":"23"},"image":"public\\/images\\/map.png","travel_guide":{"content":{"1557471160450":{"name":"Food and Drinks","content":"<div class=\\"row\\">\\r\\n<div class=\\"col-md-4\\">\\r\\n<div class=\\"vk-img mb-20 mb-lg-0\\"><img alt=\\"\\" src=\\"http:\\/\\/localhost:88\\/luxury-travel\\/storage\\/app\\/public\\/images\\/dest-1.jpg\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"col-md-8\\">\\r\\n<div>\\r\\n<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>\\r\\n<!--                                                <a href=\\"blog-details.php\\" class=\\"_link\\">View details<\\/a>--><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>"},"1557471323846":{"name":"Holiday","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471336860":{"name":"Money","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471351101":{"name":"Health requirements","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471370516":{"name":"Communication & business hours","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471383477":{"name":"What type of holidays in Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471412525":{"name":"Travel Tips for Cambodia","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471429525":{"name":"What to Pack to Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471451501":{"name":"Etiquette","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}},"tour_guide":"6"},"blog_category":["5"]}', NULL, NULL, NULL, NULL, 'public/images/home-2-1.jpg', 1, NULL, '2019-05-10 06:57:49', '2019-05-10 07:40:56'),
(2, 'Cambodia', 'cambodia', 'blog-category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 06:58:37', '2019-05-10 06:58:37'),
(3, 'Laos', 'laos', 'blog-category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 06:58:47', '2019-05-10 06:58:47'),
(4, 'Thailand', 'thailand', 'blog-category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 06:58:56', '2019-05-10 06:58:56'),
(5, 'Vietnam', 'vietnam', 'blog-category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 06:59:15', '2019-05-10 06:59:15'),
(6, 'Halong Bay Tour', 'halong-bay-tour', 'blog-category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 06:59:21', '2019-05-10 06:59:21'),
(7, 'Myanmar', 'myanmar', 'blog-category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 06:59:28', '2019-05-10 06:59:28'),
(8, 'Southeast Asia', 'southeast-asia', 'blog-category', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 06:59:37', '2019-05-10 06:59:37'),
(9, 'Culinary Tours', 'culinary-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:22:43', '2019-05-10 07:22:43'),
(10, 'Family Tours', 'family-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:23:41', '2019-05-10 07:23:41'),
(11, 'Biking & Trekking', 'biking-trekking', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:23:51', '2019-05-10 07:23:51'),
(12, 'Culture Tours', 'culture-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:23:59', '2019-05-10 07:23:59'),
(13, 'Adventure Tours', 'adventure-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:06', '2019-05-10 07:24:06'),
(14, 'Honeymoon Tours', 'honeymoon-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:11', '2019-05-10 07:24:11'),
(15, 'Golf Tours', 'golf-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:18', '2019-05-10 07:24:18'),
(16, 'Stopover Tours', 'stopover-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:28', '2019-05-10 07:24:28'),
(17, 'Excursion Tours', 'excursion-tours', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:36', '2019-05-10 07:24:36'),
(18, 'Beach Vacation', 'beach-vacation', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:41', '2019-05-10 07:24:41'),
(19, 'Day Trips', 'day-trips', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:47', '2019-05-10 07:24:47'),
(20, 'Set Departure', 'set-departure', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:24:53', '2019-05-10 07:24:53'),
(21, 'Ultra-Luxury Wellness & Spa Packages', 'ultra-luxury-wellness-spa-packages', 'tour-category', '{"banner":{"top":{"image":null},"bottom":{"image":null}}}', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-10 07:25:01', '2019-05-10 07:25:01'),
(22, 'Laos', 'laos', 'destination-category', '{"banner":{"top":{"image":null,"name":"The Kingdom of Wonder"},"bottom":{"image":null}},"name":"Explore Our Cambodia","content":"Cambodia''s geography has played a substantial role in shaping its history. Its central location along the Indochinese Peninsula and relatively flat terrain facilitated the rise of the Khmer Empire which flourished from its base in Angkor for over 500 years. Conversely, Cambodia''s proximity to the chaos of the Vietnam War sowed seeds of catastrophe that the country is now recovering from. Both of these periods are inextricable to the identity of both Cambodia, and its people","count":{"status":"1","population":"15m","temple":"1,2k","city":"23"},"image":"public\\/images\\/map.png","travel_guide":{"content":{"1557471160450":{"name":"Food and Drinks","content":"<div class=\\"row\\">\\r\\n<div class=\\"col-md-4\\">\\r\\n<div class=\\"vk-img mb-20 mb-lg-0\\"><img alt=\\"\\" src=\\"http:\\/\\/localhost:88\\/luxury-travel\\/storage\\/app\\/public\\/images\\/dest-1.jpg\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"col-md-8\\">\\r\\n<div>\\r\\n<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>\\r\\n<!--                                                <a href=\\"blog-details.php\\" class=\\"_link\\">View details<\\/a>--><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>"},"1557471323846":{"name":"Holiday","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471336860":{"name":"Money","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471351101":{"name":"Health requirements","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471370516":{"name":"Communication & business hours","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471383477":{"name":"What type of holidays in Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471412525":{"name":"Travel Tips for Cambodia","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471429525":{"name":"What to Pack to Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471451501":{"name":"Etiquette","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}},"tour_guide":"8"},"blog_category":["5"]}', NULL, NULL, NULL, NULL, 'public/images/home-3.jpg', 1, NULL, '2019-05-10 06:57:49', '2019-05-10 07:40:49'),
(23, 'Cambodia', 'cambodia', 'destination-category', '{"banner":{"top":{"image":null,"name":"The Kingdom of Wonder"},"bottom":{"image":null}},"name":"Explore Our Cambodia","content":"Cambodia''s geography has played a substantial role in shaping its history. Its central location along the Indochinese Peninsula and relatively flat terrain facilitated the rise of the Khmer Empire which flourished from its base in Angkor for over 500 years. Conversely, Cambodia''s proximity to the chaos of the Vietnam War sowed seeds of catastrophe that the country is now recovering from. Both of these periods are inextricable to the identity of both Cambodia, and its people","count":{"status":"1","population":"15m","temple":"1,2k","city":"23"},"image":"public\\/images\\/map.png","travel_guide":{"content":{"1557471160450":{"name":"Food and Drinks","content":"<div class=\\"row\\">\\r\\n<div class=\\"col-md-4\\">\\r\\n<div class=\\"vk-img mb-20 mb-lg-0\\"><img alt=\\"\\" src=\\"http:\\/\\/localhost:88\\/luxury-travel\\/storage\\/app\\/public\\/images\\/dest-1.jpg\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"col-md-8\\">\\r\\n<div>\\r\\n<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>\\r\\n<!--                                                <a href=\\"blog-details.php\\" class=\\"_link\\">View details<\\/a>--><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>"},"1557471323846":{"name":"Holiday","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471336860":{"name":"Money","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471351101":{"name":"Health requirements","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471370516":{"name":"Communication & business hours","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471383477":{"name":"What type of holidays in Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471412525":{"name":"Travel Tips for Cambodia","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471429525":{"name":"What to Pack to Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471451501":{"name":"Etiquette","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}},"tour_guide":"6"},"blog_category":["5"]}', NULL, NULL, NULL, NULL, 'public/images/home-4.jpg', 1, NULL, '2019-05-10 06:57:49', '2019-05-10 07:40:39'),
(24, 'Thailand', 'thailand', 'destination-category', '{"banner":{"top":{"image":null,"name":"The Kingdom of Wonder"},"bottom":{"image":null}},"name":"Explore Our Cambodia","content":"Cambodia''s geography has played a substantial role in shaping its history. Its central location along the Indochinese Peninsula and relatively flat terrain facilitated the rise of the Khmer Empire which flourished from its base in Angkor for over 500 years. Conversely, Cambodia''s proximity to the chaos of the Vietnam War sowed seeds of catastrophe that the country is now recovering from. Both of these periods are inextricable to the identity of both Cambodia, and its people","count":{"status":"1","population":"15m","temple":"1,2k","city":"23"},"image":"public\\/images\\/map.png","travel_guide":{"content":{"1557471160450":{"name":"Food and Drinks","content":"<div class=\\"row\\">\\r\\n<div class=\\"col-md-4\\">\\r\\n<div class=\\"vk-img mb-20 mb-lg-0\\"><img alt=\\"\\" src=\\"http:\\/\\/localhost:88\\/luxury-travel\\/storage\\/app\\/public\\/images\\/dest-1.jpg\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"col-md-8\\">\\r\\n<div>\\r\\n<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>\\r\\n<!--                                                <a href=\\"blog-details.php\\" class=\\"_link\\">View details<\\/a>--><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>"},"1557471323846":{"name":"Holiday","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471336860":{"name":"Money","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471351101":{"name":"Health requirements","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471370516":{"name":"Communication & business hours","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471383477":{"name":"What type of holidays in Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471412525":{"name":"Travel Tips for Cambodia","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471429525":{"name":"What to Pack to Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471451501":{"name":"Etiquette","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}},"tour_guide":"6"},"blog_category":["5"]}', NULL, NULL, NULL, NULL, 'public/images/home-5.jpg', 1, NULL, '2019-05-10 06:57:49', '2019-05-10 07:40:33'),
(25, 'Myanmar', 'myanmar', 'destination-category', '{"banner":{"top":{"image":null,"name":"The Kingdom of Wonder"},"bottom":{"image":null}},"name":"Explore Our Cambodia","content":"Cambodia''s geography has played a substantial role in shaping its history. Its central location along the Indochinese Peninsula and relatively flat terrain facilitated the rise of the Khmer Empire which flourished from its base in Angkor for over 500 years. Conversely, Cambodia''s proximity to the chaos of the Vietnam War sowed seeds of catastrophe that the country is now recovering from. Both of these periods are inextricable to the identity of both Cambodia, and its people","count":{"status":"1","population":"15m","temple":"1,2k","city":"23"},"image":"public\\/images\\/map.png","travel_guide":{"content":{"1557471160450":{"name":"Food and Drinks","content":"<div class=\\"row\\">\\r\\n<div class=\\"col-md-4\\">\\r\\n<div class=\\"vk-img mb-20 mb-lg-0\\"><img alt=\\"\\" src=\\"http:\\/\\/localhost:88\\/luxury-travel\\/storage\\/app\\/public\\/images\\/dest-1.jpg\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"col-md-8\\">\\r\\n<div>\\r\\n<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>\\r\\n<!--                                                <a href=\\"blog-details.php\\" class=\\"_link\\">View details<\\/a>--><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>"},"1557471323846":{"name":"Holiday","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471336860":{"name":"Money","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471351101":{"name":"Health requirements","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471370516":{"name":"Communication & business hours","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471383477":{"name":"What type of holidays in Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471412525":{"name":"Travel Tips for Cambodia","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471429525":{"name":"What to Pack to Cambodia?","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557471451501":{"name":"Etiquette","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}},"tour_guide":"3"},"blog_category":["5"]}', NULL, NULL, NULL, NULL, 'public/images/home-7.jpg', 1, NULL, '2019-05-10 06:57:49', '2019-05-10 07:43:33'),
(26, 'My son', 'my-son', 'blog-tag', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:47:16', '2019-05-10 07:47:44'),
(27, 'My son guide', 'my-son-guide', 'blog-tag', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:47:24', '2019-05-10 07:47:37'),
(28, 'My son sacterday', 'my-son-sacterday', 'blog-tag', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:48:52', '2019-05-10 07:48:52'),
(29, 'Holiday to vietnam', 'holiday-to-vietnam', 'blog-tag', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:49:05', '2019-05-10 07:49:05'),
(30, NULL, NULL, 'blog-category-main', '{"banner":"public\\/images\\/ads-1.jpg","link":"#"}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:49:13', '2019-05-10 08:54:36'),
(31, NULL, NULL, 'activity-contact-main', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 09:14:10', '2019-05-10 09:14:10'),
(32, 'Tell us more about your preferences!', NULL, 'inquire-category-main', '{"content":"We provide tailor made private tour packages to Southeast Asia and the Far East. By filling up this form, you help us better understand your needs and wants in order to create an itinerary that matches you.","faq":"1","hotel_category":{"1557539975509":"3 stars","1557539976610":"4 stars","1557539977242":"5 stars"},"travel_style":{"1557540001821":"Classic & culture","1557540367439":"Leisure","1557540368166":"Family","1557540368868":"Beach Holiday","1557540369322":"Art Tour","1557540369866":"Honeymoon","1557540370426":"Others"}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 09:25:45', '2019-05-11 02:06:39'),
(33, NULL, NULL, 'contact-category-main', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-11 02:53:54', '2019-05-11 02:53:54');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `post_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

DROP TABLE IF EXISTS `configs`;
CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(10) unsigned NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `type`, `content`, `created_at`, `updated_at`) VALUES
(1, 'general', '{"site_logo":null,"site_favicon":null,"site_payment":null,"site_name":"Luxury Travel","site_hotline":"(+84) 834 686 996","site_email":"sales@luxurytravelvietnam.com","site_meta_title":"meta title Chung","site_meta_description":"meta description chung","site_script_header":null,"site_script_footer":null,"site_enquiry_image":null,"site_enquiry_banner":null,"site_enquiry_text":"Whatever you want your trip to include, at Lux Travel we\\u2019ll create a fully personalised vacation just for you"}', NULL, '2019-05-10 04:55:28'),
(2, 'social', '{"social":{"facebook":"#","twitter":"#","tripadvisor":"#","google_plus":"#","whatsapp":"#","skype":"#"}}', NULL, '2019-05-10 07:34:25'),
(3, 'footer_menu', '{"luxury_travel":"<ul class=\\"vk-footer__list vk-footer__list--style-3\\">\\r\\n\\t<li><a href=\\"#\\">Who We Are<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Meet Our Team<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Why Travel With Us<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Why Travel With Us<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Logo Meaning<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Our Awards<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">&ldquo;WOW&rdquo; Services<\\/a><\\/li>\\r\\n<\\/ul>","information":"<ul class=\\"vk-footer__list vk-footer__list--style-3\\">\\r\\n\\t<li><a href=\\"http:\\/\\/localhost:88\\/luxury-travel\\/contact\\">Contact Us<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">FAQs<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Payment Policies<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">For Travel Agents<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Brochure<\\/a><\\/li>\\r\\n\\t<li><a href=\\"#\\">Blog<\\/a><\\/li>\\r\\n<\\/ul>"}', NULL, '2019-05-10 05:21:09'),
(4, 'tour-filter', '{"filter_price":{"1557469952685":{"name":"< $500","min":null,"max":"500"},"1557472421477":{"name":"$500 - $1500","min":"500","max":"1500"},"1557472422483":{"name":"$1500 - $2000","min":"1500","max":"2000"},"1557472422922":{"name":"$2000 - $3000","min":"2000","max":"3000"},"1557472720670":{"name":"More $3000","min":"3000","max":null}},"filter_duration":{"1557469963484":{"name":"< 5 day","min":null,"max":"5"},"1557469964251":{"name":"5 - 7 day","min":"5","max":"7"},"1557469964731":{"name":"7 - 10 day","min":"7","max":"10"},"1557469965900":{"name":"10 - 15 day","min":"10","max":"15"},"1557470331343":{"name":"> 15 day","min":"15","max":null}}}', '2019-05-10 05:34:53', '2019-05-10 07:19:13');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotline` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `order_menu` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `address`, `phone`, `hotline`, `email`, `content`, `type`, `status`, `order_menu`, `created_at`, `updated_at`) VALUES
(1, 'Ho Chi Minh Office', 'R301, FL3, Tran Quy Bld, 57 Le Thi Hong Gam Street, District 1, HCMC', '+ 84. 28.3914.0476', '+84. 906.980.167', 'sales@luxurytravelvietnam.com', NULL, 'address', NULL, NULL, '2019-05-10 05:25:58', '2019-05-10 05:25:58'),
(2, 'Dang Nang Office', '73 Y Lan Nguyen Phi Street, Hai Chau Dist, Danang', '+84.236-3611-981', '+84.236-3611-982', 'sales@luxurytravelvietnam.com', NULL, 'address', NULL, NULL, '2019-05-10 05:26:26', '2019-05-10 05:26:26'),
(3, 'Hanoi Head Office', '456 Lac Long Quan Str., Tay Ho, Hanoi, Vietnam', '+84. 24-3927-4120', '+84. 834-68-69-96', 'sales@luxurytravelvietnam.com', NULL, 'address', NULL, NULL, '2019-05-10 05:26:52', '2019-05-10 05:26:52');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D''Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `name`, `content`, `status`, `type`, `lang_id`, `created_at`, `updated_at`) VALUES
(1, 'Inquire Page', '{"1557484062882":{"name":"How to book with us","content":"<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>"},"1557484081568":{"name":"Customize your holiday","content":"<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>"},"1557484133889":{"name":"Get ready for your dream trips","content":"<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>"},"1557484146409":{"name":"Booking Guide","content":"<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>"}}', NULL, 'faq', NULL, '2019-05-10 10:29:19', '2019-05-10 10:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tour_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `url`, `name`, `description`, `type`, `tour_id`, `created_at`, `updated_at`) VALUES
(37, 'public/images/tour-detial-1.jpg', NULL, NULL, NULL, 1, '2019-05-11 04:27:53', '2019-05-11 04:27:53'),
(38, 'public/images/tour-detial-1.jpg', NULL, NULL, NULL, 1, '2019-05-11 04:27:53', '2019-05-11 04:27:53'),
(39, 'public/images/tour-detial-1.jpg', NULL, NULL, NULL, 1, '2019-05-11 04:27:53', '2019-05-11 04:27:53'),
(40, 'public/images/tour-detial-1.jpg', NULL, NULL, NULL, 1, '2019-05-11 04:27:53', '2019-05-11 04:27:53'),
(41, 'public/images/tour-detial-1.jpg', NULL, NULL, NULL, 1, '2019-05-11 04:27:53', '2019-05-11 04:27:53'),
(42, 'public/images/tour-detial-1.jpg', NULL, NULL, NULL, 1, '2019-05-11 04:27:53', '2019-05-11 04:27:53');

-- --------------------------------------------------------

--
-- Table structure for table `langs`
--

DROP TABLE IF EXISTS `langs`;
CREATE TABLE IF NOT EXISTS `langs` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `langs`
--

INSERT INTO `langs` (`id`, `name`, `locale`, `link`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', NULL, NULL, '2019-05-11 03:48:16'),
(2, 'French', 'fr', NULL, NULL, '2019-05-11 03:48:16'),
(3, 'German', 'de', NULL, NULL, '2019-05-11 03:48:16'),
(4, 'Spanish', 'es', NULL, NULL, '2019-05-11 03:48:16'),
(5, 'Italian', 'it', NULL, NULL, '2019-05-11 03:48:16');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(10) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `order_menu` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `image`, `position`, `content`, `order_menu`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Pham Ha', 'public/images/team-1.jpg', 'Founder / CEO', 'After 12 years in the industry, Pham Ha found his own business in 2004 providing luxury tours in Vietnam. Pham Ha who is determined to create the most unforgettable travel experiences for high-end tourists.', 8, 1, '2019-05-10 07:00:41', '2019-05-10 07:10:54'),
(2, 'Thang Phan', 'public/images/team-2.jpg', 'Director of Sales and Marketing', '''m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.', 7, 1, '2019-05-10 07:01:06', '2019-05-10 07:11:07'),
(3, 'Duong Vuong', 'public/images/team-3.jpg', 'English Sales Executive', '''m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.', NULL, 1, '2019-05-10 07:01:06', '2019-05-10 07:01:06'),
(4, 'Quynh Nguyen', 'public/images/team-4.jpg', 'English Sales Executive', '''m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.', NULL, 1, '2019-05-10 07:01:06', '2019-05-10 07:01:06'),
(5, 'Vu Long', 'public/images/team-5.jpg', 'English Sales Executive', '''m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.', NULL, 1, '2019-05-10 07:01:06', '2019-05-10 07:01:06'),
(6, 'Hao Chu', 'public/images/team-6.jpg', 'English Sales Executive', '''m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.', NULL, 1, '2019-05-10 07:01:06', '2019-05-10 07:01:06'),
(7, 'Quynh Anh', 'public/images/team-7.jpg', 'English Sales Executive', '''m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.', NULL, 1, '2019-05-10 07:01:06', '2019-05-10 07:01:06'),
(8, 'Le Mai', 'public/images/team-8.jpg', 'English Sales Executive', '''m proud and excited to introduce our countries to visitors from around the world. Consider me a local friend and I will show you how wonderful our destinations and their people can be.', NULL, 1, '2019-05-10 07:01:06', '2019-05-10 07:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(49, '2014_10_12_000000_create_users_table', 1),
(50, '2014_10_12_100000_create_password_resets_table', 1),
(51, '2019_02_21_141019_create_config_table', 1),
(52, '2019_04_16_151806_create_brands_table', 1),
(53, '2019_04_17_161358_create_langs_table', 1),
(54, '2019_04_17_161359_create_post_table', 1),
(55, '2019_04_17_161360_create_categories_table', 1),
(56, '2019_04_17_161361_create_comments_table', 1),
(57, '2019_04_18_201923_create_blog_tag_table', 1),
(58, '2019_04_18_214017_create_blog_category_table', 1),
(59, '2019_04_19_111703_create_members_table', 1),
(60, '2019_04_21_203929_create_contacts_table', 1),
(61, '2019_04_22_144125_create_faqs_table', 1),
(62, '2019_04_22_144126_create_tours_table', 1),
(63, '2019_04_22_144127_create_image_table', 1),
(64, '2019_04_23_094331_create_orders_table', 1),
(65, '2019_05_11_083744_create_order_category_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `activity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_child` int(11) DEFAULT NULL,
  `number_adult` int(11) DEFAULT NULL,
  `departure_date` date DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `tour_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_category`
--

DROP TABLE IF EXISTS `order_category`;
CREATE TABLE IF NOT EXISTS `order_category` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_short` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `lang_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order_menu` int(11) DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `name`, `slug`, `image`, `content_short`, `content`, `type`, `status`, `lang_id`, `parent_id`, `order_menu`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Home', NULL, NULL, NULL, '{"section1":{"image":null,"name":{"main":"Discover South East Asia","sub":"Luxury Tailor-made Experience to"},"content":[{"name":"Tailor made","content":"You can expect a consistent set of amenities you need to live like you do at home\\u2013from"},{"name":"Local Expert","content":"If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t"},{"name":"Premium service","content":"You can expect a consistent set of amenities you need to live like you do at home"}]},"section2":{"name":"We offer luxury travel exclusively to Southeast Asia","link":null,"content":"Luxury Travel Ltd. was founded in 2004 by Mr. Pham Ha, passionate traveler and entrepreneur. With more than 15 years of experience in the travel industry, he is a keynote speaker and luxury travel expert in Vietnam and around the globe. When Pham Ha saw the need for a tour company offering services that would specialize in high-end travelers, he didn\\u2019t hesitate for long."},"section3":{"name":"How it works","name_sub":"Amazing holiday packages in 3 simple steps"},"section4":{"a_1557545931483":{"image":"public\\/images\\/home-2.jpg","name":"Vietnam","content":"Do you want to discover not only one but two or three destinations in Southeast Asia?","link":null},"a_1557545932601":{"image":"public\\/images\\/home-3.jpg","name":"Laos","content":"Do you want to discover not only one but two or three destinations in Southeast Asia?","link":null},"a_1557545933324":{"image":"public\\/images\\/home-4.jpg","name":"Thailand","content":"Do you want to discover not only one but two or three destinations in Southeast Asia?","link":null},"a_1557545934344":{"image":"public\\/images\\/home-5.jpg","name":"Myanmar","content":"Do you want to discover not only one but two or three destinations in Southeast Asia?","link":null},"a_1557545935091":{"image":"public\\/images\\/home-6.jpg","name":"Multi countries tours","content":"Do you want to discover not only one but two or three destinations in Southeast Asia?","link":null},"a_1557545936257":{"image":"public\\/images\\/home-7.jpg","name":"Cambodia","content":"Do you want to discover not only one but two or three destinations in Southeast Asia?","link":null}},"section5":{"name":"Tour highlight"},"section6":{"name":"Tour Style","name_sub":"Find your ideal tour"},"section7":{"name":"Tripadvisor","link":null,"content":{"1557545895908":{"name":"Jack Goodwin","country":"Argentina","date":"9\\/30\\/2018","content":"My husband and I just got back from a 11 night trip during which we visited Hanoi, Halong Bay, Hoi An,Siem Reap and Hoi Chi Min in that order. Luxury Travel organized everything except flights to Vietnam and back. We used private tours and \\u2026"},"1557545896713":{"name":"Jack Goodwin","country":"Argentina","date":"9\\/30\\/2018","content":"My husband and I just got back from a 11 night trip during which we visited Hanoi, Halong Bay, Hoi An,Siem Reap and Hoi Chi Min in that order. Luxury Travel organized everything except flights to Vietnam and back. We used private tours and \\u2026"},"1557545897336":{"name":"Jack Goodwin","country":"Argentina","date":"9\\/30\\/2018","content":"My husband and I just got back from a 11 night trip during which we visited Hanoi, Halong Bay, Hoi An,Siem Reap and Hoi Chi Min in that order. Luxury Travel organized everything except flights to Vietnam and back. We used private tours and \\u2026"}}},"section8":{"name":"News and Special"}}', 'home', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 05:33:03', '2019-05-11 03:42:55'),
(2, '{"main":"Book The Best Deal","sub":"Select your package"}', NULL, NULL, NULL, '{"main":"Pay in easy installments & get ready to enjoy your holiday.","sub":"& tell us your preferences"}', 'how-it-work', NULL, NULL, NULL, 3, NULL, NULL, '2019-05-10 06:43:57', '2019-05-10 06:46:59'),
(3, '{"main":"Get multiple free quotes","sub":"Get multiple free quotes"}', NULL, NULL, NULL, '{"main":"Connect with top 3 agents, compare quotes & customize further.","sub":"from verified travel experts"}', 'how-it-work', NULL, NULL, NULL, 2, NULL, NULL, '2019-05-10 06:44:59', '2019-05-10 06:46:41'),
(4, '{"main":"Personalise This Package","sub":"Customize & book"}', NULL, NULL, NULL, '{"main":"Make changes as per your travel plan & submit the request.","sub":"a perfect holiday experience"}', 'how-it-work', NULL, NULL, NULL, 1, NULL, NULL, '2019-05-10 06:45:13', '2019-05-10 06:45:49'),
(5, 'About', NULL, NULL, NULL, '{"section1":{"image":null,"name":"Who We Are","content":"Luxury Travel Ltd. was founded in 2004 by Mr. Pham Ha, passionate traveler and entrepreneur. With more than 15 years of experience in the travel industry, he is a keynote speaker and luxury travel expert in Vietnam and around the globe. When Pham Ha saw the need for a tour company offering services that would specialize in high-end travelers, he didn\\u2019t hesitate for long. That\\u2019s how the idea of creating a travel company with an emphasis on ultra-luxurious experiences, privately guides and bespoke holidays came to life. As a lover of Arts, Writing, History and Culture, Pham Ha created personalized tours with an immersive approach to traveling."},"section2":{"name":"Our Business Is Sustainable And Responsible","content":["We are a 100% fully registered and privately-owned Vietnamese company. Our industry and consumer awards are the testimony to our excellent reputation and unbeatable experience in the region, and much of our business comes from referrals and business network recommendations. We\\u2019ll help travelers satisfy their passion for travel by sharing our insider knowledge, while our connections will get them access to unique places and one-of-a-kind experiences. Our customers have given us great feedback, and you can tell by our reviews on TripAdvisor. The company is proud of its 99% satisfaction rate and we have a repeat client rate of over 60%.","Following this idea, Pham Ha also designed the Luxury Travel University programs for his colleagues to give them the most accurate training when serving high-end travelers from all over the world. The most crucial part of the business is through the travel agents, who advise and read on to the needs of the travelers. Everyone working at Luxury Travel has customer care and service skills without equal. Our Tour guides speak many languages and everyone, from managers, marketers, travel agents to guides and chauffeurs have one thing in mind: Touching Hearts of every traveler that chooses Luxury Travel."]},"section3":{"image":"public\\/images\\/about-2.jpg"},"section4":{"name":"Our Selling Point","content":[{"name":"100%Tailor-made","content":"At Luxury Travel Vietnam we design customized travel services for leisure, business travelers, and MICE. We operate our own fleet of luxury vehicles and provide access to all kinds of luxury transports. Our team will be your personal experiences manager creating an authentic and 100% tailor-made holiday."},{"name":"Local Travel Experts","content":"With more than 14 years of experiences, our knowledgeable staff will advise you in finding the most suitable and wonderful itineraries according to your tastes and their extended knowledge of each region. We bring to you the best holiday without any middleman cost."},{"name":"Unique Experiences","content":"We understand that you are unique and will make sure that your journey is exceptional. With distinctive activites, off-the-beaten track destinations and immersion in local life, our travel packages are above all, experiential."}]},"section5":{"name":"Creating Authentic Experiences For Discerning Travelers.","image":"public\\/images\\/about-4.jpg","content":{"1557471965295":{"name":"First Luxury TO and DMC in Vietnam","content":"<div class=\\"row\\">\\r\\n<div class=\\"col-md-4\\">\\r\\n<div class=\\"vk-img\\"><img alt=\\"\\" src=\\"http:\\/\\/localhost:88\\/luxury-travel\\/storage\\/app\\/public\\/images\\/dest-1.jpg\\" \\/><\\/div>\\r\\n<\\/div>\\r\\n\\r\\n<div class=\\"col-md-8\\">\\r\\n<div>\\r\\n<p>While not as varied as food from neighboring Malaysia, Thailand, or Vietnam, Khmer food is tasty and cheap and is invariably accompanied by rice (or occasionally noodles). And unlike their Thai and Lao neighbors, Cambodians generally do not have a taste for spicy hot food, and black pepper is the preferred choice in cooking instead of chili peppers<\\/p>\\r\\n<a class=\\"_link\\" href=\\"#\\">View details<\\/a><\\/div>\\r\\n<\\/div>\\r\\n<\\/div>"},"1557472032823":{"name":"Our Products","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472052076":{"name":"Our Quality Assurance","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472065508":{"name":"Our Reputation","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472075859":{"name":"Our Vision","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557472096101":{"name":"Company Information","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}},"section6":{"name":"Meet Our Team","content":"Our team, dedicated to agents and tour operators, will be your personal experience managers and deliver you and your customers the world\\u2019s most authentic luxury travel experiences with excellent service, know-how and trust."}}', 'about', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:04:10', '2019-05-10 07:08:39'),
(6, 'Bryce Canyon A Stunning Us Travel Destination', 'bryce-canyon-a-stunning-us-travel-destination', '{"thumbnail":"public\\/images\\/blog-1.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:15:58'),
(7, 'Party Jokes Startling But Unnecessary', 'party-jokes-startling-but-unnecessary', '{"thumbnail":"public\\/images\\/blog-2.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:36:56'),
(8, 'Coventry City Guide Including Coventry Hotels', 'coventry-city-guide-including-coventry-hotels', '{"thumbnail":"public\\/images\\/blog-3.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:37:35'),
(9, 'An awkward bird symbolizes the fight over America''s West', 'an-awkward-bird-symbolizes-the-fight-over-americas-west', '{"thumbnail":"public\\/images\\/blog-4.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:38:26'),
(10, '''Steal'' 3-day, 2-night Itinerary in Luang Prabang from …', 'steal-3-day-2-night-itinerary-in-luang-prabang-from-…', '{"thumbnail":"public\\/images\\/blog-5.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:38:57'),
(11, 'These Thailand Honeymoon Packages Are Just For You', 'these-thailand-honeymoon-packages-are-just-for-you', '{"thumbnail":"public\\/images\\/blog-6.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:39:22'),
(12, 'Travel In Style – The 17th Edition', 'travel-in-style-–-the-17th-edition', '{"thumbnail":"public\\/images\\/blog-7.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:39:44'),
(13, 'Bryce Canyon A Stunning Us Travel Destination1', 'bryce-canyon-a-stunning-us-travel-destination1', '{"thumbnail":"public\\/images\\/blog-1.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:40:01'),
(14, 'Party Jokes Startling But Unnecessary1', 'party-jokes-startling-but-unnecessary1', '{"thumbnail":"public\\/images\\/blog-2.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:40:26'),
(15, 'Coventry City Guide Including Coventry Hotels1', 'coventry-city-guide-including-coventry-hotels1', '{"thumbnail":"public\\/images\\/blog-3.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:40:54'),
(17, 'Bryce Canyon A Stunning Us Travel Destination2', 'bryce-canyon-a-stunning-us-travel-destination2', '{"thumbnail":"public\\/images\\/blog-2.jpg","banner":null}', 'Brent Conrad talks with everyone from, frequent travelers to the busy family that can only get away for vacation every couple of years. He states that saving', '<div>\r\n<h3>Nom village is quite strange name to many travelers. It is about 30 kilometers far from East of Hanoi.</h3>\r\n\r\n<p>Responsible tourism is defined as: &ldquo;Travel conducted in such manner as not to harm or degrade the cultural or natural environment of the places visited.&ldquo; Acting as a local and responsible tour operator, we aim to create better places for people to visit and better places for people to live. We are committed to making our business more sustainable and responsible in every way we can. By taking measured steps to reduce the negative effects of the travel industry on our environment, and accentuate the positive impact of tourism. We make sure that our tours don&rsquo;t harm the environment and promote local business to help the community.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-2.jpg" /> <span>Visiting Nom village in your holiday to Vietnam</span></p>\r\n\r\n<h3>Paragliding in Khau Pha &ndash; New adventure experience when traveling to Vietnam</h3>\r\n\r\n<p>May and September are considered as the most nicest times to visit Northern Vietnam, such as Sapa, Mu Cang Chai, Dong Van, Lung Cu. If you travel to Vietnam during May when all those areas are in harvesting season, you will have the chance to see the most stunning and graceful landscape. The common ways to explore the the area are trekking and motorbiking. Most local travel agencies suggest to add these in your Vietnam itineraries. However, traveling to Vietnam this summer, you can try paragliding &ndash; a complete new adventure experience.</p>\r\n\r\n<p><img alt="" src="http://developer5.gco.vn/luxury-travel/storage/app/public/images/blog-detail-3.jpg" /> <span> From Khau Pha, you will the breathtaking view of stunning natural scenery</span></p>\r\n</div>', 'blog', 1, NULL, NULL, NULL, NULL, NULL, '2019-05-10 07:53:34', '2019-05-10 08:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

DROP TABLE IF EXISTS `tours`;
CREATE TABLE IF NOT EXISTS `tours` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `map` text COLLATE utf8mb4_unicode_ci,
  `plan` text COLLATE utf8mb4_unicode_ci,
  `hotel` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `highlight` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lang_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `faq_id` int(10) unsigned DEFAULT NULL,
  `destination_id` int(10) unsigned DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_promotion` int(11) DEFAULT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`id`, `name`, `slug`, `image`, `duration`, `content`, `map`, `plan`, `hotel`, `file`, `status`, `highlight`, `parent_id`, `lang_id`, `category_id`, `faq_id`, `destination_id`, `price`, `price_promotion`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Discover Exotic Lands and Enjoy the Warmth and Comfort', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort', 'public/images/shop-1.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, 1, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:53'),
(2, 'Discover Exotic Lands and Enjoy the Warmth and Comfort1', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort1', 'public/images/shop-2.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557482767423":{"name":null,"content":null},"1557482776732":{"name":null,"content":null}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, 1, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:49'),
(3, 'Discover Exotic Lands and Enjoy the Warmth and Comfort2', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort2', 'public/images/shop-3.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557482799711":{"name":null,"content":null}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, 1, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:45'),
(4, 'Discover Exotic Lands and Enjoy the Warmth and Comfort3', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort3', 'public/images/shop-4.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, 1, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:40'),
(5, 'Discover Exotic Lands and Enjoy the Warmth and Comfort4', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort4', 'public/images/shop-5.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, 1, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:37'),
(6, 'Discover Exotic Lands and Enjoy the Warmth and Comfort6', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort6', 'public/images/shop-6.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, 1, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:33');
INSERT INTO `tours` (`id`, `name`, `slug`, `image`, `duration`, `content`, `map`, `plan`, `hotel`, `file`, `status`, `highlight`, `parent_id`, `lang_id`, `category_id`, `faq_id`, `destination_id`, `price`, `price_promotion`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(7, 'Discover Exotic Lands and Enjoy the Warmth and Comfort7', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort7', 'public/images/shop-7.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, NULL, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:29'),
(8, 'Discover Exotic Lands and Enjoy the Warmth and Comfort8', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort8', 'public/images/shop-8.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, NULL, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:24'),
(9, 'Discover Exotic Lands and Enjoy the Warmth and Comfort9', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort9', 'public/images/shop-9.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, NULL, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:27:18'),
(10, 'Discover Exotic Lands and Enjoy the Warmth and Comfort10', 'discover-exotic-lands-and-enjoy-the-warmth-and-comfort10', 'public/images/shop-10.jpg', 5, '{"main":"<p>The &ldquo; Lux Signature Luxurious Experiences in Cambodia in 10 Days Tour &rdquo; provides the chance to explore the capital Phnom Penh, the legendary temples of Angkor and Tonle Sap Lake at a leisurely pace, combined with time to relax at the Song Saa Island Resort, which is set on two pristine islands in the Gulf of Thailand.<\\/p>","include":"<ul>\\r\\n\\t<li>Private door-to-door pick-up &amp; drop-off services<\\/li>\\r\\n\\t<li>All private transportation<\\/li>\\r\\n\\t<li>Accommodation shared in Double room with daily breakfast<\\/li>\\r\\n\\t<li>All domestic flights<\\/li>\\r\\n\\t<li>Experienced English speaking guide<\\/li>\\r\\n\\t<li>Private boat trip as per tour<\\/li>\\r\\n\\t<li>Meals as stated in the itinerary (B: Breakfast; L: Lunch; D: Dinner)<\\/li>\\r\\n\\t<li>All sightseeing tickets<\\/li>\\r\\n\\t<li>Tissues and water on coach<\\/li>\\r\\n\\t<li>Vietnam Visa On Arrival pre-approved letter<\\/li>\\r\\n\\t<li>Services charges and government tax<\\/li>\\r\\n\\t<li>Exclusive and personalized single agent\\/single customer service<\\/li>\\r\\n\\t<li>No extra charge after services confirmation<\\/li>\\r\\n\\t<li>24\\/7 hotline support guaranteed<\\/li>\\r\\n<\\/ul>","exclude":"<ul>\\r\\n\\t<li>International flights &amp; departure taxes if any<\\/li>\\r\\n\\t<li>Personal expenses, tips, and gratuities<\\/li>\\r\\n\\t<li>Travel insurance<\\/li>\\r\\n\\t<li>Beverage<\\/li>\\r\\n\\t<li>Peak season or public holidays surcharge (if any)<\\/li>\\r\\n\\t<li>Compulsory Gala dinner on X-Mas or New Year (if any)<\\/li>\\r\\n\\t<li>Vietnam visa stamp fee (US$ 25 &amp; 1 passport photo for each person)<\\/li>\\r\\n\\t<li>Other services not mentioned above<\\/li>\\r\\n<\\/ul>"}', '{"image":"public\\/images\\/map-2.jpg","link":"#"}', '{"1557480017340":{"name":"Day 1: Hoi An (D)","content":"<p><img alt=\\"\\" src=\\"http:\\/\\/developer5.gco.vn\\/luxury-travel\\/storage\\/app\\/public\\/images\\/tour-detail-2.jpg\\" style=\\"width: 770px; height: 322px;\\" \\/><\\/p>\\r\\n\\r\\n<p>Upon arrival at Hanoi&#39;s Noi Bai International Airport, you will be greeted by your guide and transferred to your hotel. This is Vietnam&#39;s capital and celebrated its anniversary of 1,000 years in 2010. Historical emphasis on the arts and learning stipulates that buildings in Hanoi remain small, simple and in harmony with nature. You will have the afternoon at your leisure before an evening welcome dinner at a local restaurant and an overnight stay in Hanoi .<\\/p>"},"1557480061235":{"name":"Day 2: City tour (B,L)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480091923":{"name":"Day 3: Halong Bay \\u2013 Overnight on Emperor Cruise with UNLIMITED services (*) (B,L,D)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480105762":{"name":"Day 4: Halong Bay \\u2013 Hanoi \\u2013 flight to Danang \\u2013 Hoi An (Brunch)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480126290":{"name":"Day 5: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480138371":{"name":"Day 6: Hoi An Wellness retreat (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"},"1557480158874":{"name":"Day 7: Hoi An \\u2013 Danang - Departure (B)","content":"<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.<\\/p>"}}', '{"1557480177428":{"type":"First Class Category \\/ 4-star hotel","content":{"1557480192555":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480194145":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480196025":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}},"1557480211045":{"type":"First Class Category \\/ 5-star hotel","content":{"1557480213049":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480213633":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"},"1557480227201":{"city":"PhnomPenh","name":"Sofitel Phnom Penh Phokeethra Hotel","type":"Deluxe"}}}}', 'public/files/2019-05-10_1557480172751_map-2.jpg', 1, NULL, NULL, NULL, 13, 1, 1, 7045, 2000, NULL, NULL, '2019-05-10 09:22:52', '2019-05-11 04:26:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'letien524@gmail.com', '$2y$10$MEbJk618o/5J.GoQlkG/AuncP9K5Cp3DkfwXMEyDAp/IigsZh0B92', NULL, 1, NULL, NULL, NULL),
(2, 'gco_admin', 'letien524@gmail.com', '$2y$10$60QN3uhKKmOCc/AGa.aXteg8H5liFdISbmar.KHEmwX9zLBwBbDEC', NULL, 1, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_category_post_id_foreign` (`post_id`),
  ADD KEY `blog_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `blog_tag`
--
ALTER TABLE `blog_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_tag_post_id_foreign` (`post_id`),
  ADD KEY `blog_tag_category_id_foreign` (`category_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faqs_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_tour_id_foreign` (`tour_id`);

--
-- Indexes for table `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_country_id_foreign` (`country_id`),
  ADD KEY `orders_tour_id_foreign` (`tour_id`);

--
-- Indexes for table `order_category`
--
ALTER TABLE `order_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_category_order_id_foreign` (`order_id`),
  ADD KEY `order_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_lang_id_foreign` (`lang_id`);

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tours_lang_id_foreign` (`lang_id`),
  ADD KEY `tours_category_id_foreign` (`category_id`),
  ADD KEY `tours_destination_id_foreign` (`destination_id`),
  ADD KEY `tours_faq_id_foreign` (`faq_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `blog_tag`
--
ALTER TABLE `blog_tag`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `langs`
--
ALTER TABLE `langs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_category`
--
ALTER TABLE `order_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD CONSTRAINT `blog_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_category_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_tag`
--
ALTER TABLE `blog_tag`
  ADD CONSTRAINT `blog_tag_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `langs` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `faqs`
--
ALTER TABLE `faqs`
  ADD CONSTRAINT `faqs_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `langs` (`id`);

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_tour_id_foreign` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `orders_tour_id_foreign` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_category`
--
ALTER TABLE `order_category`
  ADD CONSTRAINT `order_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_category_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `langs` (`id`);

--
-- Constraints for table `tours`
--
ALTER TABLE `tours`
  ADD CONSTRAINT `tours_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `tours_destination_id_foreign` FOREIGN KEY (`destination_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `tours_faq_id_foreign` FOREIGN KEY (`faq_id`) REFERENCES `faqs` (`id`),
  ADD CONSTRAINT `tours_lang_id_foreign` FOREIGN KEY (`lang_id`) REFERENCES `langs` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
