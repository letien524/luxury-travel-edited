$(function(){

    $('input[type=checkbox]').on('change', function () {
        console.log(this);
        $(this).closest('.col-lg-12').siblings('.col-lg-12').find('input').prop('checked', false);
    })

    $('#modal-default').on('hidden.bs.modal', function () {
        $(this).find('input').val('').prop('checked', false);
    })

    var updateOutput = function (e) {

        var list = e.length ? e : $(e.target);
        var output = list.data('output');

        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    }

    var actionNestable = function (e) {
        var item = $(e.target.parentElement);
        var action = item.data('action');
        var parent = $(e.target).closest('.dd');
        var list = parent.find('ol').first();
        var modal = $('#modal-default');


        if (typeof action === "undefined") {
            action = $(e.target).data('action')
        }


        switch (action) {
            case "itemDelete":
                var check = confirm("Bạn có chắc muốn xóa đi thành phần này ?");
                if (check) {
                    item.closest('li').remove();
                    updateOutput($('#nestable').data('output', $('#nestable-output')));
                }
                break;
            case "itemCreate":
                console.log('itemCreate');
                $("#itemStore").data('action', 'itemStore');
                break;
            case "itemStore":
                console.log('itemStore');
                var data = $('#formItemMenu').serializeArray();

                var name = data[0].value;
                var slug = data[1].value;
                var is_destination = '';
                var text = '';


                if (data.length > 2) {
                    if (data[2].name === 'is_destination')
                        is_destination = 'is_destination';
                    else
                        is_destination = 'is_activity';
                }
                text = '<a href="#" data-target="#modal-default" data-toggle="modal" title="' + slug + '" data-action="itemEdit" class="text-primary"><span>' +  slug.slice(0, 30) + (slug.length > 30 ? "..." : "") + '</span></a> | ';
                if (is_destination === 'is_destination') {
                    text += '<a href="#" data-target="#modal-default" data-toggle="modal" title="Destination" data-action="itemEdit" class="text-primary"><span class="badge label-warning">Destination</span></a> | ';
                } else if (is_destination === 'is_activity') {
                    text += '<a href="#" data-target="#modal-default" data-toggle="modal" title="Activity" data-action="itemEdit" class="text-primary"><span class="badge label-primary">Activity</span></a> | ';
                }


                var result = '<li class="dd-item" data-id="' + Date.now() + '" data-name="' + name + '" data-link="' + slug + '" data-destination="' + is_destination + '"> ' +
                    '<div class="dd-handle">' + name + '</div> ' +
                    '<div class="dd-action"> ' + text +
                    '<a href="#" data-target="#modal-default" data-toggle="modal" title="Sửa" data-action="itemEdit" class="text-primary">' +
                    '<i class="fa fa-pencil-square-o"></i></a> | ' +
                    '<a href="javascript:void(0)" title="Xóa" data-action="itemDelete" class="text-danger">' +
                    '<i class="fa fa-trash-o"></i></a>' +
                    ' </div> ' +
                    '</li>'

                list.append(result);
                updateOutput($('#nestable').data('output', $('#nestable-output')));

                modal.modal('hide');
                modal.find('input').val('').prop('checked', false);
                break;

            case "itemEdit":
                console.log('itemEdit');
                $("#itemStore").data('action', 'itemUpdate');

                var data = $(e.target).closest('li').data();
                console.log(data);
                var input = modal.find('input');

                $("#itemStore").data('id', data.id);

                input.each(function () {
                    var nameAttr = $(this).attr('name');

                    if (nameAttr === 'name') {
                        $(this).val(data.name);
                    } else if (nameAttr === 'slug') {
                        $(this).val(data.link);
                    } else if (nameAttr === 'is_destination') {
                        if (data.destination === 'is_destination') {
                            $(this).prop('checked', true);
                        }
                    } else if (nameAttr === 'is_activity') {
                        if (data.destination === 'is_activity') {
                            $(this).prop('checked', true);
                        }
                    }

                });
                break;
            case "itemUpdate":
                console.log('itemUpdate');

                var data = $('#formItemMenu').serializeArray();

                var name = data[0].value;
                var slug = data[1].value;
                var is_destination = '';
                var text = '';


                if (data.length > 2) {
                    if (data[2].name === 'is_destination')
                        is_destination = 'is_destination';
                    else
                        is_destination = 'is_activity';
                }
                text = '<a href="#" data-target="#modal-default" data-toggle="modal" title="' + slug + '" data-action="itemEdit" class="text-primary"><span>' + slug.slice(0, 30) + (slug.length > 30 ? "..." : "") + '</span></a> | ';
                if (is_destination === 'is_destination') {
                    text += '<a href="#" data-target="#modal-default" data-toggle="modal" title="Destination" data-action="itemEdit" class="text-primary"><span class="badge label-warning">Destination</span></a> | ';
                } else if (is_destination === 'is_activity') {
                    text += '<a href="#" data-target="#modal-default" data-toggle="modal" title="Activity" data-action="itemEdit" class="text-primary"><span class="badge label-primary">Activity</span></a> | ';
                }


                var result = '<li class="dd-item" data-id="' + $("#itemStore").data('id') + '" data-name="' + name + '" data-link="' + slug + '" data-destination="' + is_destination + '"> ' +
                    '<div class="dd-handle">' + name + '</div> ' +
                    '<div class="dd-action"> ' + text +
                    '<a href="#" data-target="#modal-default" data-toggle="modal" title="Sửa" data-action="itemEdit" class="text-primary">' +
                    '<i class="fa fa-pencil-square-o"></i></a> | ' +
                    '<a href="javascript:void(0)" title="Xóa" data-action="itemDelete" class="text-danger">' +
                    '<i class="fa fa-trash-o"></i></a>' +
                    ' </div> ' +
                    '</li>'

                $('li[data-id=' + $("#itemStore").data('id') + ']').replaceWith(result);
                updateOutput($('#nestable').data('output', $('#nestable-output')));

                modal.modal('hide');
                modal.find('input').val('').prop('checked', false);


                break;
            default:
                break;
        }
    };


    $('#nestable').nestable().on('change', updateOutput).on('click', actionNestable);

    updateOutput($('#nestable').data('output', $('#nestable-output')));

});